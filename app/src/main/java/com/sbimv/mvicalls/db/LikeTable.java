package com.sbimv.mvicalls.db;

import android.provider.BaseColumns;


/**
 * Created by Yudha Pratama Putra on 01/11/18.
 */
public final class LikeTable {
    private LikeTable(){}

    public static final String[] ALL_COLUMNS = {
            Entry._ID,
            Entry.COLUMN_NAME_CALLER_ID_UPLOADER,
            Entry.COLUMN_NAME_CALLER_ID,
            Entry.COLUMN_NAME_STATUS_COMMENT,
            Entry.COLUMN_NAME_SOURCE_VIDEO
    };

    public static class Entry implements BaseColumns {
        public static final String TABLE_NAME = "tbl_like";
        public static final String COLUMN_NAME_CALLER_ID_UPLOADER = "caller_id_uploader";
        public static final String COLUMN_NAME_CALLER_ID = "caller_id";
        public static final String COLUMN_NAME_STATUS_COMMENT = "status_comment";
        public static final String COLUMN_NAME_SEND_COMMENT = "send";
        public static final String COLUMN_NAME_SOURCE_VIDEO = "source_video";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                    Entry._ID + " INTEGER PRIMARY KEY autoincrement," +
                    Entry.COLUMN_NAME_CALLER_ID_UPLOADER + " TEXT UNIQUE," +
                    Entry.COLUMN_NAME_CALLER_ID + " TEXT," +
                    Entry.COLUMN_NAME_STATUS_COMMENT + " TEXT," +
                    Entry.COLUMN_NAME_SEND_COMMENT + " TEXT," +
                    Entry.COLUMN_NAME_SOURCE_VIDEO + " TEXT)";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Entry.TABLE_NAME;
}
