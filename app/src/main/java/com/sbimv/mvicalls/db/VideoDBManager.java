package com.sbimv.mvicalls.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Hendi on 08/29/18.
 */

public class VideoDBManager {
    static final String TAG = VideoDBManager.class.getSimpleName();
    static VideoDBManager instance;

    DBHelper dbHelper;

    public VideoDBManager(Context applicationContext){
        dbHelper = DBHelper.getInstance(applicationContext);
    }

    public static VideoDBManager getInstance(Context appContext){
        if(instance == null)
            instance = new VideoDBManager(appContext);
        return instance;
    }

    public long insertOrUpdateVideo(String caller_id, String filePath){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        long result;
        try {
            // insert new record
            values.put(VideoTable.Entry.COLUMN_NAME_CALLER_ID, caller_id);
            values.put(VideoTable.Entry.COLUMN_NAME_LAST_PATH, filePath);

            result = db.insertOrThrow(
                    VideoTable.Entry.TABLE_NAME,
                    null,
                    values);
        }
        catch (SQLException e){
            // update existing record
            values.put(VideoTable.Entry.COLUMN_NAME_LAST_PATH, filePath);

            result = db.update(
                    VideoTable.Entry.TABLE_NAME,
                    values,
                    VideoTable.Entry.COLUMN_NAME_CALLER_ID+"=?",
                    new String[]{caller_id});
        }

        // close database
        //db.close();
        return result;
    }

    public String getLastPath(String caller_id){
        String lastPath = null;
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query(
                VideoTable.Entry.TABLE_NAME,
                VideoTable.ALL_COLUMNS,
                VideoTable.Entry.COLUMN_NAME_CALLER_ID + "=?",
                new String[]{caller_id},
                null,
                null,
                null);

        if (cursor.getCount() > 0 && cursor.moveToFirst()) {
            lastPath = cursor.getString(cursor.getColumnIndex(VideoTable.Entry.COLUMN_NAME_LAST_PATH));
        }

        return lastPath;
    }
}
