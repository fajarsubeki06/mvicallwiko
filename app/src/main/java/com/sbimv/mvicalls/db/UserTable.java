package com.sbimv.mvicalls.db;

import android.provider.BaseColumns;

/**
 * Created by Hendi on 10/21/17.
 */

public final class UserTable {
    private UserTable(){}

    public static final String[] ALL_COLUMNS = {
            UserEntry._ID,
            UserEntry.COLUMN_NAME_CALLER_ID,
            UserEntry.COLUMN_NAME_NAME,
//            UserEntry.COLUMN_NAME_NAME_PHONE_BOOK,// added name phone book
            UserEntry.COLUMN_NAME_MSISDN,
            UserEntry.COLUMN_NAME_PIC,
            UserEntry.COLUMN_NAME_STATUS,
            UserEntry.COLUMN_NAME_DEVICE,
            UserEntry.COLUMN_NAME_VIDEO,
            UserEntry.COLUMN_NAME_PROFILES,
            UserEntry.COLUMN_NAME_RATIO,
            UserEntry.COLUMN_NAME_PROFILE_STATUS
    };

    public static class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "tbl_caller";
        public static final String COLUMN_NAME_CALLER_ID = "caller_id";
        public static final String COLUMN_NAME_NAME = "name";
//        public static final String COLUMN_NAME_NAME_PHONE_BOOK = "name_phone_book";// added name phone book
        public static final String COLUMN_NAME_MSISDN = "msisdn";
        public static final String COLUMN_NAME_PIC = "pic";
        public static final String COLUMN_NAME_STATUS = "status";
        public static final String COLUMN_NAME_DEVICE = "device";
        public static final String COLUMN_NAME_VIDEO = "video";
        public static final String COLUMN_NAME_PROFILES = "profiles";
        public static final String COLUMN_NAME_RATIO = "ratio";
        public static final String COLUMN_NAME_PROFILE_STATUS = "profile_status";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + UserEntry.TABLE_NAME + " (" +
                    UserEntry._ID + " INTEGER PRIMARY KEY," +
                    UserEntry.COLUMN_NAME_CALLER_ID + " TEXT UNIQUE," +
                    UserEntry.COLUMN_NAME_NAME + " TEXT," +
//                    UserEntry.COLUMN_NAME_NAME_PHONE_BOOK + " TEXT," +
                    UserEntry.COLUMN_NAME_MSISDN + " TEXT UNIQUE," +
                    UserEntry.COLUMN_NAME_PIC + " TEXT," +
                    UserEntry.COLUMN_NAME_STATUS + " TEXT," +
                    UserEntry.COLUMN_NAME_DEVICE + " TEXT," +
                    UserEntry.COLUMN_NAME_PROFILES + " TEXT," +
                    UserEntry.COLUMN_NAME_PROFILE_STATUS + " TEXT," +
                    UserEntry.COLUMN_NAME_RATIO + " TEXT," +
                    UserEntry.COLUMN_NAME_VIDEO + " TEXT)"; // added name phone book

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + UserEntry.TABLE_NAME; // added name phone book
}
