package com.sbimv.mvicalls.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sbimv.mvicalls.activity.ContactTabActivity;
import com.sbimv.mvicalls.sync.SyncUtils;

/**
 * Created by Hendi on 10/21/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 7;
    public static final String DATABASE_NAME = "mvicall.db";

    private static DBHelper mInstance = null;
    private Context mContext;

    public static DBHelper getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new DBHelper(ctx.getApplicationContext());
        }
        return mInstance;
    }

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(UserTable.SQL_CREATE_ENTRIES); // added name phone book
        db.execSQL(VideoTable.SQL_CREATE_ENTRIES);
        db.execSQL(LikeTable.SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(UserTable.SQL_DELETE_ENTRIES);
        db.execSQL(VideoTable.SQL_DELETE_ENTRIES);
        db.execSQL(LikeTable.SQL_DELETE_ENTRIES);
        onCreate(db);

        try {
            SyncUtils.permissionSyncContact(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
