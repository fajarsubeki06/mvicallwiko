package com.sbimv.mvicalls.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.ProfileItem;
import com.sbimv.mvicalls.util.Util;

/**
 * Created by Hendi on 11/8/17.
 */

public class ContactDBManager {

    static final String TAG = ContactDBManager.class.getSimpleName();
    static ContactDBManager instance;

    // variable
    private Context applicationContext;
    private DBHelper dbHelper;
    private Gson gson = new GsonBuilder().create();
    private Cursor cursor;
    private SQLiteDatabase database;
    private String prefix;


    // private constructor
    public ContactDBManager(Context applicationContext){
        this.applicationContext = applicationContext;
        // init db helper
        dbHelper = DBHelper.getInstance(applicationContext);
    }

    public static ContactDBManager getInstance(Context appContext){
        if(instance == null)
            instance = new ContactDBManager(appContext);
        return instance;
    }

    public void insertContact(ContactItem item){
        // define database
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // start insert
        ContentValues values = new ContentValues();
        values.put(UserTable.UserEntry.COLUMN_NAME_CALLER_ID, item.caller_id);
        values.put(UserTable.UserEntry.COLUMN_NAME_MSISDN, item.msisdn);
        values.put(UserTable.UserEntry.COLUMN_NAME_NAME, item.name.isEmpty()?"Unknown":item.name); // added name phone book
//        values.put(UserTable.UserEntry.COLUMN_NAME_NAME_PHONE_BOOK, item.name_phone_book == null?"Unknown":item.name_phone_book);// added name phone book
        values.put(UserTable.UserEntry.COLUMN_NAME_PIC, item.caller_pic);
        values.put(UserTable.UserEntry.COLUMN_NAME_VIDEO, item.video_caller);
        values.put(UserTable.UserEntry.COLUMN_NAME_STATUS, item.status_caller);
        values.put(UserTable.UserEntry.COLUMN_NAME_DEVICE, item.device);
        values.put(UserTable.UserEntry.COLUMN_NAME_RATIO, item.ratio);
        values.put(UserTable.UserEntry.COLUMN_NAME_PROFILE_STATUS, item.profile_status);
        // convert profiles into string
        String profilesStr = gson.toJson(item.profiles, ProfileItem.class);
        values.put(UserTable.UserEntry.COLUMN_NAME_PROFILES, profilesStr);
        // do insert
        try {
            db.insertOrThrow(UserTable.UserEntry.TABLE_NAME, null, values);
        }catch (SQLException e){
            values.remove(UserTable.UserEntry.COLUMN_NAME_CALLER_ID);
            values.remove(UserTable.UserEntry.COLUMN_NAME_MSISDN);

            long updateRes = updateIfExist(item.msisdn, values, db); // update if get constraint exception
            Log.d(TAG, "CONFLICT, update result : "+updateRes);
        }

        // close database
//        db.close();
    }

    private long updateIfExist(String key, ContentValues cv, SQLiteDatabase db){
        return db.update(
                UserTable.UserEntry.TABLE_NAME,
                cv,
                UserTable.UserEntry.COLUMN_NAME_MSISDN+"=?",
                new String[]{key});
    }

    // ==========================================================================================================================================================
    // ====================== INSERT DATA LIKERS ================================================================================================================
    // ==========================================================================================================================================================
    public void insertLikers(String caller_id, String caller_id_uploader, String status_comment, String send, String source_video){
        // define database
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // start insert
        ContentValues value = new ContentValues();
        value.put(LikeTable.Entry.COLUMN_NAME_CALLER_ID, caller_id);
        value.put(LikeTable.Entry.COLUMN_NAME_CALLER_ID_UPLOADER, caller_id_uploader);
        value.put(LikeTable.Entry.COLUMN_NAME_STATUS_COMMENT, status_comment);
        value.put(LikeTable.Entry.COLUMN_NAME_SEND_COMMENT, send);
        value.put(LikeTable.Entry.COLUMN_NAME_SOURCE_VIDEO, source_video);

        // do insert
        try {
            db.insertOrThrow(LikeTable.Entry.TABLE_NAME, null, value);
        }catch (SQLException e){
            value.remove(LikeTable.Entry.COLUMN_NAME_CALLER_ID_UPLOADER);
            long updateRest = updateLikeIfExist(caller_id_uploader, value, db); // update if get constraint exception
            Log.d("", "CONFLICT, update result : "+updateRest);
        }
    }

    private long updateLikeIfExist(String key, ContentValues cv, SQLiteDatabase db){
        return db.update(LikeTable.Entry.TABLE_NAME, cv, LikeTable.Entry.COLUMN_NAME_CALLER_ID_UPLOADER+"=?", new String[]{key});
    }
    // ==========================================================================================================================================================
    // ==========================================================================================================================================================
    // ==========================================================================================================================================================

    public ContactItem getCallerByMSISDN(String msisdn){
        String prefix = AppPreference.getPreference(instance.applicationContext, AppPreference.PREFIX);
        msisdn = Util.formatMSISDNREG(msisdn,prefix);

        ContactItem item = null;

        if(!TextUtils.isEmpty(msisdn)) {
            SQLiteDatabase database = DBHelper
                    .getInstance(applicationContext)
                    .getReadableDatabase();

            Cursor cursor = database.query(
                    UserTable.UserEntry.TABLE_NAME,
                    UserTable.ALL_COLUMNS,
                    UserTable.UserEntry.COLUMN_NAME_MSISDN + "=?",
                    new String[]{msisdn},
                    null,
                    null,
                    null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    item = new ContactItem();
                    item.caller_id = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_CALLER_ID));
                    item.name = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_NAME)); // added name phone book
//                    item.name_phone_book = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_NAME_PHONE_BOOK)); // added name phone book
                    item.msisdn = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_MSISDN));
                    item.caller_pic = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_PIC));
                    item.video_caller = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_VIDEO));
                    item.status_caller = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_STATUS));
                    item.ratio = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_RATIO));
                    item.profile_status = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_PROFILE_STATUS));

                    String profilesStr = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_PROFILES));
                    ProfileItem profile = gson.fromJson(profilesStr, ProfileItem.class);
                    item.profiles = profile;
                    break;
                }
            }
//            cursor.close();
        }
        return item;
    }

    public ContactItem getCallerByID(String callerId){
        String prefix = AppPreference.getPreference(instance.applicationContext, AppPreference.PREFIX);
        callerId = Util.formatMSISDNREG(callerId,prefix);

        ContactItem item = null;

        if(!TextUtils.isEmpty(callerId)) {
            SQLiteDatabase database = DBHelper
                    .getInstance(applicationContext)
                    .getReadableDatabase();

            Cursor cursor = database.query(
                    UserTable.UserEntry.TABLE_NAME,
                    UserTable.ALL_COLUMNS,
                    UserTable.UserEntry.COLUMN_NAME_CALLER_ID + "=?",
                    new String[]{callerId},
                    null,
                    null,
                    null);

            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    item = new ContactItem();
                    item.caller_id = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_CALLER_ID));
                    item.name = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_NAME)); // added name phone book
                    item.msisdn = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_MSISDN));
                    item.caller_pic = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_PIC));
                    item.video_caller = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_VIDEO));
                    item.status_caller = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_STATUS));
                    item.profile_status = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_PROFILE_STATUS));

                    String profilesStr = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_PROFILES));
                    ProfileItem profile = gson.fromJson(profilesStr, ProfileItem.class);
                    item.profiles = profile;
                    break;
                }
            }
//            cursor.close();
        }
        return item;
    }

    public String [] getRegisteredMSISDN(String [] msisdn){

        if(msisdn != null && msisdn.length > 3 && msisdn.length != 0){
            String whereStatement = "";
            for(int i=0; i<msisdn.length; i++){
                whereStatement += ",";
                if(i==0){
                    whereStatement += "(?";
                }
                else if(i==msisdn.length-1){
                    whereStatement += "?)";
                }
                else{
                    whereStatement += "?";
                }
            }

            whereStatement = whereStatement.substring(1);
            whereStatement = UserTable.UserEntry.COLUMN_NAME_MSISDN + " IN " + whereStatement;

            SQLiteDatabase database = DBHelper
                    .getInstance(applicationContext)
                    .getReadableDatabase();

            Cursor cursor = database.query(
                    UserTable.UserEntry.TABLE_NAME,
                    UserTable.ALL_COLUMNS,
                    whereStatement,
                    msisdn,
                    null,
                    null,
                    null);

            String [] result = null;
            if (cursor.getCount() > 0) {
                int index = 0;
                result = new String[cursor.getCount()];
                while (cursor.moveToNext()) {
                    result[index] = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_MSISDN));
                    index++;
                }
            }
            cursor.close();
            return result;
        }
        return null;
    }

    public Cursor getCallerMSISDN(String msisdn) {
        String selectQuery = "SELECT * FROM "+UserTable.UserEntry.TABLE_NAME+" WHERE msisdn = '"+msisdn+"'";

        SQLiteDatabase database = DBHelper
                .getInstance(applicationContext)
                .getReadableDatabase();

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor == null) {
            return null;
        }else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }

        return cursor;

    }

    public Cursor getUserMV(String msisdn) {
        String selectQuery = "SELECT msisdn FROM "+UserTable.UserEntry.TABLE_NAME+" WHERE msisdn = '"+isSparator(msisdn)+"'";

        SQLiteDatabase database = DBHelper
                .getInstance(applicationContext)
                .getReadableDatabase();

        Cursor cursor = database.rawQuery(selectQuery, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;

    }

    public Cursor getMVContactAll() { // added name phone book, pencarian berdasarkan name phone book karna name phone book not null - checkout old version 19-12-2018

        try {
            prefix = AppPreference.getPreference(applicationContext,AppPreference.PREFIX);
        }catch (Exception e){
            if (e instanceof NullPointerException){
                Toast.makeText(applicationContext, "Failed while getting Friends data", Toast.LENGTH_SHORT).show();
                Log.e("ContactDBManager","prefix null");
            }
        }

        String selectQuery = "SELECT * FROM "+UserTable.UserEntry.TABLE_NAME+" WHERE msisdn NOT IN ('"+prefix+"','+0') ORDER BY "+UserTable.UserEntry.COLUMN_NAME_NAME+" COLLATE NOCASE ASC "; // added name phone book
        SQLiteDatabase database = DBHelper.getInstance(applicationContext).getReadableDatabase();
        Cursor cursor = database.rawQuery(selectQuery, null); // added name phone book - checkout old version 19-12-2018 (COLUMN_NAME_NAME_PHONE_BOOK)

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;

    }

    public String[] selectAllDataNameUsers() {
        try {
            String[] arrData = null;

            SQLiteDatabase database = DBHelper
                    .getInstance(applicationContext)
                    .getReadableDatabase();

            String selectQuery = "SELECT name,msisdn FROM " + UserTable.UserEntry.TABLE_NAME;
            Cursor cursor = database.rawQuery(selectQuery, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    arrData = new String[cursor.getCount()];
                    int i = 0;
                    do {
                        String name = cursor.getString(0);
                        String number = cursor.getString(1);
                        if (TextUtils.isEmpty(name) && TextUtils.isEmpty(number))
                            return null;
                            arrData[i] = name+", "+number;
                        i++;
                    } while (cursor.moveToNext());
                }
            }
            cursor.close();
            return arrData;
        } catch (Exception e) {
            return null;
        }
    }

    public Cursor getCriteriaContact(String arg) { // added name phone book, pencarian berdasarkan name phone book karna name phone book not null - checkout old version 19-12-2018
        String selectQuery = "SELECT * FROM " + UserTable.UserEntry.TABLE_NAME
                + " WHERE "+ UserTable.UserEntry.COLUMN_NAME_NAME + " LIKE '%" + isSparator(arg) + "%'"
                +" AND msisdn NOT IN ('"+prefix+"','+0')"
                +" ORDER BY "+UserTable.UserEntry.COLUMN_NAME_NAME
                +"  COLLATE NOCASE ASC"; // added name phone book - checkout old version 19-12-2018 (COLUMN_NAME_NAME_PHONE_BOOK)

        SQLiteDatabase database = DBHelper
                .getInstance(applicationContext)
                .getReadableDatabase();

        return database.rawQuery(selectQuery, null);

    }

    private String isSparator(String... arg){
        if (!arg[0].contains("'")){
            return arg[0];
        }
        return "";
    }

}
