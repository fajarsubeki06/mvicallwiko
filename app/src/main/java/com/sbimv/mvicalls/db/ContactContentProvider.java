package com.sbimv.mvicalls.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Hendi on 11/8/17.
 */

public class ContactContentProvider extends ContentProvider{

    private static final String AUTHORITY = "com.sbimv.mvicalls.db";
    private static final String BASE_PATH = "tbl_caller";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH );
    private static final int CONTACTS = 1;
    private static final int CONTACT_ID = 2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY,BASE_PATH, CONTACTS);
        uriMatcher.addURI(AUTHORITY,BASE_PATH + "/*",CONTACT_ID);
    }

    DBHelper mDBHelper;

    @Override
    public boolean onCreate() {
        mDBHelper = DBHelper.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        SQLiteDatabase database = mDBHelper.getReadableDatabase();
        Cursor cursor;
        switch (uriMatcher.match(uri)) {
            case CONTACTS:
                cursor =  database.query(UserTable.UserEntry.TABLE_NAME, UserTable.ALL_COLUMNS,
                        s,null,null,null,UserTable.UserEntry.COLUMN_NAME_NAME +" ASC");
                break;
            case CONTACT_ID:
                String val = uri.getLastPathSegment();
                val = "%"+val+"%";
                String whereStatement = UserTable.UserEntry.COLUMN_NAME_NAME + " like ?";

                cursor =  database.query(UserTable.UserEntry.TABLE_NAME, UserTable.ALL_COLUMNS,
                        whereStatement, new String[]{val},null,null,UserTable.UserEntry.COLUMN_NAME_NAME +" ASC");
                break;
            default:
                throw new IllegalArgumentException("This is an Unknown URI " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(),uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
