package com.sbimv.mvicalls.db;

import android.provider.BaseColumns;

public final class MessageTable {

    private MessageTable(){}

    public static final String[] ALL_COLUMNS = {
            Entry._ID,
            Entry.COLUMN_NAME_TITLE,
            Entry.COLUMN_NAME_MESSAGE,
            Entry.COLUMN_NAME_TIME_STAMP
    };

    public static class Entry implements BaseColumns {
        public static final String TABLE_NAME = "tbl_inbox";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_MESSAGE = "message";
        public static final String COLUMN_NAME_TIME_STAMP = "timestamp";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                    Entry._ID + " INTEGER PRIMARY KEY," +
                    Entry.COLUMN_NAME_TITLE + " TEXT," +
                    Entry.COLUMN_NAME_MESSAGE + " TEXT," +
                    Entry.COLUMN_NAME_TIME_STAMP + " TEXT UNIQUE)";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Entry.TABLE_NAME;
}
