package com.sbimv.mvicalls.db;

import android.provider.BaseColumns;

/**
 * Created by Hendi on 08/29/18.
 */

public final class VideoTable {
    private VideoTable(){}

    public static final String[] ALL_COLUMNS = {
            Entry._ID,
            Entry.COLUMN_NAME_CALLER_ID,
            Entry.COLUMN_NAME_LAST_PATH
    };

    public static class Entry implements BaseColumns {
        public static final String TABLE_NAME = "tbl_video_caller";
        public static final String COLUMN_NAME_CALLER_ID = "caller_id";
        public static final String COLUMN_NAME_LAST_PATH = "last_path";
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Entry.TABLE_NAME + " (" +
                    Entry._ID + " INTEGER PRIMARY KEY," +
                    Entry.COLUMN_NAME_CALLER_ID + " TEXT UNIQUE," +
                    Entry.COLUMN_NAME_LAST_PATH + " TEXT)";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Entry.TABLE_NAME;
}
