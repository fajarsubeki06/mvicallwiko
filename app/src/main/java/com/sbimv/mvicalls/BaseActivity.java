package com.sbimv.mvicalls;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;
import android.provider.Settings;
import android.provider.Telephony;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.StatsLog;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustEvent;
import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sbimv.mvicalls.activity.InboxActivity;
import com.sbimv.mvicalls.activity.MainActivity;
import com.sbimv.mvicalls.activity.SplashActivity;
import com.sbimv.mvicalls.activity.UploadVideoToneActivity;
import com.sbimv.mvicalls.apprtc.AppStatusBarNotifier;
import com.sbimv.mvicalls.apprtc.OutgoingScreenActivity;
import com.sbimv.mvicalls.apprtc.SinchService;
import com.sbimv.mvicalls.firebase.NotificationUtils;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.jobscheduler.MJobScheduler;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataSubs;
import com.sbimv.mvicalls.pojo.DataSubscriberUser;
import com.sbimv.mvicalls.pojo.InboxModel;
import com.sbimv.mvicalls.pojo.ModelTopic;
import com.sbimv.mvicalls.services.SyncConfigService;
import com.sbimv.mvicalls.sync.SyncConfig;
import com.sbimv.mvicalls.sync.SyncUtils;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseListener;
import com.sbimv.mvicalls.util.ShowCaseWrapper;
import com.sbimv.mvicalls.view.DialogUpload;
import com.sbimv.mvicalls.walktrough.FriendsDemoActivity;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import agency.tango.materialintroscreen.BuildConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseActivity extends AppCompatActivity implements ServiceConnection, ComponentCallbacks2 {

    public static MediaController mediaController;
    private SinchService.SinchServiceInterface mSinchServiceInterface;

    private static final int JOB_ID = 1001;
// flagging/ Tagging
//    public static final String SUBS_SHORT_CODE = "92355";
    public static boolean contentBuys = false;
    public static boolean firstGetData = false;
    public static String[] permissions = new String[]{
//            Manifest.permission.PROCESS_OUTGOING_CALLS,
//            Manifest.permission.READ_CALL_LOG,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.ACCESS_NOTIFICATION_POLICY};
    protected static LangViewModel model;
    private static List<SubscriptionInfo> subsInfoList;
    private static ProgressDialog pdg;

//    final String[] XLSim = {"+62817","+62818","+62819","+62859","+62877","+62878"};
//    final String[] isatSim = {"+62855","+62856","+62857","+62858","+62814","+62815","+62816"};
//    final String[] tselSim = {"+62811", "+62812", "+62813", "+62821", "+62822", "+62823", "+62852", "+62853", "+62851"};

    private static int iCounter;
    protected boolean isViewAttached = false;
    protected Toolbar toolbar;
    protected boolean mBackButtonEnable = true;
    private final int mDoubleClickInterval = 400;
    /*
     * Firebase Remote Config............
     * */
    protected FirebaseRemoteConfig mFirebaseRemoteConfig;
    protected String SWITCH_PAYMENT_METHOD = "payment_method";
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String TAG = "";
    //    private ContactItem own;
    private android.app.AlertDialog dialog;
    private DialogUpload dialogUpload;
    private long LAST_CLICK_TIME = 0;
    private TextView txtCount;
    private RelativeLayout toInbox;
    private FirebaseAuth mAuth;
    private JobScheduler jobScheduler;
    private JobInfo jobInfo;
    //FIREBASE
    private ProgressDialog pd;
    private BroadcastReceiver broadcastReceiver;
    private boolean subsFree = false;
    private String shareLink;
    private boolean isActive = false;
    private ProgressDialog progressD;
    //Set Timer
    public static int TIME_LOADING = 2000;
    private PopupWindow mPopupWindow;
    private boolean mIsPopupVisible;

    protected static boolean thisNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    // added by Hendi 22 November 2018
    public static void getSubscriptionStatus(String phoneNumber, final SubscriptionStatusListener listener) {
        Call<DataSubscriberUser> call = ServicesFactory.getService().getStatusSubscribe(phoneNumber);
        call.enqueue(new Callback<DataSubscriberUser>() {
            @Override
            public void onResponse(@NonNull Call<DataSubscriberUser> call, @NonNull Response<DataSubscriberUser> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getData() != null && response.body().getData().size() > 0) {

                    DataSubs mData = response.body().getData().get(0);
                    listener.onComplete(mData);

                    String status = mData.getStatus();
                    String subs_start = selectionCharacter(mData.getSub_start());
                    String server_date = selectionCharacter(mData.getServer_date());

                    if (!TextUtils.isEmpty(status) && !TextUtils.isEmpty(subs_start) && !TextUtils.isEmpty(server_date)){
                        if (status.equals("1") && subs_start.equals(server_date)){
                            Log.i("response","Adjust Success");

                            AdjustEvent event = new AdjustEvent("n7yxf2"); // Adjust event tracker,
                            event.setCallbackId("aj_app_register_complete");
                            event.addCallbackParameter("key", "value");
                            event.addPartnerParameter("foo", "bar");
                            Adjust.trackEvent(event);

                        }
                    }

                    Log.i("response","Success");

                } else {
                    listener.onComplete(null);
                }
            }

            @Override
            public void onFailure(Call<DataSubscriberUser> call, Throwable t) {
                listener.onFail();
            }
        });
    }

    // added by Hendi 22 November 2018
    public static void getSubscriptionStatus(Activity activity, String phoneNumber, final SubscriptionStatusListener listener) {
        if (activity == null){
            return;
        }
        Call<DataSubscriberUser> call = ServicesFactory.getService().getStatusSubscribe(phoneNumber);
        call.enqueue(new Callback<DataSubscriberUser>() {
            @Override
            public void onResponse(@NonNull Call<DataSubscriberUser> call, @NonNull Response<DataSubscriberUser> response) {
                if (response.isSuccessful() && response.body() != null && response.body().getData() != null && response.body().getData().size() > 0) {

                    DataSubs mData = response.body().getData().get(0);
                    listener.onComplete(mData);

                    String status = mData.getStatus();
                    String subs_start = selectionCharacter(mData.getSub_start());
                    String server_date = selectionCharacter(mData.getServer_date());

                    if (!TextUtils.isEmpty(status) && !TextUtils.isEmpty(subs_start) && !TextUtils.isEmpty(server_date)){
                        if (status.equals("1") && subs_start.equals(server_date)){
                            Log.i("response","Adjust Success");

                            AdjustEvent event = new AdjustEvent("n7yxf2"); // Adjust event tracker,
                            event.setCallbackId("aj_app_register_complete");
                            event.addCallbackParameter("key", "value");
                            event.addPartnerParameter("foo", "bar");
                            Adjust.trackEvent(event);

                            try {
                                AppEventsLogger logger = AppEventsLogger.newLogger(activity);
                                Bundle params = new Bundle();
                                params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, "registration_complete");
                                logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }
                    }

                    Log.i("response","Success");

                } else {
                    listener.onComplete(null);
                }
            }

            @Override
            public void onFailure(Call<DataSubscriberUser> call, Throwable t) {
                listener.onFail();
            }
        });
    }

    private static String selectionCharacter(String arg){
        String selection = "";
        try {
            if (!TextUtils.isEmpty(arg)) {
                String[] split = arg.split(" ");
                selection = split[0].substring(0, 10);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return selection;
    }

    public static boolean isSubscribeActive(DataSubs dataSubs) {
        return dataSubs != null && dataSubs.getStatus() != null && dataSubs.getStatus().equals("1");
    }

    public static boolean isFreemiumActive(DataSubs dataSubs) {
        return dataSubs != null && dataSubs.free_duration > 0;
    }


    /**
     * Detect SIM Card
     * Added by Yudha Pratama Putra, 04 Sep 2018
     */
    public static boolean checkSIMState(Activity activity) {
        boolean dual = false;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
            SubscriptionManager subscriptionManager = SubscriptionManager.from(activity);

            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                subsInfoList = subscriptionManager.getActiveSubscriptionInfoList();
                if (subsInfoList.size() == 1) {
                    dual = false;
                } else if (subsInfoList.size() == 2) {
                    dual = true;
                }
            } else {
                Toast.makeText(activity, "Check your permission Phone State...", Toast.LENGTH_SHORT).show();
            }
        }
        return dual;
    }

    /**
     * Common send sms method
     * Added by Hendi, 03 Sep 2018
     */
    public static void sendSMS(String destination, String smsContent) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(destination, null, smsContent, null, null);
    }

    /**
     * Common send sms method with SubscriptionId
     * Added by Yudha Pratama Putra, 04 Sep 2018
     */
    public static void sendSMS(String destination, String smsContent, int subsID) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            SmsManager.getSmsManagerForSubscriptionId(subsID).sendTextMessage(destination, null, smsContent, null, null);
        }
    }

    /**
     * Waiting dialog after compose SMS. Waiting dialog will be shown after user click subscribe or buy content.
     * Added by Hendi, 03 Sep 2018
     */
    public static void sendSMSChargingAndWaiting(Activity activity, String destination, String smsContent) {

//        if (!checkSIMState(activity)) {
//            sendSMSChargingAndWaiting(activity, destination, smsContent, null);
//        } else {
//            showDualSIMDialog(activity, destination, smsContent, null);
//        }

        sendSMSChargingAndWaiting(activity, destination, smsContent, null);

    }

//========================================                                      ============================================
//========================================      FUNCTION FIREBASE DATABASE      ============================================
//========================================                                      ============================================

    public static void sendSMSChargingAndWaiting(final Activity activity, String destination, String smsContent, final ChargingListener listener) {

//        if (!checkSIMState(activity)) {
//            waitingProses(activity, destination, smsContent, listener);
//        } else {
//            showDualSIMDialog(activity, destination, smsContent, listener);
//        }

        showDialogSmsIntent(activity, destination, smsContent, listener);

    }

    public static void showDialogSmsIntent(final Activity activity, final String destination, final String smsContent, final ChargingListener listener) {

        if (!(activity).isFinishing()) {

            String msgDialog;
            String btnDialog;
            LangViewModel modelLang = LangViewModel.getInstance();
            ContactItem own = SessionManager.getProfile(activity);
            if (modelLang != null && own != null) {
                String strMsisdn = TextUtils.isEmpty(own.msisdn)?"":own.msisdn;

                msgDialog = TextUtils.isEmpty(modelLang.getLang().string.wordDialogTelcoPay+" "+modelLang.getLang().string.registeredNumber+" : "+strMsisdn) ?
                        "You are going to subscribe MViCall. Be sure to use registered number to send the sms registration."+" "+modelLang.getLang().string.registeredNumber+" : "+strMsisdn
                        : modelLang.getLang().string.wordDialogTelcoPay+" "+modelLang.getLang().string.registeredNumber+" : "+strMsisdn;
                btnDialog = TextUtils.isEmpty(modelLang.getLang().string.wordTelcoPosButton)
                        ? "Next"
                        : modelLang.getLang().string.wordTelcoPosButton;
            }else {
                msgDialog = "You are going to subscribe MViCall. Be sure to use registered number to send the sms registration. Registered number : ";
                btnDialog = "Next";
            }

            final AlertDialog dialog = new AlertDialog.Builder(activity)
                    .setMessage(msgDialog)
                    .setPositiveButton(btnDialog, null) //Set to null. We override the onclick
                    .setCancelable(true).create();

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button buttonPositive = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                    buttonPositive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            waitingProses(activity, destination, smsContent, listener);
                        }
                    });
                }
            });
            dialog.show();

        }
    }

//==============================================================================================================================
//==============================================================================================================================
//==============================================================================================================================

//========================================                                  ========================================
//========================================      FUNCTION CALL SERVICE       ========================================
//========================================                                  ========================================

    private static void waitingProses(final Activity activity, String destination, String smsContent, final ChargingListener listener) {

//        String title = activity.getResources().getString(R.string.title_delay_proses);
//        String message = activity.getResources().getString(R.string.body_delay_proses);
//
//        if(!((Activity) activity).isFinishing()){
//             pdg = ProgressDialog.show(activity, title, message, true, false);
//        }
//
//        long delayTime = 05 * 1000; // 5 seconds

//        contentBuys = true;
//        sendSMS(destination, smsContent);

//        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                contentBuys = false;
//                if(!((Activity) activity).isFinishing()){
//                    pdg.dismiss();
//                }
//                if(listener != null)
//                    listener.onDelayFinished();
//            }
//        }, delayTime);

        if (!activity.isFinishing()) {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("smsto:" + destination));  // This ensures only SMS apps respond
            intent.putExtra("sms_body", smsContent);
            if (intent.resolveActivity(activity.getPackageManager()) != null) {
                activity.startActivity(intent);
            } else {
                Toast.makeText(activity, "Aplikasi sms tidak di temukan. Anda harus tentukan pengaturan default sms", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Waiting Prosess
     * Added by Yudha Pratama Putra, 04 Sep 2018
     */
    private static void waitingProses(final Activity activity, String destination, String smsContent, final ChargingListener listener, final int subsID) {

//        String title = activity.getResources().getString(R.string.title_delay_proses);
//        String message = activity.getResources().getString(R.string.body_delay_proses);
//
//        if(!((Activity) activity).isFinishing()){
//            pdg = ProgressDialog.show(
//                    activity,
//                    title,
//                    message,
//                    true,
//                    false);
//        }

//        long delayTime = 10 * 1000; // 10 seconds

//        contentBuys = true;
//        sendSMS(destination, smsContent, subsID);

//        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                contentBuys = false;
//                if(!((Activity) activity).isFinishing()){
//                    pdg.dismiss();
//                }
//                if(listener != null)
//                    listener.onDelayFinished();
//            }
//        }, delayTime);

        if (!activity.isFinishing()) {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("smsto:" + destination));  // This ensures only SMS apps respond
            intent.putExtra("sms_body", smsContent);
            if (intent.resolveActivity(activity.getPackageManager()) != null) {
                activity.startActivity(intent);
            } else {
                Toast.makeText(activity, "Applikasi sms tidak di temukan. Anda harus tentukan settingan default sms", Toast.LENGTH_SHORT).show();
            }
        }

    }

    protected static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(LocaleHelper.onAttach(context));
    }

//===================================================================================================================
//===================================================================================================================
//===================================================================================================================
//========================================                                   ========================================
//========================================  FUNCTION FIREBASE NOTIFICATION   ========================================
//========================================                                   ========================================

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isViewAttached = true;
        model = LangViewModel.getInstance();

        // Get data user profile
//        try {
//            own = SessionManager.getProfile(this);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        try{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }catch (Exception e){
            e.printStackTrace();
        }

        /*
         * Firebase Remote Config............
         * */
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance(); // Firebase Remote Config - Yudha Pratama Puta, 15-01-2019
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(false).build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults); // * Required default value (Firebase Remote Config)....

        getApplicationContext().bindService(new Intent(this, SinchService.class), this, BIND_AUTO_CREATE);
        progressD = new ProgressDialog(this);


    }

    /*
     * Firebase Remote Config............
     * */
    protected void fetchSwitchPayment(OnCompleteListener<Void> listener) {
        long cacheExpiration = 100; // 1 hour in seconds.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }
        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(this, listener);
    }

    public String curLang() {
//        String lang = LocaleHelper.getLanguage(this);
        String lang = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
        if (TextUtils.isEmpty(lang)) {
            Locale locale = getResources().getConfiguration().locale;
            lang = locale.getLanguage();
        }
        return lang;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        contentBuys = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissionAllow();
        }

        // check dialog auto logout....
        boolean isDialogAutoLogout = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.DIALOG_AUTO_LOGOUT, false);
        if (isDialogAutoLogout) {
            processAutoLogout();
        }

    }

    @Override
    protected void onDestroy() {
        isViewAttached = false;
        super.onDestroy();
    }

    protected void processAutoLogout(){
        showDialogAutoLogout();
        new Handler().postDelayed(() -> {
            FirebaseAuth.getInstance().signOut();
            deleteAppData();
            PreferenceUtil.getEditor(BaseActivity.this).putBoolean(PreferenceUtil.DIALOG_AUTO_LOGOUT, false).commit();
        }, 3000);
    }

    protected void deleteAppData() {
        try {
            // clearing app data
            String packageName = getApplicationContext().getPackageName();
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("pm clear " + packageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Dialog Auto Logout.........
    protected void showDialogAutoLogout() {
        if (!BaseActivity.this.isFinishing()) {
            try {

                String titleLogout = TextUtils.isEmpty(model.getLang().string.titleLogout)
                        ? "Kamu baru saja login di Device Lain"
                        : model.getLang().string.titleLogout;

                String subtitleLogout = TextUtils.isEmpty(model.getLang().string.subtitleLogout)
                        ? "MviCallmu akan logout dari device ini"
                        : model.getLang().string.subtitleLogout;

                ProgressDialog pd = new ProgressDialog(BaseActivity.this);
                pd.setTitle(titleLogout);
                pd.setMessage(subtitleLogout);
                pd.setCancelable(false);
                pd.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//========================================                        ============================================
//========================================      APPRTC SINCH      ============================================
//========================================                        ============================================

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

        String strSinchComponen = SinchService.class.getName();
        String strComponen = componentName.getClassName();

        if(!TextUtils.isEmpty(strSinchComponen) && !TextUtils.isEmpty(strComponen)){
            if(strSinchComponen.equals(strComponen)){
                mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
                onServiceConnected();
            }
        }

    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

        String strSinchComponen = SinchService.class.getName();
        String strComponen = componentName.getClassName();

        if(!TextUtils.isEmpty(strSinchComponen) && !TextUtils.isEmpty(strComponen)){
            if(strSinchComponen.equals(strComponen)){
                mSinchServiceInterface = null;
                onServiceDisconnected();
                clearNotification();
            }
        }
    }

    protected void showNotification(String title, String content, String state){
        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(content)) {
            AppStatusBarNotifier updateStatusBarNotifier = AppStatusBarNotifier.getInstance(this);
            if (updateStatusBarNotifier != null) {
                updateStatusBarNotifier.updateNotification(title, content, state, OutgoingScreenActivity.class);
            }
        }
    }

    protected void clearNotification(){
        AppStatusBarNotifier appStatusBarNotifier = AppStatusBarNotifier.getInstance(this);
        if (appStatusBarNotifier != null) {
            appStatusBarNotifier.clearAllNotifications();
        }
    }

    protected void onServiceConnected() {
        // for subclasses
        System.out.println("onServiceConnected");
    }

    protected void onServiceDisconnected() {
        // for subclasses
        System.out.println("onServiceDisconnected");
    }

    protected SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }

//========================================                                      ============================================
//========================================      FUNCTION FIREBASE DATABASE      ============================================
//========================================                                      ============================================

    protected void checkConnection (CallBackConnection listener) {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference connectedRef = database.getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NotNull DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    listener.onResponse(true);
                }else {
                    listener.onResponse(false);
                }
            }

            @Override
            public void onCancelled(@NotNull DatabaseError error) {
                listener.onResponse(false);
            }
        });
    }

    public interface CallBackConnection {
        void onResponse(boolean connected);
    }

    protected void saveContactFirebase(ContactItem own) {
        if (own != null) {
            String callerId = own.caller_id;
            if (!TextUtils.isEmpty(own.caller_id)) {
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child("MviContact").child(callerId).setValue(own).addOnSuccessListener(aVoid ->
                        Log.i("firebase_db_report", "Berhasil menyimpan data"))
                        .addOnFailureListener(e ->
                                Log.i("firebase_db_report", "Gagal menyimpan data"));
            }
        }
    }

    protected void deleteContactDBFirebase() {
        ContactItem contact = SessionManager.getProfile(this);
        if (contact != null) {
            String uid = contact.caller_id;
            if (!TextUtils.isEmpty(uid)) {
                DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("MviContact").child(uid);
                current_user_db.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.i("succses_delete", "succses");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i("failed_delete", "failed");
                    }
                });
            }
        }

    }

    /**
     * Start Alarm Trigger
     * Update Yudha Pratama Putra, 18 sep 2018
     */
    protected void callAlarmTriggerService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            alarmSetTrigger();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ComponentName serviceComponent = new ComponentName(this, MJobScheduler.class);
                JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, serviceComponent);
                builder.setPeriodic(1000);
                builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
                builder.setPersisted(true);
                jobInfo = builder.build();
                jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
                startSchedule();
//                alarmSetTrigger();
            }
//            else if ((Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT)){
//               alarmSetTrigger();
//            }
        }
    }

    private void alarmSetTrigger() {
        Intent broadcastIntent = new Intent(".receiverNetwork.SensorRestarterBroadcastReceiver");
        sendBroadcast(broadcastIntent);
    }

    /**
     * Start Scheduler
     * Update Yudha Pratama Putra, 18 sep 2018
     */
    private void startSchedule() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            jobScheduler.schedule(jobInfo);
        }
    }


//=====================================================================================================================
//=====================================================================================================================
//=====================================================================================================================

//===================================================================================================================
//===================================================================================================================
//===================================================================================================================

//========================================                              =============================================
//========================================  FUNCTIONS FITURE INBOX      =============================================
//========================================                              =============================================

    /**
     * Stop Scheduler
     * Update Yudha Pratama Putra, 18 sep 2018
     */
    private void stopSchedule() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            jobScheduler.cancel(JOB_ID);
        }
    }


//===================================================================================================================
//===================================================================================================================
//===================================================================================================================

//========================================                              =============================================
//======================================== ALL FUNCTIONS FITURE BASIC   =============================================
//========================================                              =============================================

    /**
     * PopUp Firebase Push Notification
     * Update Yudha Pratama Putra, 07 sep 2018
     */
//    protected void firebaseLoadBroadcast() {
////        String ccodeViet = PreferenceUtil.getPref(BaseActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
////        if (!TextUtils.isEmpty(ccodeViet)) {
////            if (Objects.requireNonNull(ccodeViet).equalsIgnoreCase("+84")) {
////                LocalBroadcastManager.getInstance(BaseActivity.this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(ConfigPref.REGISTRATION_COMPLETE));
////                LocalBroadcastManager.getInstance(BaseActivity.this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
////                try {
////                    NotificationUtils.clearNotifications(BaseActivity.this);
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
////            }
////        }
//    }

    /**
     * Firebase Send Token
     * Added by Yudha Pratama Putra, 12 Sept 2018
     */
    protected void generateTokenFirebase(final ContactItem own) {
        if (own != null)
            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    if (task.isSuccessful()) {
                        String token = Objects.requireNonNull(task.getResult()).getToken();
                        if (!TextUtils.isEmpty(token)) {
                            PreferenceUtil.getEditor(BaseActivity.this).putString(PreferenceUtil.TOKEN_FCM, token).commit();
                            sendRegID(own.msisdn, token);
                        }
                    }
                }
            });
    }

    protected void generateTokenFirebase() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (task.isSuccessful()) {
                            String token = task.getResult().getToken();
                            if (!TextUtils.isEmpty(token)) {
                                PreferenceUtil.getEditor(BaseActivity.this).putString(PreferenceUtil.TOKEN_FCM, token).commit();
                            }
                        }
                    }
                });
        SyncUtils.permissionSyncContact(BaseActivity.this);
    }

//    protected void firebasePauseBroadcast() {
////        String ccodeViet = PreferenceUtil.getPref(BaseActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
////        if (!TextUtils.isEmpty(ccodeViet)) {
////            if (Objects.requireNonNull(ccodeViet).equalsIgnoreCase("+84")) {
////                LocalBroadcastManager.getInstance(BaseActivity.this).unregisterReceiver(mRegistrationBroadcastReceiver);
////            }
////        }
//    }

//    protected void pushNotifFirebase(final Activity activity) {
//
////        String ccodeViet = PreferenceUtil.getPref(BaseActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
////        if (!TextUtils.isEmpty(ccodeViet)) {
////            if (Objects.requireNonNull(ccodeViet).equalsIgnoreCase("+84")) {
////                mRegistrationBroadcastReceiver = new BroadcastReceiver() {
////                    @Override
////                    public void onReceive(Context context, Intent intent) {
////                        if (!activity.isFinishing()) {
////                            if (intent.getAction().equals(ConfigPref.REGISTRATION_COMPLETE)) {
////                                ContactItem owns = SessionManager.getProfile(activity);
////                                if (owns != null) {
////                                    getDataTopic(owns.msisdn);
////                                }
////                            } else if (intent.getAction().equals(ConfigPref.PUSH_NOTIFICATION)) {
////                                String title = intent.getStringExtra("title");
////                                String message = intent.getStringExtra("message");
////                                String direction = intent.getStringExtra("direction");
////                                popUpSuccessBuy(title, message, direction);
////                                counterDataInbox();
////                                Intent i = new Intent("com.sbimv.mvicalls.UPDATE_INBOX");
////                                LocalBroadcastManager.getInstance(context).sendBroadcast(i);
////                            }
////
////                        }
////                    }
////                };
////            }
////        }
//
//    }

    //New Broadcast
    protected void dialogUpSuccessBuy(String title, String message, final String direction) throws Exception {
        if (!BaseActivity.this.isFinishing()) {

            new AwesomeSuccessDialog(BaseActivity.this)
                    .setTitle(title)
                    .setMessage(message)
                    .setColoredCircle(R.color.dialogSuccessBackgroundColor)
                    .setDialogIconAndColor(R.mipmap.ic_check_white, R.color.white)
                    .setCancelable(false)
                    .setPositiveButtonText("OK")
                    .setPositiveButtonbackgroundColor(R.color.dialogSuccessBackgroundColor)
                    .setPositiveButtonTextColor(R.color.white)
                    .setPositiveButtonClick(new Closure() {
                        @Override
                        public void exec() {
                            if (!TextUtils.isEmpty(direction) && direction.equalsIgnoreCase("purchase")) {
                                Intent intent = new Intent(BaseActivity.this, UploadVideoToneActivity.class);
                                startActivity(intent);
                            }
                        }
                    }).show();


        }
    }

    protected void getDataTopic(String msisdn) {
        Call<APIResponse<List<ModelTopic>>> call = ServicesFactory.getService().getTopic(msisdn);
        call.enqueue(new Callback<APIResponse<List<ModelTopic>>>() {
            @Override
            public void onResponse(Call<APIResponse<List<ModelTopic>>> call, Response<APIResponse<List<ModelTopic>>> response) {
                if (response.body() != null && response.isSuccessful() & response.body().isSuccessful()) {
                    List<ModelTopic> data = response.body().data;
                    if (data != null && data.size() > 0) {

                        for (ModelTopic modelTopic : data) {
                            isActive = modelTopic.active;
                            if (isActive == true) {
                                String strTopic = modelTopic.topic;
                                if (!TextUtils.isEmpty(strTopic)) {
                                    FirebaseMessaging.getInstance().subscribeToTopic(strTopic);
                                }
                            }
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<APIResponse<List<ModelTopic>>> call, Throwable t) {
                Log.e("Topic On Failure", "Data Topic On Failure");
            }
        });

    }

    /**
     * Remote Config Update Version App...
     * Added by Yudha Pratama Putra, 12 Nov 2018
     */
    protected void showBroadcastConfig(Activity activity) {
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.UPDATE_CONFIG");
        LocalBroadcastManager.getInstance(activity).registerReceiver(broadcastReceiver, _if);
    }

    protected void stopBroadcastConfig(Activity activity) {
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(broadcastReceiver);
    }

//    protected void getBroadcastUpdateVersionLang(Activity activity) {
//        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.UPDATE_VERSION_lANG");
//        LocalBroadcastManager.getInstance(activity).registerReceiver(broadcastReceiver, _if);
//    }

//    protected void stopBroadcastUpdateVesrionLang(Activity activity) {
//        LocalBroadcastManager.getInstance(activity).unregisterReceiver(broadcastReceiver);
//    }

    protected void updateLang(Activity activity, SyncConfig.UpdateLangListener listener) {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (context != null && intent != null) {
                    if (!activity.isFinishing()) {

                        boolean mustUpdateLang = intent.getBooleanExtra("must_update_lang", false);
                        if (mustUpdateLang) {
                            String ccode = PreferenceUtil.getPref(activity).getString(PreferenceUtil.COUNTRY_CODE, "1");
                            String cNamecode = PreferenceUtil.getPref(activity).getString(PreferenceUtil.COUNTRY_NAME_CODE, "EN");
                            LangViewModel model = LangViewModel.getInstance();
                            model.getLangRemote(cNamecode, ccode, listener::onResponse);
                        }

                    }

                }

            }
        };
    }

    //New Broadcast
    protected BroadcastReceiver popupNotifAppears = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (context != null && intent != null){

                String strFrom = intent.getStringExtra("from");
                if (!TextUtils.isEmpty(strFrom)){

                    if (strFrom.equals("notif_logout")){
                        processAutoLogout();
                    }

                }else {

                    String ccodeViet = PreferenceUtil.getPref(BaseActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
                    if (!TextUtils.isEmpty(ccodeViet)) {
                        if (Objects.requireNonNull(ccodeViet).equalsIgnoreCase("+84")) {

                            String title = TextUtils.isEmpty(intent.getStringExtra("title"))?"":intent.getStringExtra("title");
                            String message = TextUtils.isEmpty(intent.getStringExtra("message"))?"":intent.getStringExtra("message");
                            String direction = TextUtils.isEmpty(intent.getStringExtra("direction"))?"":intent.getStringExtra("direction");

                            try {
                                dialogUpSuccessBuy(title, message, direction);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    }

//                    String title = TextUtils.isEmpty(intent.getStringExtra("title"))?"":intent.getStringExtra("title");
//                    String message = TextUtils.isEmpty(intent.getStringExtra("message"))?"":intent.getStringExtra("message");
//                    String direction = TextUtils.isEmpty(intent.getStringExtra("direction"))?"":intent.getStringExtra("direction");
//
//                    try {
//                        dialogUpSuccessBuy(title, message, direction);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }


                }


            }
        }
    };

//==========================================================================================================
//==========================================================================================================
//==========================================================================================================

//========================================                          ========================================
//======================================== ALL FUNCTIONS CHARGING   ========================================
//========================================                          ========================================

//    /**
//     * Get Charging Content
//     */
//    protected void doChargingContent(String price, String contentID, String callerID, String nope, View vSnack) {
//        String msisdn = nope;
//        String filter = msisdn.substring(0, 6);
//        String tsel, isat, XL;
//
//        // Filter kartu
//        boolean detected = false;
//
//        for (int t = 0; t < tselSim.length; t++) {
//            tsel = String.valueOf(Arrays.asList(tselSim).get(t));
//            if (filter.contains(tsel)) {
//                smsChargingContent(price, contentID);
//                detected = true;
//            }
//        }
//        for (int i = 0; i < isatSim.length; i++) {
//            isat = String.valueOf(Arrays.asList(isatSim).get(i));
//            if (filter.contains(isat)) {
//                callChargingContent();
//                detected = true;
//            }
//        }
//        for (int x=0 ; x<XLSim.length;x++){
//            XL = String.valueOf(Arrays.asList(XLSim).get(x));
//            if (filter.contains(XL)){
//                smsChargingContent(price, contentID);
//                detected = true;
//            }
//        }
//        if (!detected){
//            showSnackBar(vSnack, getString(R.string.not_telco_sim));
//        }
//        else {
//        }
//
//    }

    /**
     * Get Charging Content
     */
//    protected void getDtChargingContent(String msisdn, String content_id, String type) {
//        Call<APIResponse<ArrayList<DataChargingContent>>> call = ServicesFactory.getService().getChargingContent(msisdn, content_id, type);
//        call.enqueue(new Callback<APIResponse<ArrayList<DataChargingContent>>>() {
//            @Override
//            public void onResponse(Call<APIResponse<ArrayList<DataChargingContent>>> call, Response<APIResponse<ArrayList<DataChargingContent>>> response) {
//                if (response.isSuccessful()) {
//                    ArrayList<DataChargingContent> datas = response.body().data;
//                    if (datas != null && datas.size() > 0) {
//                        DataChargingContent dataChargingContent = datas.get(0);
//                        sendSMSChargingAndWaiting(BaseActivity.this, dataChargingContent.getSdc(), dataChargingContent.getKeyword());
//
//                        // google analytics...
//                        if (own != null) {
//                            GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_PREMIUM_CONTENT_PREVIEW, TrackerConstant.EVENT_CAT_PREMIUM_CONTENT_PREVIEW, "click buy", own.caller_id + "/" + dataChargingContent.getKeyword());
//                        }
//
//                    }
//                }
//            }
//
//            @Override
//            public void isFailure(Call<APIResponse<ArrayList<DataChargingContent>>> call, Throwable t) {
//                toast(getString(R.string.error_fetching_data));
//            }
//        });
//    }

    protected void configDynamic(final Activity activity) {
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (!activity.isFinishing()) {
                    checkUpdateApp(activity);
                }
            }
        };
    }

    /**
     * Web charging
     */
//    private void webChargingContent(String callerID, String contentID, String price) {
//        Call<DataWebCharging> call = ServicesFactory.getService().getWebCharging(callerID, contentID, price);
//        call.enqueue(new Callback<DataWebCharging>() {
//            @Override
//            public void onResponse(Call<DataWebCharging> call, Response<DataWebCharging> response) {
//                if (response.isSuccessful()) {
//                    String url = response.body().getUrl();
//                    Intent intent = new Intent(getApplicationContext(), WebChargingContentActivity.class);
//                    intent.putExtra("url", url);
//                    startActivity(intent);
//                }
//            }
//
//            @Override
//            public void isFailure(Call<DataWebCharging> call, Throwable t) {
//                toast("Failed, Check Your Connection!");
//            }
//        });
//    }

    /**
     * Subs Charging
     */
//    protected void subsCharging(String noMsisdn,String smsCode,View viewCat) {
//        String msisdn = noMsisdn;
//        String filter = msisdn.substring(0, 6);
//        String tsel;
//        boolean detectForSubs = false;
//
////      Filter kartu
//        for (int i = 0; i < tselSim.length; i++) {
//            tsel = String.valueOf(Arrays.asList(tselSim).get(i));
//            if (filter.contains(tsel)) {
//                subsAutoCompose(smsCode);
//                detectForSubs = true;
//            }
//        }
//        for (int i = 0; i < XLSim.length; i++) {
//            tsel = String.valueOf(Arrays.asList(XLSim).get(i));
//            if (filter.contains(tsel)) {
//                subsAutoCompose(smsCode);
////                reqWebCharging(smsCode,msisdn);
//                detectForSubs = true;
//            }
//        }
//        if (!detectForSubs){
//            showSnackBar(viewCat, getString(R.string.not_telco_sim));
//        }
//    }

    /**
     * Req Web Charging
     */
//    private void reqWebCharging(String smsCode,String msisdn) {
//        Call<DataSubscription> call = ServicesFactory.getService().getSubscription(smsCode, msisdn);
//        call.enqueue(new Callback<DataSubscription>() {
//            @Override
//            public void onResponse(Call<DataSubscription> call, Response<DataSubscription> response) {
//                if (response.isSuccessful()) {
//                    String url = response.body().getUrl();
//                    Intent intent = new Intent(getApplicationContext(), webChargingSubscribeActivity.class);
//                    intent.putExtra("urlCharging", url);
//                    startActivity(intent);
//                }
//            }
//
//            @Override
//            public void isFailure(Call<DataSubscription> call, Throwable t) {
//
//            }
//        });
//    }

    protected void checkUpdateApp(Activity activity) {
//        ConfigData cdt = SessionManager.getConfigData(activity);
//        if (cdt != null) {
//            String appVersionUpdate = cdt.version_code;
//            if (!TextUtils.isEmpty(appVersionUpdate)) {
//                if (thisNumeric(appVersionUpdate)) {
//                    int updatedVersion = Integer.parseInt(appVersionUpdate);
//                    if (updatedVersion > BuildConfig.VERSION_CODE) {
//                        if (!activity.isFinishing()) {
//                            new AlertDialog.Builder(activity)
////                                    .setTitle(R.string.title_update_app)
//                                    .setTitle(model.getLang().string.titleUpdateApp)
//                                    .setMessage(model.getLang().string.msgUpdateApp)
//                                    .setCancelable(false).setNegativeButton(model.getLang().string.cancelConfirm, new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    dialog.dismiss();
//                                }
//                            }).setPositiveButton(model.getLang().string.approveCofirm, new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    openLinkDownload();
//                                    dialog.dismiss();
//                                }
//                            }).show();
//
//                        }
//                    } else {
//                        Log.d("Response", "checkUpdate: false");
//                    }
//                }
//            }
//        } else {
//            Intent i = new Intent(activity, SyncConfigService.class);
//            startService(i);
//        }
    }

    protected void openLinkDownload() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    /**
     * Counter Data Inbox
     */
    protected void counterDataInbox() {

//        String nope = "";
//        try {
//            ContactItem own = SessionManager.getProfile(this);
//            nope = own.msisdn;
//        } catch (NullPointerException e) {
//            e.printStackTrace();
//            if (e instanceof NullPointerException) {
//                return;
//            }
//        }

        ContactItem own = SessionManager.getProfile(this);
        if (own != null) {
            String nope = own.msisdn;
            Call<APIResponse<List<InboxModel>>> call = ServicesFactory.getService().inbox(nope, "", "");
            call.enqueue(new Callback<APIResponse<List<InboxModel>>>() {
                @Override
                public void onResponse(Call<APIResponse<List<InboxModel>>> call, Response<APIResponse<List<InboxModel>>> response) {
                    if (response.body() != null && response.isSuccessful() && response.body().isSuccessful()) {
                        List<InboxModel> data = response.body().data;
                        if (data == null) {
                            txtCount.setVisibility(View.GONE);
                            return;
                        }
                        iCounter = 0;
                        for (int i = 0; i < data.size(); i++) {
                            if (data.get(i).status.equals("1")) {
                                iCounter += 1;
                            }
                        }
                        if (iCounter > 0) {
                            String equalData = String.valueOf(iCounter);
                            txtCount.setVisibility(View.VISIBLE);
                            txtCount.setText(equalData);
                        } else {
                            txtCount.setVisibility(View.GONE);
                        }
                    } else {
                        txtCount.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<APIResponse<List<InboxModel>>> call, Throwable t) {
                    Log.i("errorListProfile", "List profile failed");
                }
            });
        }
    }

    protected void ucInfo() {
        Toast.makeText(this, "Available coming soon", Toast.LENGTH_SHORT).show();
    }

    protected void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void toastShort(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Default Toolbar
     */
    protected void setDefaultToolbar(boolean backButtonEnable) {
        setDefaultToolbar(backButtonEnable, null);
    }

//============================================================================================================
//============================================================================================================
//============================================================================================================

//========================================                            ========================================
//========================================    ALL FUNCTIONS SETTING   ========================================
//========================================                            ========================================

    @SuppressLint("CheckResult")
    protected void setDefaultToolbar(boolean backButtonEnable, String title) {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        View backButton = findViewById(R.id.btn_actionbar_back);
        ImageView ivLogo = findViewById(R.id.iv_logo);
        TextView tvTitle = findViewById(R.id.tv_title_menu);

        txtCount = findViewById(R.id.txtCount);
        toInbox = findViewById(R.id.rl_inbox);

//        Glide.with(BaseActivity.this)
//                .load(getResources().getIdentifier("logo_rebranding", "drawable", BaseActivity.this.getPackageName()))
//                .into(ivLogo);

        Glide.with(BaseActivity.this).asBitmap().load(getResources().getIdentifier("logo_rebranding", "drawable", BaseActivity.this.getPackageName()))
                .into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                final int height = resource.getHeight();
                final int width = resource.getWidth();
                ivLogo.post(() -> {
                    int imgW = ivLogo.getMeasuredWidth();
                    int imgH = imgW * height / width;
                    ivLogo.getLayoutParams().height = imgH;
                    ivLogo.requestLayout();
                    ivLogo.setImageBitmap(resource);
                });
            }
        });


        if (TextUtils.isEmpty(title)) {
            tvTitle.setText("");
            tvTitle.setVisibility(View.GONE);
            ivLogo.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            ivLogo.setVisibility(View.GONE);
        }

        if (backButtonEnable) {
            toInbox.setVisibility(View.GONE);
            backButton.setVisibility(View.VISIBLE);
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    boolean isEneble = AppPreference.getPreferenceBoolean(BaseActivity.this,AppPreference.ENABLE_BACK);
                    if (mBackButtonEnable) {
//                        if (!isEneble){
                        finish();
//                        }else {
//                            Toast.makeText(BaseActivity.this, getString(R.string.wording_enable_back), Toast.LENGTH_SHORT).show();
//                        }
                    }
                    contentBuys = false;
                }
            });
        } else {
            backButton.setVisibility(View.GONE);
            toInbox.setVisibility(View.VISIBLE);
            backButton.setOnClickListener(null);
        }

        toInbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), InboxActivity.class);
                startActivity(i);
            }
        });

    }

    /**
     * Default Snackbar
     */
    protected void showSnackBar(View v, String message) {
        if (isViewAttached == false) return;
        Snackbar sb = Snackbar.make(v, message, Snackbar.LENGTH_SHORT);
        View vb = sb.getView();
        vb.setBackgroundColor(getResources().getColor(R.color.grey_868686));
        sb.show();
    }

    /**
     * Default Menu Drawable
     */
    protected void setDefaultDrawable() {
        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.menu);
        toolbar.setOverflowIcon(drawable);
    }

    /**
     * PopUp Firebase Push Notification
     * Added by Asykur, 05 Sep 2018
     * Update Yudha Pratama Putra, 05 sep 2018
     */
    protected void popUpSuccessBuy(String title, String message, final String direction) {
        new AwesomeSuccessDialog(BaseActivity.this)
                .setTitle(title)
                .setMessage(message)
                .setColoredCircle(R.color.dialogSuccessBackgroundColor)
                .setDialogIconAndColor(R.mipmap.ic_check_white, R.color.white)
                .setCancelable(false)
                .setPositiveButtonText("OK")
                .setPositiveButtonbackgroundColor(R.color.dialogSuccessBackgroundColor)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
            @Override
            public void exec() {
                if (!TextUtils.isEmpty(direction) && direction.equalsIgnoreCase("purchase")) {
                    Intent intent = new Intent(BaseActivity.this, UploadVideoToneActivity.class);
                    startActivity(intent);
                }
            }
        }).show();
    }

//================================================================================================================
//================================================================================================================
//================================================================================================================
//========================================                                ========================================
//========================================    ALL FUNCTIONS SET DEFAULT   ========================================
//========================================                                ========================================

    /**
     * Call charging
     */
    protected void callChargingContent() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode("*123*590*1#")));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

//==========================================================================================================
//==========================================================================================================
//==========================================================================================================
//========================================                            ========================================
//====================================   ALL STATIC METHOD (Utilities)   =====================================
//========================================                            ========================================

    /**
     * isConfigDataNotNull
     */
    protected boolean isConfigDataNotNull(ConfigData cdt) {
        return cdt != null;
    }

    protected void checkSubs(final String msisdn, final CheckSubscribe check) {

        // Then, inside onClick method of Button
        long currentClickTime = System.currentTimeMillis();

        if (currentClickTime - LAST_CLICK_TIME <= mDoubleClickInterval) {
            Log.i("detected", "Double click detected");
        } else {
            LAST_CLICK_TIME = System.currentTimeMillis();

            // FREE FOR PERU
            String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE,"+62");
            if (ccode.equals("+51")){
                check.success(true);
                return;
            }

            getSubscriptionStatus(BaseActivity.this, msisdn, new SubscriptionStatusListener() {
                @Override
                public void onComplete(DataSubs subsData) {
                    boolean isActive = isFreemiumActive(subsData) || isSubscribeActive(subsData);
                    check.success(isActive);
                }

                @Override
                public void onFail() {
                    String failure = TextUtils.isEmpty(model.getLang().string.str_toast_data_failure)
                            ? "data failure or check your connection..."
                            : model.getLang().string.str_toast_data_failure;
                    toast(failure);
                }
            });
        }
    }

    /**
     * Fungsi Share App
     */
    protected void shareSocialMedia() {
        String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(getApplicationContext());
        List<String> PackageName = new ArrayList<>();
        PackageName.add("com.facebook.katana");
        PackageName.add("com.twitter.android");
        PackageName.add("com.whatsapp");
        PackageName.add("com.google.android.gm");
        PackageName.add("com.instagram.android");
        PackageName.add("jp.naver.line.android");
        PackageName.add(defaultSmsPackageName);

        List<Intent> targetedShareIntents = new ArrayList<>();
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        List<ResolveInfo> resInfo = getApplicationContext().getPackageManager().queryIntentActivities(share, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                String shareBody = model.getLang().string.letsInstallMvicall + model.getLang().string.linkShareaApp;
                Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);
                if (PackageName.contains(info.activityInfo.packageName.toLowerCase())) {
                    targetedShare.setType("text/plain");
                    targetedShare.putExtra(Intent.EXTRA_TEXT, shareBody);
                    targetedShare.setPackage(info.activityInfo.packageName.toLowerCase());
                    targetedShareIntents.add(targetedShare);
                }
            }
            if (targetedShareIntents.size() > 0) {
                Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "MViCall Share");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[targetedShareIntents.size()]));
                startActivity(chooserIntent);
            } else {
                Toast.makeText(getApplicationContext(), "Installed application first", Toast.LENGTH_LONG).show();
            }

        }
    }

    /**
     * Check Permission
     */
    protected void checkPermissionAllow() {
        Dexter.withActivity(this).withPermissions(permissions).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    Log.i("allow all permission", "All permissions are granted");
                } else if (!report.getDeniedPermissionResponses().isEmpty()) {
                    showSettingsDialog();
                }
            }


            @Override
            public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                Log.i("error persion", String.valueOf(error));
            }
        }).onSameThread().check();
    }

    protected void showInfoDialog(){
        if(!BaseActivity.this.isFinishing()) {

            String strTitle = TextUtils.isEmpty(model.getLang().string.titleDialogUsageConnection)?"":model.getLang().string.titleDialogUsageConnection;
            String strApprove = TextUtils.isEmpty(model.getLang().string.wordingAllApprove)?"":model.getLang().string.wordingAllApprove;
            String strMsg = TextUtils.isEmpty(model.getLang().string.msgDialog)?"Check your internet connection and try again":model.getLang().string.msgDialog;

            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle(strTitle)
                    .setMessage(strMsg)
                    .setPositiveButton(strApprove, null)
                    .setCancelable(true).create();

            dialog.setOnShowListener(dialog1 -> {
                Button buttonPositive = ((AlertDialog) dialog1).getButton(AlertDialog.BUTTON_POSITIVE);
                buttonPositive.setOnClickListener(v -> {
                    if(!BaseActivity.this.isFinishing()) {
                        dialog1.dismiss();
                    }
                });
            });
            dialog.show();
        }
    }

    protected void showSettingsDialog() {

        if (!BaseActivity.this.isFinishing()) {
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle(model.getLang().string.titlePermissionAlert)
                    .setMessage(model.getLang().string.bodyPermissionAlert)
                    .setPositiveButton(model.getLang().string.buttonPermissionAlert, null) //Set to null. We override the onclick
                    .setCancelable(false).create();

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button buttonPositive = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                    buttonPositive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            openSettings();
                        }
                    });

                }
            });
            dialog.show();
        }

    }

    protected void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Get Default Status Profile
     */
    protected void sendRegID(String... arg) {
        Call<APIResponse<String>> call = ServicesFactory.getService().updateRegId(arg[0], arg[1]);
        call.enqueue(new Callback<APIResponse<String>>() {
            @Override
            public void onResponse(Call<APIResponse<String>> call, Response<APIResponse<String>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    Log.i("wordUpdateRegId", "Update regID success");
                } else {
                    Log.i("wordUpdateRegId", "Update regID failed");
                }
            }

            @Override
            public void onFailure(Call<APIResponse<String>> call, Throwable t) {
                Log.i("onfailure_send_regid", "Update regID failed");
            }
        });
    }

    /**
     * Dialog Setting SIM Card
     * Added by Yudha Pratama Putra, 04 Sep 2018
     */
    protected void showDualSIMDialog(final Activity activity, final String destination, final String smsContent, final ChargingListener listener) {

        if (!(activity).isFinishing()) {

            final AlertDialog dialog = new AlertDialog.Builder(activity)
                    .setTitle(model.getLang().string.titleDialogCekSimcard)
                    .setMessage(model.getLang().string.bodyDialogCekSimcard)
                    .setNegativeButton("SIM 1", null)
                    .setPositiveButton("SIM 2", null) //Set to null. We override the onclick
                    .setCancelable(true).create();

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    // Click SIM 1
                    Button buttonNegative = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE);
                    buttonNegative.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
                                if (subsInfoList != null) {
                                    int subsID = subsInfoList.get(0).getSubscriptionId();
                                    waitingProses(activity, destination, smsContent, listener, subsID);
                                }
                            }

                        }
                    });
                    // Click SIM 2
                    Button buttonPositive = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                    buttonPositive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
                                if (subsInfoList != null) {
                                    int subsID = subsInfoList.get(1).getSubscriptionId();
                                    waitingProses(activity, destination, smsContent, listener, subsID);
                                }
                            }
                        }
                    });
                }
            });
            dialog.show();
        }
    }

    protected boolean checkNetwork(final View snackView) {

        final Snackbar snackbar = Snackbar.make(snackView, model.getLang().string.noConnection, Snackbar.LENGTH_INDEFINITE).setActionTextColor(getResources().getColor(R.color.white));
        View snackbarLayout = snackbar.getView();
        snackbarLayout.setBackgroundColor(getResources().getColor(R.color.orange_gmail));
        TextView textView = snackbarLayout.findViewById(R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setCompoundDrawablePadding(getResources().getDimensionPixelOffset(R.dimen.fab_margin));

        String strMltClose = TextUtils.isEmpty(model.getLang().string.close)?"":model.getLang().string.close;

        snackbar.setAction(strMltClose, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                snackbar.dismiss();
                return false;
            }
            return false;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isViewAttached) // skip updating view
                    return;
                snackbar.show();
            }
        }, 1000);

        return false;
    }

    /**
     * Buy content FREE
     * Added by Hendi Hidayat, 10 Sep 2018
     */
    protected void buyContentFree(String content_id, Button btnSubscribe) {
        try{
            ContactItem profile = SessionManager.getProfile(this);
//        String title = getResources().getString(R.string.title_delay_proses);
//        String message = getResources().getString(R.string.body_delay_proses);
//        showProgressDialogs(title,message);
            if(profile != null) {
                Call<APIResponse> call = ServicesFactory.getService().buyContentFree(profile.msisdn, content_id);
                call.enqueue(new Callback<APIResponse>() {
                    @Override
                    public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                        dissmisProgressDialogs();
                        if (response.isSuccessful() && response.body().isSuccessful()) {
                            toast(model.getLang().string.successGetFreeContent);
                            setButton(btnSubscribe);
//                    if (!isFinishing()){
//                        showSuccessDialog();
//                    }
                        } else {
                            assert response.body() != null;
                            if (response.body().code.equals("300")){
                                //toast(model.getLang().string.failedGetFreeContent);
                                String sError = TextUtils.isEmpty(model.getLang().string.str_have_content)
                                        ?"You are still subscribed to this content, see at my collections":model.getLang().string.str_have_content;
                                toast(sError);
                                setButton(btnSubscribe);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<APIResponse> call, Throwable t) {
                        dissmisProgressDialogs();
                        toast(model.getLang().string.wordingFailureResponse);
                    }
                });
            }
        }catch (Exception e){

        }
    }

    private void setButton(Button button){
        String titleSee = TextUtils.isEmpty(model.getLang().string.seeCollection)
                ? "See On Your Collection"
                : model.getLang().string.seeCollection;
        button.setText(titleSee);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(BaseActivity.this, UploadVideoToneActivity.class));
            }
        });
    }

    private void showSuccessDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Update Video Tone Success");
        builder.setCancelable(false);
        builder.setMessage("Your video tone successfully updated, tap MY COLLECTION to see you collection.");
        builder.setPositiveButton("MY COLLECTION", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), UploadVideoToneActivity.class);
                startActivity(intent);
            }
        });
        builder.setNegativeButton("BACK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    //Vietnam
    public static void showCaseIntroVietEleven(Activity activity) {
        try {
            PreferenceUtil.getEditor(activity).putBoolean(PreferenceUtil.CASE_ELEVEN_VIETNAME, true).commit();
            String strWordingTitleVietEleven = TextUtils.isEmpty(model.getLang().string.caseTitleUploadVideo)
                    ?"What a talent!" :
                    model.getLang().string.caseTitleUploadVideo;

            String strWordingSubtitleVietEleven = TextUtils.isEmpty(model.getLang().string.caseSubtitleUploadVideo)
                    ?"Each personal video must be approved before being used as a ringtone. I will confirm to you when our team approved your video." :
                    model.getLang().string.caseSubtitleUploadVideo;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(activity);
            showCaseWrapper.showFancyVietnameEleven(
                    ShowCaseWrapper.SC_VIET_ELEVEN,
                    strWordingTitleVietEleven,
                    strWordingSubtitleVietEleven,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            showController(activity);

//                            try {
//                                showAgreementDialog(activity, title, msg, btnPositive, btnNegative);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
                            //activity.startActivity(new Intent(activity, FriendsDemoActivity.class).putExtra("ALMOST", "true"));
                        }

                        @Override
                        public void onOk() {
                            showController(activity);
                            //activity.startActivity(new Intent(activity, FriendsDemoActivity.class).putExtra("ALMOST", "true"));
                        }

                        @Override
                        public void onSkip() {
                            showController(activity);
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void showController(Activity activity){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mediaController.setVisibility(View.VISIBLE);
            }
        }, 2500);
    }

//==========================================================================================================
//==========================================================================================================
//==========================================================================================================

    private void showProgressDialogs(String... arg) {
        if (!isViewAttached) {
            return;
        } else {
            try {
                pd = ProgressDialog.show(this, arg[0], arg[1], true, false);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    private void dissmisProgressDialogs() {
        if (!isViewAttached) {
            return;
        } else {
            try {
                pd.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    protected AlertDialog DialogTurnOffWifi(final String price) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        alertBuilder.setMessage(model.getLang().string.wordingChangeWifiState);
        alertBuilder.setPositiveButton(model.getLang().string.turnOffWifi, null);
        AlertDialog dialog1 = alertBuilder.create();
        dialog1.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button buttonPositive = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                buttonPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Intent intent = new Intent("turn_off_wifi");
                        intent.putExtra("price", price);
                        intent.putExtra("isData", Boolean.parseBoolean("true"));
                        getApplicationContext().sendBroadcast(intent);
                    }
                });
            }
        });
        return dialog1;
    }

    protected void showCustomProgressDialog (String sMesage) throws Exception {

        if (progressD != null) {
            progressD.setMessage(sMesage);
            progressD.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressD.setCancelable(false);
            progressD.show();
        }

    }

    protected void dismissProgressDialog() throws Exception{
        if (progressD != null) {
            progressD.dismiss();
        }
    }


    /**
     * Check Subscribe
     */
    public interface CheckSubscribe {
        void success(boolean isSuccess);
    }

    public interface SubscriptionStatusListener {
        void onComplete(DataSubs subsData);

        void onFail();
    }

    public interface ChargingListener {
        void onDelayFinished();
    }

    public void setFinalScase(){
        PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.FINAL_SCASE, true).commit();
        Intent i = new Intent(getApplicationContext(), SyncConfigService.class);
        startService(i);
    }

    public void showCaseToFalse() {
        PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.SHOW_CASE_DEMO_PROFILE, false).commit();
        AppPreference.setPreferenceBoolean(getApplicationContext(), AppPreference.FIRST_SHOW_CASE, false);
        AppPreference.setPreferenceBoolean(getApplicationContext(), AppPreference.SHOW_CASE_TIMELINE, false);
        AppPreference.setPreferenceBoolean(getApplicationContext(), AppPreference.SHOW_CASE_PROFILE, false);
        AppPreference.setPreferenceBoolean(getApplicationContext(), AppPreference.SHOW_CASE_VIDEOS, false);
        AppPreference.setPreferenceBoolean(getApplicationContext(), AppPreference.SHOW_CASE_FRIENDS, false);
        PreferenceUtil.getEditor(this).putBoolean(PreferenceUtil.SETUP_PROFILE, true).commit();
        PreferenceUtil.getEditor(this).putBoolean(PreferenceUtil.SETUP_CONTACT, true).commit();
        PreferenceUtil.getEditor(this).putBoolean(PreferenceUtil.SETUP_SHARE, true).commit();
        PreferenceUtil.getEditor(this).putBoolean(PreferenceUtil.SETUP_VIDEOTONE, true).commit();
    }

    public void setKeyboard(View mParentLayout){
        try{
            mParentLayout.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
                Rect r = new Rect();

                mParentLayout.getWindowVisibleDisplayFrame(r);

                int heightDiff = mParentLayout.getRootView().getHeight() - (r.bottom - r.top);
                if (heightDiff > 100) {
                    if (mIsPopupVisible) {
                        keepKeyboard();
                        mIsPopupVisible = false;
                        mPopupWindow.dismiss();
                    }
                }

            });

            checkKeyboardIsOpen(mParentLayout, mParentLayout);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void keepKeyboard() {
       try{
           this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
       }catch (Exception e){
           e.printStackTrace();
       }
    }


    public void checkKeyboardIsOpen(final View contentView, View parentView) {
        try{
            parentView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {

                Rect r = new Rect();
                contentView.getWindowVisibleDisplayFrame(r);

            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}

