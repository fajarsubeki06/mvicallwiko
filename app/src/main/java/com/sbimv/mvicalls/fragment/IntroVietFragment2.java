package com.sbimv.mvicalls.fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.sbimv.mvicalls.R;

import java.util.Objects;

import agency.tango.materialintroscreen.SlideFragment;

public class IntroVietFragment2 extends SlideFragment {

    public IntroVietFragment2() {
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_intro_viet_2, container, false);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView imageBackground = view.findViewById(R.id.imageBackground);
        ImageView imageView = view.findViewById(R.id.imageView8);
        imageView.bringToFront();

        Glide.with(this)
                .load(getResources().getIdentifier("bg_gradient_two","drawable",getContext().getPackageName()))
                .into(imageBackground);

        setGlide("vietname2", imageView);
    }

    private void setGlide(String img_name, ImageView imageView){
        RequestOptions defBanner = new RequestOptions();
        Glide.with(Objects.requireNonNull(getActivity())).asBitmap().
                load(getResources().getIdentifier(img_name,"drawable", Objects.requireNonNull(getContext()).getPackageName())).
                apply(defBanner).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(final Bitmap bitmap, Transition<? super Bitmap> transition) {
                final int w = bitmap.getWidth();
                final int h = bitmap.getHeight();
                imageView.post(new Runnable() {
                    @Override
                    public void run() {
                        int imgW = imageView.getMeasuredWidth();
                        int imgH = imgW*h/w;
                        imageView.getLayoutParams().height = imgH;
                        imageView.requestLayout();
                        imageView.setImageBitmap(bitmap);
                    }
                });
            }
        });
    }

    @Override
    public int backgroundColor() {
        return R.color.abu_abu;
    }

    @Override
    public int buttonsColor() {
        return R.color.dark_grey_trans;
    }
}
