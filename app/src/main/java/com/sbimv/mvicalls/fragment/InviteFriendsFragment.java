package com.sbimv.mvicalls.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Telephony;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.activity.ContactTabActivity;
import com.sbimv.mvicalls.adapter.CustomGridViewAdapter;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataBannerPromo;
import com.sbimv.mvicalls.pojo.DataDynamicLink;
import com.sbimv.mvicalls.pojo.ModelCustomGrid;
import com.sbimv.mvicalls.services.SyncConfigService;
import com.sbimv.mvicalls.util.GridViewWithHeaderAndFooter;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.view.DialogUpload;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class InviteFriendsFragment extends Fragment {

    ModelCustomGrid[] sosmed = {
            new ModelCustomGrid("SMS", "sms", R.drawable.ics_sms, "share_sms"),
            new ModelCustomGrid("Email", "mailto", R.drawable.ics_email, "share_email"),
            new ModelCustomGrid("WhatsApp", "com.whatsapp", R.drawable.ics_whatsapp, "share_wa"),
            new ModelCustomGrid("Facebook", "com.facebook.katana", R.drawable.ics_facebook, "share_fb"),
            new ModelCustomGrid("Instagram", "com.instagram.android", R.drawable.ics_instagram, "share_ig"),
            new ModelCustomGrid("Line", "jp.naver.line.android", R.drawable.ics_line, "share_line"),
            new ModelCustomGrid("Viber", "com.viber.voip", R.drawable.ics_viber, "share_vb"),
            new ModelCustomGrid("WeChat", "com.tencent.mm", R.drawable.ics_wechat, "share_wc"),
            new ModelCustomGrid("Zalo", "com.zing.zalo", R.drawable.zalo, "share_zl"),
            new ModelCustomGrid("Telegram", "org.telegram.messenger", R.drawable.ics_telegram, "share_tl")};

    ArrayList<ModelCustomGrid> modelCustomGrids = new ArrayList<>();
//    private boolean isDoInvite;
//    private boolean doAction = false;
    private LangViewModel model;
    private GridViewWithHeaderAndFooter androidGridView;
    private ContactItem own;
    private String wordingDynamic;
    private DataBannerPromo datas;
    private ViewPager viewPager;

    /**
     * Remote Config Update Version App...
     * Added by Yudha Pratama Putra, 12 Nov 2018
     */
    private BroadcastReceiver showConfigUpdate = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkWording();
        }
    };
    private DialogUpload dialogUpload;
    private ImageView imgs;

    public InviteFriendsFragment() {
        // Required empty public constructor
    }

    private void getBannerPromo() {
        Call<APIResponse<DataBannerPromo>> call = ServicesFactory.getService().getBanner();
        call.enqueue(new Callback<APIResponse<DataBannerPromo>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<DataBannerPromo>> call, @NotNull Response<APIResponse<DataBannerPromo>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    datas = response.body().data;
                    if (datas != null) {
                        if (isAdded()) {
                            try {
                                Glide.with(Objects.requireNonNull(getActivity())).load(datas.getBanner()).into(imgs);
                                imgs.setVisibility(View.VISIBLE);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<DataBannerPromo>> call, @NotNull Throwable t) {
//                if (!((ContactTabActivity) getActivity()).isFinishing()) {
//                    Toast.makeText(getActivity(), model.getLang().string.wordingFailureResponse, Toast.LENGTH_SHORT).show();
//                }
            }
        });
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invite_friends, container, false);
        androidGridView = view.findViewById(R.id.grid_view_image_text);
        model = LangViewModel.getInstance();
        viewPager = view.findViewById(R.id.vp_contact);

//        isDoInvite = AppPreference.getPreferenceBoolean(Objects.requireNonNull(getActivity()), AppPreference.ISDOINVITE);
        own = SessionManager.getProfile(getActivity());
        for (ModelCustomGrid aSosmed : sosmed) {
            String name = aSosmed.name;
            String namePackage = aSosmed.package_name;
            int imgCount = aSosmed.icon;
            String source = aSosmed.source;

            if (namePackage.equalsIgnoreCase("sms")) {
                modelCustomGrids.add(new ModelCustomGrid(name, namePackage, imgCount, source));
            } else if (namePackage.equalsIgnoreCase("mailto")) {
                modelCustomGrids.add(new ModelCustomGrid(name, namePackage, imgCount, source));
            } else {
                validateIntent(name, namePackage, imgCount, source);
            }
        }


        androidGridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int width = androidGridView.getMeasuredWidth();
                setGridView(width);
                androidGridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        return view;
    }

    private void setGridView(int footerSize) {
        LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
        View footerView = layoutInflater.inflate(R.layout.banner, null);
        footerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, footerSize));
        imgs = footerView.findViewById(R.id.imgBanners);
        androidGridView.addFooterView(footerView);

        imgs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // action webView
                if (!TextUtils.isEmpty(datas.getUrl())) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(datas.getUrl()));
                    startActivity(browserIntent);
                }
                // action to mvicall page
                else if (!TextUtils.isEmpty(datas.getSource_content())) {
                    String link = datas.getSource_content();

                    String appLink = link
                            .replace("http", "app")
                            .replace("https", "app");
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appLink)));

                }
            }
        });

        /**
         * Add data model to adapter...
         * Added by Yudha Pratama Putra, 14 Des 2018
         */
        CustomGridViewAdapter adapterViewAndroid = new CustomGridViewAdapter(getActivity(), modelCustomGrids);
        androidGridView.setAdapter(adapterViewAndroid);

        androidGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                doAction = true;
                try {
                    final String name = modelCustomGrids.get(position).name;
                    final String packageName = modelCustomGrids.get(position).package_name;
                    String source = modelCustomGrids.get(position).source;
                    getDynamicLink(source, own.caller_id, name, packageName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        getBannerPromo();
    }

    private void doIntent(String packageName, String link, String name) {
        if (packageName.equals("mailto")) {
            sendShareEmailAction(link);
            sendGoogleAnalitycs(name);

        } else if (packageName.equals("sms")) {
            String smsAction = Telephony.Sms.getDefaultSmsPackage(getActivity());
            if (!TextUtils.isEmpty(smsAction)) {
                sendActionShare(smsAction, link);
                sendGoogleAnalitycs(name);
            }
        } else {
            sendActionShare(packageName, link);
            sendGoogleAnalitycs(name);
        }
    }

    private void getDynamicLink(String source, String caller_id, final String name, final String packageName) {
        showCustomDialog(model.getLang().string.progressWording);
        Call<APIResponse<DataDynamicLink>> call = ServicesFactory.getService().getDynamicShare(source, caller_id);
        call.enqueue(new Callback<APIResponse<DataDynamicLink>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<DataDynamicLink>> call, @NotNull Response<APIResponse<DataDynamicLink>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    DataDynamicLink data = response.body().data;
                    if (data != null) {
                        String link = data.getShortLink();
                        AppPreference.setPreference(getActivity(), name, link);
                        doIntent(packageName, link, name);
                    }
                }
                dismissDialogs();
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<DataDynamicLink>> call, @NotNull Throwable t) {
                dismissDialogs();
            }
        });
    }

    /**
     * Validate app sosmed exist...
     * Added by Yudha Pratama Putra, 14 Des 2018
     */
    private void validateIntent(String name, String namePackage, int imgCount, String source) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        List<ResolveInfo> resInfo = Objects.requireNonNull(getActivity()).getPackageManager().queryIntentActivities(share, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                if (namePackage.contains(info.activityInfo.packageName.toLowerCase())) {
                    modelCustomGrids.add(new ModelCustomGrid(name, namePackage, imgCount, source));
                    break;
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent i = new Intent(getActivity(), SyncConfigService.class);
        Objects.requireNonNull(getActivity()).startService(i);
        checkWording();

        /**
         * Remote Config Update Version App...
         * Added by Yudha Pratama Putra, 12 Nov 2018
         */
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.UPDATE_CONFIG");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(showConfigUpdate, _if);
    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        boolean isSkipped = PreferenceUtil.getPref(Objects.requireNonNull(getActivity())).getBoolean(PreferenceUtil.SKIP_INVITE, false);
//        if (isSkipped) {
//            return;
//        }
//        if (isDoInvite) {
//            if (doAction) {
//                AppPreference.setPreferenceBoolean(getActivity(), AppPreference.ISDOINVITE, false);
//                ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
//                showCaseWrapper.showFancyNext(ShowCaseWrapper.ID_ALLSET, new ShowCaseListener.NextListener() {
//                    @Override
//                    public void onNext() {
//                        Objects.requireNonNull(getActivity()).finish();
//                    }
//                });
//            }
//        }
//    }

    @Override
    public void onStop() {
        super.onStop();
        /**
         * Remote Config Update Version App...
         * Added by Yudha Pratama Putra, 12 Nov 2018
         */
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).unregisterReceiver(showConfigUpdate);
        dismissDialogs();
    }

    /**
     * Woarding Dynamic...
     * Added by Yudha Pratama Putra, 12 Nov 2018
     */
    private void checkWording() {
        try {
            ConfigData cdt = SessionManager.getConfigData(getActivity());
            if (cdt != null) {
                String wordShareIn = cdt.wording_share_id;
                String wordShareEn = cdt.wording_share_en;
                if (!TextUtils.isEmpty(wordShareIn) && !TextUtils.isEmpty(wordShareEn)) {
                    try {
                        if (((ContactTabActivity) getActivity()).curLang().equalsIgnoreCase("EN")) {
                            wordingDynamic = wordShareEn;
                        } else {
                            wordingDynamic = wordShareIn;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } else {
                Intent i = new Intent(getActivity(), SyncConfigService.class);
                Objects.requireNonNull(getActivity()).startService(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Send data to google analitycs...
     * Added by Yudha Pratama Putra, 14 Des 2018
     */
    private void sendGoogleAnalitycs(String action) {
        try {
            GATRacker.getInstance(Objects.requireNonNull(getActivity()).getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_CONTACT, TrackerConstant.EVENT_CAT_CONTACT, "click share", own.caller_id + "/" + action);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Share email...
     * Added by Yudha Pratama Putra, 14 Des 2018
     */
    private void sendShareEmailAction(String link) {
        try {
            if (!TextUtils.isEmpty(wordingDynamic)) {
                String filtered = wordingDynamic.replace("[LINK]", link);
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "MViCall");
                intent.putExtra(Intent.EXTRA_TEXT, filtered);
                startActivity(Intent.createChooser(intent, "Sending Email..."));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Share action...
     * Added by Yudha Pratama Putra, 14 Des 2018
     */
    private void sendActionShare(String pName, String link) {
        try {
            List<Intent> targetedShareIntents = new ArrayList<>();
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            List<ResolveInfo> resInfo = Objects.requireNonNull(getActivity()).getPackageManager().queryIntentActivities(share, 0);
            if (resInfo != null && resInfo.size() > 0) {
                for (ResolveInfo info : resInfo) {

                    if (!TextUtils.isEmpty(wordingDynamic)) { // validate wording dynamic null...
                        String shareBody = wordingDynamic.replace("[LINK]", link);
                        Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);

                        if (pName.contains(info.activityInfo.packageName.toLowerCase())) {
                            targetedShare.setType("text/plain");
                            targetedShare.putExtra(Intent.EXTRA_TEXT, shareBody);
                            targetedShare.setPackage(info.activityInfo.packageName.toLowerCase());
                            targetedShareIntents.add(targetedShare);
                        }
                    } else {
                        String firends = TextUtils.isEmpty(model.getLang().string.str_toast_friends_not_sync)
                                ? "Data has not been synced, please try again..."
                                : model.getLang().string.str_toast_friends_not_sync;
                        Toast.makeText(getActivity(), firends, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                if (targetedShareIntents.size() > 0) {
                    Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "MViCall Share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[targetedShareIntents.size()]));
                    startActivity(chooserIntent);
                } else {
                    String not = TextUtils.isEmpty(model.getLang().string.str_toast_friends_not_installed)
                            ? "App Not Installed"
                            : model.getLang().string.str_toast_friends_not_installed;
                    Toast.makeText(getActivity(),not, Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCustomDialog(String... arg) {
        try {
            dialogUpload = new DialogUpload();
            dialogUpload.setMessage(arg[0]);
            dialogUpload.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dismissDialogs() {
        try {
            dialogUpload.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}