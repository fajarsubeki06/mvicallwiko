package com.sbimv.mvicalls.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.BuildConfig;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.activity.PopUpProcedureMGM;
import com.sbimv.mvicalls.adapter.LeaderBoardAdapter;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataMGM;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemberGetMemberFragment extends Fragment implements View.OnClickListener {

//    public static String howtoWin = "howToWin";
//    public static String rewardsMGM = "rewardsMGM";
//    public static String winnerMGM = "winnerMGM";

    private Button btnHowtoWin, btnTnC, btnWinner;
//    private ContactItem own;
    private ImageView imgBanner;
    private RecyclerView rvLeaderBoard;
    private DataMGM datas;
    private List<DataMGM.MGMRank> datasList = new ArrayList<>();
    private DataMGM.MGMRank currentUser;
    private ProgressBar pgMGM;
    private LinearLayout linViewGroupMGM;
    private LinearLayout noConnectionState;
    private TextView tvNameRankFooter, tvPhoneRankFooter, tvNoteRankFooter, tvRankFooter;
    private RelativeLayout relaRankFooter;
    private CircleImageView imgRankFooter;
    private LangViewModel model;

    public MemberGetMemberFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_member_get_member, container, false);
        model = LangViewModel.getInstance();

        relaRankFooter = view.findViewById(R.id.RelaRankFooter);
        tvNameRankFooter = view.findViewById(R.id.nameRankFooter);
        tvPhoneRankFooter = view.findViewById(R.id.phoneRankFooter);
        tvNoteRankFooter = view.findViewById(R.id.noteRankFooter);
        tvRankFooter = view.findViewById(R.id.rankFooter);
        imgRankFooter = view.findViewById(R.id.imgRankFooter);

        btnHowtoWin = view.findViewById(R.id.btnHowtoWin);
        btnHowtoWin.setText(model.getLang().string.howToWin);

        btnTnC = view.findViewById(R.id.btnTnC);
        btnTnC.setText(model.getLang().string.tnc_mgm);

        btnWinner = view.findViewById(R.id.btnWinner);
        btnWinner.setText(model.getLang().string.rewardsnWinner);

        imgBanner = view.findViewById(R.id.imgBannerMGM);
        TextView tvLeaderBoard = view.findViewById(R.id.tvLeaderBoard);
        tvLeaderBoard.setText(model.getLang().string.leaderboard);
        rvLeaderBoard = view.findViewById(R.id.rvLeaderBoard);
        pgMGM = view.findViewById(R.id.pgMGM);
        linViewGroupMGM = view.findViewById(R.id.linvGroupMGM);
        noConnectionState = view.findViewById(R.id.layoutNoConnection);
        Button btnReConnect = view.findViewById(R.id.btnReConnect);
        btnReConnect.setOnClickListener(this);

        try {
            GATRacker.getInstance(Objects.requireNonNull(getActivity()).getApplication()).sendScreen(TrackerConstant.SCREEN_MGM);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logFcb_screen_pestarewardEvent();
        return view;
    }
    public void logFcb_screen_pestarewardEvent () {
        AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
        logger.logEvent("fcb_screen_pestareward");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            btnHowtoWin.setOnClickListener(this);
            btnTnC.setOnClickListener(this);
            btnWinner.setOnClickListener(this);
            initRecyCler();
            getLeaderBoard();
        }
    }

    private void initRecyCler() {
        if (isSafe()) {
            LinearLayoutManager manager = new LinearLayoutManager(getContext());
            manager.setOrientation(LinearLayoutManager.VERTICAL);
            rvLeaderBoard.setLayoutManager(manager);
            LeaderBoardAdapter adapter = new LeaderBoardAdapter(getContext(), datasList);
            rvLeaderBoard.setAdapter(adapter);
            rvLeaderBoard.getAdapter().notifyDataSetChanged();
        }
    }

    private void getLeaderBoard() {
        int versionCode = 0;
        ContactItem own = SessionManager.getProfile(getActivity());
        if (own != null) {

            String lang = PreferenceUtil.getPref(Objects.requireNonNull(getActivity())).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
            String strMsisdn = own.msisdn;
            versionCode = BuildConfig.VERSION_CODE;
            String appVersion = String.valueOf(versionCode);

            if (!TextUtils.isEmpty(lang) && !TextUtils.isEmpty(strMsisdn) && !TextUtils.isEmpty(appVersion)) {

                Call<APIResponse<DataMGM>> call = ServicesFactory.getService().getMGM(strMsisdn, lang, appVersion);
                call.enqueue(new Callback<APIResponse<DataMGM>>() {
                    @Override
                    public void onResponse(@NotNull Call<APIResponse<DataMGM>> call, @NotNull Response<APIResponse<DataMGM>> response) {
                        if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                            if (isSafe()) {
                                datas = response.body().data;
                                currentUser = response.body().data.getCurrent_user();
                                try {
                                    if (datas != null && datas.getList() != null && datas.getCurrent_user() != null && currentUser != null) {
                                        if (isSafe()) {
                                            datasList.clear();
                                            datasList.addAll(datas.getList());
                                            setDataRankFooter(own, currentUser);
                                            pgMGM.setVisibility(View.GONE);
                                            linViewGroupMGM.setVisibility(View.VISIBLE);

                                            String urlBanner = datas.getBanner();
                                            if (!TextUtils.isEmpty(urlBanner)) {
                                                loadImageBanner(urlBanner);
                                            }
                                        }
                                    } else {
                                        if (isSafe()) {
                                            pgMGM.setVisibility(View.GONE);
                                            String empty = TextUtils.isEmpty(model.getLang().string.str_pesta_reward_leader_empty)
                                                    ? "Data Leaderboard is empty"
                                                    : model.getLang().string.str_pesta_reward_leader_empty;
                                            Toast.makeText(getContext(), empty, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    rvLeaderBoard.getAdapter().notifyDataSetChanged();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else {
                                if (isSafe()) {
                                    try {
                                        pgMGM.setVisibility(View.GONE);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<APIResponse<DataMGM>> call, @NotNull Throwable t) {
                        if (isSafe()) {
                            try {
                                pgMGM.setVisibility(View.GONE);
                                noConnectionState.setVisibility(View.VISIBLE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            }
        }
    }

    @SuppressLint({"SetTextI18n", "CheckResult"})
    private void setDataRankFooter(ContactItem own, DataMGM.MGMRank currentUser) {
        if (isSafe()) {
            try {
                if (own != null) {
                    String urlPic = TextUtils.isEmpty(own.caller_pic)?"":own.caller_pic;
                    String strLangPoint = TextUtils.isEmpty(model.getLang().string.point)?"":model.getLang().string.point;
                    String strPoint = TextUtils.isEmpty(currentUser.getPoin())?"":currentUser.getPoin();
                    String strNote = TextUtils.isEmpty(currentUser.getNote())?"":currentUser.getNote();
                    String strNameRank = TextUtils.isEmpty(model.getLang().string.yourPointMGM)?"":model.getLang().string.yourPointMGM;
                    String name = TextUtils.isEmpty(currentUser.getName())?"":currentUser.getName();

                    String[] filtered = name.split(" ");
                    StringBuilder nameBuilder = new StringBuilder();

                    for (String val : filtered) {
                        if (val.length() >= 4) {
                            nameBuilder.append(val, 0, val.length() - 3).append("xxx ");
                        } else if (val.length() >= 3) {
                            nameBuilder.append(val, 0, val.length() - 2).append("xx ");
                        } else if (val.length() >= 2) {
                            nameBuilder.append(val, 0, val.length() - 1).append("x ");
                        }
                    }

                    if (isSafe()) {
                        relaRankFooter.setVisibility(View.VISIBLE);
                        tvNoteRankFooter.setText(strNote);
                        tvRankFooter.setText(strPoint + " " + strLangPoint);
                        tvNameRankFooter.setText(strNameRank);

                        RequestOptions requestOptions = new RequestOptions();
                        requestOptions.placeholder(R.drawable.userpng);
                        requestOptions.error(R.drawable.userpng);
                        Glide.with(imgRankFooter.getContext()).load(urlPic).apply(requestOptions).into(imgRankFooter);
                    }

                }

            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    private void loadImageBanner(String banner) {
        if (isSafe()) {
            try {
                Glide.with(imgBanner).asBitmap().load(banner).into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NotNull final Bitmap bitmap, Transition<? super Bitmap> transition) {
                        try {
                            if (isSafe()) {
                                final int w = bitmap.getWidth();
                                final int h = bitmap.getHeight();
                                imgBanner.post(() -> {
                                    int ivW = imgBanner.getMeasuredWidth();
                                    imgBanner.getLayoutParams().height = ivW * h / w;
                                    imgBanner.requestLayout();
                                    imgBanner.setImageBitmap(bitmap);
                                    imgBanner.setOnClickListener(v -> {
                                        if (isSafe()) {
                                            if (getActivity() != null) {
                                                try {
                                                    ViewPager vp = getActivity().findViewById(R.id.vp_contact); // Fetch ViewPager instance
                                                    vp.setCurrentItem(vp.getCurrentItem() - 1); // Increment ViewPager's position
                                                    ContactItem own = SessionManager.getProfile(getActivity());
                                                    if (own != null) {
                                                        try {
                                                            GATRacker.getInstance(getActivity().getApplication())
                                                                    .sendEventWithScreen(TrackerConstant.SCREEN_MGM,
                                                                            TrackerConstant.EVENT_CAT_BANNER_MGM,
                                                                            "click banner mgm", own.caller_id);
                                                        }catch (Exception e){
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });
                                });
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnTnC: // Syarat dan ketentuan... (TnC)
                if (datas != null){
                    String valuesReward = datas.getTnc();
                    if (!TextUtils.isEmpty(valuesReward)){
                        parsingDataToPopUp(valuesReward);
                        return;
                    }
                }
                showErrorReport();
                break;
            case R.id.btnHowtoWin: // Cara ikutan.... (Rule)
                if (datas != null){
                    String valuesHowToWin = datas.getRule();
                    if (!TextUtils.isEmpty(valuesHowToWin)){
                        parsingDataToPopUp(valuesHowToWin);
                        return;
                    }
                }
                showErrorReport();
                break;
            case R.id.btnWinner: // Reward dan pemenang... (Winner)
                if (datas != null){
                    String valuesWinner = datas.getWinner();
                    if (!TextUtils.isEmpty(valuesWinner)){
                        parsingDataToPopUp(valuesWinner);
                        return;
                    }
                }
                showErrorReport();
                break;
            case R.id.btnReConnect:
                noConnectionState.setVisibility(View.GONE);
                pgMGM.setVisibility(View.VISIBLE);
                new Handler().postDelayed(this::getLeaderBoard, 2000);
        }
    }

    private void parsingDataToPopUp(String value){
        startActivity(new Intent(getContext(), PopUpProcedureMGM.class).putExtra("urlWebview", value));
    }

    private void showErrorReport(){
        String errorReport = TextUtils.isEmpty(model.getLang().string.errorFetchingData)?"Error fetching data":model.getLang().string.errorFetchingData;
        Toast.makeText(getContext(), errorReport, Toast.LENGTH_SHORT).show();
    }

    private boolean isSafe() {
        return !(this.isRemoving() || this.getActivity() == null || this.isDetached() || !this.isAdded() || this.getView() == null);
    }

}
