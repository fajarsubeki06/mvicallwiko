package com.sbimv.mvicalls.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.util.PreferenceUtil;

import agency.tango.materialintroscreen.SlideFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class Intro9 extends SlideFragment {


    public Intro9() {
        // Required empty public constructor
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro9, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView imageBackground9 = view.findViewById(R.id.imageBackground9);
        ImageView imageView = view.findViewById(R.id.imageView);
        imageView.bringToFront();

        Glide.with(this)
                .load(getResources().getIdentifier("wl_intro_bg","drawable",getContext().getPackageName()))
                .into(imageBackground9);

        String res = getString(R.string.wl_intro_autostart);
        String check = (PreferenceUtil.getPref(getContext()).getString(PreferenceUtil.SAVED_LANG_KEY, "EN"));
        if (check.equalsIgnoreCase("ID")) {
            res = "new_improve_wl_auto_in";
        }else if (check.equalsIgnoreCase("TH")){
            res = "new_improve_wl_auto_th";
        } else{
            res = "new_improve_wl_auto_en";
        }



        Glide.with(this)
                .load(getResources().getIdentifier(res,"drawable",getContext().getPackageName()))
                .into(imageView);

        try {
            GATRacker.getInstance(getActivity().getApplication()).sendScreen(TrackerConstant.SCREEN_INTRO_PAGE_2);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int backgroundColor() {
        return R.color.abu_abu;
    }

    @Override
    public int buttonsColor() {
        return R.color.dark_grey_trans;
    }
}
