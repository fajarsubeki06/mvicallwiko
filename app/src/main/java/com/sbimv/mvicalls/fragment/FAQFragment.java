package com.sbimv.mvicalls.fragment;


import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sbimv.mvicalls.R;

import org.jetbrains.annotations.NotNull;

/**
 * A simple {@link Fragment} subclass.
 */
public class FAQFragment extends Fragment {

    private SwipeRefreshLayout swipeRefreshLayout;
    private WebView webView;
    String url;

    public FAQFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_faq,container,false);
        swipeRefreshLayout = view.findViewById(R.id.swapFAQTab);
        webView = view.findViewById(R.id.wvFAQTab);
        return view;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        WebSettings settings = webView.getSettings();
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String urlValue = bundle.getString("urlFaq");
            if (!TextUtils.isEmpty(urlValue)) {
                url = urlValue;
                webView.setWebViewClient(new MyWebViewClient());
                webView.loadUrl(url);
            }
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.reload();
            }
        });
    }

    public class MyWebViewClient extends WebViewClient{
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        public void onPageFinished(WebView view, String urlWeb) {
            swipeRefreshLayout.setRefreshing(false);
            url = urlWeb;
            super.onPageFinished(view, url);
        }
    }
}
