package com.sbimv.mvicalls.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;

import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.elconfidencial.bubbleshowcase.BubbleShowCase;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseListener;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.adapter.ContactMVICallCursorAdapter;
import com.sbimv.mvicalls.db.ContactContentProvider;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.Objects;

/**
 * Created by Hendi on 11/6/17.
 */

public class ContactMVICallListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, ContactMVICallCursorAdapter.onListClickedCallListner {

    private static String TAG = ContactMVICallListFragment.class.getSimpleName();
    private static final int QUERY_ID = 123;
    // view binding
    private ListView mListView;
    private LinearLayout linSosmed;
    private EditText mEtSearch;
    private ImageView mBtnClearText;
    // variables
    private ContactMVICallCursorAdapter mAdapter;
    private String mSearchTerm;
    private int mPreviouslySelectedSearchItem = 0;
    protected Cursor cursor;
    private ContactDBManager contactDBManager;
    private LinearLayout linInvite;
    private LangViewModel model;

    private OnCallDataClickedCallListner onListener;
    private ShimmerFrameLayout shimmerFriends;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCallDataClickedCallListner) {
            onListener = (OnCallDataClickedCallListner) context;
        }else {
            throw new ClassCastException(context.toString()
                    + " must implemenet ContactMVICallListFragment.OnCallDataClickedCallListner");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactDBManager = new ContactDBManager(getActivity());
        mAdapter = new ContactMVICallCursorAdapter(getActivity(), null, false,this);
        model = LangViewModel.getInstance();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mPreviouslySelectedSearchItem == 0) {
            getLoaderManager().initLoader(QUERY_ID, null, this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_list, container, false);

        shimmerFriends = view.findViewById(R.id.shimmerFriends);

        BubbleInvite();
        linSosmed = view.findViewById(R.id.linSosmed);
        linSosmed.setVisibility(View.GONE);
        mListView = view.findViewById(R.id.lv_contact);
        mEtSearch = view.findViewById(R.id.et_search);

        mBtnClearText = view.findViewById(R.id.btn_clear_text);
        mEtSearch.setHint(model.getLang().string.search);
        mListView.setClickable(true);

        mBtnClearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEtSearch.setText("");
            }
        });

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String newFilter = !TextUtils.isEmpty(charSequence) ? charSequence.toString() : "";
                if (mSearchTerm == null && newFilter == null) {
                    return;
                }
                if (mSearchTerm != null && mSearchTerm.equals(newFilter)) {
                    return;
                }
                if (contactDBManager == null)
                    return;
                mSearchTerm = newFilter;
                cursor = contactDBManager.getCriteriaContact(mSearchTerm);
                mAdapter.swapCursor(cursor);


            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        return view;
    }

    private void BubbleInvite() {
        if (!((Activity) getContext()).isFinishing()) {
            try {
                new BubbleShowCaseBuilder(((Activity) getContext()))
                        .title(model.getLang().string.inviteMoreFriends)
                        .description(model.getLang().string.wordingScInviteFriends)
                        .targetView(linInvite)
                        .arrowPosition(BubbleShowCase.ArrowPosition.BOTTOM)
                        .backgroundColor(getResources().getColor(R.color.white))
//                        .closeActionImage(getResources().getDrawable(R.drawable.close_dark))
                        .textColor(getResources().getColor(R.color.black))
                        .showOnce("LININVITE")
                        .disableCloseAction(true)
                        .titleTextSize(17)
                        .descriptionTextSize(15)
                        .listener(new BubbleShowCaseListener() {
                            @Override
                            public void onCloseActionImageClick(@NotNull BubbleShowCase bubbleShowCase) {

                            }

                            @Override
                            public void onBubbleClick(@NotNull BubbleShowCase bubbleShowCase) {

                            }

                            @Override
                            public void onBackgroundDimClick(@NotNull BubbleShowCase bubbleShowCase) {

                            }

                            @Override
                            public void onTargetClick(@NotNull BubbleShowCase bubbleShowCase) {
                                bubbleShowCase.dismiss();
                            }

                        })
                        .show();
            } catch (Exception e) {
                return;
            }

        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        if (id == QUERY_ID) {
            Uri contentUri;

            if (mSearchTerm == null) {
                contentUri = ContactContentProvider.CONTENT_URI;
            } else {
                contentUri = Uri.withAppendedPath(ContactContentProvider.CONTENT_URI, Uri.encode(mSearchTerm));
            }

            return new CursorLoader(getActivity(),
                    contentUri,
                    null,
                    null,
                    null,
                    null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // This swaps the new cursor into the adapter.
        if (loader.getId() == QUERY_ID) {
            data = contactDBManager.getMVContactAll();
            mAdapter.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == QUERY_ID) {
            mAdapter.swapCursor(null);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mListView.setAdapter(mAdapter);
                shimmerFriends.setVisibility(View.GONE);
            }
        }, BaseActivity.TIME_LOADING);
        shimmerFriends.stopShimmer();
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.UPDATE_FRIEND");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(updateListBR, _if);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(updateListBR);
        clearTextSearch();
    }

    private BroadcastReceiver updateListBR = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            clearTextSearch();
            getContext().getContentResolver().notifyChange(ContactContentProvider.CONTENT_URI, null, false);

        }
    };

    /**
     * Clear Text Search...
     * Added by Yudha Pratama Putra, 08 Nov 2018
     */
    private void clearTextSearch() {
        mEtSearch.setText("");
        mEtSearch.clearFocus();
    }

    @Override
    public void onListSelected(Context ctx, String msisdn, String caller_id,String device) {
        if (!TextUtils.isEmpty(device)){
            ConfigData cdt = SessionManager.getConfigData(getActivity());
            if (cdt != null) {
                if (device.equals("ios")) {
//                    boolean isIosEnable = cdt.a2ac_ios;
                    selectionCall(true,ctx,msisdn,caller_id);
                } else {
                    boolean isAndroidEnable = cdt.a2ac_android;
                    selectionCall(isAndroidEnable,ctx,msisdn,caller_id);
                }
            }else {
                callConventional(ctx,msisdn);
            }
        }else {
            callConventional(ctx,msisdn);
        }
    }

    private void selectionCall(boolean isEnable, Context ctx, String msisdn, String caller_id){
        if (getActivity() instanceof OnCallDataClickedCallListner) {
            if (isEnable){
                onListener.onDataSelected(ctx,msisdn,caller_id);
            }else {
                callConventional(ctx, msisdn);
//                showOptionCall(ctx,msisdn,caller_id);
            }
        }
    }

    private void callConventional(Context ctx, String msisdn){
        Intent phoneIntent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", msisdn, null));
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        ctx.startActivity(phoneIntent);
    }

    private void showOptionCall(Context ctx, String msisdn, String caller_id){
        new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setTitle("Panggilan telepon")
                .setMessage("Pilihan cara telepon teman mu")

                .setNegativeButton("Dengan data", (dialog, which) -> {
                    dialog.dismiss();
                    if (getActivity() instanceof OnCallDataClickedCallListner) {
                        onListener.onDataSelected(ctx,msisdn,caller_id);
                    }
                })

                .setPositiveButton("Dengan pulsa", (dialog, which) -> {
                    dialog.dismiss();
                    callConventional(ctx,msisdn);
                }).show();
    }

    public interface OnCallDataClickedCallListner {
        void onDataSelected(Context ctx, String msisdn, String caller_id);
    }

    @Override
    public void onResume() {
        super.onResume();
        shimmerFriends.startShimmer();
    }

    @Override
    public void onPause() {
        super.onPause();
        shimmerFriends.stopShimmer();
    }
}
