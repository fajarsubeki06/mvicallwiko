package com.sbimv.mvicalls.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
//import androidx.preference.PreferenceFragmentCompat;
//import androidx.preference.SwitchPreferenceCompat;
import android.telecom.TelecomManager;
import android.text.TextUtils;

import com.sbimv.mvicalls.R;

import java.util.Objects;

public class SettingsFragment
//        extends PreferenceFragmentCompat
{

//    private SwitchPreferenceCompat overlayPref;
//    private SwitchPreferenceCompat calldefaultPref;

    public SettingsFragment() {}

//    @Override
//    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
////        setPreferencesFromResource(R.xml.preferences, rootKey);
//
//        overlayPref = (SwitchPreferenceCompat) findPreference(this.getResources().getString(R.string.overlay_switch));
//        calldefaultPref = (SwitchPreferenceCompat) findPreference(this.getResources().getString(R.string.call_default_switch));
//
//        overlayPref.setOnPreferenceClickListener(preference -> {
//            Intent itn = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + Objects.requireNonNull(getActivity()).getPackageName()));
//            if (itn.resolveActivity(getActivity().getPackageManager()) != null) {
//                startActivityForResult(itn, 1234);
//            }
//            return false;
//        });
//
//        calldefaultPref.setOnPreferenceClickListener(preference -> {
//            allowPhoneDefault();
//            return false;
//        });

//    }

//    @Override
//    public void onStart() {
//        super.onStart();
//
//        if (!isCanDrawOverlay()){
//            overlayPref.setEnabled(true);
//            overlayPref.setChecked(false);
//        }else {
//            overlayPref.setEnabled(false);
//            overlayPref.setChecked(true);
//        }
//
//        if (isDefaultCall()){
//            calldefaultPref.setEnabled(true);
//            calldefaultPref.setChecked(false);
//        }else {
//            calldefaultPref.setEnabled(false);
//            calldefaultPref.setChecked(true);
//        }

//    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//        if (isDefaultCall()){
//            calldefaultPref.setEnabled(true);
//            calldefaultPref.setChecked(false);
//        }else {
//            calldefaultPref.setEnabled(false);
//            calldefaultPref.setChecked(true);
//        }

//    }

    private void allowPhoneDefault(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//            Intent intent = new Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER);
//            intent.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, Objects.requireNonNull(getActivity()).getPackageName());
//            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
//                startActivity(intent);
//            }
        }
    }

    private boolean isDefaultCall(){
        boolean isActive = false;
//        try {
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                TelecomManager telecomManager = (TelecomManager) Objects.requireNonNull(getActivity()).getSystemService(Context.TELECOM_SERVICE);
//                if (telecomManager != null) {
//                    String dialerPackage = "";
//                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//                        dialerPackage = TextUtils.isEmpty(telecomManager.getDefaultDialerPackage()) ? "" : telecomManager.getDefaultDialerPackage();
//                        String strPackageName = TextUtils.isEmpty(getActivity().getPackageName()) ? "" : getActivity().getPackageName();
//                        isActive = TextUtils.isEmpty(dialerPackage) || TextUtils.isEmpty(strPackageName) || dialerPackage.equalsIgnoreCase(strPackageName);
//                    }
//                }
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
        return isActive;
    }

    private boolean isCanDrawOverlay(){
        boolean isActive = false;

//        if (Build.VERSION.SDK_INT >= 23) {
//            isActive = Settings.canDrawOverlays(getActivity());
//        }

        return isActive;
    }

}
