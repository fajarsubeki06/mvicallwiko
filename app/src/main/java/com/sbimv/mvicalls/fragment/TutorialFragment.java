package com.sbimv.mvicalls.fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.pojo.DataHowTo;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TutorialFragment extends Fragment {
    private RecyclerView recyclerView;
    private ArrayList<DataHowTo> dataList = new ArrayList<>();
    private WebView webView;
    private SwipeRefreshLayout swipe;
    String url;

    public TutorialFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tutorial2, container, false);
        swipe = view.findViewById(R.id.swipeTutorial);
        webView = view.findViewById(R.id.wvTutorial);
        return view;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        WebSettings settings = webView.getSettings();
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String urlValue = bundle.getString("urlHowToUse");
            if (!TextUtils.isEmpty(urlValue)) {
                url = urlValue;
                webView.setWebViewClient(new TutorWebViewClient());
                webView.loadUrl(url);
            }
        }

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.reload();
            }
        });
    }

    private class TutorWebViewClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String urlWeb) {
            swipe.setRefreshing(false);
            url = urlWeb;
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            swipe.setRefreshing(true);
        }

    }

}
