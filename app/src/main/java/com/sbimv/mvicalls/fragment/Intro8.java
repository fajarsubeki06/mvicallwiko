package com.sbimv.mvicalls.fragment;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.util.PreferenceUtil;

import java.util.Locale;
import java.util.Objects;

import agency.tango.materialintroscreen.SlideFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class Intro8 extends SlideFragment {


    public Intro8() {
        // Required empty public constructor
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro8, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageView imageBackground = view.findViewById(R.id.imageBackground);
        ImageView imageView = view.findViewById(R.id.imageView8);
        imageView.bringToFront();

        Glide.with(this)
                .load(getResources().getIdentifier("wl_intro_bg","drawable",getContext().getPackageName()))
                .into(imageBackground);

        //String res = getString(R.string.wl_intro);

//        if (PreferenceUtil.getPref(getContext()).getString(PreferenceUtil.SAVED_LANG_KEY, "EN").equalsIgnoreCase("ID")) {
//            res = "wl_new_design_in";
//        }else{
//            res = "wl_new_design_en";
//        }


        String getLanguage = PreferenceUtil.getPref(getActivity()).getString(PreferenceUtil.GET_LANG, "");
        if (!TextUtils.isEmpty(getLanguage)){
            if (getLanguage.equalsIgnoreCase("vi") || getLanguage.equalsIgnoreCase("vn")){
                setGlide("bg_viet", imageView);
            } else if (getLanguage.equalsIgnoreCase("th")){
                setGlide("wl_new_design_th", imageView);
            } else{
                setGlide("wl_new_design_en", imageView);
            }
        }


//        Glide.with(this)
//                .load(getResources().getIdentifier(res,"drawable",getContext().getPackageName()))
//                .into(imageView);

        try {
            GATRacker.getInstance(getActivity().getApplication()).sendScreen(TrackerConstant.SCREEN_INTRO_PAGE_1);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setGlide(String img_name, ImageView image){
        Glide.with(this)
                .load(getResources().getIdentifier(img_name,"drawable",getContext().getPackageName()))
                .into(image);
    }

//    getResources().getIdentifier(getString(R.string.image_intro)

    @Override
    public int backgroundColor() {
        return R.color.abu_abu;
    }

    @Override
    public int buttonsColor() {
        return R.color.dark_grey_trans;
    }
}
