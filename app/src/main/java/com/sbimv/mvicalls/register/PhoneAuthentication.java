package com.sbimv.mvicalls.register;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;

import android.provider.Settings;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.activity.IntroActivity;
import com.sbimv.mvicalls.apprtc.SinchService;
import com.sbimv.mvicalls.databinding.ActivityPhoneAuthenticationBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.intro.IntroActivitySecond;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.receiver.InstallReferrerReceiver;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;
import com.sinch.android.rtc.SinchError;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneAuthentication extends BaseActivity implements View.OnClickListener, SinchService.StartFailedListener{

    private TextView tvPhoneNumber;
    private Button btnVerification;
    private TextView tvWrongNumber;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PinView pinViewCode;
    private String mVerificationID;
    private String smsCode;
    Timer mTimer;
    private int mCounter = 60;
    Handler mHandler = new Handler(Looper.getMainLooper());
    private TextView tvTimeCount;
    private TextView tvResendCode;
    private String mCountryCode;
    private String mGetSerialSIM;
    private ProgressBar progress_bar;
    private ImageView imgResendGrey;
    private ImageView imgResendGren;
    private ActivityPhoneAuthenticationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_phone_authentication);
        LangViewModel model = LangViewModel.getInstance();
        binding.setLangModel(model);

        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
        tvWrongNumber = findViewById(R.id.tvWrongNumber);
        btnVerification = findViewById(R.id.btnVerification);
        pinViewCode = findViewById(R.id.pinViewCode);
        pinViewCode.setInputType(InputType.TYPE_CLASS_TEXT);
        tvTimeCount = findViewById(R.id.tvTimeCount);
        tvResendCode = findViewById(R.id.tvResendCode);
        progress_bar = findViewById(R.id.progress_bar);
        imgResendGrey = findViewById(R.id.imgResendGrey);
        imgResendGren = findViewById(R.id.imgResendGren);

        mAuth = FirebaseAuth.getInstance();
//        mAuth.setLanguageCode("th");

        String ccodeTh = PreferenceUtil.getPref(PhoneAuthentication.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
        assert ccodeTh != null;
        if (!TextUtils.isEmpty(ccodeTh) && Objects.requireNonNull(ccodeTh).equalsIgnoreCase("+66")){
            mAuth.setLanguageCode("th");
        }

        btnVerification.setOnClickListener(this);
        tvResendCode.setOnClickListener(this);
        tvWrongNumber.setOnClickListener(this);
        setTimmer();

        /**
         * Intent Get String ......
         */
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            String getMsisdn = bundle.getString("valNum");
            String countyCode = bundle.getString("countyCode");
            String getSerialSIM = bundle.getString("getSerialSIM");
//            if (getSerialSIM == null){
//                getSerialSIM = "";
//            }
//            if (!TextUtils.isEmpty(getMsisdn) && !TextUtils.isEmpty(countyCode) && !TextUtils.isEmpty(getSerialSIM)) {
            if (!TextUtils.isEmpty(getMsisdn) && !TextUtils.isEmpty(countyCode)) {
                tvPhoneNumber.setText(getMsisdn);
                mCountryCode = countyCode;
                mGetSerialSIM = getSerialSIM;
                sendVerificationCode(getMsisdn);
            }
        }

        /*
         * Facebook Screen Event
         * */
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_otp");

        /**
         * Google Analitycs Screen......
         */
        try {

            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_AUTH);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setTimmer(){
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mCounter--;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            tvTimeCount.setText(String.valueOf(mCounter));
                        }catch (Exception e){
                            return;
                        }

                        if(mCounter == 0){
                            mCounter = 60;
                            mTimer.cancel();
                            stopTimmer();
                        }

                    }
                });

            }
        }, 0, 1000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopTimmer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork(tvPhoneNumber);
    }

    /**
     * Double tap to exit......
     */
    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        showSnackBar(model.getLang().string.pressToExit);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onClick(View v) {

            if (btnVerification == v){
            String pinValue = pinViewCode.getText().toString();
            if (!TextUtils.isEmpty(pinValue)){
                stopTimmer();
                validateShowProgress(true);
                verifyVerificationCode(pinValue);
            }else {
                showSnackBar(model.getLang().string.wordingEmptyVerificationCode);
            }
            // Hide keyboard...
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(pinViewCode.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
            }
            // Send Google Analitycs...
            String msisdn = tvPhoneNumber.getText().toString();
            if (!TextUtils.isEmpty(msisdn) && mResendToken != null) {
                sendGoogleTracker(TrackerConstant.SCREEN_AUTH, TrackerConstant.EVENT_CAT_AUTH, "click verification code", msisdn);
            }

        }else if (tvResendCode == v){
            String msisdn = tvPhoneNumber.getText().toString();
            if (!TextUtils.isEmpty(msisdn) && mResendToken != null) {
                // Resend verification...
                setTimmer();
                resendVerificationCode(msisdn);
                validateResendCode(false);
            }else {
                showSnackBar(model.getLang().string.errorFetchingData);
            }
        }else if (tvWrongNumber == v){
            Intent intent = new Intent(PhoneAuthentication.this, PhoneRegister.class);
            startActivity(intent);
            finish();
        }
    }

// =================================================================================================================================================
// ================================================================ Firebase Auth Function =========================================================
// =================================================================================================================================================

    /**
     * Verification code......
     */
    private void verifyVerificationCode(String code) {
        if (!TextUtils.isEmpty(mVerificationID) && mVerificationID != null) {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationID, code);
            signInWithPhoneAuthCredential(credential);
        }else {
            String sError = TextUtils.isEmpty(model.getLang().string.str_verification_invalid)
                    ?"Verification code invalid...":model.getLang().string.str_verification_invalid;
            showSnackBar(sError);
            stopTimmer();
        }
    }

    /**
     * Function resend code......
     */
    private void resendVerificationCode(String number) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(number, 60, TimeUnit.SECONDS, PhoneAuthentication.this, mCallbacks, mResendToken);
        // Send Google Analitycs...
        sendGoogleTracker(TrackerConstant.SCREEN_AUTH, TrackerConstant.EVENT_CAT_AUTH, "resend verification code", number);
    }

    /**
     * Function send code......
     */
    private void sendVerificationCode(String number) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                PhoneAuthentication.this,
                mCallbacks);
    }

    /**
     * Callback response......
     */
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            if (phoneAuthCredential != null && phoneAuthCredential.getSmsCode() != null) {
                smsCode = phoneAuthCredential.getSmsCode();
                if (!TextUtils.isEmpty(smsCode)) {
                    pinViewCode.setText(smsCode);
                    validateShowProgress(true);
                    verifyVerificationCode(smsCode);
                }
            }else {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            String sError = TextUtils.isEmpty(model.getLang().string.str_verification_failed)
                    ?"VerificationFailed":model.getLang().string.str_verification_failed;
            showSnackBar(sError);
            stopTimmer();
        }

        /**
         * Function On Code Sent......
         */
        @Override
        public void onCodeSent(String verificationID, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            mVerificationID = verificationID;
            mResendToken = forceResendingToken;
        }

        /**
         * Function Phone Auth Time Out......
         */
        @Override
        public void onCodeAutoRetrievalTimeOut(String s) {
            super.onCodeAutoRetrievalTimeOut(s);
            String rto = TextUtils.isEmpty(model.getLang().string.str_all_page_rto)
                    ? "Request Time Out"
                    : model.getLang().string.str_all_page_rto;
            showSnackBar(rto);
            stopTimmer();
            // google analytics send phone auth time out...
            String msisdn = tvPhoneNumber.getText().toString();
            if (!TextUtils.isEmpty(msisdn)){
                sendGoogleTracker(TrackerConstant.SCREEN_AUTH, TrackerConstant.EVENT_CAT_AUTH, "phone auth time out", msisdn);
            }
        }
    };

    /**
     * Sign In With Phone Auth Credential......
     */
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(PhoneAuthentication.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        String msisdnAuth = "";
                        if (task.isSuccessful() && task != null) {
                            msisdnAuth = task.getResult().getUser().getPhoneNumber();
                            if (!TextUtils.isEmpty(msisdnAuth)) {
                                stopTimmer();
                                oauthToken(msisdnAuth);
                                // google analytics send phone auth succcessful...
                                sendGoogleTracker(TrackerConstant.SCREEN_AUTH, TrackerConstant.EVENT_CAT_AUTH, "phone auth succcessful", msisdnAuth);
                            }else {

                                showSnackBar(model.getLang().string.authenticationFailed);
                                stopTimmer();
                            }

                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                String otp = TextUtils.isEmpty(model.getLang().string.str_otp_page_invalid_code)
                                        ? "Invalid Code Entered..."
                                        : model.getLang().string.str_otp_page_invalid_code;
                                showSnackBar(otp);

                                stopTimmer();
                                // google analytics send phone auth failed...
                                String tvmsisdn = tvPhoneNumber.getText().toString();
                                if (!TextUtils.isEmpty(tvmsisdn)){
                                    sendGoogleTracker(TrackerConstant.SCREEN_AUTH, TrackerConstant.EVENT_CAT_AUTH, "phone auth failed", msisdnAuth);
                                }
                            }
                        }
                    }
                });
    }

// ===============================================================================================================================================================
// ============================================================ Communication Data Server ========================================================================
// ===============================================================================================================================================================

    /**
     * auth user validation......
     */
    private void oauthToken(String msisdn) {
        // Added by hendi
        String referrer = InstallReferrerReceiver.getReferrer(getApplicationContext());

        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        String device_id = TextUtils.isEmpty(android_id)?"":android_id;

        Call<APIResponse<ContactItem>> call = ServicesFactory.getService().register(msisdn, referrer, "android", mGetSerialSIM, device_id);
        call.enqueue(new Callback<APIResponse<ContactItem>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<ContactItem>> call, @NotNull Response<APIResponse<ContactItem>> response) {
                if(response.body() != null && response.isSuccessful() && response.body().isSuccessful()){
                    if (!isViewAttached)
                        return;
                    ContactItem item = response.body().data;
                    if (item != null){
                        if (!TextUtils.isEmpty(item.name)){
                            //If Old User...
                            if (setPreferenceSimCard() ==  true){

                                String sCallerId = item.caller_id;
                                if(getSinchServiceInterface() != null) {
                                    if (!getSinchServiceInterface().isStarted() && !TextUtils.isEmpty(sCallerId)) {
                                        getSinchServiceInterface().startClient(sCallerId); // Send caller_id to server sinch
                                    }
                                }

                                SessionManager.saveProfile(PhoneAuthentication.this, item);
                                nextProcess();
                            }else {
                                String sError = TextUtils.isEmpty(model.getLang().string.str_session_error)
                                        ?"Save session error....":model.getLang().string.str_session_error;
                                showSnackBar(sError);
                            }
                        }else {
                            //If New User...
                            if (setPreferenceSimCard() ==  true){

                                String sCallerId = item.caller_id;
                                if(getSinchServiceInterface() != null) {
                                    if (!getSinchServiceInterface().isStarted() && !TextUtils.isEmpty(sCallerId)) {
                                        getSinchServiceInterface().startClient(sCallerId); // Send caller_id to server sinch
                                    }
                                }

                                SessionManager.saveProfile(PhoneAuthentication.this, item);
                                PreferenceUtil.getEditor(PhoneAuthentication.this).putBoolean(PreferenceUtil.NEWUSER, true).commit();
                                nextProcess();
                            }else {
                                String sError = TextUtils.isEmpty(model.getLang().string.str_session_error)
                                        ?"Save session error....":model.getLang().string.str_session_error;
                                showSnackBar(sError);
                            }
                        }

                    }else {
                        String sError = TextUtils.isEmpty(model.getLang().string.str_failed_register)
                                ?"Failed while getting registration data...":model.getLang().string.str_failed_register;
                        showSnackBar(sError);
                    }

                }
                else{
                    showSnackBar( model.getLang().string.authenticationFailed);
                }
            }
            @Override
            public void onFailure(@NotNull Call<APIResponse<ContactItem>> call, @NotNull Throwable t) {
                if (!isViewAttached)
                    return;
                showSnackBar( model.getLang().string.authenticationFailed);
            }
        });
    }

// ===============================================================================================================================================================
// ===============================================================================================================================================================
// ===============================================================================================================================================================

    /**
     * Next Process......
     */
    private void nextProcess(){
        // remove referrer
        InstallReferrerReceiver.setReferrer(getApplicationContext(), null);
        AppPreference.setPreference(PhoneAuthentication.this, AppPreference.PREFIX, mCountryCode);
        PreferenceUtil.getEditor(PhoneAuthentication.this).putBoolean(PreferenceUtil.ISLOGIN, true).commit();
        if (!isViewAttached)
            return;

        boolean isSwitch = PreferenceUtil.getPref(PhoneAuthentication.this).getBoolean(PreferenceUtil.ISWITCH, false); //Is Login
        if (isSwitch == true){
            startActivity(new Intent(PhoneAuthentication.this, IntroActivitySecond.class));
        }else {
            startActivity(new Intent(PhoneAuthentication.this, IntroActivity.class));
        }

        generateTokenFirebase();

//        Intent intent = new Intent(PhoneAuthentication.this, IntroActivity.class);
//        Intent intent = new Intent(PhoneAuthentication.this, IntroActivitySecond.class);
//        startActivity(intent);

        finish();
    }

//    private void nextProcess(){
//        // remove referrer
//        InstallReferrerReceiver.setReferrer(getApplicationContext(), null);
//        AppPreference.setPreference(PhoneAuthentication.this, AppPreference.PREFIX, mCountryCode);
//        if (!isViewAttached)
//            return;
//        Intent intent = new Intent(PhoneAuthentication.this, IntroActivity.class);
//        startActivity(intent);
//        finish();
//    }

    /**
     * Save Preference SIM Card......
     */
    private boolean setPreferenceSimCard(){
        boolean has = false;
        String preferMsisdn = tvPhoneNumber.getText().toString();
//        if (!TextUtils.isEmpty(preferMsisdn) && !TextUtils.isEmpty(mCountryCode)){
        if (!TextUtils.isEmpty(preferMsisdn) && !TextUtils.isEmpty(mGetSerialSIM) && !TextUtils.isEmpty(mCountryCode)){
            String setPrefMsisdn = Util.formatMSISDNREG(preferMsisdn,mCountryCode);
            AppPreference.setPreference(PhoneAuthentication.this, AppPreference.SERIAL_SIM, mGetSerialSIM);
            AppPreference.setPreference(PhoneAuthentication.this, AppPreference.CURRENT_MSISDN, setPrefMsisdn);
            has = true;
        }else {
            has = false;
        }
        return has;
    }

    /**
     * Google Analitycs Tracker......
     */
    private void stopTimmer(){
        if (mTimer != null){
            mTimer.cancel();
            validateResendCode(true);
            validateShowProgress(false);
        }
    }

    /**
     * Validate UI Resend Code......
     */
    private void validateResendCode(boolean isResend){
        if (!PhoneAuthentication.this.isFinishing()) {
            if (isResend == true) {
                tvResendCode.setTextColor(getResources().getColor(R.color.green_dark));
                tvResendCode.setEnabled(true);
                imgResendGrey.setVisibility(View.GONE);
                imgResendGren.setVisibility(View.VISIBLE);
                tvTimeCount.setVisibility(View.GONE);
                mCounter = 60;
            } else {
                tvResendCode.setTextColor(getResources().getColor(R.color.grey_ACACACF));
                tvResendCode.setEnabled(false);
                imgResendGren.setVisibility(View.GONE);
                imgResendGrey.setVisibility(View.VISIBLE);
                tvTimeCount.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Validate Show Progress......
     */
    private void validateShowProgress(boolean isShowProgress){
        if (!PhoneAuthentication.this.isFinishing()) {
            if (isShowProgress == true) {
                progress_bar.setVisibility(View.VISIBLE);
                btnVerification.setVisibility(View.INVISIBLE);
            }else {
                progress_bar.setVisibility(View.INVISIBLE);
                btnVerification.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Google Analitycs Tracker......
     */
    private void sendGoogleTracker(String screen, String event, String action, String phoneNumber){
        try {
            GATRacker.getInstance(getApplication()).sendEventWithScreen(screen, event, action, phoneNumber);
        }catch (Exception e){
            e.printStackTrace();
            return;
        }
    }

    /**
     * UI Snackbar........
     */
    private void showSnackBar(String argument){
        if (!isViewAttached)
            return;
        try{
            Snackbar.make(findViewById(android.R.id.content), argument, Snackbar.LENGTH_LONG).show();
        }catch (Exception e){
            return;
        }
    }

    @Override
    protected void onServiceConnected() {
        System.out.println("onServiceConnected");
        if(getSinchServiceInterface() != null) {
            getSinchServiceInterface().setStartListener(this);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        super.onServiceDisconnected(componentName);
        System.out.println("onServiceDisconnected");
    }

    @Override
    public void onStartFailed(SinchError error) {
        System.out.println("onStartFailed");
    }

    @Override
    public void onStarted() {
        System.out.println("onStarted");
    }

}