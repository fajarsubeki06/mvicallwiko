package com.sbimv.mvicalls.register;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import androidx.databinding.DataBindingUtil;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import androidx.core.app.ActivityCompat;

import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustEvent;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.base.StandardSystemProperty;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.BuildConfig;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.LocaleHelper;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.activity.CakraAuthentication;
import com.sbimv.mvicalls.activity.IntroActivity;
import com.sbimv.mvicalls.activity.TermConditionActivity;
import com.sbimv.mvicalls.activity.UploadVideoToneActivity;
import com.sbimv.mvicalls.apprtc.SinchService;
import com.sbimv.mvicalls.databinding.ActivityPhoneRegisterBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.intro.IntroActivitySecond;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.ModelCheckTelko;
import com.sbimv.mvicalls.receiver.InstallReferrerReceiver;
import com.sbimv.mvicalls.util.PermissionUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;
import com.sbimv.mvicalls.util.ValidationInputUtil;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;
import com.sinch.android.rtc.SinchError;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneRegister extends BaseActivity implements View.OnClickListener,
        CountryCodePicker.OnCountryChangeListener, CompoundButton.OnCheckedChangeListener, SinchService.StartFailedListener {

    final String key = "lang_has_set";
    final String key_country = "country_has_set";
    /**
     * Double tap to exit......
     */
    boolean doubleBackToExitPressedOnce = false;
    private EditText etPhoneNo;
    private Button btnRegister;
    private TextView tvTNC;
    private CountryCodePicker ccp;
    private String getSerialSIM;
    private String countyCode;
    private String countryNameCode;
    private String countryName;
    private ProgressBar progress_bar;
    private ImageView imgLogo;
    private ActivityPhoneRegisterBinding mBinding;
    private boolean isTakeUrl = false;
    private String getSimNumber;
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_phone_register);
        mBinding.setLangModel(model);

        linearLayout = findViewById(R.id.linearLayout);
        setKeyboard(linearLayout);

        imgLogo = findViewById(R.id.imgLogo);
        etPhoneNo = findViewById(R.id.etPhoneNo);
        ccp = findViewById(R.id.codeCountry);
        btnRegister = findViewById(R.id.btnReqToken);
        progress_bar = findViewById(R.id.progress_bar);
        tvTNC = findViewById(R.id.tvTnC);
        CheckBox cbTNC = findViewById(R.id.cbTNC);

        Glide.with(PhoneRegister.this)
                .asBitmap()
                .load(R.drawable.logo_rebranding_register)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(final Bitmap bitmap, Transition<? super Bitmap> transition) {
                        final int w = bitmap.getWidth();
                        final int h = bitmap.getHeight();
                        imgLogo.post(() -> {
                            int ivW = imgLogo.getMeasuredWidth();
                            int ivH = ivW * h / w;
                            imgLogo.getLayoutParams().height = ivH;
                            imgLogo.requestLayout();
                            imgLogo.setImageBitmap(bitmap);
                        });

                    }
                });


        // Inisial on action...........
        tvTNC.setOnClickListener(PhoneRegister.this);
        btnRegister.setOnClickListener(PhoneRegister.this);
        cbTNC.setOnCheckedChangeListener(this);

        // Country code....

        if (ccp != null) {
            String selectedCCP = PreferenceManager.getDefaultSharedPreferences(this).getString(key_country, null);
            if (!TextUtils.isEmpty(selectedCCP)) {
                ccp.setDefaultCountryUsingNameCode(selectedCCP);
                ccp.resetToDefaultCountry();
            }

            getListCountryCode();

            String defaultCountryCode = ccp.getDefaultCountryCodeWithPlus();
            if (!TextUtils.isEmpty(defaultCountryCode)) {
                countyCode = defaultCountryCode;
            }

            countryNameCode = TextUtils.isEmpty(ccp.getDefaultCountryNameCode()) ? "" : ccp.getDefaultCountryNameCode();
            countryName = ccp.getDefaultCountryName();
            ccp.setOnCountryChangeListener(this);

            if (!TextUtils.isEmpty(ccp.getDefaultCountryCode())) {
                String ccode = ccp.getDefaultCountryCode();
                if (!TextUtils.isEmpty(ccode)) {
                    getBaseUrl(ccode.replace("+", "")); // Get Base URL for country code
                    model.setLangFromRemote(countryNameCode, ccode, () -> mBinding.invalidateAll()); //update language
                }
            }

        }

        /*
         * Adjust................
         * */
        AdjustEvent event = new AdjustEvent("wy3g1q");
        event.setCallbackId("aj_app_register");
        event.addCallbackParameter("key", "value");
        event.addPartnerParameter("foo", "bar");
        Adjust.trackEvent(event);

        // google analytics...
        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_REGISTER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
         * Facebook Screen Event
         * */
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_register");

        readDynamicLink();

    }

    //==========================================================================================================

    private void readDynamicLink() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        if (pendingDynamicLinkData == null)
                            return;
                        Uri deepLink = pendingDynamicLinkData.getLink();
                        Log.e("DEEP LINK", String.valueOf(deepLink));
                        if (deepLink != null) {
                            dynamicLinkInstall(deepLink);
                        }
                    }
                });
    }

    private void dynamicLinkInstall(Uri deepLink) {
        if (deepLink != null) {

            if (deepLink.getBooleanQueryParameter("_msisdn", false)
                    && deepLink.getBooleanQueryParameter("content_id", false)) {

                String msisdn = deepLink.getQueryParameter("_msisdn");
                String content = deepLink.getQueryParameter("content_id");

                if (!TextUtils.isEmpty(msisdn) && !TextUtils.isEmpty(content)) {

                    PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.MSISDN_DEEPLINK, msisdn).commit();
                    PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.CONTENT_DEEPLINK, content).commit();

                }

            } else if (deepLink.getBooleanQueryParameter("_msisdn", false)) {

                String source = deepLink.getQueryParameter("_msisdn");
                if (!TextUtils.isEmpty(source)) {
                    etPhoneNo.setText(source);
                    PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.BY_PASS, true).commit();
                    PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.BY_PASS_NUMBER, source).commit();
                    checkPermissionGranded();
                }

            } else if (deepLink.getBooleanQueryParameter("source", false)) {

                String source = deepLink.getQueryParameter("source");
                String url = deepLink.toString();
                if (!TextUtils.isEmpty(source) && !TextUtils.isEmpty(url)) {
                    PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.SOURCE_DEEPLINK, source).commit();
                    PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.URL_DEEPLINK, url).commit();
                }

            }
        }
    }

    private void checkPermissionGranded() {
        Dexter.withActivity(this).withPermissions(permissions).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    boolean isByPass = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.BY_PASS, false);
                    if (isByPass) {

                        String sMsisdn = PreferenceUtil.getPref(getApplicationContext())
                                .getString(PreferenceUtil.BY_PASS_NUMBER, "");

                        if (!TextUtils.isEmpty(sMsisdn)) {
                            oauthToken(sMsisdn);
                        }

                    }
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

            }
        }).onSameThread().check();
    }

    private void oauthToken(String msisdn) {

        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        String device_id = TextUtils.isEmpty(android_id)?"":android_id;

        String referrer = InstallReferrerReceiver.getReferrer(getApplicationContext());
        Call<APIResponse<ContactItem>> call = ServicesFactory.getService().register(msisdn, referrer, "android", getSerialSIM, device_id);
        call.enqueue(new Callback<APIResponse<ContactItem>>() {
            @Override
            public void onResponse(Call<APIResponse<ContactItem>> call, Response<APIResponse<ContactItem>> response) {
                if (response.body() != null && response.isSuccessful() && response.body().isSuccessful()) {
                    if (!isViewAttached)
                        return;
                    ContactItem item = response.body().data;
                    if (item != null) {
                        SessionManager.saveProfile(PhoneRegister.this, item);
                        if (!TextUtils.isEmpty(item.name)) {
                            //If Old User...
                            if (setPreferenceSimCard(msisdn) == true) {

                                String sCallerId = item.caller_id;
                                if (getSinchServiceInterface() != null) {
                                    if (!getSinchServiceInterface().isStarted() && !TextUtils.isEmpty(sCallerId)) {
                                        getSinchServiceInterface().startClient(sCallerId); // Send caller_id to server sinch
                                    }
                                }

                                SessionManager.saveProfile(PhoneRegister.this, item);
                                nextProcess();
                            } else {
                                String sError = TextUtils.isEmpty(model.getLang().string.str_session_error)
                                        ?"Save session error....":model.getLang().string.str_session_error;
                                showSnackBar(sError);
                            }
                        } else {
                            //If New User...
                            if (setPreferenceSimCard(msisdn) == true) {

                                String sCallerId = item.caller_id;
                                if (getSinchServiceInterface() != null) {
                                    if (!getSinchServiceInterface().isStarted() && !TextUtils.isEmpty(sCallerId)) {
                                        getSinchServiceInterface().startClient(sCallerId); // Send caller_id to server sinch
                                    }
                                }

                                SessionManager.saveProfile(PhoneRegister.this, item);
                                PreferenceUtil.getEditor(PhoneRegister.this).putBoolean(PreferenceUtil.NEWUSER, true).commit();
                                nextProcess();
                            } else {
                                String sError = TextUtils.isEmpty(model.getLang().string.str_session_error)
                                        ?"Save session error....":model.getLang().string.str_session_error;
                                showSnackBar(sError);
                            }
                        }

                    } else {
                        String sError = TextUtils.isEmpty(model.getLang().string.str_failed_register)
                                ?"Failed while getting registration data...":model.getLang().string.str_failed_register;
                        showSnackBar(sError);
                    }

                } else {
                    String sError = TextUtils.isEmpty(model.getLang().string.str_auth_failed)
                            ?"Authentication failed":model.getLang().string.str_auth_failed;
                    showSnackBar(sError);
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ContactItem>> call, Throwable t) {
                if (!isViewAttached)
                    return;
                String sError = TextUtils.isEmpty(model.getLang().string.str_auth_failed)
                        ?"Authentication failed":model.getLang().string.str_auth_failed;
                showSnackBar(sError);
            }
        });
    }

    private void showSnackBar(String argument) {
        if (!isViewAttached)
            return;
        try {
            Snackbar.make(findViewById(android.R.id.content), argument, Snackbar.LENGTH_LONG).show();
        } catch (Exception e) {
            return;
        }
    }

    @Override
    protected void onServiceConnected() {
        System.out.println("onServiceConnected");
        if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().setStartListener(this);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        super.onServiceDisconnected(componentName);
        System.out.println("onServiceDisconnected");
    }

    @Override
    public void onStartFailed(SinchError error) {
        System.out.println("onStartFailed");
    }

    @Override
    public void onStarted() {
        System.out.println("onStarted");
    }

    private boolean setPreferenceSimCard(String msisdn) {
        boolean has = false;
        String preferMsisdn = msisdn;
        if (!TextUtils.isEmpty(preferMsisdn) && !TextUtils.isEmpty(getSerialSIM) && !TextUtils.isEmpty(countyCode)) {
            String setPrefMsisdn = Util.formatMSISDNREG(preferMsisdn, countyCode);
            AppPreference.setPreference(PhoneRegister.this, AppPreference.SERIAL_SIM, getSerialSIM);
            AppPreference.setPreference(PhoneRegister.this, AppPreference.CURRENT_MSISDN, setPrefMsisdn);
            has = true;
        } else {
            has = false;
        }
        return has;
    }

    private void nextProcess() {
        // remove referrer
        InstallReferrerReceiver.setReferrer(getApplicationContext(), null);
        AppPreference.setPreference(PhoneRegister.this, AppPreference.PREFIX, countyCode);
        PreferenceUtil.getEditor(PhoneRegister.this).putBoolean(PreferenceUtil.ISLOGIN, true).commit();
        if (!isViewAttached)
            return;

        boolean isSwitch = PreferenceUtil.getPref(PhoneRegister.this).getBoolean(PreferenceUtil.ISWITCH, false); //Is Login
        if (isSwitch == true) {
            startActivity(new Intent(PhoneRegister.this, IntroActivitySecond.class));
        } else {
            startActivity(new Intent(PhoneRegister.this, IntroActivity.class));
        }
        generateTokenFirebase();

        PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.BY_PASS, false).commit();
        PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.BY_PASS_NUMBER, "").commit();

        finish();
    }

    //===============================================================================================

    private void getListCountryCode() {
        Call<APIResponse<ArrayList<String>>> call = ServicesFactory.getService().getListCountryCode();
        call.enqueue(new Callback<APIResponse<ArrayList<String>>>() {
            @Override
            public void onResponse(@org.jetbrains.annotations.NotNull Call<APIResponse<ArrayList<String>>> call, @org.jetbrains.annotations.NotNull Response<APIResponse<ArrayList<String>>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    try {
                        ArrayList<String> list = new ArrayList<String>();
                        list = response.body().data;
                        if (list.size() > 0) {
                            String str = TextUtils.join(",", list);
                            if (!TextUtils.isEmpty(str)) {
                                if (ccp != null) {
                                    ccp.setCustomMasterCountries(str);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("response null", "response null");
                }
            }

            @Override
            public void onFailure(@org.jetbrains.annotations.NotNull Call<APIResponse<ArrayList<String>>> call, @org.jetbrains.annotations.NotNull Throwable t) {
                Log.e("err_list_country_code", "isFailure");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            readPhoneAuto();
        } catch (Exception e) {
            e.printStackTrace();
        }

        checkNetwork(etPhoneNo);

        boolean isByPass = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.BY_PASS, false);
        if (isByPass) {
            checkPermissionGranded();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        isProgress(false);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        toast(model.getLang().string.pressToExit);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    /**
     * OnCheckedChanged methode.......
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            btnRegister.setEnabled(true);
            btnRegister.setBackground(getResources().getDrawable(R.drawable.selector_rounded_blue_logo));
            btnRegister.setTextColor(getResources().getColor(R.color.white));
        } else {
            btnRegister.setEnabled(false);
            btnRegister.setBackground(getResources().getDrawable(R.drawable.button_selector));
            btnRegister.setTextColor(getResources().getColor(R.color.white));
        }
    }

    /**
     * OnClick methode........
     */
    @Override
    public void onClick(View view) {

        if (tvTNC == view) { // TNC on click............
            Intent intent = new Intent(PhoneRegister.this, TermConditionActivity.class);
            startActivity(intent);
            // google analytics...
            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_REGISTER, TrackerConstant.EVENT_CAT_REGISTER, "click tnc", "");
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (btnRegister == view) { // Button register on click...............

            if (checkValidation(etPhoneNo)) {
                showDialogRegister();
            }

        }
    }

    public void showDialogRegister() {
        if (!PhoneRegister.this.isFinishing()) {
            try {
                final AlertDialog dialog = new AlertDialog.Builder(PhoneRegister.this)
                        .setMessage(model.getLang().string.wordPopupRegister)
                        .setNegativeButton(model.getLang().string.wordChangeInDialog, null)
                        .setPositiveButton(model.getLang().string.wordTelcoPosButton, null) //Set to null. We override the onclick
                        .setCancelable(true).create();

                dialog.setOnShowListener(dialogInterface -> {

                    Button buttonNegative = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                    buttonNegative.setOnClickListener(view -> dialog.dismiss());

                    Button buttonPositive = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    buttonPositive.setOnClickListener(view -> {

                        PreferenceUtil.getEditor(PhoneRegister.this).putString(PreferenceUtil.COUNTRY_CODE, countyCode).commit();
                        PreferenceUtil.getEditor(PhoneRegister.this).putString(PreferenceUtil.COUNTRY_NAME_CODE, countryNameCode).commit();
                        PreferenceUtil.getEditor(PhoneRegister.this).putString(PreferenceUtil.COUNTRY_NAME, countryName).commit();

                        String cciso = TextUtils.isEmpty(countryNameCode)?"EN":countryNameCode;
                        LocaleHelper.setLocale(this, cciso);

                        String phoneNo = etPhoneNo.getText().toString();
                        String filter = "";
                        if (phoneNo.startsWith("0")) {
                            filter = phoneNo.replaceFirst("0", "");
                        } else if (phoneNo.startsWith("+")) {
                            filter = takeCountryCode(phoneNo, countryNameCode);
                        }  else {
                            filter = phoneNo;
                        }

                        String msisdn = countyCode + filter;
                        if (!TextUtils.isEmpty(msisdn) && !TextUtils.isEmpty(countyCode) /*&& !TextUtils.isEmpty(getSerialSIM)*/) {
                            isProgress(true);
                            checkTelko(msisdn, countyCode, getSerialSIM);

                        }
                        dialog.dismiss();
                    });
                });
                dialog.show();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private void isProgress(boolean isLoading) {
        if (!PhoneRegister.this.isFinishing()) {
            if (isLoading) {
                progress_bar.setVisibility(View.VISIBLE);
                btnRegister.setVisibility(View.GONE);
            } else {
                progress_bar.setVisibility(View.GONE);
                btnRegister.setVisibility(View.VISIBLE);
            }
        }
    }

    private void checkTelko(final String msisdn, final String countyCode, final String getSerialSIM) {
        int versionCode = BuildConfig.VERSION_CODE;
        String appVersion = String.valueOf(versionCode);

        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        String device_id = TextUtils.isEmpty(android_id)?"":android_id;


        Call<APIResponse<List<ModelCheckTelko>>> call = ServicesFactory.getService().checkTelco(msisdn, getSerialSIM, appVersion, device_id);
        call.enqueue(new Callback<APIResponse<List<ModelCheckTelko>>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<List<ModelCheckTelko>>> call, @NotNull Response<APIResponse<List<ModelCheckTelko>>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    List<ModelCheckTelko> data = response.body().data;
                    if (data != null) {

                        String ccodeTh = PreferenceUtil.getPref(PhoneRegister.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
                        assert ccodeTh != null;
                        if (!TextUtils.isEmpty(ccodeTh) && Objects.requireNonNull(ccodeTh).equalsIgnoreCase("+66")){
                            String strName = data.get(0).name;
                            if (TextUtils.isEmpty(strName)){
                                showAlert();
                                return;
                            }
                        }

                        String strValidate = data.get(0).auth_type;
                        if (!TextUtils.isEmpty(strValidate)) {

                            if (strValidate.equals("bypass")) {
                                oauthToken(msisdn);
                                return;
                            }

                            if (strValidate.equals("cakra")) {
                                authPageSelected(true, msisdn, countyCode, getSerialSIM);
                            } else {
                                authPageSelected(false, msisdn, countyCode, getSerialSIM);
                            }
                        } else {
                            authPageSelected(false, msisdn, countyCode, getSerialSIM);
                        }
                        //authPageSelected(true, msisdn, countyCode, getSerialSIM);
                    } else {
                        authPageSelected(false, msisdn, countyCode, getSerialSIM);
                        //authPageSelected(true, msisdn, countyCode, getSerialSIM);
                    }


                }
                isProgress(false);
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<List<ModelCheckTelko>>> call, @NotNull Throwable t) {
                isProgress(false);
                authPageSelected(false, msisdn, countyCode, getSerialSIM);
                //authPageSelected(true, msisdn, countyCode, getSerialSIM);
            }
        });
    }

    private void authPageSelected(boolean isCakra, String msisdn, String countyCode, String getSerialSIM) {
        Intent itn;
        if (isTakeUrl) {
            if (isCakra)
                itn = new Intent(PhoneRegister.this, CakraAuthentication.class);
            else
                itn = new Intent(PhoneRegister.this, PhoneAuthentication.class);

            itn.putExtra("valNum", msisdn);
            itn.putExtra("countyCode", countyCode);
            itn.putExtra("getSerialSIM", getSerialSIM);
            startActivity(itn);
            finish();

            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_REGISTER, TrackerConstant.EVENT_CAT_REGISTER, "click register", msisdn);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {

            Call<APIResponse<String>> call = ServicesFactory.getServiceBaseURL().getBaseUrl(countyCode);
            call.enqueue(new Callback<APIResponse<String>>() {
                @Override
                public void onResponse(@NotNull Call<APIResponse<String>> call, @NotNull Response<APIResponse<String>> response) {
                    if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                        isTakeUrl = true;
                        authPageSelected(isCakra, msisdn, countyCode, getSerialSIM);
                    } else {
                        isTakeUrl = false;
                        String sError = TextUtils.isEmpty(model.getLang().string.str_connection_failed)
                                ?"The connection to server failed. Please check your connection and try again.":model.getLang().string.str_connection_failed;
                        Toast.makeText(PhoneRegister.this,
                                sError,
                                Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<APIResponse<String>> call, @NotNull Throwable t) {
                    isTakeUrl = false;
                    String sError = TextUtils.isEmpty(model.getLang().string.str_connection_failed)
                            ?"The connection to server failed. Please check your connection and try again.":model.getLang().string.str_connection_failed;
                    Toast.makeText(PhoneRegister.this,
                            sError,
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * OnCountrySelected methode..........
     */
    @Override
    public void onCountrySelected(Country country) {
        if (country != null && ccp != null) {
            countyCode = ccp.getSelectedCountryCodeWithPlus();
            countryNameCode = ccp.getSelectedCountryNameCode();
            countryName = ccp.getSelectedCountryName();
            String ccode = ccp.getSelectedCountryCode();

            if (!TextUtils.isEmpty(ccode)) {
                getBaseUrl(ccode.replace("+", "")); // Get Base URL for country code
                model.setLangFromRemote(countryNameCode, ccode, () -> mBinding.invalidateAll());  //update language
            }

            // google analytics...
            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_REGISTER, TrackerConstant.EVENT_CAT_REGISTER, "change country code", countyCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getBaseUrl(String ccode) {
        isTakeUrl = false;
        Call<APIResponse<String>> call = ServicesFactory.getServiceBaseURL().getBaseUrl(ccode);
        call.enqueue(new Callback<APIResponse<String>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<String>> call, @NotNull Response<APIResponse<String>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    String baseURL = response.body().data;
                    PreferenceUtil.getEditor(PhoneRegister.this).putString(PreferenceUtil.BASE_URL, baseURL).commit();
                    isTakeUrl = true;
                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<String>> call, @NotNull Throwable t) {
                isTakeUrl = false;
                Toast.makeText(PhoneRegister.this, model.getLang().string.wordingFailureResponse, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Read phone auto function.............
     */
//    public void readPhoneAuto() {
//        if (PermissionUtil.hashPermission(PhoneRegister.this, Manifest.permission.READ_PHONE_STATE)) {
//            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
//                if (telephonyManager != null) {
//                    setDefaultLanguage(telephonyManager.getSimCountryIso());
//                    String filter;
//                    if (telephonyManager.getSimSerialNumber() != null && !TextUtils.isEmpty(telephonyManager.getSimSerialNumber())) {
//                        getSerialSIM = telephonyManager.getSimSerialNumber();
//                    }
//                    if (telephonyManager.getLine1Number() != null && !TextUtils.isEmpty(telephonyManager.getLine1Number())) {
//                        String getSimNumber = telephonyManager.getLine1Number();
//                        if (getSimNumber.startsWith("0")) {
//                            filter = getSimNumber.replaceFirst("0", "");
//                        } else if (getSimNumber.startsWith("+")) {
//                            filter = takeCountryCode(getSimNumber, countryNameCode);
//                        } else {
//                            filter = getSimNumber;
//                        }
//                        String setMsisdn = Util.formatMSISDNREG(filter, countyCode);
//                        etPhoneNo.setText(setMsisdn);
//                    }
//                }
//            }
//        }
//    }

    @SuppressLint("HardwareIds")
    public void readPhoneAuto() throws Exception {
        if (PermissionUtil.hashPermission(PhoneRegister.this, Manifest.permission.READ_PHONE_STATE)) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                SubscriptionManager manager = (SubscriptionManager) getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE); // min Lollipop MR1
                if (manager != null) {
                    int defaultSmsId = manager.getDefaultSubscriptionId(); // min Nougat
                    if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }

                    SubscriptionInfo info = manager.getActiveSubscriptionInfo(defaultSmsId);
                    if (info != null){
                        getSerialSIM = TextUtils.isEmpty(info.getIccId())?"":info.getIccId();
                        String infoNumber = TextUtils.isEmpty(info.getNumber())?"":info.getNumber();

                        String filter;
                        if (!TextUtils.isEmpty(infoNumber)){
                            getSimNumber = infoNumber;
                            if (getSimNumber.startsWith("0")) {
                                filter = getSimNumber.replaceFirst("0", "");
                            } else if (getSimNumber.startsWith("+")) {
                                filter = takeCountryCode(getSimNumber, countryNameCode);
                            } else {
                                filter = getSimNumber;
                            }
                            String setMsisdn = Util.formatMSISDNREG(filter, countyCode);
                            etPhoneNo.setText(setMsisdn);
                        }
                    }
                }

            }
            else {

                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE)
                        == PackageManager.PERMISSION_GRANTED) {

                    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    if (telephonyManager != null) {

                        String simCountryIso = TextUtils.isEmpty(telephonyManager.getSimCountryIso())?""
                                :telephonyManager.getSimCountryIso();

                        if (!TextUtils.isEmpty(simCountryIso)) {

                            setDefaultLanguage(simCountryIso);
                            String simSerialNumber = TextUtils.isEmpty(telephonyManager.getSimSerialNumber())
                                    ? "" : telephonyManager.getSimSerialNumber();

                            String filter = "";
                            if (!TextUtils.isEmpty(simSerialNumber)) {
                                getSerialSIM = simSerialNumber;
                            }

                            String line1Number = TextUtils.isEmpty(telephonyManager.getLine1Number()) ? "" :
                                    telephonyManager.getLine1Number();

                            if (!TextUtils.isEmpty(line1Number)) {
                                getSimNumber = line1Number;
                                if (getSimNumber.startsWith("0")) {
                                    filter = getSimNumber.replaceFirst("0", "");
                                } else if (getSimNumber.startsWith("+")) {
                                    filter = takeCountryCode(getSimNumber, countryNameCode);
                                } else {
                                    filter = getSimNumber;
                                }
                                String setMsisdn = Util.formatMSISDNREG(filter, countyCode);
                                etPhoneNo.setText(setMsisdn);
                            }
                        }
                    }
                }

            }
        }
    }


    private void setDefaultLanguage(String simCountryCodeISO) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(PhoneRegister.this);
        boolean hasSet = preferences.getBoolean(key, false);
        if (!hasSet) {
            preferences.edit().putBoolean(key, true).apply();

            // set default language
            if (simCountryCodeISO.equalsIgnoreCase("ID")) {
                // set to Bahasa Indonesia
                LocaleHelper.setLocale(this, simCountryCodeISO);
            }

            // set default country code spinner
            // ID,VN,TH,MY,SG,MM,PH,LA,KH,BN
            String code = simCountryCodeISO.toUpperCase();
            switch (code) {
                case "ID":
                case "VN":
                case "TH":
                case "MY":
                case "HK":
                case "SG":
                case "MM":
                case "PH":
                case "LA":
                case "KH":
                case "BN":
                case "PE":
                    preferences.edit().putString(key_country, code).apply();
            }
            recreate();
        }
    }

    /**
     * Check validation string empty...
     */
    private boolean checkValidation(EditText etPhoneNo) {
        boolean ret = true;
        if (!ValidationInputUtil.hasText(etPhoneNo)) ret = false;
        return ret;
    }

    /**
     * Take Country Code...
     */
    private String takeCountryCode(String filterNo, String codeName) {
        long nationalNumber = 0;
        try {
            // phone must begin with '+'
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(this);
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(filterNo, codeName);
            nationalNumber = numberProto.getNationalNumber();
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }
        return String.valueOf(nationalNumber);
    }

    private void showAlert(){
        if (!PhoneRegister.this.isFinishing()){
            try{
                String strTitle = TextUtils.isEmpty(model.getLang().string.title_ais_dialog)
                        ?"Sorry!" :
                        model.getLang().string.title_ais_dialog;

                String strSubTitle = TextUtils.isEmpty(model.getLang().string.block_not_ais_user)
                        ?"Only available for AIS user" :
                        model.getLang().string.block_not_ais_user;


                String strWordingOke = TextUtils.isEmpty(model.getLang().string.title_ok)
                        ?"Oke" :
                        model.getLang().string.title_ok;

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                View v = getLayoutInflater().inflate(R.layout.dialog_ais, null);
                builder.setView(v);

                TextView titleCongrats = v.findViewById(R.id.textTitleAis);
                TextView subtitleAis = v.findViewById(R.id.textSubTitleAis);
                Button ok = v.findViewById(R.id.btnOkAis);

                titleCongrats.setText(strTitle);
                subtitleAis.setText(strSubTitle);

                final AlertDialog alert = builder.create();
                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alert.setCanceledOnTouchOutside(true);

                ok.setText(strWordingOke);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        isProgress(false);
                        alert.dismiss();
                    }
                });

                alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        alert.dismiss();
                        isProgress(false);
                    }
                });


                if (!alert.isShowing()) {
                    alert.show();
                } else {
                    alert.dismiss();
                    isProgress(false);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
