package com.sbimv.mvicalls.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;

import com.diroag.floatingwindows.service.FloatingWindowController;

public class FloatingViewUtil {

    static FloatingViewUtil util;
    Context appContext;
    FloatingWindowController fwc;
    FloatingButtonCall view;

    public static FloatingViewUtil getInstance(Context _appContext){
        if(util == null){
            util = new FloatingViewUtil(_appContext);
        }
        return util;
    }

    private FloatingViewUtil(Context ctx){
        appContext = ctx;
        fwc = FloatingWindowController.create(ctx.getApplicationContext());
    }

    public void showView(){
        int[] point;
        view = new FloatingButtonCall(appContext);
        point = view.getYX();
        fwc.showAtLocation(view, Gravity.TOP | Gravity.START, point[0], point[1]);
    }

    public void dismissView(){
        if (fwc != null && view != null) {
            view.dismiss();
        }
    }

}
