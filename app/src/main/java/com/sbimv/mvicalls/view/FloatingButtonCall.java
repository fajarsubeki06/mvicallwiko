package com.sbimv.mvicalls.view;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.diroag.floatingwindows.service.FloatingWindowView;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.icallservices.InCallPresenter;
import com.sbimv.mvicalls.util.Util;

public class FloatingButtonCall extends FloatingWindowView {

    private int [] screen = Util.getScreenSize(getContext());
    private double maxWidthScale = 0.9;
    private FloatingButtonCall mFloatingButton;

    public FloatingButtonCall(Context context) {
        super(context);
    }

    @Override
    public void dismiss() {
        if(mFloatingButton != null){
            mFloatingButton = null;
        }
        super.dismiss();
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.floating_button_call, null, false);
        RelativeLayout mButtonCall = view.findViewById(R.id.rl_button_call);

        int w = (int) (screen[0] * maxWidthScale);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(w, RelativeLayout.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(params);

        mButtonCall.setOnClickListener(v -> {
            try {
                Intent intent = InCallPresenter.getInstance().getInCallIntent(false, false);
                getContext().startActivity(intent);
            }catch (NullPointerException e){
                e.printStackTrace();
            }
        });

        return view;
    }

    public int[] getYX () {
        int screenWidth = screen[0];
        int w = (int) (screenWidth * maxWidthScale);
        int x = (screenWidth - w) / 2;
        //int y = screen[1] / 4;
        int y = w / 2 + 10;
        int[] size = {x, y};
        return size;
    }

}
