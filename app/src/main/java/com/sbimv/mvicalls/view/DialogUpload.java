package com.sbimv.mvicalls.view;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;

/**
 * Created by Hendi on 10/29/17.
 */

public class DialogUpload extends DialogFragment {

    private TextView tvProgress;
    private boolean isMessage = false;
    private String strMessage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
        setStyle(STYLE_NO_FRAME, R.style.TransparentDialogTheme);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View view = inflater.inflate(R.layout.dialog_upload, null, false);
        TextView tv = view.findViewById(R.id.tv_progres);
        LangViewModel model = LangViewModel.getInstance();
        tv.setText(model.getLang().string.preparing);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvProgress = view.findViewById(R.id.tv_progres);
        if (!TextUtils.isEmpty(strMessage)){
            tvProgress.setText(strMessage);
        }
//        if (isMessage && !TextUtils.isEmpty(strMessage) && tvProgress != null){
//            tvProgress.setText(strMessage);
//        }

    }

    public void setMessage(String message){
        if(!TextUtils.isEmpty(message)){
//            tvProgress.setText(message);
            strMessage = message;
        }
    }

    public void setMessageProgress(String message){
        if(!TextUtils.isEmpty(message)){
            isMessage = true;
            strMessage = message;
        }else {
            isMessage = false;
            strMessage = "";
        }
    }

}
