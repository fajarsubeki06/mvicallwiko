package com.sbimv.mvicalls.view;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.diroag.floatingwindows.service.FloatingWindowController;
import com.diroag.floatingwindows.service.FloatingWindowView;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.db.DBHelper;
import com.sbimv.mvicalls.db.LikeTable;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.ProfileItem;
import com.sbimv.mvicalls.services.SyncCommentService;
import com.sbimv.mvicalls.util.CallerVideoManager;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;
import com.sbimv.mvicalls.util.Utils;

import net.protyposis.android.mediaplayer.MediaPlayer;
import net.protyposis.android.mediaplayer.MediaSource;
import net.protyposis.android.mediaplayer.VideoView;
import java.text.DecimalFormat;


public class VideoToneView extends FloatingWindowView {

    @SuppressLint("StaticFieldLeak")
    private static FloatingWindowController fwc;
    @SuppressLint("StaticFieldLeak")
    private static VideoToneView view;
    private final String TAG = "VideoToneView";

    private final int mCurrentRingerMode;
    private final int mCurrentRingerVolume;
    private final int mCurrentMusicVolume;
    private final int mRequiredVolume = 10;

    private int [] screen = Util.getScreenSize(getContext());
    private double maxWidthScale = 0.8;

    // view bind
    // main status layout
    private View photoStatusLayout;
    private View videoStatusLayout;
    // child view
    private VideoView mVideoView;
    private TextView tvName;
    private TextView tvPhone;
    private TextView tvStatus;
    private TextView tvVideoName;
    private TextView tvVideoPhone;
    private ImageView ivUser;
    private TextView tvProfile;

    // Like Action
    private ImageButton btnLike;
    private ImageButton btnReport;

    // variables
    private AudioManager mAudioManager;
    private String msisdn;
    private MediaSource mMediaSource;
    private boolean isIncoming;
    private boolean isPhonestate;
    private boolean wasPlayed;
    private boolean isPreview;
    private SharedPreferences sharedPreferences;
    private Uri uri;
    private String rul;
    private ContactItem own;
    private LangViewModel model;
    private FrameLayout frameLayout;
    private ConstraintLayout constraintLayout;


    public VideoToneView(Context context, String requirementId, String msisdn, boolean isPhonestate,boolean isIncoming, boolean isPreview) {
        super(context);

        if (requirementId == null)
            throw new NullPointerException("requirementId can't be null");

        this.msisdn = msisdn;
        this.mAudioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        this.isIncoming = isIncoming;
        this.isPhonestate = isPhonestate;
        this.isPreview = isPreview;

        this.mCurrentRingerMode = mAudioManager.getRingerMode();
        this.mCurrentMusicVolume =  mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        this.mCurrentRingerVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_RING);

        if (mCurrentRingerVolume > 0){
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, isPreview ? mCurrentMusicVolume:mRequiredVolume, 0);
        }else {
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
        }

    }

    private void resetCurrentRingerMode(){
        if (wasPlayed) {
            wasPlayed = false;
            try {
                VideoToneView.this.mAudioManager.setRingerMode(mCurrentRingerMode);
                VideoToneView.this.mAudioManager.setStreamVolume(AudioManager.STREAM_RING, mCurrentRingerVolume,0);
                VideoToneView.this.mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurrentMusicVolume,0);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void dismiss() {
        resetCurrentRingerMode();
        if(mVideoView != null){
            mVideoView.stopPlayback();
            mVideoView.setVideoSource(null);
            mVideoView = null;
        }
        super.dismiss();
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater) {
        model = LangViewModel.getInstance();
        View view = inflater.inflate(R.layout.view_video_tone, null, false);
        TextView tvLike = view.findViewById(R.id.tvQtyLike);
        TextView tvReport = view.findViewById(R.id.tvQtyReport);
        frameLayout = view.findViewById(R.id.iv_video_big);
        constraintLayout = view.findViewById(R.id.constraint);

        tvLike.setText(model.getLang().string.like);
        tvReport.setText(model.getLang().string.wordingReport);
        own = SessionManager.getProfile(getContext());

        int scWidth = screen[0];
        int scHeight = screen[1];
        if (scWidth > scHeight){
            scWidth = scHeight;
        }

        int w = (int) (scWidth * maxWidthScale);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(w, RelativeLayout.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(params);

        photoStatusLayout = view.findViewById(R.id.layout_photo_status);
        videoStatusLayout = view.findViewById(R.id.layout_video_status);
        tvName = view.findViewById(R.id.tv_name);
        tvPhone = view.findViewById(R.id.tv_phone);
        tvStatus = view.findViewById(R.id.tv_status);
        tvVideoName = view.findViewById(R.id.tv_video_name);
        tvVideoPhone = view.findViewById(R.id.tv_video_phone);
        tvProfile = view.findViewById(R.id.tv_status2);
        ivUser = view.findViewById(R.id.iv_user);

        // Like Action
        btnLike = view.findViewById(R.id.btnLike);
        btnReport = view.findViewById(R.id.btnReport);

        view.findViewById(R.id.iv_close).setOnClickListener(v -> {
            dismiss();
            try {
                GATRacker.getInstance((Application) getContext()).sendEventWithScreen(TrackerConstant.SCREEN_CALLER_WINDOW, TrackerConstant.EVENT_CAT_CALLER_WINDOW, "click close button", own.caller_id);
            }catch (Exception e){
                e.printStackTrace();
            }
        });

        showCallerWindow(view);

        return view;
    }

    @SuppressLint("CheckResult")
    private void showCallerWindow(View root){
        // caller contact
        boolean isDemo = PreferenceUtil.getPref(getContext()).getBoolean(PreferenceUtil.DEMO_VIDEO, false);
        ContactItem item;
        if (isDemo){
            item = SessionManager.getProfile(getContext());
        }else {
            item = ContactDBManager.getInstance(getContext()).getCallerByMSISDN(msisdn);
        }
        // video tone is enable
        boolean isVideoToneEnable = PreferenceUtil
                .getPref(getContext())
                .getBoolean(PreferenceUtil.VIDEO_TONE_ENABLE, true);
        // show caller profile
        tvName.setText(item.getName("Unknown"));
        tvPhone.setText(item.msisdn);
        tvVideoName.setText(item.getName());
        tvVideoPhone.setText(item.msisdn);

        try {
            Glide.with(getContext()).load(item.caller_pic)
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.user_images)).into(ivUser);
        }catch (Exception e){
            e.printStackTrace();
        }

        if(!isIncoming) {

            int ww = (int) (screen[0] * maxWidthScale);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ww, RelativeLayout.LayoutParams.WRAP_CONTENT);
            root.setLayoutParams(params);

            if (own != null){
                String status = item.getStatusCaller();
                for(ProfileItem.Item i: item.profiles.list){

                    if(own.msisdn.equalsIgnoreCase(i.msisdn)){
                        status = i.profile;
                        break;
                    }
                }
                tvStatus.setText(status); // munculin profile sesuai msisdn
            }

        } else if(TextUtils.isEmpty(item.profile_status)){
            tvProfile.setVisibility(View.GONE);
        }else{
            tvProfile.setVisibility(View.GONE);
            tvProfile.setText(item.profile_status);
        }

        if(!TextUtils.isEmpty(item.video_caller) && isVideoToneEnable && isIncoming){ // show video tone only on incoming call
            photoStatusLayout.setVisibility(View.GONE);
            videoStatusLayout.setVisibility(View.VISIBLE);
            rul = item.video_caller;
            uri = CallerVideoManager.getVideoUri(getContext(), rul);

            String source_ratio = item.getRatio();
            setConstraintView(source_ratio, root);

            sharedPreferences = getContext().getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);
            try {
                initVideoView(root, uri);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else{ // show status only
            photoStatusLayout.setVisibility(View.VISIBLE);
            videoStatusLayout.setVisibility(View.GONE);
        }

        getStatusLikers(item.caller_id,item.video_caller);
// ==================================================================================================================================================================
// ========================================================================= Like Action ============================================================================
// ==================================================================================================================================================================


        btnLike.setOnClickListener(v -> {
            selectButton(true);

            String wLike = TextUtils.isEmpty(model.getLang().string.str_wording_like)
                    ?"Liked":model.getLang().string.str_wording_like;

            likeAction(item,"1",wLike);
            try{
                GATRacker.getInstance((Application)getContext()).sendEventWithScreen(TrackerConstant.SCREEN_CALLER_WINDOW,TrackerConstant.EVENT_CAT_CALLER_WINDOW,"click button like",own.caller_id);
            }catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnReport.setOnClickListener(v -> {
            selectButton(false);
            String wReport = TextUtils.isEmpty(model.getLang().string.str_wording_report)
                    ?"Reported...":model.getLang().string.str_wording_report;
            likeAction(item,"0",wReport);
            try{
                GATRacker.getInstance((Application)getContext()).sendEventWithScreen(TrackerConstant.SCREEN_CALLER_WINDOW,TrackerConstant.EVENT_CAT_CALLER_WINDOW,"click button report",own.caller_id);
            }catch (Exception e){
                e.printStackTrace();
            }
        });
    }

    private void setConstraintView (String dimention_ratio, View root){

       if (!TextUtils.isEmpty(dimention_ratio)) {
           if (dimention_ratio.equalsIgnoreCase("9:16") || dimention_ratio.equalsIgnoreCase("4:5")) {
               if (root != null) {
                   double maxWidthScale45 = 0.7;
                   int ww = (int) (screen[0] * maxWidthScale45);
                   RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ww, RelativeLayout.LayoutParams.WRAP_CONTENT);
                   root.setLayoutParams(params);
               }
           }
       }else {
           dimention_ratio = "16:9";

       }

        ConstraintSet set = new ConstraintSet();
        set.clone(constraintLayout);
        set.setDimensionRatio(frameLayout.getId(), dimention_ratio);
        set.applyTo(constraintLayout);


    }

    private void likeAction(ContactItem item, String status_like, String message){
        ContactItem owns = SessionManager.getProfile(getContext());
        if (owns != null) {
            String caller_id = owns.caller_id;
            String id_uploader = item.caller_id;
            //Save database local...
            saveLikeIntoDB(caller_id, id_uploader, status_like, item.video_caller);
            if (!isPhonestate) { // Validate request from phone state....
                syncComment(id_uploader);
            }
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    // Check data status & status send data likers...
    private void getStatusLikers(String... arg) {
        if (!TextUtils.isEmpty(arg[0]) && !TextUtils.isEmpty(arg[1])) {
            String selectQuery = "SELECT * FROM " + LikeTable.Entry.TABLE_NAME + " WHERE caller_id_uploader = '" + arg[0] + "' AND source_video = '" + arg[1] + "'";
            SQLiteDatabase database = DBHelper.getInstance(getContext()).getReadableDatabase();
            if (database != null) {
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor != null && cursor.getCount() > 0) {
                    if (cursor.moveToFirst()) {
                        do {
                            String status = cursor.getString(cursor.getColumnIndex(LikeTable.Entry.COLUMN_NAME_STATUS_COMMENT));
                            String send = cursor.getString(cursor.getColumnIndex(LikeTable.Entry.COLUMN_NAME_SEND_COMMENT));
                            if (send.equals("1")) {
                                if (status.equals("0")) {
                                    selectButton(false);
                                } else if (status.equals("1")) {
                                    selectButton(true);
                                }
                            }
                        } while (cursor.moveToNext());
                        cursor.close();
                    }
                }
            }
        }
    }

    private void selectButton (boolean isLike){
        if (isLike){
            btnLike.setBackground(getContext().getResources().getDrawable(R.drawable.circle_green_line));
            btnReport.setBackground(getContext().getResources().getDrawable(R.drawable.bg_circle));
            btnLike.setEnabled(false);
            btnReport.setEnabled(true);
        }else {
            btnReport.setBackground(getContext().getResources().getDrawable(R.drawable.circle_orange_mail));
            btnLike.setBackground(getContext().getResources().getDrawable(R.drawable.bg_circle));
            btnLike.setEnabled(true);
            btnReport.setEnabled(false);
        }
    }

    private void syncComment(String id_uploader){
        Intent i = new Intent(getContext(), SyncCommentService.class);
        i.putExtra("id_uploader",id_uploader);
        getContext().startService(i);
    }

    private void saveLikeIntoDB(String caller_id, String caller_id_uploader, String status_comment, String source_video){
        if (!TextUtils.isEmpty(caller_id) && !TextUtils.isEmpty(caller_id_uploader) && !TextUtils.isEmpty(status_comment) && !TextUtils.isEmpty(source_video)) {
            ContactDBManager db = ContactDBManager.getInstance(getContext());
            db.insertLikers(caller_id,caller_id_uploader,status_comment, "0",source_video);
        }
    }

// ==================================================================================================================================================================
// ==================================================================================================================================================================
// ==================================================================================================================================================================


    public int[] getXY () {
        int screenWidth = screen[0];
        int screenHeight = screen[1];

        if (screenWidth > screenHeight){
            screenWidth = screenHeight;
        }

        int w = (int) (screenWidth * maxWidthScale);
        int x = (screenWidth - w) / 2;
        //int y = screen[1] / 4;
        int y = w / 2 + 10;
        int[] size = {x, y};
        return size;
    }

    public int[] getTopXY (double maxWidth) {
        int screenWidth = screen[0];
        int screenHeight = screen[1];

        if (screenWidth > screenHeight){
            screenWidth = screenHeight;
        }

        int w = (int) (screenWidth * maxWidth);
        int x = (screenWidth - w) / 2;
        int y = screen[1] / 15;
//        int y = w / 2 - 130;
        int[] size = {x, y};
        return size;
    }


    public int[] getYX () {
        int screenWidth = screen[0];
        int screenHeight = screen[1];

        if (screenWidth > screenHeight){
            screenWidth = screenHeight;
        }

        int w = (int) (screenWidth * maxWidthScale);
        int x = (screenWidth - w) / 2;
        int y = w / 2 + 99;
        int[] size = {x, y};
        return size;
    }

    private void validateRingMode(){
        try {
            if(mCurrentRingerMode == AudioManager.RINGER_MODE_VIBRATE){
                VideoToneView.this.mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, isPreview ? mCurrentMusicVolume : 0, 0);
            }else if (mCurrentRingerMode == AudioManager.RINGER_MODE_SILENT){
                VideoToneView.this.mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, isPreview ? mCurrentMusicVolume : 0, 0);
            } else{
                VideoToneView.this.mAudioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0); // New Update Methode
                new Handler().postDelayed(() -> {
                    if (mAudioManager.getRingerMode() != AudioManager.RINGER_MODE_SILENT || mAudioManager.getRingerMode() != AudioManager.RINGER_MODE_VIBRATE) {
                        VideoToneView.this.mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, isPreview ? mCurrentMusicVolume : mRequiredVolume, 0); // New Update Methode
                    }
                }, 1000);

            }
        }
        catch (SecurityException se){
            Log.i("error", String.valueOf(se));
        }
    }

    private void onlineVideoView(final View root, final Uri mVideoUri, final String url) {
        // force ringtone to silent mode
        mVideoView = root.findViewById(R.id.video_view);
        mVideoView.setOnPreparedListener(vp -> {
            validateRingMode();
            vp.setLooping(true);
        });

        Utils.MediaSourceAsyncCallbackHandler mMediaSourceAsyncCallbackHandler =
                new Utils.MediaSourceAsyncCallbackHandler() {
                    @Override
                    public void onMediaSourceLoaded(MediaSource mediaSource) {
                        try {

                            mVideoView = root.findViewById(R.id.video_view);
                            mVideoView.setVideoURI (mVideoUri);
                            mVideoView.setVisibility (View.VISIBLE);
                            mVideoView.setVideoPath (url);
                            mVideoView.requestFocus ();

                            new Handler().postDelayed(() -> {
                                wasPlayed = true;

                                if (mVideoView != null){
                                    mVideoView.start();
                                }

                            }, 1000);

                            sharedPreferences = getContext().getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putBoolean("online", false);
                            editor.apply();

                        }
                        catch (Exception e){
                            Log.e(TAG,"error loading video", e);
                        }

                    }

                    @Override
                    public void onException(Exception e) {
                        Log.e(TAG, "error loading video", e);
                    }
                };

        if(mMediaSource == null) {
            Utils.uriToMediaSourceAsync(getContext(), mVideoUri, mMediaSourceAsyncCallbackHandler);
        } else {
            mMediaSourceAsyncCallbackHandler.onMediaSourceLoaded(mMediaSource);
        }
    }

    private void initVideoView(final View root, final Uri mVideoUri) throws Exception {
        // init video caller window
        mVideoView = root.findViewById(R.id.video_view);
        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer vp) {
                validateRingMode();
                vp.setLooping(true);
            }
        });
        mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                onlineVideoView(root,uri,rul);
                return true;
            }
        });

        Utils.MediaSourceAsyncCallbackHandler mMediaSourceAsyncCallbackHandler =
                new Utils.MediaSourceAsyncCallbackHandler() {

                    @Override
                    public void onMediaSourceLoaded(MediaSource mediaSource) {
                        try {
                            mMediaSource = mediaSource;
                            mVideoView.setVideoSource(mediaSource);
                            mVideoView.setPlaybackSpeed(1);
                            mVideoView.setZOrderOnTop(false);

                            new Handler().postDelayed(() -> {
                                wasPlayed = true;
                                if (mVideoView != null){
                                    mVideoView.start();
                                }
                            }, 1000);
                        }
                        catch (Exception e){
                            Log.e(TAG,"error loading video", e);
                        }

                    }

                    @Override
                    public void onException(Exception e) {
                        Log.e(TAG, "error loading video", e);
                    }
                };

        if(mMediaSource == null) {
            Utils.uriToMediaSourceAsync(getContext(), mVideoUri, mMediaSourceAsyncCallbackHandler);
        } else {
            mMediaSourceAsyncCallbackHandler.onMediaSourceLoaded(mMediaSource);
        }
    }

    public static VideoToneView showDummyFloatingWindow(Context appContext, String msisdn, boolean isPhonestate,boolean isIncoming, boolean isPreview){
        view = new VideoToneView(appContext, "R7721", msisdn, isPhonestate,isIncoming, isPreview);
        fwc = FloatingWindowController.create(appContext);
        int[] point = view.getXY();
        fwc.showAtLocation(view, Gravity.TOP | Gravity.START, point[0], point[1]);
        return view;
    }

    public static VideoToneView showDummyFloatingWindow(Context appContext, String msisdn, String ratio,  boolean isPhonestate,boolean isIncoming, boolean isPreview){
        int[] point;
        view = new VideoToneView(appContext, "R7721", msisdn, isPhonestate,isIncoming, isPreview);
        fwc = FloatingWindowController.create(appContext);

        if (!TextUtils.isEmpty(ratio)){
            switch (ratio) {
                case "9:16":
                    point = view.getTopXY(0.6);
                    break;
                case "4:5":
                    point = view.getTopXY(0.7);
                    break;
                case "3:4":
                    point = view.getTopXY(0.8);
                    break;
                default:
                    point = view.getXY();
                    break;
            }
        }else {
            point = view.getXY();
        }

        fwc.showAtLocation(view, Gravity.TOP | Gravity.START, point[0], point[1]);
        return view;

    }

}