package com.sbimv.mvicalls.view;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.sbimv.mvicalls.util.CustomFontAttr;
import com.sbimv.mvicalls.util.FontCache;

/**
 * Created by Hendi on 10/22/15.
 */
public class XTextView extends AppCompatTextView {
    private AttributeSet attrs;

    public XTextView(Context context) {
        super(context);

        if(!isInEditMode())
            init();
    }

    public XTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.attrs = attrs;
        if(!isInEditMode())
            init();
    }

    public XTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.attrs = attrs;
        if(!isInEditMode())
            init();
    }

    private void init(){
        String fontName = CustomFontAttr.getSelectedFont(getContext(), attrs);
        Typeface typeface = FontCache.get(fontName, getContext());

        if(android.os.Build.VERSION.SDK_INT<android.os.Build.VERSION_CODES.HONEYCOMB ||
                android.os.Build.VERSION.SDK_INT>android.os.Build.VERSION_CODES.HONEYCOMB_MR2){
            setTypeface(typeface);
        }
    }

}
