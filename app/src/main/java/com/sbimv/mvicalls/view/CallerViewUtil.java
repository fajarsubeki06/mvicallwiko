package com.sbimv.mvicalls.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;

import com.diroag.floatingwindows.service.FloatingWindowController;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.pojo.ContactItem;


public class CallerViewUtil {

    private final static String VIDEO_VIEW_ID = "MVC2017";
    // single instance
    private static CallerViewUtil util;

    // var
    private Context appContext;
    private FloatingWindowController fwc;
    private VideoToneView view;


    public static CallerViewUtil getInstance(Context _appContext){
        if(util == null){
            util = new CallerViewUtil(_appContext);
        }
        return util;
    }

    private CallerViewUtil(Context ctx){
        appContext = ctx;
        fwc = FloatingWindowController.create(ctx.getApplicationContext());
    }

    public void showView(String msisdn, boolean isPhonestate,boolean isIncoming){
        int[] point;
        if(!TextUtils.isEmpty(msisdn)) {
            view = new VideoToneView(appContext, VIDEO_VIEW_ID, msisdn, isPhonestate,isIncoming,false);
            if (isIncoming) {

                ContactItem item = ContactDBManager.getInstance(appContext).getCallerByMSISDN(msisdn);
                if (item != null) {
                    String strMetaData = item.getRatio();
                    if (!TextUtils.isEmpty(strMetaData)) {
                        switch (strMetaData) {
                            case "9:16":
                                point = view.getTopXY(0.6);
                                break;
                            case "4:5":
                                point = view.getTopXY(0.7);
                                break;
                            case "3:4":
                                point = view.getTopXY(0.8);
                                break;
                            default:
                                point = view.getXY();
                                break;
                        }
                    }else {
                        point = view.getXY();
                    }
                }else {
                    point = view.getXY();
                }

            }else {
                point = view.getYX();
            }
            fwc.showAtLocation(view, Gravity.TOP | Gravity.START, point[0], point[1]);
        }
    }

    public void dismissView(){
        if (fwc != null && view != null) {
            view.dismiss();
        }
    }

}