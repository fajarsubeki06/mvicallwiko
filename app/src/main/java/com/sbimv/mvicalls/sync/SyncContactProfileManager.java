package com.sbimv.mvicalls.sync;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.CallerVideoManager;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.SessionManager;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class SyncContactProfileManager {

    static final String TAG = "SyncContactProfileManager";
    static SyncContactProfileManager instance;
    private SharedPreferences sharedPreferences;
    private boolean uploadCont = false;

    // private constructor
    private SyncContactProfileManager(Context applicationContext){
        this.applicationContext = applicationContext;
    }

    // variables
    boolean isProcessing;
    Context applicationContext;
    Set<String> contactsSet = new HashSet<>();
    ContactItem own;

    public static SyncContactProfileManager getInstance(Context appContext){
        if(instance == null)
            instance = new SyncContactProfileManager (appContext);
        return instance;
    }

    public boolean isSynchronizing(){
        return isProcessing;
    }

    public void firstsync(){
        if(isProcessing)
            return;

        LangViewModel model = LangViewModel.getInstance();
        String download = TextUtils.isEmpty(model.getLang().string.str_set_up_profile_vid_profile)
                ? "Downloading Video Profile..."
                :  model.getLang().string.str_set_up_profile_vid_profile;

        SyncUtils.showNotificationProgress(applicationContext, download);
        isProcessing = true; // set processing flag
        Intent it = new Intent("com.sbimv.mvicalls.START_PROGRESS");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(it);

        own = SessionManager.getProfile(applicationContext);

        downloadProfileVideo();

        isProcessing = false;
        SyncUtils.hideNotificationProgress(applicationContext);

        Intent il = new Intent("com.sbimv.mvicalls.DOWNLOAD_VIDEO");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(il);

    }

    public void sync(){
        if(isProcessing)
            return;

        LangViewModel model = LangViewModel.getInstance();
        String download = TextUtils.isEmpty(model.getLang().string.str_set_up_profile_vid_profile)
                ? "Downloading Video Profile..."
                :  model.getLang().string.str_set_up_profile_vid_profile;

        SyncUtils.showNotificationProgress(applicationContext, download);
        isProcessing = true; // set processing flag

        own = SessionManager.getProfile(applicationContext);

        downloadProfileVideo();

        isProcessing = false;
        SyncUtils.hideNotificationProgress(applicationContext);

        Intent il = new Intent("com.sbimv.mvicalls.UPLOAD_VIDEO");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(il);

    }

    private void downloadProfileVideo() {
        if (!TextUtils.isEmpty(own.video_caller)) {
            Log.i("download_video", own.video_caller);
            Call<ResponseBody> call = ServicesFactory.getService().downloadVideo(own.video_caller);
            try {
                Response<ResponseBody> response = call.execute();
                FileUtil.writeResponseBodyToDisk(response.body(),
                        CallerVideoManager.getDisPath(applicationContext),
                        CallerVideoManager.generateUniversalName("current_video.mp4"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
