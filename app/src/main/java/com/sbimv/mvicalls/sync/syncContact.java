package com.sbimv.mvicalls.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by admin on 1/11/2018.
 */

class syncContact extends AsyncTask<List<String>, Integer, String> {
    private syncINTERCASE INTERFACE;
    private List<String> contacts;
    private ContactItem own;
    private Context appContext;
    private final int batchnumber = 20;
    private Context context;

    public interface  syncINTERCASE {
        void onStart();
        void onFinish(List<String> data);
    }

    public syncContact(List<String> contacts, syncINTERCASE iface, Context con, ContactItem own) {
        this.contacts = contacts;
        this.INTERFACE = iface;
        this.own = own;
        this.appContext = con;
    }

    @Override
    protected void onPreExecute() {
        INTERFACE.onStart();
    }

    @Override
    protected String doInBackground(List<String>[] lists) {
        return synchronize();
    }

    @Override
    protected void onPostExecute(String s) {
        INTERFACE.onFinish(contacts);
    }

    private String synchronize() {
        if(this.contacts.isEmpty()) return null;

        // set batch number
        int setSize = this.contacts.size();
        int div = setSize / batchnumber;
        int mod = setSize % batchnumber;
        int totalBatch = div + (mod > 0 ? 1 : 0);

        for(int currentBatch=0; currentBatch<totalBatch; currentBatch++){
            String json = getJsonArrayBatch(currentBatch, totalBatch, this.contacts);
            try {
                getUsers(json);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return "00";
    }

    private void getUsers(String msisdns) throws IOException {
        Log.i("", "Send data to backend server");
        String uid = this.own.caller_id;
        Call<APIResponse<List<ContactItem>>> call = ServicesFactory.getService().getContact(this.own.caller_id, msisdns);
        Response<APIResponse<List<ContactItem>>> response = call.execute();

        if(response.isSuccessful() && response.body().isSuccessful()){
            List<ContactItem> contacts =  response.body().data;
            if(contacts.size() > 0){
                saveIntoDB(contacts);
            }
        }
    }

    private void saveIntoDB(List<ContactItem> contacts){

        own = SessionManager.getProfile(this.appContext);

        Log.i("", "Save data to local database");
        ContactDBManager db = ContactDBManager.getInstance(this.appContext);
        for(ContactItem item: contacts){

            String ctMsisdn = item.msisdn;
            String prMsisdn = own.msisdn;

            if (!ctMsisdn.equals(prMsisdn)){
                db.insertContact(item);
            }

        }
    }

    private String getJsonArrayBatch(int bacth, int batchTotal, List<String> list) {
        int startIdx;
        int endIdx;

        // set start and end index
        if (batchTotal > 1) {
            // set start index
            if (bacth == 0)
                startIdx = 0;
            else
                startIdx = bacth * batchnumber;

            // set end index
            if (bacth == batchTotal - 1)
                endIdx = contacts.size() - 1;
            else
                endIdx = ((bacth + 1) * batchnumber) - 1;
        } else {
            startIdx = 0;
            endIdx = contacts.size() - 1;
        }

        Log.d("", startIdx + " : " + endIdx);

        List<String> batchList = new ArrayList<>();
        for (int i = startIdx; i <= endIdx; i++) {
            String temp = list.get(i);
            batchList.add(temp);
        }

        String json = new Gson().toJson(batchList, new TypeToken<List<String>>() {
        }.getType());
        return json;
    }
}