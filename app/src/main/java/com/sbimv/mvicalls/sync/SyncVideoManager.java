package com.sbimv.mvicalls.sync;

import android.content.Context;
import android.content.Intent;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.sbimv.mvicalls.db.VideoDBManager;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.CallerVideoManager;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Hendi on 11/8/17.
 * Edit by Yudha Pratama Putra 25/09/18
 */

public class SyncVideoManager {

    static SyncVideoManager instance;
    Context applicationContext;
    private DatabaseReference mDatabase;
    boolean isProcessing;
    private ContactItem own;
    VideoDBManager videoDB;

    private SyncVideoManager(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public static SyncVideoManager getInstance(Context appContext) {
        if (instance == null) instance = new SyncVideoManager(appContext);
        return instance;
    }

    public void sync(String... arg) {
        if (isProcessing) return;

        own = SessionManager.getProfile(applicationContext);
        isProcessing = true;

        getVideoUser();

        isProcessing = false;
        hideLoading();
        SyncUtils.setLastSync(applicationContext);

    }

    private void getVideoUser() {
    }

    private void hideLoading(){
        PreferenceUtil.getEditor(applicationContext)
                .putBoolean(PreferenceUtil.SYNC_LOADING, false).commit();
        //Preference sync experience...
        PreferenceUtil.getEditor(applicationContext)
                .putBoolean(PreferenceUtil.SYNC_EXPERIENCE, true).commit();

        Intent ol = new Intent("com.sbimv.mvicalls.HIDE");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(ol);
    }

    private void downloadAllVideo(List<ContactItem> contacts){
        // show done sync message
        Log.i("LoadStatus","start...");

        File file;
        for(ContactItem item: contacts){
            if(!TextUtils.isEmpty(item.video_caller)){

                // skip download video file if exist (added by Hendi, 29-08-2018)
                file = new File(
                        CallerVideoManager.getDirPath(applicationContext),
                        CallerVideoManager.generateFileName(item.video_caller));

                if(file.exists()) {
                    continue;
                }

                // download video file if no exist
                Call<ResponseBody> call = ServicesFactory.getService().downloadVideo(item.video_caller);
                try {
                    Response<ResponseBody> response = call.execute();
                    FileUtil.writeResponseBodyToDisk(
                            response.body(),
                            CallerVideoManager.getDirPath(applicationContext),
                            CallerVideoManager.generateFileName(item.video_caller));
                    // insert or update to video table (added by Hendi, 29-08-2018)
                    file = new File(
                            CallerVideoManager.getDirPath(applicationContext),
                            CallerVideoManager.generateFileName(item.video_caller));
                    videoDB.insertOrUpdateVideo(item.caller_id, file.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        Log.i("LoadStatus","done...");
    }

}
