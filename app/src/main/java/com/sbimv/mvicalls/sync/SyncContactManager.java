package com.sbimv.mvicalls.sync;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.db.VideoDBManager;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.CallerVideoManager;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Yudha Pratama Putra on 04/07/18.
 */

public class SyncContactManager {

    private static final String TAG = "SyncContactManager";
    private static SyncContactManager instance;
    private Cursor cursor;
    private int countUser = 0;
    private String nameUser;
    private LangViewModel model = LangViewModel.getInstance();
    private int status = 0;

    // private constructor
    private SyncContactManager(Context applicationContext) {
        this.applicationContext = applicationContext;
        this.videoDB = VideoDBManager.getInstance(applicationContext);
    }

    // variables
    private boolean isProcessing;
    private Context applicationContext;
    private Set<String> contactsSet = new HashSet<>();
    private ContactItem own;
    private VideoDBManager videoDB;

    private int batchNumber = 20;
    private int currentBatch = 0;
    private int totalBatch = 0;

    //Method for call contact manager in another class
    public static SyncContactManager getInstance(Context appContext) {
        if (instance == null) instance = new SyncContactManager(appContext);
        return instance;
    }

    public boolean isSynchronizing() {
        return isProcessing;
    }

    public void sync() {
        if (isProcessing) return;

        // show start sync message
//        SyncUtils.showNotificationProgress(applicationContext, "Contact sync...");

        // set own data
        own = SessionManager.getProfile(applicationContext);
        if (own == null) return;
        // show start sync message
        showLoading();
        isProcessing = true; // set processing flag
        // Generate token
        generateTokenFcm();
        // load contact and add all into set list
//        loadContacts();
//        selectContactNumber();
        getContactList();
        // sync with contact batch
        syncBatch();
        // clear sync flag data
        clearData();
        // notify all receiverNetwork
        Intent i = new Intent("com.sbimv.mvicalls.UPDATE_FRIEND");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(i);
        // set last sync time
        SyncUtils.setLastSync(applicationContext);
        // show done sync message
        hideLoading();
        isProcessing = false;
        Log.i("LoadStatus", "done...");

        // show done sync message
        // SyncUtils.hideNotificationProgress(applicationContext);

        if (countUser > 0) { // validate new user....
            if (countUser == 1) {
                if (!TextUtils.isEmpty(nameUser)) {
                    SyncUtils.showStatusNotification(applicationContext, "contact_tab_activity", model.getLang().string.wordingTitle, nameUser + " " + model.getLang().string.notifAddUser);
                } else {
                    SyncUtils.showStatusNotification(applicationContext, "contact_tab_activity", model.getLang().string.wordingTitle, countUser + " " + model.getLang().string.wordAddNewUser);
                }
            } else {
                SyncUtils.showStatusNotification(applicationContext, "contact_tab_activity", model.getLang().string.wordingTitle, countUser + " " + model.getLang().string.wordAddNewUser);
            }
        }
        countUser = 0;
        nameUser = "";

    }

    //*
    // Send Broadcast Show
    //*
    private void showLoading() {
        PreferenceUtil.getEditor(applicationContext).putBoolean(PreferenceUtil.SYNC_LOADING, true).commit();
        Intent il = new Intent("com.sbimv.mvicalls.SHOW");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(il);
    }

    //*
    // Send Broadcast Hide
    //*
    private void hideLoading() {
        PreferenceUtil.getEditor(applicationContext).putBoolean(PreferenceUtil.SYNC_LOADING, false).commit();
        //Preference sync experience...
        PreferenceUtil.getEditor(applicationContext).putBoolean(PreferenceUtil.SYNC_EXPERIENCE, true).commit();

        Intent ol = new Intent("com.sbimv.mvicalls.HIDE");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(ol);
    }

    //*
    // Get List user from API
    //*
    private void getUsers(String msisdns) throws IOException {
        String tokenfcm = PreferenceUtil.getPref(applicationContext).getString(PreferenceUtil.TOKEN_FCM, "");
        Call<APIResponse<List<ContactItem>>> call = ServicesFactory.getService().getSyncContact(own.caller_id, msisdns, tokenfcm);
        Response<APIResponse<List<ContactItem>>> response = call.execute();
        if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
            List<ContactItem> contacts = response.body().data;
            if (contacts.size() > 0) {
                saveIntoDB(contacts); // save to db local
            }
        }
    }

    //*
    // Generate Token Firebase and save to local
    //
    private void generateTokenFcm() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                String token = Objects.requireNonNull(task.getResult()).getToken();
                PreferenceUtil.getEditor(applicationContext).putString(PreferenceUtil.TOKEN_FCM, token).commit();
            }
        });
    }

    //*
    // Get MSISDN FROM Local DB
    //
    private static boolean getCallerID(String... arg) {
        boolean found = false;
        try {
            ContactDBManager contactDBManager = new ContactDBManager(instance.applicationContext);
            Cursor data = contactDBManager.getCallerMSISDN(arg[0]);
            if (data != null) {
                found = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return found;
    }

    //*
    // Save contact to db local
    //
    private void saveIntoDB(List<ContactItem> contacts) {
        ContactDBManager db;
        try {
            db = ContactDBManager.getInstance(applicationContext);
        } catch (Exception e) {
            return;
        }

        for (ContactItem item : contacts) {
            if (item != null) {

                String name = item.name;
                if (TextUtils.isEmpty(name)) { // validate empty name
                    String phonenum = item.msisdn;
                    item.name = "~ " + selectionContactDetail(phonenum);
                }

                String caller_msisdn = TextUtils.isEmpty(item.msisdn)?"":item.msisdn;
                if (!getCallerID(caller_msisdn)) { // check new user in database....
                    countUser += 1;
                    nameUser = TextUtils.isEmpty(item.name)?"":item.name;
                }

                db.insertContact(item);
                Intent i = new Intent("com.sbimv.mvicalls.UPDATE_FRIEND");
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(i); // send broadcast update friend

            }
        }
        downloadAllVideo(contacts); // download all video friend
    }

    //*
    // Download all video friend
    //*
    private void downloadAllVideo(List<ContactItem> contacts) {
        // show done sync message
        Log.i("LoadStatus", "start...");

        File file;
        for (ContactItem item : contacts) {
            if (!TextUtils.isEmpty(item.video_caller)) {
                // skip download video file if exist (added by Hendi, 29-08-2018)
                file = new File(CallerVideoManager.getDirPath(applicationContext), CallerVideoManager.generateFileName(item.video_caller));

                if (file.exists()) {
                    continue;
                }

                // download video file if no exist

                Call<ResponseBody> call = ServicesFactory.getService().downloadVideo(item.video_caller);
                try {
                    Response<ResponseBody> response = call.execute();
                    FileUtil.writeResponseBodyToDisk(response.body(), CallerVideoManager.getDirPath(applicationContext), CallerVideoManager.generateFileName(item.video_caller));
                    // insert or update to video table (added by Hendi, 29-08-2018)
                    file = new File(CallerVideoManager.getDirPath(applicationContext), CallerVideoManager.generateFileName(item.video_caller));
                    videoDB.insertOrUpdateVideo(item.caller_id, file.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        Log.i("LoadStatus", "done...");
    }

    //*
    // Get array from contact list size
    //*
    private String getJsonArrayBatch(int bacth, int batchTotal, List<String> list) {
        int startIdx;
        int endIdx;

        // set start and end index
        if (batchTotal > 1) {
            // set start index
            if (bacth == 0) startIdx = 0;
            else startIdx = bacth * batchNumber;

            // set end index
            if (bacth == batchTotal - 1) endIdx = contactsSet.size() - 1;
            else endIdx = ((bacth + 1) * batchNumber) - 1;
        } else {
            startIdx = 0;
            endIdx = contactsSet.size() - 1;
        }

        Log.d(TAG, startIdx + " : " + endIdx);

        List<String> batchList = new ArrayList<>();
        for (int i = startIdx; i <= endIdx; i++) {
            String temp = list.get(i);
            batchList.add(temp);
        }

        String json = new Gson().toJson(batchList, new TypeToken<List<String>>() {}.getType());
        if (TextUtils.isEmpty(json))
            json = "";

        return json;

    }

    //*
    // Clear data contact
    //*
    private void clearData() {
        contactsSet.clear();
        totalBatch = 0;
        currentBatch = 0;
    }

    //*
    // get select contact from phone book
    //*
    private void getContactList() {
        String[] PROJECTION = new String[] {
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER
        };

        ContentResolver cr = applicationContext.getContentResolver();
        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION, null, null, null);
        if (cursor != null) {
            if (cursor.getCount() >0) {
                try {
                    while (cursor.moveToNext()) {
                        String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        if (!TextUtils.isEmpty(number)) {
                            String prefix = AppPreference.getPreference(instance.applicationContext, AppPreference.PREFIX);
                            String formatted = Util.formatMSISDNREG(number, prefix);
                            String phonenumber = Util.formatMSISDNREG(own.msisdn, prefix);
                            if (!TextUtils.isEmpty(formatted)) { //validate format prefix empty
                                if (!formatted.equals(phonenumber)) {
                                    contactsSet.add(formatted); // add to db local
                                }
                            }
                        }
                    }
                } finally {
                    cursor.close();
                }
            }
        }
    }

    //*
    // Sync contact and set progress
    //*
    private void syncBatch() {

        if (contactsSet.size() == 0)
            return;

        final List<String> list = new ArrayList<>(contactsSet);
        // set batch number
        int setSize = contactsSet.size();
        int div = setSize / batchNumber;
        int mod = setSize % batchNumber;
        totalBatch = div + (mod > 0 ? 1 : 0);

        for (currentBatch = 0; currentBatch < totalBatch; currentBatch++) {
            String json = getJsonArrayBatch(currentBatch, totalBatch, list);
            try {
                setPercentageProgress(totalBatch);
                getUsers(json);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //*
    // Percentage progress
    //*
    private void setPercentageProgress(int count){
        status += 1;
        if (status == count) {
            status = 100;
            sendBroadcastProgress(status);
            status = 0;
            return;
        }

        int result10 = Math.round( 10 * count / 100 );
        int result30 = Math.round( 30 * count / 100 );
        int result60 = Math.round( 60 * count / 100 );
        int result90 = Math.round( 90 * count / 100 );

        if (status == result10){
            sendBroadcastProgress(10); // send brodcast progress
        }else if (status == result30){
            sendBroadcastProgress(30);
        }else if (status == result60){
            sendBroadcastProgress(60);
        }else if (status == result90){
            sendBroadcastProgress(90);
        }


    }

    //*
    // Send Broadcast Progress
    //*
    private void sendBroadcastProgress(int progress){
        Intent intentVerApp = new Intent("com.sbimv.mvicalls.SHOW");
        intentVerApp.putExtra("from", "percent_progress");
        intentVerApp.putExtra("percent", progress);
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intentVerApp);
    }


    // Validate when the username is empty filled in the name of the phone book contact... - added by Yudha Pratama Putra, 06 November 2018
    private String selectionContactDetail(String msisdn) {
        ContentResolver cr = applicationContext.getContentResolver();
        String selection = "(" + ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + msisdn + "%') OR (" + ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER + " LIKE '%" + msisdn + "%')";
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER};
        try {
            cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, selection, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    if (!TextUtils.isEmpty(name)) {
                        return name;
                    }
                }
            }
            cursor.close();
        }
        return "Unknown";

    }

}