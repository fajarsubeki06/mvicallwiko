package com.sbimv.mvicalls.sync;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.services.DownloadVideoService;
import com.sbimv.mvicalls.util.CallerVideoManager;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SyncCuration {

    private Context applicationContext;
    private static SyncCuration instance;
    private boolean isProcessing;
    protected static LangViewModel model;
    private String sDownload = "";

    private SyncCuration(Context applicationContext){
        this.applicationContext = applicationContext;
    }

    public static SyncCuration getInstance(Context appContext){
        if(instance == null)
            instance = new SyncCuration (appContext);
        return instance;
    }

    public boolean isSynchronizing(){
        return isProcessing;
    }

    public void sync(){

        if(isProcessing)
            return;

        LangViewModel modelLang = LangViewModel.getInstance();
        String download = TextUtils.isEmpty(modelLang.getLang().string.str_set_up_profile_vid_profile)
                ? "Downloading Video Profile..."
                :  modelLang.getLang().string.str_set_up_profile_vid_profile;
        SyncUtils.showNotificationProgress(applicationContext, download);

        isProcessing = true; // set processing flag

        ContactItem own = SessionManager.getProfile(applicationContext);
        if (own != null){
            oauthToken(own);
        }

        isProcessing = false;
        SyncUtils.hideNotificationProgress(applicationContext);

    }

    private void oauthToken(ContactItem own) {
        if (own != null){
            String msisdn = own.msisdn;
            if (!TextUtils.isEmpty(msisdn)) {

                Call<APIResponse<ContactItem>> call = ServicesFactory.getService().getProfile(msisdn);
                call.enqueue(new Callback<APIResponse<ContactItem>>() {
                    @Override
                    public void onResponse(@NotNull Call<APIResponse<ContactItem>> call, @NotNull Response<APIResponse<ContactItem>> response) {
                        if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                            ContactItem item = response.body().data;
                            item.profiles = null;// Untuk syncronize firebase
                            SessionManager.saveProfile(applicationContext, item);

                            if (!SyncContactProfileManager.getInstance(applicationContext).isSynchronizing()) {
                                PreferenceUtil.getEditor(applicationContext)
                                        .putBoolean(PreferenceUtil.FIRST_SYNC_DEFAULT_VIDEO, false).commit();
                                Intent i = new Intent(applicationContext, DownloadVideoService.class);
                                applicationContext.startService(i);

                            }
                            saveContactFirebase(own);
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<APIResponse<ContactItem>> call, @NotNull Throwable t) {
                    }
                });

            }
        }

    }

    private void saveContactFirebase(ContactItem own) {
        if (own != null) {
            String callerId = own.caller_id;
            if (!TextUtils.isEmpty(own.caller_id)) {
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                mDatabase.child("MviContact").child(callerId).setValue(own).addOnSuccessListener(aVoid ->
                        Log.i("firebase_db_report", "Berhasil menyimpan data"))
                        .addOnFailureListener(e ->
                                Log.i("firebase_db_report", "Gagal menyimpan data"));
            }
        }

    }

}
