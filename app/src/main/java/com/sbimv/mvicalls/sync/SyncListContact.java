package com.sbimv.mvicalls.sync;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.db.VideoDBManager;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.CallerVideoManager;
import com.sbimv.mvicalls.util.ContactsQuery;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class SyncListContact {

    private Context applicationContext;
    private static SyncListContact instance;
    private boolean isProcessing;

    private Set<String> contactsSet = new HashSet<>();
    private int batchNumber = 200;
    private ContactItem own;
    private VideoDBManager videoDB;

    private int currentBatch = 0;
    private int totalBatch = 0;

    private int countUser = 0;
    private String nameUser;
    private LangViewModel model = LangViewModel.getInstance();
    private ArrayList<ContactItem> listVideo = new ArrayList<ContactItem>();


    private SyncListContact(Context applicationContext) {
        this.applicationContext = applicationContext;
        this.videoDB = VideoDBManager.getInstance(applicationContext);
    }

    public static SyncListContact getInstance(Context appContext) {
        if (instance == null) instance = new SyncListContact(appContext);
        return instance;
    }

    public boolean isSynchronizing() {
        return isProcessing;
    }

    public void sync() {
        if (isProcessing) return;

        own = SessionManager.getProfile(applicationContext);
        if (own == null) return;

        showLoading();
        isProcessing = true; // set processing flag

        loadContacts();

        syncBatch();

        clearData();

        downloadAllVideo();

        Intent i = new Intent("com.sbimv.mvicalls.UPDATE_FRIEND");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(i);

        SyncUtils.setLastSync(applicationContext);
        hideLoading();
        isProcessing = false;

        if (countUser > 0) { // validate new user....
            if (countUser == 1) {
                if (!TextUtils.isEmpty(nameUser)) {
                    SyncUtils.showStatusNotification(applicationContext, "contact_tab_activity", model.getLang().string.wordingTitle, nameUser + " " + model.getLang().string.notifAddUser);
                } else {
                    SyncUtils.showStatusNotification(applicationContext, "contact_tab_activity", model.getLang().string.wordingTitle, countUser + " " + model.getLang().string.wordAddNewUser);
                }
            } else {
                SyncUtils.showStatusNotification(applicationContext, "contact_tab_activity", model.getLang().string.wordingTitle, countUser + " " + model.getLang().string.wordAddNewUser);
            }
        }
        countUser = 0;
        nameUser = "";

    }

    private void loadContacts() {
        ContentResolver cr = applicationContext.getContentResolver();
        String[] PROJECTION = ContactsQuery.PROJECTION;

        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION, null, null, null);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    String lookUpKey = "";
                    try {
                        lookUpKey = cursor.getString(ContactsQuery.LOOKUP_KEY);
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                    if (!TextUtils.isEmpty(lookUpKey)) loadContactDetail(lookUpKey);
                }
            }
            cursor.close();
        }
    }

    private void loadContactDetail(String lookUpKey) {

        ContentResolver cr = applicationContext.getContentResolver();
        String[] COLS = {ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, COLS,
                ContactsContract.Data.LOOKUP_KEY + " = ?", new String[]{lookUpKey}, null);
        String prefix = AppPreference.getPreference(instance.applicationContext, AppPreference.PREFIX);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {

                    if (own != null) {

                        String strMsisdn = own.msisdn;
                        String phone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        if (!TextUtils.isEmpty(strMsisdn) && !TextUtils.isEmpty(phone) && !TextUtils.isEmpty(prefix)) {
                            String formatted = Util.formatMSISDNREG(phone, prefix);
                            String phonenumber = Util.formatMSISDNREG(strMsisdn, prefix);

                            if (!TextUtils.isEmpty(formatted) && !TextUtils.isEmpty(phonenumber)) {
                                if (!formatted.equals(phonenumber)) {
                                    contactsSet.add(formatted);
                                }
                            }
                        }

                    }
                }
            }
            cursor.close();
        }

    }

    private void syncBatch() {
        if (contactsSet.size() == 0) return;
        ArrayList<String> list = new ArrayList<>(contactsSet);
        // set batch number
        int setSize = contactsSet.size();
        int div = setSize / batchNumber;
        int mod = setSize % batchNumber;
        totalBatch = div + (mod > 0 ? 1 : 0);

        for (currentBatch = 0; currentBatch < totalBatch; currentBatch++) {
            String json = getJsonArrayBatch(currentBatch, totalBatch, list);
            try {
                getUsers(json);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getJsonArrayBatch(int bacth, int batchTotal, List<String> list) {
        int startIdx;
        int endIdx;

        // set start and end index
        if (batchTotal > 1) {
            // set start index
            if (bacth == 0) startIdx = 0;
            else startIdx = bacth * batchNumber;

            // set end index
            if (bacth == batchTotal - 1) endIdx = contactsSet.size() - 1;
            else endIdx = ((bacth + 1) * batchNumber) - 1;
        } else {
            startIdx = 0;
            endIdx = contactsSet.size() - 1;
        }

        List<String> batchList = new ArrayList<>();
        for (int i = startIdx; i <= endIdx; i++) {
            String temp = list.get(i);
            if (!TextUtils.isEmpty(temp)) {
                batchList.add(temp);
            }
        }

        String json = new Gson().toJson(batchList, new TypeToken<List<String>>() {
        }.getType());
        return json;

    }

    private void getUsers(String msisdns) throws IOException {
        String tokenfcm = PreferenceUtil.getPref(applicationContext).getString(PreferenceUtil.TOKEN_FCM, "");
        if (own != null) {
            String caller_id = own.caller_id;
            if (!TextUtils.isEmpty(tokenfcm) && !TextUtils.isEmpty(caller_id)) {
                Call<APIResponse<List<ContactItem>>> call = ServicesFactory.getService().getSyncContact(caller_id, msisdns, tokenfcm);
                Response<APIResponse<List<ContactItem>>> response = call.execute();
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    List<ContactItem> contacts = response.body().data;
                    if (contacts.size() > 0) {
                        saveIntoDB(contacts);
                    }
                }
            }
        }
    }

    private void saveIntoDB(List<ContactItem> contacts) {

        ContactDBManager db = ContactDBManager.getInstance(applicationContext);
        for (ContactItem item : contacts) {
            if (item != null) {

                String name = item.name;
                if (TextUtils.isEmpty(name)) {
                    String phonenum = item.msisdn;
                    try {
                        item.name = "~ " + selectionContactDetail(phonenum);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                String caller_msisdn = TextUtils.isEmpty(item.msisdn)?"":item.msisdn;
                if (!getCallerID(caller_msisdn)) { // check new user in database....
                    countUser += 1;
                    nameUser = TextUtils.isEmpty(item.name)?"":item.name;
                }

                db.insertContact(item);
            }
        }

        Intent i = new Intent("com.sbimv.mvicalls.UPDATE_FRIEND");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(i);
        listVideo.addAll(contacts);

    }

    private static boolean getCallerID(String... arg) {
        boolean found = false;
        try {
            ContactDBManager contactDBManager = new ContactDBManager(instance.applicationContext);
            Cursor data = contactDBManager.getCallerMSISDN(arg[0]);
            if (data != null) {
                found = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return found;
    }

    // Validate when the username is empty filled in the name of the phone book contact... - added by Yudha Pratama Putra, 06 November 2018
    private String selectionContactDetail(String msisdn) throws Exception {
        ContentResolver cr = applicationContext.getContentResolver();
        String selection = "(" + ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + msisdn + "%') OR (" + ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER + " LIKE '%" + msisdn + "%')";
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER};

        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, selection, null, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    if (!TextUtils.isEmpty(name)) {
                        return name;
                    }
                }
            }
            cursor.close();
        }
        return "Unknown";

    }

    private void downloadAllVideo() {

        File file;
        for (ContactItem item : listVideo) {
            if (!TextUtils.isEmpty(item.video_caller)) {
                // skip download video file if exist (added by Hendi, 29-08-2018)
                file = new File(CallerVideoManager.getDirPath(applicationContext), CallerVideoManager.generateFileName(item.video_caller));

                if (file.exists()) {
                    continue;
                }
                // download video file if no exist
                Call<ResponseBody> call = ServicesFactory.getService().downloadVideo(item.video_caller);
                try {
                    Response<ResponseBody> response = call.execute();
                    FileUtil.writeResponseBodyToDisk(response.body(), CallerVideoManager.getDirPath(applicationContext), CallerVideoManager.generateFileName(item.video_caller));
                    // insert or update to video table (added by Hendi, 29-08-2018)
                    file = new File(CallerVideoManager.getDirPath(applicationContext), CallerVideoManager.generateFileName(item.video_caller));
                    videoDB.insertOrUpdateVideo(item.caller_id, file.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }

    private void clearData() {
        contactsSet.clear();
        totalBatch = 0;
        currentBatch = 0;
    }

    private void showLoading() {
        PreferenceUtil.getEditor(applicationContext).putBoolean(PreferenceUtil.SYNC_LOADING, true).commit();
        Intent il = new Intent("com.sbimv.mvicalls.SHOW");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(il);
    }

    private void hideLoading() {
        PreferenceUtil.getEditor(applicationContext).putBoolean(PreferenceUtil.SYNC_LOADING, false).commit();
        //Preference sync experience...
        PreferenceUtil.getEditor(applicationContext).putBoolean(PreferenceUtil.SYNC_EXPERIENCE, true).commit();

        Intent ol = new Intent("com.sbimv.mvicalls.HIDE");
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(ol);
    }

}
