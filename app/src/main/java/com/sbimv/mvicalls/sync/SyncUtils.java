package com.sbimv.mvicalls.sync;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.SplashActivity;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.services.SyncContactServices;
import com.sbimv.mvicalls.util.PermissionUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.ProtectPermission;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class SyncUtils {

    public static final String SYNC_CONTACT_PREFERENCE = "syncContact";
    public static final String SYNC_CONTACT_STATUS = "sync_status";
    private static long SYNC_TRIGGERED_AT = 6 * 60 * 60 * 1000; // min * sec * millis
    private static long SYNC_INTERVAL = 6 * 60 * 60 * 1000; // min * sec * millis

    private static final int NOTIFICATION_ID = 0;
    private static final int NOTIFICATION_STATUS = 444;
    static String CHANNEL_ID = "my_channel_01";
    private static long TEN_MINUTES = 10 * 60 * 1000;
    private static LangViewModel model = LangViewModel.getInstance();

    public static boolean canPerformSync (Context ctx, long sync_interval){
        SharedPreferences pref = PreferenceUtil.getPref(ctx);
        long lastSync = pref.getLong("last_sync", 0);
        long delta = System.currentTimeMillis() - lastSync;
        if(lastSync == 0 || delta > sync_interval) {
            boolean isProcessing = SyncContactManager
                    .getInstance(ctx.getApplicationContext())
                    .isSynchronizing();

            return isProcessing == false;
        }
        return false;
    }

    public static void setLastSync (Context ctx){
        PreferenceUtil
                .getEditor(ctx)
                .putLong("last_sync", System.currentTimeMillis())
                .commit();
    }

    public static void setLastGetCharge(Context ctx){
        PreferenceUtil.getEditor(ctx).putLong(PreferenceUtil.LASTGET, System.currentTimeMillis() - TEN_MINUTES).commit();
    }

    public static void setAlarmManager(Context ctx){

        ConfigData cdt = SessionManager.getConfigData(ctx);
        if (cdt != null && cdt.sync_duration > 0){
            int sync_duration = cdt.sync_duration;
            SYNC_TRIGGERED_AT = sync_duration;
            SYNC_INTERVAL = sync_duration;
        }

        if(canPerformSync(ctx, SYNC_INTERVAL)) {
            AlarmManager alarmManager;
            PendingIntent alarmIntent;

            Intent intent = new Intent(ctx, SyncContactServices.class);
            alarmIntent = PendingIntent.getService(ctx, 777, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager = (AlarmManager) ctx.getSystemService(Context.ALARM_SERVICE);
            alarmManager.setInexactRepeating(
                    AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + SYNC_TRIGGERED_AT,
                    SYNC_INTERVAL,
                    alarmIntent
            );
        }
    }

    public static void showToastInService(final Context ctx, final String message){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Check Protect Apps On Activity..............
    public static void checkProtected(final Activity activity){
        Intent[] permission = new Intent[]{
                ProtectPermission.permission.INTENT_ASUS,
                ProtectPermission.permission.INTENT_COLOROS,
                ProtectPermission.permission.INTENT_HUAWEI,
                ProtectPermission.permission.INTENT_IQOO,
                ProtectPermission.permission.INTENT_IQOO_STARTUP,
                ProtectPermission.permission.INTENT_LETV,
                //StarUp Manager XIAOMI
                ProtectPermission.permission.INTENT_XIAOMI,
                //StarUp Manager OPPO
                ProtectPermission.permission.INTENT_OPPO1,
                ProtectPermission.permission.INTENT_OPPO2,
                ProtectPermission.permission.INTENT_OPPO3,
                //Floating Windows OPPO
                ProtectPermission.permission.INTENT_FLOATING_OPPO1,
                ProtectPermission.permission.INTENT_FLOATING_OPPO2,
                ProtectPermission.permission.INTENT_FLOATING_OPPO3,
                //StarUp Manager VIVO
                ProtectPermission.permission.INTENT_VIVO
        };

        List<Intent> listPermissionProtect = new ArrayList<>();
        for (final Intent intn : permission){
            if (activity.getPackageManager().resolveActivity(intn, PackageManager.MATCH_DEFAULT_ONLY) != null){
                try{
                    activity.startActivity(intn);
                }catch (Exception e){
                    break;
                }

            }

        }
        if (!listPermissionProtect.isEmpty()){
            ActivityCompat.requestPermissions(activity,listPermissionProtect.toArray(new String[listPermissionProtect.size()]),789);
        }
    }

//    public static void customToast(Activity activity) {
//        Context context=activity;
//        LayoutInflater inflater = activity.getLayoutInflater();
//        View customToastroot =inflater.inflate(R.layout.custom_toast, null);
//        Toast customtoast=new Toast(context);
//        customtoast.setView(customToastroot);
//        customtoast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,0, 0);
//        customtoast.setDuration(Toast.LENGTH_LONG);
//        customtoast.show();
//    }

    public static void permissionSyncContact(Activity activity) {
        ConnectivityManager manager = (ConnectivityManager) activity.getSystemService(CONNECTIVITY_SERVICE);
        if (manager != null) {
            boolean is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();//For 3G check
            boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();//For WiFi Check
            System.out.println(is3g + " net " + isWifi);
            if (!is3g && !isWifi) {
                Toast.makeText(activity, model.getLang().string.checkNetwork, Toast.LENGTH_LONG).show();
            } else {
                if (PermissionUtil.hashPermission(activity, BaseActivity.permissions)) {
                    if (!SyncContactManager.getInstance(activity).isSynchronizing()) {
                        Intent i = new Intent(activity, SyncContactServices.class);
                        activity.startService(i);
                    }
                }
            }
        }
    }

    public static void permissionSyncContact(Context context) throws Exception {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        if (manager != null) {
            boolean is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();//For 3G check
            boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();//For WiFi Check
            System.out.println(is3g + " net " + isWifi);
            if (!is3g && !isWifi) {
                Toast.makeText(context, model.getLang().string.checkNetwork, Toast.LENGTH_LONG).show();
            } else {
                if (PermissionUtil.hashPermission(context, BaseActivity.permissions)) {
                    if (!SyncContactManager.getInstance(context).isSynchronizing()) {
                        Intent i = new Intent(context, SyncContactServices.class);
                        context.startService(i);
                    }
                }
            }
        }
    }

    public static void stopSyncContact(Context context) { //  Resume sync contact....
        if (PermissionUtil.hashPermission(context, BaseActivity.permissions)) {
            if (SyncContactManager.getInstance(context).isSynchronizing()) {
                Intent i = new Intent(context, SyncContactServices.class);
                context.stopService(i);
            }
        }
    }

    public static void resumeSyncContact(Context context) { //  Resume sync contact....
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        if (manager == null)
            return;
        boolean is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();//For 3G check
        boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();//For WiFi Check
        System.out.println(is3g + " net " + isWifi);
        if (!is3g && !isWifi) {
            Toast.makeText(context, model.getLang().string.checkNetwork, Toast.LENGTH_LONG).show();
        } else {
            if (PermissionUtil.hashPermission(context, BaseActivity.permissions)) {
                if (SyncContactManager.getInstance(context).isSynchronizing()) {
                    Intent i = new Intent(context, SyncContactServices.class);
                    context.startService(i);
                }
            }
        }
    }

    // Check Permission On Activity..............
    public static boolean checkPermission(Activity activity) {
        int result;

        List<String> listPermissionNeeded = new ArrayList<>();
        for (String p : BaseActivity.permissions){
            result = ContextCompat.checkSelfPermission(activity,p);
            if (result != PackageManager.PERMISSION_GRANTED){
                listPermissionNeeded.add(p);
            }
        }
        if (!listPermissionNeeded.isEmpty()){
            ActivityCompat.requestPermissions(activity,listPermissionNeeded.toArray(new String[listPermissionNeeded.size()]),789);
            return false;
        }

        return true;
    }

    //........................................
    public static void showNotificationProgress(Context applicationContext, String title){
        NotificationManager notificationManager = (NotificationManager)
                applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);

        String sync = TextUtils.isEmpty(model.getLang().string.str_set_up_profile_sync)
                ? "Sync"
                : model.getLang().string.str_set_up_profile_sync;

        //Set notification information:
        Notification.Builder notificationBuilder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                int notifyID = 1;
                String CHANNEL_ID = "mvicall_channel";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "mvicall_notification", importance);
                Notification notification = new Notification.Builder(applicationContext)
                        .setOngoing(true)
                        .setAutoCancel(false)
                        .setContentTitle(sync)
                        .setContentText(title)
                        .setSmallIcon(R.mipmap.icx_notif)
                        .setChannelId(CHANNEL_ID)
                        .build();

                NotificationManager mNotificationManager =
                        (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.createNotificationChannel(mChannel);
                mNotificationManager.notify(NOTIFICATION_ID , notification);

            }else {

                notificationBuilder = new Notification.Builder(applicationContext)
                        .setOngoing(true)
                        .setAutoCancel(false)
                        .setVisibility(Notification.VISIBILITY_PRIVATE)
                        .setContentTitle(sync)
                        .setContentText(title)
                        .setSmallIcon(R.mipmap.icx_notif)
                        .setProgress(0, 0, true);

                Notification notification = notificationBuilder.build();
                notificationManager.notify(NOTIFICATION_ID, notification);

            }

        }else{
            notificationBuilder = new Notification.Builder(applicationContext)
                    .setOngoing(true)
                    .setAutoCancel(false)
                    .setContentTitle(sync)
                    .setContentText(title)
                    .setSmallIcon(R.mipmap.icx_notif)
                    .setProgress(0, 0, true);

            Notification notification = notificationBuilder.build();
            notificationManager.notify(NOTIFICATION_ID, notification);

        }
    }

    @SuppressLint("WrongConstant")
    public static void showStatusNotification(Context applicationContext, String channel, String title , String message){

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(applicationContext);
        final Notification notification;
        NotificationManager notificationManager = (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = new Intent(applicationContext, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle = new Bundle();
        bundle.putString("direction", channel);
        intent.putExtras(bundle);
        PendingIntent notifyPendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, 0);

        //Set notification information:
        Notification.Builder notificationBuilder = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                String CHANNEL_ID = "mvicall_notif";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "mvicall_notif", importance);
                notification = mBuilder
                        .setAutoCancel(true)
                        .setContentIntent(notifyPendingIntent)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setSmallIcon(R.mipmap.icx_notif)
                        .setChannelId(CHANNEL_ID)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .build();
                NotificationManager mNotificationManager = (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.createNotificationChannel(mChannel);
                mNotificationManager.notify(NOTIFICATION_STATUS , notification);

            }else {

                notification = mBuilder
                        .setAutoCancel(true)
                        .setVisibility(Notification.VISIBILITY_PRIVATE)
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentIntent(notifyPendingIntent)
                        .setContentText(message)
                        .setSmallIcon(R.mipmap.icx_notif)
                        .build();
                notificationManager.notify(NOTIFICATION_STATUS, notification);

            }

        }else{

            notification = mBuilder
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setContentIntent(notifyPendingIntent)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.icx_notif).build();
            notificationManager.notify(NOTIFICATION_STATUS, notification);

        }

//        if (applicationContext != null) {
//            // Sound
//            final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext.getPackageName() + "/raw/notification");
//            // Icon...
//            final int icon = R.drawable.logo_1;
//            // Builder...
//            final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(applicationContext);
//
//            NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
//            Notification notification;
//            notification = mBuilder.setSmallIcon(icon)
//                    .setTicker(title)
//                    .setWhen(0)
//                    .setAutoCancel(true)
//                    .setSound(alarmSound)
//                    .setStyle(inboxStyle)
//                    .setSmallIcon(R.drawable.logo_1)
//                    .setContentTitle(title)
//                    .setContentText(message)
//                    .setChannelId(channel)
//                    .build();
//            NotificationManager notificationManager = (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(ConfigPref.NOTIFICATION_ID_STATUS, notification);
//        }

    }

//    public static void showPayloadNotification(Context applicationContext, String title, String body){
//        NotificationManager notificationManager = (NotificationManager)
//                applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        //Set notification information:
//        Notification.Builder notificationBuilder = null;
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//
//            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//
//                int notifyID = 1;
//                String CHANNEL_ID = "mvicall_channel_payload";
//                int importance = NotificationManager.IMPORTANCE_HIGH;
//                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "mvicall_notification_payload", importance);
//                Notification notification = new Notification.Builder(applicationContext)
//                        .setAutoCancel(true)
//                        .setContentTitle(title)
//                        .setContentText(body)
//                        .setSmallIcon(R.mipmap.icx_notif)
//                        .setChannelId(CHANNEL_ID)
//                        .build();
//
//                NotificationManager mNotificationManager =
//                        (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
//                mNotificationManager.createNotificationChannel(mChannel);
//                mNotificationManager.notify(NOTIFICATION_ID , notification);
//
//            }else {
//
//                notificationBuilder = new Notification.Builder(applicationContext)
//                        .setAutoCancel(true)
//                        .setVisibility(Notification.VISIBILITY_PRIVATE)
//                        .setContentTitle(title)
//                        .setContentText(body)
//                        .setSmallIcon(R.mipmap.icx_notif);
//
//                Notification notification = notificationBuilder.build();
//                notificationManager.notify(NOTIFICATION_ID, notification);
//
//            }
//
//        }else{
//
//            notificationBuilder = new Notification.Builder(applicationContext)
//                    .setAutoCancel(true)
//                    .setContentTitle(title)
//                    .setContentText(body)
//                    .setSmallIcon(R.mipmap.icx_notif);
//
//            Notification notification = notificationBuilder.build();
//            notificationManager.notify(NOTIFICATION_ID, notification);
//
//        }
//    }


    public static void hideNotificationProgress(Context applicationContext){
        NotificationManager notificationManager = (NotificationManager)
                applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }


    /**
     * Cek Status Syncronize kontak
     *
     * @param context
     * @return
     */
    public static boolean checkSyncStatus(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SYNC_CONTACT_PREFERENCE, Context.MODE_PRIVATE);
        boolean status = sp.getString(SYNC_CONTACT_STATUS, "0").equals("1");
        Log.i("Cek Sync ", "" + status);
        return status;
    }

    /**
     * Update status syncronize
     *
     * @param context
     */
    public static void updateSyncPreference(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SYNC_CONTACT_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SYNC_CONTACT_STATUS, "1");
        editor.apply();
    }

    public interface SYNC {
        void startSYNC();
    }

}
