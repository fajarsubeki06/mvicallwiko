package com.sbimv.mvicalls.sync;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.sbimv.mvicalls.db.DBHelper;
import com.sbimv.mvicalls.db.LikeTable;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.DataLike;
import java.io.IOException;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Response;

// Add Yudha Pratama Putra 3/11/2018

public class SyncComment {

    static final String TAG = "SyncComment";
    static SyncComment instance;
    private SQLiteDatabase database;

    // private constructor
    private SyncComment(Context applicationContext){
        this.applicationContext = applicationContext;
    }

    // variables
    boolean isProcessing;
    Context applicationContext;

    public static SyncComment getInstance(Context appContext){
        if(instance == null)
            instance = new SyncComment (appContext);
        return instance;
    }

    public boolean isSynchronizing(){
        return isProcessing;
    }

    // Sync...
    public void sync(String... arg){
        if(isProcessing)
            return;

        LangViewModel model = LangViewModel.getInstance();

        String wSend = TextUtils.isEmpty(model.getLang().string.str_sending_data)
                ?"Sending data...":model.getLang().string.str_sending_data;

        SyncUtils.showNotificationProgress(applicationContext, wSend);
        isProcessing = true; // set processing flag
        getDataLocal(arg[0]);
        isProcessing = false; // set processing flag
        SyncUtils.hideNotificationProgress(applicationContext);

    }

    // Select data likers...
    public void getDataLocal (String... arg) {
        if (!TextUtils.isEmpty(arg[0])) {
            String selectQuery = "SELECT * FROM " + LikeTable.Entry.TABLE_NAME + " WHERE caller_id_uploader = '" + arg[0] + "'";

            try {
                database = DBHelper.getInstance(applicationContext).getReadableDatabase();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            if (database != null) {
                Cursor cursor = database.rawQuery(selectQuery, null);
                if (cursor != null && cursor.getCount() > 0) {
                    if (cursor.moveToFirst()) {
                        do {
                            String caller_id_uploader = cursor.getString(cursor.getColumnIndex(LikeTable.Entry.COLUMN_NAME_CALLER_ID_UPLOADER));
                            String caller_id = cursor.getString(cursor.getColumnIndex(LikeTable.Entry.COLUMN_NAME_CALLER_ID));
                            String status = cursor.getString(cursor.getColumnIndex(LikeTable.Entry.COLUMN_NAME_STATUS_COMMENT));
                            String source = cursor.getString(cursor.getColumnIndex(LikeTable.Entry.COLUMN_NAME_SOURCE_VIDEO));
                            try {
                                sendComment(caller_id_uploader, caller_id, status, source);
                            } catch (IOException e) {
                                return;
                            }
                        } while (cursor.moveToNext());
                        cursor.close();
                    }
                }
            }
        }

    }

    // Get parsing data API
    private void sendComment(String caller_id_uploader, String caller_id, String status, String source) throws IOException {
        if (!TextUtils.isEmpty(caller_id_uploader) && !TextUtils.isEmpty(caller_id) && !TextUtils.isEmpty(status) && !TextUtils.isEmpty(source)) {
            Call<APIResponse<ArrayList<DataLike>>> call = ServicesFactory.getService().getLike(caller_id_uploader, caller_id, source, status);
            Response<APIResponse<ArrayList<DataLike>>> response = call.execute();
            if (response.isSuccessful() && response.body().isSuccessful()) {
                ArrayList<DataLike> contacts = response.body().data;
                if (contacts != null &&contacts.size() > 0) {
                    updateLikers(contacts);
                }
            }
        }
    }

    // UPDATE data likers....
    private void updateLikers(ArrayList<DataLike> contacts){
        try{
            database = DBHelper.getInstance(applicationContext).getWritableDatabase();
        }catch (Exception e){
            e.printStackTrace();
            return;
        }
        if (database != null) {
            if (contacts != null && contacts.size() > 0) {
                for (DataLike data : contacts) {
                    String id_uploader = data.caller_id;
                    database.execSQL(" UPDATE tbl_like SET send = '1' WHERE caller_id_uploader = '" + id_uploader + "' ");
                }
            }
        }
    }

}
