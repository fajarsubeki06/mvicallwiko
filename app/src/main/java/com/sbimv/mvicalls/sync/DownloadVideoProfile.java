package com.sbimv.mvicalls.sync;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.CallerVideoManager;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class DownloadVideoProfile {

    static final String TAG = "DownloadVideoProfile";
    static DownloadVideoProfile instance;
    private SharedPreferences sharedPreferences;
    private boolean uploadCont = false;

    // private constructor
    private DownloadVideoProfile(Context applicationContext){
        this.applicationContext = applicationContext;
    }

    // variables
    boolean isProcessing;
    Context applicationContext;
    Set<String> contactsSet = new HashSet<>();
    ContactItem own;

    public static DownloadVideoProfile getInstance(Context appContext){
        if(instance == null)
            instance = new DownloadVideoProfile (appContext);
        return instance;
    }

    public boolean isSynchronizing(){
        return isProcessing;
    }

    public void firstsync(){
        LangViewModel model = LangViewModel.getInstance();
        String download = TextUtils.isEmpty(model.getLang().string.str_set_up_profile_vid_profile)
                ? "Downloading Video Profile..."
                :  model.getLang().string.str_set_up_profile_vid_profile;
        if(isProcessing)
            return;

        PreferenceUtil.getEditor(applicationContext).putBoolean(PreferenceUtil.ACTION_SAVE_PROFILE, true).commit();
        SyncUtils.showNotificationProgress(applicationContext, download);
        isProcessing = true; // set processing flag

        ContactItem own = SessionManager.getProfile(applicationContext);
        if (own != null){
            String urlUserVideo = own.video_caller;
            if (!TextUtils.isEmpty(urlUserVideo)){
                urlUserVideo = urlUserVideo.replaceAll(" ", "%20");
                downloadProfileVideo(urlUserVideo);
            }else {
                downloadProfileVideo("https://api.mvicall.com/uploads/video/mvicall_video_default.mp4");
            }
        }else {
            downloadProfileVideo("https://api.mvicall.com/uploads/video/mvicall_video_default.mp4");
        }

    }

    public void sync(){
        if(isProcessing)
            return;

        LangViewModel model = LangViewModel.getInstance();
        String download = TextUtils.isEmpty(model.getLang().string.str_set_up_profile_vid_profile)
                ? "Downloading Video Profile..."
                :  model.getLang().string.str_set_up_profile_vid_profile;

        PreferenceUtil.getEditor(applicationContext).putBoolean(PreferenceUtil.ACTION_SAVE_PROFILE, true).commit();
        SyncUtils.showNotificationProgress(applicationContext, download);
        isProcessing = true; // set processing flag

        own = SessionManager.getProfile(applicationContext);

        downloadProfileVideo();

    }

    private void downloadProfileVideo(String url) {
        if (!TextUtils.isEmpty(url)) {
            url = url.replaceAll(" ", "%20");
            Call<ResponseBody> call = ServicesFactory.getService().downloadVideo(url);
            try {
                Response<ResponseBody> response = call.execute();
                FileUtil.writeResponseBodyToDisk(response.body(),
                        CallerVideoManager.getDisPath(applicationContext),
                        CallerVideoManager.generateUniversalName("current_video.mp4"));

                isProcessing = false;
                SyncUtils.hideNotificationProgress(applicationContext);

                Intent il = new Intent("com.sbimv.mvicalls.DEMO_UPLOAD_VIDEO");
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(il);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void downloadProfileVideo() {
        if (!TextUtils.isEmpty(own.video_caller)) {
            Log.i("download_video", own.video_caller);
            Call<ResponseBody> call = ServicesFactory.getService().downloadVideo(own.video_caller);
            try {
                Response<ResponseBody> response = call.execute();
                FileUtil.writeResponseBodyToDisk(response.body(),
                        CallerVideoManager.getDisPath(applicationContext),
                        CallerVideoManager.generateUniversalName("current_video.mp4"));

                isProcessing = false;
                SyncUtils.hideNotificationProgress(applicationContext);

                Intent il = new Intent("com.sbimv.mvicalls.DEMO_UPLOAD_VIDEO");
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(il);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
