package com.sbimv.mvicalls.sync;

import android.content.Context;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;



import agency.tango.materialintroscreen.BuildConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncConfig {

    private static final String TAG = "SyncConfig";
    private static SyncConfig instance;
    private ContactItem own;
    // variables
    private boolean isProcessing;
    private Context applicationContext;

    // private constructor
    private SyncConfig(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public static SyncConfig getInstance(Context appContext) {
        if (instance == null)
            instance = new SyncConfig(appContext);
        return instance;
    }

    public boolean isSynchronizing() {
        return isProcessing;
    }

    // Sync...
    public void sync() {
        if (isProcessing)
            return;
        // set own data
        own = SessionManager.getProfile(applicationContext);
        if (own == null)
            return;

        isProcessing = true; // set true processing flag
        getParsingData();
        isProcessing = false; // set false processing flag
    }

    private void getParsingData() {
        if (own == null)
            return;

        String sMsisdn = TextUtils.isEmpty(own.msisdn)?"":own.msisdn;
//        paramsShowcaseFinished(applicationContext)
        Call<APIResponse<ConfigData>> call = ServicesFactory.getService().appsConfig(sMsisdn,paramsShowcaseFinished(applicationContext));
        call.enqueue(new Callback<APIResponse<ConfigData>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<ConfigData>> call, @NotNull Response<APIResponse<ConfigData>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful() && response.body().data != null) {

                    try {

                        int curVersion = BuildConfig.VERSION_CODE;
                        int prevVersion = PreferenceUtil.getPref(applicationContext).getInt(PreferenceUtil.APP_VERSION, 0);

                        ConfigData newConfig = response.body().data;
                        ConfigData oldConfig = SessionManager.getConfigData(applicationContext);
                        boolean mustUpdateLang = false;

                        if (oldConfig == null) {
                            mustUpdateLang = true;
                        } else if (!oldConfig.getVersion_lang().equalsIgnoreCase(newConfig.getVersion_lang())) {
                            mustUpdateLang = true;
                        } else if (curVersion != prevVersion) {
                            mustUpdateLang = true;
                        }

                        PreferenceUtil.getEditor(applicationContext).putInt(PreferenceUtil.APP_VERSION, curVersion).commit();
                        Intent intentVerApp = new Intent("com.sbimv.mvicalls.UPDATE_CONFIG");
                        intentVerApp.putExtra("must_update_lang", mustUpdateLang);
                        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intentVerApp);

                        SessionManager.saveConfigData(applicationContext, newConfig);//save configData

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<ConfigData>> call, @NotNull Throwable t) {
                Log.i("", "Data config on failure...");
            }
        });
    }

    private boolean paramsShowcaseFinished (Context context) {

        boolean isScFinish;
        boolean scFinished;
        boolean cdtFinished;

        if (context == null)
            return false;

        scFinished = PreferenceUtil.getPref(context).getBoolean(PreferenceUtil.FINAL_SCASE, false);
        ConfigData cdt = SessionManager.getConfigData(context);
        if (cdt != null)
            cdtFinished = cdt.showcase_finish;
        else
            cdtFinished = false;

        isScFinish = scFinished || cdtFinished;

        return isScFinish;

    }

    public interface UpdateLangListener {
        void onResponse();
    }

}
