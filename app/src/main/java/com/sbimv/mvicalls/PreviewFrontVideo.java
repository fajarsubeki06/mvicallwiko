package com.sbimv.mvicalls;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.activity.InboxActivity;
import com.sbimv.mvicalls.activity.MainActivity;
import com.sbimv.mvicalls.activity.PopUpSubsActivity;
import com.sbimv.mvicalls.activity.PopupVietSubsActivity;
import com.sbimv.mvicalls.activity.ProfileActivity;
import com.sbimv.mvicalls.activity.TransactionOtpActivity;
import com.sbimv.mvicalls.activity.TselActivity;
import com.sbimv.mvicalls.activity.UploadVideoToneActivity;
import com.sbimv.mvicalls.adapter.FrontVideoAdapter;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.CheckPurchase;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataBannerPromo;
import com.sbimv.mvicalls.pojo.DataChargingContent;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreviewFrontVideo extends BaseActivity {

    public Button btnSubscribe;
    MediaController mediaController;
    private TextView Nama;
    private TextView Judul;
    private TextView Harga;
    private TextView JudulRelated;
    private RecyclerView rvFrontVideo;
    private VideoView videoview;
    private DataDinamicVideos vid;
    private DataDinamicTitle title;
    private LinearLayout layout;
    private String url_video = "";
    private String TYPE_TRX = "buy";
    private ProgressBar pgVideo;
    private ContactItem own;

    private boolean isData;
    private ProgressDialog progressDialog;
    private String urlCharging;
    private int exit = 0;
    private boolean isOnline = true;
    private ArrayList<DataChargingContent> datas;

    BillingClient mBillingClient;
    List<String> mSKUList = new ArrayList<>();
    private String status;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isData) {
                if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                    final Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                while (!isConnected(PreviewFrontVideo.this)) {
                                    int wait = 1000;

                                    Thread.sleep(wait);
                                    exit += wait;
                                    if (exit == 10000) {
//                                        progressDialog.dismiss();
                                        PreviewFrontVideo.this.runOnUiThread(new Runnable() {
                                            public void run() {
                                                toast(model.getLang().string.errorFetchingData);
                                            }
                                        });
                                        isOnline = false;
                                        break;
                                    }
                                }

                                if (isOnline) {
                                    getUrlCharging();
                                    isData = false;
                                }

                            } catch (Exception e) {
//                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    };
                    thread.start();
                }
                isData = false;
            }
        }
    };


    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium_video_preview);
        setDefaultToolbar(true);
        own = SessionManager.getProfile(PreviewFrontVideo.this);

        layout = findViewById(R.id.linPreview);
        Nama = findViewById(R.id.txtNamaPreview);
        Judul = findViewById(R.id.txtJudulPreview);
        Harga = findViewById(R.id.txtHargaPreview);
        JudulRelated = findViewById(R.id.judulRelated);
        rvFrontVideo = findViewById(R.id.rvRelatedVideo);
        pgVideo = findViewById(R.id.pgVideo);
        btnSubscribe = findViewById(R.id.btnBuy);
        videoview = findViewById(R.id.vv_preview_latest);
        btnSubscribe.setEnabled(true);

//        progressDialog = new ProgressDialog(PreviewFrontVideo.this);
//        progressDialog.setCancelable(false);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setMessage("Processing...");


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            vid = bundle.getParcelable("video");
            title = bundle.getParcelable("title");
        }

        if (title != null && vid != null) {
            JudulRelated.setText(title.getOthers());
            Nama.setText(vid.getAlias());
            Judul.setText(vid.getJudul());

            String currency = TextUtils.isEmpty(vid.price_label)?"Rp.":vid.price_label;
            if (vid.price.equals("0")) {
                Harga.setText(model.getLang().string.free);
                setButton(model.getLang().string.grabNow);
            } else {
                Harga.setText(currency +" "+ vid.getPrice());
                setButton(model.getLang().string.subscribe);
            }

            initVideoView();
            initRecycler();

        }


        btnSubscribe.setOnClickListener(view -> {

            //Check Content free....
            if (vid == null)
                return;

            String strPrice = vid.getPrice();
            String strContentId = vid.getContent_id();

            if (TextUtils.isEmpty(strPrice) || TextUtils.isEmpty(strContentId)) {
                String parsing = TextUtils.isEmpty(model.getLang().string.str_preview_video_premium_parsing_failed)
                        ? "Parsing data failed"
                        : model.getLang().string.str_preview_video_premium_parsing_failed;
                toast(parsing);
                return;
            }

            if (contentFree(strPrice)) { // if content free...
                buyContentFree(strContentId, btnSubscribe);
            } else {

                ContactItem ctItem = SessionManager.getProfile(PreviewFrontVideo.this);
                if (ctItem == null) {
                    String parsing = TextUtils.isEmpty(model.getLang().string.str_preview_video_premium_parsing_failed)
                            ? "Parsing data failed"
                            : model.getLang().string.str_preview_video_premium_parsing_failed;
                    toast(parsing);
                    return;
                }

                String strMsisdn = TextUtils.isEmpty(ctItem.msisdn)?"":ctItem.msisdn;
                if (!TextUtils.isEmpty(strMsisdn)) {
                    checkSubs(strMsisdn, isSuccess -> {
                        if (!isSuccess) {
                            if (!PreviewFrontVideo.this.isFinishing()) {
                                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                assert ccode != null;
                                if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")) {
                                    startActivity(new Intent(PreviewFrontVideo.this, PopupVietSubsActivity.class));
                                    return;
                                }
                                startActivity(new Intent(PreviewFrontVideo.this, PopUpSubsActivity.class));
                            }
                        } else {
                            getDataBuyContent(strMsisdn, strContentId, TYPE_TRX);
                        }
                    });
                }else {
                    toast("Msisdn null....");
                }

            }

        });

        setupBillingClient();

        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_PREMIUM_CONTENT_PREVIEW); // google analytics...
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setButton(String text){
        String sMsisdn = TextUtils.isEmpty(own.msisdn)?"":own.msisdn;
        String cId = TextUtils.isEmpty(vid.getContent_id())?"":vid.getContent_id();
        checkContentPurchase(sMsisdn, cId, new CheckStatusListener() {
            @Override
            public void onCheckingContent(String status, boolean isSuccess) {
                if (!TextUtils.isEmpty(status)){
                    if (isSuccess){
                        if (status.equalsIgnoreCase("true")){
                            String titleSee = TextUtils.isEmpty(model.getLang().string.seeCollection)
                                    ? "See On My Collection"
                                    : model.getLang().string.seeCollection;
                            btnSubscribe.setText(titleSee);
                            btnSubscribe.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    startActivity(new Intent(PreviewFrontVideo.this, UploadVideoToneActivity.class));
                                }
                            });
                        }else{
                            btnSubscribe.setText(text);
                        }
                    }else{
                        Toast.makeText(PreviewFrontVideo.this, model.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private boolean contentFree(String price){
        boolean isFree = false;
        if (price.equals("0")){
            isFree = true;
        }
        return isFree;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(PreviewFrontVideo.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(PreviewFrontVideo.this).unregisterReceiver(popupNotifAppears);
    }


    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(PreviewFrontVideo.this).unregisterReceiver(receiver);
    }

    private void initVideoView() {

        url_video = vid.getSource_content();
        url_video = url_video.replaceAll(" ", "%20");

        mediaController = new MediaController(this);
        mediaController.setAnchorView(mediaController);

        Uri uri = Uri.parse(url_video);
        videoview.setVideoURI(uri);
        videoview.setMediaController(mediaController);
        videoview.requestFocus();
        videoview.setVisibility(View.VISIBLE);

        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                pgVideo.setVisibility(View.GONE);
                videoview.start();
            }
        });
        videoview.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                pgVideo.setVisibility(View.GONE);
                showAlertErrorPlay();
                return true;
            }
        });
    }

    private void showAlertErrorPlay(){
        if (!PreviewFrontVideo.this.isFinishing()) {

            String sTitle = TextUtils.isEmpty(model.getLang().string.str_cannot_play)
                    ? "Can't play this video."
                    : model.getLang().string.str_cannot_play;

            String sApprove = TextUtils.isEmpty(model.getLang().string.approveCofirm)
                    ? "Ok"
                    : model.getLang().string.approveCofirm;

            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setMessage(sTitle)
                    .setCancelable(false)
                    .setPositiveButton(sApprove, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).show();
        }
    }

    private void initRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvFrontVideo.setLayoutManager(layoutManager);
        FrontVideoAdapter ar = new FrontVideoAdapter(title);
        rvFrontVideo.setAdapter(ar);
    }

    // =============================================== Parsing API Charging =================================================
// ======================================================================================================================
    private void getDataBuyContent(String msisdn, final String content_id, String type) {
        String lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
        String multiLang = TextUtils.isEmpty(lang)?"":lang;
        Call<APIResponse<ArrayList<DataChargingContent>>> call = ServicesFactory.getService().getNewChargingContent(msisdn, content_id, type,multiLang);
        call.enqueue(new Callback<APIResponse<ArrayList<DataChargingContent>>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<ArrayList<DataChargingContent>>> call, @NotNull Response<APIResponse<ArrayList<DataChargingContent>>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    datas = response.body().data;
                    if (datas != null && datas.size() > 0) {
                        if (datas.size() == 1) {
                            DataChargingContent dataChargingContent = datas.get(0);
                            paymentProcess(dataChargingContent, content_id); // Payment process....
                        } else {
                            showPaymentMethod(datas, content_id); // Show popup payment......
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<ArrayList<DataChargingContent>>> call, @NotNull Throwable t) {
                toast(model.getLang().string.errorFetchingData);
            }
        });
    }

    private void showPaymentMethod(ArrayList<DataChargingContent> datas, final String content_id) {
        if (!PreviewFrontVideo.this.isFinishing()) {

            final HashMap<String, DataChargingContent> paymentMap = new HashMap<>();
            for (DataChargingContent payment : datas) {
                String channel = TextUtils.isEmpty(payment.getChannel()) ? "telco" : payment.getChannel();
                paymentMap.put(channel, payment);
            }

            final AlertDialog.Builder builder = new AlertDialog.Builder(PreviewFrontVideo.this);
            final View view = this.getLayoutInflater().inflate(R.layout.custom_dialog_allert, null);
            /*
             * Inisialisasi.......
             * */
            LinearLayout mTelco = view.findViewById(R.id.ln_telco);
            LinearLayout mGpay = view.findViewById(R.id.ln_gpay);
            ImageView mCancel = view.findViewById(R.id.iv_cancel);
            TextView tvChoosePayment = view.findViewById(R.id.tvChoosePayment);
            TextView tvDescPayment = view.findViewById(R.id.tvDescPayment);
            TextView tvDescTelco = view.findViewById(R.id.tvDescTelco);
            TextView tvGpay = view.findViewById(R.id.tvChooseGpay);

            tvGpay.setText(model.getLang().string.gunakanGPay);
            tvDescTelco.setText(model.getLang().string.wordButtonTelcoDialogPayment);
            tvDescPayment.setText(model.getLang().string.wordBodyPay);
            tvChoosePayment.setText(model.getLang().string.pilihCaraPembayaran);

            builder.setView(view);
            builder.setCancelable(true);
            final AlertDialog dialog = builder.create();
            /*
             * Telco Event Click.......
             * */
            mTelco.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!PreviewFrontVideo.this.isFinishing()) {
                        PreferenceUtil.getEditor(PreviewFrontVideo.this).putBoolean(PreferenceUtil.TSEL_BUY, true);
                        dialog.dismiss();
                        DataChargingContent selected = (paymentMap.get("weboptin") == null) ? paymentMap.get("telco") : paymentMap.get("weboptin");

                        if (selected != null) {
                            String channel = selected.getChannel();
                            if (!TextUtils.isEmpty(channel)) {
                                // weboptin & not connect wifi
                                if (selected.getChannel().equals("weboptin") && !Util.isWifiConnected(PreviewFrontVideo.this)) {
                                    if (!PreviewFrontVideo.this.isFinishing()) {
                                        dialog.dismiss();
                                        getUrlCharging();
                                    }
                                }

                                else if (selected.getChannel().equals("weboptin_otp") && !Util.isWifiConnected(PreviewFrontVideo.this)) {
                                    if (!PreviewFrontVideo.this.isFinishing()) {
                                        dialog.dismiss();
                                        getUrlCharging();
                                    }
                                }

                                //*
                                // Add payment method DCB for thailand
                                // *
                                else if (selected.getChannel().equals("dcb")){
                                    ContactItem owns = SessionManager.getProfile(PreviewFrontVideo.this);
                                    if (owns != null && vid != null) {

                                        String sAlias = vid.alias;
                                        String sName = vid.judul;
                                        String sMsisdn = owns.msisdn;
                                        String sContentId = vid.content_id;
                                        String sPrice = vid.price;
                                        String sPriceLbl = vid.price_label;
                                        String sType = "buy";

                                        startActivity(new Intent(PreviewFrontVideo.this, TransactionOtpActivity.class)
                                                .putExtra("sAlias", sAlias)
                                                .putExtra("sName", sName)
                                                .putExtra("sMisdn", sMsisdn)
                                                .putExtra("sContenId", sContentId)
                                                .putExtra("sPrice", sPrice)
                                                .putExtra("sPriceLabel", sPriceLbl)
                                                .putExtra("sType", sType)
                                        );

                                    }
                                }

                                else if (selected.getChannel().equals("ussd")){ // Enable channel USSD for vietnam
                                    if (!PreviewFrontVideo.this.isFinishing()) {
                                        dialog.dismiss();
                                        getUrlCharging();
                                    }
                                }

                                // weboptin & connect wifi
                                else if (selected.getChannel().equals("weboptin") && Util.isWifiConnected(PreviewFrontVideo.this)) {
                                    showAlertWifi();
                                }
                                else if (selected.getChannel().equals("weboptin_otp") && Util.isWifiConnected(PreviewFrontVideo.this)) {
                                    showAlertWifi();
                                }
                                // Gpay
                                else {
                                    paymentProcess(selected, content_id);
                                }
                            } else {
                                paymentProcess(selected, content_id);
                            }
                        }
                    }
                }
            });
            /*
             * G-pay Event Click.......
             * */
            mGpay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    DataChargingContent selected = paymentMap.get("GOOGLE");
                    if (selected != null) {
                        paymentProcess(selected, content_id);
                    }
                }
            });
            /*
             * Cancel Event Click.......
             * */
            mCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    private void showAlertWifi() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PreviewFrontVideo.this);
        builder1.setMessage(model.getLang().string.wordingChangeWifiState);
        builder1.setPositiveButton(model.getLang().string.turnOffWifi, null);
        AlertDialog dialog1 = builder1.create();
        dialog1.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button buttonPositive = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                buttonPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        if (wifiManager != null) {
                            if (!PreviewFrontVideo.this.isFinishing()) {
                                wifiManager.setWifiEnabled(false);
                                isData = true;
                                dialog.dismiss();
//                                progressDialog.show();
                            }
                        }
                    }
                });
            }
        });
        dialog1.show();
    }

    private void paymentProcess(DataChargingContent dataChargingContent, String content_id) {
        if (dataChargingContent != null) {
            String strChannel = dataChargingContent.getChannel();
            if (!TextUtils.isEmpty(strChannel) && strChannel.equalsIgnoreCase("GOOGLE")) {
                mSKUList.add(dataChargingContent.getKeyword());
                onActionPurchase(dataChargingContent.getKeyword());
            }
            else if (!TextUtils.isEmpty(strChannel) && strChannel.equalsIgnoreCase("ussd")) { // Enable channel USSD for vietnam
                getUrlCharging();
            }
            else if (!TextUtils.isEmpty(strChannel) && strChannel.equalsIgnoreCase("weboptin")) {
                getUrlCharging();
            }
            else if (!TextUtils.isEmpty(strChannel) && strChannel.equalsIgnoreCase("weboptin_otp")) {
                getUrlCharging();
            }

            //*
            // Add payment method DCB for thailand
            // *
            else if (!TextUtils.isEmpty(strChannel) && strChannel.equalsIgnoreCase("dcb")){
                ContactItem owns = SessionManager.getProfile(PreviewFrontVideo.this);
                if (owns != null && vid != null) {

                    String sAlias = vid.alias;
                    String sName = vid.judul;
                    String sMsisdn = owns.msisdn;
                    String sContentId = vid.content_id;
                    String sPrice = vid.price;
                    String sPriceLbl = vid.price_label;
                    String sType = "buy";

                    startActivity(new Intent(PreviewFrontVideo.this, TransactionOtpActivity.class)
                            .putExtra("sAlias", sAlias)
                            .putExtra("sName", sName)
                            .putExtra("sMisdn", sMsisdn)
                            .putExtra("sContenId", sContentId)
                            .putExtra("sPrice", sPrice)
                            .putExtra("sPriceLabel", sPriceLbl)
                            .putExtra("sType", sType)
                    );

                }
            }

            else{
                sendSMSChargingAndWaiting(PreviewFrontVideo.this, dataChargingContent.getSdc(), dataChargingContent.getKeyword()); // Telco payment
            }
            // Google Analitycs...............
            ContactItem own = SessionManager.getProfile(PreviewFrontVideo.this);
            if (own != null) {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_PREMIUM_CONTENT_PREVIEW, TrackerConstant.EVENT_CAT_PREMIUM_CONTENT_PREVIEW, "click buy", own.caller_id + "/" + dataChargingContent.getKeyword());
            }
        } else {
            String premium = TextUtils.isEmpty(model.getLang().string.str_preview_video_premium_data_kosong)
                    ? "Data kosong atau Methode Pembayaran ini tidak tersedia"
                    : model.getLang().string.str_preview_video_premium_data_kosong;
            toast(premium);
        }
    }

// ============================================== Function Google Payment ===============================================
// ======================================================================================================================

    private void setupBillingClient() { // Setup Billing...
        mBillingClient = BillingClient
                .newBuilder(this)
                .setListener(mPurchaseUpdateListener)
                .build();

        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(int billingResponseCode) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    System.out.println("BILLING | startConnection | RESULT OK");
                } else {
                    System.out.println("BILLING | startConnection | RESULT: "+billingResponseCode);
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                System.out.println("BILLING | onBillingServiceDisconnected | DISCONNECTED");
            }
        });
    }

    PurchasesUpdatedListener mPurchaseUpdateListener = new PurchasesUpdatedListener() {
        @Override
        public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
            if (responseCode == BillingClient.BillingResponse.OK){
                System.out.println("BILLING | startConnection | RESULT OK");

                if (purchases != null) {
                    for (Purchase buy : purchases){
                        mBillingClient.consumeAsync(buy.getPurchaseToken(), (responseCode1, purchaseToken) -> {
                            System.out.println("null");
                        });
                    }
                }

                buyContentFree(vid.content_id, btnSubscribe);

            } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED){
                System.out.println("BILLING | startConnection | RESULT: "+responseCode);
            } else{
                System.out.println("BILLING | onBillingServiceDisconnected | DISCONNECTED");
            }
        }
    };

    public void onActionPurchase(String product){
        if (mBillingClient.isReady()) {
            SkuDetailsParams params = SkuDetailsParams
                    .newBuilder()
                    .setSkusList(mSKUList)
                    .setType(BillingClient.SkuType.INAPP)
                    .build();

            mBillingClient.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
                @Override
                public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
                    if(responseCode == BillingClient.BillingResponse.OK){
                        System.out.println("querySkuDetailsAsync, responseCode: "+responseCode);
                        if(skuDetailsList.size() > 0) {
                            for (int i = 0; i<skuDetailsList.size(); i++){
                                String idProduct = skuDetailsList.get(i).getSku();
                                if (!TextUtils.isEmpty(product) && !TextUtils.isEmpty(idProduct)) {
                                    if (product.equals(idProduct)) {
                                        performPurchase(skuDetailsList.get(i));
                                        return;
                                    }
                                }
                            }

                        }
                    }else{
                        System.out.println("Can't querySkuDetailsAsync, responseCode: "+responseCode);
                    }
                }
            });
        } else {
            System.out.println("Billing Client not ready");
        }
    }

    public void performPurchase(SkuDetails details){
        Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);
        for (Purchase sourcePurchase : purchasesResult.getPurchasesList()) {
            if(sourcePurchase != null){
                ConsumeResponseListener listener = (responseCode, purchaseToken) -> {
                };
                mBillingClient.consumeAsync(sourcePurchase.getPurchaseToken(), listener);
            }else{
                System.out.println("null");
            }
        }

        BillingFlowParams billingFlowParams = BillingFlowParams
                .newBuilder()
                .setSkuDetails(details)
                .build();
        mBillingClient.launchBillingFlow(this, billingFlowParams);
    }


// ======================================================================================================================
// ======================================================================================================================

    private void getUrlCharging() {

        ContactItem owns = SessionManager.getProfile(PreviewFrontVideo.this);
        if (owns != null && vid != null){

            String sMsisdn = owns.msisdn;
            String sContentId = vid.content_id;
            String sPrice = vid.price;

            if (!TextUtils.isEmpty(sMsisdn) && !TextUtils.isEmpty(sContentId) && !TextUtils.isEmpty(sPrice)){
                if (!PreviewFrontVideo.this.isFinishing()) {
//                    progressDialog.show();
                    Call<APIResponse<String>> call = ServicesFactory.getService().getUrlChargingTsel(sMsisdn, sContentId, sPrice);
                    call.enqueue(new Callback<APIResponse<String>>() {
                        @Override
                        public void onResponse(Call<APIResponse<String>> call, Response<APIResponse<String>> response) {
                            if (response.isSuccessful() && response.body() != null) {
                                urlCharging = response.body().data;
                                if (!TextUtils.isEmpty(urlCharging)) {

                                    ArrayList<DataChargingContent> getSubData = SessionManager.getChargeArrayList(getApplicationContext());
                                    if (getSubData != null && getSubData.size()>0) {
                                        String sChannel = getSubData.get(0).getChannel(); // check methode payment g-pay...
                                        if (!TextUtils.isEmpty(sChannel) && sChannel.equalsIgnoreCase("weboptin_otp")){
                                            startActivity(new Intent(PreviewFrontVideo.this, TselActivity.class).putExtra("sUrl", urlCharging));
                                        }else {
                                            if (!PreviewFrontVideo.this.isFinishing()) {
                                                WebView webView = new WebView(PreviewFrontVideo.this);
                                                webView.setWebViewClient(new TselWebViewClient());
                                                webView.loadUrl(urlCharging);
                                            }
                                        }
                                    }


                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<APIResponse<String>> call, Throwable t) {
                            if (!PreviewFrontVideo.this.isFinishing()) {
//                                progressDialog.dismiss();
                                Toast.makeText(PreviewFrontVideo.this, model.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }

        }
    }

    private class TselWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String urlWeb) {
            if (!PreviewFrontVideo.this.isFinishing()) {
                urlCharging = urlWeb;
                super.onPageFinished(view, urlCharging);
//                progressDialog.dismiss();
                //after dissmiss, popup USSD TSEL should shown
            }
        }
    }

    private void checkContentPurchase(String msisdn, String content_id, CheckStatusListener listener){
        Call<APIResponse<CheckPurchase>> call = ServicesFactory.getService().checkContentPurchase(msisdn, content_id);
        call.enqueue(new Callback<APIResponse<CheckPurchase>>() {
            @Override
            public void onResponse(Call<APIResponse<CheckPurchase>> call, Response<APIResponse<CheckPurchase>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    if (response.body().data != null){
                        status = response.body().data.getStatus();
                        if (!TextUtils.isEmpty(status)){
                            listener.onCheckingContent(status, true);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResponse<CheckPurchase>> call, Throwable t) {
                toast(model.getLang().string.wordingFailureResponse);
                btnSubscribe.setText(model.getLang().string.grabNow);
//                btnSubscribe.setEnabled(false);
//                btnSubscribe.setClickable(false);
            }
        });
    }

    public interface CheckStatusListener{
        void onCheckingContent(String status, boolean isSuccess);
    }
}
