package com.sbimv.mvicalls.timeline;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.ModelContentVideo;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.ArrayList;
import java.util.List;


public class VideoRecycler extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_LOADING = 1;
    public static final int VIEW_TYPE_NORMAL = 0;
    private List<ModelContentVideo> mInfoList;
    private boolean isLoadingAdded = false;
    private Context context;
    private LangViewModel model = LangViewModel.getInstance();

    public VideoRecycler(Context context) {
        this.context = context;
        mInfoList = new ArrayList<>();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false));
            case VIEW_TYPE_LOADING:
                return new LoadingVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return mInfoList == null ? 0 : mInfoList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mInfoList.size() - 1 && isLoadingAdded) ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
    }


    public void onRelease() {
        if (mInfoList != null) {
            mInfoList.clear();
            mInfoList = null;
        }
    }

    public class ViewHolder extends BaseViewHolder {

        private ImageButton mBtnReport;
        private ImageButton mBtnLike;
        private TextView tvCounter;
        public TextView textViewName;
        public TextView tvActionLike;
        public TextView tvActionReport;
        public ImageView mCover;
        public ImageView mImgProfile;
        public ProgressBar mProgressBar;
        public ProgressBar mProgress;
        public final View parent;
        private LinearLayout mLnReport;
        private LinearLayout mLnLike;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.tvName);
            tvCounter = itemView.findViewById(R.id.tvCounter);
            mCover = itemView.findViewById(R.id.cover);
            mImgProfile = itemView.findViewById(R.id.imageViewUser);
            mProgressBar = itemView.findViewById(R.id.progressBar);
            mProgress = itemView.findViewById(R.id.progressBarPaging);
            mLnReport = itemView.findViewById(R.id.ln_report);
            mLnLike = itemView.findViewById(R.id.ln_like);
            mBtnReport = itemView.findViewById(R.id.btnReport);
            mBtnLike = itemView.findViewById(R.id.btnLike);

            tvActionLike = itemView.findViewById(R.id.tvActionLike);
            tvActionReport = itemView.findViewById(R.id.tvActionReport);

            parent = itemView;
        }

        protected void clear() {

        }

        public void onBind(final int position) {
            super.onBind(position);
            parent.setTag(this);

            final ModelContentVideo videoInfo = mInfoList.get(position);
            textViewName.setText(TextUtils.isEmpty(videoInfo.getName()) ? "Unknow..." : videoInfo.getName());

            Glide.with(itemView.getContext()).load(videoInfo.getThumb_video()).into(mCover);

            String uriString = videoInfo.getName_pic();
            uriString = uriString.replaceAll(" ", "%20");

            RequestOptions request = new RequestOptions();
            request.placeholder(R.drawable.userpng);
            Glide.with(itemView.getContext()).load(uriString).apply(request).into(mImgProfile);

            // multi language...
            String wLike = TextUtils.isEmpty(model.getLang().string.timelineLikeButton)
                    ?"Like":model.getLang().string.timelineLikeButton;
            String wReport = TextUtils.isEmpty(model.getLang().string.timelineReportButton)
                    ?"Report":model.getLang().string.timelineReportButton;

            String count = videoInfo.getCounter();
            if (count.equals("0")){
                tvCounter.setVisibility(View.INVISIBLE);
            }else {
                tvCounter.setVisibility(View.VISIBLE);
                tvCounter.setText(count+" "+wLike);
                videoInfo.counter = count;
            }

            tvActionLike.setText(wLike);
            tvActionReport.setText(wReport);

            int likeStatus = videoInfo.like_status;
            if (likeStatus == 1) {
                mBtnLike.setBackground(context.getResources().getDrawable(R.drawable.circle_green_line));
                mBtnLike.setEnabled(false);
            }else {
                mBtnLike.setBackground(context.getResources().getDrawable(R.drawable.bg_circle));
                mBtnLike.setEnabled(true);
            }

            int reportStatus = videoInfo.report_status;
            if (reportStatus == 1) {
                mBtnReport.setBackground(context.getResources().getDrawable(R.drawable.circle_orange_mail));
                mBtnReport.setEnabled(false);
            }else {
                mBtnReport.setBackground(context.getResources().getDrawable(R.drawable.bg_circle));
                mBtnReport.setEnabled(true);
            }

            mBtnReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String urlVideo = videoInfo.getVideo_caller();
                    String callerLike = videoInfo.getCaller_id();
                    ContactItem own = SessionManager.getProfile(context);
                    if (own ==  null)
                        return;
                    sendComment(own.caller_id, callerLike, urlVideo,videoInfo,position,"report");
                    videoInfo.report_status = 1;
                    notifyItemChanged(position);
                }
            });

            mBtnLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String urlVideo = videoInfo.getVideo_caller();
                    String callerLike = videoInfo.getCaller_id();
                    ContactItem own = SessionManager.getProfile(context);
                    if (own ==  null)
                        return;
                    sendComment(own.caller_id, callerLike, urlVideo,videoInfo,position,"like");
                    videoInfo.like_status = 1;
                    notifyItemChanged(position);
                }
            });

        }
    }

    public void sendComment(String callerId, String callLike, String videoLike, final ModelContentVideo videoInfo, final int position, final String type) {

        Intent i = new Intent(context, SyncReportService.class);
        i.putExtra("callerId",callerId);
        i.putExtra("callLike",callLike);
        i.putExtra("videoLike",videoLike);
        i.putExtra("type",type);
        context.startService(i);
    }

    public class LoadingVH extends BaseViewHolder {
        public LoadingVH(View itemView) {
            super(itemView);
        }

        @Override
        protected void clear() {
        }
    }

    public void add(ModelContentVideo r) {
        mInfoList.add(r);
        notifyItemInserted(mInfoList.size() - 1);
    }

    public void addAll(List<ModelContentVideo> moveResults) {
        for (ModelContentVideo result : moveResults) {
            add(result);
        }
    }

    public void remove(ModelContentVideo r) {
        int position = mInfoList.indexOf(r);
        if (position > -1) {
            mInfoList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ModelContentVideo());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mInfoList.size() - 1;
        ModelContentVideo result = getItem(position);

        if (result != null) {
            mInfoList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public ModelContentVideo getItem(int position) {
        return mInfoList.get(position);
    }


}
