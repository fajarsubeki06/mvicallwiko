package com.sbimv.mvicalls.timeline;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ModelResponse;
import com.sbimv.mvicalls.sync.SyncUtils;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncReport {

    public static final String TAG = "SyncReport";
    public static SyncReport instance;

    // private constructor
    private SyncReport(Context applicationContext){
        this.applicationContext = applicationContext;
    }

    // variables
    boolean isProcessing;
    Context applicationContext;

    public static SyncReport getInstance(Context appContext){
        if(instance == null)
            instance = new SyncReport (appContext);
        return instance;
    }

    public boolean isSynchronizing(){
        return isProcessing;
    }

    // Sync...
    public void sync(String callerId,String callLike,String videoLike,String type){
        if(isProcessing)
            return;
//        SyncUtils.showNotificationProgress(applicationContext, "Sending data...");
        isProcessing = true; // set processing flag
        sendReport(callerId,callLike,videoLike,type);
        isProcessing = false; // set processing flag
//        SyncUtils.hideNotificationProgress(applicationContext);

    }

    // Get parsing data API
    private void sendReport(String callerId, String callLike, String videoLike, final String type){
        if (!TextUtils.isEmpty(callerId) && !TextUtils.isEmpty(callLike) && !TextUtils.isEmpty(videoLike) && !TextUtils.isEmpty(type)) {
            Call<APIResponse<ArrayList<ModelResponse>>> call = ServicesFactory.getService().getLikeTimeline(callerId,callLike,videoLike,type);
            call.enqueue(new Callback<APIResponse<ArrayList<ModelResponse>>>() {
                @Override
                public void onResponse(Call<APIResponse<ArrayList<ModelResponse>>> call, Response<APIResponse<ArrayList<ModelResponse>>> response) {
                    if (response.isSuccessful() && response.body().isSuccessful()) {
                        Log.i("Success","Success");
                    }
//                    SyncUtils.hideNotificationProgress(applicationContext);
                }

                @Override
                public void onFailure(Call<APIResponse<ArrayList<ModelResponse>>> call, Throwable t) {
                    Log.i("onfailure_send_regid","Update regID failed");
//                    SyncUtils.hideNotificationProgress(applicationContext);
                }
            });
        }
    }

}
