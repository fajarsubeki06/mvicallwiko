package com.sbimv.mvicalls.timeline;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.ExoPlayerRecycler;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.ModelContentVideo;
import com.sbimv.mvicalls.pojo.ModelDataContent;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;



import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TimelineActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener {

    private VideoRecycler mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ExoPlayerRecycler recyclerViewFeed;
    private boolean firstTime = true;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 10;
    private int currentPage = PAGE_START;
    private ProgressBar mProgressBar;
    private ModelDataContent dataContent;
    private String limit;
    private String lastUpdate;
    private int PAGE_CONTENT;
    private LinearLayoutManager linearLayoutManager;
    private Drawable dividerDrawable;
    private String lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");

        initialize();
        loadContentVideo();


    }

    private void initialize(){
        mSwipeRefreshLayout = findViewById(R.id.swipe_list);
        recyclerViewFeed = findViewById(R.id.recyclerViewFeed);
        mProgressBar = findViewById(R.id.main_progress);

        recyclerViewFeed.setAdapter(mAdapter);
        mAdapter = new VideoRecycler(this);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider_drawable);

        recyclerViewFeed.setLayoutManager(linearLayoutManager);
        recyclerViewFeed.addItemDecoration(new DividerItemDecoration(dividerDrawable));
        recyclerViewFeed.setItemAnimator(new DefaultItemAnimator());

        mSwipeRefreshLayout.setOnRefreshListener(this);

        recyclerViewFeed.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!TextUtils.isEmpty(lastUpdate) && PAGE_CONTENT == TOTAL_PAGES) {
                            loadContentVideo(lastUpdate);
                        }
                    }
                }, 500);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    @Override
    protected void onPause() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                recyclerViewFeed.onPausePlayer();
            }
        });
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (!TimelineActivity.this.isFinishing()) {
            if (recyclerViewFeed != null) {
                finish();
            }
        }
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        if(recyclerViewFeed!=null) {
            recyclerViewFeed.onRelease();
        }
        super.onDestroy();

    }

    private void loadContentVideo() {
        ContactItem contactItem = SessionManager.getProfile(TimelineActivity.this);
        if (contactItem == null)
            return;

            String callerId = contactItem.caller_id;

        if (TextUtils.isEmpty(callerId))
            return;

        Call<APIResponse<ModelDataContent>> call = ServicesFactory.getService().getTimeline(lang, "", callerId);
        call.enqueue(new Callback<APIResponse<ModelDataContent>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<ModelDataContent>> call, @NotNull Response<APIResponse<ModelDataContent>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    mAdapter.clear();
                    recyclerViewFeed.clear();

                    dataContent = response.body().data;

                    limit = dataContent.getLimit();
                    PAGE_CONTENT = Integer.parseInt(limit);
                    lastUpdate = dataContent.getLast_update();

                    List<ModelContentVideo> videoInfoList = dataContent.getContent();
                    recyclerViewFeed.setVideoInfoList(videoInfoList);
                    mProgressBar.setVisibility(View.GONE);
                    mAdapter.addAll(videoInfoList);
                    isLoading = false;

                    initRecycle(mAdapter);

                    mSwipeRefreshLayout.setRefreshing(false);
                    if (firstTime) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recyclerViewFeed.playVideo();
                            }
                        }, 1000);
                        firstTime = false;
                        recyclerViewFeed.scrollToPosition(0);
                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<ModelDataContent>> call, @NotNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void loadContentVideo(String id) {
        ContactItem contactItem = SessionManager.getProfile(TimelineActivity.this);
        if (contactItem == null)
            return;

        String callerId = contactItem.caller_id;

        if (TextUtils.isEmpty(callerId))
            return;

        Call<APIResponse<ModelDataContent>> call = ServicesFactory.getService().getTimeline(lang, id, "");
        call.enqueue(new Callback<APIResponse<ModelDataContent>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<ModelDataContent>> call, @NotNull Response<APIResponse<ModelDataContent>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    mAdapter.removeLoadingFooter();
                    isLoading = false;

                    dataContent = response.body().data;
                    limit = dataContent.getLimit();
                    PAGE_CONTENT = Integer.parseInt(limit);
                    lastUpdate = dataContent.getLast_update();

                    List<ModelContentVideo> contentInfoLists = dataContent.getContent();
                    recyclerViewFeed.setVideoInfoList(contentInfoLists);
                    mAdapter.addAll(contentInfoLists);
                    mSwipeRefreshLayout.setRefreshing(false);

                    initRecycle(mAdapter);

                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<ModelDataContent>> call, @NotNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void initRecycle(VideoRecycler mAdapter) {
        if (currentPage != TOTAL_PAGES)
            mAdapter.addLoadingFooter();
        else
            isLastPage = true;
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                loadContentVideo();
            }
        });
    }
}
