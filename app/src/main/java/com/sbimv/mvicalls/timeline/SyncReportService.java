package com.sbimv.mvicalls.timeline;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

public class SyncReportService extends IntentService {
    private String callerId;
    private String callLike;
    private String videoLike;
    private String type;

    public SyncReportService() {
        super("SyncReportService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if (intent != null) {
            try{
                callerId = intent.getStringExtra("callerId");
                callLike = intent.getStringExtra("callLike");
                videoLike = intent.getStringExtra("videoLike");
                type = intent.getStringExtra("type");
            }catch (Exception e){
                return;
            }
            SyncReport.getInstance(this.getApplicationContext()).sync(callerId,callLike,videoLike,type);
        }

    }
}
