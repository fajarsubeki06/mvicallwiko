package com.sbimv.mvicalls.jobscheduler;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;

import android.os.Build;
import android.util.Log;
import android.widget.Toast;
import com.sbimv.mvicalls.services.AutostartService;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class MJobScheduler extends JobService {

    private MJobExecuter mJobExecuter;

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {

        mJobExecuter = new MJobExecuter()
        {
            @Override
            protected void onPostExecute(String s) {
                Log.i("mJobExecuter","Running");
                callAutoService();
                jobFinished(jobParameters, false);
            }
        };

        mJobExecuter.execute();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        mJobExecuter.cancel(true);
        return false;
    }

    private void callAutoService(){
        AutostartService mSensorService = new AutostartService(getApplicationContext());
        Intent mServiceIntent = new Intent(getApplicationContext(), mSensorService.getClass());
        if (Build.VERSION.SDK_INT >= 26) {
            getBaseContext().startForegroundService(new Intent(mServiceIntent));
            Log.i("callService","Scheduler");
        } else {
            getBaseContext().startService(new Intent(mServiceIntent));
            Log.i("callService","Scheduler");

        }
    }
}
