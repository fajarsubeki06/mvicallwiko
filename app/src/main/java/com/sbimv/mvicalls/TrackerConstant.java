package com.sbimv.mvicalls;

public interface TrackerConstant {

    String SCREEN_HOME = "Home";
    String SCREEN_REGISTER = "Register";
    String SCREEN_AUTH = "Authentication";
    String SCREEN_PERSONAL_VIDEO = "Video Ringtone";
    String SCREEN_PREMIUM_CONTENT_PREVIEW = "Premium Content Preview";
    String SCREEN_UPLOAD_PERSONAL_VIDEO = "Upload Video Ringtone";
    String SCREEN_SUBSCRIPTION_DIALOG = "Subscription";
    String SCREEN_CONTACT = "Contact";
    String SCREEN_CALLER_WINDOW = "Caller Window";
    String SCREEN_INTRO_TWO = "intro two";
    String SCREEN_PROFILE = "Profile";
    String SCREEN_HOW_TO = "How to";
    String SCREEN_BEST_DEAL = "Best Deal";
    String SCREEN_LV_EXPLORE = "Live Explore";
    String SCREEN_REWARD = "Reward";
    String SCREEN_MGM = "MGM";
    String SCREEN_INTRO_PAGE_1 = "Intro Page 1";
    String SCREEN_INTRO_PAGE_2 = "Intro Page 2";
    String SCREEN_OVERLAY_PERMISSION = "Overlay Permission";

    String SCREEN_IN_APP2APP = "in_app2app";
    String SCREEN_OUT_APP2APP = "out_app2app";

    String SCREEN_IN_CALL = "in_call";
    String SCREEN_OUT_CALL = "out_call";

    // SCREEN DEMO
    String SCREEN_DEMO_PROFILE = "edit profil-demo";
    String SCREEN_DEMO_SIMULATOR = "callsimulator-demo";
    String SCREEN_DEMO_SHARE = "tellfriend-demo";
    String SCREEN_DEMO_CONTACT = "kontak-demo";
    String SCREEN_DEMO_FREE_CONTENT = "free content-demo";

    String EVENT_CAT_HOME = "home";
    String EVENT_CAT_REGISTER = "register";
    String EVENT_CAT_AUTH = "authentication";
    String EVENT_CAT_PERSONAL_VIDEO = "personal video";
    String EVENT_CAT_PREMIUM_CONTENT_PREVIEW = "premium content";
    String EVENT_CAT_UPLOAD_PERSONAL_VIDEO = "upload personal video";
    String EVENT_CAT_MY_COLLECTION = "my collection";
    String EVENT_CAT_SUBSCRIPTION_DIALOG = "subscription";
    String EVENT_CAT_CONTACT = "contact";
    String EVENT_CAT_POPUP_MGM = "popup";
    String EVENT_CAT_BANNER_MGM = "banner";
    String EVENT_CAT_CALLER_WINDOW = "caller window";
    String EVENT_CAT_PROFILE = "profile";
    String EVENT_CAT_SMS_VERIFICATION = "verification sms";

    String EVENT_CAT_SAVE_PERSONAL_PROFILE = "save personal profile";
    String EVENT_CAT_VIDEO_CALLER = "video caller";
    String EVENT_CAT_STATUS_CALLER = "status contact";

}
