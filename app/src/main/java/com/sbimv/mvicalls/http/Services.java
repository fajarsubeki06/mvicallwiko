package com.sbimv.mvicalls.http;


import com.sbimv.mvicalls.language.LanguageModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.BackSound;
import com.sbimv.mvicalls.pojo.CheckPurchase;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataAppSettings;
import com.sbimv.mvicalls.pojo.DataBannerPromo;
import com.sbimv.mvicalls.pojo.DataBestDeal;
import com.sbimv.mvicalls.pojo.DataChargingContent;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;
import com.sbimv.mvicalls.pojo.DataDynamicLink;
import com.sbimv.mvicalls.pojo.DataHowTo;
import com.sbimv.mvicalls.pojo.DataHowtoGetPoin;
import com.sbimv.mvicalls.pojo.DataJsonCharging;
import com.sbimv.mvicalls.pojo.DataLike;
import com.sbimv.mvicalls.pojo.DataMGM;
import com.sbimv.mvicalls.pojo.DataRedeemReward;
import com.sbimv.mvicalls.pojo.DataRewards;
import com.sbimv.mvicalls.pojo.DataSubscriberUser;
import com.sbimv.mvicalls.pojo.DataSubscription;
import com.sbimv.mvicalls.pojo.DataVideoHowTo;
import com.sbimv.mvicalls.pojo.DataViewContent;
import com.sbimv.mvicalls.pojo.DataWebCharging;
import com.sbimv.mvicalls.pojo.DataWellcomePush;
import com.sbimv.mvicalls.pojo.InboxModel;
import com.sbimv.mvicalls.pojo.ModelCheckTelko;
import com.sbimv.mvicalls.pojo.ModelDataContent;
import com.sbimv.mvicalls.pojo.ModelOTP;
import com.sbimv.mvicalls.pojo.ModelPopupBanner;
import com.sbimv.mvicalls.pojo.ModelResponse;
import com.sbimv.mvicalls.pojo.ModelTopic;
import com.sbimv.mvicalls.pojo.MyCollection;
import com.sbimv.mvicalls.pojo.SetVideoTone;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface Services {

    /**
     * Get Data Topic Subscribe
     * Added by Yudha Pratama Putra, 26 Des 2018
     */
    @FormUrlEncoded
    @POST("getTopic")
    Call<APIResponse<List<ModelTopic>>> getTopic(@Field("msisdn") String msisdn); // Get data topic subscribe

    @FormUrlEncoded
    @POST("dynamicLink")
    Call<APIResponse<DataDynamicLink>> getDynamicShare(@Field("source") String source,
                                                       @Field("ref") String ref);

    @FormUrlEncoded
    @POST("videoLike")
    Call<APIResponse<ArrayList<DataLike>>> getLike(@Field("caller_id") String caller_id,
                                                   @Field("caller_id_like") String caller_id_like,
                                                   @Field("video_like") String video_like,
                                                   @Field("status") String sttus);

//    @FormUrlEncoded
//    @POST("getLike")
//    Call<APIResponse<ArrayList<DataLike>>> getLikeTimeline(@Field("caller_id") String caller_id,
//                                                   @Field("caller_id_like") String caller_id_like,
//                                                     @Field("video_caller") String video_like);

    @FormUrlEncoded
    @POST("getLike")
    Call<APIResponse<ArrayList<ModelResponse>>> getLikeTimeline(@Field("caller_id") String caller_id,
                                                                @Field("caller_id_like") String caller_id_like,
                                                                @Field("video_caller") String video_like,
                                                                @Field("type") String type);

    @FormUrlEncoded
    @POST("tutorial")
    Call<APIResponse<DataVideoHowTo>> getVideoID(
            @Field("lang") String lang
    );

    @FormUrlEncoded
    @POST("appSetting")
    Call<APIResponse<DataAppSettings>> getAppSettings(
            @Field("lang") String lang
    );

    @FormUrlEncoded
    @POST("bestDeal")
    Call<APIResponse<List<DataBestDeal>>> getBestDeal(@Field("msisdn") String msisdn,
                                                      @Field("deal_id") String deal_id);

    @FormUrlEncoded
    @POST("contentCategory")
    Call<APIResponse<List<DataDinamicTitle>>> getDinamicTitle(
            @Field("msisdn") String msisdn,
            @Field("lang") String lang);

    @FormUrlEncoded
    @POST("contentCategoryNew")
    Call<APIResponse<List<DataDinamicTitle>>> getDinamicTitleNew(
            @Field("msisdn") String msisdn,
            @Field("lang") String lang);

    @FormUrlEncoded
    @POST("contentDetail")
    Call<APIResponse<DataViewContent>> getDetailContent(
            @Field("msisdn") String msisdn,
            @Field("lang") String lang,
            @Field("content_id") String content_id);


    @FormUrlEncoded
    @POST("newListMenu")
    Call<APIResponse<ArrayList<DataChargingContent>>> getNewChargingContent(
            @Field("msisdn") String msisdn,
            @Field("content_id") String content_id,
            @Field("type") String type,
            @Field("lang") String lang
    );

    @FormUrlEncoded
    @POST("frontContent")
    Call<APIResponse<DataDinamicTitle>> getFrontContent(@Field("msisdn") String msisdn,
                                                        @Field("lang") String lang);

    @FormUrlEncoded
    @POST("checkContentPurcase")
    Call<APIResponse<CheckPurchase>> checkContentPurchase(@Field("msisdn") String msisdn,
                                                          @Field("content_id") String content_id);
    /*
     * Regiseter....
     * */
    @FormUrlEncoded
    @POST("register")
    Call<APIResponse<ContactItem>> register(
            @Field("msisdn") String msisdn,
            @Field("referrer") String referrer,
            @Field("os") String os,
            @Field("serial_sim") String imsi,
            @Field("device_id") String device_id
    );

    /*
     * Cakra OTP....
     * */
    @FormUrlEncoded
    @POST("register")
    Call<APIResponse<ContactItem>> cakraOtp(
            @Field("msisdn") String msisdn,
            @Field("referrer") String referrer,
            @Field("os") String os,
            @Field("otp") String otp,
            @Field("serial_sim") String imsi,
            @Field("device_id") String device_id
    );

    /*
    * API getProfile....
    * */
    @FormUrlEncoded
    @POST("getProfile")
    Call<APIResponse<ContactItem>> getProfile(
            @Field("msisdn") String msisdn
    );

    @GET("exampleapi.php")
    Call<List<String>> getProducts();

    @FormUrlEncoded
    @POST("pushCall")
    Call<APIResponse> sendPushCall(
                @Field("caller_id") String caller_id,
            @Field("caller_id_target") String caller_id_target,
            @Field("type") String type
    );
//    @FormUrlEncoded
//    @POST("listMenuSubsBuy")
//    Call<APIResponse<List<DataPopUpSubs>>> getDataPopUpSubs(@Field("msisdn") String msisdn);

    @FormUrlEncoded
    @POST("inboxList")
    Call<APIResponse<List<InboxModel>>> inbox(
            @Field("msisdn") String nope,
            @Field("type") String type,
            @Field("id") String id
    );

    @FormUrlEncoded
    @POST("timelineVideo")
    Call<APIResponse<ModelDataContent>> getTimeline(@Field("lang") String lang,
                                                        @Field("last_updated") String last_updated,
                                                        @Field("caller_id") String caller_id
                                                    );

    @FormUrlEncoded
    @POST("checkSubscribe")
    Call<DataSubscriberUser> getStatusSubscribe(@Field("msisdn") String msisdn);

    @FormUrlEncoded
    @POST("newSubs")
    Call<DataSubscription> getSubscription(
            @Field("sms") String sms,
            @Field("msisdn") String msisdn
    );

    @FormUrlEncoded
    @POST("premiumChargingNew")
    Call<DataWebCharging> getWebCharging(
            @Field("caller_id") String caller_id,
            @Field("content_id") String content_id,
            @Field("price") String price
    );

    // @FormUrlEncoded
    @POST("catalog")
    Call<APIResponse<List<DataDinamicVideos>>> getLatestVideo();

    @Multipart
    @POST("uploadVideo")
    Call<APIResponse> uploadVideo(@Part MultipartBody.Part file,
                                  @Part("caller_id") RequestBody caller_id
    );

    @Multipart
    @POST("updateProfile")
    Call<APIResponse<ContactItem>> updateProfile(
            @Part("caller_id") RequestBody caller_id,
            @Part("name") RequestBody name,
            @Part("email") RequestBody email,
            @Part("profile_status") RequestBody profile_status,
            @Part MultipartBody.Part caller_pic
    );

    @FormUrlEncoded
    @POST("updateStatusCaller")
    Call<APIResponse<String>> updateCallerStatus(
            @Field("caller_id") String caller_id,
            @Field("status_caller") String status_caller
    );

    // Riset for data config...
    @FormUrlEncoded
    @POST("appsConfig")
    Call<APIResponse<ConfigData>> appsConfig(
            @Field("msisdn") String msisdn,
            @Field("showcase_finish") boolean showcase_finish
    );

    @FormUrlEncoded
    @POST("listProfileStatus")
    Call<APIResponse<ContactItem>> listProfileStatus(
            @Field("msisdn") String msisdn
    );

    @FormUrlEncoded
    @POST("updateRegId")
    Call<APIResponse<String>> updateRegId(
            @Field("msisdn") String caller_id,
            @Field("reg_id") String status_caller
    );

    @FormUrlEncoded
    @POST("getDataSyncContact")
    Call<APIResponse<List<ContactItem>>> getContact(
            @Field("caller_id") String caller_id,
            @Field("msisdn") String msisdn
    );

    @FormUrlEncoded
    @POST("getDataSyncContact")
    Call<APIResponse<List<ContactItem>>> getSyncContact(
            @Field("caller_id") String caller_id,
            @Field("msisdn") String msisdn,
            @Field("reg_id") String regid
    );

    @Streaming
    @GET
    Call<ResponseBody> downloadVideo(@Url String fileUrl);

    @FormUrlEncoded
    @POST("addProfile")
    Call<APIResponse<ContactItem>> addProfile(
            @Field("caller_id") String caller_id,
            @Field("msisdn") String msisdn,
            @Field("profile") String profile
    );

    @FormUrlEncoded
    @POST("delProfile")
    Call<APIResponse> deleteProfile(
            @Field("caller_id") String caller_id,
            @Field("msisdn") String msisdn
    );

    @FormUrlEncoded
    @POST("PremiumCharging")
    Call<APIResponse<DataJsonCharging>> getCharging(
            @Field("caller_id") String caller_id,
            @Field("price") String price,
            @Field("content_id") String content_id
    );

    @FormUrlEncoded
    @POST("myCollection")
    Call<APIResponse<List<MyCollection>>> getMyCollection(
            @Field("caller_id") String caller_id
    );

    @FormUrlEncoded
    @POST("contentFree")
    Call<APIResponse<List<DataDinamicVideos>>> getFreContent(
            @Field("msisdn") String msisdn,
            @Field("lang") String lang
    );

    @FormUrlEncoded
    @POST("setVideoTone")
    Call<APIResponse<List<SetVideoTone>>> getVideoTone(
            @Field("caller_id") String caller_id,
            @Field("content_id") String content_id
    );

    @FormUrlEncoded
    @POST("sentMsg")
    Call<APIResponse<List<ModelOTP>>> getOTP(
            @Field("msisdn") String msisdn
    );

    @FormUrlEncoded
    @POST("checkTelco")
    Call<APIResponse<List<ModelCheckTelko>>> checkTelco(
            @Field("msisdn") String msisdn,
            @Field("serial_sim") String serial_sim,
            @Field("app_version") String app_version,
            @Field("device_id") String device_id
    );

    @FormUrlEncoded
    @POST("verifySMS")
    Call<APIResponse<ContactItem>> getVerifySMS(
            @Field("msisdn") String msisdn,
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("chargingFree")
    Call<APIResponse> buyContentFree(
            @Field("msisdn") String msisdn,
            @Field("content_id") String content_id
    );

    @FormUrlEncoded
    @POST("campaign")
    Call<APIResponse> campaign(
            @Field("url") String url,
            @Field("msisdn") String msisdn
    );

    @FormUrlEncoded
    @POST("popupBanner")
    Call<APIResponse<ModelPopupBanner>> popupBanner(
            @Field("msisdn") String msisdn,
            @Field("lang") String lang,
            @Field("os") String os,
            @Field("app_version") String app_version
    );

    @FormUrlEncoded
    @POST("reward")
    Call<APIResponse<DataRewards>> getRewards(@Field("msisdn") String msisdn,
                                              @Field("lang") String lang);

    @FormUrlEncoded
    @POST("redeemReward")
    Call<APIResponse<DataRedeemReward>> getRedeemReward(@Field("msisdn") String msisdn,
                                                        @Field("type") String type,
                                                        @Field("price") String price,
                                                        @Field("reward_id") String reward_id,
                                                        @Field("action") String action,
                                                        @Field("contentID") String contentID);
    @FormUrlEncoded
    @POST("howToGetPoin")
    Call<APIResponse<DataHowtoGetPoin>> getHowtoGetPoin(@Field("lang") String lang);

    @FormUrlEncoded
    @POST("howToUse")
    Call<APIResponse<ArrayList<DataHowTo>>> getTutorial(@Field("lang") String lang);

    @FormUrlEncoded
    @POST("tselBuy")
    Call<APIResponse<String>> getUrlChargingTsel(@Field("msisdn") String msisdn,
                                                 @Field("content_id") String content_id,
                                                 @Field("price") String price);

    @FormUrlEncoded
    @POST("dcb_req")
    Call<APIResponse<String>> reqDcb(@Field("msisdn") String msisdn,
                                                 @Field("content_id") String content_id,
                                                 @Field("price") String price,
                                        @Field("type") String type);

    @FormUrlEncoded
    @POST("dcb_transaction")
    Call<APIResponse<String>> dcbTransaction(@Field("otp") String otp,
                                                @Field("trx_id") String trx_id,
                                                @Field("msisdn") String msisdn,
                                                @Field("price") String price,
                                                @Field("content_id") String content_id,
                                                @Field("type") String type);

    @FormUrlEncoded
    @POST("gpay")
    Call<APIResponse<ArrayList<String>>> getSubsGpay(
             @Field("msisdn") String msisdn,
             @Field("type") String type,
             @Field("product_purchased") String product_purchased,
             @Field("canceled") boolean success,
             @Field("period") String period,
             @Field("price") String price
    );

    @FormUrlEncoded
    @POST("welcomeNotif")
    Call<APIResponse<DataWellcomePush>> getWellcomePush(@Field("lang") String lang);

    @FormUrlEncoded
    @POST("multiLanguage")
    Call<APIResponse<LanguageModel>> getLang(@Field("ccode") String ccode);

    @POST("shareBanner")
    Call<APIResponse<DataBannerPromo>> getBanner();

    @FormUrlEncoded
    @POST("https://api.mvicall.com/index.php/api/getBaseUrl")
    Call<APIResponse<String>> getBaseUrl(@Field("ccode")String ccode);

    @FormUrlEncoded
    @POST("listMemberGetMember")
    Call<APIResponse<DataMGM>> getMGM(@Field("msisdn") String msisdn,
                                      @Field("lang")String lang,
                                      @Field("app_version")String version_app);

    @POST("listCountryCode")
    Call<APIResponse<ArrayList<String>>> getListCountryCode();

    @FormUrlEncoded
    @POST("listBackSound")
    Call<APIResponse<List<BackSound>>> getBackSound(@Field("msisdn") String msisdn);
}
