package com.sbimv.mvicalls.http;

import android.content.Context;
import android.os.Build;

import com.sbimv.mvicalls.BuildConfig;
import com.sbimv.mvicalls.util.PreferenceUtil;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yudha Pratama Putra on 15/08/18.
 */
public class ServicesFactory {
    private static boolean ENABLE_LOGGING = BuildConfig.DEBUG;
    //Production
    private static String TEST_URL = "http://www.kentongandigitalbbf.com/service/";
    private static String PRODUCTION_URL =  BuildConfig.BASE_URL_PRODUCTION;
    private static String LOCAL_URL = "http://192.168.43.99/";
    //Staging
    private static String STAGING_URL = BuildConfig.BASE_URL_STAGING;
    //Staging V2
    private static String STAGING_URL_V2 = BuildConfig.BASE_URL_STAGING_V2;

    /**
     * Staging
     */
    public static Services getServiceStaging() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(STAGING_URL)
                .client(generateClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(Services.class);
    }

    /**
     * Staging V2
     */
    public static Services getServiceStagingV02() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(STAGING_URL_V2)
                .client(generateClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(Services.class);
    }

    /**
     * for get base url only
     */
    public static Services getServiceBaseURL() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PRODUCTION_URL)
                .client(generateClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(Services.class);
    }


    /**
     * Production International
     */
    public static Services getServiceGlobal() {
        /** production **/
        String baseUrl = PreferenceUtil.getInstance().getPref().getString(PreferenceUtil.BASE_URL,BuildConfig.BASE_URL_PRODUCTION);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(generateClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(Services.class);

        /** staging **/
//        return getServiceStaging();
    }

    public static Services getTestService() {
        /** production **/
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TEST_URL)
                .client(generateClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(Services.class);
    }

    /**
     * Production Local
     *
     */
    public static Services getService() {
        /** production **/
        PreferenceUtil preferenceUtil = PreferenceUtil.getInstance();
        if (preferenceUtil == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(PRODUCTION_URL)
                    .client(generateClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(Services.class);
        }else {
            return getServiceGlobal();
        }
        /** staging **/
//        return getServiceStaging();
    }

    public static Services getServices() {
        /** production **/
        PreferenceUtil preferenceUtil = PreferenceUtil.getInstance();
        if (preferenceUtil == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(LOCAL_URL)
                    .client(generateClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit.create(Services.class);
        }else {
            return getServiceGlobal();
        }
        /** staging **/
//        return getServiceStaging();
    }

    private static OkHttpClient generateClient() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS);

        if (ENABLE_LOGGING) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            clientBuilder.addInterceptor(loggingInterceptor);
        }
        return clientBuilder.build();
    }

//    private static OkHttpClient generateClient() {
//        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
//                .connectTimeout(60, TimeUnit.SECONDS)
//                .readTimeout(60, TimeUnit.SECONDS)
//                .writeTimeout(60, TimeUnit.SECONDS);
//
//        if (ENABLE_LOGGING) {
//            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
//            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//            clientBuilder.addInterceptor(loggingInterceptor);
//        }
//        return clientBuilder.build();
//    }

}