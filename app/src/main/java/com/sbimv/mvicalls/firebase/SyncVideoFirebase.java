package com.sbimv.mvicalls.firebase;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.google.firebase.database.DatabaseReference;
import com.sbimv.mvicalls.db.VideoDBManager;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.sync.SyncUtils;
import com.sbimv.mvicalls.util.CallerVideoManager;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.SessionManager;
import java.io.File;
import java.io.IOException;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class SyncVideoFirebase {

    static SyncVideoFirebase instance;
    Context applicationContext;
    private DatabaseReference mDatabase;
    boolean isProcessing;
    private ContactItem own;
    VideoDBManager videoDB;

    private SyncVideoFirebase(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    public static SyncVideoFirebase getInstance(Context appContext) {
        if (instance == null) instance = new SyncVideoFirebase(appContext);
        return instance;
    }

    public void sync(String... arg) {
        if (isProcessing) return;

        own = SessionManager.getProfile(applicationContext);
        isProcessing = true;

        downloadAllVideo(arg[0],arg[1]);

        isProcessing = false;
        SyncUtils.setLastSync(applicationContext);

    }

    private void downloadAllVideo(String... arg){
        // show done sync message
        Log.i("LoadStatus","start...");

        File file;
        if(!TextUtils.isEmpty(arg[0])){

            // skip download video file if exist (added by Hendi, 29-08-2018)
            file = new File(
                    CallerVideoManager.getDirPath(applicationContext),
                    CallerVideoManager.generateFileName(arg[0]));

            if(!file.exists()) {
                // download video file if no exist
                Call<ResponseBody> call = ServicesFactory.getService().downloadVideo(arg[0]);
                try {
                    Response<ResponseBody> response = call.execute();
                    FileUtil.writeResponseBodyToDisk(
                            response.body(),
                            CallerVideoManager.getDirPath(applicationContext),
                            CallerVideoManager.generateFileName(arg[0]));
                    // insert or update to video table (added by Hendi, 29-08-2018)
//                        file = new File(
//                                CallerVideoManager.getDirPath(applicationContext),
//                                CallerVideoManager.generateFileName(arg[0]));
//                        videoDB.insertOrUpdateVideo(arg[1], file.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        Log.i("LoadStatus","done...");
    }

}
