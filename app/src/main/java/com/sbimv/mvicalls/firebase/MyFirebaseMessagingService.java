package com.sbimv.mvicalls.firebase;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.BuildConfig;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.SplashActivity;
import com.sbimv.mvicalls.activity.Utility;
import com.sbimv.mvicalls.apprtc.SinchService;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.services.SyncCurationVideos;
import com.sbimv.mvicalls.sync.SyncUtils;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.SinchHelpers;
import com.sinch.android.rtc.calling.CallNotificationResult;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private NotificationUtils notificationUtils;
    private final String PREFERENCE_FILE = "com.sinch.android.rtc.sample.push.shared_preferences";
    SharedPreferences sharedPreferences;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage == null)
            return;

        String strSound = remoteMessage.getData().get("sound");
        if (!TextUtils.isEmpty(strSound)){

            String title = remoteMessage.getData().get("title");
            String message = remoteMessage.getData().get("body");
            String direction = remoteMessage.getData().get("direction");
            String timestamp = null;

            if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(message)) {
                Intent pushNotification = new Intent(ConfigPref.PUSH_NOTIFICATION);
                pushNotification.putExtra("title", title);
                pushNotification.putExtra("message", message);
                pushNotification.putExtra("timestamp", timestamp);
                pushNotification.putExtra("direction", direction);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                showNotificationMessage(getApplicationContext(), title, message, timestamp, pushNotification);

            }

            return;
        }

        Object datas = remoteMessage.getData();
        if (datas!= null) {

            String sCuration = datas.toString();
            if (!TextUtils.isEmpty(sCuration)) {
                if (sCuration.equalsIgnoreCase("{status=Approved}")) {
                    startService(new Intent(getApplicationContext(), SyncCurationVideos.class));
                    return;
                }else if (sCuration.equalsIgnoreCase("{status=Logout}")) {
                    if(!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                        Utility.getInstance(getApplicationContext()).logoutApp(true);
                    }else {
                        Utility.getInstance(getApplicationContext()).logoutApp(false);
                    }
                    return;
                }
            }
        }

        Map<String, String> data = remoteMessage.getData();

        if (data!=null) {

            if (SinchHelpers.isSinchPushPayload(data)) {
                new ServiceConnection() {
                    private Map payload;

                    @Override
                    public void onServiceConnected(ComponentName name, IBinder service) {
                        Context context = getApplicationContext();
                        sharedPreferences = context.getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);

                        if (payload != null) {
                            SinchService.SinchServiceInterface sinchService = (SinchService.SinchServiceInterface) service;
                            if (sinchService != null) {
                                NotificationResult result = sinchService.relayRemotePushNotificationPayload(payload);
                                if (result != null) {
                                    if (result.isValid() && result.isCall()) {
                                        CallNotificationResult callResult = result.getCallResult();
                                        if (callResult != null && result.getDisplayName() != null) {
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.putString(callResult.getRemoteUserId(), result.getDisplayName());
                                            editor.apply();
                                        }
                                    }
                                }
                            }
                        }
                        payload = null;
                    }

                    @Override
                    public void onServiceDisconnected(ComponentName name) {
                    }

                    void relayMessageData(Map<String, String> data) {
                        payload = data;
                        getApplicationContext().bindService(new Intent(getApplicationContext(), SinchService.class), this, BIND_AUTO_CREATE);
                    }
                }.relayMessageData(data);

            }

        }

        handleMessage(remoteMessage);


    }

    private void handleMessage(RemoteMessage rm) {
        if(rm.getNotification() == null) return; // terminate process notification payload null

        // initialize value
        String title = rm.getNotification().getTitle();
        String message = rm.getNotification().getBody();
        String timestamp = null;
        String direction = null;
        String imageUrl = null;

        // add data payload
        Map<String, String> data = rm.getData();
        if(data!= null && data.size()>0){
            timestamp = data.get("timestamp");
            direction = data.get("direction");
            imageUrl = data.get("image");
        }

        // send broadcast
        Intent pushNotification = new Intent(ConfigPref.PUSH_NOTIFICATION);
        pushNotification.putExtra("title", title);
        pushNotification.putExtra("message", message);
        pushNotification.putExtra("timestamp", timestamp);
        pushNotification.putExtra("direction", direction);
        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

        // show notification at notification center if app in background (OPTIONAL)
        if(!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            if (TextUtils.isEmpty(imageUrl)) {
                showNotificationMessage(getApplicationContext(), title, message, timestamp, pushNotification);
            } else {
                showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, pushNotification, imageUrl);
            }
        }

    }

    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        try {

            Log.e(TAG, "sendRegistrationToServer: " + s);
            PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.TOKEN_FCM, s).commit();

            ContactItem own = SessionManager.getProfile(getApplicationContext());
            if (own == null)
                return;

            Call<APIResponse<String>> call = ServicesFactory.getService().updateRegId(own.caller_id, s);
            call.enqueue(new Callback<APIResponse<String>>() {
                @Override
                public void onResponse(@NotNull Call<APIResponse<String>> call, @NotNull Response<APIResponse<String>> response) {
                    if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                        Log.i("wordUpdateRegId", "Update regID success");
                    } else {
                        Log.i("wordUpdateRegId", "Update regID failed");
                    }
                }

                @Override
                public void onFailure(@NotNull Call<APIResponse<String>> call, @NotNull Throwable t) {
                    Log.i("onfailure_send_regid", "Update regID failed");
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
