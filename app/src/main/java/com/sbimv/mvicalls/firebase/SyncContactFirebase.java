package com.sbimv.mvicalls.firebase;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.db.VideoDBManager;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.services.SyncFirebaseVideoServices;

import java.util.Collections;
import java.util.List;

public class SyncContactFirebase {

    static SyncContactFirebase instance;
    private String caller_id;
    private String video_caller;
    private String nameCaller;
    private Cursor cursor;
    private ContactDBManager db;

    // private constructor
    private SyncContactFirebase(Context applicationContext){
        this.applicationContext = applicationContext;
        this.videoDB = VideoDBManager.getInstance(applicationContext);
    }

    // variables
    boolean isProcessing;
    Context applicationContext;
    VideoDBManager videoDB;

    public static SyncContactFirebase getInstance(Context appContext){
        if(instance == null)
            instance = new SyncContactFirebase(appContext);
        return instance;
    }

    public boolean isSynchronizing(){
        return isProcessing;
    }

    public void sync(){

        if(isProcessing)
            return;

        isProcessing = true; // set true processing flag
        getContact();
        isProcessing = false; // set false processing flag

    }

    private void getContact(){
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("MviContact");
//        Query recentPostQuery = mDatabase.limitToFirst(100);
        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                if (dataSnapshot != null && dataSnapshot.exists()) {
//
//                    Object sNameCaller = dataSnapshot.child("name").getValue();
//                    Object sCallerID = dataSnapshot.child("caller_id").getValue();
//                    Object sVideoCaller = dataSnapshot.child("video_caller").getValue();
//
//                    if (sNameCaller != null && sCallerID != null && sVideoCaller != null){
//                        nameCaller = sNameCaller.toString();
//                        caller_id = sCallerID.toString();
//                        video_caller = sVideoCaller.toString();
//
//                        if (getCallerID(caller_id) == true){
//                            List<ContactItem> contact = Collections.singletonList(dataSnapshot.getValue(ContactItem.class));
//                            saveIntoDB(contact,video_caller,caller_id);
//                        }
//
//                    }
//
//                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot != null && dataSnapshot.exists()) {

                    Object sNameCaller = dataSnapshot.child("name").getValue();
                    Object sMsisdn = dataSnapshot.child("msisdn").getValue();
                    Object sCallerID = dataSnapshot.child("caller_id").getValue();
                    Object sVideoCaller = dataSnapshot.child("video_caller").getValue();

                    if (sNameCaller != null && sMsisdn != null && sCallerID != null && sVideoCaller != null){
                        nameCaller = sNameCaller.toString();
                        String msisdn = sMsisdn.toString();
                        caller_id = sCallerID.toString();
                        video_caller = sVideoCaller.toString();

                        if (getCallerID(msisdn)){
                            List<ContactItem> contact = Collections.singletonList(dataSnapshot.getValue(ContactItem.class));
                            saveIntoDB(contact,video_caller,caller_id);
//                            SyncUtils.showStatusNotification(applicationContext, caller_id,"Perubahan kontak status",nameCaller+" melakukan perubahan pada profile nya..."); // Riset for notif change user data profile...
                        }
                    }

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void saveIntoDB(List<ContactItem> contacts,String... arg){
        try{
            db = ContactDBManager.getInstance(applicationContext);
        }catch (Exception e){
            return;
        }
        if (db != null) {
            if (contacts != null && contacts.size()>0) {
                for (ContactItem item : contacts) {

                    String name = item.name;
                    if (TextUtils.isEmpty(name)) {
                        String phonenum = item.msisdn;
                        item.name = "~ " + selectionContactDetail(phonenum);
                    }

//                    String phonenum = item.msisdn;
//                    item.name_phone_book = selectionContactDetail(phonenum); // added name phone book

                    db.insertContact(item);
                    Intent i = new Intent("com.sbimv.mvicalls.UPDATE_FRIEND");
                    LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(i);
                }

                if (!TextUtils.isEmpty(arg[0]) && !TextUtils.isEmpty(arg[1])) {
                    Intent syncVideoIntent = new Intent(applicationContext, SyncFirebaseVideoServices.class);
                    syncVideoIntent.putExtra("video_caller", arg[0]);
                    syncVideoIntent.putExtra("caller_id", arg[1]);
                    applicationContext.startService(syncVideoIntent);
                }

            }
        }

    }

    private static boolean getCallerID(String... arg) {
        boolean found = false;
        try {
            ContactDBManager contactDBManager = new ContactDBManager(instance.applicationContext);
            Cursor data = contactDBManager.getCallerMSISDN(arg[0]);
            if (data != null) {
                found = true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return found;
    }

    // Validate when the username is empty filed in the name of the phone book contact... - added by Yudha Pratama Putra, 06 November 2018
    private String selectionContactDetail(String msisdn){
        ContentResolver cr = applicationContext.getContentResolver();
        String selection = "(" + ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + msisdn + "%') OR (" + ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER + " LIKE '%" + msisdn + "%')";
        String[] projection = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER};

        try{
            cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, selection, null, null);
        }catch (Exception e){
            e.printStackTrace();
        }

        if (cursor!=null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                if (!TextUtils.isEmpty(name)) {
                    return name;
                }
            }
        }
        cursor.close();

        return "Unknown";
    }

}
