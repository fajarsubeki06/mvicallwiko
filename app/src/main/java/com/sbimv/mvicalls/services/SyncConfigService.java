package com.sbimv.mvicalls.services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;

import com.sbimv.mvicalls.sync.SyncConfig;


public class SyncConfigService extends IntentService {

    public SyncConfigService() {
        super("SyncConfigService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        // sync in all network
        SyncConfig.getInstance(this.getApplicationContext()).sync();
    }
}
