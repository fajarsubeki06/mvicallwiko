package com.sbimv.mvicalls.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.view.View;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;

import java.util.Timer;
import java.util.TimerTask;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

public class AutostartService extends Service {
    final String CHANNEL_ID = "101";
    final String CHANNEL_NAME = this.getClass().getSimpleName();

    private Timer timer;
    private TimerTask timerTask;
    private Context context;
    private int counter;
    private LangViewModel model = LangViewModel.getInstance();

    public AutostartService() {}

    public AutostartService(Context applicationContext) {
        super();
        context = applicationContext;
    }

    @Override
    public void onCreate() {
        startForeground();
        startTimer();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Intent broadcastIntent = new Intent(".receiverNetwork.SensorRestarterBroadcastReceiver");
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Log.i("LOW", "ondestroy!");
        stoptimertask();
        Intent broadcastIntent = new Intent(".receiverNetwork.SensorRestarterBroadcastReceiver");
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stoptimertask();
        Intent broadcastIntent = new Intent(".receiverNetwork.SensorRestarterBroadcastReceiver");
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public boolean stopService(Intent name) {
        stoptimertask();
        Intent broadcastIntent = new Intent(".receiverNetwork.SensorRestarterBroadcastReceiver");
        sendBroadcast(broadcastIntent);
        return super.stopService(name);
    }

    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 1000, 1000); //
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
            }
        };
    }

    public void stoptimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void startForeground() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel();
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID );
        @SuppressLint("WrongConstant") Notification notification = notificationBuilder
                .setOngoing(true)
                .setSmallIcon(R.mipmap.icx_notif)
                .setPriority(PRIORITY_MIN)
                .setAutoCancel(false)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentTitle("MViCall")
                .setTicker("MViCall")
                .setContentText(model.getLang().string.wordingForeground)
                .setVisibility(View.GONE)
                .build();
        startForeground(100, notification);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createNotificationChannel() {
        NotificationChannel chan = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);
    }
}