package com.sbimv.mvicalls.services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.firebase.SyncContactFirebase;
import com.sbimv.mvicalls.util.PermissionUtil;


public class SyncFirebaseService extends IntentService {

    public SyncFirebaseService() {
        super("SyncContactServices");
    }



    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        // sync in all network
        if(PermissionUtil.hashPermission(getApplicationContext(), BaseActivity.permissions)){
            SyncContactFirebase
                    .getInstance(this.getApplicationContext())
                    .sync();
        }

    }
}