package com.sbimv.mvicalls.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.SplashActivity;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.receiver.DailyNotifReceiver;
import com.sbimv.mvicalls.util.PreferenceUtil;

public class DailyReminderPushNotifService extends IntentService {
    static LangViewModel model = LangViewModel.getInstance();

    public DailyReminderPushNotifService() {
        super("DailyNotification");
    }

    //handle notification
    public static void sendNotification(final Context context) {
        String notif_title;
        String notif_content;

        boolean isRegistered = PreferenceUtil.getPref(context).getBoolean(PreferenceUtil.IS_REGISTERED, false);
        if (!isRegistered) {

            notif_title = model.getLang().string.thanks;
            notif_content = model.getLang().string.wordingWellcomePush;
        } else {

            notif_title = model.getLang().string.oneStepMore;
            notif_content = model.getLang().string.contentWordingScase;
        }

        Intent notifIntent = new Intent(context, SplashActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notifIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        final NotificationManager notificationManagerCompat = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String NOTIFICATION_CHANNEL_ID = "mvi_id";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.icx_notif)
                .setContentTitle(notif_title)
                .setContentText(notif_content)
                .setContentIntent(contentIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notif_content))
                .setAutoCancel(true)
                .setSound(alarmSound);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String NOTIFICATION_CHANNEL_NAME = "mvi_channel";
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH);
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);

            if (notificationManagerCompat != null) {
                notificationManagerCompat.createNotificationChannel(channel);
            }
        }

        final Notification notification = builder.build();
        if (notificationManagerCompat != null) {
            int NOTIFICATION_ID = 1;
            notificationManagerCompat.notify(NOTIFICATION_ID, notification);
        }
    }

    public static void runNewAlarmManager(Context context, long interval, int new_reqCode) {
        Intent newAlarmIntent = new Intent(context, DailyNotifReceiver.class);
        newAlarmIntent.putExtra("req_code", new_reqCode);
        PendingIntent newPendingIntent = PendingIntent.getBroadcast(context, new_reqCode, newAlarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        int SDK_INT = Build.VERSION.SDK_INT;
        if (SDK_INT < Build.VERSION_CODES.KITKAT)
            manager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, interval, newPendingIntent);
        else if (Build.VERSION_CODES.KITKAT <= SDK_INT && SDK_INT < Build.VERSION_CODES.M)
            manager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, interval, newPendingIntent);
        else if (SDK_INT >= Build.VERSION_CODES.M) {
            manager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, interval, newPendingIntent);
        }
    }

    public static void cancelPrevAlarmManager(Context context, int requestCode) {
        Intent notifItent = new Intent(context, DailyNotifReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, requestCode, notifItent, PendingIntent.FLAG_UPDATE_CURRENT);
        pendingIntent.cancel();
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        int reqCode = PreferenceUtil.getPref(getApplicationContext()).getInt(PreferenceUtil.REQ_CODE, 0);

//        final int ONEDAY = 2 * 60 * 1000;
//        final int THREEDAY = 3 * 60 * 1000;
//        final int SEVENDAYS = 4 * 60 * 1000;

        final int ONEDAY = 16 * 60 * 60 * 1000;
        final int THREEDAY = 40 * 60 * 60 * 1000;
        final int SEVENDAYS = 88 * 60 * 60 * 1000;

        if (reqCode == 8) { // push 8 hours
            //update FLAG
            cancelPrevAlarmManager(getApplicationContext(), reqCode);
            sendNotification(getApplicationContext());
            runNewAlarmManager(getApplicationContext(), SystemClock.elapsedRealtime() + ONEDAY, 11);
            PreferenceUtil.getEditor(getApplicationContext()).putInt(PreferenceUtil.REQ_CODE, 11).commit();

        } else if (reqCode == 11) { // push 1 day
            //update FLAG
            cancelPrevAlarmManager(getApplicationContext(), reqCode);
            sendNotification(getApplicationContext());
            runNewAlarmManager(getApplicationContext(), SystemClock.elapsedRealtime() + THREEDAY, 22);
            PreferenceUtil.getEditor(getApplicationContext()).putInt(PreferenceUtil.REQ_CODE, 22).commit();

        } else if (reqCode == 22) { // push 3 days
            //update FLAG
            cancelPrevAlarmManager(getApplicationContext(), reqCode);
            sendNotification(getApplicationContext());
            runNewAlarmManager(getApplicationContext(), SystemClock.elapsedRealtime() + SEVENDAYS, 44);
            PreferenceUtil.getEditor(getApplicationContext()).putInt(PreferenceUtil.REQ_CODE, 44).commit();
        } else if (reqCode == 44) { // push 7 days
            //update FLAG
            cancelPrevAlarmManager(getApplicationContext(), reqCode);
            sendNotification(getApplicationContext());
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.FINISH_SCASE, true).commit();
        }
    }

}
