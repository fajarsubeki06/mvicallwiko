package com.sbimv.mvicalls.services;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.sbimv.mvicalls.sync.SyncCuration;


public class SyncCurationVideos extends IntentService {

    public SyncCurationVideos() {
        super("SyncCurationVideos");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        SyncCuration
                .getInstance(this.getApplicationContext())
                .sync();
    }
}
