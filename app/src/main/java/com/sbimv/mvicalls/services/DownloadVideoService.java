package com.sbimv.mvicalls.services;

import android.app.IntentService;
import android.content.Intent;

import com.sbimv.mvicalls.sync.DownloadVideoProfile;
import com.sbimv.mvicalls.util.PreferenceUtil;

public class DownloadVideoService extends IntentService {

    public DownloadVideoService() {
        super("DownloadVideoService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        boolean firstSyncVideoProfile = PreferenceUtil.getPref(this)
                .getBoolean(PreferenceUtil.FIRST_SYNC_DEFAULT_VIDEO, false);

        if (!firstSyncVideoProfile) {
            DownloadVideoProfile
                    .getInstance(this.getApplicationContext())
                    .firstsync();

            PreferenceUtil.getEditor(getApplicationContext())
                    .putBoolean(PreferenceUtil.FIRST_SYNC_DEFAULT_VIDEO, true).commit();

        }else {
            DownloadVideoProfile.getInstance(this.getApplicationContext()).sync();
        }

    }

}
