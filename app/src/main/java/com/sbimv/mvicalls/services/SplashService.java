package com.sbimv.mvicalls.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;

import com.sbimv.mvicalls.activity.SplashActivity;

public class SplashService extends Service {
    public SplashService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                Intent intent1 = new Intent(SplashService.this,SplashActivity.class);
//                startActivity(intent1);
//            }
//        },3000);
        Intent intent1 = new Intent(SplashService.this,SplashActivity.class);
        startActivity(intent1);
        return START_STICKY;
    }
}
