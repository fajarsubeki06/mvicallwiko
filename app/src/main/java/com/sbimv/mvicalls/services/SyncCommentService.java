package com.sbimv.mvicalls.services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;
import com.sbimv.mvicalls.sync.SyncComment;


public class SyncCommentService extends IntentService {
    private String id;

    public SyncCommentService() {
        super("SyncCommentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if (intent != null) {
            try{
                id = intent.getStringExtra("id_uploader");
            }catch (Exception e){
                return;
            }
            SyncComment.getInstance(this.getApplicationContext()).sync(id);
        }

    }
}
