package com.sbimv.mvicalls.services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.sync.SyncContactManager;
import com.sbimv.mvicalls.util.PermissionUtil;

public class SyncContactServices extends IntentService {

    public SyncContactServices() {
        super("SyncContactServices");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        // sync in all network
        if(PermissionUtil.hashPermission(getApplicationContext(), BaseActivity.permissions)){
            SyncContactManager
                    .getInstance(this.getApplicationContext())
                    .sync();
        }

    }
}
