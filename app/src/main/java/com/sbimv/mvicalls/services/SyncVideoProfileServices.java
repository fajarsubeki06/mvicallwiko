package com.sbimv.mvicalls.services;

import android.app.IntentService;
import android.content.Intent;
import com.sbimv.mvicalls.sync.SyncContactProfileManager;
import com.sbimv.mvicalls.util.PreferenceUtil;

public class SyncVideoProfileServices extends IntentService {

    public SyncVideoProfileServices() {
        super("SyncVideoProfileServices");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        boolean firstSyncVideoProfile = PreferenceUtil.getPref(this)
                .getBoolean(PreferenceUtil.FIRST_SYNC_VIDEO_CONTENT, false);

        if (firstSyncVideoProfile == false) {

            SyncContactProfileManager
                    .getInstance(this.getApplicationContext())
                    .firstsync();

            PreferenceUtil.getEditor(getApplicationContext())
                    .putBoolean(PreferenceUtil.FIRST_SYNC_VIDEO_CONTENT, true).commit();

        }else {
            SyncContactProfileManager
                    .getInstance(this.getApplicationContext())
                    .sync();
        }

    }

}
