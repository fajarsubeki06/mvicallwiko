package com.sbimv.mvicalls.services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;
import com.sbimv.mvicalls.sync.SyncVideoManager;


public class SyncVideoServices extends IntentService {

    public SyncVideoServices() {
        super("SyncVideoServices");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        // sync in all network
        SyncVideoManager
                .getInstance(this.getApplicationContext())
                .sync();

    }
}
