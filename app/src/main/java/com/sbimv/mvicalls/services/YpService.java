package com.sbimv.mvicalls.services;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;


public class YpService extends JobService {

    BackgroundTask backgroundTask;

    @Override
    public boolean onStartJob(final JobParameters job) {

        backgroundTask = new BackgroundTask()
        {
            @Override
            protected void onPostExecute(String s) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    getApplicationContext().startForegroundService(new Intent(getApplicationContext(), AutostartService.class));
                } else {
                    getApplicationContext().startService(new Intent(getApplicationContext(), AutostartService.class));
                }

                Log.i("callService","JobDispatcher");
                jobFinished(job,false);
            }
        };

        backgroundTask.execute();

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return true;
    }

    public static class BackgroundTask extends AsyncTask<Void,Void,String> {

        @Override
        protected String doInBackground(Void... voids) {
            return "MVICALL from background jobs";
        }
    }



}
