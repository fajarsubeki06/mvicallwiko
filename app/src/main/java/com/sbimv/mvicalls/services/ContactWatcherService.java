package com.sbimv.mvicalls.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.contact.ContactListPair;
import com.sbimv.mvicalls.contact.ContactObserver;
import com.sbimv.mvicalls.contact.FindAddedContactTask;
import com.sbimv.mvicalls.contact.OnAddedContactFound;
import com.sbimv.mvicalls.contact.OnContactChanged;
import com.sbimv.mvicalls.sync.SyncContactManager;

import java.util.ArrayList;

public class ContactWatcherService extends Service implements OnContactChanged, OnAddedContactFound {

    ContactObserver observer;
    BroadcastReceiver screenOnReceiver;
    ArrayList<Long> oldContacts;

    public ContactWatcherService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        PreferenceManager.setDefaultValues(getApplicationContext(), R.xml.preferences, false);

        screenOnReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                context.startService(new Intent(context, ContactWatcherService.class));
            }
        };

        registerReceiver(screenOnReceiver, new IntentFilter(Intent.ACTION_SCREEN_ON));

        oldContacts = getContacts();
        observer = new ContactObserver(this);
        getApplicationContext().getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_VCARD_URI, false, observer);
    }

    @Override
    public void onContactChanged(Uri contact) {
        ContactListPair clp = new ContactListPair(oldContacts, getContacts());
        FindAddedContactTask task = new FindAddedContactTask(this);
        task.execute(clp);
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(screenOnReceiver);
        getApplicationContext().getContentResolver().unregisterContentObserver(observer);
    }

    @SuppressLint("NewApi")
    @Override
    public void onAddedContactFound(Long contact) {
        oldContacts = getContacts();

        if (contact == -1)
            return;

        if (!SyncContactManager.getInstance(getApplicationContext()).isSynchronizing()) {
            Intent i = new Intent(this, SyncContactServices.class);
            startService(i);
        }

    }

    public ArrayList<Long> getContacts() {
        ArrayList<Long> contacts = new ArrayList<Long>();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if(cur.getCount() > 0) {
            while(cur.moveToNext()) {
                contacts.add(cur.getLong(cur.getColumnIndex(ContactsContract.Contacts._ID)));
            }
        }

        return contacts;
    }

    public String getPhoneNumber(long id) {
        ArrayList<String> phones = new ArrayList<String>();
        ContentResolver m = getContentResolver();
        Cursor cursor = m.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                new String[]{""+id}, null);

        while (cursor.moveToNext())
        {
            phones.add(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
        }

        cursor.close();

        return null;
    }

}
