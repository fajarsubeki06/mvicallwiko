package com.sbimv.mvicalls.services;

import android.app.IntentService;
import android.content.Intent;
import androidx.annotation.Nullable;
import com.sbimv.mvicalls.firebase.SyncVideoFirebase;

public class SyncFirebaseVideoServices extends IntentService {
    private String videoCall;
    private String callerID;

    public SyncFirebaseVideoServices() {
        super("SyncFirebaseVideoServices");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if (intent != null) {
            try{
                videoCall = intent.getStringExtra("video_caller");
                callerID = intent.getStringExtra("caller_id");
            }catch (NullPointerException e){
                e.printStackTrace();
            }
            // sync in all network
            SyncVideoFirebase
                    .getInstance(this.getApplicationContext())
                    .sync(videoCall,callerID);
        }

    }
}
