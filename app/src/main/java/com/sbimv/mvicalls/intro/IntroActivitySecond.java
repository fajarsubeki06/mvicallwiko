package com.sbimv.mvicalls.intro;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.LocaleHelper;
import com.sbimv.mvicalls.PreviewFrontVideo;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.activity.MainActivity;
import com.sbimv.mvicalls.activity.PopUpSubsActivity;
import com.sbimv.mvicalls.activity.PopupVietSubsActivity;
import com.sbimv.mvicalls.activity.TransactionOtpActivity;
import com.sbimv.mvicalls.activity.TselActivity;
import com.sbimv.mvicalls.billings.SubscriptionGoogleManager;
import com.sbimv.mvicalls.fragment.Intro9;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataChargingContent;
import com.sbimv.mvicalls.pojo.DataSubs;
import com.sbimv.mvicalls.pojo.GpayData;
import com.sbimv.mvicalls.receiver.DailyNotifReceiver;
import com.sbimv.mvicalls.services.SyncConfigService;
import com.sbimv.mvicalls.sync.SyncConfig;
import com.sbimv.mvicalls.sync.SyncUtils;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;



import java.util.ArrayList;

import agency.tango.materialintroscreen.BuildConfig;
import agency.tango.materialintroscreen.MaterialIntroActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class IntroActivitySecond extends MaterialIntroActivity {

    private String urlCharging = "";
    private ProgressDialog progressDialog;
    private boolean isData;
    private int exit = 0;
    private boolean isOnline = true;
    private WebView webView;
    private LangViewModel model = LangViewModel.getInstance();
    private SubscriptionGoogleManager subscriptionGoogleManager;
    private String keyword;

    private boolean isDrawOverlay = false;
    int clickcount=0;
    private long mLastClickTime = 0;

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isData) {
                if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
                    final Thread thread = new Thread(){
                        @Override
                        public void run() {
                            try {
                                while (!isConnected(IntroActivitySecond.this)){
                                    int wait = 1000;

                                    Thread.sleep(wait);
                                    exit += wait;
                                    if (exit == 10000){
//                                        progressDialog.dismiss();
                                        IntroActivitySecond.this.runOnUiThread(new Runnable() {
                                            public void run() {
                                                Toast.makeText(IntroActivitySecond.this, model.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        isOnline = false;
                                        break;
                                    }
                                }

                                if (isOnline){
                                    getwebCharging();
                                    isData = false;
                                }

                            }catch (Exception e){
//                                progressDialog.dismiss();
                                e.printStackTrace();
                            }
                        }
                    };
                    thread.start();
                }
                isData = false;
            }
        }
    };

    private static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(LocaleHelper.onAttach(context));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableLastSlideAlphaExitTransition(false);

        webView = new WebView(IntroActivitySecond.this);
        checkSubscribe();

//        progressDialog = new ProgressDialog(IntroActivitySecond.this);
//        progressDialog.setMessage("Processing...");
//        progressDialog.setCanceledOnTouchOutside(false);


        addSlide(new Intro9());
        cancelnotifAlarm();


        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_INTRO_TWO);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void cancelnotifAlarm() {
        int currentReqCode = PreferenceUtil.getPref(IntroActivitySecond.this).getInt(PreferenceUtil.REQ_CODE, 0);
        Intent notifItent = new Intent(IntroActivitySecond.this, DailyNotifReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(IntroActivitySecond.this, currentReqCode, notifItent, PendingIntent.FLAG_UPDATE_CURRENT);
        pendingIntent.cancel();
        AlarmManager alarmManager = (AlarmManager) IntroActivitySecond.this.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }
        PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.FINISH_SCASE, true).commit();
    }

    // ================================== Check Subscribe... ================================== added by Yudha Pratama Putra, 15 okt 2018
    private void checkSubscribe() {
        ContactItem own = SessionManager.getProfile(IntroActivitySecond.this);
        if (own != null) {
            String strMsisdn = own.msisdn;
            if (!TextUtils.isEmpty(strMsisdn)) {
                BaseActivity.getSubscriptionStatus(IntroActivitySecond.this, strMsisdn, new BaseActivity.SubscriptionStatusListener() {
                    @Override
                    public void onComplete(DataSubs subsData) {
                        if (subsData != null) {
                            boolean isSubsActive = BaseActivity.isSubscribeActive(subsData);
                            boolean isFreemium = BaseActivity.isFreemiumActive(subsData);
                            if (isSubsActive || isFreemium) {
                                boolean isFreeSubs = subsData.isFree_sub();
                                if (isFreeSubs){
                                    showDialogFreeSubs();
                                }
                            } else {
                                getSubsBuy(strMsisdn);
                                AppEventsLogger logger = AppEventsLogger.newLogger(IntroActivitySecond.this);
                                logger.logEvent("fcb_screen_otp_success");
                            }
                        }
                    }

                    @Override
                    public void onFail() {
                        getSubsBuy(strMsisdn);
                    }
                });
            }
        }
    }

    private void showDialogFreeSubs(){
        if (!IntroActivitySecond.this.isFinishing()) {

            String strFreeSubs = TextUtils.isEmpty(model.getLang().string.wordingFreeSubs)
                    ?"Terima kasih, kamu telah berlangganan MViCall Rp 3300/minggu"
                    :model.getLang().string.wordingFreeSubs;

            String strConfirm = TextUtils.isEmpty(model.getLang().string.confirm)
                    ?"Oke"
                    :model.getLang().string.confirm;

            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(IntroActivitySecond.this);
            alertBuilder.setMessage(strFreeSubs);
            alertBuilder.setPositiveButton(strConfirm, null);
            AlertDialog dialog1 = alertBuilder.create();
            dialog1.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button buttonPositive = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                    buttonPositive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!IntroActivitySecond.this.isFinishing()) {
                                dialog.dismiss();
                            }
                        }
                    });
                }
            });
            dialog1.show();
        }
    }

    private void getSubsBuy(String phoneNumber) {
        String lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
        String multiLang = TextUtils.isEmpty(lang)?"":lang;
        Call<APIResponse<ArrayList<DataChargingContent>>> call = ServicesFactory.getService().getNewChargingContent(phoneNumber, "", "sub",multiLang);
        call.enqueue(new Callback<APIResponse<ArrayList<DataChargingContent>>>() {
            @Override
            public void onResponse(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<ArrayList<DataChargingContent>>> call, @org.jetbrains.annotations.NotNull @NotNull Response<APIResponse<ArrayList<DataChargingContent>>> response) {
                if (response.isSuccessful() &&  response.body() != null && response.body().isSuccessful()) {
                    if (!IntroActivitySecond.this.isFinishing()) {
                        ArrayList<DataChargingContent> data = response.body().data;
                        if (data != null && data.size() > 0) {
                            SessionManager.saveChargeArrayList(IntroActivitySecond.this, data); // Save data payment method to local...
                            checkRuleSubscribe(data);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@org.jetbrains.annotations.NotNull Call<APIResponse<ArrayList<DataChargingContent>>> call, @org.jetbrains.annotations.NotNull Throwable t) {

            }
        });
    }

    private void checkRuleSubscribe(ArrayList<DataChargingContent> data){
        if (data.size() ==  1){ // data array == 1

            String channel = data.get(0).getChannel();
            keyword = data.get(0).getKeyword();
            boolean confirmEnable = data.get(0).isConfirm_enable();

            if (confirmEnable){ // If confirm enable = true
                if (!IntroActivitySecond.this.isFinishing()) {
                    String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                    if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                        startActivity(new Intent(IntroActivitySecond.this, PopupVietSubsActivity.class));
                        return;
                    }
                    startActivity(new Intent(IntroActivitySecond.this, PopUpSubsActivity.class));
                }
            }else { // If confirm enable = false

                if (!TextUtils.isEmpty(channel) && channel.equals("umb")) { // Call UMB
                    callChargingContent(keyword);
                }
                else if (channel.equals("ussd")){ // Enable channel USSD for vietnam
                    if (SessionManager.getChargeArrayList(IntroActivitySecond.this) != null) {
//                        progressDialog.show();
                        getwebCharging();
                    }
                }
                else if (!TextUtils.isEmpty(channel) && channel.equals("weboptin") && !Util.isWifiConnected(IntroActivitySecond.this)) { // Call weboptin...
                    if (SessionManager.getChargeArrayList(IntroActivitySecond.this) != null) {
//                        progressDialog.show();
                        getwebCharging();
                    }
                }
                else if (!TextUtils.isEmpty(channel) && channel.equals("weboptin_otp") && !Util.isWifiConnected(IntroActivitySecond.this)) { // Call weboptin...
                    if (SessionManager.getChargeArrayList(IntroActivitySecond.this) != null) {
                        getwebCharging();
                    }
                }

                //*
                // Add payment method DCB for thailand
                // *
                else if (!TextUtils.isEmpty(channel) && channel.equals("dcb")){
                    ContactItem owns = SessionManager.getProfile(IntroActivitySecond.this);
                    if (owns != null) {

                        String sMsisdn = TextUtils.isEmpty(owns.msisdn)?"":owns.msisdn;
                        String sPrice = TextUtils.isEmpty(String.valueOf(data.get(0).getPrice()))?"":String.valueOf(data.get(0).getPrice());
                        String pricesLbl = TextUtils.isEmpty(String.valueOf(data.get(0).getPrice_label()))?"":String.valueOf(data.get(0).getPrice_label());
                        String sType = "sub";

                        if (!TextUtils.isEmpty(sPrice)) {
                            startActivity(new Intent(IntroActivitySecond.this, TransactionOtpActivity.class)
                                    .putExtra("sMisdn", sMsisdn)
                                    .putExtra("sContenId", "")
                                    .putExtra("sPrice", sPrice)
                                    .putExtra("sPriceLabel", pricesLbl)
                                    .putExtra("sType", sType)
                            );
                        }

                    }
                }

                else if (!TextUtils.isEmpty(channel) && channel.equals("weboptin") && Util.isWifiConnected(IntroActivitySecond.this)) { // Call weboptin case wifi...
                    showDialogWebOptin();
                }

                else if (!TextUtils.isEmpty(channel) && channel.equals("weboptin_otp") && Util.isWifiConnected(IntroActivitySecond.this)) { // Call weboptin case wifi...
                    showDialogWebOptin();
                }

                else if (!TextUtils.isEmpty(channel) && channel.equalsIgnoreCase("GOOGLE")) { // Call google payment
                    if (!IntroActivitySecond.this.isFinishing()) {
                        if (!TextUtils.isEmpty(keyword)) {
                            subscriptionGoogleManager = new SubscriptionGoogleManager(IntroActivitySecond.this, subsListener); // check subscriber g-pay...
                        }
                    }
                }
                else {
                    if (!IntroActivitySecond.this.isFinishing()) { // call popup subs.....
                        String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                        if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                            startActivity(new Intent(IntroActivitySecond.this, PopupVietSubsActivity.class));
                            return;
                        }
                        startActivity(new Intent(IntroActivitySecond.this, PopUpSubsActivity.class));
                    }
                }
            }

        }

        else if (data.size() > 1){ // data array > 1
            if (!IntroActivitySecond.this.isFinishing()) { // call popup subs.....
                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                    startActivity(new Intent(IntroActivitySecond.this, PopupVietSubsActivity.class));
                    return;
                }
                startActivity(new Intent(IntroActivitySecond.this, PopUpSubsActivity.class));
            }
        }
    }

    private void showDialogWebOptin(){
        if (!IntroActivitySecond.this.isFinishing()) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(IntroActivitySecond.this);
            alertBuilder.setMessage(model.getLang().string.wordingTurnOffWifiSubs);
            alertBuilder.setPositiveButton(model.getLang().string.turnOffWifiNext, null);
            AlertDialog dialog1 = alertBuilder.create();
            dialog1.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button buttonPositive = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                    buttonPositive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!IntroActivitySecond.this.isFinishing()) {
                                dialog.dismiss();
//                                progressDialog.show();
                                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                                if (wifiManager != null) {
                                    wifiManager.setWifiEnabled(false);
                                }
                                isData = true;
                            }
                        }
                    });
                }
            });
            dialog1.show();
        }
    }

    private void getwebCharging() {
        if (!IntroActivitySecond.this.isFinishing()) {
            ContactItem ctm = SessionManager.getProfile(IntroActivitySecond.this);
            if (ctm != null) {
                String sMsisdn = ctm.msisdn;
                if (!TextUtils.isEmpty(sMsisdn)) {
                    ArrayList<DataChargingContent> getSubData = SessionManager.getChargeArrayList(getApplicationContext());
                    if (getSubData != null && getSubData.size()>0) {
                        String  price = String.valueOf(getSubData.get(0).getPrice());
                        String sPrice = TextUtils.isEmpty(price) ? "" : price;
                        Call<APIResponse<String>> call = ServicesFactory.getService().getUrlChargingTsel(sMsisdn, null, sPrice);
                        call.enqueue(new Callback<APIResponse<String>>() {
                            @Override
                            public void onResponse(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<String>> call, @org.jetbrains.annotations.NotNull @NotNull Response<APIResponse<String>> response) {
                                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                                    if (!IntroActivitySecond.this.isFinishing()) {
                                        urlCharging = response.body().data;
                                        if (!TextUtils.isEmpty(urlCharging)) {
                                            String sChannel = getSubData.get(0).getChannel(); // check methode payment g-pay...
                                            if (!TextUtils.isEmpty(sChannel) && sChannel.equalsIgnoreCase("weboptin_otp")){
                                                startActivity(new Intent(IntroActivitySecond.this, TselActivity.class).putExtra("sUrl", urlCharging));
                                            }else {
                                                if (webView != null) {
                                                    webView.setWebViewClient(new TselWebViewClient());
                                                    webView.loadUrl(urlCharging);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<String>> call, @org.jetbrains.annotations.NotNull @NotNull Throwable t) {
                                if (!IntroActivitySecond.this.isFinishing()) {
//                                    progressDialog.dismiss();
                                    Toast.makeText(IntroActivitySecond.this, model.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    private class TselWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String urlWeb) {
            if (!IntroActivitySecond.this.isFinishing()) {
//                progressDialog.dismiss();
                urlCharging = urlWeb;
                if(!TextUtils.isEmpty(urlCharging)){
                    super.onPageFinished(view, urlCharging);
                }
                //after dissmiss, popup USSD TSEL should shown
            }
        }
    }

    // ================================== Method Call UMB... ================================== added by Yudha Pratama Putra, 15 okt 2018
    private void callChargingContent(String umb) {
        if (!IntroActivitySecond.this.isFinishing()) {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + Uri.encode(umb)));
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(callIntent);
        }
    }

    // ============================================== Check google subscribe ================================================
// ======================================================================================================================
    private SubscriptionGoogleManager.SubscriptionGoogleListener subsListener = new SubscriptionGoogleManager.SubscriptionGoogleListener() {
        @Override
        protected void onBillingConnected(boolean success) {
            if (success){
                if (!TextUtils.isEmpty(keyword)) {
                    subscriptionGoogleManager.loadSKU(keyword);
                }
            }else {
                String failed = TextUtils.isEmpty(model.getLang().string.str_toast_intro_activity2_data_failed)
                        ? "Data on failed..."
                        : model.getLang().string.str_toast_intro_activity2_data_failed;
                Toast.makeText(IntroActivitySecond.this,  failed, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onSubsChecked(boolean exists) {

            ContactItem contactItem = SessionManager.getProfile(getApplicationContext());
            if (!exists) { // If Unsubscribe not exixts....

                if (contactItem != null) {

                    subscriptionGoogleManager.updateSubsStatus(contactItem.msisdn, null, null, true, null, null); // Update status Unsubscribe to server...

                    String strMsisdn = contactItem.msisdn;
                    if (!TextUtils.isEmpty(strMsisdn)) {
                        BaseActivity.getSubscriptionStatus(IntroActivitySecond.this, strMsisdn, new BaseActivity.SubscriptionStatusListener() {
                            @Override
                            public void onComplete(DataSubs subsData) {
                                if (!BaseActivity.isSubscribeActive(subsData)) {
                                    if (!IntroActivitySecond.this.isFinishing()) { // If unsubscribe, call popup subs.....
                                        String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                        if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                                            startActivity(new Intent(IntroActivitySecond.this, PopupVietSubsActivity.class));
                                            return;
                                        }
                                        startActivity(new Intent(IntroActivitySecond.this, PopUpSubsActivity.class));
                                    }
                                }
                            }

                            @Override
                            public void onFail() {
                                getSubsBuy(strMsisdn);
                            }
                        });
                    }

                }


            }else { // If subscribe exists.....
                if (contactItem != null) {

                    try {
                        GpayData gpayData = SessionManager.getGpayData(IntroActivitySecond.this);
                        if (gpayData != null) {

                            String msisdn = gpayData.msisdn;
                            String type = gpayData.type;
                            String json = gpayData.json;
                            boolean cancel = gpayData.cancel;
                            String periode = gpayData.periode;
                            String price = gpayData.price;

                            if (!TextUtils.isEmpty(msisdn) && !TextUtils.isEmpty(type) && !TextUtils.isEmpty(json) && !TextUtils.isEmpty(periode) && !TextUtils.isEmpty(price)) {
                                subscriptionGoogleManager.updateSubsStatus(msisdn, type, json, cancel, periode, price); // Update status Unsubscribe to server...
                            }

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }
        }
    };

// ======================================================================================================================
// ======================================================================================================================



    @Override
    public void onFinish() {
        super.onFinish();
        getParsingConfig();
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(receiver, intentFilter);
        if (!SyncConfig.getInstance(IntroActivitySecond.this).isSynchronizing()) {
            startService(new Intent(IntroActivitySecond.this, SyncConfigService.class));
        }

        if (isDrawOverlay) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!Settings.canDrawOverlays(getApplicationContext())) {
                    isDrawOverlay = false;
                    return;
                }
            }
            isDrawOverlay = true;
            processFinish();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (webView != null) {
            webView.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (webView != null) {
            webView.onResume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);

        if (webView != null) {
            if (!TextUtils.isEmpty(urlCharging)) {
                webView.loadUrl(urlCharging);
                webView.clearHistory();
                webView.clearCache(true);
                webView.removeAllViews();
                webView.stopLoading();
                webView.setWebViewClient(null);
                webView.setWebChromeClient(null);
                webView.destroy();
                webView = null;
            }
        }

//        if (progressDialog != null && progressDialog.isShowing()){
//            progressDialog.dismiss();
//        }
    }

    private void processFinish() {

        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        if (!isDrawOverlay) {

            // Call Protected Overlay...
            SyncUtils.checkProtected(IntroActivitySecond.this);
            if (Build.VERSION.SDK_INT >= 23) {
                if (!Settings.canDrawOverlays(getApplicationContext())) {
                    Intent itn = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(itn, 1234);
                    isDrawOverlay = true;
                    try {
                        GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_OVERLAY_PERMISSION);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                }
            }
        }

        ConfigData cdt = SessionManager.getConfigData(IntroActivitySecond.this);
        if (cdt != null) {
            boolean isScFinish = cdt.showcase_finish;
            if (isScFinish) {
                setDataPreferences();
                startActivity(new Intent(IntroActivitySecond.this, MainActivity.class));
                showCaseToFalse();
                finish();
                return;
            }
        }

        setDataPreferences();
        startActivity(new Intent(IntroActivitySecond.this, SetupProfileActivity.class));
        finish();

    }

    private void setDataPreferences(){
        PreferenceUtil.getEditor(IntroActivitySecond.this).putBoolean(PreferenceUtil.INTRO, true).commit();
        PreferenceUtil.getEditor(IntroActivitySecond.this).putBoolean(PreferenceUtil.INTRO_ACTIVITY_SECOND, true).commit();
    }

    private void showCaseToFalse() {
        PreferenceUtil.getEditor(IntroActivitySecond.this).putBoolean(PreferenceUtil.SHOW_CASE_DEMO_PROFILE, false).commit();
        AppPreference.setPreferenceBoolean(IntroActivitySecond.this, AppPreference.FIRST_SHOW_CASE, false);
        AppPreference.setPreferenceBoolean(IntroActivitySecond.this, AppPreference.SHOW_CASE_TIMELINE, false);
        AppPreference.setPreferenceBoolean(IntroActivitySecond.this, AppPreference.SHOW_CASE_PROFILE, false);
        AppPreference.setPreferenceBoolean(IntroActivitySecond.this, AppPreference.SHOW_CASE_VIDEOS, false);
        AppPreference.setPreferenceBoolean(IntroActivitySecond.this, AppPreference.SHOW_CASE_FRIENDS, false);
        PreferenceUtil.getEditor(this).putBoolean(PreferenceUtil.SETUP_PROFILE, true).commit();
        PreferenceUtil.getEditor(this).putBoolean(PreferenceUtil.SETUP_CONTACT, true).commit();
        PreferenceUtil.getEditor(this).putBoolean(PreferenceUtil.SETUP_SHARE, true).commit();
        PreferenceUtil.getEditor(this).putBoolean(PreferenceUtil.SETUP_VIDEOTONE, true).commit();
    }

    private void getParsingConfig() {
        ContactItem own = SessionManager.getProfile(IntroActivitySecond.this);
        if (own == null) {

            String failure = TextUtils.isEmpty(model.getLang().string.str_toast_intro_activity2_profil_failure)
                    ? "Data profil on failure"
                    : model.getLang().string.str_toast_intro_activity2_profil_failure;
            Toast.makeText(IntroActivitySecond.this, failure, Toast.LENGTH_SHORT).show();
            return;
        }

        String sMsisdn = TextUtils.isEmpty(own.msisdn)?"":own.msisdn;
//        paramsShowcaseFinished(IntroActivitySecond.this)
        Call<APIResponse<ConfigData>> call = ServicesFactory.getService().appsConfig(sMsisdn, paramsShowcaseFinished(IntroActivitySecond.this));
        call.enqueue(new Callback<APIResponse<ConfigData>>() {
            @Override
            public void onResponse(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<ConfigData>> call,
                                   @org.jetbrains.annotations.NotNull @NotNull Response<APIResponse<ConfigData>> response) {

                if (response.isSuccessful() && response.body() != null
                        && response.body().isSuccessful() && response.body().data != null) {

                    try {
                        ConfigData newConfig = response.body().data;
                        SessionManager.saveConfigData(IntroActivitySecond.this, newConfig);//save configData
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

                processFinish();

            }

            @Override
            public void onFailure(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<ConfigData>> call, @org.jetbrains.annotations.NotNull @NotNull Throwable t) {
                processFinish();
                Toast.makeText(IntroActivitySecond.this, "Data config on failure...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean paramsShowcaseFinished (Context context) {

        boolean isScFinish;
        boolean scFinished;
        boolean cdtFinished;

        if (context == null)
            return false;

        scFinished = PreferenceUtil.getPref(context).getBoolean(PreferenceUtil.FINAL_SCASE, false);
        ConfigData cdt = SessionManager.getConfigData(context);
        if (cdt != null)
            cdtFinished = cdt.showcase_finish;
        else
            cdtFinished = false;

        isScFinish = scFinished || cdtFinished;

        return isScFinish;

    }


}