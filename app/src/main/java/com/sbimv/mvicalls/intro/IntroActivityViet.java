package com.sbimv.mvicalls.intro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.sbimv.mvicalls.LocaleHelper;
import com.sbimv.mvicalls.fragment.IntroVietFragment;
import com.sbimv.mvicalls.fragment.IntroVietFragment2;
import com.sbimv.mvicalls.fragment.IntroVietFragment3;
import com.sbimv.mvicalls.register.PhoneRegister;
import com.sbimv.mvicalls.util.PreferenceUtil;

import agency.tango.materialintroscreen.MaterialIntroActivity;

public class IntroActivityViet extends MaterialIntroActivity {

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(LocaleHelper.onAttach(context));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableLastSlideAlphaExitTransition(false);

        addSlide(new IntroVietFragment());
        addSlide(new IntroVietFragment2());
        addSlide(new IntroVietFragment3());
    }

    @Override
    public void onFinish() {
        super.onFinish();

        startActivity(new Intent(IntroActivityViet.this, PhoneRegister.class));

        finish();
    }

}
