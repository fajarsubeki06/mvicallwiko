package com.sbimv.mvicalls.intro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.sbimv.mvicalls.LocaleHelper;
import com.sbimv.mvicalls.activity.IntroVietnamActivity;
import com.sbimv.mvicalls.fragment.Intro8;
import com.sbimv.mvicalls.fragment.Intro9;
import com.sbimv.mvicalls.fragment.IntroVietFragment;
import com.sbimv.mvicalls.register.PhoneRegister;
import com.sbimv.mvicalls.util.PreferenceUtil;

import agency.tango.materialintroscreen.MaterialIntroActivity;


public class IntroActivityFirst extends MaterialIntroActivity {

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(LocaleHelper.onAttach(context));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableLastSlideAlphaExitTransition(false);

        addSlide(new Intro8());
        //addSlide(new IntroVietFragment());

    }

    @Override
    public void onFinish() {
        super.onFinish();
        PreferenceUtil.getEditor(IntroActivityFirst.this).putBoolean(PreferenceUtil.INTRO_ACTIVITY_FIRST, true).commit();

        String getLanguage = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.GET_LANG, "");
        startActivity(new Intent(IntroActivityFirst.this, PhoneRegister.class));


//        if (!TextUtils.isEmpty(getLanguage)){
//            assert getLanguage != null;
//            if (getLanguage.equalsIgnoreCase("vi") || getLanguage.equalsIgnoreCase("vn")){
//                startActivity(new Intent(IntroActivityFirst.this, IntroVietnamActivity.class));
//            }else{
//            }
//        }


        finish();
    }

}
