package com.sbimv.mvicalls.interactor;

import android.text.TextUtils;

import com.sbimv.mvicalls.contract.MainContract;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.DataBestDeal;
import com.sbimv.mvicalls.pojo.DataVideoHowTo;
import com.sbimv.mvicalls.pojo.InboxModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class IntractorImpl implements MainContract.Intractor {

    //*
    // Get Data Inbox
    // *
    @Override
    public void getInbox(String msisdn, String type, String id, OnInboxFinishedListener inboxFinishedListener) {
        inboxFinishedListener.onProgress();
        Call<APIResponse<List<InboxModel>>> call = ServicesFactory.getService().inbox(msisdn, type, id);
        call.enqueue(new Callback<APIResponse<List<InboxModel>>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<List<InboxModel>>> call,
                                   @NotNull Response<APIResponse<List<InboxModel>>> response) {
                if (response.body() != null && response.isSuccessful() && response.body().isSuccessful()) {
                    List<InboxModel> dataArrayList = response.body().data;
                    if (!TextUtils.isEmpty(type)){
                        if (type.equalsIgnoreCase("read")){
                            inboxFinishedListener.onDetailSuccess(dataArrayList);
                        }else {
                            inboxFinishedListener.onSuccess(dataArrayList);
                        }
                    }else {
                        inboxFinishedListener.onSuccess(dataArrayList);
                    }
                }else {
                    inboxFinishedListener.onEmpty();
                }
                inboxFinishedListener.onFinish();
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<List<InboxModel>>> call, @NotNull Throwable t) {
                inboxFinishedListener.onFailure();
                inboxFinishedListener.onFinish();
                inboxFinishedListener.onEmpty();
            }
        });

    }

    //*
    // Get Data How To Use
    // *
    @Override
    public void getHowToUse(String lang, OnHowToUseFinishedListener onHowToUseFinishedListener) {
        onHowToUseFinishedListener.onProgress();
        Call<APIResponse<DataVideoHowTo>> call = ServicesFactory.getService().getVideoID(lang);
        call.enqueue(new Callback<APIResponse<DataVideoHowTo>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<DataVideoHowTo>> call,
                                   @NotNull Response<APIResponse<DataVideoHowTo>> response) {
                if (response.isSuccessful() && response.body() != null) {

                    String videoId = response.body().data.getVideo_code();
                    String urlHowToUse = response.body().data.getHowtouse();
                    String urlFaq = response.body().data.getFaq();

                    onHowToUseFinishedListener.onSuccess(videoId, urlHowToUse, urlFaq);
                }else {
                    onHowToUseFinishedListener.onEmpty();
                }
                onHowToUseFinishedListener.onFinish();
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<DataVideoHowTo>> call, @NotNull Throwable t) {
                onHowToUseFinishedListener.onFailure();
                onHowToUseFinishedListener.onFinish();
            }
        });

    }

    @Override
    public void getBestDeal(String msisdn, String deal_id, OnBestDealFinishedListener onBestDealFinishedListener) {
        Call<APIResponse<List<DataBestDeal>>> call = ServicesFactory.getService().getBestDeal(msisdn,deal_id);
        call.enqueue(new Callback<APIResponse<List<DataBestDeal>>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<List<DataBestDeal>>> call,
                                   @NotNull Response<APIResponse<List<DataBestDeal>>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    List<DataBestDeal> data = response.body().data;
                    onBestDealFinishedListener.onSuccess(data);
                }else {
                    onBestDealFinishedListener.onEmpty();
                }
                onBestDealFinishedListener.onFinish();
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<List<DataBestDeal>>> call, @NotNull Throwable t) {
                onBestDealFinishedListener.onFailure();
                onBestDealFinishedListener.onFinish();
            }
        });
    }


}
