package com.sbimv.mvicalls.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Handler;
import android.telecom.TelecomManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.db.DBHelper;
import com.sbimv.mvicalls.db.LikeTable;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.services.SyncCommentService;
import com.sbimv.mvicalls.util.PermissionUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.view.CallerViewUtil;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallReceiver extends BroadcastReceiver {
    final String TAG = CallReceiver.class.getSimpleName();
    private static MyPhoneStateListener phoneListener;
    private SQLiteDatabase database;
    private ContactItem contactItem;

    public void onReceive(Context context, Intent intent) {
        String MODEL = android.os.Build.MODEL;
        String device_type = TextUtils.isEmpty(MODEL)?"":MODEL;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||Build.VERSION.SDK_INT <= Build.VERSION_CODES.O_MR1 || Build.MODEL.equalsIgnoreCase("Oppo A3s")||Build.DEVICE.equalsIgnoreCase("Oppo A3s")|| !isDefaultCall(context)) {
            try {
                if (phoneListener == null) {
                    TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                    phoneListener = new MyPhoneStateListener(context);
                    telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
                }

            } catch (Exception e) {
                Log.e(TAG, " " + e);
            }

        }

    }

    private boolean isDefaultCall(Context context){
        boolean isActive = false;
//        String MODEL = android.os.Build.MODEL;
//        String device_type = TextUtils.isEmpty(MODEL)?"":MODEL;
        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                TelecomManager telecomManager = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
                if (telecomManager != null) {
                    String dialerPackage = "";
                    dialerPackage = TextUtils.isEmpty(telecomManager.getDefaultDialerPackage()) ? "" : telecomManager.getDefaultDialerPackage();
                    String strPackageName = TextUtils.isEmpty(context.getPackageName()) ? "" : context.getPackageName();
                    isActive = TextUtils.isEmpty(dialerPackage) || TextUtils.isEmpty(strPackageName) || dialerPackage.equalsIgnoreCase(strPackageName);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return isActive;
    }

    private class MyPhoneStateListener extends PhoneStateListener {
        final String TAG = MyPhoneStateListener.class.getSimpleName();
        Context ctx;
        boolean wasRinging;

        public MyPhoneStateListener(Context ctx) {
            this.ctx = ctx;
        }

        public void onCallStateChanged(int state, final String incomingNumber) {
            Log.d(TAG, "STATE : " + state + ", INCOMING NUMBER : " + incomingNumber);

            if (state == TelephonyManager.CALL_STATE_RINGING) {
                wasRinging = true;
                showVideoCaller(incomingNumber);
//                sendNotificationTriggered();
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                if(wasRinging){
                    CallerViewUtil.getInstance(ctx.getApplicationContext()).dismissView();
                }
                wasRinging = false;
            }else if (state == TelephonyManager.CALL_STATE_IDLE){
                wasRinging = false;
                CallerViewUtil.getInstance(ctx.getApplicationContext()).dismissView();

                // Sending data like video caller.............................
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            contactItem = ContactDBManager.getInstance(ctx).getCallerByMSISDN(incomingNumber);
                        }catch (Exception e){
                            e.printStackTrace();
                            return;
                        }
                        if (contactItem != null) {
                            String id = contactItem.caller_id;
                            getDatabase(id);
                        }
                    }
                }, 3000);

            }

        }

        private void showVideoCaller(final String... arg) {
            if (PermissionUtil.hashPermission(ctx, BaseActivity.permissions)) {
                ContactItem item = ContactDBManager.getInstance(ctx).getCallerByMSISDN(arg[0]);
                if (item == null) {
                    CallerViewUtil.getInstance(ctx.getApplicationContext()).dismissView();
                } else {

                    CallerViewUtil.getInstance(ctx.getApplicationContext()).showView(arg[0], true,true);

                    // send push call in
                    ContactItem me = SessionManager.getProfile(ctx);
                    if(me != null && !TextUtils.isEmpty(me.caller_id)) {
                        sendCallPushIn(me.caller_id, item.caller_id);
                    }
                }
            }
        }

        // Find data like in database.....
        public void getDatabase(String... arg) {
            if (!TextUtils.isEmpty(arg[0])) {
                String selectQuery = "SELECT * FROM " + LikeTable.Entry.TABLE_NAME + " WHERE caller_id_uploader = '" + arg[0] + "'";
                try{
                    database = DBHelper.getInstance(ctx).getReadableDatabase();
                }catch (Exception e){
                    e.printStackTrace();
                    return;
                }
                if (database != null) {
                    Cursor cursor = database.rawQuery(selectQuery, null);
                    if (cursor != null && cursor.getCount() > 0) {
                        if (cursor.moveToFirst()) {
                            do {
                                String send = cursor.getString(cursor.getColumnIndex(LikeTable.Entry.COLUMN_NAME_SEND_COMMENT));
                                String caller_id_uploader = cursor.getString(cursor.getColumnIndex(LikeTable.Entry.COLUMN_NAME_CALLER_ID_UPLOADER));
                                if (send.equals("0")) {
                                    if (!TextUtils.isEmpty(caller_id_uploader)) {
                                        syncComment(caller_id_uploader);
                                    }
                                }
                            } while (cursor.moveToNext());
                            cursor.close();
                        }
                    }
                }

            }

        }

        // Call service backround....
        private void syncComment(String id_uploader){
            Intent i = new Intent(ctx.getApplicationContext(), SyncCommentService.class);
            i.putExtra("id_uploader",id_uploader);
            ctx.getApplicationContext().startService(i);
        }
    }

    void sendCallPushIn(String caller_id, String caller_id_target){
        Call<APIResponse> call = ServicesFactory.getService().sendPushCall(caller_id, caller_id_target, "ID");
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) { }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) { }
        });
    }

}