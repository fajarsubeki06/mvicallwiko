package com.sbimv.mvicalls.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.PermissionUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.view.CallerViewUtil;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OutGoingCallReceiver extends BroadcastReceiver {

    public static final String ACTION = "com.sbimv.mvicalls.services";

    @Override
    public void onReceive(final Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

                final String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
                if (PermissionUtil.hashPermission(context, BaseActivity.permissions)) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ContactItem item = ContactDBManager.getInstance(context).getCallerByMSISDN(number);
                            if (item == null) return;
                            // show caller window (status)
                            CallerViewUtil.getInstance(context.getApplicationContext()).showView(number, true, false);

                            // send push call out
                            ContactItem me = SessionManager.getProfile(context);
                            if (me != null && !TextUtils.isEmpty(me.caller_id)) {
                                sendCallPushOut(me.caller_id, item.caller_id);
                            }
                        }
                    }, 300);
                }

            }
        }
    }

    void sendCallPushOut(String caller_id, String caller_id_target){
        Call<APIResponse> call = ServicesFactory.getService().sendPushCall(caller_id, caller_id_target, "out");
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse> call, @NotNull Response<APIResponse> response) { }

            @Override
            public void onFailure(@NotNull Call<APIResponse> call, @NotNull Throwable t) { }
        });
    }
}