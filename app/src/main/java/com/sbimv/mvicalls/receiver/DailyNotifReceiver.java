package com.sbimv.mvicalls.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.sbimv.mvicalls.services.DailyReminderPushNotifService;

public class DailyNotifReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        // start service only below oreo
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            Intent i = new Intent(context, DailyReminderPushNotifService.class);
            context.startService(i);
        }
    }
}
