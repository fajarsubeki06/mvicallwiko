package com.sbimv.mvicalls.receiver;


import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;

import com.sbimv.mvicalls.services.AutostartService;
import com.sbimv.mvicalls.util.PermissionUtil;

public class SensorRestarterBroadcastReceiver extends BroadcastReceiver {

    AlarmManager alarmManager;
    PendingIntent alarmIntent;
    static final long SYNC_TRIGGERED_AT =  1 * 60 * 1000; // min * sec * millis
    static final long SYNC_INTERVAL = 1 * 60 * 1000; // min * sec * millis

    @Override
    public void onReceive(Context context, Intent intent) {
        intent = new Intent(context, AutostartService.class);
        alarmIntent = PendingIntent.getService(context, 777, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + SYNC_INTERVAL, alarmIntent);
            Log.i("callService","Alarm Manager");
        } else {
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + SYNC_TRIGGERED_AT, SYNC_INTERVAL, alarmIntent);
            Log.i("callService","Alarm Manager");
        }
    }
}
