package com.sbimv.mvicalls.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.sbimv.mvicalls.services.SyncContactServices;


public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, SyncContactServices.class);
        context.startService(i);
    }
}
