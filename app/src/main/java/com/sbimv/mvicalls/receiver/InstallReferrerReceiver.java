package com.sbimv.mvicalls.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.annotation.RequiresPermission;

import com.google.android.gms.analytics.CampaignTrackingReceiver;

public class InstallReferrerReceiver extends BroadcastReceiver {

    public static final String REFERRER = "MV_INSTALL_REFERRER";

    @RequiresPermission(
            allOf = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE"}
    )
    @Override
    public void onReceive(Context context, Intent intent) {
        String referrer = intent.getStringExtra("referrer");
        setReferrer(context, referrer);

        new CampaignTrackingReceiver().onReceive(context, intent);
    }

    public static void setReferrer(Context context, String referrer){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(REFERRER, referrer);
        editor.apply();
    }

    public static String getReferrer(Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(REFERRER, "");
    }
}
