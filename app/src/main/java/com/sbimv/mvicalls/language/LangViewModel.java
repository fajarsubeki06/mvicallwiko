package com.sbimv.mvicalls.language;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.firebase.database.annotations.NotNull;
import com.google.gson.Gson;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.util.PreferenceUtil;



import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LangViewModel {

    private static LangViewModel instance;
    private final String defLangKey = "EN";
    private final String defLangValue = "{\"db_name\":\"db_mvicall_en\",\"ccode\":null,\"string\":{\"app_name\":\"MViCall\",\"title_dialog_usage_connection\":\"Information\",\"body_dialog_usage_connection\":\"You are using a cellular data connection or can switch to WiFi\",\"title_dialog_cek_simcard\":\"Detected Dual SIM\",\"body_dialog_cek_simcard\":\"Devices detected with dual sim cards, to buy this content via SMS with the sim card options listed on MViCall\",\"wording_all_approve\":\"Oke\",\"wording_all_cancel\":\"Cancel\",\"title_delay_proses\":\"Processing\",\"body_delay_proses\":\"Please be patient until payment process done\",\"under_construction\":\"Under construction\",\"user_name\":\"User Name\",\"more_videos\":\"View All\",\"edit_profile\":\"Edit Profile\",\"video_tone\":\"Video Ringtone\",\"preview_video\":\"Preview Video\",\"title_permission_alert\":\"Need Permissions\",\"body_permission_alert\":\"This app needs permission to use this feature. You can grant them in app settings > permission.\",\"button_permission_alert\":\"GOTO SETTINGS\",\"friend\":\"Friends\",\"btnclose\":\"Close\",\"term_of_service\":\"Terms and Conditions\",\"settings\":\"Settings\",\"enable_status\":\"Enable status\",\"change_video_tone\":\"Change Video Ringtone\",\"more_categories\":\"More Categories\",\"gallery\":\"Gallery\",\"upload\":\"Upload\",\"my_collection\":\"My Collection\",\"funny_video\":\"Funny Videos\",\"for_all_contacts\":\"User (msisdn)\",\"active_to\":\"Active to :\",\"search\":\"Search\",\"preparing\":\"Preparing...\",\"login_register\":\"Continue\",\"profile_info\":\"Make sure the number you want to add the status is a number already using MViCall application\",\"set_status\":\"Set Status\",\"confirm\":\"Confirm\",\"write_status\":\"Write status\",\"phone_number\":\"Phone number\",\"about\":\"About MViCall\",\"inbox\":\"Inbox\",\"detail_inbox\":\"Message details\",\"contact_us\":\"Contact Us\",\"item_phone\":\"Phone\",\"cs_support\":\"Customer Support\",\"cb_agree\":\"I agree to the applicable Mvicall terms and conditions\",\" Mvicall yang berlaku\\\"\":\"Choose Contact\",\"choose_contact\":\"Related Video\",\"related_video\":\"Buy\",\"subscribe\":\"Buy\",\"camera\":\"Rec\",\"current_video_tone\":\"Current Video Ringtone\",\"choose_language\":\"Choose Language\",\"subscription_package\":\"Subscribe\",\"price_above_excludes_ppn_10\":\"Price above excludes PPN 10%\",\"choose_subscription\":\"Choose your buy package and get premium feature\",\"check_network\":\"Please make sure your network connection is available\",\"check_sync\":\"Your friend list has not been syncronized !!\",\"change_language\":\"Change Language\",\"set_contact_status\":\"Set Contact Status\",\"my_status\":\"My Status\",\"lets_install_mvicall\":\"I can DIRECT YOUR RINGTONE DIRECTLY with MY PERSONAL VIDEO, when I call you! Want too?\\n\",\"select_image_from\":\"Select image from\",\"select_image_from_camera\":\"Camera\",\"version\":\"Version\",\"privacy_policy\":\"Privacy Policy\",\"invite_friends\":\"Invite Friends\",\"intro1_wording\":\"Thank you for install MVICALL, Up-To-Date Application that can change your friends  Ringtone to cool video !\",\"rekam_video_personalmu\":\"Record Your Video\",\"intro2_wording\":\"You can record your really cool video in your phone with MVICALL.\",\"unggah_video_npersonalmu_di_mvicall\":\"Upload Your Personal\\nVideo in MViCall\",\"intro3_wording\":\"Upload your video NOW ! So, When you call your friends, your video will pop up on the phone screen.\",\"ajak_temanmu_untuk_npakai_mvicall\":\"Invite Your Friends \\nto Use MViCall\",\"intro4_wording\":\"Invite all your friends to install and use MVicall to make it COOLER!!\",\"telpon_temanmu_nseperti_biasa\":\"When You Call Your Friends, Your Video Will Pop Up On The Phone Screen.\",\"intro5_wording\":\"Your video will pop up on the phone screen to replace your friends Ringtone.\",\"banyak_koleksi\":\"You Can Draw From And Make Use of a Large Collections of Videos From Your Favorite Artists.\",\"jangan_lupa_pasang_status\":\"Don't Forget Install Your \\nMViCall Status\",\"status_kamu_akan\":\"Your status will appear on the phone screen of your friends that are calling you and you can also set different statuses for selected friends.\",\"invite\":\"Invite\",\"active_until\":\"Active until\",\"write_profile\":\"Write your profile here ...\",\"no_connection\":\"No internet connections.\",\"close\":\"Close\",\"message\":\"Message\",\"empty_inbox\":\"You don't have inbox message\",\"are_you_sure_delete_status\":\"Are you sure to delete status for\",\"delete_status\":\"Delete Status\",\"whatsapp\":\"WhatsApp\",\"telegram\":\"Telegram\",\"facebook\":\"Facebook\",\"instagram\":\"Instagram\",\"line\":\"LINE\",\"email\":\"Email\",\"telp_cs\":\"(021) 22892355\",\"checking_status\":\"Checking status...\",\"default_status\":\"Hi..Iam using stunning app MViCall\",\"wellcome_to_mvicall\":\"Welcome to MViCall\",\"please_sign_up_to_continue\":\"To start using MViCall \\n please input your MAIN NUMBER on your phone.\",\"what_do_you_think\":\"What do you think ?\",\"status_for_all_contact\":\"Status for all contact\",\"error_fetching_data\":\"Error while fetching data, Check your connection\",\"how_to\":\"How to use\",\"best_deals\":\"Best Deals\",\"reward\":\"Reward\",\"sign_out\":\"Sign out\",\"signing_out_wording\":\"MViCall signing out because detecting different simcard\",\"title_walkthrough8\":\"Complete Your Profile\\nas Cool as Possible\",\"wording_intro8\":\"Upload your profile photo, use your coolest name on profile and update your status as creative as you can. And now you are Cool!\",\"best_deal_empty_wording\":\"The best deal will come soon for you here\",\"image_intro\":\"walkthrough8en\",\"wording_foreground\":\"Change your friends ringtones, use your choosen video!\",\"delete_confirm\":\"Are you sure want to delete this message?\",\"approve_cofirm\":\"Yes\",\"cancel_confirm\":\"No\",\"wording_showcase_friends\":\"Invite your family and friends to install MViCall, and your cool videos can appear on their screen when you call them.\",\"wording_showcase_profile\":\"Update your coolest profile photo and your newest status!\",\"wording_showcase_editvideotone\":\"Record your video, upload it, and add the collection with cool videos available here\",\"change_personal_video\":\"Change Video Ringtone\",\"more_personal_video\":\"More Videos\",\"update_name\":\"Update Name\",\"wording_update_name\":\"You can update your profile name here, make sure the name you entered is the real name. Want to update now?\",\"update_status\":\"Update Status\",\"wording_update_status\":\"You can update your status here. Want to update now?\",\"set_status_contact\":\"Set Status Contact\",\"wording_set_status_contact\":\"You can give special status to each of your friends in MViCall.\",\"synchronize_contact\":\"Synchronize Contact\",\"wording_sync_contact\":\"To find out what your friends are using MViCall, tap here !\",\"wording_change_video\":\"You can update your video ringtone here. Update your video?\",\"wording_more_videos\":\"Available more personal video options here.\",\"disslike\":\"Dislike\",\"like\":\"Like\",\"title_update_app\":\"New Version Available\",\"msg_update_app\":\"Please update to new version to continue use\",\"wording_report\":\"Report\",\"wording_showcase_rec\":\"You can directly record videos by tapping this button\",\"record_video\":\"Record Video\",\"wording_show_case_gallery\":\"You can also upload your videos personally from the gallery by tapping this button\",\"record\":\"Record\",\"choose_video\":\"Choose Video\",\"real_name\":\"Real Name\",\"delete_message\":\"Delete Message\",\"wording_sc_confirmation\":\"Let you know about the features of this application\",\"no\":\"No\",\"You can invite more friends to install MViCall here and get more REWARDS. Invite friends now?\":\"You can invite more friends to install MViCall here, Invite friends now?\",\"title_dialog_cek_simcard_umb\":\"Two sim cards have been detected, to buy this content, please select the sim card registered with MViCall\",\"wording_next_showcase\":\"Very good. Please go to next step.\",\"button_sc_next\":\"Next\",\"finish\":\"Finish\",\"wording_finish_sc\":\"Thank you for using MViCall. Share it to your friends as much as possible and look forward to the surprises and prizes you can get through this application.\",\"start_now\":\"Start Now\",\"wording_upload_personal_video\":\"Now upload your personal video by tap this button!\",\"upload_personal_video\":\"Upload Video Ringtone\",\"invite_more_friends\":\"Invite More Friends\",\"pref_locale\":\"in\",\"syncronizing\":\"Synchronizing\",\"authenticating\":\"Authenticating\",\"synchronization_process\":\"Synchronization process\",\"wording_failure_response\":\"Failed fetching data, check your connection.\",\"removing_profile\":\"Removing profile\",\"profile_removed\":\"Profile removed\",\"updating_profile\":\"Updating profile\",\"updating_status\":\"Updating status\",\"update_status_success\":\"Update status success\",\"update_status_failed\":\"Update status failed\",\"update_profile_success\":\"Update profile success\",\"update_profile_failed\":\"Update profile failed\",\"uploading\":\"Uploading\",\"upload_video_failed\":\"Upload video failed\",\"authentication_failed\":\"Authentication failed\",\"free\":\"Free\",\"grab_now\":\"Grab Now\",\"wording_enter_code\":\"You already used this promo\",\"wording_wrong_number\":\"Enter the code that was send to\",\"wording_button_continue\":\"continue\",\"wording_digit\":\"continue\",\"wording_resend_code\":\"Enter 6-digit code\",\"wording_redeem_promo\":\"Resend code\",\"wechat\":\"Wechat\",\"viber\":\"Viber\",\"zalo\":\"Zalo\",\"wording_enable_back\":\"Complete the instructions first.\",\"wording_video_too_big\":\"Video size too big, please choose other smaller video\",\"skip\":\"Skip\",\"wl_intro\":\"wl_new_design_en\",\"wl_intro_autostart\":\"new_improve_wl_auto_en\",\"notif_add_user\":\"is now on MViCall ! Call him\\/her Now so he\\/she sees your Videotone!\",\"word_add_new_user\":\"of your Friend are now on MViCall ! Call them Now so their see your Videotone!\",\"wording_title\":\"New Friend!\",\"progress_wording\":\"Loading\",\"redeem_poin\":\"Redeem\",\"your_point\":\"Your Point : [POINT] Point\",\"point\":\"Point\",\"point_reward\":\"Point & Reward\",\"choose\":\"Choose\",\"choose_5_video\":\"Choose [ITEM] Video\",\"choose_5_more_video\":\"Choose [ITEM] More Video\",\"wording_over_choose_video\":\"Exceeds the amount allowed\",\"wording_success_redeem_video\":\"Congratulations, your points have been successfully redeemed for [ITEM]! Press NEXT to see your video collection now.\",\"success\":\"Success\",\"not_enough_point\":\"Your points are not enough\",\"next\":\"Next\",\"back\":\"Back\",\"wording_success_redeem\":\"Congratulation, you success redeem your points for [ITEM]\",\"redeem_now\":\"Redeem Now\",\"wording_empty_reward\":\"Exciting rewards will be here for you\",\"wording_choose_free_content\":\"You can also choose Premium Video on our marketplace. There's FREE Premium Video you can directly choose for you too!\",\"updating_video_tone\":\"Updating video tone\",\"wording_succes_update_videotone\":\"Update videotone successfully\",\"how_to_get_points\":\"How to get points\",\"title_add_profile\":\"User Name\",\"free_content\":\"Free Content\",\"wording_got_free_content\":\"Congratulation, you get free content, check in your video collection\",\"go_to_my_collection\":\"Go to my collection >\",\"word_button_telco_dialog_payment\":\"Pay Using Telco Balance\",\"word_dialog_telco_pay\":\"You will buy premium content using telco balance via sms. Make sure to send the sms from registered number on MViCall.\",\"word_telco_pos_button\":\"Next\",\"gunakan_g_pay\":\"Use G-Pay\",\"pilih_cara_pembayaran\":\"Choose Payment Methode\",\"word_body_pay\":\"Please choose payment methode to buy this premium content\",\"word_popup_register\":\"Make sure you are using your MAIN NUMBER on your phone.\",\"word_change_in_dialog\":\"Change\",\"wording_change_wifi_state\":\"Turn off wifi connection, Use the provider network to continue the purchase\",\"turn_off_wifi\":\"Turn Off Wifi & Buy\",\"turn_off_wifi_next\":\"Turn Off Wifi & Next\",\"wording_turn_off_wifi_subs\":\"Turn off wifi connection and use provider network to continue\",\"wording_wellcome_push\":\"Free Premium Gadgets & Vehicles. Register your Main Number NOW on MViCall!\",\"thanks\":\"Thank you\",\"one_step_more\":\"a little bit longer\",\"content_wording_scase\":\"You only need a few steps to get started using cool features at MViCall. Continue the process and earn points MViCall\",\"empty_screen\":\"Oops!! An unexpected error occurred\",\"please_retry\":\"Please retry after sometime\",\"retry\":\"Retry\",\"description_dialpad_back\":\"Navigate back\",\"description_dialpad_overflow\":\"More options\",\"description_delete_button\":\"backspace\",\"description_image_button_plus\":\"plus\",\"description_voicemail_button\":\"voicemail\",\"default_notification_description\":\"Default sound \",\"callFailed_userBusy\":\"Line busy\",\"callFailed_congestion\":\"Network busy\",\"callFailed_timedOut\":\"No response, timed out\",\"callFailed_server_unreachable\":\"Server unreachable\",\"callFailed_number_unreachable\":\"Number unreachable\",\"callFailed_invalid_credentials\":\"Incorrect username or password\",\"callFailed_out_of_network\":\"Out of network call\",\"callFailed_server_error\":\"Server error. Try again later.\",\"callFailed_noSignal\":\"No signal\",\"callFailed_limitExceeded\":\"ACM limit exceeded\",\"callFailed_powerOff\":\"Radio off\",\"callFailed_simError\":\"No SIM or SIM error\",\"callFailed_outOfService\":\"Cellular network not available\",\"callFailed_fdn_only\":\"Outgoing calls are restricted by FDN.\",\"callFailed_dialToUssd\":\"DIAL request modified to USSD request.\",\"callFailed_dialToSs\":\"DIAL request modified to SS request.\",\"callFailed_dialToDial\":\"DIAL request modified to DIAL with different number.\",\"callFailed_dsac_restricted\":\"Calls restricted by access control.\",\"callFailed_dsac_restricted_emergency\":\"Emergency calls restricted by access control.\",\"callFailed_dsac_restricted_normal\":\"Normal calls restricted by access control.\",\"callFailed_unobtainable_number\":\"Invalid number\",\"incall_error_missing_voicemail_number\":\"Voicemail number unknown.\",\"callFailed_video_call_tty_enabled\":\"Cannot make video calls when TTY is enabled.\",\"ringtone_silent\":\"None\",\"ringtone_unknown\":\"Unknown ringtone\",\"wording_timeline\":\"NEW FEATURES! See the latest UNIQUE Ringtone video from MViCall users around the world here.\",\"db_name\":\"db_lang_en\",\"tap_to_exit\":\"Tab again to end this process ...\",\"wording_empty_verification_code\":\"Verification code cannot be empty\",\"link_share_app\":\"http:\\/\\/bit.ly\\/pakaimvicall\",\"save\":\"Save\",\"live_explore\":\"LIVE EXPLORE\",\"holla\":\"Holla!!\",\"explore\":\"Explore\",\"press_to_exit\":\"Press back button again to exit\",\"wording_sc_invite_friends\":\"You can invite more friends to install MViCall here and get more REWARDS. Invite friends now?\",\"set_video_tone\":\"Set As Video Tone\"}}";
    private LanguageModel lang;
    private Context context;

    private LangViewModel(Context context) {
        this.context = context;
        String langKEY = PreferenceUtil.getPref(context).getString(PreferenceUtil.SAVED_LANG_KEY, null);
        Gson gson = new Gson();
        if (TextUtils.isEmpty(langKEY)) {
            PreferenceUtil.getEditor(context)
                    .putString(PreferenceUtil.SAVED_LANG_KEY, defLangKey)
                    .putString(PreferenceUtil.SAVED_LANG_VALUE, defLangValue)
                    .commit();
            lang = gson.fromJson(defLangValue, LanguageModel.class);
        } else {
            String savedLangValue = PreferenceUtil.getPref(context).getString(PreferenceUtil.SAVED_LANG_VALUE, null);
            lang = gson.fromJson(savedLangValue, LanguageModel.class);
        }
    }

    public static LangViewModel init(Context context) {
        synchronized (LangViewModel.class) {
            if (instance == null) {
                instance = new LangViewModel(context);
            }
        }
        return instance;
    }

    public static LangViewModel getInstance() {
        return instance;
    }

    public LanguageModel getLang() {
        return lang;
    }

    private boolean isCurrentLang(String countryNameCode) {
        String curLang = PreferenceUtil.getPref(context).getString(PreferenceUtil.SAVED_LANG_KEY, defLangKey);
        return countryNameCode.equalsIgnoreCase(curLang);
    }

//    public boolean isVersionUpdated(String newVersion) {
////        String curVersion = SessionManager.getConfigData(context).version_lang;
////        String curVersion = PreferenceUtil.getPref(context).getString(PreferenceUtil.VERSION_LANG, "101");
//        if (newVersion.equalsIgnoreCase(curVersion)) {
//            return true;
//        } else {
//            return false;
//        }
//    }

//    public void setUpdateVersionLang(final String countryNameCode, String countryCode, LangRemoteListener listener) {
//        if (isVersionUpdated(newVersion)) {
//            return;
//        }
//        getLangRemote(countryNameCode, countryCode, listener);
//    }

    public void getLangRemote(final String countryNameCode, String countryCode, final LangRemoteListener listener) {
        Call<APIResponse<LanguageModel>> call = ServicesFactory.getService().getLang(countryCode);
        call.enqueue(new Callback<APIResponse<LanguageModel>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<LanguageModel>> call, @NotNull Response<APIResponse<LanguageModel>> response) {
                if (response.body() != null && response.body().isSuccessful() && response.isSuccessful()) {
                    final LanguageModel data = response.body().data;

                    if (data != null) {
                        Gson gson = new Gson();
                        String json = gson.toJson(data);

                        PreferenceUtil.getEditor(context)
                                .putString(PreferenceUtil.SAVED_LANG_KEY, countryNameCode)
                                .putString(PreferenceUtil.SAVED_LANG_VALUE, json)
                                .commit();

                        lang = data;
                        listener.onUpdate();
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResponse<LanguageModel>> call, Throwable t) {
                Toast.makeText(context, "Failure when fetching data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setLangFromRemote(final String countryNameCode, String countryCode, LangRemoteListener listener) {
        if (isCurrentLang(countryNameCode)) {
            return;
        }
        getLangRemote(countryNameCode, countryCode, listener);
    }

    public interface LangRemoteListener {
        void onUpdate();
    }
}