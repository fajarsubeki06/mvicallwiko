package com.sbimv.mvicalls.language;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LanguageModel {
    @SerializedName("db_name")
    @Expose
    public String dbName;
    @SerializedName("ccode")
    @Expose
    public String ccode;
    @SerializedName("string")
    @Expose
    public StringLanguage string;

    public LanguageModel() {
    }

}
