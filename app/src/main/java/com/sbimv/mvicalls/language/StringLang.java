package com.sbimv.mvicalls.language;

import android.os.Parcel;
import android.os.Parcelable;

public class StringLang implements Parcelable {
    public static final Creator<StringLang> CREATOR = new Creator<StringLang>() {
        @Override
        public StringLang createFromParcel(Parcel in) {
            return new StringLang(in);
        }

        @Override
        public StringLang[] newArray(int size) {
            return new StringLang[size];
        }
    };
    private int id;
    private String cname;
    private String lang_item;
    private String lang_text;

    public StringLang() {
    }

    protected StringLang(Parcel in) {
        id = in.readInt();
        cname = in.readString();
        lang_item = in.readString();
        lang_text = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getLang_item() {
        return lang_item;
    }

    public void setLang_item(String lang_item) {
        this.lang_item = lang_item;
    }

    public String getLang_text() {
        return lang_text;
    }

    public void setLang_text(String lang_text) {
        this.lang_text = lang_text;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(cname);
        dest.writeString(lang_item);
        dest.writeString(lang_text);
    }
}
