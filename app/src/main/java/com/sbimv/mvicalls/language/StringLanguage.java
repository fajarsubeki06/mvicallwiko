package com.sbimv.mvicalls.language;

import com.google.gson.annotations.SerializedName;

public class StringLanguage {

    @SerializedName("app_name")

    public String appName;
    @SerializedName("title_dialog_usage_connection")

    public String titleDialogUsageConnection;
    @SerializedName("body_dialog_usage_connection")

    public String bodyDialogUsageConnection;
    @SerializedName("title_dialog_cek_simcard")

    public String titleDialogCekSimcard;
    @SerializedName("body_dialog_cek_simcard")

    public String bodyDialogCekSimcard;
    @SerializedName("wording_all_approve")

    public String wordingAllApprove;
    @SerializedName("wording_all_cancel")

    public String wordingAllCancel;
    @SerializedName("title_delay_proses")

    public String titleDelayProses;
    @SerializedName("body_delay_proses")

    public String bodyDelayProses;
    @SerializedName("under_construction")

    public String underConstruction;
    @SerializedName("user_name")

    public String userName;
    @SerializedName("more_videos")

    public String moreVideos;
    @SerializedName("edit_profile")

    public String editProfile;
    @SerializedName("video_tone")

    public String videoTone;
    @SerializedName("preview_video")

    public String previewVideo;
    @SerializedName("title_permission_alert")

    public String titlePermissionAlert;
    @SerializedName("body_permission_alert")

    public String bodyPermissionAlert;
    @SerializedName("button_permission_alert")

    public String buttonPermissionAlert;
    @SerializedName("friend")

    public String friend;
    @SerializedName("btnclose")

    public String btnclose;
    @SerializedName("term_of_service")

    public String termOfService;
    @SerializedName("settings")

    public String settings;
    @SerializedName("enable_status")

    public String enableStatus;
    @SerializedName("change_video_tone")

    public String changeVideoTone;
    @SerializedName("more_categories")

    public String moreCategories;
    @SerializedName("gallery")

    public String gallery;
    @SerializedName("upload")

    public String upload;
    @SerializedName("my_collection")

    public String myCollection;
    @SerializedName("funny_video")

    public String funnyVideo;
    @SerializedName("for_all_contacts")

    public String forAllContacts;
    @SerializedName("active_to")

    public String activeTo;
    @SerializedName("search")

    public String search;
    @SerializedName("preparing")

    public String preparing;
    @SerializedName("login_register")

    public String loginRegister;
    @SerializedName("profile_info")

    public String profileInfo;
    @SerializedName("set_status")

    public String setStatus;
    @SerializedName("confirm")

    public String setName;
    @SerializedName("name")

    public String confirm;
    @SerializedName("write_status")

    public String writeStatus;
    @SerializedName("phone_number")

    public String phoneNumber;
    @SerializedName("about")

    public String about;
    @SerializedName("inbox")

    public String inbox;
    @SerializedName("detail_inbox")

    public String detailInbox;
    @SerializedName("contact_us")

    public String contactUs;
    @SerializedName("item_phone")

    public String itemPhone;
    @SerializedName("cs_support")

    public String csSupport;
    @SerializedName("cb_agree")

    public String cbAgree;
    @SerializedName(" Mvicall yang berlaku\"")

    public String mvicallYangBerlaku;
    @SerializedName("choose_contact")

    public String chooseContact;
    @SerializedName("related_video")

    public String relatedVideo;
    @SerializedName("subscribe")

    public String subscribe;
    @SerializedName("camera")

    public String camera;
    @SerializedName("current_video_tone")

    public String currentVideoTone;
    @SerializedName("choose_language")

    public String chooseLanguage;
    @SerializedName("subscription_package")

    public String subscriptionPackage;
    @SerializedName("price_above_excludes_ppn_10")

    public String priceAboveExcludesPpn10;
    @SerializedName("choose_subscription")

    public String chooseSubscription;
    @SerializedName("check_network")

    public String checkNetwork;
    @SerializedName("check_sync")

    public String checkSync;
    @SerializedName("change_language")

    public String changeLanguage;
    @SerializedName("set_contact_status")

    public String setContactStatus;
    @SerializedName("my_status")

    public String myStatus;
    @SerializedName("lets_install_mvicall")

    public String letsInstallMvicall;
    @SerializedName("select_image_from")

    public String selectImageFrom;
    @SerializedName("select_image_from_camera")

    public String selectImageFromCamera;
    @SerializedName("version")

    public String version;
    @SerializedName("privacy_policy")

    public String privacyPolicy;
    @SerializedName("invite_friends")

    public String inviteFriends;
    @SerializedName("intro1_wording")

    public String intro1Wording;
    @SerializedName("rekam_video_personalmu")

    public String rekamVideoPersonalmu;
    @SerializedName("intro2_wording")

    public String intro2Wording;
    @SerializedName("unggah_video_npersonalmu_di_mvicall")

    public String unggahVideoNpersonalmuDiMvicall;
    @SerializedName("intro3_wording")

    public String intro3Wording;
    @SerializedName("ajak_temanmu_untuk_npakai_mvicall")

    public String ajakTemanmuUntukNpakaiMvicall;
    @SerializedName("intro4_wording")

    public String intro4Wording;
    @SerializedName("telpon_temanmu_nseperti_biasa")

    public String telponTemanmuNsepertiBiasa;
    @SerializedName("intro5_wording")

    public String intro5Wording;
    @SerializedName("banyak_koleksi")

    public String banyakKoleksi;
    @SerializedName("jangan_lupa_pasang_status")

    public String janganLupaPasangStatus;
    @SerializedName("status_kamu_akan")

    public String statusKamuAkan;
    @SerializedName("invite")

    public String invite;
    @SerializedName("active_until")

    public String activeUntil;
    @SerializedName("write_profile")

    public String writeProfile;
    @SerializedName("no_connection")

    public String noConnection;
    @SerializedName("close")

    public String close;
    @SerializedName("message")

    public String message;
    @SerializedName("empty_inbox")

    public String emptyInbox;
    @SerializedName("are_you_sure_delete_status")

    public String areYouSureDeleteStatus;
    @SerializedName("delete_status")

    public String deleteStatus;
    @SerializedName("whatsapp")

    public String whatsapp;
    @SerializedName("telegram")

    public String telegram;
    @SerializedName("facebook")

    public String facebook;
    @SerializedName("instagram")

    public String instagram;
    @SerializedName("line")

    public String line;
    @SerializedName("email")

    public String email;
    @SerializedName("telp_cs")

    public String telpCs;
    @SerializedName("checking_status")

    public String checkingStatus;
    @SerializedName("default_status")

    public String defaultStatus;
    @SerializedName("wellcome_to_mvicall")

    public String wellcomeToMvicall;
    @SerializedName("please_sign_up_to_continue")

    public String pleaseSignUpToContinue;
    @SerializedName("what_do_you_think")

    public String whatDoYouThink;
    @SerializedName("status_for_all_contact")

    public String statusForAllContact;
    @SerializedName("error_fetching_data")

    public String errorFetchingData;
    @SerializedName("how_to")

    public String howTo;
    @SerializedName("best_deals")

    public String bestDeals;
    @SerializedName("reward")

    public String reward;
    @SerializedName("sign_out")

    public String signOut;
    @SerializedName("signing_out_wording")

    public String signingOutWording;
    @SerializedName("title_walkthrough8")

    public String titleWalkthrough8;
    @SerializedName("wording_intro8")

    public String wordingIntro8;
    @SerializedName("best_deal_empty_wording")

    public String bestDealEmptyWording;
    @SerializedName("image_intro")

    public String imageIntro;
    @SerializedName("wording_foreground")

    public String wordingForeground;
    @SerializedName("delete_confirm")

    public String deleteConfirm;
    @SerializedName("approve_cofirm")

    public String approveCofirm;
    @SerializedName("cancel_confirm")

    public String cancelConfirm;
    @SerializedName("wording_showcase_friends")

    public String wordingShowcaseFriends;
    @SerializedName("wording_showcase_profile")

    public String wordingShowcaseProfile;
    @SerializedName("wording_showcase_editvideotone")

    public String wordingShowcaseEditvideotone;
    @SerializedName("change_personal_video")

    public String changePersonalVideo;
    @SerializedName("more_personal_video")

    public String morePersonalVideo;
    @SerializedName("update_name")

    public String updateName;
    @SerializedName("wording_update_name")

    public String wordingUpdateName;
    @SerializedName("update_status")

    public String updateStatus;
    @SerializedName("wording_update_status")

    public String wordingUpdateStatus;
    @SerializedName("set_status_contact")

    public String setStatusContact;
    @SerializedName("wording_set_status_contact")

    public String wordingSetStatusContact;
    @SerializedName("synchronize_contact")

    public String synchronizeContact;
    @SerializedName("wording_sync_contact")

    public String wordingSyncContact;
    @SerializedName("wording_change_video")

    public String wordingChangeVideo;
    @SerializedName("wording_more_videos")

    public String wordingMoreVideos;
    @SerializedName("disslike")

    public String disslike;
    @SerializedName("like")

    public String like;
    @SerializedName("title_update_app")

    public String titleUpdateApp;
    @SerializedName("msg_update_app")

    public String msgUpdateApp;
    @SerializedName("wording_report")

    public String wordingReport;
    @SerializedName("wording_showcase_rec")

    public String wordingShowcaseRec;
    @SerializedName("record_video")

    public String recordVideo;
    @SerializedName("wording_show_case_gallery")

    public String wordingShowCaseGallery;
    @SerializedName("record")

    public String record;
    @SerializedName("choose_video")

    public String chooseVideo;
    @SerializedName("real_name")

    public String realName;
    @SerializedName("delete_message")

    public String deleteMessage;
    @SerializedName("wording_sc_confirmation")

    public String wordingScConfirmation;
    @SerializedName("no")

    public String no;
    @SerializedName("wording_sc_invite_friends")

    public String wordingScInviteFriends;
    @SerializedName("title_dialog_cek_simcard_umb")

    public String titleDialogCekSimcardUmb;
    @SerializedName("wording_next_showcase")

    public String wordingNextShowcase;
    @SerializedName("button_sc_next")

    public String buttonScNext;
    @SerializedName("finish")

    public String finish;
    @SerializedName("wording_finish_sc")

    public String wordingFinishSc;
    @SerializedName("start_now")

    public String startNow;
    @SerializedName("wording_upload_personal_video")

    public String wordingUploadPersonalVideo;
    @SerializedName("upload_personal_video")

    public String uploadPersonalVideo;
    @SerializedName("invite_more_friends")

    public String inviteMoreFriends;
    @SerializedName("pref_locale")

    public String prefLocale;
    @SerializedName("syncronizing")

    public String syncronizing;
    @SerializedName("authenticating")

    public String authenticating;
    @SerializedName("synchronization_process")

    public String synchronizationProcess;
    @SerializedName("wording_failure_response")

    public String wordingFailureResponse;
    @SerializedName("removing_profile")

    public String removingProfile;
    @SerializedName("profile_removed")

    public String profileRemoved;
    @SerializedName("updating_profile")

    public String updatingProfile;
    @SerializedName("updating_status")

    public String updatingStatus;
    @SerializedName("update_status_success")

    public String updateStatusSuccess;
    @SerializedName("update_status_failed")

    public String updateStatusFailed;
    @SerializedName("update_profile_success")

    public String updateProfileSuccess;
    @SerializedName("update_profile_failed")

    public String updateProfileFailed;
    @SerializedName("uploading")

    public String uploading;
    @SerializedName("upload_video_failed")

    public String uploadVideoFailed;
    @SerializedName("authentication_failed")

    public String authenticationFailed;
    @SerializedName("free")

    public String free;
    @SerializedName("grab_now")

    public String grabNow;
    @SerializedName("wording_enter_code")

    public String wordingEnterCode;
    @SerializedName("wording_wrong_number")

    public String wordingWrongNumber;
    @SerializedName("wording_button_continue")

    public String wordingButtonContinue;
    @SerializedName("wording_digit")

    public String wordingDigit;
    @SerializedName("wording_resend_code")

    public String wordingResendCode;
    @SerializedName("wording_redeem_promo")

    public String wordingRedeemPromo;
    @SerializedName("wechat")

    public String wechat;
    @SerializedName("viber")

    public String viber;
    @SerializedName("zalo")

    public String zalo;
    @SerializedName("wording_enable_back")

    public String wordingEnableBack;
    @SerializedName("wording_video_too_big")

    public String wordingVideoTooBig;
    @SerializedName("skip")

    public String skip;
    @SerializedName("wl_intro")

    public String alertNoSim;
    @SerializedName("alert_no_sim")

    public String wlIntro;
    @SerializedName("wl_intro_autostart")

    public String wlIntroAutostart;
    @SerializedName("notif_add_user")

    public String notifAddUser;
    @SerializedName("word_add_new_user")

    public String wordAddNewUser;
    @SerializedName("wording_title")

    public String wordingTitle;
    @SerializedName("progress_wording")

    public String progressWording;
    @SerializedName("redeem_poin")

    public String redeemPoin;
    @SerializedName("your_point")

    public String yourPoint;
    @SerializedName("point")

    public String point;
    @SerializedName("point_reward")

    public String pointReward;
    @SerializedName("choose")

    public String choose;
    @SerializedName("choose_5_video")

    public String choose5Video;
    @SerializedName("choose_5_more_video")

    public String choose5MoreVideo;
    @SerializedName("wording_over_choose_video")

    public String wordingOverChooseVideo;
    @SerializedName("wording_success_redeem_video")

    public String wordingSuccessRedeemVideo;
    @SerializedName("success")

    public String success;
    @SerializedName("not_enough_point")

    public String notEnoughPoint;
    @SerializedName("next")

    public String next;
    @SerializedName("back")

    public String back;
    @SerializedName("wording_success_redeem")

    public String wordingSuccessRedeem;
    @SerializedName("redeem_now")

    public String redeemNow;
    @SerializedName("wording_empty_reward")

    public String wordingEmptyReward;
    @SerializedName("wording_choose_free_content")

    public String wordingChooseFreeContent;
    @SerializedName("updating_video_tone")

    public String updatingVideoTone;
    @SerializedName("wording_succes_update_videotone")

    public String wordingSuccesUpdateVideotone;
    @SerializedName("how_to_get_points")

    public String howToGetPoints;
    @SerializedName("title_add_profile")

    public String titleAddProfile;
    @SerializedName("free_content")

    public String freeContent;
    @SerializedName("wording_got_free_content")

    public String wordingGotFreeContent;
    @SerializedName("go_to_my_collection")

    public String goToMyCollection;
    @SerializedName("word_button_telco_dialog_payment")

    public String wordButtonTelcoDialogPayment;
    @SerializedName("word_dialog_telco_pay")

    public String wordDialogTelcoPay;
    @SerializedName("word_telco_pos_button")

    public String wordTelcoPosButton;
    @SerializedName("gunakan_g_pay")

    public String gunakanGPay;
    @SerializedName("pilih_cara_pembayaran")

    public String pilihCaraPembayaran;
    @SerializedName("word_body_pay")

    public String wordBodyPay;
    @SerializedName("word_popup_register")

    public String wordPopupRegister;
    @SerializedName("word_change_in_dialog")

    public String wordChangeInDialog;
    @SerializedName("wording_change_wifi_state")

    public String wordingChangeWifiState;
    @SerializedName("turn_off_wifi")

    public String turnOffWifi;
    @SerializedName("turn_off_wifi_next")

    public String turnOffWifiNext;
    @SerializedName("wording_turn_off_wifi_subs")

    public String wordingTurnOffWifiSubs;
    @SerializedName("wording_wellcome_push")

    public String wordingWellcomePush;
    @SerializedName("thanks")

    public String thanks;
    @SerializedName("one_step_more")

    public String oneStepMore;
    @SerializedName("content_wording_scase")

    public String contentWordingScase;
    @SerializedName("empty_screen")

    public String emptyScreen;
    @SerializedName("please_retry")

    public String pleaseRetry;
    @SerializedName("retry")

    public String retry;
    @SerializedName("description_dialpad_back")

    public String descriptionDialpadBack;
    @SerializedName("description_dialpad_overflow")

    public String descriptionDialpadOverflow;
    @SerializedName("description_delete_button")

    public String descriptionDeleteButton;
    @SerializedName("description_image_button_plus")

    public String descriptionImageButtonPlus;
    @SerializedName("description_voicemail_button")

    public String descriptionVoicemailButton;
    @SerializedName("default_notification_description")

    public String defaultNotificationDescription;
    @SerializedName("callFailed_userBusy")

    public String callFailedUserBusy;
    @SerializedName("callFailed_congestion")

    public String callFailedCongestion;
    @SerializedName("callFailed_timedOut")

    public String callFailedTimedOut;
    @SerializedName("callFailed_server_unreachable")

    public String callFailedServerUnreachable;
    @SerializedName("callFailed_number_unreachable")

    public String callFailedNumberUnreachable;
    @SerializedName("callFailed_invalid_credentials")

    public String callFailedInvalidCredentials;
    @SerializedName("callFailed_out_of_network")

    public String callFailedOutOfNetwork;
    @SerializedName("callFailed_server_error")

    public String callFailedServerError;
    @SerializedName("callFailed_noSignal")

    public String callFailedNoSignal;
    @SerializedName("callFailed_limitExceeded")

    public String callFailedLimitExceeded;
    @SerializedName("callFailed_powerOff")

    public String callFailedPowerOff;
    @SerializedName("callFailed_simError")

    public String callFailedSimError;
    @SerializedName("callFailed_outOfService")

    public String callFailedOutOfService;
    @SerializedName("callFailed_fdn_only")

    public String callFailedFdnOnly;
    @SerializedName("callFailed_dialToUssd")

    public String callFailedDialToUssd;
    @SerializedName("callFailed_dialToSs")

    public String callFailedDialToSs;
    @SerializedName("callFailed_dialToDial")

    public String callFailedDialToDial;
    @SerializedName("callFailed_dsac_restricted")

    public String callFailedDsacRestricted;
    @SerializedName("callFailed_dsac_restricted_emergency")

    public String callFailedDsacRestrictedEmergency;
    @SerializedName("callFailed_dsac_restricted_normal")

    public String callFailedDsacRestrictedNormal;
    @SerializedName("callFailed_unobtainable_number")

    public String callFailedUnobtainableNumber;
    @SerializedName("incall_error_missing_voicemail_number")

    public String incallErrorMissingVoicemailNumber;
    @SerializedName("callFailed_video_call_tty_enabled")

    public String callFailedVideoCallTtyEnabled;
    @SerializedName("ringtone_silent")

    public String ringtoneSilent;
    @SerializedName("ringtone_unknown")

    public String ringtoneUnknown;
    @SerializedName("wording_timeline")

    public String wordingTimeline;
    @SerializedName("db_name")

    public String dbName;
    @SerializedName("tap_to_exit")

    public String tapToExit;
    @SerializedName("wording_empty_verification_code")

    public String wordingEmptyVerificationCode;

    @SerializedName("link_share_app")
    public String linkShareaApp;

    @SerializedName("save")
    public String save;

    @SerializedName("live_explore")
    public String liveExplore;

    @SerializedName("holla")
    public String holla;

    @SerializedName("press_to_exit")
    public String pressToExit;

    @SerializedName("explore")
    public String explore;

    @SerializedName("set_video_tone")
    public String setVideoTone;

    @SerializedName("success_get_free_content")
    public String successGetFreeContent;

    @SerializedName("failed_get_free_content")
    public String failedGetFreeContent;

    @SerializedName("your_point_mgm")
    public String yourPointMGM;

    @SerializedName("wording_sc_mgm")
    public String wordingScMGM;

    @SerializedName("how_to_win")
    public String howToWin;

    @SerializedName("rewards")
    public String rewards;

    @SerializedName("winner")
    public String winner;

    // New wording for mgm
    @SerializedName("tnc_mgm")
    public String tnc_mgm;

    @SerializedName("rewards_n_winner")
    public String rewardsnWinner;
    //------------

    @SerializedName("mgm")
    public String mgm;

    @SerializedName("leaderboard")
    public String leaderboard;

    //New Wording................

    @SerializedName("wording_edit_name_demo")
    public String wordingEditNameDemo;

    @SerializedName("wording_edit_video_demo")
    public String wordingEditVideoDemo;

    @SerializedName("wording_button_premium")
    public String wordingButtonPremium;

    @SerializedName("desc_intro_demo")
    public String descIntroDemo;

    @SerializedName("wording_title_intro_demo")
    public String wordingTitleIntroDemo;

    @SerializedName("wording_next_intro_demo")
    public String wordingNextIntroDemo;

    @SerializedName("wording_demo_call")
    public String wordingDemoCall;

    @SerializedName("wording_demo_sync")
    public String wordingDemoSync;

    @SerializedName("wording_demo_next")
    public String wordingDemoNext;

    @SerializedName("wording_share_intro_demo")
    public String wordingShareIntroDemo;

    @SerializedName("wording_button_intro_demo")
    public String wordingButtonIntroDemo;

    @SerializedName("wording_demo_edu")
    public String wordingDemoEdu;

    @SerializedName("wording_demo_cation")
    public String wordingDemoCation;

    @SerializedName("title_activity_free_content")
    public String titleActivityFreeContent;

    @SerializedName("title_demo_profile")
    public String titleDemoProfile;

    @SerializedName("title_demo_video")
    public String titleDemoVideo;

    @SerializedName("button_premium")
    public String buttonPremium;

    @SerializedName("button_next")
    public String buttonNext;

    @SerializedName("title_demo_dialer")
    public String titleDemoDialer;

    @SerializedName("title_demo_share")
    public String titleDemoShare;

    @SerializedName("demo_dialer_name")
    public String demoDialerName;

    @SerializedName("demo_dialer_label")
    public String demoDialerLabel;

    @SerializedName("demo_dialer_status")
    public String demoDialerStatus;

    @SerializedName("wording_video_failure")
    public String wordingVideoFailure;

    //Wording AppRTC................

    @SerializedName("status_incoming_apprtc")
    public String statusIncomingAppRtc;

    @SerializedName("state_incoming_apprtc")
    public String stateIncomingAppRtc;

    @SerializedName("state_calling_apprtc")
    public String stateCallingAppRtc;

    @SerializedName("state_connected_apprtc")
    public String stateConnectedAppRtc;

    @SerializedName("state_ringing_apprtc")
    public String stateRingingAppRtc;

    @SerializedName("state_unknow_apprtc")
    public String stateUnknowAppRtc;

    @SerializedName("notif_title_outgoing")
    public String notifTitleOutgoing;

    @SerializedName("notif_content_outgoing")
    public String notifContentOutgoing;

    @SerializedName("notif_title_during_call")
    public String notifTitleDuringCall;

    @SerializedName("notif_content_during_call")
    public String notifContentDuringCall;

    @SerializedName("notif_title_incoming")
    public String notifTitleIncoming;

    @SerializedName("notif_content_incoming")
    public String notifContentIncoming;

    //Wording Timeline................
    @SerializedName("timeline_like_button")
    public String timelineLikeButton;

    @SerializedName("timeline_report_button")
    public String timelineReportButton;

    // Wording Contact US
    @SerializedName("tag_ig")
    public String tagIg;

    @SerializedName("tagFbm")
    public String tagFbm;

    @SerializedName("cancel_wording")
    public String cancelWording;

    @SerializedName("msg_dialog")
    public String msgDialog;

    @SerializedName("successUseVideo")
    public String successUseVideo;

    @SerializedName("registeredNumber")
    public String registeredNumber;

    @SerializedName("wordingFreeSubs")
    public String wordingFreeSubs;

    @SerializedName("wordingFinalShowCase")
    public String wFinalShowCase;

    @SerializedName("isNameEmptyNotification")
    public String isNameEmptyNotification;

    @SerializedName("msgBeforeUpload")
    public String msgBeforeUpload;

    @SerializedName("wordingAgree")
    public String wordingAgree;

    @SerializedName("wordingCuration")
    public String wordingCuration;

    @SerializedName("wordingDownloads")
    public String wordingDownloads;

    @SerializedName("backsound")
    public String backSound;

    //Wording Vietnam
    @SerializedName("caseTitleIntro")
    public String caseTitleIntro;

    @SerializedName("caseSubtitleIntro")
    public String caseSubtitleIntro;

    @SerializedName("caseTitleInputName")
    public String caseTitleInputName;

    @SerializedName("caseSubtitleInputName")
    public String caseSubtitleInputName;

    @SerializedName("caseNameEditText")
    public String caseNameEditText;

    @SerializedName("caseTitleFinishInput")
    public String caseTitleFinishInput;

    @SerializedName("caseTitleIntroMvicall")
    public String caseTitleIntroMvicalll;

    @SerializedName("caseTitleTypes")
    public String caseTitleTypes;

//    @SerializedName("current_video_tone")
//    public String caseButtonRecord;

    @SerializedName("caseButtonRecordDesc")
    public String caseButtonRecordDesc;

//    @SerializedName("button_premium")
//    public String caseButtonPremium;

    @SerializedName("caseButtonPremiumDesc")
    public String caseButtonPremiumDesc;

    @SerializedName("caseButtonPremiumOffering")
    public String caseButtonPremiumOffering;

    @SerializedName("caseChoosePremiumVideo")
    public String caseChoosePremiumVideo;

    @SerializedName("caseJustClickHere")
    public String caseJustClickHere;

    @SerializedName("caseSeeVideoRintone")
    public String caseSeeVideoRintone;

//    @SerializedName("back")
//    public String caseNext;

    @SerializedName("caseTitleRecordVideo")
    public String caseTitleRecordVideo;

    @SerializedName("caseTitleUploadVideo")
    public String caseTitleUploadVideo;

    @SerializedName("caseSubtitleUploadVideo")
    public String caseSubtitleUploadVideo;

    @SerializedName("caseTitleAlmostSet")
    public String caseTitleAlmostSet;

    @SerializedName("caseTitleAlmostSet2")
    public String caseTitleAlmostSet2;

    @SerializedName("caseTitleAutomatically")
    public String caseTitleAutomatically;

    @SerializedName("caseTitleSynchronize")
    public String caseTitleSynchronize;

    @SerializedName("caseTitleShare")
    public String caseTitleShare;

    @SerializedName("caseTitleTimeShare")
    public String caseTitleTimeShare;

    @SerializedName("caseTitleFriendWillBeJoin")
    public String caseTitleFriendWillBeJoin;

    @SerializedName("caseTitleFriendWillBeJoin2")
    public String caseTitleFriendWillBeJoin2;

    @SerializedName("caseTitleAlmostForgot")
    public String caseTitleAlmostForgot;

    @SerializedName("caseTitleBrowseVideo")
    public String caseTitleBrowseVideo;

    @SerializedName("caseTitleEditProfileHere")
    public String caseTitleEditProfileHere;

    @SerializedName("caseEditProfile")
    public String caseEditProfile;

    @SerializedName("caseCongratulations")
    public String caseCongratulations;

    @SerializedName("caseSubtitleCongrat")
    public String caseSubtitleCongrat;

    @SerializedName("caseTitleValidation")
    public String caseTitleValidation;

    @SerializedName("caseTitleRecord")
    public String caseTitleRecord;

    @SerializedName("caseTitleVideo")
    public String caseTitleVideo;

    @SerializedName("caseTitleRecordBtn")
    public String caseTitleRecordBtn;

    @SerializedName("caseTitleVideoBtn")
    public String caseTitleVideoBtn;

    @SerializedName("caseTitleSave")
    public String caseTitleSave;

    //NEW SHOWCASE
    @SerializedName("titleSetProfile")
    public String titleSetProfile;

    @SerializedName("titleSkip")
    public String titleSkip;

    @SerializedName("titleMulai")
    public String titleMulai;

    @SerializedName("titleChooseVideo")
    public String titleChooseVideo;

    @SerializedName("titleSeeVideo")
    public String titleSeeVideo;

    @SerializedName("titlePreviewVideo")
    public String titlePreviewVideo;

    @SerializedName("titleYourFriend")
    public String titleYourFriend;

    @SerializedName("titleListFriend")
    public String titleListFriend;

    @SerializedName("titleSyncContact")
    public String titleSyncContact;

    @SerializedName("titleShare")
    public String titleShare;

    @SerializedName("subtitleShare")
    public String subtitleShare;

    @SerializedName("titleFriendJoin")
    public String titleFriendJoin;

    @SerializedName("titleShareMV")
    public String titleShareMV;

    @SerializedName("titleCongrats")
    public String titleCongrats;

    @SerializedName("subtileCongrat")
    public String subtileCongrat;

    @SerializedName("titleAddMusic")
    public String titleAddMusic;

    @SerializedName("titleYes")
    public String titleYes;

    @SerializedName("titleNo")
    public String titleNo;

    @SerializedName("titleSuccessMerge")
    public String titleSuccessMerge;

    @SerializedName("titleDownload")
    public String titleDownload;

    @SerializedName("see_collection")
    public String seeCollection;

    @SerializedName("titleTutorial")
    public String titleTutorial;

    @SerializedName("titleFAQ")
    public String titleFAQ;

    @SerializedName("titleLogout")
    public String titleLogout;

    @SerializedName("subtitleLogout")
    public String subtitleLogout;

    //New Wording Thailand
    @SerializedName("str_register_page_search")
    public String str_register_page_search;

    @SerializedName("str_register_page_country")
    public String str_register_page_country;

    @SerializedName("str_otp_page_invalid_code")
    public String str_otp_page_invalid_code;

    @SerializedName("str_all_page_rto")
    public String str_all_page_rto;

    @SerializedName("str_set_up_profile_sync")
    public String str_set_up_profile_sync;

    @SerializedName("str_set_up_profile_vid_profile")
    public String str_set_up_profile_vid_profile;

    @SerializedName("str_set_up_profile_not_empty")
    public String str_set_up_profile_not_empty;

    @SerializedName("str_set_personal_stat_nmr_tdk_kenal")
    public String str_set_personal_stat_nmr_tdk_kenal;

    @SerializedName("str_set_personal_stat_update_profil")
    public String str_set_personal_stat_update_profil;

    @SerializedName("str_set_personal_stat_add_profil")
    public String str_set_personal_stat_add_profil;

    @SerializedName("str_set_personal_stat__no_msisdn")
    public String str_set_personal_stat__no_msisdn;

    @SerializedName("str_set_personal_stat__profile_update")
    public String str_set_personal_stat__profile_update;

    @SerializedName("str_set_personal_stat__profile_added")
    public String str_set_personal_stat__profile_added;

    @SerializedName("str_set_personal_stat_update_failed")
    public String str_set_personal_stat_update_failed;

    @SerializedName("str_set_personal_stat_add_failed")
    public String str_set_personal_stat_add_failed;

    @SerializedName("str_edit_profile_remove_failed")
    public String str_edit_profile_remove_failed;

    @SerializedName("str_toast_data_failure")
    public String str_toast_data_failure;

    @SerializedName("str_toast_inbox_data_failure")
    public String str_toast_inbox_data_failure;

    @SerializedName("str_about_mvicall_mark")
    public String str_about_mvicall_mark;

    @SerializedName("str_about_mvicall_versi")
    public String str_about_mvicall_versi;

    @SerializedName("str_video_edit_video_prepared")
    public String str_video_edit_video_prepared;

    @SerializedName("str_video_editor_sec")
    public String str_video_editor_sec;

    @SerializedName("str_video_editor_save")
    public String str_video_editor_save;

    @SerializedName("str_video_editor_cancel")
    public String str_video_editor_cancel;

    @SerializedName("str_toast_choose_backsound_failure_data")
    public String str_toast_choose_backsound_failure_data;

    @SerializedName("str_preview_video_please_wait")
    public String str_preview_video_please_wait;

    @SerializedName("str_preview_video_preaparing")
    public String str_preview_video_preaparing;

    @SerializedName("str_preview_video_not_support")
    public String str_preview_video_not_support;

    @SerializedName("str_preview_video_failed_upload")
    public String str_preview_video_failed_upload;

    @SerializedName("str_preview_video_ready_upload")
    public String str_preview_video_ready_upload;

    @SerializedName("str_preview_video_premium_parsing_failed")
    public String str_preview_video_premium_parsing_failed;

    @SerializedName("str_preview_video_premium_data_kosong")
    public String str_preview_video_premium_data_kosong;

    @SerializedName("str_toast_intro_activity2_data_failed")
    public String str_toast_intro_activity2_data_failed;

    @SerializedName("str_toast_intro_activity2_profil_failure")
    public String str_toast_intro_activity2_profil_failure;

    @SerializedName("str_toast_friends_not_sync")
    public String str_toast_friends_not_sync;

    @SerializedName("str_toast_friends_not_installed")
    public String str_toast_friends_not_installed;

    @SerializedName("str_friends_sync_process")
    public String str_friends_sync_process;

    @SerializedName("str_friends_sync_complete")
    public String str_friends_sync_complete;

    @SerializedName("str_pesta_reward_leader_empty")
    public String str_pesta_reward_leader_empty;

    @SerializedName("str_wording_error_failed")
    public String str_wording_error_failed;

    @SerializedName("str_wording_like")
    public String str_wording_like;

    @SerializedName("str_wording_report")
    public String str_wording_report;

    @SerializedName("str_sending_data")
    public String str_sending_data;

    @SerializedName("str_session_error")
    public String str_session_error;

    @SerializedName("str_failed_register")
    public String str_failed_register;

    @SerializedName("str_auth_failed")
    public String str_auth_failed;

    @SerializedName("str_connection_failed")
    public String str_connection_failed;

    @SerializedName("str_have_content")
    public String str_have_content;

//Transaction OTP
    @SerializedName("str_jdl_otp_act")
    public String str_jdl_otp_act;

    @SerializedName("str_content_otp")
    public String str_content_otp;

    @SerializedName("str_jdl_insert")
    public String str_jdl_insert;

    @SerializedName("str_text_otp")
    public String str_text_otp;

    @SerializedName("str_btn_buy")
    public String str_btn_buy;

    @SerializedName("str_no_get_otp")
    public String str_no_get_otp;

    @SerializedName("str_resend_otp")
    public String str_resend_otp;

    @SerializedName("str_for")
    public String str_for;

    @SerializedName("str_title_subs")
    public String str_title_subs;

    @SerializedName("str_monthly")
    public String str_monthly;

    @SerializedName("str_subscribe")
    public String str_subscribe;

    @SerializedName("title_ais_dialog")
    public String title_ais_dialog;

    @SerializedName("title_ok")
    public String title_ok;

    @SerializedName("block_not_ais_user")
    public String block_not_ais_user;

    @SerializedName("str_second")
    public String str_second;

    @SerializedName("str_verification_invalid")
    public String str_verification_invalid;

    @SerializedName("str_verification_failed")
    public String str_verification_failed;

    @SerializedName("str_error_retrieval")
    public String str_error_retrieval;

    @SerializedName("str_cannot_play")
    public String str_cannot_play;

    @SerializedName("str_input_name")
    public String str_input_name;

    @SerializedName("str_wording_trim_progress")
    public String str_wording_trim_progress;

    @SerializedName("str_wording_more_than_video")
    public String str_wording_more_than_video;

    @SerializedName("str_select_song")
    public String str_select_song;
}
