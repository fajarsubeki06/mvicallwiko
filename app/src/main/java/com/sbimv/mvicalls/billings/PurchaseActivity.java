package com.sbimv.mvicalls.billings;

import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.icallservices.Log;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchaseActivity extends AppCompatActivity {

    ScrollView mScrollView;
    LinearLayout mLinearLayout;
    TextView mLoading;

    BillingClient mBillingClient;
    List<String> mSKUList = new ArrayList<>();
    private Button btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);

        mScrollView = new ScrollView(PurchaseActivity.this);
        mScrollView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mLinearLayout = new LinearLayout(PurchaseActivity.this);
        mLinearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mLinearLayout.setOrientation(LinearLayout.VERTICAL);
        mLinearLayout.setGravity(Gravity.CENTER);
        mLinearLayout.setPadding(40, 40, 40, 40);
        mScrollView.addView(mLinearLayout);

        mLoading = new TextView(this);
        mLoading.setText("LOADING PRODUCTS...");
        mLoading.setTextColor(Color.BLUE);
        mLinearLayout.addView(mLoading);
        setContentView(mScrollView);

        setupBillingClient();
        loadProducts();
    }

    private void setupBillingClient() {
        mBillingClient = BillingClient
                .newBuilder(this)
                .setListener(mPurchaseUpdateListener)
                .build();

        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(int billingResponseCode) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    System.out.println("BILLING | startConnection | RESULT OK");
                } else {
                    System.out.println("BILLING | startConnection | RESULT: "+billingResponseCode);
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                System.out.println("BILLING | onBillingServiceDisconnected | DISCONNECTED");
            }
        });
    }

    void loadProducts(){
        Call<List<String>> call = ServicesFactory.getTestService().getProducts();
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    mLinearLayout.removeAllViews();
                    for (final String product : response.body()) {

                        mSKUList.add(product);
                        
                        btn = new Button(PurchaseActivity.this);
                        btn.setText(product);
                        btn.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        btn.setOnClickListener(view -> {
                            if (!TextUtils.isEmpty(product))
                                onActionSubscribe(product);

                        });
                        mLinearLayout.addView(btn);

                    }

                }else {
                    Log.i("test_failed","test_failed");
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t) {
                mLoading.setText(t.getLocalizedMessage()==null? "" : t.getLocalizedMessage());
            }
        });
    }

    PurchasesUpdatedListener mPurchaseUpdateListener = (responseCode, purchases) -> {
        if (responseCode == BillingClient.BillingResponse.OK){
            System.out.println("BILLING | startConnection | RESULT OK");
        } else if (responseCode == BillingClient.BillingResponse.USER_CANCELED){
            System.out.println("BILLING | startConnection | RESULT: "+responseCode);
        } else{
            System.out.println("BILLING | onBillingServiceDisconnected | DISCONNECTED");
        }
    };

    public void onActionSubscribe(String product){
        if (mBillingClient.isReady()) {
            SkuDetailsParams params = SkuDetailsParams
                    .newBuilder()
                    .setSkusList(mSKUList)
                    .setType(BillingClient.SkuType.SUBS)
                    .build();

            mBillingClient.querySkuDetailsAsync(params, (responseCode, skuDetailsList) -> {
                if(responseCode == BillingClient.BillingResponse.OK){
                    System.out.println("querySkuDetailsAsync, responseCode: "+responseCode);
                    if(skuDetailsList.size() > 0) {

                        for (int i = 0; i<skuDetailsList.size(); i++){
                            String idProduct = skuDetailsList.get(i).getSku();
                            if (product.equals(idProduct)){
                                performSubscribe(skuDetailsList.get(i));
                                return;
                            }
                        }

                    }
                }else{
                    System.out.println("Can't querySkuDetailsAsync, responseCode: "+responseCode);
                }
            });
        } else {
            System.out.println("Billing Client not ready");
        }
    }

    public void performSubscribe(SkuDetails details){
        BillingFlowParams billingFlowParams = BillingFlowParams
                .newBuilder()
                .setSkuDetails(details)
                .build();
        mBillingClient.launchBillingFlow(this, billingFlowParams);
    }
}
