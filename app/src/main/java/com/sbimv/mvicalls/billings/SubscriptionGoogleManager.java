package com.sbimv.mvicalls.billings;


import android.app.Activity;
import androidx.annotation.NonNull;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustEvent;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.GpayData;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SubscriptionGoogleManager {

    private BillingClient mBillingClient;
    private List<String> mSKUList;
    private SubscriptionGoogleListener mListener;
    private SkuDetails skuDetails;
    private Activity activity;

    private String priceLabel = "";
    private String purchasedJson;

    public SubscriptionGoogleManager (Activity activity, SubscriptionGoogleListener listener){
        this.activity = activity;
        this.mListener = listener;
        initBillingClient();
    }

    private void initBillingClient() {
        mBillingClient = BillingClient
                .newBuilder(activity)
                .setListener(mPurchaseUpdateListener)
                .build();
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(int responseCode) {
                if (responseCode == BillingClient.BillingResponse.OK) {
                    mListener.onBillingConnected(true);
                }else{
                    mListener.onBillingConnected(false);
                }

                if (mSKUList != null && mSKUList.size()>0) {
                    String sku_id = mSKUList.get(0);
                    if (!TextUtils.isEmpty(sku_id)) {
                        checkSubsHistory(sku_id);
                    }
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                mListener.onBillingConnected(false);
            }
        });
    }

    private PurchasesUpdatedListener mPurchaseUpdateListener = (responseCode, purchases) -> {
        if (responseCode == BillingClient.BillingResponse.OK){
            if (purchases != null) {
                mListener.onSubscribe(purchases.get(0).getOriginalJson(),
                        String.valueOf(skuDetails.getPriceAmountMicros() / 1000000),
                        skuDetails.getSubscriptionPeriod());

                AdjustEvent event = new AdjustEvent("n7yxf2"); // Adjust event tracker,
                event.setCallbackId("aj_app_register_complete");
                event.addCallbackParameter("key", "value");
                event.addPartnerParameter("foo", "bar");
                Adjust.trackEvent(event);

                Toast.makeText(activity, "Success subscribe", Toast.LENGTH_SHORT).show();
            }
        }
        else if (responseCode == BillingClient.BillingResponse.USER_CANCELED){
            System.out.println("BILLING | startConnection | RESULT: "+responseCode);
        }
        else{
            System.out.println("BILLING | onBillingServiceDisconnected | DISCONNECTED");
        }
    };

    public void loadSKU(String skuID){
        if (mBillingClient.isReady()) {
            mSKUList = Arrays.asList(skuID);

            SkuDetailsParams params = SkuDetailsParams
                    .newBuilder()
                    .setSkusList(mSKUList)
                    .setType(BillingClient.SkuType.SUBS)
                    .build();

            mBillingClient.querySkuDetailsAsync(params, (int responseCode, List<SkuDetails> skuDetailsList)->{

                if(responseCode == BillingClient.BillingResponse.OK){
                    if(skuDetailsList.size() > 0) {
                        skuDetails = skuDetailsList.get(0);

                        String period = skuDetails.getSubscriptionPeriod();//*****
                        String periodLabel = "Day";
                        if(period.toLowerCase().contains("w"))
                            periodLabel = "Week";
                        else if(period.toLowerCase().contains("m"))
                            periodLabel = "Month";
                        else if(period.toLowerCase().contains("y"))
                            periodLabel = "Year";

                        priceLabel = skuDetails.getPrice() + "/ "+ periodLabel + ", autorenewal";
                        mListener.onProductLoaded();
                    }

                }else{
                    System.out.println("Can't querySkuDetailsAsync, responseCode: "+responseCode);
                }
            });
        } else {
            System.out.println("Billing Client not ready");
        }
    }

    public void performSubscribe(){
        BillingFlowParams billingFlowParams = BillingFlowParams
                .newBuilder()
                .setSkuDetails(skuDetails)
                .build();
        mBillingClient.launchBillingFlow(activity, billingFlowParams);
    }

    private void checkSubsHistory(String skuID){
        purchasedJson = null;
        Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.SUBS);
        if(purchasesResult != null && purchasesResult.getPurchasesList() != null && purchasesResult.getPurchasesList().size() > 0){
            for(Purchase purchase:purchasesResult.getPurchasesList()){
                String sku = purchase.getSku();
                if(!TextUtils.isEmpty(sku) && sku.equals(skuID)){
                    purchasedJson = purchase.getOriginalJson();
                    mListener.onSubsChecked(true);
                    return;
                }
            }
        }
        mListener.onSubsChecked(false);
    }

    public void updateSubsStatus(String msisdn, String type, String json, boolean cancel, String periode, String price) {

        if(!TextUtils.isEmpty(msisdn)) {

            Call<APIResponse<ArrayList<String>>> call = ServicesFactory.getService().getSubsGpay(msisdn, type, json, cancel, periode, price);
            call.enqueue(new Callback<APIResponse<ArrayList<String>>>() {
                @Override
                public void onResponse(@NonNull Call<APIResponse<ArrayList<String>>> call, @NonNull Response<APIResponse<ArrayList<String>>> response) {
                    if (response.body() != null && response.isSuccessful()) {
                        mListener.onUpdateSubsStatus(true, json, price, periode);
                        try {
                            GpayData gpayData = new GpayData(msisdn, type, json, cancel, periode, price);
                            SessionManager.saveDataGpay(activity, gpayData); // Save data g-pay....
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        return;
                    }
                    mListener.onUpdateSubsStatus(false, json, price, periode);
                }

                @Override
                public void onFailure(@NonNull Call<APIResponse<ArrayList<String>>> call, @NonNull Throwable t) {
                    mListener.onUpdateSubsStatus(false, json, price, periode);
                }

            });

        }
    }

    public static abstract class SubscriptionGoogleListener {
        protected void onBillingConnected(boolean success){}
        protected void onProductLoaded(){}
        protected void onSubscribe(String json, String price, String period){}
        protected void onUpdateSubsStatus(boolean success, String json, String price, String period){}
        protected void onSubsChecked(boolean exists){}
    }

}
