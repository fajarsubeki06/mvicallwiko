package com.sbimv.mvicalls.billings;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.icallservices.Log;

//import org.solovyev.android.checkout.ActivityCheckout;
//import org.solovyev.android.checkout.Billing;
//import org.solovyev.android.checkout.Checkout;
//import org.solovyev.android.checkout.Purchase;
//import org.solovyev.android.checkout.RequestListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionsActivity extends AppCompatActivity {

    ScrollView mScrollView;
    LinearLayout mLinearLayout;
    TextView mLoading;
//    private ActivityCheckout mCheckout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mScrollView = new ScrollView(SubscriptionsActivity.this);
        mScrollView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mLinearLayout = new LinearLayout(SubscriptionsActivity.this);
        mLinearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mLinearLayout.setOrientation(LinearLayout.VERTICAL);
        mLinearLayout.setGravity(Gravity.CENTER);
        mLinearLayout.setPadding(40, 40, 40, 40);
        mScrollView.addView(mLinearLayout);

        mLoading = new TextView(this);
        mLoading.setText("LOADING PRODUCTS...");
        mLoading.setTextColor(Color.BLUE);
        mLinearLayout.addView(mLoading);
        setContentView(mScrollView);

//        final Billing billing = AppDelegate.get(SubscriptionsActivity.this).getBilling();
//        mCheckout = Checkout.forActivity(SubscriptionsActivity.this, billing);
//        mCheckout.start();

        loadProducts();

    }

    void loadProducts(){
        Call<List<String>> call = ServicesFactory.getTestService().getProducts();
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    mLinearLayout.removeAllViews();
                    Button btn;

                    for (final String product : response.body()) {

                        btn = new Button(SubscriptionsActivity.this);
                        btn.setText(product);
                        btn.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        btn.setOnClickListener(view -> {
//                            if (!TextUtils.isEmpty(product))
//                                purchase(product);
                        });
                        mLinearLayout.addView(btn);
                    }

                }else {
                    Log.i("test_failed","test_failed");
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t) {
                mLoading.setText(t.getLocalizedMessage()==null? "" : t.getLocalizedMessage());
            }
        });
    }

//    private void purchase(String sku) {
//        mCheckout.startPurchaseFlow("subs",sku, null, new PurchaseListener());
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        mCheckout.onActivityResult(requestCode, resultCode, data);
//        super.onActivityResult(requestCode, resultCode, data);
//    }
//
//    @Override
//    protected void onDestroy() {
//        mCheckout.stop();
//        super.onDestroy();
//    }
//
//    private class PurchaseListener implements RequestListener<Purchase> {
//        @Override
//        public void onSuccess(@Nonnull Purchase result) {
//            Toast.makeText(SubscriptionsActivity.this, "Success...", Toast.LENGTH_SHORT).show();
//            loadProducts();
//        }
//
//        @Override
//        public void onFailure(int response, @Nonnull Exception e) {
//            Toast.makeText(SubscriptionsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
//            loadProducts();
//        }
//    }

}
