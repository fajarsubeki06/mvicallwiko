package com.sbimv.mvicalls;

import android.app.Application;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class GATRacker {

    private static GATRacker instance;

    AppDelegate appDelegate;

    public static GATRacker getInstance(Application application){
        if(instance == null)
            instance = new GATRacker(application);
        return instance;
    }

    private GATRacker(Application application){
        appDelegate = (AppDelegate) application;
    }

    public void sendScreen(String screenName){
        Tracker tracker = appDelegate.getTracker();
        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.ScreenViewBuilder().setCampaignParamsFromUrl("").build());
    }

    public void sendEventWithScreen(String screenName,String cat,String action,String label,long val){
        Tracker tracker = appDelegate.getTracker();
        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(cat)
                .setAction(action)
                .setLabel(label)
                .setValue(val)
                .build());
    }
    public void sendEventWithScreen(String screenName,String cat,String action,String label){
        Tracker tracker = appDelegate.getTracker();
        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(cat)
                .setAction(action)
                .setLabel(label)
                .build());
    }


    public void sendEvent(String cat,String action,String label){
        Tracker tracker = appDelegate.getTracker();
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(cat)
                .setAction(action)
                .setLabel(label)
                .build());
    }

}
