/*
 * Copyright (C) 2019 MViCall
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sbimv.mvicalls.icallservices;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.IBinder;
import androidx.annotation.RequiresApi;
import android.telecom.Call;
import android.telecom.CallAudioState;
import android.telecom.InCallService;

import com.sbimv.mvicalls.view.CallerViewUtil;

@RequiresApi(api = Build.VERSION_CODES.M)
public class InCallServiceImpl extends InCallService {

    @Override
    public void onCallAudioStateChanged(CallAudioState audioState) {
        AudioModeProvider.getInstance().onAudioStateChanged(audioState);
    }

    @Override
    public void onBringToForeground(boolean showDialpad) {
        InCallPresenter.getInstance().onBringToForeground(showDialpad);
    }

    @Override
    public void onCallAdded(Call call) {

        currentRingMode();
        try {
            dismissVideoCaller();
        } catch (Exception e) {
            e.printStackTrace();
        }
        CallList.getInstance().onCallAdded(call);
        InCallPresenter.getInstance().onCallAdded(call);
    }

    @Override
    public void onCallRemoved(Call call) {

        currentRingMode();
        try {
            dismissVideoCaller();
        } catch (Exception e) {
            e.printStackTrace();
        }
        CallList.getInstance().onCallRemoved(call);
        InCallPresenter.getInstance().onCallRemoved(call);
    }

    @Override
    public void onCanAddCallChanged(boolean canAddCall) {
        InCallPresenter.getInstance().onCanAddCallChanged(canAddCall);
    }

    @Override
    public IBinder onBind(Intent intent) {
        final Context context = getApplicationContext();
        final ContactInfoCache contactInfoCache = ContactInfoCache.getInstance(context);
        InCallPresenter.getInstance().setUp(
                getApplicationContext(),
                CallList.getInstance(),
                AudioModeProvider.getInstance(),
                new StatusBarNotifier(context, contactInfoCache),
                contactInfoCache,
                new ProximitySensor(context, AudioModeProvider.getInstance(), new AccelerometerListener(context)));
        InCallPresenter.getInstance().onServiceBind();
        InCallPresenter.getInstance().maybeStartRevealAnimation(intent);
        TelecomAdapter.getInstance().setInCallService(this);

        return super.onBind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        super.onUnbind(intent);

        currentRingMode();
        try {
            dismissVideoCaller();
        } catch (Exception e) {
            e.printStackTrace();
        }

        InCallPresenter.getInstance().onServiceUnbind();
        tearDown();

        return false;
    }

    private void tearDown() {

        currentRingMode();
        try {
            dismissVideoCaller();
        } catch (Exception e) {
            e.printStackTrace();
        }

        TelecomAdapter.getInstance().clearInCallService();
        CallList.getInstance().clearOnDisconnect();
        InCallPresenter.getInstance().tearDown();
    }

    private void currentRingMode(){

        try {
            final Context mContext = getApplicationContext();
            AudioManager mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
            int mCurrentRingerMode = mAudioManager.getRingerMode();
            int mCurrentMusicVolume =  mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            int mCurrentRingerVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_RING);

            mAudioManager.setRingerMode(mCurrentRingerMode);
            mAudioManager.setStreamVolume(AudioManager.STREAM_RING, mCurrentRingerVolume,0);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurrentMusicVolume,0);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void dismissVideoCaller() throws Exception{
        final Context context = getApplicationContext();
        if (context!= null) {
            CallerViewUtil.getInstance(context).dismissView();
        }
    }

}
