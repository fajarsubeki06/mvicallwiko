/*
 * Copyright (C) 2019 MViCall
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sbimv.mvicalls.icallservices.Utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import androidx.core.content.ContextCompat;
import android.telecom.TelecomManager;
import android.text.TextUtils;

import com.sbimv.mvicalls.icallservices.common.TelecomManagerCompat;

public class TelecomUtil {
    private static boolean sWarningLogged = false;

    public static void silenceRinger(Context context) {
        if (hasModifyPhoneStatePermission(context)) {
            try {
                TelecomManagerCompat.silenceRinger(getTelecomManager(context));
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }
    public static boolean hasModifyPhoneStatePermission(Context context) {
        return isDefaultDialer(context)
                || hasPermission(context, Manifest.permission.MODIFY_PHONE_STATE);
    }

    private static boolean hasPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isDefaultDialer(Context context) {
        final boolean result = TextUtils.equals(context.getPackageName(), TelecomManagerCompat.getDefaultDialerPackage(getTelecomManager(context)));
        if (result) {
            sWarningLogged = false;
        } else {
            if (!sWarningLogged) {
                sWarningLogged = true;
            }
        }
        return result;
    }

    private static TelecomManager getTelecomManager(Context context) {
        return (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
    }
}
