package com.sbimv.mvicalls.icallservices.widgetDialpad.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;

import com.sbimv.mvicalls.R;


public class SettingsUtil {
    private static final String DEFAULT_NOTIFICATION_URI_STRING =
            Settings.System.DEFAULT_NOTIFICATION_URI.toString();

    public static void updateRingtoneName(
            Context context, Handler handler, int type, String key, int msg) {
        final Uri ringtoneUri;
        boolean defaultRingtone = false;
        if (type == RingtoneManager.TYPE_RINGTONE) {
            ringtoneUri = RingtoneManager.getActualDefaultRingtoneUri(context, type);
        } else {
            final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            String uriString = prefs.getString(key, DEFAULT_NOTIFICATION_URI_STRING);
            if (TextUtils.isEmpty(uriString)) {
                ringtoneUri = null;
            } else {
                if (uriString.equals(DEFAULT_NOTIFICATION_URI_STRING)) {
                    defaultRingtone = true;
                    ringtoneUri = RingtoneManager.getActualDefaultRingtoneUri(context, type);
                } else {
                    ringtoneUri = Uri.parse(uriString);
                }
            }
        }
        CharSequence summary = context.getString(R.string.ringtone_unknown);
        if (ringtoneUri == null) {
            summary = context.getString(R.string.ringtone_silent);
        } else {
            final Ringtone ringtone = RingtoneManager.getRingtone(context, ringtoneUri);
            if (ringtone != null) {
                try {
                    final String title = ringtone.getTitle(context);
                    if (!TextUtils.isEmpty(title)) {
                        summary = title;
                    }
                } catch (SQLiteException sqle) {
                    sqle.printStackTrace();
                }
            }
        }
        if (defaultRingtone) {
            summary = context.getString(R.string.default_notification_description, summary);
        }
        handler.sendMessage(handler.obtainMessage(msg, summary));
    }
}
