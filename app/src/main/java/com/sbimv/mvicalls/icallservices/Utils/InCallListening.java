package com.sbimv.mvicalls.icallservices.Utils;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.ContactsContract;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.telecom.Call;
import android.telecom.VideoProfile;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.Toast;

import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.icallservices.DialerActivity;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;
import com.sbimv.mvicalls.view.CallerViewUtil;

import retrofit2.Callback;
import retrofit2.Response;


public class InCallListening {

    private static InCallListening instance;
    private Context appContext;
    private Call mCall;
    private Cursor cursor;
    private String phoneState;
    private static final int NOTIFICATION_IN_CALL = 1;

    public static InCallListening getInstance(Context _appContext){
        if(instance == null){
            instance = new InCallListening(_appContext);
        }
        return instance;
    }

    private InCallListening(Context ctx){
        appContext = ctx;
    }

    public void callAdded (Call call){
        this.mCall = call;
        if (mCall != null){
            String phoneNumber = infoPhoneNumber(call);

            if (!TextUtils.isEmpty(phoneNumber)) {

                String prefix = AppPreference.getPreference(appContext, AppPreference.PREFIX);
                String msisdn = Util.formatMSISDNREG(phoneNumber,prefix);

                TelephonyManager telephonyManager = (TelephonyManager) appContext.getSystemService(Context.TELEPHONY_SERVICE);
                MyPhoneStateListener phoneListener = new MyPhoneStateListener(appContext);
                telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

                if (call.getState() == Call.STATE_RINGING) {
                    phoneState = "incoming";
                    inCommingProcess(msisdn);
                }else if (call.getState() == Call.STATE_SELECT_PHONE_ACCOUNT || call.getState() == Call.STATE_CONNECTING) {
                    phoneState = "outgoing";
                    outGoingCallProcess(msisdn);
                }
                String name = TextUtils.isEmpty(infoName(phoneNumber)) ? "Unknow..." : infoName(phoneNumber);
                callIntent(phoneNumber,name,phoneState);
                statusbarNotification("InCall",phoneState);
            }
        }
    }

    private class MyPhoneStateListener extends PhoneStateListener {
        Context ctx;
        boolean wasRinging;
        public MyPhoneStateListener(Context ctx) {
            this.ctx = ctx;
        }

        public void onCallStateChanged(int state, final String incomingNumber) {
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                wasRinging = true;
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                Toast.makeText(appContext, "Connected...", Toast.LENGTH_SHORT).show();
                LocalBroadcastManager.getInstance(appContext).sendBroadcast(new Intent("com.yp021004.calldefault.Connected"));
                if(wasRinging){
                    CallerViewUtil.getInstance(ctx.getApplicationContext()).dismissView();
                }
                wasRinging = false;
            }else if (state == TelephonyManager.CALL_STATE_IDLE){
                wasRinging = false;
                CallerViewUtil.getInstance(ctx.getApplicationContext()).dismissView();
            }
        }
    }

    /*
     * Process InCommingCaller............................
     * */
    public void inCommingProcess(final String msisdn) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ContactItem item = ContactDBManager.getInstance(appContext).getCallerByMSISDN(msisdn);
                if (item == null) {
                    CallerViewUtil.getInstance(appContext).dismissView();
                } else {
                    CallerViewUtil.getInstance(appContext).showView(msisdn,true,true);

                    // send push call in
                    ContactItem me = SessionManager.getProfile(appContext);
                    if(me != null && !TextUtils.isEmpty(me.caller_id)) {
                        sendCallPushIn(me.caller_id, item.caller_id);
                    }
                }
            }
        }, 1000);

    }

    void sendCallPushIn(String caller_id, String caller_id_target){
        retrofit2.Call<APIResponse> call = ServicesFactory.getService().sendPushCall(caller_id, caller_id_target, "ID");
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(retrofit2.Call<APIResponse> call, Response<APIResponse> response) { }

            @Override
            public void onFailure(retrofit2.Call<APIResponse> call, Throwable t) { }
        });
    }

    /*
     * Process OutGoingCaller...........................
     * */
    private void outGoingCallProcess(final String msisdn){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                ContactItem item = ContactDBManager.getInstance(appContext).getCallerByMSISDN(msisdn);
                if (item == null)
                    return;

                CallerViewUtil.getInstance(appContext).showView(msisdn,true,false);
                ContactItem me = SessionManager.getProfile(appContext);

                if(me != null && !TextUtils.isEmpty(me.caller_id)) {
                    sendCallPushOut(me.caller_id, item.caller_id);
                }

            }
        }, 300);
    }

    void sendCallPushOut(String caller_id, String caller_id_target){
        retrofit2.Call<APIResponse> call = ServicesFactory.getService().sendPushCall(caller_id, caller_id_target, "out");
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(retrofit2.Call<APIResponse> call, Response<APIResponse> response) { }

            @Override
            public void onFailure(retrofit2.Call<APIResponse> call, Throwable t) { }
        });
    }

    private String infoPhoneNumber(Call details){
        String subscribeInfo = "";
            Uri uri = details.getDetails().getHandle();
            String decode = Uri.decode(uri != null ? uri.getSchemeSpecificPart() : null);
            CharSequence charSequence = decode;
            if (charSequence != null){
                subscribeInfo = charSequence.toString();
            }else {
                subscribeInfo = "";
            }
        return subscribeInfo;
    }

    public String infoName(String msisdn){
        ContentResolver cr = appContext.getContentResolver();
        String selection = "(" + ContactsContract.CommonDataKinds.Phone.NUMBER + " = '"+msisdn+"') OR (" + ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER + " = '"+msisdn+"')";
        String[] projection = new String[] {
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER
        };

        try{
            cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, selection, null, null);
        }catch (Exception e){
            e.printStackTrace();
        }

        if (cursor!=null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                if (!TextUtils.isEmpty(name)) {
                    return name;
                }
            }
        }
        return "Unknow...";
    }

    public void loadSpeaker(AudioManager mAudioManager){
        if (mAudioManager.isSpeakerphoneOn()) {
            mAudioManager.setSpeakerphoneOn(false);
            Toast.makeText(appContext, "Loudspeaker Off", Toast.LENGTH_SHORT).show();
        } else {
            mAudioManager.setSpeakerphoneOn(true);
            Toast.makeText(appContext, "Loudspeaker On", Toast.LENGTH_SHORT).show();
        }
    }

    public void endCall(Activity activity){
        if (mCall != null){
            mCall.disconnect();
            mCall = null;
            activity.finish();
        }
    }

    public void answare(){
        if (mCall != null){
            mCall.answer(VideoProfile.STATE_AUDIO_ONLY);
        }
    }

    public void holdCall(boolean checked){
        if (mCall != null){
            if (checked) {
                mCall.hold();
                Toast.makeText(appContext, "Call Hold", Toast.LENGTH_SHORT).show();
            }else {
                mCall.unhold();
                Toast.makeText(appContext, "Call Unhold", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void muteMic(AudioManager mAudioManager){
        if (mAudioManager.isMicrophoneMute()) {
            mAudioManager.setMicrophoneMute(false);
            Toast.makeText(appContext, "Microphone unmute", Toast.LENGTH_SHORT).show();
        } else {
            mAudioManager.setMicrophoneMute(true);
            Toast.makeText(appContext, "Microphone mute", Toast.LENGTH_SHORT).show();
        }
    }

    public void callIntent(String phoneNumber, String name, String state){
        appContext.startActivity(new Intent(appContext, DialerActivity.class)
                .putExtra("msisdn",phoneNumber)
                .putExtra("name",name)
                .putExtra("state",state)
                .setFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION | Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    public void playDtmfTone(char digit) {
        if (mCall != null) {
            mCall.playDtmfTone(digit);
        }
    }

    public void stopDtmfTone() {
        if (mCall != null) {
            mCall.stopDtmfTone();
        }
    }

    public void statusbarNotification(String title, String content){

        NotificationManager mNotificationManager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
        final Notification.Builder builder = getNotificationBuilder();

        Intent intent = new Intent(appContext, DialerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent inCallPendingIntent = PendingIntent.getActivity(appContext, 0, intent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel("", "mvicall_notif", importance);
            builder.setAutoCancel(false);
            builder.setContentIntent(inCallPendingIntent);
            builder.setContentTitle(title);
            builder.setContentText(content);
            builder.setAutoCancel(false);
            builder.setSmallIcon(R.drawable.ic_call_white_24dp);
            builder.setChannelId("");
            mNotificationManager.createNotificationChannel(mChannel);

        }else {
            builder.setCategory(Notification.CATEGORY_CALL);
            builder.setSmallIcon(R.drawable.fab_ic_call);
            builder.setContentTitle(title);
            builder.setContentText(content);
            builder.setContentIntent(inCallPendingIntent);
            builder.setColor(appContext.getResources().getColor(R.color.dialer_theme_color));
            builder.build();

        }

        Notification notification = builder.build();
        mNotificationManager.notify(NOTIFICATION_IN_CALL, notification);
    }

    private Notification.Builder getNotificationBuilder() {
        final Notification.Builder builder = new Notification.Builder(appContext);
        builder.setOngoing(true);
        builder.setPriority(Notification.PRIORITY_HIGH);
        return builder;
    }

    public void clearAllCallNotifications() {
        NotificationManager notificationManager =
                (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_IN_CALL);

    }

}
