package com.sbimv.mvicalls.icallservices.services;

import android.content.Intent;
import android.os.Trace;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.telecom.Call;
import android.telecom.InCallService;

import com.sbimv.mvicalls.icallservices.Utils.InCallListening;
import com.sbimv.mvicalls.view.CallerViewUtil;

public class InCallServiceImp extends InCallService {
    @Override
    public void onCallAdded(Call call) {
        Trace.beginSection("InCallServiceImpl.onCallAdded");
        InCallListening.getInstance(getApplicationContext()).callAdded(call);
        Trace.endSection();
    }

    @Override
    public void onCallRemoved(Call call) {
        Trace.beginSection("InCallServiceImpl.onCallAdded");
        LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(new Intent("com.yp021004.calldefault.onCallRemoved"));
        CallerViewUtil.getInstance(getApplicationContext()).dismissView();
        InCallListening.getInstance(getApplicationContext()).clearAllCallNotifications();
        Trace.endSection();
    }
}
