package com.sbimv.mvicalls.icallservices;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.icallservices.Utils.InCallListening;
import com.sbimv.mvicalls.icallservices.fragments.KeypadFragment;


public class DialerActivity extends AppCompatActivity implements View.OnClickListener {

    private KeypadFragment mKeypadFragment;
    private String mDtmfText;
    private ImageButton mEndCall;
    private ImageButton mEndCallTwo;

    private ToggleButton mButtonLoadSpeak;
    private ToggleButton mButtonDialpad;
    private ToggleButton mHoldButton;
    private ToggleButton mMuteButton;

    private TextView mPhoneNumber;
    private AudioManager mAudioManager;
    private TextView mName;
    private FrameLayout mFrameEndCall;
    private RelativeLayout mFrameAnsware;
    private ImageButton mAnsware;
    private boolean isHoldSelected = false;
    private int time = 0;
    private TextView textTimer;

    @Override
    protected void onCreate(Bundle saveIntance) {
        super.onCreate(saveIntance);

        int flags = WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_IGNORE_CHEEK_PRESSES;
        getWindow().addFlags(flags);

        setContentView(R.layout.activity_dialer);

        mButtonLoadSpeak = findViewById(R.id.audioButton);
        mButtonDialpad = findViewById(R.id.dialpadButton);
        mHoldButton = findViewById(R.id.holdButton);
        mMuteButton = findViewById(R.id.muteButton);

        mPhoneNumber = findViewById(R.id.txtPhoneNumber);
        mName = findViewById(R.id.txtName);
        mAnsware = findViewById(R.id.floating_answare_button);
        mEndCall = findViewById(R.id.floating_end_call_action_button);
        mEndCallTwo = findViewById(R.id.floating_end_button);
        mFrameEndCall = findViewById(R.id.floating_end_call);
        mFrameAnsware = findViewById(R.id.relative_answare);
        textTimer = findViewById(R.id.tvtimer);

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            String strMsisdn = bundle.getString("msisdn");
            String strName = bundle.getString("name");
            String strState = bundle.getString("state");
            mName.setText(strName);
            mPhoneNumber.setText("Calling : " + strMsisdn);

            if (!TextUtils.isEmpty(strState)) {
                if (strState.equals("incoming")) {
                    inCallSelected(true);
                } else {
                    inCallSelected(false);
                }
            }
        }

        mButtonLoadSpeak.setOnClickListener(this);
        mButtonDialpad.setOnClickListener(this);
        mEndCall.setOnClickListener(this);
        mEndCallTwo.setOnClickListener(this);
        mAnsware.setOnClickListener(this);
        mHoldButton.setOnClickListener(this);
        mMuteButton.setOnClickListener(this);
    }

    private void inCallSelected(boolean isComming) {
        if (isComming == true) {
            mFrameEndCall.setVisibility(View.GONE);
            mFrameAnsware.setVisibility(View.VISIBLE);
        } else {
            mFrameEndCall.setVisibility(View.VISIBLE);
            mFrameAnsware.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter _if = new IntentFilter("com.yp021004.calldefault.onCallRemoved");
        LocalBroadcastManager.getInstance(DialerActivity.this).registerReceiver(removeUiDialer, _if);

        IntentFilter _it = new IntentFilter("com.yp021004.calldefault.Connected");
        LocalBroadcastManager.getInstance(DialerActivity.this).registerReceiver(timerUiDialer, _it);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(DialerActivity.this).unregisterReceiver(removeUiDialer);
        LocalBroadcastManager.getInstance(DialerActivity.this).unregisterReceiver(timerUiDialer);
    }

    private BroadcastReceiver timerUiDialer = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            textTimer.setText("Connected");
        }
    };

    private BroadcastReceiver removeUiDialer = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            finishActivity();
        }
    };

    private void finishActivity() {
        if (!DialerActivity.this.isFinishing()) {
            mName.setText("");
            mPhoneNumber.setText("");
            InCallListening.getInstance(DialerActivity.this).endCall(DialerActivity.this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mKeypadFragment != null) {
            mKeypadFragment.setDtmfText(mDtmfText);
            mDtmfText = null;
        }
    }

    @Override
    protected void onPause() {
        if (mKeypadFragment != null) {
            mKeypadFragment.onDialerKeyUp(null);
        }
        super.onPause();
    }

    private void changeStateSelected(ToggleButton button){

    }

    @Override
    public void onClick(View view) {
        if (mButtonLoadSpeak == view){
            if (!mButtonLoadSpeak.isSelected()){
                mButtonLoadSpeak.setSelected(true);
                mButtonLoadSpeak.setBackground(getResources().getDrawable(R.drawable.btn_compound_audio_white));
            }else {
                mButtonLoadSpeak.setSelected(false);
                mButtonLoadSpeak.setBackground(getResources().getDrawable(R.drawable.btn_compound_audio));
            }
            InCallListening.getInstance(DialerActivity.this).loadSpeaker(mAudioManager);

        }else if (mButtonDialpad == view){

            Fragment fragment = new KeypadFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_frame, fragment, fragment.getClass().getSimpleName())
                    .addToBackStack(null)
                    .commit();

        }else if (mEndCall == view){

            InCallListening.getInstance(DialerActivity.this).endCall(DialerActivity.this);

        }else if (mAnsware == view){
            InCallListening.getInstance(DialerActivity.this).answare();
            inCallSelected(false);
        }else if (mEndCallTwo == view){
            InCallListening.getInstance(DialerActivity.this).endCall(DialerActivity.this);
        }else if (mHoldButton == view){

            if (isHoldSelected == false) {
                isHoldSelected = true;
                mHoldButton.setSelected(true);
                mHoldButton.setBackground(getResources().getDrawable(R.drawable.btn_compound_hold_white));
                InCallListening.getInstance(DialerActivity.this).holdCall(!mHoldButton.isSelected());
            }else {
                isHoldSelected = false;
                mHoldButton.setSelected(false);
                mHoldButton.setBackground(getResources().getDrawable(R.drawable.btn_compound_hold));
                InCallListening.getInstance(DialerActivity.this).holdCall(mHoldButton.isSelected());
            }

        }else if (mMuteButton == view){
            if (!mMuteButton.isSelected()){
                mMuteButton.setSelected(true);
                mMuteButton.setBackground(getResources().getDrawable(R.drawable.btn_compound_mute_white));
            }else {
                mMuteButton.setSelected(false);
                mMuteButton.setBackground(getResources().getDrawable(R.drawable.btn_compound_mute));
            }
            InCallListening.getInstance(DialerActivity.this).muteMic(mAudioManager);
        }
    }

//    public void setExcludeFromRecents(boolean exclude) {
//        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//        List<ActivityManager.AppTask> tasks = am.getAppTasks();
//        int taskId = getTaskId();
//        for (int i = 0; i < tasks.size(); i++) {
//            ActivityManager.AppTask task = tasks.get(i);
//            if (task.getTaskInfo().id == taskId) {
//                try {
//                    task.setExcludeFromRecents(exclude);
//                } catch (RuntimeException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

}
