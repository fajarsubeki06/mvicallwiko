/*
 * Copyright (C) 2019 MViCall
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.sbimv.mvicalls.icallservices;

import android.telephony.PhoneNumberUtils;

/**
 * Logic for call buttons.
 */
public class DialpadPresenter extends Presenter<DialpadPresenter.DialpadUi> implements InCallPresenter.InCallStateListener {

    private Call mCall;
    private static final String ID_PREFIX = Call.class.getSimpleName() + "_";
    private int sIdCounter = 0;

    @Override
    public void onUiReady(DialpadUi ui) {
        super.onUiReady(ui);
        InCallPresenter.getInstance().addListener(this);
        mCall = CallList.getInstance().getOutgoingOrActive();
    }

    @Override
    public void onUiUnready(DialpadUi ui) {
        super.onUiUnready(ui);
        InCallPresenter.getInstance().removeListener(this);
    }

    @Override
    public void onStateChange(InCallPresenter.InCallState oldState, InCallPresenter.InCallState newState, CallList callList) {
        mCall = callList.getOutgoingOrActive();
    }

    /**
     * Processes the specified digit as a DTMF key, by playing the
     * appropriate DTMF tone, and appending the digit to the EditText
     * field that displays the DTMF digits sent so far.
     *
     */
    public final void processDtmf(char c) {
        if (PhoneNumberUtils.is12Key(c) && mCall != null) {
            getUi().appendDigitsToField(c);
            String fn = mCall.getId();
            TelecomAdapter.getInstance().playDtmfTone(fn, c);
        }
    }

    public void stopDtmf() {
        if (mCall != null) {
            TelecomAdapter.getInstance().stopDtmfTone(mCall.getId());
        }
    }

    public interface DialpadUi extends Ui {
        void setVisible(boolean on);
        void appendDigitsToField(char digit);
    }
}
