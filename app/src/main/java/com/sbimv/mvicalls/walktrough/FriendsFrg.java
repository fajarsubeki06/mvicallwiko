package com.sbimv.mvicalls.walktrough;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.CursorLoader;
import androidx.loader.content.Loader;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TimeLineNewActivity;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.activity.MainActivity;
import com.sbimv.mvicalls.activity.VideoToneActivity;
import com.sbimv.mvicalls.adapter.ContactMVICallCursorAdapter;
import com.sbimv.mvicalls.db.ContactContentProvider;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.services.DownloadVideoService;
import com.sbimv.mvicalls.services.SyncConfigService;
import com.sbimv.mvicalls.sync.SyncContactProfileManager;
import com.sbimv.mvicalls.sync.SyncUtils;
import com.sbimv.mvicalls.util.PermissionUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseListener;
import com.sbimv.mvicalls.util.ShowCaseWrapper;

import java.util.Objects;

import me.toptas.fancyshowcase.FancyShowCaseView;

import static com.sbimv.mvicalls.BaseActivity.permissions;


public class FriendsFrg extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, ContactMVICallCursorAdapter.onListClickedCallListner {

    private static final int QUERY_ID = 123;
    // view binding
    static ListView mListView;
    // variables
    private ContactMVICallCursorAdapter mAdapter;
    private String mSearchTerm;
    private int mPreviouslySelectedSearchItem = 0;
    protected Cursor cursor;
    private ContactDBManager contactDBManager;
    private LangViewModel model;
    static RelativeLayout rlNext, relaViet;
    private LinearLayout lnDemo;
    public static ImageView mImgSync;
    private LinearLayout lnContainer;
    private Button btnNext;
    private TextView txtDemoDialer;
    private String strDemoDialer;
    private String strBtnNext;
    private OnCallFriendsListner onListener;
    private FancyShowCaseView fancyShowCaseViewFirst;
    private ImageView imgCall;
    private static boolean isfinishorDenied = false;

    //
    static LinearLayout linearLayout, linearLayoutViet;
    static RelativeLayout relativeShare;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCallFriendsListner) {
            onListener = (OnCallFriendsListner) context;
        }else {
            throw new ClassCastException(context.toString()
                    + " must implemenet ContactMVICallListFragment.OnCallDataClickedCallListner");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactDBManager = new ContactDBManager(getActivity());
        mAdapter = new ContactMVICallCursorAdapter(getActivity(), null, false, this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mPreviouslySelectedSearchItem == 0) {
            getLoaderManager().initLoader(QUERY_ID, null, this);
        }
    }

    @SuppressLint("CheckResult")
    @Nullable
    @Override
    public View onCreateView(@org.jetbrains.annotations.NotNull @NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact_demo, container, false);
        model = LangViewModel.getInstance();

        mListView = view.findViewById(R.id.lv_contact);
        imgCall = view.findViewById(R.id.imgCall);
        mImgSync = view.findViewById(R.id.img_sync);
        lnDemo = view.findViewById(R.id.ln_demo);
        btnNext = view.findViewById(R.id.btnNext);
        rlNext = view.findViewById(R.id.rl_next);
        relaViet = view.findViewById(R.id.relativeViet);
        lnContainer = view.findViewById(R.id.ln_container);
        txtDemoDialer = view.findViewById(R.id.txtDemoDialer);
        linearLayout = view.findViewById(R.id.linear);
        linearLayoutViet = view.findViewById(R.id.linearVietnam);
        relativeShare = view.findViewById(R.id.relativeShare);
        mListView.setClickable(true);

        strDemoDialer = TextUtils.isEmpty(model.getLang().string.titleDemoDialer)
                ?"Coba Video Ringtonemu !"
                : model.getLang().string.titleDemoDialer;

        strBtnNext = TextUtils.isEmpty(model.getLang().string.buttonNext)
                ?"Lanjut"
                : model.getLang().string.buttonNext;

        txtDemoDialer.setText(strDemoDialer);

        initViewNewShowcase(view);

        String ccodeViet = PreferenceUtil.getPref(getActivity()).getString(PreferenceUtil.COUNTRY_CODE,"1");
        if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
            linearLayoutViet.setVisibility(View.VISIBLE);
            relaViet.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.GONE);
        }

        strDemoDialer = TextUtils.isEmpty(model.getLang().string.titleDemoDialer)
                ?"Coba Video Ringtonemu !"
                : model.getLang().string.titleDemoDialer;

        strBtnNext = TextUtils.isEmpty(model.getLang().string.buttonNext)
                ?"Lanjut"
                : model.getLang().string.buttonNext;

        btnNext.setText(strBtnNext);

        mImgSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SyncUtils.permissionSyncContact(Objects.requireNonNull(getActivity()));

                String sync = TextUtils.isEmpty(model.getLang().string.synchronizationProcess)
                        ?"Synchronization in process..."
                        : model.getLang().string.synchronizationProcess;

                Toast.makeText(getActivity(), sync, Toast.LENGTH_SHORT).show();

            }
        });

        lnDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContactItem own = SessionManager.getProfile(getActivity());
                if (own != null) {
                    PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SHOW_CALL_DEMO, true).commit();
                    startActivity(new Intent(getActivity(), DemoDialerActivity.class));
                }
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ccodeViet = PreferenceUtil.getPref(getActivity()).getString(PreferenceUtil.COUNTRY_CODE,"1");
                if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
                    strtactivity();
                }else{
                    PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SETUP_CONTACT, true).commit();
                    startActivity(new Intent(getActivity(), ShareDemoActivity.class));
                    getActivity().finish();
//                    relativeShare.setVisibility(View.VISIBLE);
//                    mListView.setVisibility(View.GONE);
//                    rlNext.setVisibility(View.GONE);
                }
            }
        });

        try {
            GATRacker.getInstance(Objects.requireNonNull(getActivity()).getApplication()).sendScreen(TrackerConstant.SCREEN_DEMO_CONTACT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void initViewNewShowcase(View view){
        String strWordingListFriends = TextUtils.isEmpty(model.getLang().string.titleListFriend)
                ?"List Teman" :
                model.getLang().string.titleListFriend;

        String strWordingSkipButton = TextUtils.isEmpty(model.getLang().string.titleSkip)
                ?"Lewati" :
                model.getLang().string.titleSkip;

        String strWordingSync = TextUtils.isEmpty(model.getLang().string.titleSyncContact)
                ?"Sync Contact" :
                model.getLang().string.titleSyncContact;

        String strWordingTitle = TextUtils.isEmpty(model.getLang().string.titleShare)
                ?"Share & Menangkan!" :
                model.getLang().string.titleShare;

        String strWordingSubtitle = TextUtils.isEmpty(model.getLang().string.subtitleShare)
                ?"Share MViCall ke temanmu dan menangkan hadiahnya!" :
                model.getLang().string.subtitleShare;

        String strWordingFriendJoin = TextUtils.isEmpty(model.getLang().string.titleFriendJoin)
                ?"Ajak Teman" :
                model.getLang().string.titleFriendJoin;

        TextView titleTool = view.findViewById(R.id.judul_activity);
        Button buttonSkip = view.findViewById(R.id.btn_lewat);
        TextView textSync = view.findViewById(R.id.textSync);
        TextView titleShare = view.findViewById(R.id.textTitleShare);
        TextView subtitleShare = view.findViewById(R.id.textShare);
        Button buttonJoin = view.findViewById(R.id.buttonAjak);
        ImageView imageSync = view.findViewById(R.id.imageSync);

        titleTool.setText(strWordingListFriends);
        buttonSkip.setText(strWordingSkipButton);
        textSync.setText(strWordingSync);
        titleShare.setText(strWordingTitle);
        subtitleShare.setText(strWordingSubtitle);
        buttonJoin.setText(strWordingFriendJoin);

        buttonJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SETUP_CONTACT, true).commit();
               startActivity(new Intent(getActivity(), ShareDemoActivity.class));
               getActivity().finish();
            }
        });

        imageSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SyncUtils.permissionSyncContact(Objects.requireNonNull(getActivity()));

                String sync = TextUtils.isEmpty(model.getLang().string.synchronizationProcess)
                        ?"Synchronization in process..."
                        : model.getLang().string.synchronizationProcess;

                Toast.makeText(getActivity(), sync, Toast.LENGTH_SHORT).show();
            }
        });

        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCaseToFalse();
                //setFinalScase();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

    }

    public void setFinalScase(){
        PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.FINAL_SCASE, true).commit();
        Intent i = new Intent(getActivity(), SyncConfigService.class);
        getActivity().startService(i);
    }

    public void showCaseToFalse() {
        PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.SHOW_CASE_DEMO_PROFILE, false).commit();
        AppPreference.setPreferenceBoolean(getActivity(), AppPreference.FIRST_SHOW_CASE, false);
        AppPreference.setPreferenceBoolean(getActivity(), AppPreference.SHOW_CASE_TIMELINE, false);
        AppPreference.setPreferenceBoolean(getActivity(), AppPreference.SHOW_CASE_PROFILE, false);
        AppPreference.setPreferenceBoolean(getActivity(), AppPreference.SHOW_CASE_VIDEOS, false);
        AppPreference.setPreferenceBoolean(getActivity(), AppPreference.SHOW_CASE_FRIENDS, false);
        PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.SETUP_PROFILE, true).commit();
        PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.SETUP_CONTACT, true).commit();
        PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.SETUP_SHARE, true).commit();
        PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.SETUP_VIDEOTONE, true).commit();
    }

    private void strtactivity(){
        PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SETUP_CONTACT, true).commit();
        startActivity(new Intent(getActivity(), ShareDemoActivity.class).putExtra("ShareDemo", "true"));
        getActivity().finish();
    }

//    private void showCaseDemoCall() {
//        try {
//            LangViewModel model = LangViewModel.getInstance();
//            String strWordingDemoCall = TextUtils.isEmpty(model.getLang().string.wordingDemoCall) ? "Lihat bagaimana Video ringtonemu akan muncul saat kamu telfon temanmu dengan klik disini" : model.getLang().string.wordingDemoCall;
//            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
//            showCaseWrapper.showCaseDemoContact(ShowCaseWrapper.DEMO_CALL, strWordingDemoCall, lnDemo, new ShowCaseListener.ActionListener() {
//                @Override
//                public void onScreenTap() {
//                    ContactItem own = SessionManager.getProfile(getActivity());
//                    if (own != null) {
//                        PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SHOW_CALL_DEMO, true).commit();
//                        startActivity(new Intent(getActivity(), DemoDialerActivity.class));
//                    }
//                }
//
//                @Override
//                public void onOk() {
//                    ContactItem own = SessionManager.getProfile(getActivity());
//                    if (own != null) {
//                        PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SHOW_CALL_DEMO, true).commit();
//                        startActivity(new Intent(getActivity(), DemoDialerActivity.class));
//                    }
//                }
//
//                @Override
//                public void onSkip() {
//
//                }
//
//            });
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

//    private void showCaseDemoFive() {
//        try {
//            LangViewModel model = LangViewModel.getInstance();
//
//            String strWordingDemoEdu = TextUtils.isEmpty(model.getLang().string.wordingDemoEdu) ?
//                    "Sekarang yuk coba telfon temanmu! Video yang kamu rekam/pilih akan muncul langsung saat kamu telfon dia!" : model.getLang().string.wordingDemoEdu;
//
//            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
//            showCaseWrapper.showCaseDemoFive(ShowCaseWrapper.DEMO_FIVE, strWordingDemoEdu, new ShowCaseListener.ActionListener() {
//                @Override
//                public void onScreenTap() {
//                    showCaseDemoSync();
//                }
//
//                @Override
//                public void onOk() {
//                    showCaseDemoSync();
//                }
//
//                @Override
//                public void onSkip() {
//
//                }
//
//            });
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

//    private void showCaseDemoSync() {
//        try {
//            LangViewModel model = LangViewModel.getInstance();
//
//            String strWordingDemoSync = TextUtils.isEmpty(model.getLang().string.wordingDemoSync) ? "Klik disini untuk mulai sinkronisasi Teman MViCallmu" : model.getLang().string.wordingDemoSync;
//
//            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
//            showCaseWrapper.showCaseDemoContact(ShowCaseWrapper.DEMO_SYNC, strWordingDemoSync, mImgSync, new ShowCaseListener.ActionListener() {
//                @Override
//                public void onScreenTap() {
//                    SyncUtils.permissionSyncContact(Objects.requireNonNull(getActivity()));
//                    Toast.makeText(getActivity(), "Synchronization in process...", Toast.LENGTH_SHORT).show();
//                    showCaseDemoSevent();
//                }
//
//                @Override
//                public void onOk() {
//                    SyncUtils.permissionSyncContact(Objects.requireNonNull(getActivity()));
//                    Toast.makeText(getActivity(), "Synchronization in process...", Toast.LENGTH_SHORT).show();
//                    showCaseDemoSevent();
//                }
//
//                @Override
//                public void onSkip() {
//
//                }
//
//            });
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

//    private void showCaseDemoSevent() {
//        try {
//            LangViewModel model = LangViewModel.getInstance();
//            String strWordingDemoCation = TextUtils.isEmpty(model.getLang().string.wordingDemoCation) ?
//                    "Temanmu yang sudah gunakan MViCall akan muncul disni" : model.getLang().string.wordingDemoCation;
//            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
//            showCaseWrapper.showCaseDemoContact(ShowCaseWrapper.DEMO_SEVENT, strWordingDemoCation, lnContainer, new ShowCaseListener.ActionListener() {
//                @Override
//                public void onScreenTap() {
//                    showCaseDemoNext();
//                }
//
//                @Override
//                public void onOk() {
//                    showCaseDemoNext();
//                }
//
//                @Override
//                public void onSkip() {
//
//                }
//
//            });
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

//    private void showCaseDemoNext() {
//        try {
//            LangViewModel model = LangViewModel.getInstance();
//
//            String strWordingDemoNext = TextUtils.isEmpty(model.getLang().string.wordingDemoNext)
//                    ?"Belum ada Teman MViCall? Ajak temanmu untuk pakai MViCall dan dapatkan REWARD spesial dari MViCall. Tekan LANJUT untuk Share MViCall ke teman-temanmu" :
//                    model.getLang().string.wordingDemoNext;
//
//            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
//            showCaseWrapper.showFancyDemoEditVideo(ShowCaseWrapper.DEMO_NEXT, strWordingDemoNext, btnNext, new ShowCaseListener.ActionListener() {
//                @Override
//                public void onScreenTap() {
//
//                }
//
//                @Override
//                public void onOk() {
//
//                }
//
//                @Override
//                public void onSkip() {
//
//                }
//            });
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onResume() {
        super.onResume();

        //checkShowCase();

        String ccodeViet = PreferenceUtil.getPref(getActivity()).getString(PreferenceUtil.COUNTRY_CODE,"1");
        if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
            rlNext.setVisibility(View.VISIBLE);
            checkShowCase();
            return;
        }

        boolean showDemo = PreferenceUtil.getPref(Objects.requireNonNull(getActivity())).getBoolean(PreferenceUtil.SHOW_CALL_DEMO, false);
        if (showDemo){
            rlNext.setVisibility(View.VISIBLE);
            //showCaseDemoFive();
        }
    }

    private void checkShowCase(){
        boolean caseVietnamEight = PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceUtil.CASE_EIGHT_VIETNAME, false);
        boolean caseVietnamNine = PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceUtil.CASE_NINE_VIETNAME, false);

        if (!caseVietnamEight){
            showCaseIntroVietEight();
        }else if (!caseVietnamNine){
            showCaseIntroVietNine();
        }
        //showCaseIntroVietEight();

    }

    private void showCaseIntroVietEight() {
        try {
            PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.CASE_EIGHT_VIETNAME, true).commit();
            LangViewModel model = LangViewModel.getInstance();

            String strWordingTitleVietSeven = TextUtils.isEmpty(model.getLang().string.caseSeeVideoRintone)
                    ?"Now let's see how your new video ringtone will look when it appears on your friend's phone" :
                    model.getLang().string.caseSeeVideoRintone;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
            showCaseWrapper.showFancyVietnameEight(
                    ShowCaseWrapper.SC_VIET_EIGHT,
                    strWordingTitleVietSeven,
                    imgCall,
                    new ShowCaseListener.ActionListener() {

                @Override
                public void onScreenTap() {
                    showCaseIntroVietNine();
                }

                @Override
                public void onOk() {
                    showCaseIntroVietNine();
                }

                @Override
                public void onSkip() {
                    showCaseIntroVietNine();
                }

            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showCaseIntroVietNine() {
        try {
            PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.SHOW_CASE_SETUP2, false).commit();
            PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.CASE_NINE_VIETNAME, true).commit();
            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
            showCaseWrapper.showFancyVietnameNine(
                    ShowCaseWrapper.SC_VIET_NINE,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            prefFriendsCase();
                            startActivity(new Intent(getActivity(), SetupProfileActivity.class).putExtra("sc_viet","record"));
                            Objects.requireNonNull(getActivity()).finish();
                        }

                        @Override
                        public void onOk() {
                            prefFriendsCase();
                            startActivity(new Intent(getActivity(), SetupProfileActivity.class).putExtra("sc_viet","record"));
                            Objects.requireNonNull(getActivity()).finish();
                        }

                        @Override
                        public void onSkip() {
                            prefFriendsCase();
                            startActivity(new Intent(getActivity(), SetupProfileActivity.class).putExtra("sc_viet","record"));
                            Objects.requireNonNull(getActivity()).finish();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void prefFriendsCase(){
        PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.SHOW_CASE_FRIEND, true).commit();
        PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.SHOW_CASE_SETUP2, false).commit();
    }

    @NotNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        if (id == QUERY_ID) {
            Uri contentUri;

            if (mSearchTerm == null) {
                contentUri = ContactContentProvider.CONTENT_URI;
            } else {
                contentUri = Uri.withAppendedPath(ContactContentProvider.CONTENT_URI, Uri.encode(mSearchTerm));
            }

            return new CursorLoader(Objects.requireNonNull(getActivity()),
                    contentUri,
                    null,
                    null,
                    null,
                    null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(@NotNull Loader<Cursor> loader, Cursor data) {
        // This swaps the new cursor into the adapter.
        if (loader.getId() == QUERY_ID) {
            data = contactDBManager.getMVContactAll();
            mAdapter.swapCursor(data);

            String ccodeViet = PreferenceUtil.getPref(getActivity()).getString(PreferenceUtil.COUNTRY_CODE,"1");
            if (!TextUtils.isEmpty(ccodeViet) && !ccodeViet.equalsIgnoreCase("+84")){
                if (mAdapter.getCount() == 0){
                    relativeShare.setVisibility(View.VISIBLE);
                    mListView.setVisibility(View.GONE);
                    btnNext.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void onLoaderReset(@NotNull Loader<Cursor> loader) {
        if (loader.getId() == QUERY_ID) {
            mAdapter.swapCursor(null);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mListView.setAdapter(mAdapter);
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.UPDATE_FRIEND");
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).registerReceiver(updateListBR, _if);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).unregisterReceiver(updateListBR);
    }

    private BroadcastReceiver updateListBR = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Objects.requireNonNull(getContext()).getContentResolver().notifyChange(ContactContentProvider.CONTENT_URI, null, false);

        }
    };

    @Override
    public void onListSelected(Context ctx, String msisdn, String caller_id, String device) {
        if (!TextUtils.isEmpty(device) && device.equalsIgnoreCase("ios")){
            if (getActivity() instanceof OnCallFriendsListner) {
                onListener.onDataSelected(ctx,msisdn,caller_id);
            }
        }else {
            callConventional(ctx,msisdn);
//            showOptionCall(ctx,msisdn,caller_id);
        }
    }

    private void showOptionCall(Context ctx, String msisdn, String caller_id){
        new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setTitle("Panggilan telepon")
                .setMessage("Pilihan cara telepon teman mu")

                .setNegativeButton("Dengan data", (dialog, which) -> {
                    dialog.dismiss();
                    if (getActivity() instanceof OnCallFriendsListner) {
                        onListener.onDataSelected(ctx,msisdn,caller_id);
                    }
                })

                .setPositiveButton("Dengan pulsa", (dialog, which) -> {
                    dialog.dismiss();
                    callConventional(ctx,msisdn);
                }).show();
    }

    private void callConventional(Context ctx, String msisdn){
        Intent phoneIntent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", msisdn, null));
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        ctx.startActivity(phoneIntent);
    }

    public interface OnCallFriendsListner {
        void onDataSelected(Context ctx, String msisdn, String caller_id);
    }
}
