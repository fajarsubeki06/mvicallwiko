package com.sbimv.mvicalls.walktrough;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.google.firebase.database.annotations.NotNull;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TimeLineNewActivity;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.activity.MainActivity;
import com.sbimv.mvicalls.activity.PopUpSubsActivity;
import com.sbimv.mvicalls.activity.PopupVietSubsActivity;
import com.sbimv.mvicalls.activity.TrimmingActivity;
import com.sbimv.mvicalls.activity.VideoToneActivity;
import com.sbimv.mvicalls.adapter.BacksoundAdapter;
import com.sbimv.mvicalls.databinding.SetupProfileFrgBinding;
import com.sbimv.mvicalls.http.ProgressRequestBody;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.BackSound;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.services.DownloadVideoService;
import com.sbimv.mvicalls.services.SyncVideoProfileServices;
import com.sbimv.mvicalls.sync.SyncContactProfileManager;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.FileUtils;
import com.sbimv.mvicalls.util.ImageSelector;
import com.sbimv.mvicalls.util.OwnVideoManager;
import com.sbimv.mvicalls.util.PermissionUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseListener;
import com.sbimv.mvicalls.util.ShowCaseWrapper;
import com.sbimv.mvicalls.util.Util;
import com.sbimv.mvicalls.util.ValidationInputUtil;
import com.sbimv.mvicalls.util.VideoSelector;
import com.sbimv.mvicalls.util.video_audio_merge.StringVideo;
import com.sbimv.mvicalls.util.video_audio_merge.UtilVideoAudio;
import com.sbimv.mvicalls.util.video_audio_merge.VideoAudioCallBack;
import com.sbimv.mvicalls.util.video_audio_merge.VideoAudioMerge;
import com.sbimv.mvicalls.util.video_compress.CompressionListener;
import com.sbimv.mvicalls.util.video_compress.VideoCompressor;
import com.sbimv.mvicalls.view.DialogUpload;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import me.toptas.fancyshowcase.FancyShowCaseView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetupProfileActivity extends BaseActivity implements VideoAudioCallBack, View.OnClickListener, CompressionListener,
        View.OnTouchListener, BacksoundAdapter.OnItemClickListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener{

    private TextView mUserPhone;
    private Button btnSave;
    private ImageSelector mImageSelector;
    private CircleImageView ivUserPic;
    private File proPicPath;
    private FrameLayout mediaControllerAnchor;

    public final int REQUEST_ADD_PROFILE = 908;
    public final int REQUEST_EDIT_PROFILE = 907;
    @SuppressLint("StaticFieldLeak")
    public static EditText etUserName;
    private ImageView btnUserPic;
    private DialogUpload dialogUpload;
    private LinearLayout btnRecord;
    private LinearLayout btnGallery;
    private LinearLayout btnPremium, btnPremium2;
    private LinearLayout btnUploadVideo;
    private LinearLayout btnCancel;
    private VideoView videoView;
    private Button btnLanjut;

    private boolean isUploading = false;
    private boolean isSelectVideo = false;
    private boolean isScDoing = false;

    private Uri trimmedUri;
    private Uri videoUriForUpload;
    public final int REQUEST_PERMISSION = 789;
    private static final int REQUEST_GET_VIDEO = 0x01;
    private static final int REQUEST_VIDEO_TRIMMER = 0x02;
    final int REQUEST_CAPTURE = 1324;
    private RequestBody _caller_id;
    private String urlBy = "";
    private boolean setCont = false;

    private String path;
    private File file, masterVideoFile, masterAudioFile;
    private long length;

    //Ffmpeg
    private StringVideo stringVideo = new StringVideo();
    private ProgressDialog progressDialog;
    UtilVideoAudio utilVideoAudio = new UtilVideoAudio();
    private String filePathVideo, stringVideo_;
    Uri sendUri;

    //Backsound
    public MediaPlayer mediaPlayer;
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    private String outputhFileName = "audio" + ".mp3";
    private UtilVideoAudio utils = new UtilVideoAudio();
    private int duration = 0;
    private ImageView image;
    private List<BackSound> backSoundList = new ArrayList<>();
    android.app.AlertDialog alert;
    BacksoundAdapter backsoundAdapter;
    boolean onError;

    private LinearLayout buttonExplore;

    long delay = 1000;
    long last_text_edit = 0;
    Handler handler = new Handler();

    boolean doubleBackToExitPressedOnce = false;

    private String h = "";
    private String w = "";

    final String[] galleryPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    final String[] cameraPermissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

    VideoSelector mVideoSelector = VideoSelector.create(this, new VideoSelector.VideoSelectorDelegate() {
        @Override
        public void onCaptured(final Uri selectedUri) {
            new Handler().postDelayed(() -> {
                if (selectedUri != null) {
                    startTrimActivity(selectedUri);
                    PreferenceUtil.getEditor(SetupProfileActivity.this).putString(PreferenceUtil.FROM_CAMERA, "camera").commit();
                }
            }, 1000);
        }

        @Override
        public void onSelected(final Uri selectedUri) {
            showDialogs();
            new Handler().postDelayed(() -> {
                dissmisDialogs();

                try {
                    path = FileUtils.getRealPath(SetupProfileActivity.this, selectedUri);
                    if (path != null) {
                        file = new File(path);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    selectedButtonUpload(false);
                    String not = TextUtils.isEmpty(model.getLang().string.str_preview_video_not_support)
                            ? "File not Support"
                            : model.getLang().string.str_preview_video_not_support;
                    toast(not); // File not supported
                }

                try {
                    length = file.length();
                    length = length / 1024;

                    if (length > 40000) {
                        new android.app.AlertDialog.Builder(SetupProfileActivity.this).setMessage(model.getLang().string.wordingVideoTooBig).setCancelable(false).setPositiveButton("Ok", (dialog, which) -> {

                        }).show();

                    } else {
                        if (selectedUri != null) {
                            startTrimActivity(selectedUri);
                            PreferenceUtil.getEditor(SetupProfileActivity.this).putString(PreferenceUtil.FROM_GALLERY, "gallery").commit();

                        }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    selectedButtonUpload(false);
                    String not = TextUtils.isEmpty(model.getLang().string.str_preview_video_not_support)
                            ? "File not Support"
                            : model.getLang().string.str_preview_video_not_support;
                    toast(not); // File not supported
                }

            }, 1000);
        }

        @Override
        public void onCancelCapture() {
//            String ccode = PreferenceUtil.getPref(SetupProfileActivity.this).getString(PreferenceUtil.COUNTRY_CODE, "1");
//            if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
//                BaseActivity.showCaseIntroVietEleven(SetupProfileActivity.this);
//            }
        }

        @Override
        public void onCancelGallery() {
//            String ccode = PreferenceUtil.getPref(SetupProfileActivity.this).getString(PreferenceUtil.COUNTRY_CODE, "1");
//            if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
//                BaseActivity.showCaseIntroVietEleven(SetupProfileActivity.this);
//            }
        }
    });

    private boolean isProcessVideo = false;
    private boolean isProcessImage = false;
    private FancyShowCaseView fancyShowCaseViewFirst;
    ContactItem own;
    private LinearLayout lnSelectedVd;

    private VideoCompressor mVideoCompressor = new VideoCompressor(this);

    Button buttonSkip;
    LinearLayout linearClick;
    boolean isShowBs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SetupProfileFrgBinding binding = DataBindingUtil.setContentView(SetupProfileActivity.this, R.layout.setup_profile_frg);
        binding.setLangModel(model);

        lnSelectedVd = findViewById(R.id.lnSelectedVd);
        mUserPhone = findViewById(R.id.et_user_phone);
        EditText mUserPhoneViet = findViewById(R.id.et_user_phoneviet);
        btnSave = findViewById(R.id.btnSave);
        ivUserPic = findViewById(R.id.iv_user_pic);
        linearClick = findViewById(R.id.linearClick);
        //
        buttonSkip = findViewById(R.id.btn_lewat);

        etUserName = findViewById(R.id.et_user_name);
        etUserName.requestFocus();

        linearClick.setOnClickListener(view -> {
            try{
                Util.hideKeyboard(SetupProfileActivity.this);
            }catch (Exception e){
                e.printStackTrace();
            }
        });

        etUserName.setOnFocusChangeListener((view, b) -> {
            if (!b){
                try{
                    Util.hideKeyboard(SetupProfileActivity.this);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        btnUserPic = findViewById(R.id.btn_edit_user_pic);
        btnLanjut = findViewById(R.id.btnLanjut);

        btnRecord = findViewById(R.id.btnRecord);
        btnGallery = findViewById(R.id.btnGallery);
        btnPremium = findViewById(R.id.btnPremium);
        btnPremium2 = findViewById(R.id.btnPremium2);
        btnUploadVideo = findViewById(R.id.btn_upload_video);
        btnCancel = findViewById(R.id.btn_cancel);
        buttonExplore = findViewById(R.id.btn_live);

        videoView = findViewById(R.id.video_view);
        mediaControllerAnchor = findViewById(R.id.controller_anchor);

        btnSave.setOnClickListener(this);
        btnLanjut.setOnClickListener(this);
        btnUserPic.setOnClickListener(this);
        ivUserPic.setOnClickListener(this);
        etUserName.addTextChangedListener(textWatcherUserName);

        btnRecord.setOnClickListener(this);
        btnGallery.setOnClickListener(this);
        btnPremium.setOnClickListener(this);
        btnPremium2.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnUploadVideo.setOnClickListener(this);

        String ccodeViet = PreferenceUtil.getPref(SetupProfileActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");

        downloaVideoDefault();

        assert ccodeViet != null;
        if (!TextUtils.isEmpty(ccodeViet) && Objects.requireNonNull(ccodeViet).equalsIgnoreCase("+84")){
            //downloaVideoDefault();
            initViewViet();
            btnLanjut.setVisibility(View.VISIBLE);
//            Intent data = getIntent();
//            if (data != null){
//                String key = data.getStringExtra("scviet");
//                if (key != null){
//                    BaseActivity.showCaseIntroVietEleven(SetupProfileActivity.this);
//                }
//            }
        }

        initViewNewShowcase();

        own = SessionManager.getProfile(SetupProfileActivity.this);

        if (own != null) {
            mUserPhone.setText(TextUtils.isEmpty(own.msisdn) ? "" : own.msisdn);
            etUserName.setText(TextUtils.isEmpty(own.name) ? "" : own.name);

            mUserPhoneViet.setText(TextUtils.isEmpty(own.msisdn) ? "" : own.msisdn);
            setUserInfo(own);
        }

        mImageSelector = ImageSelector.create(SetupProfileActivity.this, uri -> CropImage.activity(uri).setAspectRatio(1, 1).start(SetupProfileActivity.this));

        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_DEMO_PROFILE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initViewViet() {
        LinearLayout customToolbar = findViewById(R.id.linearViet);
        RelativeLayout relativeLayout = findViewById(R.id.relativeGreyViet);
        LinearLayout linearLayout = findViewById(R.id.linearPhoneViet);
        ImageView imageView = findViewById(R.id.imageUser);
        LinearLayout linearLayout1 = findViewById(R.id.linearSave);
        RelativeLayout relativeLayout1 = findViewById(R.id.relaTrackerViet);
        RelativeLayout relativeLayout2 = findViewById(R.id.relativeViet);
        CardView cardView = findViewById(R.id.cardviewViet);

        buttonExplore.setVisibility(View.GONE);
        customToolbar.setVisibility(View.VISIBLE);
        relativeLayout.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.VISIBLE);
        linearLayout1.setVisibility(View.VISIBLE);
        relativeLayout1.setVisibility(View.VISIBLE);
        relativeLayout2.setVisibility(View.VISIBLE);
        etUserName.setBackground(getResources().getDrawable(R.drawable.selector_rounded_grey_e7e7e7));
        btnPremium2.setVisibility(View.VISIBLE);
        btnPremium.setVisibility(View.GONE);

        ViewGroup.MarginLayoutParams cardViewMarginParams = (ViewGroup.MarginLayoutParams) cardView.getLayoutParams();
        cardViewMarginParams.setMargins(0, 0, 0, 0);
        cardView.requestLayout();
        cardView.setRadius(0);

        //
        LinearLayout linearLayout2 = findViewById(R.id.linear);
        RelativeLayout relativeLayout3 = findViewById(R.id.view);

        final float scale = getResources().getDisplayMetrics().density;
        int pixels_height = (int) (38 * scale + 0.5f);

        linearLayout2.setVisibility(View.GONE);
        relativeLayout3.setVisibility(View.GONE);
        mUserPhone.setVisibility(View.GONE);
        btnRecord.setBackground(getResources().getDrawable(R.drawable.selector_rounded_grey_e4e4e4));
        btnRecord.getLayoutParams().height = pixels_height;
        btnRecord.getLayoutParams().width = WindowManager.LayoutParams.WRAP_CONTENT;
        btnRecord.requestLayout();
        btnGallery.setBackground(getResources().getDrawable(R.drawable.selector_rounded_grey_e4e4e4));
        btnGallery.getLayoutParams().height = pixels_height;
        btnGallery.getLayoutParams().width = WindowManager.LayoutParams.WRAP_CONTENT;
        btnPremium.setBackground(getResources().getDrawable(R.drawable.selector_rounded_grey_e4e4e4));
        btnPremium.getLayoutParams().height = pixels_height;
        btnPremium.getLayoutParams().width = WindowManager.LayoutParams.WRAP_CONTENT;
    }

    private void initViewNewShowcase(){
        String strWordingTitleTool = TextUtils.isEmpty(model.getLang().string.titleSetProfile)
                ?"Set Profilmu" :
                model.getLang().string.titleSetProfile;

        String strWordingSkipButton = TextUtils.isEmpty(model.getLang().string.titleSkip)
                ?"Lewati" :
                model.getLang().string.titleSkip;

        String strWordingMulai = TextUtils.isEmpty(model.getLang().string.titleMulai)
                ?"untuk mulai gunakan MviCall" :
                model.getLang().string.titleMulai;

        String strWordingChooseVideo = TextUtils.isEmpty(model.getLang().string.titleChooseVideo)
                ?"Plih Video Ringtonemu" :
                model.getLang().string.titleChooseVideo;

        String strWordingSeeVideo = TextUtils.isEmpty(model.getLang().string.titleSeeVideo)
                ?"Lihat VideoTonemu" :
                model.getLang().string.titleSeeVideo;

        //NEW SHOWCASE
        TextView titleTool = findViewById(R.id.judul_activity);
        TextView titleSetProfile = findViewById(R.id.textViewSetProfile);
        TextView titleSetMulai = findViewById(R.id.textViewSetMulai);
        TextView titleChooseVideo = findViewById(R.id.textChooseVideo);
        TextView titleSeeVideo = findViewById(R.id.textSeeVideo);
        LinearLayout buttonVideoTone = findViewById(R.id.btn_videotone);

        String ccodeViet = PreferenceUtil.getPref(SetupProfileActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
        assert ccodeViet != null;
        if (!TextUtils.isEmpty(ccodeViet) && Objects.requireNonNull(ccodeViet).equalsIgnoreCase("+84")){
           titleChooseVideo.setVisibility(View.GONE);
           buttonExplore.setVisibility(View.GONE);
           buttonVideoTone.setVisibility(View.GONE);
        }

        titleTool.setText(strWordingTitleTool);
        buttonSkip.setText(strWordingSkipButton);
        titleSetProfile.setText(strWordingTitleTool);
        titleSetMulai.setText(strWordingMulai);
        titleChooseVideo.setText(strWordingChooseVideo);
        titleSeeVideo.setText(strWordingSeeVideo);

        buttonExplore.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), TimeLineNewActivity.class);
            intent.putExtra("Setup", "True");
            startActivity(intent);
            finish();
        });

        buttonVideoTone.setOnClickListener(view -> {
            if (checkValidation()){
                PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SETUP_PROFILE, true).commit();
                Intent intent = new Intent(getApplicationContext(), VideoToneActivity.class);
                startActivity(intent);
                finish();
            }
        });

        buttonSkip.setOnClickListener(view -> {
            try{
                updateProfileName();
                showCaseToFalse();
                //setFinalScase();
                Intent intent = new Intent(SetupProfileActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }catch (Exception e){
                e.printStackTrace();
            }
        });

    }

    private Runnable input_finish_checker = () -> {
        if (System.currentTimeMillis() > (last_text_edit + delay - 500)){
            updateProfileName();
        }
    };

    //============================================ ShowCaseVietName ================================================

    private void showCaseIntroVietOne(){
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_ONE_VIETNAME, true).commit();
            String strWordingTitleIntroDemoViet = TextUtils.isEmpty(model.getLang().string.caseTitleIntro)
                    ?"Hey there!" :
                    model.getLang().string.caseTitleIntro;

            String strWordingDescIntroViet = TextUtils.isEmpty(model.getLang().string.caseSubtitleIntro)
                    ?"I am Phuong, your MViCoach. " + "\n" + "I will show you how to have" +"\n" + "MORE FUN THAN EVER with your friends" :
                    model.getLang().string.caseSubtitleIntro;

            String strWordingNextIntroDemoViet = TextUtils.isEmpty(model.getLang().string.next)
                    ?"Next" :
                    model.getLang().string.next;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(this);
            showCaseWrapper.showFancyVietnameFirst(
                    ShowCaseWrapper.SC_VIET_FIRST,
                    strWordingTitleIntroDemoViet,
                    strWordingDescIntroViet,
                    strWordingNextIntroDemoViet,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            callScTwo();
                        }

                        @Override
                        public void onOk() {
                            callScTwo();
                        }

                        @Override
                        public void onSkip() {
                            callScTwo();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showCaseIntroVietTwo(String title, String des, String sbtn){
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_TWO_VIETNAME, true).commit();
            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(this);
            showCaseWrapper.showFancyVietnameTwo(
                    ShowCaseWrapper.SC_VIET_TWO,
                    title,
                    des,
                    sbtn,
                    new ShowCaseListener.ActionsListenerEditText() {

                        @Override
                        public void onScreenTap(String name, boolean isSuccess) {
                            if (isSuccess){
                                callScThird();
                            }else{
                                Toast.makeText(SetupProfileActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onOk(String name, boolean isSuccess) {
                            if (isSuccess){
                                callScThird();
                            }else{
                                Toast.makeText(SetupProfileActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onSkip(String name, boolean isSuccess) {
                            if (isSuccess){
                                callScThird();
                            }else{
                                Toast.makeText(SetupProfileActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                            }
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showCaseIntroVietThird(String des, String name){
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_THREE_VIETNAME, true).commit();
            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(this);
            showCaseWrapper.showFancyVietnameThird(
                    ShowCaseWrapper.SC_VIET_THIRD,
                    des,
                    name,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            callScFor();
                        }

                        @Override
                        public void onOk() {
                            callScFor();
                        }

                        @Override
                        public void onSkip() {
                            callScFor();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showCaseIntroVietFor(String des){
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_FOR_VIETNAME, true).commit();
            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(this);
            showCaseWrapper.showFancyVietnameFor(
                    ShowCaseWrapper.SC_VIET_FOR,
                    des,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            callScFive();
                        }

                        @Override
                        public void onOk() {
                            callScFive();
                        }

                        @Override
                        public void onSkip() {
                            callScFive();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showCaseIntroVietFive(String des, View focusView){
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_FIV_VIETNAME, true).commit();
            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(this);
            showCaseWrapper.showFancyVietnameFive(
                    ShowCaseWrapper.SC_VIET_FIVE,
                    des,
                    focusView,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            callScRecord();
                        }

                        @Override
                        public void onOk() {
                            callScRecord();
                        }

                        @Override
                        public void onSkip() {
                            callScRecord();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showCaseIntroVietRecord(String desc, View focusView){
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_RECORD_VIETNAME, true).commit();
            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(this);
            showCaseWrapper.showFancyVietnameRecord(
                    ShowCaseWrapper.SC_VIET_RECORD,
                    desc,
                    focusView,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            callScVideo();
                        }

                        @Override
                        public void onOk() {
                            callScVideo();
                        }

                        @Override
                        public void onSkip() {
                            callScVideo();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showCaseIntroVietVideo(String desc, View focusView){
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_VIDEO_VIETNAM, true).commit();
            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(this);
            showCaseWrapper.showFancyVietnameVideo(
                    ShowCaseWrapper.SC_VIET_VIDEO,
                    desc,
                    focusView,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            callScSix();
                        }

                        @Override
                        public void onOk() {
                            callScSix();
                        }

                        @Override
                        public void onSkip() {
                            callScSix();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showCaseIntroVietSix(String des, View focusView){
        try {
            PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SETUP_PROFILE, true).commit();

            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_SIX_VIETNAME, true).commit();
            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(this);
            showCaseWrapper.showFancyVietnameSix(
                    ShowCaseWrapper.SC_VIET_SIX,
                    des,
                    focusView,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            prefFriendsCase();
                            startActivity(new Intent(SetupProfileActivity.this, FreePremiumContent.class));
                            finish();
                        }

                        @Override
                        public void onOk() {
                            prefFriendsCase();
                            startActivity(new Intent(SetupProfileActivity.this, FreePremiumContent.class));
                            finish();
                        }

                        @Override
                        public void onSkip() {
                            prefFriendsCase();
                            startActivity(new Intent(SetupProfileActivity.this, FreePremiumContent.class));
                            finish();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void prefFriendsCase(){
        PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.SHOW_CASE_SETUP, true).commit();
    }

    private void showCaseIntroVietTeen() {
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_TEN_VIETNAME, true).commit();

            String strWordingTitleVietTen = TextUtils.isEmpty(model.getLang().string.caseTitleRecordVideo)
                    ?"Looking great, right? and you can also Record your own Personal Video if you want" :
                    model.getLang().string.caseTitleRecordVideo;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(SetupProfileActivity.this);
            showCaseWrapper.showFancyVietnameTeen(
                    ShowCaseWrapper.SC_VIET_TEEN,
                    strWordingTitleVietTen,
                    btnRecord,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            actionToRecord();
                        }

                        @Override
                        public void onOk() {
                            actionToRecord();
                        }

                        @Override
                        public void onSkip() {
                            actionToRecord();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // Call Action...........
    private void actionToGallery() {
        ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
        if (own != null) {
            String strMsisdn = own.msisdn;
            if (!TextUtils.isEmpty(strMsisdn)) {
                checkSubs(strMsisdn, isSuccess -> {
                    if (!isSuccess) {
                        if (!SetupProfileActivity.this.isFinishing()) {

                            String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                            assert ccode != null;
                            if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")){
                                startActivity(new Intent(SetupProfileActivity.this, PopupVietSubsActivity.class));
                                return;
                            }

                            Intent intent = new Intent(SetupProfileActivity.this, PopUpSubsActivity.class);
                            startActivity(intent);
                        }
                    } else {
                        if (!SetupProfileActivity.this.isFinishing()) {

                            if (isUploading)
                                return;

                            mVideoSelector.gallery();
                            isProcessVideo = true;
                            sendGoogleAnalitycs(TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO, "click gallery video");

                        }
                    }
                });
            }
        }
    }

    private void actionToRecord(){
        ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
        if (own != null) {
            String strMsisdn = own.msisdn;
            if (!TextUtils.isEmpty(strMsisdn)) {
                checkSubs(strMsisdn, isSuccess -> {
                    if (!isSuccess) {
                        if (!SetupProfileActivity.this.isFinishing()) {

                            String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                            assert ccode != null;
                            if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")) {
                                startActivity(new Intent(SetupProfileActivity.this, PopupVietSubsActivity.class));
                                return;
                            }

                            Intent intent = new Intent(SetupProfileActivity.this, PopUpSubsActivity.class);
                            startActivity(intent);
                        }
                    } else {
                        if (!SetupProfileActivity.this.isFinishing()) {

                            if (isUploading)
                                return;

                            mVideoSelector.capture();
                            isProcessVideo = true;
                            sendGoogleAnalitycs(TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO, "click record video");

                        }
                    }
                });
            }
        }

    }

    private void callScTwo(){
        String strWordingTitleVietTwo = TextUtils.isEmpty(model.getLang().string.caseTitleInputName)
                ?"But hey!" :
                model.getLang().string.caseTitleInputName;

        String strWordingDescVietTwo = TextUtils.isEmpty(model.getLang().string.caseSubtitleInputName)
                ?"I don't even know your name yet. How should I call you?" :
                model.getLang().string.caseSubtitleInputName;

        String strWordingNextVietWwo = TextUtils.isEmpty(model.getLang().string.caseNameEditText)
                ?"enter name" :
                model.getLang().string.caseNameEditText;

        showCaseIntroVietTwo(strWordingTitleVietTwo, strWordingDescVietTwo, strWordingNextVietWwo);
    }



    private void callScThird(){

        ContactItem owns = SessionManager.getProfile(getApplicationContext());
        String strWordingTitleVietThree;
        if (owns != null){
            strWordingTitleVietThree = TextUtils.isEmpty(model.getLang().string.caseTitleFinishInput)
                    ?"Delighted to meet you " :
                    model.getLang().string.caseTitleFinishInput;
        }else{
            strWordingTitleVietThree = TextUtils.isEmpty(model.getLang().string.caseTitleFinishInput)
                    ?"Delighted to meet you (name) !" :
                    model.getLang().string.caseTitleFinishInput;
        }
        if (owns != null){
            showCaseIntroVietThird(strWordingTitleVietThree, "(" + owns.getName() + ")" +  " !");
        }

    }

    private void callScFor(){

        String strWordingTitleVietThree = TextUtils.isEmpty(model.getLang().string.caseTitleIntroMvicalll)
                ?"You know, MViCall is really the COOLEST app to SURPRISE YOUR FRIENDS when you call them. And you are in control." :
                model.getLang().string.caseTitleIntroMvicalll;

        showCaseIntroVietFor(strWordingTitleVietThree);

    }

    private void callScFive(){

        String strWordingTitleVietThree = TextUtils.isEmpty(model.getLang().string.caseTitleTypes)
                ?"You can choose 2 types of VIDEO RINGTONE you want to appear on your friend's phone when you call them" :
                model.getLang().string.caseTitleTypes;

        showCaseIntroVietFive(strWordingTitleVietThree, lnSelectedVd);

    }

    private void callScRecord(){

        String text = TextUtils.isEmpty(model.getLang().string.caseTitleRecord)
                ?"You can Record a Personal VIDEO RINGTONE" :
                model.getLang().string.caseTitleRecord;

        String btn = TextUtils.isEmpty(model.getLang().string.caseTitleRecordBtn)
                ?"Record" :
                model.getLang().string.caseTitleRecordBtn;

        String strWordingTitleVietThree = TextUtils.isEmpty(model.getLang().string.caseTitleTypes)
                ?"You can choose 2 types of VIDEO RINGTONE you want to appear on your friend's phone when you call them" :
                model.getLang().string.caseTitleTypes;

        showCaseIntroVietRecord(text, btnRecord);

    }

    private void callScVideo(){

        String text = TextUtils.isEmpty(model.getLang().string.caseTitleVideo)
                ?"You can Choose a Premium VIDEO RINGTONE" :
                model.getLang().string.caseTitleVideo;

        String btn = TextUtils.isEmpty(model.getLang().string.caseTitleVideoBtn)
                ?"Premium Video" :
                model.getLang().string.caseTitleVideoBtn;

        String strWordingTitleVietThree = TextUtils.isEmpty(model.getLang().string.caseTitleTypes)
                ?"You can choose 2 types of VIDEO RINGTONE you want to appear on your friend's phone when you call them" :
                model.getLang().string.caseTitleTypes;

        showCaseIntroVietVideo(text, btnPremium2);

    }

    private void callScSix(){

        String strWordingTitleVietThree = TextUtils.isEmpty(model.getLang().string.caseButtonPremiumOffering)
                ?"I have a very special welcome gift for you. I give you 6 FREE premium video ringtones to choose from! Just for you!" :
                model.getLang().string.caseButtonPremiumOffering;

        showCaseIntroVietSix(strWordingTitleVietThree, btnPremium2);

    }

    //============================================ ShowCaseVietName ================================================


//    private void showCaseIntro() {
//        boolean isShowCaseIntro = PreferenceUtil.getPref(SetupProfileActivity.this).getBoolean(PreferenceUtil.SHOW_CASE_DEMO_PROFILE, false);
//        if (!isShowCaseIntro) {
//            if (!SetupProfileActivity.this.isFinishing()) {
//                if (fancyShowCaseViewFirst == null) {
//                    LangViewModel model = LangViewModel.getInstance();
//
//                    String strWordingDescIntroDemo = TextUtils.isEmpty(model.getLang().string.descIntroDemo)
//                            ?"Untuk mulai gunakan MViCall, kita ubah profilmu dulu yuk!" :
//                            model.getLang().string.descIntroDemo;
//
//                    String strWordingTitleIntroDemo = TextUtils.isEmpty(model.getLang().string.wordingTitleIntroDemo)
//                            ?"Hai!" :
//                            model.getLang().string.wordingTitleIntroDemo;
//
//                    String strWordingNextIntroDemo = TextUtils.isEmpty(model.getLang().string.wordingNextIntroDemo)
//                            ?"Lanjut" :
//                            model.getLang().string.wordingNextIntroDemo;
//
//                    String strWordingSkipIntroDemo = TextUtils.isEmpty(model.getLang().string.skip)
//                            ?"Skip" :
//                            model.getLang().string.skip;
//
//                    fancyShowCaseViewFirst = new FancyShowCaseView.Builder(SetupProfileActivity.this).closeOnTouch(false).customView(R.layout.case_view_confirmation, new OnViewInflateListener() {
//                        @Override
//                        public void onViewInflated(@NonNull View view) {
//
//                            TextView tv = view.findViewById(R.id.textView);
//                            TextView title = view.findViewById(R.id.judul);
//                            Button btn = view.findViewById(R.id.btnOK);
//                            Button skip = view.findViewById(R.id.btnSkip);
//
//                            String mNamet = own.name;
//                            if (TextUtils.isEmpty(mNamet)){
//                                skip.setVisibility(View.GONE);
//                            }else{
//                                skip.setVisibility(View.VISIBLE);
//                            }
//
//                            tv.setText(strWordingDescIntroDemo);
//                            title.setText(strWordingTitleIntroDemo);
//                            btn.setText(strWordingNextIntroDemo);
//                            skip.setText(strWordingSkipIntroDemo);
//
//                            view.setOnClickListener(v -> {
//                                fancyShowCaseViewFirst.removeView();
//                                PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SHOW_CASE_DEMO_PROFILE, true).commit();
//                                showCaseEditProfile();
//                            });
//
//                            view.findViewById(R.id.btnOK).setOnClickListener(view12 -> {
//                                fancyShowCaseViewFirst.removeView();
//                                PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SHOW_CASE_DEMO_PROFILE, true).commit();
//                                showCaseEditProfile();
//                            });
//
//                            view.findViewById(R.id.btnSkip).setOnClickListener(view1 -> {
//                                showCaseToFalse();
//                                setFinalScase();
//                                Intent intent = new Intent(SetupProfileActivity.this, MainActivity.class);
//                                intent.putExtra("SETUP", "true");
//                                startActivity(intent);
//                                finish();
//                            });
//                        }
//                    }).build();
//
//                }
//
//                if (fancyShowCaseViewFirst.isShown()) {
//                    return;
//                }
//                fancyShowCaseViewFirst.show();
//                PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SHOW_CASE_DEMO_PROFILE, true).commit();
//            }
//        }
//    }


//    private void showCaseEditProfile() {
//        if (!SetupProfileActivity.this.isFinishing()) {
//            LangViewModel model = LangViewModel.getInstance();
//
//            String strWordingEditNameDemo = TextUtils.isEmpty(model.getLang().string.wordingEditNameDemo)
//                    ?"Isi Nama Kerenmu dan upload foto profilmu. Klik tombol 'Simpan' untuk menyimpan." :
//                    model.getLang().string.wordingEditNameDemo;
//
//            showCaseWrapper.showFancyDemoEditProfile(ShowCaseWrapper.DEMO_EDIT_PROFILE, strWordingEditNameDemo, cvProfile, new ShowCaseListener.ActionListener()  {
//                @Override
//                public void onScreenTap() {
//                    showCaseEditVideo();
//                }
//
//                @Override
//                public void onOk() {
//                    showCaseEditVideo();
//                }
//
//                @Override
//                public void onSkip() {
//
//                }
//
//            });
//        }
//    }

//    private void showCaseEditVideo(){
//        if (!SetupProfileActivity.this.isFinishing()) {
//            LangViewModel model = LangViewModel.getInstance();
//
//            String strWordingEditVideoDemo = TextUtils.isEmpty(model.getLang().string.wordingEditVideoDemo)?
//                    "Upload Video Ringtonemu dengan Rekam Video atau juga bisa gunakan Video Premium MViCall"
//                    : model.getLang().string.wordingEditVideoDemo;
//
//            showCaseWrapper.showFancyDemoEditVideo(ShowCaseWrapper.DEMO_EDIT_VIDEO, strWordingEditVideoDemo, cvVideo, new ShowCaseListener.ActionListener() {
//                @Override
//                public void onScreenTap() {
//                    downloaVideoDefault();
//                    showCaseButtonPremium();
//                }
//
//                @Override
//                public void onOk() {
//                    downloaVideoDefault();
//                    showCaseButtonPremium();
//                }
//
//                @Override
//                public void onSkip() {
//
//                }
//            });
//        }
//    }

//    private void showCaseButtonPremium(){
//        if (!SetupProfileActivity.this.isFinishing()) {
//            LangViewModel model = LangViewModel.getInstance();
//
//            String strWordingEditVideoDemo = TextUtils.isEmpty(model.getLang().string.wordingButtonPremium)?
//                    "Kamu juga bisa gunakan Video Premium GRATIS sebagai Video Ringtonemu. Pilih Videonya disini!"
//                    : model.getLang().string.wordingButtonPremium;
//
//            showCaseWrapper.showFancyDemoEditVideo(ShowCaseWrapper.DEMO_BUTTON_PREMIUM, strWordingEditVideoDemo, btnPremium, new ShowCaseListener.ActionListener() {
//                @Override
//                public void onScreenTap() {
//                }
//
//                @Override
//                public void onOk() {
//                }
//
//                @Override
//                public void onSkip() {
//                }
//            });
//        }
//    }

    private void downloaVideoDefault(){
        if (PermissionUtil.hashPermission(SetupProfileActivity.this, permissions)) {
            boolean firstSyncVideoProfile = PreferenceUtil.getPref(SetupProfileActivity.this)
                    .getBoolean(PreferenceUtil.FIRST_SYNC_DEFAULT_VIDEO, false);

            if (!firstSyncVideoProfile) {
                if (!SyncContactProfileManager.getInstance(getApplicationContext()).isSynchronizing()) {
                    Intent i = new Intent(SetupProfileActivity.this, DownloadVideoService.class);
                    startService(i);
                }
            }
        }
    }

    //============================================ General Function ================================================

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.DEMO_UPLOAD_VIDEO");
        LocalBroadcastManager.getInstance(SetupProfileActivity.this).registerReceiver(downloadVideo, _if);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(SetupProfileActivity.this).unregisterReceiver(downloadVideo);
        setMediaPlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        setMediaPlayer();
        if (image != null){
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_button));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        String ccodeViet = PreferenceUtil.getPref(SetupProfileActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");

        boolean caseFriends = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.SHOW_CASE_SETUP, false);

        assert ccodeViet != null;
        if (!TextUtils.isEmpty(ccodeViet) && Objects.requireNonNull(ccodeViet).equalsIgnoreCase("+84")) {
            if (caseFriends){
                checkShowCase2();
            }else{
                checkShowCase();
            }
        }

        boolean actionSave = PreferenceUtil.getPref(SetupProfileActivity.this).getBoolean(PreferenceUtil.ACTION_SAVE_PROFILE,false);
        if (actionSave) {
            SharedPreferences sharedPreferences = SetupProfileActivity.this.getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);
            urlBy = sharedPreferences.getString(ConfigPref.URL_BUY, "");
            assert urlBy != null;
            urlBy = urlBy.replaceAll(" ", "%20");
            setCont = sharedPreferences.getBoolean(ConfigPref.SET_CONTENT, false);
            initMediaPlayer();
        }

    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!ValidationInputUtil.hasText(etUserName)) ret = false;
        return ret;
    }

    private BroadcastReceiver downloadVideo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            doneProgress();
        }
    };

    @Override
    public void onClick(View v) {
        if (btnSave == v){
            PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.ACTION_SAVE_PROFILE, true).commit();
            Boolean update = (Boolean) btnSave.getTag();
            if (update != null && update) {
                if (checkValidation()) {
                    etUserName.clearFocus();
                    Util.hideKeyboard(SetupProfileActivity.this);
                    showDialogs();
                    new Handler().postDelayed(() -> {
                        showCustomDialog(model.getLang().string.updatingProfile);
                        updateProfile();
                        sendGoogleAnalitycs(TrackerConstant.EVENT_CAT_SAVE_PERSONAL_PROFILE, "click save profile");
                    }, 200);

                }
            }
        }else if (btnUserPic == v){
            selectImage();
            sendGoogleAnalitycs(TrackerConstant.EVENT_CAT_SAVE_PERSONAL_PROFILE, "click change picture");

        }else if (ivUserPic == v){
            selectImage();
            sendGoogleAnalitycs(TrackerConstant.EVENT_CAT_SAVE_PERSONAL_PROFILE, "click change picture");

        }else if (btnRecord == v){
            ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
            if (own != null) {
                String strMsisdn = own.msisdn;
                if (!TextUtils.isEmpty(strMsisdn)) {
                    checkSubs(strMsisdn, isSuccess -> {
                        if (!isSuccess) {
                            if (!SetupProfileActivity.this.isFinishing()) {

                                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                assert ccode != null;
                                if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                                    startActivity(new Intent(SetupProfileActivity.this, PopupVietSubsActivity.class));
                                    return;
                                }

                                Intent intent = new Intent(SetupProfileActivity.this, PopUpSubsActivity.class);
                                startActivity(intent);
                            }
                        } else {
                            if (!SetupProfileActivity.this.isFinishing()) {

                                if (isUploading)
                                    return;

                                mVideoSelector.capture();
                                isProcessVideo = true;
                                sendGoogleAnalitycs(TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO, "click record video");

                            }else{
                                Toast.makeText(this, "Click", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }


        }else if (btnGallery == v){

            ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
            if (own != null) {
                String strMsisdn = own.msisdn;
                if (!TextUtils.isEmpty(strMsisdn)) {
                    checkSubs(strMsisdn, isSuccess -> {
                        if (!isSuccess) {
                            if (!SetupProfileActivity.this.isFinishing()) {

                                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                assert ccode != null;
                                if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")){
                                    startActivity(new Intent(SetupProfileActivity.this, PopupVietSubsActivity.class));
                                    return;
                                }

                                Intent intent = new Intent(SetupProfileActivity.this, PopUpSubsActivity.class);
                                startActivity(intent);
                            }
                        } else {
                            if (!SetupProfileActivity.this.isFinishing()) {

                                if (isUploading)
                                    return;

                                mVideoSelector.gallery();
                                isProcessVideo = true;
                                sendGoogleAnalitycs(TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO, "click gallery video");

                            }
                        }
                    });
                }
            }

        }else if (btnPremium == v){
            if (isUploading)
                return;
            startActivity(new Intent(SetupProfileActivity.this, FreePremiumContent.class));
            finish();
        }else if (btnPremium2 == v){
            if (isUploading)
                return;
            startActivity(new Intent(SetupProfileActivity.this, FreePremiumContent.class));
            finish();
        }else if (btnUploadVideo == v){

            //Set string
            String title = TextUtils.isEmpty(model.getLang().string.termOfService)
                    ? "Syarat & Ketentuan"
                    : model.getLang().string.termOfService;

            String msg = TextUtils.isEmpty(model.getLang().string.msgBeforeUpload)
                    ? "Pengguna / pengunduh menjamin bahwa video yang di unduh merupakan miliknya, memperoleh izin untuk menggunakan " +
                    "/ mengunduh video ini. Penguna / pengunduh menjamin bahwa video yang diunduh tidak mengandung " +
                    "materi yang bertentangan dengan hukum serta membebaskan pihak Mvicall " +
                    "dari semua tuntutan dalam bentuk apapun di kemudian hari terkait video yang di unduh oleh penguna / pengunduh"
                    : model.getLang().string.msgBeforeUpload;

            String btnPositive = TextUtils.isEmpty(model.getLang().string.wordingAgree) ? "Setuju" : model.getLang().string.wordingAgree;
            String btnNegative = TextUtils.isEmpty(model.getLang().string.cancelWording) ? "Batal" : model.getLang().string.cancelWording;

            String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
            assert ccode != null;
            if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")){
                //processingUpload();
                try {
                    showAgreementDialog(title, msg, btnPositive, btnNegative);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return;
            }

            boolean isShowPopUpTNC = PreferenceUtil.getPref(SetupProfileActivity.this).getBoolean(PreferenceUtil.POPUP_TNC, false);
            if (!isShowPopUpTNC) {

                try {
                    showAlertUpload(title, msg, btnPositive, btnNegative);
                } catch (Exception e) {
                    e.printStackTrace();
                    processingUpload();
                }

            }else {
                processingUpload();
            }

        }else if (btnCancel == v) {

            selectedButtonUpload(false);

        }else if (btnLanjut == v){

            String strName = etUserName.getText().toString();
            if (!TextUtils.isEmpty(strName)) {
                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                assert ccode != null;
                if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")){
                    PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SETUP_PROFILE, true).commit();
                    PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SHOW_CASE_SETUP2, true).commit();
                    PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SHOW_CASE_FRIEND2, true).commit();
                    startActivity(new Intent(this, FriendsDemoActivity.class).putExtra("ALMOST2", "true"));
                    finish();
                }else{
                    PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SETUP_PROFILE, true).commit();
                    startActivity(new Intent(this, FriendsDemoActivity.class));
                    finish();
                }
            }else {
                Toast.makeText(SetupProfileActivity.this, model.getLang().string.isNameEmptyNotification, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void checkShowCase(){

        boolean caseVietnamOne = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_ONE_VIETNAME, false);
        boolean caseVietnamTwo = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_TWO_VIETNAME, false);
        boolean caseVietnamThree = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_THREE_VIETNAME, false);
        boolean caseVietnamFor = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_FOR_VIETNAME, false);
        boolean caseVietnamFive = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_FIV_VIETNAME, false);
        boolean caseVietnamRecord = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_RECORD_VIETNAME, false);
        boolean caseVietnamVideo = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_VIDEO_VIETNAM, false);
        boolean caseVietnamSix = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_SIX_VIETNAME, false);

        if (!caseVietnamOne){
            showCaseIntroVietOne();
        }else if (!caseVietnamTwo){
            callScTwo();
        } else if (!caseVietnamThree){
            callScThird();
        }else if (!caseVietnamFor){
            callScFor();
        }else if (!caseVietnamFive){
            callScFive();
        }else if (!caseVietnamRecord){
            callScRecord();
        }else if (!caseVietnamVideo){
            callScVideo();
        }else if (!caseVietnamSix){
            callScSix();
        }

    }

    private void checkShowCase2(){

        boolean caseVietnamTeen = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_TEN_VIETNAME, false);
        boolean caseVietnamEleven = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_ELEVEN_VIETNAME, false);

        if (!caseVietnamTeen){
            showCaseIntroVietTeen();
        } //BaseActivity.showCaseIntroVietEleven(SetupProfileActivity.this);


    }

    private void processingUpload(){ // Processing Upload...
        ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
        if (own != null) {
            String strMsisdn = own.msisdn;
            if (!TextUtils.isEmpty(strMsisdn)) {
                checkSubs(strMsisdn, isSuccess -> {
                    if (!isSuccess) {
                        if (!SetupProfileActivity.this.isFinishing()) {

                            String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                            assert ccode != null;
                            if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")){
                                startActivity(new Intent(SetupProfileActivity.this, PopupVietSubsActivity.class));
                                return;
                            }

                            Intent intent = new Intent(SetupProfileActivity.this, PopUpSubsActivity.class);
                            startActivity(intent);
                        }
                    } else {
                        if (!SetupProfileActivity.this.isFinishing()) {

                            if (isUploading)
                                return;

                            doUpload();
                            sendGoogleAnalitycs(TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO, "click upload video");

                        }
                    }
                });
            }
        }

    }

    @SuppressLint("WrongConstant")
    public void showAlertUpload(String title, String msg, String btnPositive, String btnNegative) throws Exception{ //Show alert upload...

        if (!SetupProfileActivity.this.isFinishing()) {

            final TextView myView = new TextView(getApplicationContext());
            myView.setText(msg);
            myView.setTextAlignment(2);
            myView.setPadding(45,5,45,5);
            myView.setTextSize(12);

            new androidx.appcompat.app.AlertDialog.Builder(SetupProfileActivity.this)
                    .setTitle(title)
                    .setView(myView)
                    .setPositiveButton(btnPositive, (dialog, which) -> {
                        if (!SetupProfileActivity.this.isFinishing()) {
                            try {
                                PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.POPUP_TNC, true).commit();
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            processingUpload();
                        }
                    }).setNegativeButton(btnNegative, (dialog, which) -> {
                if (!SetupProfileActivity.this.isFinishing()) {
                    try {
                        dialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).show();
        }

    }

    private void sendGoogleAnalitycs(String cat, String action) {
        try {
            ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
            if (own != null) {
                GATRacker.getInstance(getApplication())
                        .sendEventWithScreen(TrackerConstant.SCREEN_DEMO_PROFILE, cat, action,
                                own.caller_id + "/" + action);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_PERMISSION){
            videoFromGallery();
        }else {
            mImageSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_VIDEO_TRIMMER && resultCode == RESULT_OK) {
            PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.ACTION_SAVE_PROFILE, true).commit();
            showDialogs();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!SetupProfileActivity.this.isFinishing()) {
                        if (data.getData() != null){
                            dissmisDialogs();
                            selectedButtonUpload(true);
                            trimmedUri = data.getData();

                            sendUri = data.getData();
                            stringVideo_ = String.valueOf(sendUri);

                            videoView.setVideoURI(trimmedUri);
                            videoView.setZOrderOnTop(false);
                            videoView.start();

                            masterVideoFile = new File(trimmedUri.getPath());
                            filePathVideo = String.valueOf(masterVideoFile);

                            videoView.setOnErrorListener((mediaPlayer, i, i1) -> {
                                onError = true;
                                showAlertErrorPlay();
                                showAlert(false);
                                return true;
                            });

                            if (onError){
                                showAlert(false);
                            }else{
                                showAlert(true);
                            }

                        }
                    }
                }
            }, 1000);
        } else if (requestCode == REQUEST_VIDEO_TRIMMER && resultCode == RESULT_CANCELED){
//            String ccode = PreferenceUtil.getPref(SetupProfileActivity.this).getString(PreferenceUtil.COUNTRY_CODE, "1");
//            if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
//                BaseActivity.showCaseIntroVietEleven(SetupProfileActivity.this);
//                return;
//            }
        } else {
            PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.ACTION_SAVE_PROFILE, true).commit();
            //BaseActivity.showCaseIntroVietEleven(SetupProfileActivity.this);
            if (!isProcessImage) {
                mVideoSelector.onActivityResult(requestCode, resultCode, data);
            }
            isProcessImage = false;
        }

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultsUri = result.getUri();
                proPicPath = new File(Objects.requireNonNull(resultsUri.getPath()));
                Glide.with(SetupProfileActivity.this)
                        .load(proPicPath)
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivUserPic);
                String ccodeViet = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE,"1");
                assert ccodeViet != null;
                if (!TextUtils.isEmpty(ccodeViet) && !ccodeViet.equalsIgnoreCase("+84")){
                    updateProfile();
                }
                checkProfileChanged();
            }
        } else if (resultCode == RESULT_OK && (requestCode == REQUEST_ADD_PROFILE || requestCode == REQUEST_EDIT_PROFILE)) {
            ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
            if (own != null) {
                ContactItem item = data.getParcelableExtra("contact");
                own.profiles = item.profiles;
                SessionManager.saveProfile(this, own);
                saveContactFirebase(own);
            }
        } else {
            if (!isProcessVideo) {
                mImageSelector.onActivityResult(requestCode, resultCode, data);
            }
            isProcessVideo = false;
        }

    }

    private void showAlertErrorPlay(){
        if (!SetupProfileActivity.this.isFinishing()) {

            String sTitle = TextUtils.isEmpty(model.getLang().string.str_cannot_play)
                    ? "Can't play this video."
                    : model.getLang().string.str_cannot_play;

            String sApprove = TextUtils.isEmpty(model.getLang().string.approveCofirm)
                    ? "Ok"
                    : model.getLang().string.approveCofirm;

            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setMessage(sTitle)
                    .setCancelable(false)
                    .setPositiveButton(sApprove, (dialog, which) -> {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).show();
        }
    }

    private void showAlert(boolean isShow){

        if (isShow){
            String strWordingAddingMusic = TextUtils.isEmpty(model.getLang().string.titleAddMusic)
                    ?"Tambahkan music ke video anda?" :
                    model.getLang().string.titleAddMusic;

            String strWordingYes = TextUtils.isEmpty(model.getLang().string.titleYes)
                    ?"Ya" :
                    model.getLang().string.titleYes;

            String strWordingNo = TextUtils.isEmpty(model.getLang().string.titleNo)
                    ?"Tidak" :
                    model.getLang().string.titleNo;

            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(SetupProfileActivity.this);
            builder1.setTitle("MVICALL");
            builder1.setMessage(strWordingAddingMusic);
            builder1.setCancelable(false);
            builder1.setIcon(getResources().getDrawable(R.drawable.logo_rebranding));

            builder1.setPositiveButton(
                    strWordingYes,
                    (dialog, id) -> actionToMusic());

            builder1.setNegativeButton(
                    strWordingNo,
                    (dialog, id) -> dialog.cancel());

            android.app.AlertDialog alert11 = builder1.create();
            alert11.show();
        }

    }


    //============================================ Function Change Profile ================================================

    private void updateProfile() {
        ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
        if (own != null) {
            // multipart
            MultipartBody.Part _file = null;
            if (proPicPath != null) {
                // ini progress listener
                ProgressRequestBody.ProgressListener progressListener = (num, transferred, totalSize) -> {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(() -> {
                        try {
                            showCustomDialog("Updating " + num + "%");
                        } catch (Exception ne) {
                            ne.printStackTrace();
                        }
                    });
                };

                File file = new File(String.valueOf(proPicPath));
                // init request body
                ProgressRequestBody requestFileBody = new ProgressRequestBody(file, "multipart/form-data", progressListener);
                _file = MultipartBody.Part.createFormData("caller_pic", file.getName(), requestFileBody);
            }

            // set request body
            RequestBody _caller_id = RequestBody.create(MediaType.parse("text/plain"), own.caller_id);
            RequestBody _name = null;
            if (!etUserName.getText().toString().isEmpty())
                _name = RequestBody.create(MediaType.parse("text/plain"), etUserName.getText().toString());
            RequestBody _email = null;
            RequestBody _profile = null;

            Call<APIResponse<ContactItem>> call = ServicesFactory.getService().updateProfile(_caller_id, _name, _email, _profile, _file);
            call.enqueue(new Callback<APIResponse<ContactItem>>() {
                @Override
                public void onResponse(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<ContactItem>> call, @org.jetbrains.annotations.NotNull @NotNull Response<APIResponse<ContactItem>> response) {
                    dissmisDialogs();
                    if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                        proPicPath = null;
                        ContactItem updated = response.body().data;
                        updated.profiles = null;// Untuk syncronize firebase

                        ContactItem own = SessionManager.saveProfile(SetupProfileActivity.this, updated);
                        if (own != null) {
                            saveContactFirebase(own);
                            setUserInfo(own);
                        }

                        checkProfileChanged();
                        Toast.makeText(SetupProfileActivity.this, model.getLang().string.updateProfileSuccess, Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(SetupProfileActivity.this, model.getLang().string.updateProfileFailed, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<ContactItem>> call, @org.jetbrains.annotations.NotNull @NotNull Throwable t) {
                    dissmisDialogs();
                    Toast.makeText(SetupProfileActivity.this, model.getLang().string.updateProfileFailed, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void updateProfileName(){
        if (own == null)
            return;

        //Set Request Body
        RequestBody _caller_id = RequestBody.create(MediaType.parse("text/plain"), own.caller_id);
        RequestBody _name = null;
        if (!etUserName.getText().toString().isEmpty())
            _name = RequestBody.create(MediaType.parse("text/plain"), etUserName.getText().toString());
        RequestBody _email = null;
        RequestBody _profile = null;
        MultipartBody.Part _file = null;

        Call<APIResponse<ContactItem>> call = ServicesFactory.getService().updateProfile(_caller_id, _name, _email, _profile, _file);
        call.enqueue(new Callback<APIResponse<ContactItem>>() {
            @Override
            public void onResponse(@org.jetbrains.annotations.NotNull Call<APIResponse<ContactItem>> call, @org.jetbrains.annotations.NotNull Response<APIResponse<ContactItem>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()){
                    ContactItem updated = response.body().data;
                    updated.profiles = null;// Untuk syncronize firebase
                    PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_TWO_VIETNAME, true).commit();
                    own = SessionManager.saveProfile(SetupProfileActivity.this, updated);
                }
            }

            @Override
            public void onFailure(@org.jetbrains.annotations.NotNull Call<APIResponse<ContactItem>> call, @org.jetbrains.annotations.NotNull Throwable t) {
                Toast.makeText(SetupProfileActivity.this, model.getLang().string.updateProfileFailed, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @SuppressLint("CheckResult")
    private void setUserInfo(ContactItem own) {
        if (own != null) {
            if (!SetupProfileActivity.this.isFinishing()) {
                mUserPhone.setText(own.msisdn);
                etUserName.setText(own.getName());
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.userpng);

                Glide.with(SetupProfileActivity.this).load(own.caller_pic).apply(requestOptions).into(ivUserPic);
                saveContactFirebase(own);
            }
        }
    }

    private void selectedButtonUpload(boolean isSelected){
        if (!SetupProfileActivity.this.isFinishing()) {
            if (isSelected) {
                try {
                    btnRecord.setVisibility(View.GONE);
                    btnGallery.setVisibility(View.GONE);
                    btnPremium.setVisibility(View.GONE);
                    btnPremium2.setVisibility(View.GONE);
                    btnUploadVideo.setVisibility(View.VISIBLE);
                    btnCancel.setVisibility(View.VISIBLE);
                    buttonExplore.setVisibility(View.GONE);
                    String ccode = PreferenceUtil.getPref(SetupProfileActivity.this).getString(PreferenceUtil.COUNTRY_CODE, "1");
                    assert ccode != null;
                    if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")){
                        BaseActivity.showCaseIntroVietEleven(SetupProfileActivity.this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                    assert ccode != null;
                    if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")){
                        btnRecord.setVisibility(View.VISIBLE);
                        btnGallery.setVisibility(View.VISIBLE);
                        btnPremium2.setVisibility(View.VISIBLE);
                        //buttonExplore.setVisibility(View.VISIBLE);
                        btnUploadVideo.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.GONE);
                    }else{
                        btnRecord.setVisibility(View.VISIBLE);
                        btnGallery.setVisibility(View.VISIBLE);
                        btnPremium.setVisibility(View.VISIBLE);
                        buttonExplore.setVisibility(View.VISIBLE);
                        btnUploadVideo.setVisibility(View.GONE);
                        btnCancel.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private TextWatcher textWatcherUserName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            handler.removeCallbacks(input_finish_checker);
        }

        @Override
        public void afterTextChanged(Editable editable) {
            //checkProfileChanged();
            boolean isChanged = false;
            if (!etUserName.getText().toString().equals(own.getName())) isChanged = true;
            if (isChanged){
                buttonSkip.setVisibility(View.VISIBLE);
                if (editable.length() > 0) {
                    last_text_edit = System.currentTimeMillis();
                    handler.postDelayed(input_finish_checker, delay);
                    etUserName.setSelection(etUserName.getText().length());
                }else{
                    Toast.makeText(SetupProfileActivity.this, "The Field is Required", Toast.LENGTH_SHORT).show();
                    buttonSkip.setVisibility(View.GONE);
                }
                if (etUserName.getText().toString().trim().matches("")){
                    buttonSkip.setVisibility(View.GONE);
                }
            }else{
                etUserName.setSelection(etUserName.getText().length());
                if (editable.length() > 0){
                    buttonSkip.setVisibility(View.VISIBLE);
                }else{
                    buttonSkip.setVisibility(View.GONE);
                }
                if (etUserName.getText().toString().trim().matches("")){
                    buttonSkip.setVisibility(View.GONE);
                }
            }

        }
    };

    void checkProfileChanged() {
        boolean isChanged = false;
        ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
        if (own != null) {
            if (proPicPath != null) isChanged = true;

            if (!etUserName.getText().toString().equals(own.getName())) isChanged = true;
            if (isChanged) {
                btnSave.setBackground(getResources().getDrawable(R.drawable.selector_rounded_green));
                btnSave.setTag(true);
            } else {
                btnSave.setBackground(getResources().getDrawable(R.drawable.selector_rounded_grey_e4e4e4));
                btnSave.setTag(false);
            }
        }
    }

    private void selectImage() {
        new AlertDialog.Builder(this)
                .setTitle(model.getLang().string.selectImageFrom)
                .setItems(new String[]{model.getLang().string.selectImageFromCamera, model.getLang().string.gallery},
                        (dialogInterface, i) -> {
                            if (i == 0) {// camera
                                mImageSelector.captureImage();
                                isProcessImage = true;
                            }else {// gallery
                                mImageSelector.openGallery();
                                isProcessImage = true;
                            }
                        })
                .show();
    }

    //============================================ Function Change Video ================================================

    private void initMediaPlayer() {
        if (isSelectVideo) {
            return;
        }

        if (!SetupProfileActivity.this.isFinishing()) {

            String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
            assert ccode != null;
            if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")){
                mediaController = new MediaController(SetupProfileActivity.this);
                mediaController.setAnchorView(mediaControllerAnchor);
                mediaController.setVisibility(View.GONE);
                videoView.setMediaController(mediaController);
                videoView.setZOrderOnTop(false);
            }else {
                mediaController = new MediaController(SetupProfileActivity.this);
                mediaController.setAnchorView(mediaControllerAnchor);
                videoView.setMediaController(mediaController);
                videoView.setZOrderOnTop(false);
            }

            ContactItem contactItem = SessionManager.getProfile(SetupProfileActivity.this);
            if(contactItem == null)
                return;

            saveContactFirebase(contactItem);

            if (setCont) {
                if (!SetupProfileActivity.this.isFinishing()) {
                    try {
                        Uri uris = Uri.parse(urlBy);
                        videoView.setVideoURI(uris);
                        videoView.seekTo(100);
                        videoView.start();
                        saveContactFirebase(contactItem);
                        setCont = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {

                if (!SetupProfileActivity.this.isFinishing()) {
                    if (OwnVideoManager.isOwnVideoExist(SetupProfileActivity.this)) {
                        SetupProfileActivity.this.runOnUiThread(() -> {
                            if (!SetupProfileActivity.this.isFinishing()) {
                                try {
                                    if (isShowBs){{
                                        videoView.pause();
                                        return;
                                    }}
                                    Uri ownVideoUri = OwnVideoManager.getOwnVideoUri(SetupProfileActivity.this);
                                    videoView.setVideoURI(ownVideoUri);
                                    videoView.seekTo(100);
                                    videoView.start();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } else if (OwnVideoManager.collectionExist(SetupProfileActivity.this)) {

                        SetupProfileActivity.this.runOnUiThread(() -> {
                            if (!SetupProfileActivity.this.isFinishing()) {
                                try {
                                    if (isShowBs){{
                                        videoView.pause();
                                        return;
                                    }}
                                    Uri uri = OwnVideoManager.getCollectionUri(SetupProfileActivity.this);
                                    videoView.setVideoURI(uri);
                                    videoView.seekTo(100);
                                    videoView.start();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
            videoView.setOnPreparedListener(mediaPlayer -> mediaPlayer.setLooping(false));
            videoView.setOnErrorListener((mediaPlayer, i, i1) -> true);
        }
    }

    private void doUpload() {
        if (videoView.isPlaying()) {
            videoView.stopPlayback();
            videoView.seekTo(100);
        }
        showDialogs();
        compressVideo();

        ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
        if (own != null) {
            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(
                        TrackerConstant.SCREEN_DEMO_PROFILE,
                        TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
                        "click upload",
                        own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void videofromCapture(){
        Dexter.withActivity(SetupProfileActivity.this).withPermissions(cameraPermissions).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(SetupProfileActivity.this.getPackageManager()) != null) {
                        SetupProfileActivity.this.startActivityForResult(takeVideoIntent, REQUEST_CAPTURE);
                    }
                } else if (!report.getDeniedPermissionResponses().isEmpty()) {
                    showSettingsDialog();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).withErrorListener(error -> {
        }).onSameThread().check();
    }

    private void videoFromGallery() {
        Dexter.withActivity(SetupProfileActivity.this).withPermissions(galleryPermissions).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    Intent pickerIntent = new Intent();
                    pickerIntent.setAction(Intent.ACTION_GET_CONTENT);
                    pickerIntent.addCategory(Intent.CATEGORY_OPENABLE);
                    pickerIntent.setTypeAndNormalize("video/*");
                    SetupProfileActivity.this.startActivityForResult(Intent.createChooser(pickerIntent, "Select Video"), REQUEST_GET_VIDEO);
                } else if (!report.getDeniedPermissionResponses().isEmpty()) {
                    showSettingsDialog();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).withErrorListener(error -> {
        }).onSameThread().check();

    }

    protected void showSettingsDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SetupProfileActivity.this);
        builder.setTitle(model.getLang().string.titlePermissionAlert);
        builder.setMessage(model.getLang().string.bodyPermissionAlert);
        builder.setCancelable(false);
        builder.setPositiveButton(model.getLang().string.buttonPermissionAlert, (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.show();
    }

    protected void openSettings() {
        try {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", SetupProfileActivity.this.getPackageName(), null));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            SetupProfileActivity.this.startActivity(intent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void startTrimActivity(@NonNull Uri uri) {
        try {
            Intent intent = new Intent(SetupProfileActivity.this, TrimmingActivity.class);
            intent.putExtra("path", uri);
            startActivityForResult(intent, REQUEST_VIDEO_TRIMMER);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void compressVideo() {
        try{
            if (trimmedUri != null){
                selectCompressVideo(trimmedUri);
            }else{
                if (videoUriForUpload != null){
                    selectCompressVideo(videoUriForUpload);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

//        if (trimmedUri != null){
//            selectCompressVideo(trimmedUri);
//        }

    }

    private void selectCompressVideo(Uri uri){

        String inPath = uri.getPath();
        String outPath = getCacheDir().getPath() + File.separator + "mvicall_compressed_video.mp4";

        if(!TextUtils.isEmpty(inPath) && !TextUtils.isEmpty(outPath)){

            try {
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(inPath);
                h = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
                w = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
            }catch (Exception e){
                e.printStackTrace();
            }

            VideoCompressor.Companion.with(getApplicationContext())
                    .setInputPath(inPath)
                    .setOutputPath(outPath)
                    .setCallBack(SetupProfileActivity.this)
                    .compressVideo(w, h);

        }

    }

    private void uploadVideoFile(final String videoPath) {
        isUploading = true;
        // Progress listener
        ProgressRequestBody.ProgressListener progressListener = (num, transferred, totalSize) -> {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> showCustomDialog(model.getLang().string.uploading + " " + num + "%"));
        };

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part _file = null;
        File file = new File(videoPath);
        // init request body
        ProgressRequestBody requestFileBody = new ProgressRequestBody(file, "multipart/form-data", progressListener);
        _file = MultipartBody.Part.createFormData("video", file.getName(), requestFileBody);
        // set request body
        ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
        if (own != null) {
            String strCallerID = own.caller_id;
            if (!TextUtils.isEmpty(strCallerID)) {
                _caller_id = RequestBody.create(MediaType.parse("text/plain"), strCallerID);
            }
        }

        Call<APIResponse> call = ServicesFactory.getService().uploadVideo(_file, _caller_id);
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse> call, @org.jetbrains.annotations.NotNull @NotNull Response<APIResponse> response) {
                dissmisDialogs();
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    isUploading = false;
                    // move file on upload success
                    try {
                        SharedPreferences sharedPreferences = SetupProfileActivity.this.getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(ConfigPref.SET_CONTENT, false);
                        editor.putString(ConfigPref.URL_BUY, "");
                        editor.apply();

                        File source = new File(videoPath);
                        File dest = new File(Objects.requireNonNull(OwnVideoManager.getOwnVideoUri(SetupProfileActivity.this).getPath()));
                        FileUtil.moveFile(source, dest);
                        ContactItem own = SessionManager.getProfile(SetupProfileActivity.this);
                        if (own != null) {
                            String strMsisdn = own.msisdn;
                            if(!TextUtils.isEmpty(strMsisdn)) {
                                oauthToken(strMsisdn);
                                selectedButtonUpload(false);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(SetupProfileActivity.this, model.getLang().string.uploadVideoFailed, Toast.LENGTH_SHORT).show();
                    isUploading = false;
                    selectedButtonUpload(false);
                }
            }

            @Override
            public void onFailure(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse> call, @org.jetbrains.annotations.NotNull @NotNull Throwable t) {
                try {
                    isUploading = false;
                    dissmisDialogs();
                    selectedButtonUpload(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast.makeText(SetupProfileActivity.this, model.getLang().string.uploadVideoFailed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void oauthToken(String msisdn) {
        Call<APIResponse<ContactItem>> call = ServicesFactory.getService().getProfile(msisdn);
        call.enqueue(new Callback<APIResponse<ContactItem>>() {
            @Override
            public void onResponse(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<ContactItem>> call, @org.jetbrains.annotations.NotNull @NotNull Response<APIResponse<ContactItem>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {

                    String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                    assert ccode != null;
                    if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                        showAlertCuration();
                        mediaController.setVisibility(View.VISIBLE);
                    }else {
                        if (!SyncContactProfileManager.getInstance(getApplicationContext()).isSynchronizing()) {

                            ContactItem item = response.body().data;
                            if (item != null) {
                                item.profiles = null;// Untuk syncronize firebase
                                SessionManager.saveProfile(SetupProfileActivity.this, item);
                                Intent i = new Intent(SetupProfileActivity.this, SyncVideoProfileServices.class);
                                startService(i);
                            }

                        }
                    }


                } else {
                    Toast.makeText(SetupProfileActivity.this, model.getLang().string.authenticationFailed, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<ContactItem>> call, @org.jetbrains.annotations.NotNull @NotNull Throwable t) {
                Toast.makeText(SetupProfileActivity.this, model.getLang().string.authenticationFailed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("WrongConstant")
    public void showAgreementDialog(String title, String msg, String btnPositive, String btnNegative) throws Exception{ //Show alert upload...

        if (!SetupProfileActivity.this.isFinishing()) {

            final TextView myView = new TextView(this);
            myView.setText(msg);
            myView.setTextAlignment(2);
            myView.setPadding(45,5,45,5);
            myView.setTextSize(12);

            PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.POPUP_TNC, true).commit();
            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setTitle(title)
                    .setCancelable(false)
                    .setView(myView)
                    .setPositiveButton(btnPositive, (dialog, which) -> {
                        if (!SetupProfileActivity.this.isFinishing()) {
                            try {
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            processingUpload();
                        }
                    }).setNegativeButton(btnNegative, (dialog, which) -> {
                if (!SetupProfileActivity.this.isFinishing()) {
                    try {
                        dialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).show();
        }

    }

    private void showAlertCuration(){
        if (!SetupProfileActivity.this.isFinishing()) {

            String sCuration = TextUtils.isEmpty(model.getLang().string.wordingCuration)
                    ? "Video dalam proses review tim kami"
                    : model.getLang().string.wordingCuration;

            String sApprove = TextUtils.isEmpty(model.getLang().string.approveCofirm)
                    ? "Ok"
                    : model.getLang().string.approveCofirm;

            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setMessage(sCuration)
                    .setPositiveButton(sApprove, (dialog, which) -> {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).show();
        }
    }

    public void doneProgress() {
        isUploading = false;
        try {
            if (dialogUpload != null) {
                try {
                    dissmisDialogs();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (isScDoing) {
                    PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SHOW_CASE_UPLOAD_VIDEO, true).commit();
                    isScDoing = false;
                }

            } else {
                Intent il = new Intent("com.sbimv.mvicalls.UPLOAD_DONE");
                LocalBroadcastManager.getInstance(SetupProfileActivity.this).sendBroadcast(il);
                if (isScDoing) {
                    PreferenceUtil.getEditor(SetupProfileActivity.this).putBoolean(PreferenceUtil.SHOW_CASE_UPLOAD_VIDEO, true).commit();
                    PreferenceUtil
                            .getEditor(SetupProfileActivity.this)
                            .putBoolean(PreferenceUtil.SHOW_CASE_UPLOAD_VIDEO, true)
                            .commit();
                    isScDoing = false;
                }
            }

            isSelectVideo = false;
            selectedButtonUpload(false);
            initMediaPlayer();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ================================================= Dialog ====================================================

    private void showDialogs() {
        if(!SetupProfileActivity.this.isFinishing()) {
            try {
                dialogUpload = new DialogUpload();
                dialogUpload.show(getSupportFragmentManager(), null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showCustomDialog(String msg) {
        if(!SetupProfileActivity.this.isFinishing()) {
            try {
                dialogUpload.setMessage(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void dissmisDialogs() {
        if(!SetupProfileActivity.this.isFinishing()) {
            try {
                dialogUpload.dismiss();
                dialogUpload = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void compressionFinished(int status, boolean isVideo, String fileOutputPath) {
        if (mVideoCompressor.isDone()){
            String ready = TextUtils.isEmpty(model.getLang().string.str_preview_video_ready_upload)
                    ? "Ready to Upload"
                    : model.getLang().string.str_preview_video_ready_upload;
            showCustomDialog(ready);
            uploadVideoFile(fileOutputPath);
            videoUriForUpload = Uri.parse(fileOutputPath);
            videoView.setVideoURI(Uri.parse(fileOutputPath));
            videoView.setZOrderOnTop(false);
            videoView.start();
        }
    }

    @Override
    public void onFailure(String message) {
        if (!SetupProfileActivity.this.isFinishing()) {
            try {
                dialogUpload.dismiss();
                selectedButtonUpload(false);
                toast(model.getLang().string.uploadVideoFailed); // File not supported
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onProgress(int progress) {
        String preap = TextUtils.isEmpty(model.getLang().string.str_preview_video_preaparing)
                ? "Preparing..."
                : model.getLang().string.str_preview_video_preaparing;
        showCustomDialog(preap);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(SetupProfileActivity.this, model.getLang().string.pressToExit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    //FFmpeg
    private void stopRunningProcess(){
        FFmpeg.getInstance(getApplicationContext()).killRunningProcesses();
    }

    private Boolean isRunning(){
        return FFmpeg.getInstance(this).isFFmpegCommandRunning();
    }

    @Override
    public void onProgress(String progress) {
        Log.e("TAG", "Running " + progress);

    }

    @Override
    public void onSuccess(File file, String success) {

        String strWordingMerge = TextUtils.isEmpty(model.getLang().string.titleSuccessMerge)
                ?"Success Merge" :
                model.getLang().string.titleSuccessMerge;

        Toast.makeText(this, strWordingMerge, Toast.LENGTH_SHORT).show();
        if (success.equals(stringVideo.TYPE_VIDEO)){
            videoUriForUpload = Uri.fromFile(file);
            videoView.setVideoURI(Uri.fromFile(file));
            videoView.setZOrderOnTop(false);
            videoView.start();
        }
    }

    @Override
    public void onFailure(Exception exception) {
        exception.printStackTrace();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
        Log.e("FAILURE", Objects.requireNonNull(exception.getMessage()));
        dismissLoading();
    }

    @Override
    public void onNotAvailable(Exception notAvailable) {
        Toast.makeText(this, notAvailable.getMessage(), Toast.LENGTH_SHORT).show();
        Log.e("NOT AVAILABLE", Objects.requireNonNull(notAvailable.getMessage()));
        dismissLoading();
    }

    @Override
    public void onFinish() {
        dismissLoading();
    }

    public void showLoading(String message)
    {
        progressDialog = new ProgressDialog(SetupProfileActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissLoading(){
        progressDialog.dismiss();
    }

    //*
    // Show popup bacskound
    //*
    private void showBacksoundList(){
        if (!SetupProfileActivity.this.isFinishing()){
            isShowBs = true;
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.popup_backsound, null);
            builder.setView(v);
            builder.setCancelable(true);

            RecyclerView recyclerBacksound = v.findViewById(R.id.recyclerBacksound);
            ImageView ivClose = v.findViewById(R.id.ivClose);

            alert = builder.create();
            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alert.setCanceledOnTouchOutside(false);

            ivClose.setOnClickListener(view -> {
                alert.dismiss();
                isShowBs = false;
                setMediaPlayer();
            });

            alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    alert.dismiss();
                    setMediaPlayer();
                }
            });

            initPlayaer();
            initRecycler(recyclerBacksound);
            loadBacksound(recyclerBacksound);

            if (!alert.isShowing()) {
                alert.show();
            } else {
                alert.dismiss();
            }
        }
    }

    //*
    // Init Recycler
    //*
    private void initRecycler(RecyclerView recyclerView) {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        backsoundAdapter = new BacksoundAdapter(SetupProfileActivity.this, backSoundList, this);
        recyclerView.setAdapter(backsoundAdapter);
    }

    //*
    // Calling bakcsound list from API
    //*
    private void loadBacksound(RecyclerView recyclerView){
        if (own != null){
            String sMsisdn = TextUtils.isEmpty(own.msisdn) ? "" : own.msisdn;
            Call<APIResponse<List<BackSound>>> call = ServicesFactory.getService().getBackSound(sMsisdn);
            call.enqueue(new Callback<APIResponse<List<BackSound>>>() {
                @Override
                public void onResponse(@org.jetbrains.annotations.NotNull Call<APIResponse<List<BackSound>>> call, @org.jetbrains.annotations.NotNull Response<APIResponse<List<BackSound>>> response) {
                    assert response.body() != null;
                    if (response.isSuccessful() && response.body().isSuccessful()){
                        backSoundList = response.body().data;
                        if (backSoundList != null && backSoundList.size() != 0){
                            Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                            initRecycler(recyclerView);
                        }
                    }
                }

                @Override
                public void onFailure(@org.jetbrains.annotations.NotNull Call<APIResponse<List<BackSound>>> call, @org.jetbrains.annotations.NotNull Throwable t) {
                    String s_failure = TextUtils.isEmpty(model.getLang().string.wordingFailureResponse)
                            ? "Failure Fetching Data"
                            : model.getLang().string.wordingFailureResponse;
                    Toast.makeText(SetupProfileActivity.this, s_failure, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    //*
    // Merging Video With Audio
    //*
    private void forMerge(){
        stopRunningProcess();

        String outputhFileName = "merged_mvicall" + ".mp4";

        if (masterAudioFile != null && masterVideoFile != null){
            if (!isRunning()) {
                VideoAudioMerge.Companion.with(getApplicationContext())
                        .setAudioFile(masterAudioFile)
                        .setVideoFile(masterVideoFile)
                        .setOutputPath(utilVideoAudio.getOutputPath() + "video")
                        .setOutputFileName(outputhFileName)
                        .setVideoAudioCallBack(SetupProfileActivity.this)
                        .merge();

                String wait = TextUtils.isEmpty(model.getLang().string.str_preview_video_please_wait)
                        ? "Please Wait..."
                        : model.getLang().string.str_preview_video_please_wait;
                showLoading(wait);
            } else {
                Toast.makeText(SetupProfileActivity.this, "Operation already in progress! Try again in a while.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //*
    // Show popup bacskound
    //*
    private void actionToMusic(){
        try{
            if (filePathVideo != null && stringVideo_ != null){
                if (videoView != null){
                    if (videoView.isPlaying()) {
                        videoView.pause();
                    }
                }
                showBacksoundList();
            }else{
                Toast.makeText(this, "Choose Video", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {

    }

    //*
    // Flag if media player finish playing
    //*
    @Override
    public void onCompletion(MediaPlayer mediaPlayer2) {
        if (mediaPlayer != null && image != null){

            image.setImageResource(R.drawable.ic_play_button);

            duration = 0;
            mediaPlayer.release();
            mediaPlayer = null;

            backsoundAdapter.refreshAdapter();
            backsoundAdapter.notifyDataSetChanged();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }

    //*
    // Backsound click event
    //*
    @Override
    public void onItemPlayMusic(BackSound backSound, ImageView imageView, int previousPosition, int currentPosition) {
        image = imageView;
        if (mediaPlayer != null){
            if (mediaPlayer.isPlaying()){
                duration = mediaPlayer.getCurrentPosition();
                if (previousPosition != currentPosition){
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    duration = 0;
                }else {
                    mediaPlayer.pause();
                    mediaPlayer.reset();
                    return;
                }
            }else{
                if (previousPosition != currentPosition){
                    duration = 0;
                }
            }

            try {
                mediaPlayer.setDataSource(backSound.getSound());
                mediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else{
            initPlayaer();
            if (mediaPlayer.isPlaying()){
                duration = mediaPlayer.getCurrentPosition();
                if (previousPosition != currentPosition){
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    duration = 0;
                }else {
                    mediaPlayer.pause();
                    mediaPlayer.reset();
                    return;
                }
            }else{
                if (previousPosition != currentPosition){
                    duration = 0;
                }
            }

            try {
                mediaPlayer.setDataSource(backSound.getSound());
                mediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    //*
    // Download backsound for merging with video
    //*
    @Override
    public void onItemChoiceMusic(BackSound backSound) {
        new DownloadFileFromURL().execute(backSound.getSound());
    }


    //Download from URL
    @SuppressLint("StaticFieldLeak")
    private class DownloadFileFromURL extends AsyncTask<String, String, String> {
        protected  void onPreExecute(){
            super.onPreExecute();
            try{
                showDialog(progress_bar_type);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                int lenghOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                @SuppressLint("SdCardPath") OutputStream output = new FileOutputStream( utils.getOutputPath() + outputhFileName);
                byte[] data = new byte[1024];
                long total = 0;

                while ((count = input.read(data)) != -1){
                    total += count;
                    publishProgress(""+(int)((total*100)/lenghOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e){
                Log.e("Error : ", e.getMessage());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {
            try{
                if (!SetupProfileActivity.this.isFinishing()) {
                    pDialog.setProgress(Integer.parseInt(progress[0]));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        protected void onPostExecute(String file_url){
            try{
                dismissDialog(progress_bar_type);

                String audioPath = utils.getOutputPath() + outputhFileName;

                if (alert != null){
                    if (filePathVideo != null && stringVideo_ != null){
                        trimmedUri = null;
                        masterAudioFile = new File(audioPath);
                        masterVideoFile = new File(filePathVideo);
                        Log.e("AUDIO PATH", audioPath);
                        Log.e("VIDEO PATH", filePathVideo);
                        forMerge();

                        if (alert.isShowing()){
                            alert.dismiss();
                        }

                        selectedButtonUpload(true);

                        videoView.setVideoURI(Uri.parse(stringVideo_));
                        videoView.setZOrderOnTop(true);
                        videoView.start();

                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    //*
    // Initial Media Player
    //*
    private void initPlayaer(){
        try{
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnBufferingUpdateListener(this);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnPreparedListener(mediaPlayer -> tooglePlayPause());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //*
    // Pause Start Media Player
    //*
    private void tooglePlayPause(){
        try{
            if(mediaPlayer != null){
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                }else{
                    mediaPlayer.seekTo(duration);
                    mediaPlayer.start();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //*
    // Release Media Player to be null
    //*
    private void setMediaPlayer(){
        try{
            if (mediaPlayer != null){
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                }
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //*
    // Dialog Download Progress
    //*
    protected Dialog onCreateDialog(int id){

        String titleDownload = TextUtils.isEmpty(model.getLang().string.titleDownload)
                ? "Downloading file, Please Wait"
                : model.getLang().string.titleDownload;

        if (id == progress_bar_type) {
            try {
                pDialog = new ProgressDialog(this);
                pDialog.setMessage(titleDownload);
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return null;
    }

}