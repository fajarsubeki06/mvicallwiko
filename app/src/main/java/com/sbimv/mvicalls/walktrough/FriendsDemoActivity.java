package com.sbimv.mvicalls.walktrough;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.VideoToneActivity;
import com.sbimv.mvicalls.apprtc.OutgoingScreenActivity;
import com.sbimv.mvicalls.apprtc.SinchService;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseListener;
import com.sbimv.mvicalls.util.ShowCaseWrapper;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.sbimv.mvicalls.walktrough.FriendsFrg.mImgSync;
import static com.sbimv.mvicalls.walktrough.FriendsFrg.mListView;
import static com.sbimv.mvicalls.walktrough.FriendsFrg.relativeShare;
import static com.sbimv.mvicalls.walktrough.FriendsFrg.rlNext;

public class FriendsDemoActivity extends BaseActivity implements FriendsFrg.OnCallFriendsListner, SinchService.StartFailedListener {

    private static boolean isfinishorDenied = false;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_demo);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fm_friend, new FriendsFrg());
        ft.commit();
    }

    private void showCaseIntroVietTwelpe() {
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_TWELVE_VIETNAME, true).commit();

            LangViewModel model = LangViewModel.getInstance();

            ContactItem own = SessionManager.getProfile(getApplicationContext());
            String strWordingTitleVietThree, strWordingTitleVietThree2;
            if (own != null){
                strWordingTitleVietThree = TextUtils.isEmpty(model.getLang().string.caseTitleAlmostSet)
                        ?"Amazing Job " + "(" + own.getName()  + ")" :
                        model.getLang().string.caseTitleAlmostSet;
                strWordingTitleVietThree2 = TextUtils.isEmpty(model.getLang().string.caseTitleAlmostSet2)
                        ?"! You are almost all set. " :
                        model.getLang().string.caseTitleAlmostSet2;
            }else{
                strWordingTitleVietThree = TextUtils.isEmpty(model.getLang().string.caseTitleAlmostSet)
                        ?"Amazing Job (name)! " :
                        model.getLang().string.caseTitleAlmostSet;
                strWordingTitleVietThree2 = TextUtils.isEmpty(model.getLang().string.caseTitleAlmostSet2)
                        ?"You are almost all set. " :
                        model.getLang().string.caseTitleAlmostSet2;
            }

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(FriendsDemoActivity.this);
            showCaseWrapper.showFancyVietnameTwelpe(
                    ShowCaseWrapper.SC_VIET_TWELVE,
                    strWordingTitleVietThree,
                    strWordingTitleVietThree2,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            showCaseIntroVietThirdTeen();
                        }

                        @Override
                        public void onOk() {
                            showCaseIntroVietThirdTeen();
                        }

                        @Override
                        public void onSkip() {
                            showCaseIntroVietThirdTeen();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showCaseIntroVietThirdTeen() {
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_THIRDTEEN_VIETNAME, true).commit();

            LangViewModel model = LangViewModel.getInstance();

            String strWordingTitleVietThirdTeen = TextUtils.isEmpty(model.getLang().string.caseTitleAutomatically)
                    ?"For your video to change automatically the ringtone of your friends, they must have MViCall on their phone too." :
                    model.getLang().string.caseTitleAutomatically;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(FriendsDemoActivity.this);
            showCaseWrapper.showFancyVietnameThirdTeen(
                    ShowCaseWrapper.SC_VIET_THIRDTEEN,
                    strWordingTitleVietThirdTeen,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            showCaseIntroVietFourTeen();
                        }

                        @Override
                        public void onOk() {
                            showCaseIntroVietFourTeen();
                        }

                        @Override
                        public void onSkip() {
                            showCaseIntroVietFourTeen();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showCaseIntroVietFourTeen() {
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_FOURTEEN_VIETNAME, true).commit();
            LangViewModel model = LangViewModel.getInstance();

            String strWordingTitleVietForTeen = TextUtils.isEmpty(model.getLang().string.caseTitleSynchronize)
                    ?"To check who already installed it, just click HERE to snychronise your MViCall friends list." :
                    model.getLang().string.caseTitleSynchronize;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(FriendsDemoActivity.this);
            showCaseWrapper.showFancyVietnameFourTeen(
                    ShowCaseWrapper.SC_VIET_FOURTEEN,
                    strWordingTitleVietForTeen,
                    mImgSync,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            //strtactivity();
                        }

                        @Override
                        public void onOk() {
                            //strtactivity();
                        }

                        @Override
                        public void onSkip() {
                            //strtactivity();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void strtactivity(){
        PreferenceUtil.getEditor(Objects.requireNonNull(getApplicationContext())).putBoolean(PreferenceUtil.SETUP_CONTACT, true).commit();
        startActivity(new Intent(FriendsDemoActivity.this, ShareDemoActivity.class).putExtra("ShareDemo", "true"));
    }

    @Override
    public void onDestroy() {
        if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().stopClient();
        }
        super.onDestroy();
    }

    @Override
    public void onDataSelected(Context ctx, String msisdn, String caller_id) {
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                    callWithDataClicked(ctx, msisdn, caller_id);
                } else {
//                    showInfoDialog();
                    callConventional(ctx, msisdn);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            callWithDataClicked(ctx, msisdn, caller_id);
        }
    }

    public void callWithDataClicked(Context ctx, String msisdn, String caller_id) {
        Map<String, String> headers = new HashMap<String, String>();
        if(getSinchServiceInterface() != null) {
            try {
                Call call = getSinchServiceInterface().callUser(caller_id, headers);
                if (call != null) {
                    String callId = call.getCallId();
                    if (!TextUtils.isEmpty(callId)) {
                        Intent callScreen = new Intent(this, OutgoingScreenActivity.class);
                        callScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        callScreen.putExtra(SinchService.CALL_ID, callId);
                        callScreen.putExtra("id", caller_id);
                        startActivity(callScreen);
                    }
                }
            }catch (Exception e){
                callConventional(ctx, msisdn);
            }
        }else {
            callConventional(ctx, msisdn);
        }
    }

    private void callConventional(Context ctx, String msisdn){
        Intent phoneIntent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", msisdn, null));
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        ctx.startActivity(phoneIntent);
    }

    @Override
    protected void onServiceConnected() {

    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {

    }

    private void checkShowCase(){

        boolean caseVietnamTwelpe = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_TWELVE_VIETNAME, false);
        boolean caseVietnamThirdTeen = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_THIRDTEEN_VIETNAME, false);
        boolean caseVietnamFourTeen = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_FOURTEEN_VIETNAME, false);

        if (!caseVietnamTwelpe){
            showCaseIntroVietTwelpe();
        }else if (!caseVietnamThirdTeen){
            showCaseIntroVietThirdTeen();
        }else if (!caseVietnamFourTeen){
            showCaseIntroVietFourTeen();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        String ccodeViet = PreferenceUtil.getPref(FriendsDemoActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
        boolean caseFriends = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.SHOW_CASE_FRIEND, false);
        boolean caseFriends2 = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.SHOW_CASE_FRIEND2, false);
        boolean caseSetup2 = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.SHOW_CASE_SETUP2, false);

        if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")) {

            Intent intent = getIntent();
            String getData = intent.getStringExtra("ALMOST2");
            if (!TextUtils.isEmpty(getData)) {
                checkShowCase();
            }else{
                if (caseSetup2){
                    if (caseFriends){
                        checkShowCase();
                    }else{
                        if (caseFriends2){
                            checkShowCase();
                        }
                    }
                }else{
                    startActivity(new Intent(getApplicationContext(), SetupProfileActivity.class).putExtra("sc_viet","record"));
                    finish();
                }

            }
        }
    }

    @Override
    public void onBackPressed() {
//        String ccodeViet = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE,"1");
//        if (!TextUtils.isEmpty(ccodeViet) && !ccodeViet.equalsIgnoreCase("+84")){
//            PreferenceUtil.getEditor(Objects.requireNonNull(getApplicationContext())).putBoolean(PreferenceUtil.SETUP_CONTACT, true).commit();
//            startActivity(new Intent(getApplicationContext(), ShareDemoActivity.class));
//            finish();
//        }
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(FriendsDemoActivity.this, model.getLang().string.pressToExit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
