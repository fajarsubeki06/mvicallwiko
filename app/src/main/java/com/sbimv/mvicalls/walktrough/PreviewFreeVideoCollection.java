package com.sbimv.mvicalls.walktrough;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.activity.UploadVideoToneActivity;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;
import com.sbimv.mvicalls.services.DownloadVideoService;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.SetVideoTone;
import com.sbimv.mvicalls.sync.SyncContactProfileManager;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.OwnVideoManager;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.view.DialogUpload;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PreviewFreeVideoCollection extends AppCompatActivity {

    private VideoView vvPreviewCol;
    private DialogUpload dialogUpload;
    private DataDinamicVideos collection;
    private String decodedValue2;
    private ProgressBar progressBar;
    private ContactItem own;
    LangViewModel model;
    private String rewardID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        model = LangViewModel.getInstance();
        this.setFinishOnTouchOutside(false);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_preview_free_video_collection);

        TextView tvArtist = findViewById(R.id.tvArtistPreview);
        TextView tvTitle = findViewById(R.id.tvTitlePreview);
        ImageView imgClose = findViewById(R.id.imgClosePreviews);
        Button btnSetVideo = findViewById(R.id.btnSetAsVideoTone);
        vvPreviewCol = findViewById(R.id.vv_preview_collection);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        try {
            collection = getIntent().getParcelableExtra("dataFreeContent");
            rewardID = getIntent().getStringExtra("rewardID");
        }catch (Exception e){
            return;
        }

        tvTitle.setText(collection.judul);
        tvArtist.setText(collection.alias);
        vvPreviewCol.setZOrderOnTop(true);
        progressBar = findViewById(R.id.pgPreview);

        btnSetVideo.setText(model.getLang().string.setVideoTone);
        if (!TextUtils.isEmpty(rewardID)){
            btnSetVideo.setVisibility(View.INVISIBLE);
        }

        btnSetVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAsVideoTone();
            }
        });

        initPreviewVideo();

    }

    private void setAsVideoTone() {
        showDialog();
        own = SessionManager.getProfile(PreviewFreeVideoCollection.this);
        if (own != null){
            String callerId = own.caller_id;
            if (!TextUtils.isEmpty(callerId)){
                if (collection != null){
                    String contentId = collection.content_id;
                    if (!TextUtils.isEmpty(contentId)){
                        Call<APIResponse<List<SetVideoTone>>> call = ServicesFactory.getService().getVideoTone(callerId, contentId);
                        call.enqueue(new Callback<APIResponse<List<SetVideoTone>>>() {
                            @Override
                            public void onResponse(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<List<SetVideoTone>>> call, @org.jetbrains.annotations.NotNull @NotNull final Response<APIResponse<List<SetVideoTone>>> response) {
                                if (!PreviewFreeVideoCollection.this.isFinishing()) {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                                                if (!PreviewFreeVideoCollection.this.isFinishing()) {
                                                    List<SetVideoTone> setVideoTone = response.body().data;
                                                    String urls = TextUtils.isEmpty(setVideoTone.get(0).source_content)?"":setVideoTone.get(0).source_content;

                                                    File url = new File(urls);

                                                    try {
                                                        decodedValue2 = URLDecoder.decode(urls, "UTF-8");
                                                    } catch (UnsupportedEncodingException e) {
                                                        e.printStackTrace();
                                                    }
                                                    File dest = new File(OwnVideoManager.getOwnVideoUri(PreviewFreeVideoCollection.this).getPath());
                                                    try {
                                                        if (!SyncContactProfileManager.getInstance(PreviewFreeVideoCollection.this).isSynchronizing()) {
                                                            try {
                                                                PreferenceUtil.getEditor(PreviewFreeVideoCollection.this).putBoolean(PreferenceUtil.ACTION_SAVE_PROFILE, true).commit();
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                            }
                                                            Intent i = new Intent(PreviewFreeVideoCollection.this, DownloadVideoService.class);
                                                            startService(i);
                                                        }

                                                        // save to session and update local variable
                                                        ContactItem ci = new ContactItem();
                                                        ci.profiles = null;// Untuk syncronize firebase
                                                        ci.video_caller = decodedValue2;
                                                        own = SessionManager.saveProfile(PreviewFreeVideoCollection.this, ci);

                                                        SharedPreferences sharedPreferences = PreviewFreeVideoCollection.this.getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                                        editor.putBoolean(ConfigPref.SET_CONTENT, true);
                                                        editor.putString(ConfigPref.URL_BUY, decodedValue2);
                                                        editor.commit();

                                                        FileUtil.moveFile(url, dest);

                                                        // google analytics...
                                                        GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_DEMO_FREE_CONTENT, TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO, "set as personal video", own.caller_id + "/" + collection.content_id);

                                                    } catch (IOException e) {
                                                        e.printStackTrace();
                                                    }
                                                } else {

                                                    try {
                                                        String strWordingVideoFailure = TextUtils.isEmpty(model.getLang().string.wordingVideoFailure) ? "Video sedang mengalami gangguan, silahkan menggunakan video lain" : model.getLang().string.wordingVideoFailure;
                                                        Toast.makeText(PreviewFreeVideoCollection.this, strWordingVideoFailure, Toast.LENGTH_SHORT).show();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }

                                                    dissmisDialog();
                                                    finish();
                                                }
                                            }else {
                                                dissmisDialog();
                                                finish();
                                            }
                                        }
                                    }, 1000);
                                }
                            }

                            @Override
                            public void onFailure(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<List<SetVideoTone>>> call, @org.jetbrains.annotations.NotNull @NotNull Throwable t) {
                                Toast.makeText(PreviewFreeVideoCollection.this, model.getLang().string.wordingFailureResponse, Toast.LENGTH_SHORT).show();
                                dissmisDialog();
                                finish();
                            }
                        });

                    }
                }
            }
        }

    }

    private void initPreviewVideo() {
        MediaController mediaController = new MediaController(PreviewFreeVideoCollection.this);
        mediaController.setAnchorView(mediaController);
        String url_video = collection.source_content;
        url_video = url_video.replaceAll(" ", "%20");
        Uri uri = Uri.parse(url_video);
        vvPreviewCol.setVideoURI(uri);

        vvPreviewCol.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
                vvPreviewCol.start();
            }
        });

        vvPreviewCol.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progressBar.setVisibility(View.GONE);
                showAlertErrorPlay();
                return true;
            }
        });

    }

    private void showAlertErrorPlay(){
        if (!PreviewFreeVideoCollection.this.isFinishing()) {

            String sTitle = TextUtils.isEmpty(model.getLang().string.str_cannot_play)
                    ? "Can't play this video."
                    : model.getLang().string.str_cannot_play;

            String sApprove = TextUtils.isEmpty(model.getLang().string.approveCofirm)
                    ? "Ok"
                    : model.getLang().string.approveCofirm;

            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setMessage(sTitle)
                    .setCancelable(false)
                    .setPositiveButton(sApprove, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            finish();
                        }
                    }).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.DEMO_UPLOAD_VIDEO");
        LocalBroadcastManager.getInstance(PreviewFreeVideoCollection.this).registerReceiver(downloadVideo, _if);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(PreviewFreeVideoCollection.this).unregisterReceiver(downloadVideo);
    }

    private BroadcastReceiver downloadVideo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            doneProgress();
        }
    };

    public void doneProgress(){
        if (dialogUpload != null){
            dissmisDialog();
//            Intent intent = new Intent(PreviewFreeVideoCollection.this, SetupProfileActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);
            String strSuccess = TextUtils.isEmpty(model.getLang().string.successUseVideo)?"The video was successfully installed":model.getLang().string.successUseVideo;
            Toast.makeText(this, strSuccess, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void showDialog(){
        if (!PreviewFreeVideoCollection.this.isFinishing()){
            try{
                dialogUpload = new DialogUpload();
                dialogUpload.show((PreviewFreeVideoCollection.this).getSupportFragmentManager(), null);
                dialogUpload.setMessage(model.getLang().string.updatingVideoTone);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void dissmisDialog(){
        if (!PreviewFreeVideoCollection.this.isFinishing()){
            try{
                dialogUpload.dismissAllowingStateLoss();
                dialogUpload = null;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
