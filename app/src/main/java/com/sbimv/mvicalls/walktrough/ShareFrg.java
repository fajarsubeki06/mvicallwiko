package com.sbimv.mvicalls.walktrough;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Telephony;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.api.Distribution;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TimeLineNewActivity;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.activity.MainActivity;
import com.sbimv.mvicalls.adapter.CustomGridViewAdapter;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataBannerPromo;
import com.sbimv.mvicalls.pojo.DataDynamicLink;
import com.sbimv.mvicalls.pojo.ModelCustomGrid;
import com.sbimv.mvicalls.services.SyncConfigService;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseListener;
import com.sbimv.mvicalls.util.ShowCaseWrapper;
import com.sbimv.mvicalls.view.DialogUpload;



import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.OnViewInflateListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ShareFrg extends Fragment implements View.OnClickListener {

    ModelCustomGrid[] sosmed = {
            new ModelCustomGrid("SMS", "sms", R.drawable.ics_sms, "share_sms"),
            new ModelCustomGrid("Email", "mailto", R.drawable.ics_email, "share_email"),
            new ModelCustomGrid("WhatsApp", "com.whatsapp", R.drawable.ics_whatsapp, "share_wa"),
            new ModelCustomGrid("Facebook", "com.facebook.katana", R.drawable.ics_facebook, "share_fb"),
            new ModelCustomGrid("Instagram", "com.instagram.android", R.drawable.ics_instagram, "share_ig"),
            new ModelCustomGrid("Line", "jp.naver.line.android", R.drawable.ics_line, "share_line"),
            new ModelCustomGrid("Viber", "com.viber.voip", R.drawable.ics_viber, "share_vb"),
            new ModelCustomGrid("WeChat", "com.tencent.mm", R.drawable.ics_wechat, "share_wc"),
            new ModelCustomGrid("Zalo", "com.zing.zalo", R.drawable.zalo, "share_zl"),
            new ModelCustomGrid("Telegram", "org.telegram.messenger", R.drawable.ics_telegram, "share_tl")};

    ArrayList<ModelCustomGrid> modelCustomGrids = new ArrayList<>();
    private LangViewModel model;
    private GridView androidGridView;
    private ContactItem own;
    private String wordingDynamic;

    private BroadcastReceiver showConfigUpdate = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkWording();
        }
    };

    private DialogUpload dialogUpload;
    private Button btnFinish;
    private DataBannerPromo datas;
    private ImageView imgBannerDemo;
    private FancyShowCaseView fancyShowCaseViewFirst;


    public ShareFrg() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.share_fragment, container, false);
        model = LangViewModel.getInstance();

        androidGridView = view.findViewById(R.id.grid_share);
        imgBannerDemo = view.findViewById(R.id.imgBannerDemo);
        TextView txtTitleDemoShare = view.findViewById(R.id.txtTitleDemoShare);
        RelativeLayout relativeNext = view.findViewById(R.id.rl_next);
        LinearLayout lnSosmed = view.findViewById(R.id.lnsosmed);
        btnFinish = view.findViewById(R.id.btnFinish);
        LinearLayout linearLayout = view.findViewById(R.id.linear);
        LinearLayout linearLayoutViet = view.findViewById(R.id.linearViet);
        RelativeLayout relativeLayout = view.findViewById(R.id.rl_indicator);

        String strTitleShare = TextUtils.isEmpty(model.getLang().string.titleDemoShare) ? "Ajak Teman dan Menangkan Hadiahnya !" : model.getLang().string.titleDemoShare;
        String strBtnFinish = TextUtils.isEmpty(model.getLang().string.buttonNext) ? "Selesai" : model.getLang().string.buttonNext;

        txtTitleDemoShare.setText(strTitleShare);
        btnFinish.setText(strBtnFinish);

        btnFinish.setOnClickListener(this);
        model = LangViewModel.getInstance();
        own = SessionManager.getProfile(getActivity());

        initView(view);

        String ccodeViet = PreferenceUtil.getPref(getActivity()).getString(PreferenceUtil.COUNTRY_CODE,"1");

        if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
            //For Vietname
            linearLayoutViet.setVisibility (View.VISIBLE);
            relativeLayout.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.GONE);
        }else{
            //showCaseIntro();
        }

        for (ModelCustomGrid aSosmed : sosmed) {
            String name = aSosmed.name;
            String namePackage = aSosmed.package_name;
            int imgCount = aSosmed.icon;
            String source = aSosmed.source;

            if (namePackage.equalsIgnoreCase("sms")) {
                modelCustomGrids.add(new ModelCustomGrid(name, namePackage, imgCount, source));
            } else if (namePackage.equalsIgnoreCase("mailto")) {
                modelCustomGrids.add(new ModelCustomGrid(name, namePackage, imgCount, source));
            } else {
                validateIntent(name, namePackage, imgCount, source);
            }
        }

        setGridView();
        //showCaseIntro();

        try {
            GATRacker.getInstance(Objects.requireNonNull(getActivity()).getApplication()).sendScreen(TrackerConstant.SCREEN_DEMO_SHARE);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void initView(View view){
        String strWordingShare= TextUtils.isEmpty(model.getLang().string.titleShareMV)
                ?"Share MViCall" :
                model.getLang().string.titleShareMV;

        TextView titleTool = view.findViewById(R.id.judul_activity);
        Button buttonSkip = view.findViewById(R.id.btn_lewat);
        titleTool.setText(strWordingShare);

        buttonSkip.setVisibility(View.GONE);
    }

    private void showCaseIntro() {
        if (getActivity()!= null) {
            boolean isShowCaseIntro = PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceUtil.SHOW_CASE_DEMO_SHARE, false);
            if (!isShowCaseIntro) {
                if (fancyShowCaseViewFirst == null) {

                    try {
                        LangViewModel model = LangViewModel.getInstance();

                        String strWordingShareIntroDemo = TextUtils.isEmpty(model.getLang().string.wordingShareIntroDemo)
                                ?"Ajak temanmu untuk pakai MViCall dan menangkan REWARD SEPSIAL dari MViCall" :
                                model.getLang().string.wordingShareIntroDemo;

                        String strWordingButtonIntroDemo = TextUtils.isEmpty(model.getLang().string.wordingButtonIntroDemo)
                                ?"Share MViCall" :
                                model.getLang().string.wordingButtonIntroDemo;

                        fancyShowCaseViewFirst = new FancyShowCaseView.Builder(getActivity()).closeOnTouch(false).customView(R.layout.case_view_confirmation, new OnViewInflateListener() {
                            @Override
                            public void onViewInflated(@NonNull View view) {

                                TextView tv = view.findViewById(R.id.textView);
                                TextView title = view.findViewById(R.id.judul);
                                title.setVisibility(View.GONE);
                                Button btn = view.findViewById(R.id.btnOK);

                                tv.setText(strWordingShareIntroDemo);
                                btn.setText(strWordingButtonIntroDemo);

                                view.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        try {
                                            fancyShowCaseViewFirst.removeView();
                                            PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SHOW_CASE_DEMO_SHARE, true).commit();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }).build();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                if (fancyShowCaseViewFirst.isShown()) {
                    return;
                }
                try {
                    fancyShowCaseViewFirst.show();
                }catch (Exception e){
                    e.printStackTrace();
                }
                PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.SHOW_CASE_DEMO_SHARE, true).commit();
            }
        }
    }

    private void setGridView() {
        CustomGridViewAdapter adapterViewAndroid = new CustomGridViewAdapter(getActivity(), modelCustomGrids);
        androidGridView.setAdapter(adapterViewAndroid);

        androidGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    final String name = modelCustomGrids.get(position).name;
                    final String packageName = modelCustomGrids.get(position).package_name;
                    String source = modelCustomGrids.get(position).source;
                    PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SHARE_ACTION, true).commit();
                    getDynamicLink(source, own.caller_id, name, packageName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        getBannerPromo();

    }

    private void getBannerPromo() {
        Call<APIResponse<DataBannerPromo>> call = ServicesFactory.getService().getBanner();
        call.enqueue(new Callback<APIResponse<DataBannerPromo>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<DataBannerPromo>> call, @NotNull Response<APIResponse<DataBannerPromo>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    if (isSafe()) {
                        datas = response.body().data;
                        if (datas != null) {
                            if (isAdded()) {
                                if (isSafe()) {
                                    try {
                                        String url = datas.getBanner();
                                        if (!TextUtils.isEmpty(url)) {
                                            Glide.with(Objects.requireNonNull(getActivity())).asBitmap().load(url).into(new SimpleTarget<Bitmap>() {
                                                @Override
                                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                                    final int height = resource.getHeight();
                                                    final int width = resource.getWidth();
                                                    imgBannerDemo.post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            int imgW = imgBannerDemo.getMeasuredWidth();
                                                            int imgH = imgW * height / width;
                                                            imgBannerDemo.getLayoutParams().height = imgH;
                                                            imgBannerDemo.requestLayout();
                                                            imgBannerDemo.setImageBitmap(resource);
                                                        }
                                                    });
                                                }
                                            });
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<DataBannerPromo>> call, @NotNull Throwable t) {
            }
        });
    }

    private void doIntent(String packageName, String link, String name) {
        if (packageName.equals("mailto")) {
            sendShareEmailAction(link);
            sendGoogleAnalitycs(name);

        } else if (packageName.equals("sms")) {
            if (isSafe()) {
                sendActionShare(Telephony.Sms.getDefaultSmsPackage(getActivity()), link);
                sendGoogleAnalitycs(name);
            }
        } else {
            sendActionShare(packageName, link);
            sendGoogleAnalitycs(name);
        }
    }

    private void getDynamicLink(String source, String caller_id, final String name, final String packageName) {
        showCustomDialog(model.getLang().string.progressWording);
        Call<APIResponse<DataDynamicLink>> call = ServicesFactory.getService().getDynamicShare(source, caller_id);
        call.enqueue(new Callback<APIResponse<DataDynamicLink>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<DataDynamicLink>> call, @NotNull Response<APIResponse<DataDynamicLink>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    DataDynamicLink data = response.body().data;
                    if (data != null) {
                        String link = data.getShortLink();
                        AppPreference.setPreference(getActivity(), name, link);
                        doIntent(packageName, link, name);
                    }
                }
                dismissDialogs();
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<DataDynamicLink>> call, @NotNull Throwable t) {
                dismissDialogs();
            }
        });
    }

    private void validateIntent(String name, String namePackage, int imgCount, String source) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        List<ResolveInfo> resInfo = Objects.requireNonNull(getActivity()).getPackageManager().queryIntentActivities(share, 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo info : resInfo) {
                if (namePackage.contains(info.activityInfo.packageName.toLowerCase())) {
                    modelCustomGrids.add(new ModelCustomGrid(name, namePackage, imgCount, source));
                    break;
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Intent i = new Intent(getActivity(), SyncConfigService.class);
        Objects.requireNonNull(getActivity()).startService(i);
        checkWording();

        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.UPDATE_CONFIG");
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(showConfigUpdate, _if);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).unregisterReceiver(showConfigUpdate);
        dismissDialogs();
    }

    public String curLang() {
        String lang = "";
        try {
            lang = TextUtils.isEmpty(PreferenceUtil.getPref(Objects.requireNonNull(getActivity())).getString(PreferenceUtil.SAVED_LANG_KEY, "EN"))
                    ? "" : PreferenceUtil.getPref(Objects.requireNonNull(getActivity())).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
            if (TextUtils.isEmpty(lang)) {
                Locale locale = getResources().getConfiguration().locale;
                lang = locale.getLanguage();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return lang;
    }

    private void checkWording() {
        try {
            ConfigData cdt = SessionManager.getConfigData(getActivity());
            if (cdt != null) {
                String wordShareIn = cdt.wording_share_id;
                String wordShareEn = cdt.wording_share_en;
                if (!TextUtils.isEmpty(wordShareIn) && !TextUtils.isEmpty(wordShareEn)) {
                    try {
                        if (curLang().equalsIgnoreCase("EN")) {
                            wordingDynamic = wordShareEn;
                        } else {
                            wordingDynamic = wordShareIn;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } else {
                Intent i = new Intent(getActivity(), SyncConfigService.class);
                Objects.requireNonNull(getActivity()).startService(i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendGoogleAnalitycs(String action) {
        try {
            GATRacker.getInstance(Objects.requireNonNull(getActivity()).getApplication())
                    .sendEventWithScreen(TrackerConstant.SCREEN_DEMO_SHARE,
                            TrackerConstant.EVENT_CAT_CONTACT, "click share",
                            own.caller_id + "/" + action);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendShareEmailAction(String link) {
        try {
            if (!TextUtils.isEmpty(wordingDynamic)) {
                String filtered = wordingDynamic.replace("[LINK]", link);
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "MViCall");
                intent.putExtra(Intent.EXTRA_TEXT, filtered);
                startActivity(Intent.createChooser(intent, "Sending Email..."));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendActionShare(String pName, String link) {
        try {
            List<Intent> targetedShareIntents = new ArrayList<>();
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            List<ResolveInfo> resInfo = Objects.requireNonNull(getActivity()).getPackageManager().queryIntentActivities(share, 0);
            if (resInfo != null && resInfo.size() > 0) {
                for (ResolveInfo info : resInfo) {

                    if (!TextUtils.isEmpty(wordingDynamic)) { // validate wording dynamic null...
                        String shareBody = wordingDynamic.replace("[LINK]", link);
                        Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);

                        if (pName.contains(info.activityInfo.packageName.toLowerCase())) {
                            targetedShare.setType("text/plain");
                            targetedShare.putExtra(Intent.EXTRA_TEXT, shareBody);
                            targetedShare.setPackage(info.activityInfo.packageName.toLowerCase());
                            targetedShareIntents.add(targetedShare);
                        }
                    } else {
                        String firends = TextUtils.isEmpty(model.getLang().string.str_toast_friends_not_sync)
                                ? "Data has not been synced, please try again..."
                                : model.getLang().string.str_toast_friends_not_sync;
                        Toast.makeText(getActivity(), firends, Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                if (targetedShareIntents.size() > 0) {
                    Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "MViCall Share");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[0]));
                    startActivity(chooserIntent);
                } else {
                    String app = TextUtils.isEmpty(model.getLang().string.str_toast_friends_not_installed)
                            ? "App not instaled"
                            : model.getLang().string.str_toast_friends_not_installed;
                    Toast.makeText(getActivity(), app , Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCustomDialog(String... arg) {
        if (isSafe()) {
            try {
                dialogUpload = new DialogUpload();
                dialogUpload.setMessage(arg[0]);
                dialogUpload.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void dismissDialogs() {
        if (isSafe()) {
            try {
                dialogUpload.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (btnFinish == v){
            String ccodeViet = PreferenceUtil.getPref(getActivity()).getString(PreferenceUtil.COUNTRY_CODE,"1");

            if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
                //boolean caseVietnamEightTeen = PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceUtil.CASE_EIGHTTEEEN_VIETNAME, false);
                boolean caseVietnamSevenTeen = PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceUtil.CASE_SEVENTEEEN_VIETNAME, false);
                if (!caseVietnamSevenTeen){
                    checkShowCase();
                }else{
                    //stractivity();
                    stractivityToMain();
                }
//                PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SETUP_SHARE, true).commit();
//                startActivity(new Intent(getActivity(), TimeLineNewActivity.class));

            }else{
//                PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SETUP_SHARE, true).commit();
//                startActivity(new Intent(getActivity(), MainActivity.class).putExtra("TIMELINE", "true"));
//                getActivity().finish();
                PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SETUP_SHARE, true).commit();
                PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.CASE_DIALOG, true).commit();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("Share", "true");
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().finish();
            }

        }
    }


    private boolean isSafe() {
        return !(ShareFrg.this.isRemoving() || ShareFrg.this.getActivity() == null || ShareFrg.this.isDetached() || !ShareFrg.this.isAdded() || ShareFrg.this.getView() == null);
    }

    //SHOWCASE VIETNAM
    public void showCaseIntroVietFiveTeen() {
        try {
            PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.CASE_FIFTEEN_VIETNAME, true).commit();
            LangViewModel model = LangViewModel.getInstance();

            ContactItem own = SessionManager.getProfile(getActivity());
            String strWordingTitleVietFiveTeen;
            if (own != null){
                strWordingTitleVietFiveTeen = TextUtils.isEmpty(model.getLang().string.caseTitleShare)
                        ?"(" + own.getName() + ")" + "! You are pioneer! A trend setter! You are the first to use MViCall among your friends!" :
                        model.getLang().string.caseTitleShare;
            }else{
                strWordingTitleVietFiveTeen = TextUtils.isEmpty(model.getLang().string.caseTitleShare)
                        ?"(name)! You are pioneer! A trend setter! You are the first to use MViCall among your friends!" :
                        model.getLang().string.caseTitleShare;
            }

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
            showCaseWrapper.showFancyVietnameFiveTeen(
                    ShowCaseWrapper.SC_VIET_FIVETEEN,
                    strWordingTitleVietFiveTeen,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            showCaseIntroVietSixTeen();
                        }

                        @Override
                        public void onOk() {
                            showCaseIntroVietSixTeen();
                        }

                        @Override
                        public void onSkip() {
                            showCaseIntroVietSixTeen();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showCaseIntroVietSixTeen() {
        try {
            PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.CASE_SIXTEEEN_VIETNAME, true).commit();
            LangViewModel model = LangViewModel.getInstance();

            String strWordingTitleVietSixTeen = TextUtils.isEmpty(model.getLang().string.caseTitleTimeShare)
                    ?"Time to invite your friends to follow your lead and use MViCall too. This way they can view your video ringtone and you can see theirs!" :
                    model.getLang().string.caseTitleTimeShare;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
            showCaseWrapper.showFancyVietnameSixTeen(
                    ShowCaseWrapper.SC_VIET_SIXTEEN,
                    strWordingTitleVietSixTeen,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            //showCaseIntroVietSevenTeen();
                        }

                        @Override
                        public void onOk() {
                            //showCaseIntroVietSevenTeen();
                        }

                        @Override
                        public void onSkip() {
                            //showCaseIntroVietSevenTeen();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showCaseIntroVietSevenTeen() {
        try {
            PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.CASE_SEVENTEEEN_VIETNAME, true).commit();
            LangViewModel model = LangViewModel.getInstance();

            ContactItem own = SessionManager.getProfile(getActivity());
            String strWordingTitleVietSevenTeen, strWordingTitleVietSevenTeen2;
            if (own != null){
                strWordingTitleVietSevenTeen = TextUtils.isEmpty(model.getLang().string.caseTitleFriendWillBeJoin)
                        ?"Thumbs up for your " + "(" + own.getName() + ")":
                        model.getLang().string.caseTitleFriendWillBeJoin;
                strWordingTitleVietSevenTeen2 = TextUtils.isEmpty(model.getLang().string.caseTitleFriendWillBeJoin2)
                        ?". your friends will soon join you in using MViCall." :
                        model.getLang().string.caseTitleFriendWillBeJoin2;
            }else{
                strWordingTitleVietSevenTeen = TextUtils.isEmpty(model.getLang().string.caseTitleFriendWillBeJoin)
                        ?"Thumbs up for your (name)." :
                        model.getLang().string.caseTitleFriendWillBeJoin;
                strWordingTitleVietSevenTeen2 = TextUtils.isEmpty(model.getLang().string.caseTitleFriendWillBeJoin2)
                        ?". your friends will soon join you in using MViCall." :
                        model.getLang().string.caseTitleFriendWillBeJoin2;
            }

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
            showCaseWrapper.showFancyVietnameSevenTeen(
                    ShowCaseWrapper.SC_VIET_SEVENTEEN,
                    strWordingTitleVietSevenTeen,
                    strWordingTitleVietSevenTeen2,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            stractivityToMain();
                            //showCaseIntroVietEightTeen();
                        }

                        @Override
                        public void onOk() {
                            stractivityToMain();
                            //showCaseIntroVietEightTeen();
                        }

                        @Override
                        public void onSkip() {
                            stractivityToMain();
                            //showCaseIntroVietEightTeen();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

//    public void showCaseIntroVietEightTeen() {
//        try {
//            PreferenceUtil.getEditor(getActivity()).putBoolean(PreferenceUtil.CASE_EIGHTTEEEN_VIETNAME, true).commit();
//            LangViewModel model = LangViewModel.getInstance();
//
//            String strWordingTitleVietSevenTeen = TextUtils.isEmpty(model.getLang().string.caseTitleAlmostForgot)
//                    ?"Oh, I almost forgot! I have 1 last cool feature for you" :
//                    model.getLang().string.caseTitleAlmostForgot;
//
//            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(getActivity());
//            showCaseWrapper.showFancyVietnameEightTeen(
//                    ShowCaseWrapper.SC_VIET_EIGHTTEEN,
//                    strWordingTitleVietSevenTeen,
//                    new ShowCaseListener.ActionListener() {
//
//                        @Override
//                        public void onScreenTap() {
//                            stractivityToMain();
//                        }
//
//                        @Override
//                        public void onOk() {
//                            stractivityToMain();
//                        }
//
//                        @Override
//                        public void onSkip() {
//                            stractivityToMain();
//                        }
//
//                    });
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }

    private void stractivity(){
        PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SETUP_SHARE, true).commit();
        startActivity(new Intent(getActivity(), TimeLineNewActivity.class));
    }

    private void stractivityToMain(){
        PreferenceUtil.getEditor(Objects.requireNonNull(getActivity())).putBoolean(PreferenceUtil.SETUP_SHARE, true).commit();
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("TOMAIN", "true");
        startActivity(intent);
        getActivity().finish();
    }

    private void checkShowCase(){

        boolean caseVietnamFiveTeen = PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceUtil.CASE_FIFTEEN_VIETNAME, false);
        boolean caseVietnamSixTeen = PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceUtil.CASE_SIXTEEEN_VIETNAME, false);
        boolean caseVietnamSevenTeen = PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceUtil.CASE_SEVENTEEEN_VIETNAME, false);
        boolean caseVietnamEightTeen = PreferenceUtil.getPref(getActivity()).getBoolean(PreferenceUtil.CASE_EIGHTTEEEN_VIETNAME, false);

        if (!caseVietnamFiveTeen){
            showCaseIntroVietFiveTeen();
        }else if (!caseVietnamSixTeen){
            showCaseIntroVietSixTeen();
        }else if (!caseVietnamSevenTeen){
            showCaseIntroVietSevenTeen();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        String ccodeViet = PreferenceUtil.getPref(getActivity()).getString(PreferenceUtil.COUNTRY_CODE,"1");

        if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
            checkShowCase();
        }
    }
}
