package com.sbimv.mvicalls.walktrough;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.apprtc.AudioPlayer;
import com.sbimv.mvicalls.databinding.ActivityDemoDialerBinding;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.view.CallerViewUtil;

import de.hdodenhof.circleimageview.CircleImageView;

public class DemoDialerActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mPrimaryName;
    private TextView mCallStatus;
    private TextView mcallStateLabel;
    private ImageView mImgBackground;
    private CircleImageView mPhoto;
    private ImageButton mAnswareCall;
    private ImageButton mEndCall;
    protected LangViewModel model;
    private AudioPlayer mAudioPlayer;
    private ImageView mImgClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int flags = WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_IGNORE_CHEEK_PRESSES;
        getWindow().addFlags(flags);

        ActivityDemoDialerBinding binding = DataBindingUtil.setContentView(DemoDialerActivity.this, R.layout.activity_demo_dialer);
        model = LangViewModel.getInstance();
        binding.setLangModel(model);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.hide();
        }

        mPrimaryName = findViewById(R.id.name);
        mcallStateLabel = findViewById(R.id.callStateLabel);
        mCallStatus = findViewById(R.id.callStatus);
        mImgBackground = findViewById(R.id.imgBackground);
        mImgClose = findViewById(R.id.imgClose);
        mPhoto = findViewById(R.id.photo);
        mAnswareCall = findViewById(R.id.floating_answer_call);
        mEndCall = findViewById(R.id.floating_end_call);

        mImgClose.setOnClickListener(this);
        mAnswareCall.setOnClickListener(this);
        mEndCall.setOnClickListener(this);

        String strLabel = TextUtils.isEmpty(model.getLang().string.demoDialerLabel) ? "Demo Incoming Call" : model.getLang().string.demoDialerLabel;
        String strStatus = TextUtils.isEmpty(model.getLang().string.demoDialerStatus) ? "Ponsel" : model.getLang().string.demoDialerStatus;

        mcallStateLabel.setText(strLabel);
        mCallStatus.setText(strStatus);

        ContactItem own = SessionManager.getProfile(DemoDialerActivity.this);
        if (own != null){
            setDataInit(own);
        }

        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_DEMO_SIMULATOR);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceUtil.getEditor(DemoDialerActivity.this).putBoolean(PreferenceUtil.DEMO_VIDEO, false).commit();
    }

    @SuppressLint("CheckResult")
    private void setDataInit(ContactItem own){

        String strName = TextUtils.isEmpty(own.name)?"....":own.name;
        String strUrlPict = TextUtils.isEmpty(own.caller_pic)?"":own.caller_pic;

        mPrimaryName.setText(strName);
        mcallStateLabel.setText("Demo Incomming Call");
        mCallStatus.setText("Ponsel");

        setPlayRingtone();

        try {
            RequestOptions defProfile = new RequestOptions();
            defProfile.placeholder(R.mipmap.ic_manprofile);
            Glide.with(DemoDialerActivity.this)
                    .load(strUrlPict)
                    .apply(defProfile)
                    .into(mPhoto);

            RequestOptions defBanner = new RequestOptions();
            defBanner.placeholder(R.drawable.newversinbg_);
            Glide.with(DemoDialerActivity.this)
                    .load(strUrlPict)
                    .apply(defBanner)
                    .into(mImgBackground);
        }catch (Exception e){
            e.printStackTrace();
        }

        String strMsisdn = own.msisdn;
        if (!TextUtils.isEmpty(strMsisdn)) {
            PreferenceUtil.getEditor(DemoDialerActivity.this).putBoolean(PreferenceUtil.DEMO_VIDEO, true).commit();
            CallerViewUtil.getInstance(getApplicationContext()).showView(strMsisdn,true,true);
        }
    }

    private void setPlayRingtone() {
        try {
            mAudioPlayer = new AudioPlayer(this);
            mAudioPlayer.playRingtone();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void stopRingtone(){
        if (mAudioPlayer != null) {
            mAudioPlayer.stopRingtone();
        }
    }

    @Override
    public void onClick(View v) {
        if (mEndCall == v){
            closeActivity();
        }else if (mAnswareCall == v) {
            closeActivity();
        }else if (mImgClose == v){
            closeActivity();
        }
    }

    private void closeActivity(){
        PreferenceUtil.getEditor(DemoDialerActivity.this).putBoolean(PreferenceUtil.DEMO_VIDEO, false).commit();
        CallerViewUtil.getInstance(getApplicationContext()).dismissView();
        stopRingtone();
        finish();
    }

}
