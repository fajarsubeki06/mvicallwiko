package com.sbimv.mvicalls.walktrough;

import android.os.Parcel;
import android.os.Parcelable;

import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.pojo.MyCollection;

public class ModelFreePremiumContent implements Parcelable {

    public String category_id;
    public String category_name;
    public String content_id ;
    public String alias;
    public String judul;
    public String source_content;
    public String thumb_pic ;
    public String price ;
    public String price_label;


    public ModelFreePremiumContent(Parcel in) {
        category_id = in.readString();
        category_name = in.readString();
        content_id = in.readString();
        alias = in.readString();
        judul = in.readString();
        source_content = in.readString();
        thumb_pic = in.readString();
        price = in.readString();
        price_label = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category_id);
        dest.writeString(category_name);
        dest.writeString(content_id);
        dest.writeString(alias);
        dest.writeString(judul);
        dest.writeString(source_content);
        dest.writeString(thumb_pic);
        dest.writeString(price);
        dest.writeString(price_label);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ModelFreePremiumContent> CREATOR = new Creator<ModelFreePremiumContent>() {
        @Override
        public ModelFreePremiumContent createFromParcel(Parcel in) {
            return new ModelFreePremiumContent(in);
        }

        @Override
        public ModelFreePremiumContent[] newArray(int size) {
            return new ModelFreePremiumContent[size];
        }
    };

}
