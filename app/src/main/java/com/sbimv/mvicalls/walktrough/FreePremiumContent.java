package com.sbimv.mvicalls.walktrough;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.databinding.ActivityFreePremiumContentBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;
import com.sbimv.mvicalls.services.DownloadVideoService;
import com.sbimv.mvicalls.sync.SyncContactProfileManager;
import com.sbimv.mvicalls.util.PermissionUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseListener;
import com.sbimv.mvicalls.util.ShowCaseWrapper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FreePremiumContent extends BaseActivity {

    private List<DataDinamicVideos> collection = new ArrayList<>();
    private RecyclerView rvFreeContent;
    private ProgressBar pgFreeContent;
    private static boolean isfinishorDenied = false;
    private ImageView back, imgLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityFreePremiumContentBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_free_premium_content);
        binding.setLangModel(model);

        back = findViewById(R.id.btn_toolbar_actionbar_back);
        imgLogo = findViewById(R.id.iv_logo);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Glide.with(FreePremiumContent.this).asBitmap().load(getResources().getIdentifier("logo_rebranding", "drawable", FreePremiumContent.this.getPackageName()))
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        final int height = resource.getHeight();
                        final int width = resource.getWidth();
                        imgLogo.post(() -> {
                            int imgW = imgLogo.getMeasuredWidth();
                            int imgH = imgW * height / width;
                            imgLogo.getLayoutParams().height = imgH;
                            imgLogo.requestLayout();
                            imgLogo.setImageBitmap(resource);
                        });
                    }
                });

        PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.SHOW_CASE_SETUP2, true).commit();

        rvFreeContent = findViewById(R.id.rv_free_content);
        TextView txtTitleFree = findViewById(R.id.txtTitleFree);
        pgFreeContent = findViewById(R.id.pgFreeContent);

        String title = TextUtils.isEmpty(model.getLang().string.titleActivityFreeContent) ? "Konten Gratis" : model.getLang().string.titleActivityFreeContent;
        txtTitleFree.setText(title);

        initRecyclerView();
        loadCollection();

        String ccodeViet = PreferenceUtil.getPref(FreePremiumContent.this).getString(PreferenceUtil.COUNTRY_CODE,"1");

        if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
           //callScSeven();
            checkShowCase();
        }

        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_DEMO_FREE_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showCaseIntroVietSeven(String des){
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_SEVEN_VIETNAME, true).commit();
            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(this);
            showCaseWrapper.showFancyVietnameSeven(
                    ShowCaseWrapper.SC_VIET_SEVEN,
                    des,
                    new ShowCaseListener.ActionListener(){

                        @Override
                        public void onScreenTap() {
                            //startActivity(new Intent(FreePremiumContent.this, FriendsDemoActivity.class));
                        }

                        @Override
                        public void onOk() {
                            //startActivity(new Intent(FreePremiumContent.this, FriendsDemoActivity.class));
                        }

                        @Override
                        public void onSkip() {
                            //startActivity(new Intent(FreePremiumContent.this, FriendsDemoActivity.class));
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void callScSeven(){

        String strWordingTitleVietSeven = TextUtils.isEmpty(model.getLang().string.caseChoosePremiumVideo)
                ? "Choose here your favourite FREE premium VIDEO RINGTONE" :
                model.getLang().string.caseChoosePremiumVideo;
        showCaseIntroVietSeven(strWordingTitleVietSeven);


    }

    @SuppressLint("WrongConstant")
    private void initRecyclerView() {
        LinearLayoutManager layout = new LinearLayoutManager(this);
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        int numOfColumn = 2;
        rvFreeContent.setLayoutManager(new GridLayoutManager(this, numOfColumn));
        AdapterFreeContent ar = new AdapterFreeContent(this, collection);
        rvFreeContent.setAdapter(ar);
    }

    private void loadCollection() {

        ContactItem own = SessionManager.getProfile(FreePremiumContent.this);
        if (own != null) {

            String msisdn = TextUtils.isEmpty(own.msisdn) ? "" : own.msisdn;
            String lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");

            if (!TextUtils.isEmpty(msisdn) && !TextUtils.isEmpty(lang)) {
                Call<APIResponse<List<DataDinamicVideos>>> call = ServicesFactory.getService().getFreContent(msisdn, lang);
                call.enqueue(new Callback<APIResponse<List<DataDinamicVideos>>>() {
                    @Override
                    public void onResponse(@NotNull Call<APIResponse<List<DataDinamicVideos>>> call, @NotNull Response<APIResponse<List<DataDinamicVideos>>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            List<DataDinamicVideos> data = response.body().data;
                            if (data != null) {
                                collection.clear();
                                collection.addAll(data);
                                rvFreeContent.getAdapter().notifyDataSetChanged();
                                pgFreeContent.setVisibility(View.GONE);
                                rvFreeContent.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<APIResponse<List<DataDinamicVideos>>> call, @NotNull Throwable t) {
                        if (FreePremiumContent.this.isFinishing()) return;
                        toast(model.getLang().string.wordingFailureResponse);
                    }
                });
            }
        }

    }

    private void checkShowCase(){

        boolean caseVietnamSeven = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_SEVEN_VIETNAME, false);

        if (!caseVietnamSeven){
            callScSeven();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        String ccodeViet = PreferenceUtil.getPref(FreePremiumContent.this).getString(PreferenceUtil.COUNTRY_CODE,"1");

        if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
            startActivity(new Intent(FreePremiumContent.this, FriendsDemoActivity.class));
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.SHOW_CASE_SETUP2, true).commit();
        }else{
            startActivity(new Intent(FreePremiumContent.this, SetupProfileActivity.class));
            finish();
        }

    }

}
