package com.sbimv.mvicalls;

import android.content.Context;
import android.content.SharedPreferences;

import com.sbimv.mvicalls.util.PreferenceUtil;

/**
 * Created by admin on 3/21/2018.
 */

public class AppPreference {

    public static final String CURRENT_MSISDN = "curmsisdn";
    public static final String SERIAL_SIM = "serialsim";
    public static final String PREFIX = "prefix";
    public static final String FIRST_SHOW_CASE = "runShowcase";
    public static final String SHOW_CASE_PROFILE = "show_profile";
    public static final String SHOW_CASE_FRIENDS = "show_friends";
    public static final String SHOW_CASE_VIDEOS = "show_videos";
    public static final String SHOW_CASE_TIMELINE = "timeline";
    public static final String SHOW_CASE_FINISH = "finish_all_sc";
    public static final String BUBBLE_VIEW_NAME = "tutorfillname";
    public static final String BUBBLE_VIEW_STATUS = "tutorfillstatus";
    public static final String COUNTRY_CODE_NAME = "country_code_name";
    public static final String REC = "rec";
    public static final String GALLERY = "gallery";
    public static final String SKIPPED = "skip";
    public static final String ISDOINVITE = "isDoInvite";


    public static void setPreferenceNew (Context context, String key, String data){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferenceUtil.NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,data);
        editor.apply();
    }

    // ********* Dont use this method again *********
    public static void setPreference (Context context, String key, String data){
        if (context != null) {
            SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(key, data);
            editor.apply();
        }
    }
    // ********* Dont use this method again *********
    public static String getPreference(Context context, String key){
        SharedPreferences preferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return preferences.getString(key, null);
    }

    // ********* Dont use this method again *********
 public static void setPreferenceBoolean (Context context, String key, boolean data){
         SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
         SharedPreferences.Editor editor = sharedPreferences.edit();
         editor.putBoolean(key, data);
         editor.apply();
    }

    // ********* Dont use this method again *********
    public static boolean getPreferenceBoolean(Context context, String key){
        SharedPreferences preferences = context.getSharedPreferences(key,Context.MODE_PRIVATE);
        return preferences.getBoolean(key,true);
    }

    public static void delPreference(Context context, String key){
        SharedPreferences delPref = context.getSharedPreferences(key,Context.MODE_PRIVATE);
        delPref.edit().clear().commit();
    }

    public static boolean isExist(Context context, String key){
        SharedPreferences sharedPreferences = context.getSharedPreferences(key, Context.MODE_PRIVATE);
        return sharedPreferences.contains(key);
    }
}
