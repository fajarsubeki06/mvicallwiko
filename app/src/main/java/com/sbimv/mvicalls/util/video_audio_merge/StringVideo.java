package com.sbimv.mvicalls.util.video_audio_merge;

public class StringVideo {


    public String TYPE_VIDEO = "video";
    public String APP_FOLDER = "MViCall";
    public String DATE_FORMAT = "yyyyMMdd_HHmmss";
    public String VIDEO_FORMAT = "mp4";
    public String AUDIO_FORMAT = "mp3";
    public int VIDEO_LIMIT = 4;

}
