package com.sbimv.mvicalls.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class HowtoWebView extends WebView {

    private Boolean enableScroll = true;

    public HowtoWebView(Context context) {
        super(context);
        initDefaultSetting();
    }

    public HowtoWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initDefaultSetting();
    }

    public HowtoWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initDefaultSetting();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public HowtoWebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initDefaultSetting();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initDefaultSetting() {
        WebSettings webSettings = this.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);
        setWebChromeClient(new WebChromeClient());
        setWebViewClient(new MyWebViewClients());
    }

    public void load(String url){
        this.loadUrl(url);
    }

    private class MyWebViewClients extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view.loadUrl(request.getUrl().toString());
            }else {
                view.loadUrl(getUrl());
            }
            return true;
        }
    }

    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        if (getParent() != null) {
            enableScroll = false;
        }
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            enableScroll = true;
        }
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(enableScroll);
        }
        return super.onTouchEvent(event);
    }
}
