package com.sbimv.mvicalls.util;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataChargingContent;
import com.sbimv.mvicalls.pojo.GpayData;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SessionManager {

    static Gson gson = new Gson();
    static Gson gsons = new Gson();

    public static ContactItem saveProfile(Context context, ContactItem own){
        ContactItem updated = getProfile(context);

        if(updated == null)
            updated = own;
        else {
            if (own.name != null)
                updated.name = own.name;

            if (own.name_phone_book != null)
                updated.name_phone_book = own.name_phone_book; // added name phone book

            if (own.email != null)
                updated.email = own.email;

            if (own.caller_pic != null)
                updated.caller_pic = own.caller_pic;

            if (own.video_caller != null)
                updated.video_caller = own.video_caller;

            if (own.status_caller != null)
                updated.status_caller = own.status_caller;

            if (own.profiles != null)
                updated.profiles = own.profiles;

            if (own.profile_status != null)
                updated.profile_status = own.profile_status;

            if (own.device != null)
                updated.device = own.device;

        }

        String json = gson.toJson(updated, ContactItem.class);
        PreferenceUtil.getEditor(context).putString("own", json).commit();
        return updated;
    }

    public static ContactItem getProfile(Context context){
        String json = PreferenceUtil.getPref(context).getString("own", null);
        if(!TextUtils.isEmpty(json))
            return gson.fromJson(json, ContactItem.class);
        else
            return null;
    }

    /**
     * Preference G-Pay Data
     * Create by Yudha Pratama Putra, 17 April 2019
     */
    public static GpayData saveDataGpay(Context context, GpayData gpy){

        GpayData updated = getGpayData(context);

        if (updated == null){
            updated = gpy;
        }

        if (gpy.msisdn != null)
            updated.msisdn = gpy.msisdn;

        if (gpy.type != null)
            updated.type = gpy.type;

        if (gpy.json != null)
            updated.json = gpy.json;

        if (gpy.periode != null)
            updated.periode = gpy.periode;

        if (gpy.price != null)
            updated.price = gpy.price;

        updated.cancel = gpy.cancel;

        String json = gson.toJson(updated, GpayData.class);
        PreferenceUtil.getEditor(context).putString("gpy", json).commit();

        return updated;
    }

    public static GpayData getGpayData(Context context){
        String json = PreferenceUtil.getPref(context).getString("gpy", null);
        if(!TextUtils.isEmpty(json))
            return gson.fromJson(json, GpayData.class);
        else
            return null;
    }

    public static ConfigData saveConfigData(Context context, ConfigData cdt) {

        ConfigData updated = getConfigData(context);

        if (updated == null){
            updated = cdt;
        }
        if (cdt.wording_share_id != null)
            updated.wording_share_id = cdt.wording_share_id;

        if (cdt.wording_share_en != null)
            updated.wording_share_en = cdt.wording_share_en;

        if (cdt.wording_showcase_id != null)
            updated.wording_showcase_id = cdt.wording_showcase_id;

        if (cdt.wording_showcase_en != null)
            updated.wording_showcase_en = cdt.wording_showcase_en;

        if (cdt.wording_free_id != null)
            updated.wording_free_id = cdt.wording_free_id;

        if (cdt.wording_free_en != null)
            updated.wording_free_en = cdt.wording_free_en;

        if (cdt.version_code != null)
            updated.version_code = cdt.version_code;

        if (cdt.version_lang != null)
            updated.version_lang = cdt.version_lang;

        updated.googlepay_enable = cdt.googlepay_enable;
        updated.a2ac_android = cdt.isA2ac_andorid();
        updated.a2ac_ios = cdt.isA2ac_ios();
        updated.mgm_enable = cdt.isMgm_enable();
        updated.viettel_pay_enable = cdt.isViettel_pay_enable();
        updated.showcase_finish = cdt.isShowcase_finish();
        updated.point_reward = cdt.isPoint_reward();
        updated.best_deal = cdt.isBest_deal();
        updated.sync_duration = cdt.sync_duration;
        updated.max_duration = cdt.max_duration;

        String json = gsons.toJson(updated, ConfigData.class);
        PreferenceUtil.getEditor(context).putString("cdt", json).commit();
        return updated;
    }

    public static ConfigData getConfigData(Context context) {
        String json = PreferenceUtil.getPref(context).getString("cdt", null);
        if (json != null) {
            return gson.fromJson(json, ConfigData.class);
        } else {
            return null;
        }
    }

    /**
     * Preference Data Charging Content
     * Create Yudha Pratama Putra, 18 sep 2018
     */
    public static void saveChargeArrayList (Context context, ArrayList<DataChargingContent> list){
        Gson gson = new Gson();
        String json = gson.toJson(list);
        PreferenceUtil.getEditor(context).putString(PreferenceUtil.CHARGGING, json).commit();
    }

    /**
     * Preference Data Charging Content
     * Create Yudha Pratama Putra, 18 sep 2018
     */
    public static ArrayList<DataChargingContent> getChargeArrayList (Context context){
        String json = PreferenceUtil.getPref(context).getString(PreferenceUtil.CHARGGING, null);
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<DataChargingContent>>() {}.getType();
        return gson.fromJson(json, type);
    }

}
