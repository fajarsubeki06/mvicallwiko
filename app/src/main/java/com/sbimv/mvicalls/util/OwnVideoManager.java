package com.sbimv.mvicalls.util;

import android.content.Context;
import android.net.Uri;

import java.io.File;

/**
 * Created by Hendi on 10/30/17.
 */

public class OwnVideoManager {
    public static final String DIS_NAME = "video_profile";
    public static final String OWN_VIDEO_FILE_NAME = "current_video.mp4";
    public static int MAX_DURATION = 7;
    public static final int MAX_FREE_DURATION = 3;

//===================== Video from gallery =======================
    public static Uri getOwnVideoUri(Context context){
        String path = context.getFilesDir().getPath() + File.separator + DIS_NAME + File.separator + OWN_VIDEO_FILE_NAME;
        return Uri.parse(path);
    }
    public static boolean isOwnVideoExist(Context context){
        Uri uri = getOwnVideoUri(context);
        String path = uri.toString();
        File f = new File(path);

        return f.exists();
    }

//====================== Video from Collection =========================
    public static Uri getCollectionUri(Context context){
        String url_collection = context.getPackageResourcePath();
        return Uri.parse(url_collection);
    }

    public static boolean collectionExist (Context context){
        Uri uri = getCollectionUri(context);
        String path = uri.toString();
        File f = new File(path);
        return f.exists();
    }

}
