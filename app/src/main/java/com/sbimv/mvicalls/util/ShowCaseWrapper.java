package com.sbimv.mvicalls.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.MainActivity;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.FocusShape;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ShowCaseWrapper{

    public static String ID_PROFILE = "sc_id_profile";
    public static String ID_ALLSET = "sc_id_allset";
    public static String ID_UPLOAD_VIDEO = "sc_id_upload_video";
    public static String ID_FINISH_SC = "sc_id_finish_sc";
    public static String ID_ALL_DONE = "sc_id_all_done";
    public static String EDIT_NAME = "edit_name";
    public static String EDIT_STATUS = "edit_status";
    public static String SET_VIDEO_TONE = "set_video_tone";
    public static String RECORD = "record";
    public static String GLLERY = "gallery";
    public static String INVITE = "invite";
    public static String UPLOAD = "upload";
    public static String ABORT_UPLOAD = "abort_upload";
    public static String ABORT_UPLOAD_GALLERY = "abort_upload_gallery";
    public static String CHOOSE_FREE_CONTENT = "choose_free_content";

    public static String DEMO_EDIT_PROFILE = "demo_edit_profile";
    public static String DEMO_EDIT_VIDEO = "demo_edit_video";
    public static String DEMO_BUTTON_PREMIUM = "demo_button_premium";
    public static String DEMO_CALL = "demo_call";
    public static String DEMO_SYNC = "demo_sync";
    public static String DEMO_NEXT = "demo_next";
    public static String DEMO_FIVE = "demo_five";
    public static String DEMO_SEVENT = "demo_sevent";

    public static String SC_VIET_FIRST = "sc_viet_first";
    public static String SC_VIET_TWO = "sc_viet_two";
    public static String SC_VIET_THIRD = "sc_viet_third";
    public static String SC_VIET_FOR = "sc_viet_for";
    public static String SC_VIET_FIVE = "sc_viet_five";
    public static String SC_VIET_SIX = "sc_viet_six";
    public static String SC_VIET_SEVEN = "sc_viet_seven";
    public static String SC_VIET_EIGHT = "sc_viet_eight";
    public static String SC_VIET_NINE = "sc_viet_nine";
    public static String SC_VIET_TEEN = "sc_viet_teen";
    public static String SC_VIET_ELEVEN = "sc_viet_eleven";
    public static String SC_VIET_TWELVE = "sc_viet_twelpe";
    public static String SC_VIET_THIRDTEEN = "sc_viet_thirdteen";
    public static String SC_VIET_FOURTEEN = "sc_viet_fourteen";
    public static String SC_VIET_FIVETEEN = "sc_viet_fiveteen";
    public static String SC_VIET_SIXTEEN = "sc_viet_sixteen";
    public static String SC_VIET_SEVENTEEN = "sc_viet_seventeen";
    public static String SC_VIET_EIGHTTEEN = "sc_viet_eightteen";
    public static String SC_VIET_NINETEEN = "sc_viet_nineteen";
    public static String SC_VIET_TWENTYEEN = "sc_viet_twentyteen";
    public static String SC_VIET_TWENTTYONETEEN = "sc_viet_twentyonetteen";
    public static String SC_VIET_RECORD = "sc_viet_record2";
    public static String SC_VIET_VIDEO = "sc_viet_video2";


    public static String SUCCESS_GRAB_FREE_CONTENT = "success_free_content";

    private FancyShowCaseView fancyView;
    private Activity mActivity;

    private LangViewModel model = LangViewModel.getInstance();
    private ContactItem own;
    private EditText inputName;

    //For Video
    private VideoView mVideoView;
    private FrameLayout mediaControllerAnchor;
    private MediaController mediaController;
    private boolean setCont = false;
    private String urlBy = "";

    private ShowCaseWrapper(Activity activity) {
        mActivity = activity;
    }

    public static ShowCaseWrapper create(Activity activity) {
        return new ShowCaseWrapper(activity);
    }

    public void showFancydoAction(final String showCaseId, final String DescAction, View focusView, final ShowCaseListener.ActionListener listener) {
        boolean isShown = getPreference().getBoolean(showCaseId, false);

        if (!mActivity.isFinishing() && !isShown) {
            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(false)
                        .focusOn(focusView)
                        .roundRectRadius(90)
                        .focusBorderSize(10)
                        .closeOnTouch(false)
                        .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .customView(R.layout.case_view_action, view -> {
                            Button btnSkip = view.findViewById(R.id.btnSkipAction);
                            TextView tvDesc = view.findViewById(R.id.tvDescAction);
                            tvDesc.setText(DescAction);
                            btnSkip.setText(model.getLang().string.skip);

                            if (showCaseId.equals("upload")){
                                btnSkip.setVisibility(View.GONE);
                            }
                            view.setOnClickListener(view1 -> {
                                removeView();
                                listener.onScreenTap();
                            });
                            view.findViewById(R.id.btnOKAction).setOnClickListener(view12 -> {
                                removeView();
                                listener.onOk();
                            });
                            btnSkip.setOnClickListener(view13 -> {
                                removeView();
                                listener.onSkip();
                            });
                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyNext(String showCaseId, final ShowCaseListener.NextListener listener) {
        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(false)
                        .customView(R.layout.case_view_next, view -> {
                            Button btnNext = view.findViewById(R.id.btnFinish);
                            btnNext.setText(model.getLang().string.next);

                            view.findViewById(R.id.btnFinish).setOnClickListener(view1 -> {
                                removeView();
                                listener.onNext();
                            });
                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyFinish(String showCaseId, final String wording, final ShowCaseListener.InviteListener listener) {
        boolean isShown = getPreference().getBoolean(showCaseId, false);

        if (!mActivity.isFinishing() && !isShown) {
            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(false)
                        .customView(R.layout.case_view_all_finish, view -> {
                            TextView tvWording = view.findViewById(R.id.tvWordingAllfinish);
                            Button btnInvite = view.findViewById(R.id.btnInviteFinishSC);
                            Button btnFinish = view.findViewById(R.id.btnFinishSC);
                            btnFinish.setText(model.getLang().string.finish);
                            btnInvite.setText(model.getLang().string.inviteFriends);
                            tvWording.setText(wording);

                            view.findViewById(R.id.btnFinishSC).setOnClickListener(view1 -> {
                                removeView();
                                listener.onFinish();
                            });
                            view.findViewById(R.id.btnInviteFinishSC).setOnClickListener(view12 -> {
                                removeView();
                                listener.onInvite();
                            });
                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

//    public void showFancyIntro(final ShowCaseListener.IntroListener listener) {
//        if (!mActivity.isFinishing()) {
//            if (fancyView == null) {
//                fancyView = new FancyShowCaseView.Builder(mActivity)
//                        .closeOnTouch(false)
//                        .customView(R.layout.case_view_confirmation, new OnViewInflateListener() {
//                            @Override
//                            public void onViewInflated(@NonNull View view) {
//
//                                view.findViewById(R.id.btnNo).setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        removeView();
//                                        listener.onCancel();
//                                    }
//                                });
//                                view.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        removeView();
//                                        listener.onOk();
//
//                                    }
//                                });
//                            }
//                        }).build();
//            }
//            if (fancyView.isShown()) {
//                return;
//            }
//            fancyView.show();
//        }
//    }

    public void showFancyFreeContent(String showCaseId, final String DescAction, View focusView, final ShowCaseListener.IntroListener listener) {
        if (showCaseId == null){
            return;
        }
        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {

            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(false)
                        .focusOn(focusView)
                        .focusBorderSize(10)
                        .closeOnTouch(false)
                        .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .customView(R.layout.case_view_free_content, view -> {
                            TextView textView = view.findViewById(R.id.tvDescFreeContent);
                            Button btnChoose = view.findViewById(R.id.btnChHooseContent);
                            Button btnSkip = view.findViewById(R.id.btnSkipAction);
                            btnSkip.setText(model.getLang().string.skip);
                            btnChoose.setText(model.getLang().string.chooseVideo);
                            textView.setText(DescAction);

                            view.setOnClickListener(v -> {
                                Intent intent = new Intent(mActivity, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                                removeView();
                            });
                            view.findViewById(R.id.btnChHooseContent).setOnClickListener(view1 -> {
                                listener.onOk();
                                removeView();
                            });
                            view.findViewById(R.id.btnSkipAction).setOnClickListener(v -> {
                                boolean isSkipped = AppPreference.getPreferenceBoolean(mActivity,AppPreference.SKIPPED);
                                if (!isSkipped){
                                    removeView();
                                }else {
                                    Intent intent = new Intent(mActivity,MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    mActivity.startActivity(intent);
                                    removeView();
                                }
                            });
                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyDemoEditProfile(String showCaseId, final String DescAction, View focusView, final ShowCaseListener.ActionListener listener) {
        if (showCaseId == null){
            return;
        }
        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
//        if (!mActivity.isFinishing()) {
            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(false)
                        .focusOn(focusView)
                        .focusBorderSize(10)
                        .closeOnTouch(false)
                        .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .customView(R.layout.case_view_free_content, view -> {

                            TextView textView = view.findViewById(R.id.tvDescFreeContent);
                            Button btnChoose = view.findViewById(R.id.btnChHooseContent);
                            Button btnSkip = view.findViewById(R.id.btnSkipAction);
                            btnSkip.setText(model.getLang().string.skip);
                            btnSkip.setVisibility(View.GONE);
                            btnChoose.setText(model.getLang().string.next);
                            textView.setText(DescAction);

                            view.setOnClickListener(view1 -> {
                                removeView();
                                listener.onScreenTap();
                            });

                            view.findViewById(R.id.btnChHooseContent).setOnClickListener(view12 -> {
                                removeView();
                                listener.onOk();
                            });
                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showCaseDemoCall(String showCaseId, final String DescAction, View focusView, final ShowCaseListener.ActionListener listener) {
        if (showCaseId == null){
            return;
        }
        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
//            if (!mActivity.isFinishing()) {
                if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(false)
                        .focusOn(focusView)
                        .focusBorderSize(10)
                        .closeOnTouch(false)
                        .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .customView(R.layout.case_view_free_content, view -> {

                            TextView textView = view.findViewById(R.id.tvDescFreeContent);
                            Button btnChoose = view.findViewById(R.id.btnChHooseContent);
                            Button btnSkip = view.findViewById(R.id.btnSkipAction);
                            btnSkip.setText(model.getLang().string.skip);
                            btnSkip.setVisibility(View.GONE);
                            btnChoose.setText(model.getLang().string.next);
                            textView.setText(DescAction);

                            view.setOnClickListener(view1 -> {
                                removeView();
                                listener.onScreenTap();
                            });

                            view.findViewById(R.id.btnChHooseContent).setOnClickListener(view12 -> {
                                removeView();
                                listener.onOk();
                            });
                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }

    }

    public void showCaseDemoContact(String showCaseId, final String DescAction, View focusView, final ShowCaseListener.ActionListener listener) {
        if (showCaseId == null){
            return;
        }
        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
//            if (!mActivity.isFinishing()) {
            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(false)
                        .focusOn(focusView)
                        .focusBorderSize(10)
                        .closeOnTouch(false)
                        .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .customView(R.layout.case_view_bottom_wl, view -> {

                            TextView textView = view.findViewById(R.id.tvDescFreeContent);
                            Button btnChoose = view.findViewById(R.id.btnChHooseContent);
                            Button btnSkip = view.findViewById(R.id.btnSkipAction);
                            btnSkip.setText(model.getLang().string.skip);
                            btnSkip.setVisibility(View.GONE);
                            btnChoose.setText(model.getLang().string.next);
                            textView.setText(DescAction);

                            view.setOnClickListener(view1 -> {
                                removeView();
                                listener.onScreenTap();
                            });

                            view.findViewById(R.id.btnChHooseContent).setOnClickListener(view12 -> {
                                removeView();
                                listener.onOk();
                            });
                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }

    }

    public void showCaseDemoFive(String showCaseId, final String DescAction, final ShowCaseListener.ActionListener listener) {
        if (showCaseId == null){
            return;
        }
        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
//        if (!mActivity.isFinishing()) {
            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(false)
                        .focusBorderSize(10)
                        .closeOnTouch(false)
                        .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .customView(R.layout.case_view_bottom_wl, view -> {

                            TextView textView = view.findViewById(R.id.tvDescFreeContent);
                            Button btnChoose = view.findViewById(R.id.btnChHooseContent);
                            Button btnSkip = view.findViewById(R.id.btnSkipAction);
                            btnSkip.setText(model.getLang().string.skip);
                            btnSkip.setVisibility(View.GONE);
                            btnChoose.setText(model.getLang().string.next);
                            textView.setText(DescAction);

                            view.setOnClickListener(view1 -> {
                                removeView();
                                listener.onScreenTap();
                            });

                            view.findViewById(R.id.btnChHooseContent).setOnClickListener(view12 -> {
                                removeView();
                                listener.onOk();
                            });
                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showCaseDemoSync(String showCaseId, final String DescAction, View focusView, final ShowCaseListener.ActionListener listener) {
        if (showCaseId == null){
            return;
        }
        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
//            if (!mActivity.isFinishing()) {
                if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(false)
                        .focusOn(focusView)
                        .focusBorderSize(10)
                        .closeOnTouch(false)
                        .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .customView(R.layout.case_view_free_content, view -> {

                            TextView textView = view.findViewById(R.id.tvDescFreeContent);
                            Button btnChoose = view.findViewById(R.id.btnChHooseContent);
                            Button btnSkip = view.findViewById(R.id.btnSkipAction);
                            btnSkip.setText(model.getLang().string.skip);
                            btnSkip.setVisibility(View.GONE);
                            btnChoose.setText(model.getLang().string.next);
                            textView.setText(DescAction);

                            view.setOnClickListener(view1 -> {
                                removeView();
                                listener.onScreenTap();
                            });

                            view.findViewById(R.id.btnChHooseContent).setOnClickListener(view12 -> {
                                removeView();
                                listener.onOk();
                            });
                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyDemoEditVideo(final String showCaseId, final String DescAction, View focusView, final ShowCaseListener.ActionListener listener) {
        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
//        if (!mActivity.isFinishing()) {
                if (fancyView == null) {
                    fancyView = new FancyShowCaseView.Builder(mActivity)
                            .closeOnTouch(false)
                            .focusOn(focusView)
                            .roundRectRadius(90)
                            .focusBorderSize(10)
                            .closeOnTouch(false)
                            .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                            .focusShape(FocusShape.ROUNDED_RECTANGLE)
                            .customView(R.layout.case_view_top_action, view -> {
                                Button btnSkip = view.findViewById(R.id.btnSkipAction);
                                TextView tvDesc = view.findViewById(R.id.tvDescAction);
                                tvDesc.setText(DescAction);
                                btnSkip.setText(model.getLang().string.skip);
                                btnSkip.setVisibility(View.GONE);

                                view.setOnClickListener(view1 -> {
                                    removeView();
                                    listener.onScreenTap();
                                });
                                view.findViewById(R.id.btnOKAction).setOnClickListener(view12 -> {
                                    removeView();
                                    listener.onOk();
                                });
                            }).build();
                }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    // Showcase for vietnam.................

    public void showFancyVietnameFirst(final String showCaseId,
                                       final String title,
                                       final String DescAction,
                                       final String sBtn,
                                       final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {

        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnam, view -> {

                        ImageView imageView = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvBody = view.findViewById(R.id.textView);
                        Button btn = view.findViewById(R.id.btnOK);

                        tvTitle.setTextColor(mActivity.getResources().getColor(R.color.black));
                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_5_));
                        tvTitle.setText(title);
                        tvBody.setText(DescAction);
                        btn.setText(sBtn);

                        btn.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    @SuppressLint("SetTextI18n")
    public void showFancyVietnameTwo(final String showCaseId,
                                     final String title,
                                     final String descAction,
                                     final String sBtn,
                                     final ShowCaseListener.ActionsListenerEditText listenerEditText
                                    ) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {

        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnam, view -> {

                        ImageView imageView = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvBody = view.findViewById(R.id.textView);
                        Button btn = view.findViewById(R.id.btnOK);
                        Button btnSave = view.findViewById(R.id.btnSimpan);
                        LinearLayout setName = view.findViewById(R.id.linearSetName);
                        inputName = view.findViewById(R.id.textSetName);
                        ProgressBar progressBar = view.findViewById(R.id.progressBar);

                        btn.setVisibility(View.GONE);
                        setName.setVisibility(View.VISIBLE);
                        tvTitle.setTextColor(mActivity.getResources().getColor(R.color.black));
                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_2_));
                        tvTitle.setText(title);
                        tvBody.setText(descAction);
                        btn.setText("( " + sBtn  + " )");

                        String strWordingTitleValidation = TextUtils.isEmpty(model.getLang().string.caseTitleValidation)
                                ?"Ooops, Enter your name..." :
                                model.getLang().string.caseTitleValidation;

                        String strWordingTitleSave = TextUtils.isEmpty(model.getLang().string.save)
                                ?"Save" :
                                model.getLang().string.save;

                        String strWordingInputName = TextUtils.isEmpty(model.getLang().string.str_input_name)
                                ?"(enter name)" :
                                model.getLang().string.str_input_name;

                        inputName.setHint(strWordingInputName);
                        btnSave.setText(strWordingTitleSave);

                        own = SessionManager.getProfile(mActivity);
                        if (own != null){
                            inputName.setText(own.name);
                        }else{
                            inputName.setText("Your Name");
                        }

                        btnSave.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String getName = inputName.getText().toString();
                                if (TextUtils.isEmpty(getName)){
                                    Toast.makeText(mActivity, strWordingTitleValidation, Toast.LENGTH_SHORT).show();
                                }else{
                                    progressBar.setVisibility(View.VISIBLE);
                                    new Handler().postDelayed(() -> {
                                        progressBar.setVisibility(View.GONE);
                                        removeView();
                                        updateProfile(listenerEditText);
                                        closeKeyboard();
                                    }, 1500);
                                }
                            }
                        });

                        inputName.setOnEditorActionListener((v, actionId, event) -> {
                            if (actionId == EditorInfo.IME_ACTION_DONE) {
                                String getName = inputName.getText().toString();
                                if (TextUtils.isEmpty(getName)){
                                    Toast.makeText(mActivity, strWordingTitleValidation, Toast.LENGTH_SHORT).show();
                                }else{
                                    progressBar.setVisibility(View.VISIBLE);
                                    new Handler().postDelayed(() -> {
                                        progressBar.setVisibility(View.GONE);
                                        removeView();
                                        updateProfile(listenerEditText);
                                        closeKeyboard();
                                    }, 1500);
                                    return true;
                                }

                            }
                            return false;
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    private void updateProfile(final ShowCaseListener.ActionsListenerEditText listenerEditText){
        //Set Request Body
        RequestBody _caller_id = RequestBody.create(MediaType.parse("text/plain"), own.caller_id);
        RequestBody _name = null;
        if (!inputName.getText().toString().isEmpty())
            _name = RequestBody.create(MediaType.parse("text/plain"), inputName.getText().toString());
        RequestBody _email = null;
        RequestBody _profile = null;
        MultipartBody.Part _file = null;

        Call<APIResponse<ContactItem>> call = ServicesFactory.getService().updateProfile(_caller_id, _name, _email, _profile, _file);
        call.enqueue(new Callback<APIResponse<ContactItem>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<ContactItem>> call, @NotNull Response<APIResponse<ContactItem>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()){
                    ContactItem updated = response.body().data;
                    updated.profiles = null;// Untuk syncronize firebase
                    PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_TWO_VIETNAME, true).commit();
                    own = SessionManager.saveProfile(mActivity, updated);
                    if (own != null) {
                        SetupProfileActivity.etUserName.setText(own.getName());
                        inputName.setText(own.getName());
                        listenerEditText.onScreenTap(own.getName(), true);
                    }


                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<ContactItem>> call, @NotNull Throwable t) {
                Toast.makeText(mActivity, model.getLang().string.updateProfileFailed, Toast.LENGTH_SHORT).show();
                listenerEditText.onScreenTap(own.getName(), false);
            }
        });

    }

    private void closeKeyboard() {
        try{
            InputMethodManager inputManager = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(Objects.requireNonNull(mActivity.getCurrentFocus()).getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    public void showFancyVietnameThird(final String showCaseId,
                                       final String descAction,
                                       final String name,
                                       final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {

        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnam, view -> {

                        ImageView imageView = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvBody = view.findViewById(R.id.textView);
                        Button btn = view.findViewById(R.id.btnOK);

                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_1_));

                        tvBody.setText(descAction + name);
                        tvTitle.setVisibility(View.GONE);
                        btn.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameFor(final String showCaseId,
                                       final String descAction,
                                       final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {

        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnam, view -> {

                        ImageView imageView = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvBody = view.findViewById(R.id.textView);
                        Button btn = view.findViewById(R.id.btnOK);

                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_4_));
                        tvBody.setText(descAction);
                        tvTitle.setVisibility(View.GONE);
                        btn.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameFive(final String showCaseId,
                                     final String descAction,
                                     final View focusView,
                                     final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {

        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .focusOn(focusView)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnam, view -> {

                        ImageView imageView = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvBody = view.findViewById(R.id.textView);
                        Button btn = view.findViewById(R.id.btnOK);

                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_2_));
                        tvBody.setText(descAction);
                        tvTitle.setVisibility(View.GONE);
                        btn.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameRecord(final String showCaseId,
                                      final String descAction,
                                      final View focusView,
                                      final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {

            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .roundRectRadius(90)
                        .focusBorderSize(10)
                        .focusOn(focusView)
                        .closeOnTouch(false)
                        .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .customView(R.layout.case_view_vietnam, view -> {

                            ImageView imageView = view.findViewById(R.id.imgtutor1);
                            TextView tvTitle = view.findViewById(R.id.judul);
                            TextView tvBody = view.findViewById(R.id.textView);
                            Button btn = view.findViewById(R.id.btnOK);

                            imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_2_));
                            tvBody.setText(descAction);
                            tvTitle.setVisibility(View.GONE);
                            btn.setVisibility(View.GONE);

                            view.setOnClickListener(view1 -> {
                                removeView();
                                listener.onScreenTap();
                            });

                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameVideo(final String showCaseId,
                                       final String descAction,
                                        final View focusView,
                                        final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {

            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .roundRectRadius(90)
                        .focusBorderSize(10)
                        .focusOn(focusView)
                        .closeOnTouch(false)
                        .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .customView(R.layout.case_view_vietnam, view -> {

                            ImageView imageView = view.findViewById(R.id.imgtutor1);
                            TextView tvTitle = view.findViewById(R.id.judul);
                            TextView tvBody = view.findViewById(R.id.textView);
                            Button btn = view.findViewById(R.id.btnOK);

                            imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_2_));
                            tvBody.setText(descAction);
                            tvTitle.setVisibility(View.GONE);
                            btn.setVisibility(View.GONE);

                            view.setOnClickListener(view1 -> {
                                removeView();
                                listener.onScreenTap();
                            });

                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameSix(final String showCaseId,
                                      final String descAction,
                                      final View focusView,
                                      final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {

        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .focusOn(focusView)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnam, view -> {

                        ImageView imageView = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvBody = view.findViewById(R.id.textView);
                        Button btn = view.findViewById(R.id.btnOK);

                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_7_));
                        tvBody.setText(descAction);
                        tvTitle.setVisibility(View.GONE);
                        btn.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameSeven(final String showCaseId,
                                       final String descAction,
                                       final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {

        if (!mActivity.isFinishing()) {
            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(true)
                        .customView(R.layout.case_view_vietnam, view -> {

                            ImageView imageView = view.findViewById(R.id.imgtutor1);
                            TextView tvTitle = view.findViewById(R.id.judul);
                            TextView tvBody = view.findViewById(R.id.textView);
                            Button btn = view.findViewById(R.id.btnOK);
                            ImageView imageCharacter = view.findViewById(R.id.imageCharacter);
                            TextView textSubtitle = view.findViewById(R.id.textSubtitle);
                            RelativeLayout relativeLayout_bottom = view.findViewById(R.id.relaGone);
                            RelativeLayout relativeLayout_default = view.findViewById(R.id.relaConf);

                            relativeLayout_bottom.setVisibility(View.VISIBLE);
                            relativeLayout_default.setVisibility(View.GONE);
                            imageView.setVisibility(View.GONE);
                            tvTitle.setVisibility(View.GONE);
                            tvBody.setVisibility(View.GONE);
                            btn.setVisibility(View.GONE);

                            imageCharacter.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_8_));
                            textSubtitle.setText(descAction);

                            view.setOnClickListener(view1 -> {
                                removeView();
//                                        PreferenceUtil.getEditor(mActivity).putBoolean(PreferenceUtil.CASE_SEVEN_VIETNAME, false).commit();
//                                        mActivity.startActivity(new Intent(mActivity, FriendsDemoActivity.class));
//                                        mActivity.finish();
                                listener.onScreenTap();
                            });

                        }).build();

            }

            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }

    }
    }

    public void showFancyVietnameEight(final String showCaseId,
                                       final String DescAction,
                                       View focusView,
                                       final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .focusOn(focusView)
                        .roundRectRadius(90)
                        .focusBorderSize(10)
                        .closeOnTouch(false)
                        .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .customView(R.layout.case_view_vietnames_center, view -> {

                            Button btnOk = view.findViewById(R.id.btnOK);
                            TextView tvTitle = view.findViewById(R.id.judul);
                            TextView tvDesc = view.findViewById(R.id.textView);
                            tvDesc.setText(DescAction);
                            tvTitle.setVisibility(View.GONE);
                            btnOk.setVisibility(View.GONE);

                            view.setOnClickListener(view1 -> {
                                removeView();
                                listener.onScreenTap();
                            });

                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameNine(final String showCaseId,
                                       final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
            if (fancyView == null) {
                fancyView = new FancyShowCaseView.Builder(mActivity)
                        .closeOnTouch(false)
                        .customView(R.layout.case_view_vietnam_eight, view -> {

                            TextView textVideoName, textVideoPhone, tvLike, tvReport;
                            textVideoName = view.findViewById(R.id.tv_video_name);
                            textVideoPhone = view.findViewById(R.id.tv_video_phone);
                            tvLike = view.findViewById(R.id.tvQtyLike);
                            tvReport = view.findViewById(R.id.tvQtyReport);
                            Button btnNext = view.findViewById(R.id.btnNext);

                            // caller contact
                            ContactItem item = SessionManager.getProfile(mActivity);

                            assert item != null;
                            textVideoName.setText(item.getName());
                            textVideoPhone.setText(TextUtils.isEmpty(item.msisdn)?"":item.msisdn);
                            String like = TextUtils.isEmpty(model.getLang().string.like)
                                    ?"Like" :
                                    model.getLang().string.like;
                            String report = TextUtils.isEmpty(model.getLang().string.wordingReport)
                                    ?"Report" :
                                    model.getLang().string.wordingReport;
                            String next = TextUtils.isEmpty(model.getLang().string.next)
                                    ?"Next" :
                                    model.getLang().string.next;
                            tvLike.setText(like);
                            tvReport.setText(report);
                            btnNext.setText(next);

                            initMediaPlayer(view);

                            Button btnOk = view.findViewById(R.id.btnNext);
                            btnOk.setOnClickListener(view1 -> {
                                removeView();
                                listener.onScreenTap();
                            });

                        }).build();
            }
            if (fancyView.isShown()) {
                return;
            }
            fancyView.show();
            getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameTeen(final String showCaseId,
                                       final String DescAction,
                                       View focusView,
                                       final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .focusOn(focusView)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnames_center, view -> {

                        ImageView imgtutor1 = view.findViewById(R.id.imgtutor1);
                        Button btnOk = view.findViewById(R.id.btnOK);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvDesc = view.findViewById(R.id.textView);

                        imgtutor1.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_2_));
                        tvDesc.setText(DescAction);
                        tvTitle.setVisibility(View.GONE);
                        btnOk.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameEleven(final String showCaseId,
                                      final String Title,
                                      final String DescAction,
                                      final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnam_bottom, view -> {

                        ImageView imgtutor1 = view.findViewById(R.id.imageCharacter);
                        TextView tvDesc = view.findViewById(R.id.textSubtitle);
                        TextView tvTitle = view.findViewById(R.id.textTitle);

                        imgtutor1.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_3_));
                        tvDesc.setText(DescAction);
                        tvTitle.setText(Title);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameTwelpe(final String showCaseId,
                                        final String DescAction,
                                        final String DescAction2,
                                        final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnames_center, view -> {

                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvDesc = view.findViewById(R.id.textView);
                        Button buttonOke = view.findViewById(R.id.btnOK);

                        tvDesc.setText(DescAction + DescAction2);
                        tvTitle.setVisibility(View.GONE);
                        tvDesc.setTextSize(18);
                        buttonOke.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameThirdTeen(final String showCaseId,
                                        final String DescAction,
                                        final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnames_center, view -> {

                        ImageView imageCharacter = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvDesc = view.findViewById(R.id.textView);
                        Button buttonOke = view.findViewById(R.id.btnOK);

                        imageCharacter.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_1_));
                        tvDesc.setText(DescAction);
                        tvTitle.setVisibility(View.GONE);
                        buttonOke.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameFourTeen(final String showCaseId,
                                           final String DescAction,
                                           final View focus,
                                           final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .focusOn(focus)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
//                    .fitSystemWindows(true)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnames_center, view -> {

                        ImageView imageCharacter = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvDesc = view.findViewById(R.id.textView);
                        Button buttonOke = view.findViewById(R.id.btnOK);

                        imageCharacter.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_9_));
                        tvDesc.setText(DescAction);
                        tvTitle.setVisibility(View.GONE);
                        buttonOke.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameFiveTeen(final String showCaseId,
                                          final String DescAction,
                                          final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnam_bottom, view -> {

                        TextView tvTitle = view.findViewById(R.id.textTitle);
                        TextView tvDesc = view.findViewById(R.id.textSubtitle);

                        tvDesc.setText(DescAction);
                        tvTitle.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameSixTeen(final String showCaseId,
                                          final String DescAction,
                                          final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnam_bottom, view -> {

                        ImageView imageView = view.findViewById(R.id.imageCharacter);
                        TextView tvTitle = view.findViewById(R.id.textTitle);
                        TextView tvDesc = view.findViewById(R.id.textSubtitle);

                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_9_));
                        tvDesc.setText(DescAction);
                        tvTitle.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameSevenTeen(final String showCaseId,
                                         final String DescAction,
                                         final String DescAction2,
                                         final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnames_center, view -> {

                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvDesc = view.findViewById(R.id.textView);
                        Button btnOke = view.findViewById(R.id.btnOK);

                        tvDesc.setText(DescAction + DescAction2);
                        tvTitle.setVisibility(View.GONE);
                        btnOke.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameEightTeen(final String showCaseId,
                                           final String DescAction,
                                           final View focus,
                                           final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .focusOn(focus)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnames_center, view -> {

                        ImageView imageView = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvDesc = view.findViewById(R.id.textView);
                        Button btnOke = view.findViewById(R.id.btnOK);

                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_5_));
                        tvDesc.setText(DescAction);
                        tvTitle.setVisibility(View.GONE);
                        btnOke.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameNineTeen(final String showCaseId,
                                           final String DescAction,
                                           final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnames_center, view -> {

                        ImageView imageView = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvDesc = view.findViewById(R.id.textView);
                        Button btnOke = view.findViewById(R.id.btnOK);

                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_5_));
                        tvDesc.setText(DescAction);
                        tvTitle.setVisibility(View.GONE);
                        btnOke.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameTwenty(final String showCaseId,
                                          final String DescAction,
                                          View focus,
                                          final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .focusOn(focus)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnam_main, view -> {

                        ImageView imageView = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvDesc = view.findViewById(R.id.textView);

                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_3_));
                        tvDesc.setText(DescAction);
                        tvTitle.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    public void showFancyVietnameTwentyOne(final String showCaseId,
                                           String title,
                                        final String DescAction,
                                        final ShowCaseListener.ActionListener listener) {

        boolean isShown = getPreference().getBoolean(showCaseId, false);
        if (!mActivity.isFinishing() && !isShown) {
        if (fancyView == null) {
            fancyView = new FancyShowCaseView.Builder(mActivity)
                    .roundRectRadius(90)
                    .focusBorderSize(10)
                    .closeOnTouch(false)
                    .focusBorderColor(mActivity.getResources().getColor(R.color.orange))
                    .focusShape(FocusShape.ROUNDED_RECTANGLE)
                    .customView(R.layout.case_view_vietnames_center, view -> {

                        ImageView imageView = view.findViewById(R.id.imgtutor1);
                        TextView tvTitle = view.findViewById(R.id.judul);
                        TextView tvDesc = view.findViewById(R.id.textView);
                        Button btnOke = view.findViewById(R.id.btnOK);

                        tvTitle.setTextColor(mActivity.getResources().getColor(android.R.color.black));
                        imageView.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.character_last_));
                        tvDesc.setText(DescAction);
                        tvTitle.setText(title);
                        btnOke.setVisibility(View.GONE);

                        view.setOnClickListener(view1 -> {
                            removeView();
                            listener.onScreenTap();
                        });

                    }).build();
        }
        if (fancyView.isShown()) {
            return;
        }
        fancyView.show();
        getPreference().edit().putBoolean(showCaseId, true).apply();
        }
    }

    private void removeView() {
        if (fancyView != null) {
            fancyView.removeView();
            fancyView = null;
        }
    }

    public SharedPreferences getPreference() {
        return mActivity.getSharedPreferences("showcase_preference", Context.MODE_PRIVATE);
    }

    private void initMediaPlayer(final View root) {

        SharedPreferences sharedPreferences = mActivity.getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        urlBy = sharedPreferences.getString(ConfigPref.URL_BUY, "");
        urlBy = urlBy.replaceAll(" ", "%20");

        mVideoView = root.findViewById(R.id.video_view);
        mediaControllerAnchor = root.findViewById(R.id.controller_anchor);

        if (!mActivity.isFinishing()) {
            mediaController = new MediaController(mActivity);
            mediaController.setAnchorView(mediaControllerAnchor);
            mVideoView.setMediaController(mediaController);
            mVideoView.setZOrderOnTop(true);

            ContactItem contactItem = SessionManager.getProfile(mActivity);
            if(contactItem == null)
                return;

            if (setCont) {
                if (mActivity.isFinishing()) {
                    try {
                        Uri uris = Uri.parse(urlBy);
                        mVideoView.setVideoURI(uris);
                        mVideoView.seekTo(100);
                        mVideoView.start();
                        setCont = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {

                if (!mActivity.isFinishing()) {
                    if (OwnVideoManager.isOwnVideoExist(mActivity)) {
                        mActivity.runOnUiThread(() -> {
                            if (!mActivity.isFinishing()) {
                                try {
                                    Uri ownVideoUri = OwnVideoManager.getOwnVideoUri(mActivity);
                                    mVideoView.setVideoURI(ownVideoUri);
                                    mVideoView.seekTo(100);
                                    mVideoView.start();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } else if (OwnVideoManager.collectionExist(mActivity)) {

                        mActivity.runOnUiThread(() -> {
                            if (!mActivity.isFinishing()) {
                                try {
                                    Uri uri = OwnVideoManager.getCollectionUri(mActivity);
                                    mVideoView.setVideoURI(uri);
                                    mVideoView.seekTo(100);
                                    mVideoView.start();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
            mVideoView.setOnPreparedListener(mediaPlayer -> mediaPlayer.setLooping(false));
            mVideoView.setOnErrorListener((mediaPlayer, i, i1) -> true);
        }
    }
}
