package com.sbimv.mvicalls.util.video_compress;

import android.text.TextUtils;

public class GeneralUtils {

    public static String getDurationFromVCLogRandomAccess(String line) {
        String duration = null;
        int i1 = line.indexOf("Duration:");
        int i2 = line.indexOf(", start");

        if (i1 != -1 && i2 != -1) {
            duration = line.substring(i1 + 10, i2);
        }

        return duration;
    }

    public static String readLastTimeFromVKLogUsingRandomAccess(String line) {
        String timeStr = "00:00:00.00";
        int i1 = line.indexOf("time=");
        int i2 = line.indexOf("bitrate=");

        if (i1 != -1 && i2 != -1) {
            timeStr = line.substring(i1 + 5, i2 - 1);
        } else if (line.startsWith("video:")) {
            timeStr = "exit";
        }

        return timeStr.trim();

    }

    public static boolean isEmpty(String str) {
        return TextUtils.isEmpty(str) || str.equals("null");
    }

}
