package com.sbimv.mvicalls.util;

public class ShowCaseListener {

    public interface NextListener {
        void onNext();
    }

    public interface InviteListener{
        void onFinish();
        void onInvite();
    }

    public interface IntroListener {
        void onOk();
        void onCancel();
    }
    public interface ActionListener{
        void onScreenTap();
        void onOk();
        void onSkip();
    }

    public interface ActionsListener{
        void onScreenTap(String name);
        void onOk(String name);
        void onSkip(String name);
    }

    public interface ActionsListenerEditText{
        void onScreenTap(String name, boolean isSuccess);
        void onOk(String name,  boolean isSuccess);
        void onSkip(String name,  boolean isSuccess);
    }
}
