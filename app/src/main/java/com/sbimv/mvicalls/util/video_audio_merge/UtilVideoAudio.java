package com.sbimv.mvicalls.util.video_audio_merge;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import java.io.File;

public class UtilVideoAudio {

    StringVideo stringVideo = new StringVideo();

    public void refreshGallery(String path, Context context){
        File file = new File(path);

        try{
            Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
            Uri contentUri = Uri.fromFile(file);
            mediaScanIntent.setData(contentUri);
            context.sendBroadcast(mediaScanIntent);
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }

    public void refreshGallery(Context context){
        try{
            Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
            context.sendBroadcast(mediaScanIntent);
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }

    File getConvertedFile(String folder, String fileName){
        File f = new File(folder);
        if (!f.exists()){
            f.mkdirs();
        }
        return new File(f.getPath() + File.separator + fileName);
    }

    public String getOutputPath() {
        String path = Environment.getExternalStorageDirectory().toString() + File.separator + stringVideo.APP_FOLDER + File.separator;
        File folder = new File(path);
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return path;
    }

}
