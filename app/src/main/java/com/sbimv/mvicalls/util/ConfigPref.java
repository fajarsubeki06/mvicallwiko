package com.sbimv.mvicalls.util;

public class ConfigPref {
    public static final String SHARED_PREF_NAME = "myloginapp";
    public static final String URL_BUY = "buy";
    public static final String SET_CONTENT = "set";
    // Firebase Push Notification
//    public static final String TOPIC_GLOBAL = "global";
    public static final String TOPIC_NEWS = "news";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String AUTO_LOGOUT = "auto_logout";
    public static final int NOTIFICATION_ID = 200;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 201;
    public static final String SHARED_PREF = "ah_firebase";

}
