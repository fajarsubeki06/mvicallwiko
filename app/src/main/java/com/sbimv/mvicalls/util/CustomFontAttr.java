package com.sbimv.mvicalls.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.sbimv.mvicalls.R;

/**
 * Created by Hendi on 3/5/16.
 */
public class CustomFontAttr {
    // font style
    public static final int normal = 0;
    public static final int normal_italic = 1;
    public static final int semibold = 2;
    public static final int semibold_italic = 3;
    public static final int bold = 4;
    public static final int bold_italic = 5;

    public static String getSelectedFont(Context context, AttributeSet attrs){
        TypedArray arr = context.obtainStyledAttributes(attrs, R.styleable.CustomFontAttr);

        // uncomment if more than one font family
        //int fontFamily = arr.getInt(R.styleable.CustomFontAttr_fontFamily, CustomFontAttr.circular_std);
        int fontStyle = arr.getInt(R.styleable.CustomFontAttr_gayaFont, CustomFontAttr.normal);
        String fontName = "futura_";

        switch (fontStyle){
            case CustomFontAttr.normal:
                fontName = fontName+"normal.ttf";
                break;
            case CustomFontAttr.normal_italic:
                fontName = fontName+"normal_italic.ttf";
                break;
            case CustomFontAttr.semibold:
                fontName = fontName+"semibold.ttf";
                break;
            case CustomFontAttr.semibold_italic:
                fontName = fontName+"semibold_italic.ttf";
                break;
            case CustomFontAttr.bold:
                fontName = fontName+"bold.ttf";
                break;
            case CustomFontAttr.bold_italic:
                fontName = fontName+"bold_italic.ttf";
                break;
        }

        arr.recycle();
        return fontName;
    }
}
