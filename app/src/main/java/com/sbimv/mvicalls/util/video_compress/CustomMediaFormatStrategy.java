package com.sbimv.mvicalls.util.video_compress;

import android.media.MediaCodecInfo;
import android.media.MediaFormat;


public class CustomMediaFormatStrategy  {
    //implements MediaFormatStrategy
    public static final int AUDIO_BITRATE_AS_IS = -1;
    public static final int AUDIO_CHANNELS_AS_IS = -1;
    //private static final int DEFAULT_VIDEO_BITRATE = 2500 * 1000;
    private static final int DEFAULT_VIDEO_BITRATE = 1400 * 1000;
    private final int mVideoBitrate;
    private final int mAudioBitrate;
    private final int mAudioChannels;

    public CustomMediaFormatStrategy() {
        this(DEFAULT_VIDEO_BITRATE);
    }

    public CustomMediaFormatStrategy(int videoBitrate) {
        this(videoBitrate, AUDIO_BITRATE_AS_IS, AUDIO_CHANNELS_AS_IS);
    }

    public CustomMediaFormatStrategy(int videoBitrate, int audioBitrate, int audioChannels) {
        mVideoBitrate = videoBitrate;
        mAudioBitrate = audioBitrate;
        mAudioChannels = audioChannels;
    }

//    @Override
//    public MediaFormat createVideoOutputFormat(MediaFormat inputFormat) {
//        int width = inputFormat.getInteger(MediaFormat.KEY_WIDTH);
//        int height = inputFormat.getInteger(MediaFormat.KEY_HEIGHT);
//        int outWidth , outHeight;
//        //int max = 1280;
//        int max = 480;
//
//        // landscape
//        if (width > height){
//            if(width > max){
//                double div = (double)max / width;
//                outWidth = (int)(div * width);
//                outHeight = (int)(div * height);
//            }else
//                throw new NoCompressionException();
//        }
//        // portrait
//        else{
//            if(height > max){
//                double div = (double)max / height;
//                outWidth = (int)(div * width);
//                outHeight = (int)(div * height);
//            }else
//                throw new NoCompressionException();
//        }
//
//        MediaFormat format = MediaFormat.createVideoFormat("video/avc", outWidth, outHeight);
//        // From Nexus 4 Camera in 720p
//        format.setInteger(MediaFormat.KEY_BIT_RATE, mVideoBitrate);
//        format.setInteger(MediaFormat.KEY_FRAME_RATE, 30);
//        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 3);
//        format.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
//        return format;
//    }

//    @Override
//    public MediaFormat createAudioOutputFormat(MediaFormat inputFormat) {
//        if (mAudioBitrate == AUDIO_BITRATE_AS_IS || mAudioChannels == AUDIO_CHANNELS_AS_IS) return null;
//
//        // Use original sample rate, as resampling is not supported yet.
//        final MediaFormat format = MediaFormat.createAudioFormat(MediaFormatExtraConstants.MIMETYPE_AUDIO_AAC,
//                inputFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE), mAudioChannels);
//        format.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
//        format.setInteger(MediaFormat.KEY_BIT_RATE, mAudioBitrate);
//        return format;
//    }
}
