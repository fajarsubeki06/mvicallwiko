package com.sbimv.mvicalls.util.video_audio_merge;

import java.io.File;

public interface VideoAudioCallBack {

    void onProgress(String progress);

    void onSuccess(File file, String success);

    void onFailure(Exception exception);

    void onNotAvailable(Exception notAvailable);

    void onFinish();

}
