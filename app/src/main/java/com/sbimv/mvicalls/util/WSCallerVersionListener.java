package com.sbimv.mvicalls.util;

public interface WSCallerVersionListener {
    void onGetResponse(boolean isUpdateAvailable);
}
