package com.sbimv.mvicalls.util;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sbimv.mvicalls.pojo.DataChargingContent;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class LanguageHolder {

    public static void saveChargeArrayList(Context context, ArrayList<DataChargingContent> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        PreferenceUtil.getEditor(context).putString(PreferenceUtil.CHARGGING, json).commit();
    }


    public static ArrayList<DataChargingContent> getChargeArrayList(Context context) {
        String json = PreferenceUtil.getPref(context).getString(PreferenceUtil.CHARGGING, null);
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<DataChargingContent>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

}
