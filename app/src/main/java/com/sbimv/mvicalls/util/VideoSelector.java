package com.sbimv.mvicalls.util;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.widget.MediaController;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.activity.PopupVietSubsActivity;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;

import java.util.List;

/**
 * Created by Hendi on 12/18/2018.
 */
public class VideoSelector {

    final int REQUEST_CAPTURE = 1324;
    final int REQUEST_GALLERY = 2435;
    private LangViewModel model = LangViewModel.getInstance();

    final String [] galleryPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    final String [] cameraPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    Activity mActivity;
    VideoSelectorDelegate mDelegate;
    int mAction = 0;

    public static VideoSelector create(Activity activity, VideoSelectorDelegate delegate){
        return new VideoSelector(activity, delegate);
    }

    private VideoSelector(Activity activity, VideoSelectorDelegate delegate){
        this.mActivity = activity;
        this.mDelegate = delegate;
    }

    private void showSettingsDialog() {

        if (mActivity != null) {
            if (!mActivity.isFinishing()) {

                String sTitle = TextUtils.isEmpty(model.getLang().string.titlePermissionAlert)?"":model.getLang().string.titlePermissionAlert;
                String sMessage = TextUtils.isEmpty(model.getLang().string.bodyPermissionAlert)?"":model.getLang().string.bodyPermissionAlert;
                String sButton = TextUtils.isEmpty(model.getLang().string.buttonPermissionAlert)?"":model.getLang().string.buttonPermissionAlert;


                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity);
                builder.setTitle(sTitle);
                builder.setMessage(sMessage);
                builder.setCancelable(false);
                builder.setPositiveButton(sButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        openSettings();
                    }
                });
                builder.show();
            }
        }

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package", mActivity.getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mActivity.startActivity(intent);
    }

    public void gallery(){
        mAction = 0;

        Dexter.withActivity(mActivity).withPermissions(galleryPermissions).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    Intent pickerIntent = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.INTERNAL_CONTENT_URI);
//                    pickerIntent.setAction(Intent.ACTION_GET_CONTENT);
//                    pickerIntent.addCategory(Intent.CATEGORY_OPENABLE);
                    pickerIntent.setTypeAndNormalize("video/*");
                    mActivity.startActivityForResult(pickerIntent, REQUEST_GALLERY);
                } else if (!report.getDeniedPermissionResponses().isEmpty()) {
                    showSettingsDialog();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
            }
        }).onSameThread().check();
    }

    public void capture(){
        mAction = 1;

        Dexter.withActivity(mActivity).withPermissions(cameraPermissions).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(mActivity.getPackageManager()) != null) {
                        mActivity.startActivityForResult(takeVideoIntent, REQUEST_CAPTURE);
                    }
                } else if (!report.getDeniedPermissionResponses().isEmpty()) {
                    showSettingsDialog();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
            }
        }).onSameThread().check();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (data == null) {
            return;
        }

        if(requestCode == REQUEST_CAPTURE && resultCode == Activity.RESULT_CANCELED){
            mDelegate.onCancelCapture();
            String ccode = PreferenceUtil.getPref(mActivity).getString(PreferenceUtil.COUNTRY_CODE, "1");
//            if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
//                BaseActivity.showCaseIntroVietEleven(mActivity);
//                return;
//            }
            return;
        }

        if (requestCode == REQUEST_GALLERY && resultCode == Activity.RESULT_CANCELED){
            mDelegate.onCancelGallery();
            //BaseActivity.showCaseIntroVietEleven(mActivity);
            return;
        }

        if(requestCode == REQUEST_CAPTURE && resultCode == Activity.RESULT_OK){
            // send callback
            if (data.getData() != null){
                mDelegate.onCaptured(data.getData());
            }
            //BaseActivity.showCaseIntroVietEleven(mActivity);
            return;
        }

        if(requestCode == REQUEST_GALLERY && resultCode == Activity.RESULT_OK){
            // send callback
            if (data.getData() != null){
                mDelegate.onSelected(data.getData());
            }
            //BaseActivity.showCaseIntroVietEleven(mActivity);
            return;
        }
        // TODO show error message
    }

    public interface VideoSelectorDelegate {
        void onCaptured(Uri uri);
        void onSelected(Uri uri);
        void onCancelCapture();
        void onCancelGallery();
    }
}
