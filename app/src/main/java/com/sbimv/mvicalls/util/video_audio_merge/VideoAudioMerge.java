package com.sbimv.mvicalls.util.video_audio_merge;

import android.content.Context;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

public class VideoAudioMerge {

    private File audio;
    private File video;
    private VideoAudioCallBack videoAudioCallBack;
    private FFmpeg fFmpeg;
    private String outputPath;
    private String outputFileName;
    private Context context;
    private UtilVideoAudio utils = new UtilVideoAudio();
    private StringVideo stringVideo = new StringVideo();

    private static final String TAG = "AudioVideoMerger";

    public VideoAudioMerge(Context context) {
        this.context = context;
    }

    public VideoAudioMerge setAudioFile(File audio) {
        this.audio = audio;
        return this;
    }

    public VideoAudioMerge setVideoFile(File video) {
        this.video = video;
        return this;
    }

    public VideoAudioMerge setVideoAudioCallBack(VideoAudioCallBack videoAudioCallBack) {
        this.videoAudioCallBack = videoAudioCallBack;
        return this;
    }

    public VideoAudioMerge setOutputPath(String outputPath) {
        this.outputPath = outputPath;
        return this;
    }

    public VideoAudioMerge setOutputFileName(String outputFileName) {
        this.outputFileName = outputFileName;
        return this;
    }

    public void merge() {
        fFmpeg = FFmpeg.getInstance(context);
        try{
            fFmpeg.loadBinary(new LoadBinaryResponseHandler(){
                @Override
                public void onStart() {
                    super.onStart();
                }

                @Override
                public void onFailure() {
                    super.onFailure();
                }

                @Override
                public void onSuccess() {
                    super.onSuccess();
                    executeFFmpeg();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                }
            });
        }catch (FFmpegNotSupportedException e){
            e.printStackTrace();
            Log.e("ERROR", String.valueOf(e));
        }
    }

    private void executeFFmpeg(){

        if (audio == null || !audio.exists() || video == null || !video.exists()) {
            videoAudioCallBack.onFailure(new IOException("File not exists"));
            return;
        }
        if (!audio.canRead() || !video.canRead()) {
            videoAudioCallBack.onFailure(new IOException("Can't read the file. Missing permission?"));
            return;
        }

        File outputLocation = utils.getConvertedFile(outputPath, outputFileName);

        //        String[] cmd = new String[]{"-y", "-i", video.getPath(), "-i", audio.getPath(), "-af", "apad", "-map", "0:v", "-map", "1:a", "-c:v", "copy", "-shortest", outputLocation.getPath()};

        String[] cmd = new String[]{
                "-y","-i",video.getPath(), "-filter_complex",
                "amovie=" + audio.getPath() + ":loop=999,asetpts=N/SR/TB,aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=1.5[a1];" +
                        "[0:a]aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=0.0[a2];" +
                        " [a1][a2]amerge,pan=stereo:c0<c0+c2:c1<c1+c3[out]",
                "-map","0:v","-map","[out]","-c:v","copy","-c:a","aac","-shortest","-preset", "ultrafast", outputLocation.getPath()
        };

        try{
            fFmpeg.execute(cmd, new ExecuteBinaryResponseHandler(){
                @Override
                public void onStart() {
                    super.onStart();
                }

                @Override
                public void onProgress(String message) {
                    super.onProgress(message);
                    videoAudioCallBack.onProgress(message);
                }

                @Override
                public void onFailure(String message) {
                    super.onFailure(message);
                    if (outputLocation.exists()){
                        outputLocation.delete();
                    }
                    videoAudioCallBack.onFailure(new IOException(message));
                }

                @Override
                public void onSuccess(String message) {
                    super.onSuccess(message);
                    utils.refreshGallery(outputLocation.getPath(), context);
                    videoAudioCallBack.onSuccess(outputLocation, stringVideo.TYPE_VIDEO);
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    videoAudioCallBack.onFinish();
                }
            });
        }catch (FFmpegCommandAlreadyRunningException e){
            e.printStackTrace();
            videoAudioCallBack.onNotAvailable(e);
        }
    }

    public static final class Companion{

        @NotNull
        public final String getTAG() {
            return VideoAudioMerge.TAG;
        }

        @NotNull
        public static VideoAudioMerge with(@NotNull Context context) {

            return new VideoAudioMerge(context);
        }

    }
}
