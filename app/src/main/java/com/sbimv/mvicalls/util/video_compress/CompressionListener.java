package com.sbimv.mvicalls.util.video_compress;

public interface CompressionListener {

    void compressionFinished(int status, boolean isVideo, String fileOutputPath);

    void onFailure(String message);

    void onProgress(int progress);

}
