package com.sbimv.mvicalls.util;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import java.io.File;

/**
 * Created by Yudha on 10/30/17.
 */

public class CallerVideoManager {

    static final String DIR_NAME = "mvicall_video";
    static final String DIS_NAME = "video_profile";

    public static Uri getVideoUri(Context context, String video_url){
        if(video_url == null)
            return null;

        String fileName = generateFileName(video_url);
        String path = getDirPath(context) + File.separator + fileName;
        return Uri.parse(path);
    }

    public static String getDirPath(Context context){
        return context.getFilesDir().getPath() + File.separator + DIR_NAME;
    }

    public static String generateFileName(String videoUrl){
        return videoUrl.replace("/", "^");
    }

    public static String generateUniversalName(String videoUrl){
        return videoUrl.replace("/", "^");
    }

    public static String getDisPath(Context context){
        return context.getFilesDir().getPath() + File.separator + DIS_NAME;
    }

}
