package com.sbimv.mvicalls.util;

import android.text.TextUtils;
import android.widget.EditText;

import com.sbimv.mvicalls.language.LangViewModel;

import java.util.regex.Pattern;

public class ValidationInputUtil {


    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REGEX = "\\d{3}-\\d{7}";
    private static final String EMAIL_MSG = "invalid email";
    private static final String PHONE_MSG = "Invalid phone number";

    public static boolean isEmailAddress(EditText editText, boolean required) {
        return isValid(editText, EMAIL_REGEX, EMAIL_MSG, required);
    }

    public static boolean isPhoneNumber(EditText editText, boolean required) {
        return isValid(editText, PHONE_REGEX, PHONE_MSG, required);
    }

    public static boolean isValid(EditText editText, String regex, String errMsg, boolean required) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        if ( required && !hasText(editText) ) return false;

        if (required && !Pattern.matches(regex, text)) {
            editText.setError(errMsg);
            return false;
        }

        return true;
    }

    public static boolean hasText(EditText editText) {
        LangViewModel model = LangViewModel.getInstance();
        String be = TextUtils.isEmpty(model.getLang().string.str_set_up_profile_not_empty)
                ? "Cannot be Empty"
                :  model.getLang().string.str_set_up_profile_not_empty;
        String text = editText.getText().toString().trim();
        editText.setError(null);

        if (text.length() == 0 || text.contains("\\s")) {
            editText.setError(be);
            return false;
        }

        return true;
    }
}
