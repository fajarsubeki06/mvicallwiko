package com.sbimv.mvicalls.util.video_compress;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.sbimv.mvicalls.util.CallerVideoManager;


import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Future;

/**
 * Created by Hendi on 5/31/17.
 */

public class VideoCompressor {

    String mInputPath;
    String mOutputPath;
    Future<Void> mFuture;
    Context mAppContext;
    static VideoCompressor mCompressor;

    //FFmpeg
    public static final int SUCCESS = 1;
    public static final int FAILED = 2;
    public static final int NONE = 3;
    public static final int RUNNING = 4;

    private final ProgressCalculator mProgressCalculator;
    private boolean isFinished;
    private int status = NONE;
    private String errorMessage = "Compression Failed!";
    private FFmpeg fFmpeg;
    private String inputPath;
    private String outputPath;
    private String setRatio;
    private com.sbimv.mvicalls.util.video_compress.CompressionListener compressionListener;
    private static final String TAG = "VideoCompressor";
    private String h = "";
    private String w = "";

    public static VideoCompressor getInstance(Context appContext){
        if(mCompressor == null)
            mCompressor = new VideoCompressor(appContext);
        return mCompressor;
    }

    public VideoCompressor(Context appContext){
        this.mAppContext = appContext;
        mProgressCalculator = new ProgressCalculator();
    }

//    public void transcode(String inputPath, String outputPath, final Listener listener){
//        this.mInputPath = inputPath;
//        this.mOutputPath = outputPath;
//
//        try {
//            mFuture = MediaTranscoder.getInstance().transcodeVideo(
//                    inputPath,
//                    outputPath,
//                    new CustomMediaFormatStrategy(),
//                    new MediaTranscoder.Listener() {
//                        @Override
//                        public void onTranscodeProgress(double progress) {
//                            int percent = (int)(progress * 100.00);
//                            listener.onTranscodeProgress(progress, percent);
//                        }
//
//                        @Override
//                        public void onTranscodeCompleted() {
//                            listener.onTranscodeCompleted(mOutputPath);
//                        }
//
//                        @Override
//                        public void onTranscodeCanceled() {
//                            listener.onTranscodeCanceled();
//                        }
//
//                        @Override
//                        public void onTranscodeFailed(Exception exception) {
//                            if(exception instanceof NoCompressionException){
//                                listener.onTranscodeSkipped();
//                            }
//                            else if(exception instanceof OutputFormatUnavailableException){
//                                listener.onTranscodeFailed(exception, "Unsupport video size");
//                            }
//                            else if(exception instanceof IllegalArgumentException){
//                                listener.onTranscodeSkipped();
//                            }
//                            else
//                                listener.onTranscodeFailed(exception, "Compress video failed");
//                        }
//
//
//                    }
//            );
//        } catch (IOException e) {
//            listener.onTranscodeFailed(e, "Compress video failed");
//        }
//    }
//
//    public void cancel(boolean mayInterruptIfRunning){
//        if(mFuture != null)
//            mFuture.cancel(mayInterruptIfRunning);
//    }
//
//    public interface Listener {
//        void onTranscodeProgress(double progress, int percent);
//        void onTranscodeCompleted(String ouputPath);
//        void onTranscodeCanceled();
//        void onTranscodeFailed(Exception exception, String localeMessage);
//        void onTranscodeSkipped();
//    }

    //FFmpeg

    public VideoCompressor setOutputPath(String outputPath) {
        this.outputPath = outputPath;
        //this.setRatio = setRatioPath;
        return this;
    }

    public VideoCompressor setInputPath(String inputPath) {
        this.inputPath = inputPath;
        return this;
    }

    public VideoCompressor setCallBack(com.sbimv.mvicalls.util.video_compress.CompressionListener compressionListener) {
        this.compressionListener = compressionListener;
        return this;
    }

    public void executeFFmpeg(String w, String h) {
        if (inputPath == null || inputPath.isEmpty()) {
            status = NONE;
            if (compressionListener != null) {
                compressionListener.compressionFinished(NONE, false, null);
            }
            return;
        }


//        try{
//            Uri uri  = CallerVideoManager.getVideoUri(mAppContext, inputPath);
//            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//            retriever.setDataSource(String.valueOf(uri));
//            h = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
//            w = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
//        }catch (Exception e){
//            e.printStackTrace();
//        }

        String outputPath2 = outputPath;
        String[] commandParams = new String[27];
        commandParams[0] = "-y";
        commandParams[1] = "-noautorotate";
        commandParams[2] = "-i";
        commandParams[3] = inputPath;
        commandParams[4] = "-s";
        commandParams[5] = "" + w + "x" + h + "";
        commandParams[6] = "-r";
        commandParams[7] = "20";
        commandParams[8] = "-c:v";
        commandParams[9] = "libx264";
        commandParams[10] = "-preset";
        commandParams[11] = "ultrafast";
        commandParams[12] = "-c:a";
        commandParams[13] = "copy";
        commandParams[14] = "-me_method";
        commandParams[15] = "zero";
        commandParams[16] = "-tune";
        commandParams[17] = "fastdecode";
        commandParams[18] = "-tune";
        commandParams[19] = "zerolatency";
        commandParams[20] = "-strict";
        commandParams[21] = "-2";
        commandParams[22] = "-b:v";
        commandParams[23] = "1000k";
        commandParams[24] = "-pix_fmt";
        commandParams[25] = "yuv420p";
        commandParams[26] = outputPath2;

        try{
            fFmpeg.execute(commandParams, new ExecuteBinaryResponseHandler(){
                @Override
                public void onSuccess(String message) {
                    super.onSuccess(message);
                }

                @Override
                public void onProgress(String message) {
                    status = RUNNING;
                    Log.e("VideoCronProgress", message);
                    int progress = mProgressCalculator.calcProgress(message);
                    Log.e("VideoCronProgress == ", progress + "..");
                    if (progress != 0 && progress <= 100) {
                        if (progress >= 99) {
                            progress = 100;
                        }
                        compressionListener.onProgress(progress);
                    }
                }

                @Override
                public void onFailure(String message) {
                    status = FAILED;
                    Log.e("VideoCompressor", message);
                    if (compressionListener != null) {
                        compressionListener.onFailure("Error : " + message);
                    }
                }

                @Override
                public void onStart() {
                    super.onStart();
                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    Log.e("VideoCronProgress", "finnished");
                    isFinished = true;
                    if (compressionListener != null) {
                        compressionListener.compressionFinished(status, true, outputPath2);
                    }
                }
            });
        }catch (FFmpegCommandAlreadyRunningException e){
            status = FAILED;
            errorMessage = e.getMessage();
            if (compressionListener != null) {
                compressionListener.onFailure("Error : " + e.getMessage());
            }
        }

    }

    public void compressVideo(String w, String h) {

            fFmpeg = FFmpeg.getInstance(mAppContext);
            try{
                fFmpeg.loadBinary(new LoadBinaryResponseHandler(){
                    @Override
                    public void onFailure() {
                        super.onFailure();
                        status = FAILED;
                    }

                    @Override
                    public void onSuccess() {
                        super.onSuccess();
                        status = SUCCESS;
                        executeFFmpeg(w, h);
                    }

                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        isFinished = true;

                    }
                });
            }catch (FFmpegNotSupportedException e){
                e.printStackTrace();
                Log.e("ERROR", String.valueOf(e));
            }

    }

    public static final class Companion{

        @NotNull
        public final String getTAG() {
            return VideoCompressor.TAG;
        }

        @NotNull
        public static VideoCompressor with(@NotNull Context context) {

            return new VideoCompressor(context);
        }

    }

    public boolean isDone() {
        return status == SUCCESS || status == NONE;
    }
}
