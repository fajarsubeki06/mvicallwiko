package com.sbimv.mvicalls.util;

import android.content.Context;
import android.net.Uri;

import java.io.File;

public class CallerVideoProfile {
    static final String DIR_NAME = "mvicall_current_video";

    public static Uri getVideoUri(Context context, String video_url){
        if(video_url == null)
            return null;

        String fileName = generateFileName(video_url);
        String path = getDirPath(context) + File.separator + fileName;
        return Uri.parse(path);
    }

    public static String getDirPath(Context context){
        return context.getFilesDir().getPath() + File.separator + DIR_NAME;
    }

    public static String generateFileName(String videoUrl){
        return videoUrl.replace("/", "^");
    }
}
