package com.sbimv.mvicalls.util;

import android.content.ComponentName;
import android.content.Intent;

public final class ProtectPermission {
    public ProtectPermission() {
        throw new RuntimeException("Stub!");
    }

    public static final class permission {

        //Startup Manager Xiaomi
        public static final Intent INTENT_XIAOMI = new Intent().setComponent(new ComponentName(
                "com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
        //Floating Windows Xiaomi
        public static final Intent INTENT_XIAOMI_FOATING1 = new Intent().setComponent(new ComponentName(
                "com.miui.securitycenter", "com.miui.securitycenter.permission.AppPermissionsEditor"));
        public static final Intent INTENT_XIAOMI_FOATING2 = new Intent().setComponent(new ComponentName(
                "com.miui.securitycenter", "com.miui.permcenter.permissions.AppPermissionsEditorActivity"));
        public static final Intent INTENT_XIAOMI_FOATING3 = new Intent().setComponent(new ComponentName(
                "com.android.settings", "com.miui.securitycenter.permission.AppPermissionsEditor"));

        public static final Intent INTENT_LETV = new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"));
        public static final Intent INTENT_HUAWEI = new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity"));
        public static final Intent INTENT_COLOROS = new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
        public static final Intent INTENT_IQOO = new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity"));
        public static final Intent INTENT_IQOO_STARTUP = new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager"));
        public static final Intent INTENT_VIVO = new Intent().setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"));
        public static final Intent INTENT_ASUS = new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.MainActivity"));

        //Starup Manager OPPO
        public static final Intent INTENT_OPPO1 = new Intent().setComponent(
                new ComponentName("com.oppo.safe", "com.oppo.safe.permission.startup.StartupAppListActivity"));
        public static final Intent INTENT_OPPO2 = new Intent().setComponent(
                new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity"));
        public static final Intent INTENT_OPPO3 = new Intent().setComponent(
                new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.startupapp.StartupAppListActivity"));

        //Floating Windows OPPO
        public static final Intent INTENT_FLOATING_OPPO1 = new Intent().setComponent(
                new ComponentName("com.oppo.safe", "com.oppo.safe.permission.floatwindow.FloatWindowListActivity"));
        public static final Intent INTENT_FLOATING_OPPO2 = new Intent().setComponent(
                new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.floatwindow.FloatWindowListActivity"));
        public static final Intent INTENT_FLOATING_OPPO3 = new Intent().setComponent(
                new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.sysfloatwindow.FloatWindowListActivity"));

    }
}
