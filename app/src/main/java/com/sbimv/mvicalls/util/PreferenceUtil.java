package com.sbimv.mvicalls.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Hendi on 10/30/17.
 */

public class PreferenceUtil {

    private static PreferenceUtil instance;
    private Context context;

    private PreferenceUtil(Context context) {
        this.context = context;
    }

    public static PreferenceUtil init(Context context){
        if (instance == null){
            instance = new PreferenceUtil(context);
        }
        return instance;
    }

    public static PreferenceUtil getInstance(){
        return instance;
    }

    public static final String NAME = "mvicall_preference";
    public static final String VIDEO_TONE_ENABLE = "video_tone_enable";
    public static final String VIDEO_TONE_DISABLE = "video_tone_disable";
    public static final String PROTECT_APPS = "protect";
    public static final String SYNC_LOADING = "sync_loading";
    public static final String SYNC_EXPERIENCE = "sync_experience";
    public static final String STATUS_SET = "status_set";
    public static final String INTRO = "intro";
    public static final String INTRO_ACTIVITY_FIRST = "intro_first";
    public static final String INTRO_ACTIVITY_SECOND = "intro_second";
    public static final String SKIP_INVITE = "skip_invite";
    public static final String PROTECTED = "protected";
    public static final String DIRECTION = "direction";
    public static final String DIRECT_TO_PAGE = "bestdirect";
    public static final String NEWUSER = "newuser";
    public static final String ISLOGIN = "islogin";
    public static final String ISWITCH = "iswitch";
    public static final String CHARGGING = "charge";
    public static final String LASTGET = "lastget";
    public static final String FIRST_SYNC_VIDEO_CONTENT = "first_sync_video_content";
    public static final String FIRST_SYNC_DEFAULT_VIDEO = "first_sync_default_video";
    public static final String SHOW_CASE_UPLOAD_VIDEO = "scUpload";
    public static final String DYNAMIC_LINK = "dynamic";
    public static final String GRAB_FREE_CONTENT = "free_content";
    public static final String FROM_CAMERA = "camera";
    public static final String FROM_GALLERY = "gallery";
    public static final String TSEL_BUY = "tsel";
    public static final String FIRST_OPEN_APP = "first_open";
    public static final String REQ_CODE = "req_code";
    public static final String POINT_REWARDS = "point-rewards";
    public static final String IS_REGISTERED = "already_register";
    public static final String FINISH_SCASE = "registered";
    public static final String ISREGISTERED = "registered";
    public static final String TOKEN_FCM = "fcmtoken";
    public static final String SAVED_LANG_KEY = "saved_lang_key";
    public static final String SAVED_LANG_VALUE = "saved_lang_value";
    public static final String COUNTRY_CODE = "country_code";
//    public static final String COUNTRY_CODE_PERU = "peru";
    public static final String COUNTRY_NAME_CODE = "country_name_code";
    public static final String COUNTRY_NAME = "country_name";
    public static final String COUNTRY = "country";
    public static final String BASE_URL = "base_url";

    public static final String FINAL_SCASE = "final_scase";

    public static final String SC_STATUS = "sc_status";
    public static final String APP_VERSION = "app_version";

    public static final String SETUP_PROFILE = "setup_profile";
    public static final String SETUP_CONTACT = "setup_contact";
    public static final String SETUP_SHARE = "setup_share";
    public static final String SETUP_VIDEOTONE = "setup_videotone";
    public static final String SHARE_ACTION = "share_action";
    public static final String DEMO_VIDEO = "demo_video";
    public static final String SHOW_CALL_DEMO = "call_demo";
    public static final String SHOW_CASE_DEMO_PROFILE = "show_case_demo_profile";
    public static final String SHOW_CASE_DEMO_SHARE = "show_case_demo_share";
    public static final String SHOW_CASE_FRIEND = "show_case_friend";
    public static final String SHOW_CASE_FRIEND2 = "show_case_friend2";
    public static final String SHOW_CASE_SETUP = "show_case_setup";
    public static final String SHOW_CASE_SETUP2 = "show_case_setup2";
    public static final String SHOW_CASE_MAIN = "show_case_main";
    public static final String ACTION_SAVE_PROFILE = "action_save_profile";
    public static final String LANG_MUST_UPDATE = "lang_must_update";

    public static final String BY_PASS = "baypass";
    public static final String BY_PASS_NUMBER = "num";

    public static final String SOURCE_DEEPLINK = "source";
    public static final String MSISDN_DEEPLINK = "msidnd";
    public static final String COUNTRY_DEEPLINK = "country";
    public static final String CONTENT_DEEPLINK = "cntid";
    public static final String URL_DEEPLINK = "url_deeplink";
    public static final String POPUP_TNC = "popup_tnc";
    public static final String GET_LANG = "lang";

    public static final String CASE_ONE_VIETNAME = "case_one_viet";
    public static final String CASE_TWO_VIETNAME = "case_two_viet";
    public static final String CASE_THREE_VIETNAME = "case_three_viet";
    public static final String CASE_FOR_VIETNAME = "case_for_viet";
    public static final String CASE_FIV_VIETNAME = "case_five_viet";
    public static final String CASE_SIX_VIETNAME = "case_six_viet";
    public static final String CASE_SEVEN_VIETNAME = "case_seven_viet";
    public static final String CASE_EIGHT_VIETNAME = "case_eight_viet";
    public static final String CASE_NINE_VIETNAME = "case_nine_viet";
    public static final String CASE_TEN_VIETNAME = "case_ten_viet";
    public static final String CASE_ELEVEN_VIETNAME = "case_eleven_viet";
    public static final String CASE_TWELVE_VIETNAME = "case_twelpe_viet";
    public static final String CASE_THIRDTEEN_VIETNAME = "case_thirdteen_viet";
    public static final String CASE_FOURTEEN_VIETNAME = "case_fourteen_viet";
    public static final String CASE_FIFTEEN_VIETNAME = "case_fifteen_viet";
    public static final String CASE_SIXTEEEN_VIETNAME = "case_sixteen_viet";
    public static final String CASE_SEVENTEEEN_VIETNAME = "case_seventeen_viet";
    public static final String CASE_EIGHTTEEEN_VIETNAME = "case_eightteen_viet";
    public static final String CASE_NINETEEEN_VIETNAME = "case_nineteen_viet";
    public static final String CASE_TWENTY_VIETNAME = "case_twenty_viet";
    public static final String CASE_TWENTYONE_VIETNAME = "case_twenone_viet";
    public static final String CASE_RECORD_VIETNAME = "case_record_viet";
    public static final String CASE_VIDEO_VIETNAM = "case_video_viet";
    public static final String CASE_DIALOG = "case_dialog";
    public static final String DIALOG_AUTO_LOGOUT = "dialog_auto_logout";

    public static SharedPreferences getPref(Context context){
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public static SharedPreferences.Editor getEditor(Context context){
       return context.getSharedPreferences(NAME, Context.MODE_PRIVATE).edit();
    }

    // for get base url
    public SharedPreferences getPref(){
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public SharedPreferences.Editor getEditor(){
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE).edit();
    }
}
