package com.sbimv.mvicalls;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.multidex.MultiDex;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustAttribution;
import com.adjust.sdk.AdjustConfig;
import com.adjust.sdk.AdjustEventFailure;
import com.adjust.sdk.AdjustEventSuccess;
import com.adjust.sdk.AdjustSessionFailure;
import com.adjust.sdk.AdjustSessionSuccess;
import com.adjust.sdk.LogLevel;
import com.adjust.sdk.OnAttributionChangedListener;
import com.adjust.sdk.OnDeeplinkResponseListener;
import com.adjust.sdk.OnEventTrackingFailedListener;
import com.adjust.sdk.OnEventTrackingSucceededListener;
import com.adjust.sdk.OnSessionTrackingFailedListener;
import com.adjust.sdk.OnSessionTrackingSucceededListener;
import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.iid.FirebaseInstanceId;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.util.PreferenceUtil;

//import org.solovyev.android.checkout.Billing;


/**
 * Created by Hendi on 14/10/17.
 * Update by Yudha Pratama Putra 07/11/18
 */
public class AppDelegate extends Application {

    public static AppDelegate get(Activity activity) {
        return (AppDelegate) activity.getApplication();
    }

    /*
     * Adjust................
     * */
    private static final String ADJUS_TOKEN = "iuygvnqkjchs";

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(LocaleHelper.onAttach(context, LocaleHelper.getLanguage(context)));
        MultiDex.install(AppDelegate.this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LangViewModel.init(getApplicationContext());
        PreferenceUtil.init(this);

        /*
        * Adjust................
        * */
        String appToken = ADJUS_TOKEN;
        String environment = AdjustConfig.ENVIRONMENT_PRODUCTION;
        AdjustConfig config = new AdjustConfig(this, appToken, environment);
        //config.setAppSecret(23, 1051502373, 1034042558, 950651272, 1421298846);

        config.setLogLevel(LogLevel.VERBOSE);// Change the log   level.

        config.setOnAttributionChangedListener(new OnAttributionChangedListener() { // Set attribution delegate.
            @Override
            public void onAttributionChanged(AdjustAttribution attribution) {
                Log.d("example", "Attribution callback called!");
                Log.d("example", "Attribution: " + attribution.toString());
            }
        });

        config.setOnEventTrackingSucceededListener(new OnEventTrackingSucceededListener() { // Set event success tracking delegate.
            @Override
            public void onFinishedEventTrackingSucceeded(AdjustEventSuccess eventSuccessResponseData) {
                Log.d("example", "Event success callback called!");
                Log.d("example", "Event success data: " + eventSuccessResponseData.toString());
            }
        });

        config.setOnEventTrackingFailedListener(new OnEventTrackingFailedListener() { // Set event failure tracking delegate.
            @Override
            public void onFinishedEventTrackingFailed(AdjustEventFailure eventFailureResponseData) {
                Log.d("example", "Event failure callback called!");
                Log.d("example", "Event failure data: " + eventFailureResponseData.toString());
            }
        });

        config.setOnSessionTrackingSucceededListener(new OnSessionTrackingSucceededListener() { // Set session success tracking delegate.
            @Override
            public void onFinishedSessionTrackingSucceeded(AdjustSessionSuccess sessionSuccessResponseData) {
                Log.d("example", "Session success callback called!");
                Log.d("example", "Session success data: " + sessionSuccessResponseData.toString());
            }
        });

        config.setOnSessionTrackingFailedListener(new OnSessionTrackingFailedListener() { // Set session failure tracking delegate.
            @Override
            public void onFinishedSessionTrackingFailed(AdjustSessionFailure sessionFailureResponseData) {
                Log.d("example", "Session failure callback called!");
                Log.d("example", "Session failure data: " + sessionFailureResponseData.toString());
            }
        });

        config.setOnDeeplinkResponseListener(new OnDeeplinkResponseListener() { // Evaluate deferred deep link to be launched.
            @Override
            public boolean launchReceivedDeeplink(Uri deeplink) {
                Log.d("example", "Deferred deep link callback called!");
                Log.d("example", "Deep link URL: " + deeplink);

                return true;
            }
        });

        config.setSendInBackground(true); // Allow to send in the background.
        Adjust.addSessionCallbackParameter("sc_foo", "sc_bar"); // Add session callback parameters.
        Adjust.addSessionCallbackParameter("sc_key", "sc_value"); // Add session callback parameters.
        Adjust.addSessionPartnerParameter("sp_foo", "sp_bar"); // Add session partner parameters.
        Adjust.addSessionPartnerParameter("sp_key", "sp_value"); // Add session partner parameters.
        Adjust.removeSessionCallbackParameter("sc_foo"); // Remove session callback parameters.
        Adjust.removeSessionPartnerParameter("sp_key"); // Remove session partner parameters.
        Adjust.resetSessionCallbackParameters(); // Remove all session callback parameters.
        Adjust.resetSessionPartnerParameters(); // Remove all session partner parameters.
        Adjust.onCreate(config); // Initialise the adjust SDK.

        FirebaseInstanceId fcm = FirebaseInstanceId.getInstance();
        if (fcm != null) {
            String refreshedToken = fcm.getToken();
            if (!TextUtils.isEmpty(refreshedToken))
                Adjust.setPushToken(refreshedToken, getApplicationContext());
        }
        registerActivityLifecycleCallbacks(new AdjustLifecycleCallbacks()); // Register onResume and onPause events of all activities, for applications with minSdkVersion >= 14.

        /*
         * Facebook Screen Event
         * */
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        FacebookSdk.setIsDebugEnabled(false);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);

    }

    /*
     * Adjust................
     * */
    private static final class AdjustLifecycleCallbacks implements ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            Adjust.onResume();
        }

        @Override
        public void onActivityPaused(Activity activity) {
            Adjust.onPause();
        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }

    public synchronized Tracker getTracker() {
        GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
        String PROPERTY_ID = "UA-129031519-1";
        return analytics.newTracker(PROPERTY_ID);
    }

}

