package com.sbimv.mvicalls.contract;

import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataBestDeal;
import com.sbimv.mvicalls.pojo.InboxModel;

import java.util.List;

public interface MainContract {

    // ============================================================================================
    // Interface Presenters
    // ============================================================================================

    // *
    // Inbox Presenter
    // *
    interface InboxPresenter {
        void setDataInbox (ContactItem contactItem, String type, String id);
        void readDataInbox (ContactItem contactItem, String type, InboxModel inboxModel);
    }

    // *
    // How To Use Presenter
    // *
    interface HowToUsePresenter {
        void setDataHowToUse(String lang);
    }

    // *
    // Best Deal Presenter
    // *
    interface BestDealPresenter {
        void setDataBestDeal (ContactItem contactItem, String deal_id);
    }

    // ============================================================================================
    // Interface Views
    // ============================================================================================

    // *
    // Inbox View
    // *
    interface InboxView {
        void isProgress();
        void isFinish();
        void isEmpty();
        void isSuccess(List dataArrayList);
        void isDetailSuccess(String title, String message, String createDate);
        void isFailure();
    }

    // *
    // How To Use View
    // *
    interface HowToUseView {
        void isProgress();
        void isFinish();
        void isEmpty();
        void isSuccess(String videoId, String urlHowToUse, String urlFaq);
        void isFailure();
    }

    // *
    // Best Deal View
    // *
    interface BestDealView {
        void isProgress();
        void isFinish();
        void isEmpty();
        void isSuccess(List<DataBestDeal> data);
        void isFailure();
    }

    // ============================================================================================
    // Interactors
    // ============================================================================================
    interface Intractor {

        // ============================================================================================
        // Listener
        // ============================================================================================

        // *
        // Inbox Listener
        // *
        interface OnInboxFinishedListener {
            void onProgress();
            void onFinish();
            void onEmpty();
            void onSuccess(List dataArrayList);
            void onDetailSuccess(List<InboxModel> dataModelList);
            void onFailure();
        }

        // *
        // How To Use Listener
        // *
        interface OnHowToUseFinishedListener {
            void onProgress();
            void onFinish();
            void onEmpty();
            void onSuccess(String videoId, String urlHowToUse, String urlFaq);
            void onFailure();
        }

        // *
        // Best Deal Listener
        // *
        interface OnBestDealFinishedListener {
            void onProgress();
            void onFinish();
            void onEmpty();
            void onSuccess(List<DataBestDeal> data);
            void onFailure();
        }


        // ============================================================================================
        // API process...
        // ============================================================================================

        // *
        // Get data API Inbox
        // *
        void getInbox(String msisdn, String type, String id, OnInboxFinishedListener onInboxFinishedListener);

        // *
        // Get data API How To Use
        // *
        void getHowToUse(String lang, OnHowToUseFinishedListener onHowToUseFinishedListener);

        // *
        // Get data API Best Deal
        // *
        void getBestDeal(String msisdn, String deal_id, OnBestDealFinishedListener onBestDealFinishedListener);

    }


}
