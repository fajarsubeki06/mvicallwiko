package com.sbimv.mvicalls.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.AddProfileActivity;
import com.sbimv.mvicalls.db.UserTable;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdataProfile extends CursorAdapter {
    private LayoutInflater mInflater;
    private Context ctx;

    public AdataProfile(Context context, Cursor c, int flags) {
        super(context, c, flags);
        this.ctx = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.item_data_profile, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.mName = view.findViewById(R.id.et_user_name);
        holder.mPhone = view.findViewById(R.id.tv_phone_user);
        holder.pUser = view.findViewById(R.id.iv_user);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(final View view, final Context context, final Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();

        final String caller_id = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_CALLER_ID));
        final String photoUri = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_PIC));
        final String displayNameServer = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_NAME));// added name - checkout old version 19-12-2018
//        final String displayNamePhoneBook = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_NAME_PHONE_BOOK));// added name phone book
        final String msisdn = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_MSISDN));

        holder.mName.setText(displayNameServer.equals("Unknown") ? msisdn : displayNameServer); // added name phone book - checkout old version 19-12-2018
//        holder.mName.setText(displayNamePhoneBook.equals("Unknown") ? msisdn : displayNamePhoneBook); // added name phone book
        holder.mPhone.setText(msisdn.isEmpty() ? "" : msisdn);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.user_images);
        Glide.with(context)
                .load(photoUri)
                .apply(requestOptions)
                .into(holder.pUser);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itn = new Intent(context, AddProfileActivity.class);
                itn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                itn.putExtra("caller_id",caller_id);
                itn.putExtra("nameUsr",displayNameServer);
                itn.putExtra("phoneUsr",msisdn);
                context.startActivity(itn);
                ((Activity)ctx).finish();
            }
        });

    }

    static class ViewHolder {
        TextView mName;
        TextView mPhone;
        CircleImageView pUser;
    }

}
