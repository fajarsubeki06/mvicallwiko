package com.sbimv.mvicalls.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.PreviewVideoCollection;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.MyCollection;

import java.util.ArrayList;

public class RedeemVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int item;
    private ArrayList<MyCollection> videoList;
    private Context context;
    private String rewardID;
    private int posA;
    private ArrayList<String> contentIDList = new ArrayList<>();
    private LangViewModel model = LangViewModel.getInstance();


    public RedeemVideoAdapter(ArrayList<MyCollection> videoList, Context context, String rewardID, int total_item) {
        this.videoList = videoList;
        this.context = context;
        this.rewardID = rewardID;
        this.item = total_item;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater != null ? inflater.inflate(R.layout.item_reward_video, parent, false) : null;
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder vh = (ViewHolder) holder;
        final MyCollection data = videoList.get(position);

        vh.tvArtist.setText(data.alias);
        vh.tvJudul.setText(data.judul);

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.background);
        Glide.with(context).load(data.thumb_pic).apply(requestOptions).into(vh.img_collection);
        vh.checkBox.setOnCheckedChangeListener(null);
        vh.checkBox.setChecked(data.isChecked);

        vh.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
               data.setChecked(isChecked);

                if (item == 0 && posA != position && isChecked) {
                    Toast.makeText(context, model.getLang().string.wordingOverChooseVideo, Toast.LENGTH_SHORT).show();
                    videoList.get(position).setChecked(false);
                    compoundButton.setChecked(false);
                } else {
                    if (isChecked) {
                        item--;
                        posA = position;
                        contentIDList.add(data.content_id);
                    } else {
                        item++;
                        posA = position;
                        contentIDList.remove(data.content_id);
                    }
                    Intent intent = new Intent("select_video_reward");
                    intent.putExtra("itemVideo", item);
                    intent.putExtra("contentID", contentIDList);
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                }
            }
        });

        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PreviewVideoCollection.class);
                intent.putExtra("dataCollections", data);
                intent.putExtra("rewardID", rewardID);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvArtist;
        TextView tvJudul;
        VideoView vvPreview;
        CardView cardView;
        ImageView img_collection;
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            tvArtist = itemView.findViewById(R.id.tv_artist_collection);
            tvJudul = itemView.findViewById(R.id.tv_title_collection);
            cardView = itemView.findViewById(R.id.card_view);
            vvPreview = itemView.findViewById(R.id.vv_preview_collection);
            img_collection = itemView.findViewById(R.id.img_collection);
            checkBox = itemView.findViewById(R.id.checkView);

//            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
//                    MyCollection data = videoList.get(getAdapterPosition());
//                    if (item == 0 && posA != getAdapterPosition() && compoundButton.isChecked()) {
//                        Toast.makeText(context, R.string.wording_over_choose_video, Toast.LENGTH_SHORT).show();
//                        compoundButton.setChecked(false);
//                    } else {
//                        if (isChecked) {
//                            item--;
//                            posA = getAdapterPosition();
//                            contentIDList.add(data.content_id);
//                        } else {
//                            item++;
//                            posA = getAdapterPosition();
//                            contentIDList.remove(data.content_id);
//                        }
//
//                        Intent intent = new Intent("select_video_reward");
//                        intent.putExtra("itemVideo", item);
//                        intent.putExtra("contentID", contentIDList);
//                        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
//                    }
//                }
//            });
        }
    }
}