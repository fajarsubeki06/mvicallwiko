package com.sbimv.mvicalls.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintSet;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.db.UserTable;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.CallerVideoManager;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.view.VideoToneView;

import de.hdodenhof.circleimageview.CircleImageView;


public class ContactMVICallCursorAdapter extends CursorAdapter {

    private static final String TAG = "ContactCursorAdapter";
    private LayoutInflater mInflater;
    private ContactItem own;
    private onListClickedCallListner listner;

    public ContactMVICallCursorAdapter(Context context, Cursor c, boolean autoRequery, onListClickedCallListner listner) {
        super(context, c, autoRequery);
        mInflater = LayoutInflater.from(context);
        this.listner = listner;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        final View view = mInflater.inflate(R.layout.item_contact, viewGroup, false);
        final ViewHolder holder = new ViewHolder();

        own = SessionManager.getProfile(context);
        holder.tvUserName = view.findViewById(R.id.et_user_name); // added name phone book
        holder.tvNameServer = view.findViewById(R.id.et_name_server);// added name phone book
        holder.tvUserStatus = view.findViewById(R.id.tv_user_status);
        holder.ivUserPic = view.findViewById(R.id.iv_user);
        holder.btnPreview = view.findViewById(R.id.btnPlayViTone);
        holder.btnCall = view.findViewById(R.id.iv_phone);
        holder.relaCall = view.findViewById(R.id.relaCall);
        holder.relaPreviewVitone = view.findViewById(R.id.relaPreviewVitone);
        holder.linItemContact = view.findViewById(R.id.linItemContact);

        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final ViewHolder holder = (ViewHolder) view.getTag();

        final String photoUri = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_PIC));
//        final String displayNamePhoneBook = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_NAME_PHONE_BOOK));// added name phone book - checkout old version 19-12-2018
        final String displayNameServer = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_NAME));// added name phone book - checkout old version 19-12-2018

        final String callerId = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_CALLER_ID));
        final String msisdn = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_MSISDN));
        final String status = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_STATUS));
        final String friend_id = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_CALLER_ID));
        final String device = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_DEVICE));
        final String video_caller = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_VIDEO));
        final String ratio = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_RATIO));

        holder.tvUserName.setText(displayNameServer.equals("Unknown") ? msisdn : displayNameServer); // added name phone book - checkout old version 19-12-2018
//        holder.tvUserName.setText(displayNamePhoneBook.equals("Unknown") ? msisdn : displayNamePhoneBook); // added name phone book
//        holder.tvNameServer.setText(displayNameServer.equals("Unknown") ? "Unknown" : "~ "+displayNameServer);// added name phone book
        holder.tvUserStatus.setText(TextUtils.isEmpty(status) ? "'No Status'" : status);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.user_images);
        Glide.with(context).load(photoUri).apply(requestOptions).into(holder.ivUserPic);

        holder.relaCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (own != null) {
                    try {
                        GATRacker.getInstance(((Activity) context).getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_CONTACT, TrackerConstant.EVENT_CAT_CONTACT, "click button call", own.caller_id + "/" + friend_id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (!TextUtils.isEmpty(msisdn) && !TextUtils.isEmpty(callerId)) {
                    call(view.getContext(), msisdn, callerId, device);
                }
            }
        });

        holder.linItemContact.setEnabled(true);
        holder.linItemContact.setOnClickListener(new View.OnClickListener() {
            int i = 0;

            @Override
            public void onClick(final View view) {

                i++;
                Handler handler = new Handler();
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        i = 0;
                        if (videoToneView != null) {
                            videoToneView.dismiss();
                            videoToneView = null;
                        }

//                        videoToneView = VideoToneView.showDummyFloatingWindow(view.getContext().getApplicationContext(), msisdn, false, true, true);
                        videoToneView = VideoToneView.showDummyFloatingWindow(view.getContext().getApplicationContext(), msisdn,ratio,false, true, true);

                    }
                };

                if (i == 1) {
                    handler.postDelayed(r, 500);
                } else if (i == 2) {
                    i = 0;
                }

                if (own != null) {
                    try {
                        GATRacker.getInstance(((Activity) context).getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_CONTACT, TrackerConstant.EVENT_CAT_CONTACT, "click friend profile", own.caller_id + "/" + friend_id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        holder.relaPreviewVitone.setOnClickListener(new View.OnClickListener() {
            int i = 0;

            @Override
            public void onClick(final View view) {

                i++;
                Handler handler = new Handler();
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        i = 0;
                        if (videoToneView != null) {
                            videoToneView.dismiss();
                            videoToneView = null;
                        }
//                        videoToneView = VideoToneView.showDummyFloatingWindow(view.getContext().getApplicationContext(), msisdn, false, true, true);
                        videoToneView = VideoToneView.showDummyFloatingWindow(view.getContext().getApplicationContext(), msisdn,ratio,false, true, true);
                    }
                };

                if (i == 1) {
                    handler.postDelayed(r, 500);
                } else if (i == 2) {
                    i = 0;
                }

                if (own != null) {
                    try {
                        GATRacker.getInstance(((Activity) context).getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_CONTACT, TrackerConstant.EVENT_CAT_CONTACT, "click friend profile", own.caller_id + "/" + friend_id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

    }

    private void call(Context ctx, String number, String caller_id, String device) {
        listner.onListSelected(ctx,number, caller_id, device);
    }

    private class ViewHolder {
        TextView tvUserName;
        TextView tvNameServer;
        TextView tvUserStatus;
        CircleImageView ivUserPic;
        ImageView btnPreview,btnCall;
        RelativeLayout relaCall,relaPreviewVitone;
        LinearLayout linItemContact;
    }

//    private String getMetaData(Context context, String url) {
//        if (TextUtils.isEmpty(url))
//            return "";
//
//        Uri uri = CallerVideoManager.getVideoUri(context, url);
//        if (uri != null) {
//            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//            try {
//                retriever.setDataSource(String.valueOf(uri));
//                String h = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
//                String w = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
//
//                float wt = Float.valueOf(w);
//                float ht = Float.valueOf(h);
//                float divide_wh = wt / ht;
//
//                String _3 = "3";
//                String _4 = "4";
//                String _5 = "5";
//                String _9 = "9";
//                String _16 = "16";
//                String _1 = "1";
//
//                float f_3 = Float.valueOf(_3);
//                float f_4 = Float.valueOf(_4);
//                float f_5 = Float.valueOf(_5);
//                float f_9 = Float.valueOf(_9);
//                float f_16 = Float.valueOf(_16);
//                float f_1 = Float.valueOf(_1);
//
//                float ratio_16_9 = f_16 / f_9;
//                float ratio_9_16 = (float) 0.5633803;
//                float ratio_4_3 = f_4 / f_3;
//                float ratio_4_5 = f_4 / f_5;
//                float ratio_3_4 = f_3 / f_4;
//                float ratio_5_4 = f_5 / f_4;
//                float ratio_1_1 = f_1 / f_1;
//
//                if (divide_wh == ratio_16_9) {
//                    return  "16:9";
//                } else if (divide_wh == ratio_9_16) {
//                    return "4:5";
//                } else if (divide_wh == ratio_4_3) {
//                    return "4:3";
//                } else if (divide_wh == ratio_4_5) {
//                    return "4:5";
//                } else if (divide_wh == ratio_3_4) {
//                    return "3:4";
//                } else if (divide_wh == ratio_5_4) {
//                    return "5:4";
//                }else if (divide_wh == ratio_1_1){
//                    return "1:1";
//                }
//            } catch (Exception e) {
//                Log.e(TAG, "Exception : " + e.getMessage());
//            }
//            return "";
//        }
//
//        return "";
//    }

    // DUMMY
    VideoToneView videoToneView;

    public interface onListClickedCallListner {
        void onListSelected(Context ctx, String msisdn, String caller_id, String device);
    }
}