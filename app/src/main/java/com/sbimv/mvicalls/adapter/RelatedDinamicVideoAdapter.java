package com.sbimv.mvicalls.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.PreviewPremiumVideo;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;

import java.util.ArrayList;

/**
 * Created by admin on 1/30/2018.
 */

public class RelatedDinamicVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private DataDinamicTitle data;
    private LangViewModel model = LangViewModel.getInstance();

    public RelatedDinamicVideoAdapter(DataDinamicTitle data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_premium_video, parent, false);
        return new VH(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ArrayList<DataDinamicVideos> videos = data.getContent();
        final DataDinamicVideos dataRelated = videos.get(position);

        VH vh = (VH) holder;
        vh.judul.setText(dataRelated.judul);
        vh.nama.setText(dataRelated.alias);

        String currency = TextUtils.isEmpty(dataRelated.price_label)?"Rp.":dataRelated.price_label;

        if (dataRelated.price.equals("0")){
            vh.harga.setText(model.getLang().string.free);
        }else {
            vh.harga.setText(currency +" "+ dataRelated.price);
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.background);
        Glide.with(vh.imgVideo.getContext())
                .load(dataRelated.thumb_pic)
                .apply(requestOptions)
                .into(vh.imgVideo);

        ((VH) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PreviewPremiumVideo.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("video", dataRelated);
                intent.putExtra("title", data);
                view.getContext().startActivity(intent);
                Activity activity = (Activity) view.getContext();
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (data == null) ? 0 : data.getContent().size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView nama;
        TextView judul;
        TextView harga;
        ImageView imgVideo;

        public VH(View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.tv_artist);
            judul = itemView.findViewById(R.id.tv_title);
            harga = itemView.findViewById(R.id.tv_harga);
            imgVideo = itemView.findViewById(R.id.iv_video);

        }
    }
}
