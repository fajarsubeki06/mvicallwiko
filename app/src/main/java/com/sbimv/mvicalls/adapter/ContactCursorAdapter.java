package com.sbimv.mvicalls.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.util.ContactsQuery;

public class ContactCursorAdapter extends CursorAdapter {

    static final String TAG = "ContactCursorAdapter";
    LayoutInflater mInflater;
    private LangViewModel model = LangViewModel.getInstance();

    public ContactCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        final View view = mInflater.inflate(R.layout.item_contact_invite, viewGroup, false);

        final ViewHolder holder = new ViewHolder();
        holder.tvUserName = view.findViewById(R.id.et_user_name);
        holder.tvUserStatus = view.findViewById(R.id.tv_user_status);
        holder.ivUserPic = view.findViewById(R.id.iv_user);
        holder.ivPhone = view.findViewById(R.id.iv_phone);
        holder.contact = view.findViewById(R.id.linItemContact);
        holder.btnInvite = view.findViewById(R.id.btnInvite);
        holder.btnInvite.setText(model.getLang().string.invite);
        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final ViewHolder holder = (ViewHolder) view.getTag();

        final int id = cursor.getInt(ContactsQuery.ID);
        final String photoUri = cursor.getString(ContactsQuery.PHOTO_THUMBNAIL_DATA);
        final String displayName = cursor.getString(ContactsQuery.DISPLAY_NAME);

        holder.tvUserName.setText(displayName);
        holder.tvUserStatus.setVisibility(View.GONE);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.user_images);
        Glide.with(context)
                .load(photoUri)
                .apply(requestOptions)
                .into(holder.ivUserPic);

        holder.contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionCall(view,context,id);
            }
        });

    }

    private void actionCall(View view,Context context,int id) {

        try {
            String numMobile = null;
            String numHome = null;
            String numWork = null;

            String [] COLS = {ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE};

            Cursor phones = context.getContentResolver()
                    .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, COLS,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);

            while (phones.moveToNext()) {
                String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));

                switch (type) {
                    case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                        numHome = number;
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                        numMobile = number;
                        break;
                    case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                        numWork = number;
                        break;
                }
            }
            phones.close();

            // call mobile number
            if(numMobile != null){
                call(view.getContext(), numMobile);
                return;
            }

            // call home number
            if(numHome != null){
                call(view.getContext(), numHome);
                return;
            }

            // call work number
            if(numWork != null){
                call(view.getContext(), numWork);
                return;
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void call(Context ctx, String number){
        Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null));
        ctx.startActivity(phoneIntent);
    }

    private class ViewHolder {
        TextView tvUserName;
        TextView tvUserStatus;
        ImageView ivUserPic;
        ImageView ivPhone;
        ConstraintLayout contact;
        Button btnInvite;
    }
}
