package com.sbimv.mvicalls.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataChargingContent;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;

import java.util.ArrayList;

public class PopUpSubsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    final private Context context;
    private ArrayList<DataChargingContent> dataPopUpSubs;
    private ContactItem own;
    private ProgressDialog progressDialog;
    private boolean isData;
    private DoWeboptin weboptin;
    private LangViewModel model = LangViewModel.getInstance();
    private String price_label = "";

    public PopUpSubsAdapter(ArrayList<DataChargingContent> dataPopUpSubs, Context context) {
        this.dataPopUpSubs = dataPopUpSubs;
        this.context = context;
        this.weboptin = (DoWeboptin) context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_radio_subs, parent, false);
        own = SessionManager.getProfile(context);

        progressDialog = new ProgressDialog(v.getContext());
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Processing...");
        progressDialog.setIndeterminate(true);
        return new VH(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final DataChargingContent data = dataPopUpSubs.get(position);
        final VH vh = (VH) holder;
        final String price = String.valueOf(data.getPrice());
        String description = data.getDesc();

        try {
            price_label = TextUtils.isEmpty(data.getPrice_label()) ? "" : data.getPrice_label();
        }catch (Exception e){
            e.printStackTrace();
        }

        vh.radio.setText(price_label + " " + price + " / " + description);

        vh.btnSubs.setText(model.getLang().string.choose);
        vh.btnSubs.setOnClickListener(v -> { // Button On Click...........
            final Activity activity = ((Activity) context);
            String charging = TextUtils.isEmpty(data.getChannel())?"":data.getChannel();
            if (charging.equals("umb")) {
                if (own != null) {
                    try {
                        GATRacker.getInstance(activity.getApplication())
                                .sendEventWithScreen(TrackerConstant.SCREEN_SUBSCRIPTION_DIALOG,
                                        TrackerConstant.EVENT_CAT_SUBSCRIPTION_DIALOG, "perform UMB", own.caller_id + "/" + data.getKeyword());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                callChargingContent(data.getKeyword());
                activity.finish();

            }

            else if (charging.equals("ussd")){ // Enable channel USSD for vietnam
                weboptin.getwebCharging(String.valueOf(data.getPrice()));
            }

            else if (charging.equals("dcb")){
                String prices = TextUtils.isEmpty(String.valueOf(data.getPrice()))?"":String.valueOf(data.getPrice());
                String pricesLbl = TextUtils.isEmpty(String.valueOf(data.getPrice_label()))?"":String.valueOf(data.getPrice_label());

                weboptin.getRequestDcb(prices, pricesLbl);
            }

            else if (charging.equals("weboptin") && Util.isWifiConnected(context)) {
                showAlertWifi(price, isData);
            }else if (charging.equals("weboptin_otp") && Util.isWifiConnected(context)) {
                showAlertWifi(price, isData);
            } else if (charging.equals("weboptin") && !Util.isWifiConnected(context)) {
                weboptin.getwebCharging(String.valueOf(data.getPrice()));
            }else if (charging.equals("weboptin_otp") && !Util.isWifiConnected(context)) {
                weboptin.getwebCharging(String.valueOf(data.getPrice()));
            } else if(charging.equals("GOOGLE")){
                weboptin.getgoogleCharging();
            } else {
                if (own != null) {
                    try {
                        GATRacker.getInstance(activity.getApplication())
                                .sendEventWithScreen(TrackerConstant.SCREEN_SUBSCRIPTION_DIALOG,
                                        TrackerConstant.EVENT_CAT_SUBSCRIPTION_DIALOG, "send SMS", own.caller_id + "/" + data.getKeyword());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                BaseActivity.sendSMSChargingAndWaiting(activity, data.getSdc(), data.getKeyword(), new BaseActivity.ChargingListener() {
                    @Override
                    public void onDelayFinished() {
                        activity.finish();
                    }
                });
            }

        });

        vh.relativeLayout.setOnClickListener(new View.OnClickListener() { // Layout On Click...........
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                final Activity activity = ((Activity) context);
                String charging = data.getChannel();
                if (charging.equals("umb")) {
                    if (own != null) {
                        try {
                            GATRacker.getInstance(activity.getApplication())
                                    .sendEventWithScreen(TrackerConstant.SCREEN_SUBSCRIPTION_DIALOG,
                                            TrackerConstant.EVENT_CAT_SUBSCRIPTION_DIALOG, "perform UMB", own.caller_id + "/" + data.getKeyword());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    callChargingContent(data.getKeyword());
                    activity.finish();

                }
                else if (charging.equals("ussd")){ // Enable channel USSD for vietnam
                    weboptin.getwebCharging(String.valueOf(data.getPrice()));
                }
                else if (charging.equals("weboptin") && Util.isWifiConnected(context)) {
                    showAlertWifi(price, isData);
                } else if (charging.equals("weboptin_otp") && Util.isWifiConnected(context)) {
                    showAlertWifi(price, isData);
                } else if (charging.equals("weboptin") && !Util.isWifiConnected(context)) {
                    weboptin.getwebCharging(String.valueOf(data.getPrice()));
                }else if (charging.equals("weboptin_otp") && !Util.isWifiConnected(context)) {
                    weboptin.getwebCharging(String.valueOf(data.getPrice()));
                }

                else if (charging.equals("dcb")){
                    String prices = TextUtils.isEmpty(String.valueOf(data.getPrice()))?"":String.valueOf(data.getPrice());
                    String pricesLbl = TextUtils.isEmpty(String.valueOf(data.getPrice_label()))?"":String.valueOf(data.getPrice_label());
                    weboptin.getRequestDcb(prices, pricesLbl);
                }

                else if(charging.equals("GOOGLE")){
                    weboptin.getgoogleCharging();
                } else {
                    if (own != null) {
                        try {
                            GATRacker.getInstance(activity.getApplication())
                                    .sendEventWithScreen(TrackerConstant.SCREEN_SUBSCRIPTION_DIALOG,
                                            TrackerConstant.EVENT_CAT_SUBSCRIPTION_DIALOG, "send SMS", own.caller_id + "/" + data.getKeyword());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    BaseActivity.sendSMSChargingAndWaiting(activity, data.getSdc(), data.getKeyword(), new BaseActivity.ChargingListener() {
                        @Override
                        public void onDelayFinished() {
                            activity.finish();
                        }
                    });
                }
            }
        });
    }

    private void showAlertWifi(String s_price, boolean s_isData) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(model.getLang().string.wordingChangeWifiState);
        builder1.setPositiveButton(model.getLang().string.turnOffWifi, null);
        AlertDialog dialog1 = builder1.create();
        dialog1.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button buttonPositive = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                buttonPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isData = true;
                        dialog.dismiss();

                        Intent intent = new Intent("turn_off_wifi");
                        intent.putExtra("price", s_price);
                        intent.putExtra("isData", s_isData);
                        context.sendBroadcast(intent);
                    }
                });
            }
        });
        dialog1.show();
    }

    private void callChargingContent(String umb) {

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(umb)));
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        context.startActivity(callIntent);
    }

    @Override
    public int getItemCount() {
        return dataPopUpSubs.size();
    }

    public interface DoWeboptin {
        void getwebCharging(String price);
        void getgoogleCharging();
        void getRequestDcb(String price, String priceLbl);
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView radio;
        LinearLayout relativeLayout;
        RelativeLayout relaListPrice;
        Button btnSubs;

        public VH(View itemView) {
            super(itemView);
            radio = itemView.findViewById(R.id.rbSubs);
            relativeLayout = itemView.findViewById(R.id.rPriceList);
            relaListPrice = itemView.findViewById(R.id.relaItemPrice);
            btnSubs = itemView.findViewById(R.id.btnSubs);
        }
    }

}