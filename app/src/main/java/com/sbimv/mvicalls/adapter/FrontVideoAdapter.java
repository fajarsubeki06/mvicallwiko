package com.sbimv.mvicalls.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lid.lib.LabelImageView;
import com.sbimv.mvicalls.PreviewFrontVideo;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;

import java.util.ArrayList;

public class FrontVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private DataDinamicTitle data;
    private LangViewModel model = LangViewModel.getInstance();

    public FrontVideoAdapter(DataDinamicTitle data) {
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_premium_video, parent, false);
        return new VH(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ArrayList<DataDinamicVideos> videos = data.getContent();
        final DataDinamicVideos dataFront = videos.get(position);
        final VH vh = (VH) holder;
        String nama = dataFront.getAlias();
        vh.judul.setText(dataFront.getJudul());
        vh.nama.setText(nama);

        String currency = TextUtils.isEmpty(dataFront.price_label)?"Rp.":dataFront.price_label;

        if (dataFront.price.equals("0")){
            String free = model.getLang().string.free;
            vh.harga.setText(free);
            vh.labelImageView.setLabelText(free);
            vh.labelImageView.setVisibility(View.VISIBLE);
        }else {
            vh.harga.setText(currency +" "+ dataFront.getPrice());
            vh.labelImageView.setVisibility(View.GONE);
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.background);
        Glide.with(vh.imgVideo.getContext())
                .load(dataFront.getThumb_pic())
                .apply(requestOptions)
                .into(vh.imgVideo);

        ((VH) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                Intent intent = new Intent(view.getContext(), PreviewFrontVideo.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("video", dataFront);
                intent.putExtra("title", data);
                view.getContext().startActivity(intent);
                Activity activity = (Activity) view.getContext();
                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });

    }


    @Override
    public int getItemCount() {
        return (data == null) ? 0 : data.getContent().size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView judul;
        TextView nama;
        TextView harga;
        ImageView imgVideo;
        MediaController mediaController;
        LabelImageView labelImageView;

        public VH(View itemView) {
            super(itemView);
            judul = itemView.findViewById(R.id.tv_title);
            nama = itemView.findViewById(R.id.tv_artist);
            harga = itemView.findViewById(R.id.tv_harga);
            imgVideo = itemView.findViewById(R.id.iv_video);
            mediaController = new MediaController(itemView.getContext());
            mediaController.setAnchorView(mediaController);
            labelImageView = itemView.findViewById(R.id.labelItem);
        }
    }
}
