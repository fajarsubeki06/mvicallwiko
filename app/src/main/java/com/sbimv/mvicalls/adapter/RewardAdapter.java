package com.sbimv.mvicalls.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.PopupDetailRewardActivity;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.DataRewards;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RewardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<DataRewards.ListRewards> listRewards;
    private Context context;
    private ViewHolder viewHolder;
    private String userPoint;
    private LangViewModel model = LangViewModel.getInstance();

    public RewardAdapter(String point,ArrayList<DataRewards.ListRewards> listReward, Context context) {
        this.listRewards = listReward;
        this.context = context;
        this.userPoint = point;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater != null ? inflater.inflate(R.layout.item_rewards, parent, false) : null;
        viewHolder = new ViewHolder(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ViewHolder vh = (ViewHolder) holder;
        final DataRewards.ListRewards datas = listRewards.get(position);
        vh.tvHeadLine.setText(datas.getHeadline());
        vh.tvPoint.setText(datas.getPoin() + " " + model.getLang().string.point);

        RequestOptions defBanner = new RequestOptions();
        defBanner.placeholder(R.drawable.bg_banner_reward);
        Glide.with(context).asBitmap().load(datas.getBanner()).apply(defBanner).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(final Bitmap bitmap, Transition<? super Bitmap> transition) {
                final int w = bitmap.getWidth();
                final int h = bitmap.getHeight();
                vh.imgBG.post(new Runnable() {
                    @Override
                    public void run() {
                        int imgW = vh.imgBG.getMeasuredWidth();
                        int imgH = imgW*h/w;
                        vh.imgBG.getLayoutParams().height = imgH;
                        vh.imgBG.requestLayout();
                        vh.imgBG.setImageBitmap(bitmap);
                    }
                });
            }
        });

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.icx_mv_color);
        Glide.with(context).load(datas.getIcon()).apply(requestOptions).into(vh.imgHeadline);

        viewHolder.btnRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PopupDetailRewardActivity.class);
                intent.putExtra("dataReward", datas);
                intent.putExtra("userPoint",userPoint);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listRewards == null ? 0 : listRewards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgBG;
        private CircleImageView imgHeadline;
        private TextView tvHeadLine, tvPoint;
        private Button btnRedeem;
        private CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            imgHeadline = itemView.findViewById(R.id.imgHeadLine);
            tvHeadLine = itemView.findViewById(R.id.tvHeadLine);
            tvPoint = itemView.findViewById(R.id.tvItemPoint);
            btnRedeem = itemView.findViewById(R.id.btnRedeemPoint);
            btnRedeem.setText(model.getLang().string.redeemPoin);
            imgBG = itemView.findViewById(R.id.imgBgReward);
            checkBox = itemView.findViewById(R.id.checkView);
        }
    }
}
