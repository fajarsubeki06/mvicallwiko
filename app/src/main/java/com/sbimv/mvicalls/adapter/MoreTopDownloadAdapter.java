package com.sbimv.mvicalls.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;

import java.util.List;

//import com.squareup.picasso.Picasso;

/**
 * Created by admin on 1/9/2018.
 */

public class MoreTopDownloadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static List<DataDinamicVideos> data;
    private LangViewModel model = LangViewModel.getInstance();

    public MoreTopDownloadAdapter(List<DataDinamicVideos> latestVideoList) {
        data = latestVideoList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = null;
        if (inflater != null) {
            v = inflater.inflate(R.layout.item_premium_video,parent,false);
        }
        return new VH(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DataDinamicVideos video = data.get(position);
        VH vh = (VH) holder;

        vh.tvArtist.setText(video.alias);
        vh.tvJudul.setText(video.judul);
        String currency = TextUtils.isEmpty(video.price_label)?"Rp.":video.price_label;

        if (video.price.equals("0")){
            vh.tvPrice.setText(model.getLang().string.free);
        }else {
            vh.tvPrice.setText( currency +" "+ video.price);
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.background);

        Glide.with(vh.ivVideo.getContext())
                .load(video.thumb_pic)
                .apply(requestOptions)
                .into(vh.ivVideo);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class VH extends RecyclerView.ViewHolder {
        TextView tvArtist;
        TextView tvJudul;
        TextView tvPrice;
        ImageView ivVideo;

        public VH(View itemView) {
            super(itemView);
            tvArtist = itemView.findViewById(R.id.tv_artist_collection);
            tvJudul = itemView.findViewById(R.id.tv_title_collection);
            tvPrice = itemView.findViewById(R.id.tv_harga_big);
            ivVideo = itemView.findViewById(R.id.img_collection);
        }
    }
}
