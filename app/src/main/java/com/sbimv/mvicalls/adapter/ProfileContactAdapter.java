package com.sbimv.mvicalls.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.db.UserTable;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.ProfileItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileContactAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ContactItem own;
    private List<ProfileItem.Item> data;
    private Listener listener;
    private Cursor cursor;
    private String friend_ID;
    private Context context;
    private LangViewModel model = LangViewModel.getInstance();

    public interface Listener {
//        void onAddProfile();

        void onEditProfile(int index);

        void onDeleteProfile(int index);
    }

    public ProfileContactAdapter(ContactItem own,Cursor cursor,Context context, Listener listener) {
        this.own = own;
        this.cursor = cursor;
        this.listener = listener;
        this.context = context;

        setData();
    }

    public void setData() {
        if (this.data == null)
            this.data = new ArrayList<>();
        this.data.clear();

        // add all profile
        this.data.addAll(own.profiles.list);

        // swap all profile to 1st position
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).msisdn.equalsIgnoreCase("") && i != 0) {
                Collections.swap(data, i, 0);
                break;
            }
        }
        notifyDataSetChanged();
    }

    public List<ProfileItem.Item> getData() {
        return data;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ProfileItem.Item item = data.get(position);
        if (holder instanceof VH) {
            VH vh = (VH) holder;
            vh.tvProfile.setText(item.profile);
            String notel = item.msisdn;
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.user_images);

            String imgUri = null;
            if (!TextUtils.isEmpty(notel)){
                imgUri = getImageProfile(notel);
            }
            Glide.with(vh.imgSetStatus.getContext()).load(imgUri).apply(requestOptions).into(vh.imgSetStatus);

            if (!notel.startsWith("+")) {
                notel = "+" + notel;
            }

            if (TextUtils.isEmpty(notel)) {
                vh.tvContactName.setText(model.getLang().string.statusForAllContact);
            } else if (TextUtils.isEmpty(item.name)) {
                ContactItem contact = ContactDBManager.getInstance(vh.tvProfile.getContext()).getCallerByMSISDN(notel);

                if (contact != null) {
                    if (TextUtils.isEmpty(contact.getName()))
                        vh.tvContactName.setText(notel);
                    else {
                        item.name = contact.getName();
                        vh.tvPhone.setVisibility(View.VISIBLE);
                        vh.tvPhone.setText(item.msisdn);
                        vh.tvContactName.setText(item.name);
                    }
                } else {
                    vh.tvContactName.setText(notel);
                }

            } else {
                vh.tvContactName.setText(item.name);
            }

            if (position >= 0)
                vh.btnDeleteProfile.setVisibility(View.VISIBLE);
            else
                vh.btnDeleteProfile.setVisibility(View.GONE);
        }
    }

    // get Image profile from sqLite
    private String getImageProfile(String msisdn) {
        String img = null;
        String phone;

        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    phone = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_MSISDN));
                    if (msisdn.equalsIgnoreCase(phone)) {
                        img = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_PIC));
                        friend_ID = cursor.getString(cursor.getColumnIndex(UserTable.UserEntry.COLUMN_NAME_CALLER_ID));
                    }
                    cursor.moveToNext();
                }
            }
        }

        return img;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

//        if (viewType == 0) {
            View v = inflater.inflate(R.layout.item_profile, parent, false);
            final VH vh = new VH(v);

            vh.btnEditProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                if (listener != null)
                    listener.onEditProfile(vh.getAdapterPosition());
                }
            });

            vh.btnDeleteProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                if (listener != null)
                    listener.onDeleteProfile(vh.getAdapterPosition());
                    if (own != null){
                        if (!TextUtils.isEmpty(friend_ID)){
                            try{
                                GATRacker.getInstance(((Activity)context).getApplication())
                                        .sendEventWithScreen(TrackerConstant.SCREEN_PROFILE,
                                                TrackerConstant.EVENT_CAT_PROFILE,"contact status deleted",
                                                own.caller_id+"/"+friend_ID);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
            return vh;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == data.size() - 1)
            return 1;
        else
            return 0;
    }

    public static class VH extends RecyclerView.ViewHolder {
        TextView tvContactName, tvPhone;
        TextView tvProfile;
        CircleImageView imgSetStatus;
        public static ImageView btnEditProfile;
        public static ImageView btnDeleteProfile;

        public VH(View itemView) {
            super(itemView);
            btnEditProfile = itemView.findViewById(R.id.btn_edit_profile_status);
            btnDeleteProfile = itemView.findViewById(R.id.btn_delete_profile);
            btnEditProfile.setEnabled(true);
            btnDeleteProfile.setEnabled(true);
            imgSetStatus = itemView.findViewById(R.id.imgSetContactStatus);
            tvPhone = itemView.findViewById(R.id.tv_phone);
            tvContactName = itemView.findViewById(R.id.tv_contact_name);
            tvProfile = itemView.findViewById(R.id.tv_statusProfile);
        }
    }
}
