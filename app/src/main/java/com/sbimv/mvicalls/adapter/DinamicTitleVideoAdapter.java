package com.sbimv.mvicalls.adapter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.util.ShowCaseWrapper;

import java.util.ArrayList;

public class DinamicTitleVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<DataDinamicTitle> datas;
    private Context context;
    private ShowCaseWrapper showCaseWrapper;
    private TextView tvMore;
    private LangViewModel model = LangViewModel.getInstance();


    public DinamicTitleVideoAdapter(ArrayList<DataDinamicTitle> datas, Context context) {
        this.datas = datas;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_category_video, parent, false);
        tvMore = view.findViewById(R.id.tvMoreVideos);
        tvMore.setText(model.getLang().string.moreVideos);
        showCaseWrapper = ShowCaseWrapper.create((Activity) context);
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((VH) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public class VH extends RecyclerView.ViewHolder {

        TextView tvTitleCategory, tvMoreCategories;
        RecyclerView rvDinamicVideo;
        LinearLayout seeAll, btnSeeAll;

        public VH(View itemView) {
            super(itemView);
            tvTitleCategory = itemView.findViewById(R.id.tvCatTitle);
            tvMoreCategories = itemView.findViewById(R.id.tvMoreVideos);
            rvDinamicVideo = itemView.findViewById(R.id.rvCatalogVideos);
            seeAll = itemView.findViewById(R.id.linSeeAll);
            btnSeeAll = itemView.findViewById(R.id.btnSeeAll);
        }

        void bind(int position) {
            final DataDinamicTitle DMV = datas.get(position);
            tvTitleCategory.setText(DMV.getCategory());
//            if (DMV.getContent()==null){
//
//            }else {
//                tvTitleCategory.setText(DMV.getCategory());
//            }

            if (datas.get(position).getChild() != null) {
                seeAll.setVisibility(View.GONE);
                LinearLayoutManager layout = new LinearLayoutManager(context);
                layout.setOrientation(LinearLayoutManager.VERTICAL);
                rvDinamicVideo.setLayoutManager(layout);
                VideoClipCategoryAdapter ar = new VideoClipCategoryAdapter(datas.get(position).getChild(), context);
                rvDinamicVideo.setAdapter(ar);
                rvDinamicVideo.getAdapter().notifyDataSetChanged();
            } else {

                LinearLayoutManager layout = new LinearLayoutManager(context);
                layout.setOrientation(LinearLayoutManager.HORIZONTAL);
                rvDinamicVideo.setLayoutManager(layout);
                DinamicVideoAdapter ar = new DinamicVideoAdapter(DMV, context);
                rvDinamicVideo.setAdapter(ar);
                rvDinamicVideo.getAdapter().notifyDataSetChanged();

//                if (position == 0) {
//                    LangViewModel model = LangViewModel.getInstance();
//                    showCaseWrapper.showFancyFreeContent(ShowCaseWrapper.CHOOSE_FREE_CONTENT, model.getLang().string.wordingChooseFreeContent, rvDinamicVideo, new ShowCaseListener.IntroListener() {
//                        @Override
//                        public void onOk() {
//
//                        }
//
//                        @Override
//                        public void onCancel() {
//
//                        }
//                    });
//                }

            }
        }

    }
}

