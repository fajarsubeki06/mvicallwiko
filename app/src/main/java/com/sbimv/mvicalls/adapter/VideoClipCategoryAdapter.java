package com.sbimv.mvicalls.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.AllPremiumDinamicVideoActivity;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;

import java.util.List;

public class VideoClipCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<DataDinamicTitle> childList;
    private Context context;
    private DataDinamicTitle child;
    private LangViewModel model = LangViewModel.getInstance();

    public VideoClipCategoryAdapter(List<DataDinamicTitle> childList, Context context) {
        this.childList = childList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (inflater != null) {
            view = inflater.inflate(R.layout.item_videoclip_category, parent, false);
        }
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((VH) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return childList.size();
    }

    class VH extends RecyclerView.ViewHolder {

        private TextView tvClipCategory;
        private TextView tvMore;
        private LinearLayout seeAll;
        private RecyclerView rvClipCat;

        public VH(View itemView) {
            super(itemView);
            tvClipCategory = itemView.findViewById(R.id.tvCatTitle);
            seeAll = itemView.findViewById(R.id.linSeeAll);
            rvClipCat = itemView.findViewById(R.id.rvCatalogVideos);
            tvMore = itemView.findViewById(R.id.tvMoreVideos);
            tvMore.setText(model.getLang().string.moreVideos);
        }

        void bind(int position) {
            child = childList.get(position);
            tvClipCategory.setText(child.getCategory());
            if (child.getChild() != null) {
                seeAll.setVisibility(View.GONE);

                rvClipCat.setLayoutManager(new GridLayoutManager(context, 3));
                VideoClipContentCategoryAdapter ar = new VideoClipContentCategoryAdapter(child.getChild(), context);
                rvClipCat.setAdapter(ar);
                rvClipCat.getAdapter().notifyDataSetChanged();

            } else {
                seeAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, AllPremiumDinamicVideoActivity.class);
                        intent.putExtra("data", child);
                        context.startActivity(intent);
                    }
                });
                LinearLayoutManager layout = new LinearLayoutManager(context);
                layout.setOrientation(LinearLayoutManager.HORIZONTAL);
                rvClipCat.setLayoutManager(layout);
                DinamicVideoAdapter ar = new DinamicVideoAdapter(child, context);
                rvClipCat.setAdapter(ar);
                rvClipCat.getAdapter().notifyDataSetChanged();
            }
        }
    }

}
