package com.sbimv.mvicalls.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lid.lib.LabelImageView;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.PreviewPremiumVideo;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.AllPremiumDinamicVideoActivity;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;
import com.sbimv.mvicalls.walktrough.PreviewFreeVideoCollection;

import java.util.ArrayList;

public class DinamicVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private DataDinamicTitle datas;
    private Context context;
    private LangViewModel model = LangViewModel.getInstance();


    public DinamicVideoAdapter(DataDinamicTitle datas, Context context) {
        this.datas = datas;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        if (datas.getContent() == null){
            return 0;
        }else if (datas.getContent().size() <= 9){
            return datas.getContent().size();
        }else if (datas.getContent().size() >= 9){
            return 11;
        }else {
            return datas.getContent().size()+1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position > 9) ? R.layout.item_see_all : R.layout.item_premium_video;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == R.layout.item_premium_video) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_premium_video, parent, false);
            return new VH(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_see_all, parent, false);
            return new VHButton(view);
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case R.layout.item_premium_video:
                initContent((VH) holder, position);
                break;
            case R.layout.item_see_all:
                initBtnSeeAll((VHButton) holder, position);
                break;
            default:
                break;
        }
    }

    @SuppressLint({"CheckResult", "SetTextI18n"})
    private void initContent(VH vh, int position) {
        final ArrayList<DataDinamicVideos> dataVideoDinamic = datas.getContent();
        final DataDinamicVideos videos = dataVideoDinamic.get(position);

        vh.tvArtist.setText(dataVideoDinamic.get(position).alias);
        vh.tvSong.setText(dataVideoDinamic.get(position).judul);

        String currency = TextUtils.isEmpty(dataVideoDinamic.get(position).price_label)?"Rp.":dataVideoDinamic.get(position).price_label;
        if (dataVideoDinamic.get(position).price.equals("0")) {
            vh.tvHarga.setText(model.getLang().string.free);
            vh.labelImageView.setVisibility(View.VISIBLE);
        } else {
            vh.tvHarga.setText(currency +" "+ dataVideoDinamic.get(position).price);
            vh.labelImageView.setVisibility(View.GONE);
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.background);
        Glide.with(vh.imgVideo.getContext())
                .load(dataVideoDinamic.get(position).thumb_pic)
                .apply(requestOptions)
                .into(vh.imgVideo);

        (vh).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                BaseActivity.contentBuys = false;

                String strPrice = videos.price;
                if(!TextUtils.isEmpty(strPrice)){
                    if (strPrice.equals("0")){
                        Intent intent = new Intent(view.getContext(), PreviewFreeVideoCollection.class);
                        intent.putExtra("dataFreeContent", videos);
                        view.getContext().startActivity(intent);
                    }else {
                        Intent intent = new Intent(view.getContext(), PreviewPremiumVideo.class);
                        intent.putExtra("video", videos);
                        intent.putExtra("title", datas);
                        view.getContext().startActivity(intent);
                    }
                }

            }
        });

    }

    private void initBtnSeeAll(VHButton vhButton, int position) {
        vhButton.tvSeeAll.setText(model.getLang().string.moreVideos);

//        if (position == datas.getContent().size()){
//            vhButton.imgButtonSeeAll.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(context, AllPremiumDinamicVideoActivity.class);
//                    intent.putExtra("data", datas);
//                    context.startActivity(intent);
//                }
//            });
//        }

        vhButton.imgButtonSeeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AllPremiumDinamicVideoActivity.class);
                intent.putExtra("data", datas);
                context.startActivity(intent);
            }
        });
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tvArtist;
        TextView tvSong;
        TextView tvHarga;
        ImageView imgVideo;
        LinearLayout btnSeeAll;
        LabelImageView labelImageView;

        public VH(View itemView) {
            super(itemView);
            tvArtist = itemView.findViewById(R.id.tv_artist);
            tvSong = itemView.findViewById(R.id.tv_title);
            tvHarga = itemView.findViewById(R.id.tv_harga);
            imgVideo = itemView.findViewById(R.id.iv_video);
            btnSeeAll = itemView.findViewById(R.id.btnSeeAll);
            labelImageView = itemView.findViewById(R.id.labelItem);
            labelImageView.setLabelText(model.getLang().string.free);
        }
    }

    public class VHButton extends RecyclerView.ViewHolder {
        LinearLayout btnSeeAll;
        ImageButton imgButtonSeeAll;
        TextView tvSeeAll;


        public VHButton(View view) {
            super(view);
            btnSeeAll = view.findViewById(R.id.btnSeeAll);
            imgButtonSeeAll = view.findViewById(R.id.imgButtonSeeAll);
            tvSeeAll = itemView.findViewById(R.id.tvMoreVid);

        }

    }
}
