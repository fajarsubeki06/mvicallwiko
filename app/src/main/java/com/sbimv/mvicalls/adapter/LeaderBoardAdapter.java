package com.sbimv.mvicalls.adapter;

import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.pojo.DataMGM;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class LeaderBoardAdapter extends RecyclerView.Adapter<LeaderBoardAdapter.VH> {

    private Context context;
    private List<DataMGM.MGMRank> datasList;


    public LeaderBoardAdapter(Context context,List<DataMGM.MGMRank> datasList) {
        this.context = context;
        this.datasList = datasList;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_leaderboard,parent,false);
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        DataMGM.MGMRank datas = datasList.get(position);
        holder.onBind(datas,position);
    }

    @Override
    public int getItemCount() {
        return datasList.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        private TextView tvUserName;
        private TextView tvPhone;
        private TextView tvRank;
        private RelativeLayout rankLayout;
        private CircleImageView userPic;
        private TextView note;


        public VH(View itemView) {
            super(itemView);

            tvUserName = itemView.findViewById(R.id.tvLDName);
            tvPhone = itemView.findViewById(R.id.tvLDPhone);
            tvRank = itemView.findViewById(R.id.tvLDRank);
            rankLayout = itemView.findViewById(R.id.rankLayout);
            userPic = itemView.findViewById(R.id.imgUserMGM);
            note = itemView.findViewById(R.id.tvNoteMGM);
            if (Build.VERSION.SDK_INT >= 26){
                note.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
            }
        }

        private void onBind(DataMGM.MGMRank datas,int position){

            int stringLength = datas.getMsisdn().length();
            String phone = datas.getMsisdn().substring(0,stringLength-5)+"xxxxx";
            String name = datas.getName();

            String[] filtered = name.split(" ");
            StringBuilder nameBuilder = new StringBuilder();
            for (String val : filtered) {
                if (val.length() >= 4) {
                    nameBuilder.append(val.substring(0, val.length() - 3) + "xxx ");
                } else if (val.length() >= 3) {
                    nameBuilder.append(val.substring(0, val.length() - 2) + "xx ");
                } else if (val.length() >= 2) {
                    nameBuilder.append(val.substring(0, val.length() - 1) + "x ");
                }
            }
            tvUserName.setText(nameBuilder);
            tvPhone.setText(phone);
            tvRank.setText("#"+datas.getNo());

            if (position == 0){
                rankLayout.setBackground(context.getResources().getDrawable(R.drawable.rounded_orange_background));
                tvUserName.setTextColor(context.getResources().getColor(R.color.white));
                tvPhone.setTextColor(context.getResources().getColor(R.color.white));
                tvRank.setTextColor(context.getResources().getColor(R.color.white));
            }
        }
    }
}
