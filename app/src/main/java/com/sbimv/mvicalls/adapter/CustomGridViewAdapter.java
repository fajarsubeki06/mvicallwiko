package com.sbimv.mvicalls.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.pojo.ModelCustomGrid;

import java.util.ArrayList;

public class CustomGridViewAdapter extends BaseAdapter {
    private Context mContext;
    ArrayList<ModelCustomGrid> modelCustomGrids;

    public CustomGridViewAdapter(Context context, ArrayList<ModelCustomGrid> modelCustomGrids) {
        mContext = context;
        this.modelCustomGrids = modelCustomGrids;
    }

    @Override
    public int getCount() {
        return modelCustomGrids.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View gridViewAndroid;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            gridViewAndroid = new View(mContext);
            gridViewAndroid = inflater.inflate(R.layout.gridview_layout, null);

            TextView textViewAndroid = gridViewAndroid.findViewById(R.id.android_gridview_text);
            TextView android_gridview_package = gridViewAndroid.findViewById(R.id.android_gridview_package);
            ImageView imageViewAndroid = gridViewAndroid.findViewById(R.id.android_gridview_image);

            ModelCustomGrid model = modelCustomGrids.get(i);
            if (model != null) {
                textViewAndroid.setText(model.name);
                android_gridview_package.setText(model.package_name);
                imageViewAndroid.setImageResource(model.icon);
            }

        } else {
            gridViewAndroid = convertView;
        }

        return gridViewAndroid;
    }

}