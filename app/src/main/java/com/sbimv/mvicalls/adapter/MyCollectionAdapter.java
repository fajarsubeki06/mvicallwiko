package com.sbimv.mvicalls.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.PreviewVideoCollection;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.MyCollection;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 12/27/2017.
 */

public class MyCollectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    static List<MyCollection> data;
    private static Context mContext;
    private LangViewModel model = LangViewModel.getInstance();


    public MyCollectionAdapter(Context context, List<MyCollection> data) {
        MyCollectionAdapter.data = data;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_collection_video, parent, false);
        return new VH(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final MyCollection collection = data.get(position);
        final VH vh = (VH) holder;

        vh.tvArtist.setText(collection.alias);
        vh.tvJudul.setText(collection.judul);
        String subEnd = dateFormater(collection.sub_end);
        vh.tvActive.setText(model.getLang().string.activeTo + "\n" + subEnd);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.background);

        Glide.with(vh.ivVideo.getContext())
                .load(collection.thumb_pic)
                .apply(requestOptions)
                .into(vh.ivVideo);

        vh.img_collection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PreviewVideoCollection.class);
                intent.putExtra("dataCollections", collection);
                mContext.startActivity(intent);
                // google analytics...
                GATRacker.getInstance(((Activity)mContext).getApplication()).sendEventWithScreen(
                        TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
                        TrackerConstant.EVENT_CAT_MY_COLLECTION,
                        "click content",
                        collection.caller_id+"/"+collection.content_id);
            }
        });
    }
    private String dateFormater(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "EEE, dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tvArtist;
        TextView tvJudul;
        TextView tvActive;
        ImageView ivVideo;
        MediaController mediaController;
        VideoView vvPreview;
        CardView cardView;
        ImageView img_collection;

        public VH(View itemView) {
            super(itemView);
            tvArtist = itemView.findViewById(R.id.tv_artist_collection);
            tvJudul = itemView.findViewById(R.id.tv_title_collection);
            tvActive = itemView.findViewById(R.id.tv_date);
            ivVideo = itemView.findViewById(R.id.img_collection);
            cardView = itemView.findViewById(R.id.card_view);
            vvPreview = itemView.findViewById(R.id.vv_preview_collection);
            img_collection = itemView.findViewById(R.id.img_collection);
            mediaController = new MediaController(itemView.getContext());
            mediaController.setAnchorView(mediaController);

        }
    }

}
