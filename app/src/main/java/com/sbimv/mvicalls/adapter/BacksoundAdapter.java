package com.sbimv.mvicalls.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.BackSound;

import java.util.List;


public class BacksoundAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<BackSound> backSoundList;
    private OnItemClickListener listener;
    private int mCurrentPlayingPosition = -1;
    private int listenPosition ;
    private Context context;
    private LangViewModel model = LangViewModel.getInstance();

    public interface OnItemClickListener{
        void onItemPlayMusic(BackSound backSound, ImageView image, int previousPosition, int currentPosition);
        void onItemChoiceMusic(BackSound backSound);
    }

    public BacksoundAdapter(Context context, List<BackSound> backSoundList, OnItemClickListener listener) {
        this.context = context;
        this.backSoundList = backSoundList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater != null ? inflater.inflate(R.layout.item_backsound, parent, false) : null;
        assert view != null;
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ViewHolder vh = (ViewHolder) holder;
        final BackSound backSound = backSoundList.get(position);

        if(mCurrentPlayingPosition == position ){
            vh.imagePlay.setImageResource(R.drawable.ic_pause_button);
            vh.itemView.setBackgroundColor(context.getResources().getColor(R.color.color_orange));
            vh.btnChoose.setBackground(context.getDrawable(R.drawable.bg_button_black));
            vh.textChoose.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            vh.imagePlay.setImageResource(R.drawable.ic_play_button);
            vh.itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
            vh.btnChoose.setBackground(context.getDrawable(R.drawable.bg_stroke_button));
            vh.textChoose.setTextColor(context.getResources().getColor(R.color.color_black));
        }

        String str_select_song = TextUtils.isEmpty(model.getLang().string.str_select_song)
                ? "Pilih Lagu"
                : model.getLang().string.str_select_song;

        vh.textChoose.setText(str_select_song);
        vh.textSound.setText(backSound.getJudul());
        vh.textSinger.setText(backSound.getSinger());
        vh.btnChoose.setOnClickListener(view -> listener.onItemChoiceMusic(backSoundList.get(position)));

        vh.imagePlay.setTag(1);
        vh.imagePlay.setOnClickListener(view -> {

            int PlayStopButtonState = (int) vh.imagePlay.getTag();
            int previousPosition = mCurrentPlayingPosition;

            if (PlayStopButtonState == 1) {
                mCurrentPlayingPosition = vh.getAdapterPosition();
                vh.imagePlay.setImageResource(R.drawable.ic_pause_button);
                vh.imagePlay.setTag(2);
                vh.itemView.setBackgroundColor(context.getResources().getColor(R.color.color_orange));
                vh.btnChoose.setBackground(context.getDrawable(R.drawable.bg_button_black));
                vh.textChoose.setTextColor(context.getResources().getColor(R.color.white));
            } else {
                mCurrentPlayingPosition = -1; // nothing wil be played after hitting stop
                vh.imagePlay.setImageResource(R.drawable.ic_play_button);
                vh.imagePlay.setTag(1);
                vh.itemView.setBackgroundColor(context.getResources().getColor(R.color.color_orange));
                vh.btnChoose.setBackground(context.getDrawable(R.drawable.bg_button_black));
                vh.textChoose.setTextColor(context.getResources().getColor(R.color.white));
            }

            if(previousPosition != -1) {
                notifyItemChanged(previousPosition);
            }

//            notifyItemChanged(selectedPos);
//            selectedPos = vh.getLayoutPosition();
//            notifyItemChanged(selectedPos);

            listener.onItemPlayMusic(backSoundList.get(position), vh.imagePlay, listenPosition, position);
            listenPosition = position;
        });

    }

    @Override
    public int getItemCount() {
        return backSoundList == null ? 0 : backSoundList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private TextView textSound, textChoose, textSinger;
        private LinearLayout btnChoose;
        private ImageView imagePlay;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textSound = itemView.findViewById(R.id.textTitleSound);
            btnChoose = itemView.findViewById(R.id.btnChoose);
            imagePlay = itemView.findViewById(R.id.imagePlay);
            textChoose = itemView.findViewById(R.id.textChoose);
            textSinger = itemView.findViewById(R.id.textTitleArtis);
        }
    }

    public void refreshAdapter(){
        mCurrentPlayingPosition = -1;
    }

}
