package com.sbimv.mvicalls.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.annotations.NotNull;
import com.lid.lib.LabelImageView;
import com.sbimv.mvicalls.PreviewFrontVideo;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;
import com.sbimv.mvicalls.walktrough.PreviewFreeVideoCollection;


import java.util.ArrayList;

public class VideoFrontAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static DataDinamicTitle datas;
    private LangViewModel model = LangViewModel.getInstance();

    public VideoFrontAdapter(DataDinamicTitle data) {
        VideoFrontAdapter.datas = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_premium_video, parent, false);
        return new VH(v);
    }

    @SuppressLint({"CheckResult", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, int position) {
        final ArrayList<DataDinamicVideos> dataVideoDinamic = datas.getContent();
        final DataDinamicVideos dataVideoFront = dataVideoDinamic.get(position);

        VideoFrontAdapter.VH vh = (VideoFrontAdapter.VH) holder;
        vh.tvArtist.setText(dataVideoFront.getAlias());
        vh.tvSong.setText(dataVideoFront.getJudul());

        String currency = TextUtils.isEmpty(dataVideoFront.price_label)?"Rp":dataVideoFront.price_label;
        if (dataVideoFront.price.equals("0")){
            vh.tvHarga.setText(model.getLang().string.free);
            vh.labelImageView.setLabelText(model.getLang().string.free);
            vh.labelImageView.setVisibility(View.VISIBLE);
        }else {
            vh.tvHarga.setText(currency +" "+ dataVideoFront.getPrice());
            vh.labelImageView.setVisibility(View.GONE);
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.background);
        Glide.with(vh.imgVideo.getContext())
                .load(dataVideoFront.getThumb_pic())
                .apply(requestOptions)
                .into(vh.imgVideo);

        ((VideoFrontAdapter.VH) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                String strPrice = dataVideoFront.price;
                if(!TextUtils.isEmpty(strPrice)){
                    if (strPrice.equals("0")){
                        Intent intent = new Intent(view.getContext(), PreviewFreeVideoCollection.class);
                        intent.putExtra("dataFreeContent", dataVideoFront);
                        view.getContext().startActivity(intent);
                    }else {
                        Intent intent = new Intent(view.getContext(), PreviewFrontVideo.class );
                        intent.putExtra("title", datas);
                        intent.putExtra("video",dataVideoFront);
                        view.getContext().startActivity(intent);
                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return (datas == null) ? 0 : datas.getContent().size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tvArtist;
        TextView tvSong;
        TextView tvHarga;
        ImageView imgVideo;
        LabelImageView labelImageView;

        public VH(View itemView) {
            super(itemView);
            tvArtist = itemView.findViewById(R.id.tv_artist);
            tvSong = itemView.findViewById(R.id.tv_title);
            tvHarga = itemView.findViewById(R.id.tv_harga);
            imgVideo = itemView.findViewById(R.id.iv_video);
            labelImageView = itemView.findViewById(R.id.labelItem);
        }
    }
}
