package com.sbimv.mvicalls.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.webkit.WebView;
import android.widget.TextView;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.pojo.DataHowTo;


import java.util.ArrayList;

public class TutorialAdapter
//        extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private Context ctx;
    private ArrayList<DataHowTo> dataHowTos;
    private RecyclerView recyclerView;
    private static final int UNSELECTED = -1;
    private int selectedItem = UNSELECTED;

    public TutorialAdapter(Context context, ArrayList<DataHowTo> dataHowTos,RecyclerView recyclerView) {
        this.ctx = context;
        this.dataHowTos = dataHowTos;
        this.recyclerView = recyclerView;
    }

//    @NonNull
//    @Override
//    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        LayoutInflater layoutInflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View view = layoutInflater.inflate(R.layout.item_tutorial,parent,false);
//        return new ViewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
//        ((ViewHolder)holder).bind();
//    }
//
//    @Override
//    public int getItemCount() {
//        return (dataHowTos == null) ? 0 : dataHowTos.size();
//    }

//    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {
//        ExpandableLayout expandableLayout;
//        TextView title;
//        WebView webView;
//
//        public ViewHolder(View itemView) {
//            super(itemView);
//
//            title = itemView.findViewById(R.id.tvTitleTutor);
//            title.setOnClickListener(this);
//            webView = itemView.findViewById(R.id.wvTutorial);
//            expandableLayout = itemView.findViewById(R.id.expanTutor);
//            expandableLayout.setInterpolator(new OvershootInterpolator());
//            expandableLayout.setOnExpansionUpdateListener(this);
//        }
//        public void bind(){
//            DataHowTo data = dataHowTos.get(getAdapterPosition());
//            int position = getAdapterPosition();
//            boolean isSelected = position == selectedItem;
//
//            title.setText(data.getMenu());
//            webView.loadUrl(data.getUrl());
//            title.setSelected(isSelected);
//            expandableLayout.setExpanded(isSelected,false);
//        }
//        @Override
//        public void onClick(View v) {
//            ViewHolder holder = (ViewHolder) recyclerView.findViewHolderForAdapterPosition(selectedItem);
//            if (holder != null) {
//                holder.title.setSelected(false);
//                holder.expandableLayout.collapse();
//            }
//
//            int position = getAdapterPosition();
//            if (position == selectedItem) {
//                selectedItem = UNSELECTED;
//            } else {
//                title.setSelected(true);
//                expandableLayout.expand();
//                selectedItem = position;
//            }
//        }
//        @Override
//        public void onExpansionUpdate(float expansionFraction, int state) {
//            Log.d("ExpandableLayout", "State: " + state);
//            if (state == ExpandableLayout.State.EXPANDING) {
//                recyclerView.smoothScrollToPosition(getAdapterPosition());
//            }
//        }
//    }
}
