package com.sbimv.mvicalls.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.BestDealActivity;
import com.sbimv.mvicalls.activity.PopUpBestDeals;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.DataBestDeal;

import java.util.ArrayList;

public class BestDealAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<DataBestDeal> dataBestDeals;
    Context context;
    private LangViewModel model = LangViewModel.getInstance();

    public BestDealAdapter(ArrayList<DataBestDeal> dataBestDeals,Context context) {
        this.dataBestDeals = dataBestDeals;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_best_deal,parent,false);
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final DataBestDeal deal = dataBestDeals.get(position);
        final VH vh = (VH) holder;

        Glide.with(vh.imgBanner.getContext()).load(deal.getDeal_banner()).into(vh.imgBanner);

        ((VH) holder).itemView.setOnClickListener(view -> {
            if (deal.getPromo().equals("1")){
                Snackbar.make(vh.imgBanner, model.getLang().string.wordingRedeemPromo, Snackbar.LENGTH_SHORT).show();
            }
            else {
                if (!TextUtils.isEmpty(deal.getErr_message())){
                    DialogMissMatchSIM(deal.getErr_message());
                } else {
                    Intent intent = new Intent(context,PopUpBestDeals.class);
                    intent.putExtra("data",deal);
                    ((Activity)context).startActivityForResult(intent,BestDealActivity.REQUEST_CODE);
                }
            }
        });

    }

    private void DialogMissMatchSIM(String errMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(errMessage);
        builder.setPositiveButton("OK",null);

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(dialog1 -> {
            Button buttonPositive = ((AlertDialog) dialog1).getButton(AlertDialog.BUTTON_POSITIVE);
            buttonPositive.setOnClickListener(v -> dialog1.dismiss());
        });
        dialog.show();

    }

    @Override
    public int getItemCount() {
        return dataBestDeals.size();
    }

    public class VH extends RecyclerView.ViewHolder{
        private ImageView imgBanner;

        public VH(View itemView) {
            super(itemView);

            imgBanner = itemView.findViewById(R.id.imgBanner);

        }
    }

}
