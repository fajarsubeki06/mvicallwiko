package com.sbimv.mvicalls.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.AllPremiumDinamicVideoActivity;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;

import java.util.List;

public class VideoClipContentCategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<DataDinamicTitle> title;
    private Context context;

    public VideoClipContentCategoryAdapter(List<DataDinamicTitle> title, Context context) {
        this.title = title;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_content_clip_category, parent, false);

        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((VH) holder).bind(position);
    }

    @Override
    public int getItemCount() {
        return title.size();
    }

    class VH extends RecyclerView.ViewHolder {

        private Button btnCat;
        //        private CardView btn;
        private RecyclerView rv;

        public VH(View itemView) {
            super(itemView);
            btnCat = itemView.findViewById(R.id.btnClipCat);
            rv = itemView.findViewById(R.id.rvClipCat);
//            btn = itemView.findViewById(R.id.cardClipCat);
        }

        void bind(final int position) {
            final DataDinamicTitle dataVideoClip = title.get(position);

            String cat = dataVideoClip.getCategory();
            btnCat.setText(cat);

            btnCat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, AllPremiumDinamicVideoActivity.class);
                    intent.putExtra("data", dataVideoClip);
                    context.startActivity(intent);
                }
            });

//            LinearLayoutManager layout = new LinearLayoutManager(context);
//            layout.setOrientation(LinearLayoutManager.HORIZONTAL);
//            rv.setLayoutManager(layout);
//            DinamicVideoAdapter ar = new DinamicVideoAdapter(dataVideoClip);
//            rv.setAdapter(ar);
//            rv.getAdapter().notifyDataSetChanged();
        }
    }
}
