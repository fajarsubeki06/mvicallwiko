package com.sbimv.mvicalls.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.activity.DetailInboxActivity;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.InboxModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class InboxAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    static List<InboxModel> data;
    private Context mContext;
    private LangViewModel model = LangViewModel.getInstance();
    private onClickedDeleteListner mListener;


    public InboxAdapter(Context context, List<InboxModel> data, onClickedDeleteListner listener) {
        InboxAdapter.data = data;
        this.mListener = listener;
        this.mContext = context;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_inbox, parent, false);
        return new VH(v);
    }

    @Override
    public void onBindViewHolder(@NotNull final RecyclerView.ViewHolder holder, final int position) {
        final InboxModel collection = data.get(position);
        final VH vh = (VH) holder;

        vh.tv_id.setText(collection.id);
        vh.tv_title.setText(collection.title);
        vh.tv_message.setText(collection.message);
        vh.timestamp.setText(collection.created_date);

        vh.ln_body.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, DetailInboxActivity.class);
            intent.putExtra("dataInbox", collection);
            mContext.startActivity(intent);
        });

        if (collection.status.equals("2")){
            vh.tv_title.setTextColor(mContext.getResources().getColor(R.color.grey_ACACACF));
            vh.tv_message.setTextColor(mContext.getResources().getColor(R.color.grey_ACACACF));
            vh.timestamp.setTextColor(mContext.getResources().getColor(R.color.grey_ACACACF));
            vh.imgStatus.setImageResource(R.mipmap.icx_inbox_read_grey);
            vh.im_trash.setImageResource(R.mipmap.icx_new_trash_grey);
            vh.circle_notice.setVisibility(View.GONE);
        }else {
            vh.tv_title.setTextColor(mContext.getResources().getColor(R.color.black));
            vh.tv_message.setTextColor(mContext.getResources().getColor(R.color.grey_2));
            vh.timestamp.setTextColor(mContext.getResources().getColor(R.color.grey_2));
            vh.imgStatus.setImageResource(R.mipmap.icx_new_inbox);
            vh.im_trash.setImageResource(R.mipmap.icx_new_trash);
            vh.circle_notice.setVisibility(View.VISIBLE);
        }

        vh.im_trash.setOnClickListener(v -> {
            //DELETE MESSAGE
            String uid = collection.id;
            String delete_confirm = model.getLang().string.deleteConfirm;
            String approve_confirm = model.getLang().string.approveCofirm;
            String cancel_confirm = model.getLang().string.cancelConfirm;

            if (!TextUtils.isEmpty(uid) && !TextUtils.isEmpty(delete_confirm)
                    && !TextUtils.isEmpty(approve_confirm) && !TextUtils.isEmpty(cancel_confirm)) {
                mListener.onDelete(delete_confirm, approve_confirm, cancel_confirm, uid);

            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tv_id;
        TextView tv_title;
        TextView tv_message;
        TextView timestamp;
        ImageView im_trash;
        ImageView imgStatus;
        LinearLayout ln_body;
        RelativeLayout circle_notice;

        public VH(View itemView) {
            super(itemView);

            tv_id = itemView.findViewById(R.id.tv_id);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_message = itemView.findViewById(R.id.tv_message);
            timestamp = itemView.findViewById(R.id.timestamp);
            im_trash = itemView.findViewById(R.id.im_trash);
            ln_body = itemView.findViewById(R.id.ln_body);
            imgStatus = itemView.findViewById(R.id.imgStatus);
            circle_notice= itemView.findViewById(R.id.circle_notice);
        }
    }


    public interface onClickedDeleteListner {
        void onDelete(String delete_confirm, String approve_confirm, String cancel_confirm, String uid);
    }

}