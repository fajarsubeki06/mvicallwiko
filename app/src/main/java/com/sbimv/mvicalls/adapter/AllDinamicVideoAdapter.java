package com.sbimv.mvicalls.adapter;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lid.lib.LabelImageView;
import com.sbimv.mvicalls.PreviewPremiumVideo;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;

import java.util.ArrayList;

/**
 * Created by admin on 1/19/2018.
 */

public class AllDinamicVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private DataDinamicTitle datas;
    private LangViewModel model = LangViewModel.getInstance();

    public AllDinamicVideoAdapter(DataDinamicTitle data) {
        this.datas = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item_video_big, parent, false);
        return new VH(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ArrayList<DataDinamicVideos> dataVideoDinamic = datas.getContent();
        final DataDinamicVideos videos = dataVideoDinamic.get(position);

        VH vh = (VH) holder;
        vh.tvJudul.setText(videos.judul);
        vh.tvNama.setText(videos.alias);
        String currency = TextUtils.isEmpty(videos.price_label)? "Rp." : videos.price_label;

        if (videos.price.equals("0")) {
            vh.tvharga.setText(model.getLang().string.free);
            vh.labelImageView.setVisibility(View.VISIBLE);
        } else {
            vh.tvharga.setText(currency +" "+ videos.price);
            vh.labelImageView.setVisibility(View.GONE);
        }

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.background);

        Glide.with(vh.imgVideo.getContext())
                .load(videos.thumb_pic)
                .apply(requestOptions)
                .into(vh.imgVideo);

        ((VH) holder).itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), PreviewPremiumVideo.class);
                intent.putExtra("video", videos);
                intent.putExtra("title", datas);
                view.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (datas.getContent().size()<= 0) ? 0 : datas.getContent().size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tvJudul;
        TextView tvNama;
        TextView tvharga;
        ImageView imgVideo;
        LabelImageView labelImageView;

        public VH(View itemView) {
            super(itemView);
            tvJudul = itemView.findViewById(R.id.tv_title_menu);
            tvNama = itemView.findViewById(R.id.tv_artist_big);
            tvharga = itemView.findViewById(R.id.tv_harga_big);
            imgVideo = itemView.findViewById(R.id.iv_video_big);
            labelImageView = itemView.findViewById(R.id.labelItemBig);
            labelImageView.setLabelText(model.getLang().string.free);
        }
    }
}
