package com.sbimv.mvicalls.contact;

import android.net.Uri;

public interface OnContactChanged {
    void onContactChanged(Uri contact);
}
