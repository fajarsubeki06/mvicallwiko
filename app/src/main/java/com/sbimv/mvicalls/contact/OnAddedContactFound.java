package com.sbimv.mvicalls.contact;

public interface OnAddedContactFound
{
    void onAddedContactFound(Long result);
}