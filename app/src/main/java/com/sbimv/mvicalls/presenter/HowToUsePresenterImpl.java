package com.sbimv.mvicalls.presenter;


import android.text.TextUtils;
import com.sbimv.mvicalls.contract.MainContract;


public class HowToUsePresenterImpl implements MainContract.HowToUsePresenter, MainContract.Intractor.OnHowToUseFinishedListener {

    private MainContract.HowToUseView howToUseView;
    private MainContract.Intractor intractor;

    public HowToUsePresenterImpl(MainContract.HowToUseView mHowToUseView, MainContract.Intractor intractor){
        this.howToUseView = mHowToUseView;
        this.intractor = intractor;
    }


    @Override
    public void setDataHowToUse(String lang) {
        if (!TextUtils.isEmpty(lang)){
            intractor.getHowToUse(lang, this);
        }else {
            howToUseView.isFailure();
            howToUseView.isFinish();
        }
    }

    @Override
    public void onProgress() {
        howToUseView.isProgress();
    }

    @Override
    public void onFinish() {
        howToUseView.isFinish();
    }

    @Override
    public void onEmpty() {
        howToUseView.isEmpty();
    }

    @Override
    public void onSuccess(String videoId, String urlHowToUse, String urlFaq) {
        howToUseView.isSuccess(videoId,urlHowToUse,urlFaq);
    }

    @Override
    public void onFailure() {
        howToUseView.isFailure();
    }
}