package com.sbimv.mvicalls.presenter;


import android.text.TextUtils;

import com.sbimv.mvicalls.contract.MainContract;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataBestDeal;

import java.util.List;

public class BestDealPresenterImpl implements MainContract.BestDealPresenter, MainContract.Intractor.OnBestDealFinishedListener {

    private MainContract.BestDealView bestDealView;
    private MainContract.Intractor intractor;

    public BestDealPresenterImpl(MainContract.BestDealView mBestDealView, MainContract.Intractor intractor){
        this.bestDealView = mBestDealView;
        this.intractor = intractor;
    }

    @Override
    public void setDataBestDeal(ContactItem contactItem, String deal_id) {
        if (contactItem != null) {
            String msisdn = TextUtils.isEmpty(contactItem.msisdn)?"":contactItem.msisdn;
            if (!TextUtils.isEmpty(msisdn)) {
                intractor.getBestDeal(msisdn, deal_id, this);
                return;
            }
        }

        bestDealView.isFailure();
        bestDealView.isFinish();

    }

    @Override
    public void onProgress() {
        bestDealView.isProgress();
    }

    @Override
    public void onFinish() {
        bestDealView.isFinish();
    }

    @Override
    public void onEmpty() {
        bestDealView.isEmpty();
    }

    @Override
    public void onSuccess(List<DataBestDeal> data) {
        if (data != null && data.size() > 0) {
            bestDealView.isSuccess(data);
        }else {
            bestDealView.isFailure();
            bestDealView.isFinish();
        }
    }

    @Override
    public void onFailure() {
        bestDealView.isFailure();
    }

}