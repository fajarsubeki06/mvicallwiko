package com.sbimv.mvicalls.presenter;


import android.text.TextUtils;
import com.sbimv.mvicalls.contract.MainContract;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.InboxModel;

import java.util.List;


public class InboxPresenterImpl implements MainContract.InboxPresenter, MainContract.Intractor.OnInboxFinishedListener {

    private MainContract.InboxView inboxView;
    private MainContract.Intractor intractor;

    public InboxPresenterImpl(MainContract.InboxView mInboxView, MainContract.Intractor intractor){
        this.inboxView = mInboxView;
        this.intractor = intractor;
    }

    @Override
    public void setDataInbox(ContactItem contactItem, String type, String id) {
        if (contactItem == null)
            return;

        String msisdn = TextUtils.isEmpty(contactItem.msisdn)?"":contactItem.msisdn;
        if (TextUtils.isEmpty(msisdn))
            return;

        intractor.getInbox(msisdn, type, id, this);
    }

    @Override
    public void readDataInbox(ContactItem contactItem, String type, InboxModel inboxModel) {
        if (contactItem == null || inboxModel == null)
            return;

        String msisdn = TextUtils.isEmpty(contactItem.msisdn)?"":contactItem.msisdn;
        String id = TextUtils.isEmpty(inboxModel.id)?"":inboxModel.id;
        if (TextUtils.isEmpty(msisdn) || TextUtils.isEmpty(id))
            return;

        intractor.getInbox(msisdn, type, id, this);

    }

    @Override
    public void onProgress() {
        inboxView.isProgress();
    }

    @Override
    public void onFinish() {
        inboxView.isFinish();
    }

    @Override
    public void onEmpty() {
        inboxView.isEmpty();
    }

    @Override
    public void onSuccess(List dataArrayList) {
        if (dataArrayList.size() >0){
            inboxView.isSuccess(dataArrayList);
        }else {
            inboxView.isFailure();
        }
    }

    @Override
    public void onDetailSuccess(List<InboxModel> dataModelList) {
        if (dataModelList != null && dataModelList.size() > 0) {
            for (int i = 0; i < dataModelList.size(); i++) {
                String strTitle = dataModelList.get(i).title;
                String strMessage = dataModelList.get(i).message;
                String strCreateDate = dataModelList.get(i).created_date;

                inboxView.isDetailSuccess(strTitle, strMessage, strCreateDate);

            }
        }else {
            inboxView.isFailure();
        }
    }

    @Override
    public void onFailure() {
        inboxView.isFailure();
    }
}
