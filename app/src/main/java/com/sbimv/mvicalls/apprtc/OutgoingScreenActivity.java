package com.sbimv.mvicalls.apprtc;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.databinding.DataBindingUtil;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.databinding.ActivityOutgoingScreenBinding;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.view.CallerViewUtil;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallListener;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class OutgoingScreenActivity extends BaseActivity implements View.OnClickListener {

    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private UpdateCallDurationTask mDurationTask;

    private String mCallId;
    private long mCallStart = 0;
    private boolean speakIsActive = false;
    private boolean micIsActive = false;
    private String sMsisdn;
    private String sName;

    private TextView mCallDuration;
    private TextView mCallStatus;
    private TextView mCallState;
    private TextView mCallerName;
    private ImageButton endCallButton;
    private ImageView imgBackground;
    private ToggleButton btnSpeaker;
    private ToggleButton btnMic;
    private CircleImageView userPic;
    private LinearLayout lnCallContainer;
    private LinearLayout lnButtonOutgoing;

    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            OutgoingScreenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_IGNORE_CHEEK_PRESSES;

        getWindow().addFlags(flags);
        requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.hide();
        }

        ActivityOutgoingScreenBinding binding = DataBindingUtil.setContentView(OutgoingScreenActivity.this, R.layout.activity_outgoing_screen);
        model = LangViewModel.getInstance();
        binding.setLangModel(model);

        initial();
    }

    private void initial() {
        mCallDuration = findViewById(R.id.callDuration);
        mCallerName = findViewById(R.id.remoteUser);
        mCallStatus = findViewById(R.id.callStatus);
        mCallState = findViewById(R.id.callState);
        endCallButton = findViewById(R.id.hangupButton);
        imgBackground = findViewById(R.id.imgBackground);
        userPic = findViewById(R.id.userPic);
        btnSpeaker = findViewById(R.id.btn_speaker);
        btnMic = findViewById(R.id.btn_mic);

        // Property layout......
        lnCallContainer = findViewById(R.id.ln_call_container);
        lnButtonOutgoing = findViewById(R.id.ln_button_outgoing);

        initialAction();
    }

    private void initialAction() {

        lnCallContainer.setVisibility(View.VISIBLE);
        lnButtonOutgoing.setVisibility(View.VISIBLE);

        mAudioPlayer = new AudioPlayer(this);
        btnSpeaker.setOnClickListener(this);
        btnMic.setOnClickListener(this);
        endCallButton.setOnClickListener(this);
        String strStateOutgoingUnknown = TextUtils.isEmpty(model.getLang().string.stateUnknowAppRtc) ? "Unknown Number" : model.getLang().string.stateUnknowAppRtc;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            mCallId = bundle.getString(SinchService.CALL_ID);
            String mCaller_id = bundle.getString("id");

            if (!TextUtils.isEmpty(mCaller_id)){
                ContactItem item = ContactDBManager.getInstance(OutgoingScreenActivity.this).getCallerByID(mCaller_id);
                if (item != null) {

                    sMsisdn = TextUtils.isEmpty(item.msisdn) ? "" : item.msisdn;
                    sName = TextUtils.isEmpty(item.name) ? strStateOutgoingUnknown : item.name;
                    String sPic = TextUtils.isEmpty(item.caller_pic) ? "" : item.caller_pic;

                    mCallerName.setText(sName);
                    showBackgroundScreen(sPic);

                }else {
                    mCallerName.setText(strStateOutgoingUnknown);
                    showBackgroundScreen("");
                }
            }else{
                mCallerName.setText(strStateOutgoingUnknown);
                showBackgroundScreen("");
            }
        }

        String strNotifTitleOutgoing = TextUtils.isEmpty(model.getLang().string.notifTitleOutgoing) ? "Calling" : model.getLang().string.notifTitleOutgoing;
        String strNotifContentOutgoing = TextUtils.isEmpty(model.getLang().string.notifContentOutgoing) ? "Calling to" : model.getLang().string.notifContentOutgoing;
        showNotification(strNotifTitleOutgoing,strNotifContentOutgoing+" "+sName, "outgoing");

        try{
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_OUT_APP2APP); // Tracker screen google analitycs...
            mCallStart = System.currentTimeMillis();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void outGoingCallProcess(final String msisdn){
        new Handler().postDelayed(() -> {

            ContactItem item = ContactDBManager.getInstance(OutgoingScreenActivity.this).getCallerByMSISDN(msisdn);
            if (item == null)
                return;

            CallerViewUtil.getInstance(getApplicationContext()).showView(msisdn, true,false);

            ContactItem own = SessionManager.getProfile(OutgoingScreenActivity.this);
            if (own != null) {
                try {
                    GATRacker.getInstance(getApplication()) // Tracker event google analitycs...
                            .sendEventWithScreen(TrackerConstant.SCREEN_OUT_APP2APP,
                                    TrackerConstant.EVENT_CAT_STATUS_CALLER,
                                    "show status contact", own.msisdn);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, 500);
    }

    @Override
    public void onClick(View v) {
        if (btnSpeaker == v){
           if (speakIsActive){
               if(getSinchServiceInterface() != null){
                   speakIsActive = false;
                   getSinchServiceInterface().settSpeaker(false);
                   btnSpeaker.setSelected(false);
               }
           }else {
               if(getSinchServiceInterface() != null) {
                   speakIsActive = true;
                   getSinchServiceInterface().settSpeaker(true);
                   btnSpeaker.setSelected(true);
               }
           }

        }else if (btnMic == v){

            btnMic.isSelected();
            if (micIsActive){
                if(getSinchServiceInterface() != null) {
                    micIsActive = false;
                    getSinchServiceInterface().settMicrophone(true);
                    btnMic.setSelected(false);
                }
            }else {
                if(getSinchServiceInterface() != null) {
                    micIsActive = true;
                    getSinchServiceInterface().settMicrophone(false);
                    btnMic.setSelected(true);
                }
            }

        }else if (endCallButton == v){
            endCall();
        }

    }

    @SuppressLint("CheckResult")
    private void showBackgroundScreen(String url){
        try {

            RequestOptions defBanner = new RequestOptions();
            defBanner.placeholder(R.drawable.newversinbg_);
            Glide.with(getApplicationContext())
                    .load(url)
                    .apply(defBanner)
                    .into(imgBackground);

            // Profile Picture
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.userman);
            Glide.with(getApplicationContext())
                    .load(url).apply(requestOptions)
                    .into(userPic);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onServiceConnected() {
        if(getSinchServiceInterface() != null) {
            if (!TextUtils.isEmpty(mCallId)) {
                try {
                    Call call = getSinchServiceInterface().getCall(mCallId);
                    if (call != null) {
                        call.addCallListener(new SinchCallListener());
                        String strStatusIncoming = TextUtils.isEmpty(model.getLang().string.statusIncomingAppRtc) ? "App to App Call" : model.getLang().string.statusIncomingAppRtc;
                        String strStateOutgoingCalling = TextUtils.isEmpty(model.getLang().string.stateCallingAppRtc) ? "Calling" : model.getLang().string.stateCallingAppRtc;

                        mCallStatus.setText(strStatusIncoming);
                        mCallState.setText(strStateOutgoingCalling);

                        if (getSinchServiceInterface() != null) {
                            getSinchServiceInterface().settSpeaker(false);
                            getSinchServiceInterface().settMicrophone(true);
                        }

                        btnSpeaker.setSelected(false);
                        btnMic.setSelected(false);

                        if (!TextUtils.isEmpty(sMsisdn)) {
                            outGoingCallProcess(sMsisdn);
                        }

                    } else {
                        clearNotification();
                        CallerViewUtil.getInstance(getApplicationContext()).dismissView();
                        finish();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.STATE_INCALL_RTC_CALL");
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(showLoadingBR, _if);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(showLoadingBR);
        CallerViewUtil.getInstance(getApplicationContext()).dismissView();
        endCall();
        if(mDurationTask != null && mTimer != null){
            mDurationTask.cancel();
            mTimer.cancel();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        CallerViewUtil.getInstance(getApplicationContext()).dismissView();
    }

    private BroadcastReceiver showLoadingBR = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (!OutgoingScreenActivity.this.isFinishing()) {
                    String strState = intent.getStringExtra("state");
                    if (!TextUtils.isEmpty(strState)){
                        if (strState.equals("decline")){
                            endCall();
                        }
                    }
                }
            }
        }
    };

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private void endCall() {
        if (mAudioPlayer != null) {
            mAudioPlayer.stopProgressTone();
        }
        if(getSinchServiceInterface() != null) {
            if (!TextUtils.isEmpty(mCallId)) {
                try {
                    Call call = getSinchServiceInterface().getCall(mCallId);
                    if (call != null) {
                        call.hangup();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        clearNotification();
        CallerViewUtil.getInstance(getApplicationContext()).dismissView();
        finish();
    }

    private String formatTimespan(long timespan) throws Exception {
        long totalSeconds = timespan / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        if (mCallStart > 0) {
            try {
                mCallDuration.setText(formatTimespan(System.currentTimeMillis() - mCallStart));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private class SinchCallListener implements CallListener {

        @Override
        public void onCallEnded(Call call) {
            if (mAudioPlayer != null) {
                mAudioPlayer.stopProgressTone();
            }
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            endCall();
        }

        @Override
        public void onCallEstablished(Call call) {
            CallerViewUtil.getInstance(getApplicationContext()).dismissView();

            if (mAudioPlayer != null) {
                mAudioPlayer.stopProgressTone();
            }

            String strStateOutgoingConnected = TextUtils.isEmpty(model.getLang().string.stateConnectedAppRtc) ? "Connected" : model.getLang().string.stateConnectedAppRtc;
            mCallState.setText(strStateOutgoingConnected);

            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

            try {
                mCallStart = System.currentTimeMillis();
                mTimer = new Timer();
                mDurationTask = new UpdateCallDurationTask();
                mTimer.schedule(mDurationTask, 0, 500);
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        @Override
        public void onCallProgressing(Call call) {
            String strStateOutgoingRinging = TextUtils.isEmpty(model.getLang().string.stateRingingAppRtc) ? "Ringing" : model.getLang().string.stateRingingAppRtc;
            mCallState.setText(strStateOutgoingRinging);
            if (mAudioPlayer != null) {
                mAudioPlayer.playProgressTone();
            }
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. FCM
        }
    }
}
