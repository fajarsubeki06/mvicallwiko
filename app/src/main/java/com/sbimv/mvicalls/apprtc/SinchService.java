package com.sbimv.mvicalls.apprtc;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.sinch.android.rtc.ClientRegistration;
import com.sinch.android.rtc.NotificationResult;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.SinchClientListener;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallClientListener;

import java.util.Map;

public class SinchService extends Service {

    private static final String APP_KEY = "817d7ff9-8577-4de1-ab65-0905f921870e";
    private static final String APP_SECRET = "gKCMz5mA6UKr2PTl1hc+kw==";
    private static final String ENVIRONMENT_STAGGING = "sandbox.sinch.com";
    private static final String ENVIRONMENT_PRODUCTION = "clientapi.sinch.com";

    public static final String LOCATION = "LOCATION";
    public static final String CALL_ID = "CALL_ID";
    static final String TAG = SinchService.class.getSimpleName();

    private SinchServiceInterface mSinchServiceInterface = new SinchServiceInterface();
    private SinchClient mSinchClient;
    private String mUserId;
    private PersistedSettings mSettings;

    private StartFailedListener mListener;

    @Override
    public void onCreate() {
        super.onCreate();
        mSettings = new PersistedSettings(getApplicationContext());
    }

    @Override
    public void onDestroy() {
        if (mSinchClient != null && mSinchClient.isStarted()) {
            mSinchClient.terminate();
        }
        super.onDestroy();
    }

    private void createClient(String username) throws Exception {
        mSinchClient = Sinch.getSinchClientBuilder().context(getApplicationContext()).userId(username)
                .applicationKey(APP_KEY)
                .applicationSecret(APP_SECRET)
                .environmentHost(ENVIRONMENT_PRODUCTION).build();

        mSinchClient.setSupportCalling(true);
        mSinchClient.setSupportManagedPush(true);
        mSinchClient.setSupportActiveConnectionInBackground(true);
        mSinchClient.startListeningOnActiveConnection();
        mSinchClient.checkManifest();

        mSinchClient.addSinchClientListener(new MySinchClientListener());
        mSinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());
    }


    private void start(String userName) throws Exception {

        if (mSinchClient == null) {
            mSettings.setUsername(userName);
            mUserId = userName;
            try {
                createClient(userName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.d(TAG, "Starting SinchClient");
        mSinchClient.start();
    }

    private void changeSettSpeaker(boolean isActive) {
        if (isActive){
            mSinchClient.getAudioController().enableSpeaker();
        }else {
            mSinchClient.getAudioController().disableSpeaker();
        }
    }

    private void changeSettMicrophone(boolean isActive) {
        if (isActive){
            mSinchClient.getAudioController().unmute();
        }else {
            mSinchClient.getAudioController().mute();
        }
    }

    private void stop() {
        if (mSinchClient != null) {
            mSinchClient.terminate();
            mSinchClient = null;
        }
    }

    private boolean isStarted() {
        return (mSinchClient != null && mSinchClient.isStarted());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mSinchServiceInterface;
    }

    public class SinchServiceInterface extends Binder {

        public Call callPhoneNumber(String phoneNumber) {
            return mSinchClient.getCallClient().callPhoneNumber(phoneNumber);
        }

        public Call callUser(String userId) {
            return mSinchClient.getCallClient().callUser(userId);
        }

        public Call callUser(String userId, Map<String, String> headers) {
            return mSinchClient.getCallClient().callUser(userId, headers);
        }

        public String getUserName() {
            return mUserId;
        }

        public boolean isStarted() {
            return SinchService.this.isStarted();
        }

        public void startClient(String userName) {
            try {
                start(userName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void settSpeaker(boolean isActive) {
            changeSettSpeaker(isActive);
        }

        public void settMicrophone(boolean isActive) {
            changeSettMicrophone(isActive);
        }
        public void stopClient() {
            stop();
        }

        public void setStartListener(StartFailedListener listener) {
            mListener = listener;
        }

        public Call getCall(String callId) {
            return mSinchClient.getCallClient().getCall(callId);
        }

        public NotificationResult relayRemotePushNotificationPayload(final Map payload) {
            if (mSinchClient == null && !mSettings.getUsername().isEmpty()) {

                try {
                    createClient(mSettings.getUsername());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (mSinchClient == null && mSettings.getUsername().isEmpty()) {
                Log.e(TAG, "Can't start a SinchClient as no username is available, unable to relay push.");
                return null;
            }
            return mSinchClient.relayRemotePushNotificationPayload(payload);
        }

    }

    public interface StartFailedListener {
        void onStartFailed(SinchError error);
        void onStarted();
    }

    private class MySinchClientListener implements SinchClientListener {

        @Override
        public void onClientFailed(SinchClient client, SinchError error) {
            if (mListener != null) {
                mListener.onStartFailed(error);
            }
            mSinchClient.terminate();
            mSinchClient = null;
        }

        @Override
        public void onClientStarted(SinchClient client) {
            Log.d(TAG, "SinchClient started");
            if (mListener != null) {
                mListener.onStarted();
            }
        }

        @Override
        public void onClientStopped(SinchClient client) {
            Log.d(TAG, "SinchClient stopped");
        }

        @Override
        public void onLogMessage(int level, String area, String message) {
            switch (level) {
                case Log.DEBUG:
                    Log.d(area, message);
                    break;
                case Log.ERROR:
                    Log.e(area, message);
                    break;
                case Log.INFO:
                    Log.i(area, message);
                    break;
                case Log.VERBOSE:
                    Log.v(area, message);
                    break;
                case Log.WARN:
                    Log.w(area, message);
                    break;
            }
        }

        @Override
        public void onRegistrationCredentialsRequired(SinchClient client,
                                                      ClientRegistration clientRegistration) {
        }
    }

    private class SinchCallClientListener implements CallClientListener {

        @Override
        public void onIncomingCall(CallClient callClient, Call call) {
            if (call != null) {
                String mCallId = call.getCallId();
                String mCallerId = call.getRemoteUserId();
                if (!TextUtils.isEmpty(mCallId) && !TextUtils.isEmpty(mCallerId)) {
                    Intent intent = new Intent(SinchService.this, IncomingScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(CALL_ID, call.getCallId());
                    intent.putExtra("id", call.getRemoteUserId());
                    SinchService.this.startActivity(intent);
                }

            }
        }
    }

    private class PersistedSettings {

        public SharedPreferences mStore;
        public static final String PREF_KEY = "Sinch";

        public PersistedSettings(Context context) {
            mStore = context.getSharedPreferences(PREF_KEY, MODE_PRIVATE);
        }

        public String getUsername() {
            return mStore.getString("Username", "");
        }

        public void setUsername(String username) {
            SharedPreferences.Editor editor = mStore.edit();
            editor.putString("Username", username);
            editor.commit();
        }
    }

}
