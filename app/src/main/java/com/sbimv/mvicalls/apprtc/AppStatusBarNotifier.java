package com.sbimv.mvicalls.apprtc;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.icallservices.NotificationBroadcastReceiver;

import static com.sbimv.mvicalls.icallservices.NotificationBroadcastReceiver.ACTION_ANSWER_VOICE_INCOMING_CALL;
import static com.sbimv.mvicalls.icallservices.NotificationBroadcastReceiver.ACTION_ANSWER_VOICE_INCOMING_CALL_RTC;
import static com.sbimv.mvicalls.icallservices.NotificationBroadcastReceiver.ACTION_DECLINE_INCOMING_CALL_RTC;
import static com.sbimv.mvicalls.icallservices.NotificationBroadcastReceiver.ACTION_HANG_UP_ONGOING_CALL;


public class AppStatusBarNotifier {

    private static final int RTC_IN_CALL = 10;
    private static final int RTC_INCOMING_CALL = 20;
    private static AppStatusBarNotifier instance;
    private Context applicationContext;


    private AppStatusBarNotifier(Context applicationContext){
        this.applicationContext = applicationContext;
    }

    public static AppStatusBarNotifier getInstance(Context appContext){
        if(instance == null)
            instance = new AppStatusBarNotifier(appContext);
        return instance;
    }

    public void updateNotification(String title, String content, String state, Class<?> cls) {
        updateInCallNotification(title,content,state,cls);
    }

    private void updateInCallNotification(String title, String content, String state, Class<?> cls) {
        showNotification(title,content,state,cls);
    }

    public void clearAllNotifications(){
        NotificationManager notificationManager = (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancel(RTC_IN_CALL);
            notificationManager.cancel(RTC_INCOMING_CALL);
        }
    }

    public void showNotification(String title, String content, String state, Class<?> cls){

        NotificationManager mNotificationManager = (NotificationManager) applicationContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (mNotificationManager != null) {

            final Notification.Builder builder = getNotificationBuilder();
            final PendingIntent inCallPendingIntent = createLaunchPendingIntent(cls);
            builder.setContentIntent(inCallPendingIntent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel("", "sinchrtc_notif", importance);
                builder.setAutoCancel(false);
                builder.setContentText(content);
                builder.setSmallIcon(R.drawable.ic_call_white_24dp);
                builder.setContentTitle(title);
                builder.setChannelId("");
                mNotificationManager.createNotificationChannel(mChannel);

            } else {

                builder.setContentText(content);
                builder.setSmallIcon(R.drawable.ic_call_white_24dp);
                builder.setContentTitle(title);
                builder.setCategory(Notification.CATEGORY_CALL);
                builder.setColor(applicationContext.getResources().getColor(R.color.dialer_theme_color));
                builder.build();

            }

            if (!TextUtils.isEmpty(state)){
                if (state.equals("incoming")){
                    addAnswerAction(builder);
                    addHangupAction(builder);
                }else if (state.equals("outgoing")){
                    addHangupAction(builder);
                }else {
                    addHangupAction(builder);
                }
            }

            Notification notification = builder.build();
            mNotificationManager.notify(RTC_IN_CALL, notification);
        }

    }

    private void addAnswerAction(Notification.Builder builder) {
        PendingIntent answerVoicePendingIntent = createNotificationPendingIntent(applicationContext, ACTION_ANSWER_VOICE_INCOMING_CALL_RTC);
        builder.addAction(R.drawable.ic_call_white_24dp, applicationContext.getText(R.string.notification_action_answer), answerVoicePendingIntent);
    }

    private void addHangupAction(Notification.Builder builder) {
        PendingIntent hangupPendingIntent = createNotificationPendingIntent(applicationContext, ACTION_DECLINE_INCOMING_CALL_RTC);
        builder.addAction(R.drawable.ic_call_end_white_24dp, applicationContext.getText(R.string.notification_action_end_call), hangupPendingIntent);
    }

    private static PendingIntent createNotificationPendingIntent(Context context, String action) {
        final Intent intent = new Intent(action, null, context, NotificationBroadcastReceiver.class);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    private PendingIntent createLaunchPendingIntent(Class<?> cls) {
        final Intent intent = getInCallIntent(cls);
        return PendingIntent.getActivity(applicationContext, 0, intent, 0);
    }

    private Intent getInCallIntent(Class<?> cls) {
        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setClass(applicationContext, cls);
        return intent;
    }

    private Notification.Builder getNotificationBuilder() {
        final Notification.Builder builder = new Notification.Builder(applicationContext);
        builder.setOngoing(true);
        builder.setPriority(Notification.PRIORITY_HIGH);
        return builder;
    }

}
