package com.sbimv.mvicalls.apprtc;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.databinding.DataBindingUtil;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.databinding.ActivityIncomingScreenBinding;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.view.CallerViewUtil;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallListener;

import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class IncomingScreenActivity extends BaseActivity {

    static final String TAG = IncomingScreenActivity.class.getSimpleName();
    private String mCallId;
    private AudioPlayer mAudioPlayer;
    private String sMsisdn;
    private String sName;
    private String mCaller_id;
    private boolean speakIsActive = false;
    private boolean micIsActive = false;
    private long mCallStart = 0;
    private Timer mTimer;
    private IncomingScreenActivity.UpdateCallDurationTask mDurationTask;

    private ImageButton answer;
    private ImageButton decline;
    private ImageButton endCallButton;
    private ToggleButton btnSpeaker;
    private ToggleButton btnMic;

    private ImageView profilePic;
    private TextView remoteUser;
    private TextView mCallState;
    private TextView mCallDuration;
    private TextView mCallStatus;
    private ImageView mImageBackground;

    private LinearLayout lnCallContainer;
    private LinearLayout lnButtonOutgoing;
    private LinearLayout lnButtonIncoming;

    private class UpdateCallDurationTask extends TimerTask {
        @Override
        public void run() {
            IncomingScreenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_IGNORE_CHEEK_PRESSES;

        getWindow().addFlags(flags);
        requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.hide();
        }

        ActivityIncomingScreenBinding binding = DataBindingUtil.setContentView(IncomingScreenActivity.this, R.layout.activity_incoming_screen);
        model = LangViewModel.getInstance();
        binding.setLangModel(model);

        initial();
        setPlayRingtone();
    }

    private void initial(){

        mImageBackground = findViewById(R.id.img_bg_dialer);
        remoteUser = findViewById(R.id.remoteUser);
        mCallStatus = findViewById(R.id.callStatus);
        mCallState = findViewById(R.id.callState);
        mCallDuration = findViewById(R.id.callDuration);

        profilePic = findViewById(R.id.profilePic);
        answer = findViewById(R.id.answerButton);
        decline = findViewById(R.id.declineButton);
        endCallButton = findViewById(R.id.hangupButton);
        btnSpeaker = findViewById(R.id.btn_speaker);
        btnMic = findViewById(R.id.btn_mic);

        // Property layout......
        lnCallContainer = findViewById(R.id.ln_call_container);
        lnButtonOutgoing = findViewById(R.id.ln_button_outgoing);
        lnButtonIncoming = findViewById(R.id.ln_button_incoming);

        initialAction();
    }

    private void initialAction() {

        isAnswareCall(false);
        String strStateOutgoingUnknown = TextUtils.isEmpty(model.getLang().string.stateUnknowAppRtc) ? "Unknow Number" : model.getLang().string.stateUnknowAppRtc;
        String strStatusIncoming = TextUtils.isEmpty(model.getLang().string.statusIncomingAppRtc) ? "App to App Call" : model.getLang().string.statusIncomingAppRtc;
        String strStateIncoming = TextUtils.isEmpty(model.getLang().string.stateIncomingAppRtc) ? "Incoming Call" : model.getLang().string.stateIncomingAppRtc;

        mCallStatus.setText(strStatusIncoming);
        mCallState.setText(strStateIncoming);
        answer.setOnClickListener(mClickListener);
        decline.setOnClickListener(mClickListener);
        endCallButton.setOnClickListener(mClickListener);
        btnSpeaker.setOnClickListener(mClickListener);
        btnMic.setOnClickListener(mClickListener);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mCallId = bundle.getString(SinchService.CALL_ID);
            mCaller_id = bundle.getString("id");

            if (!TextUtils.isEmpty(mCaller_id)){
                ContactItem item = ContactDBManager.getInstance(IncomingScreenActivity.this).getCallerByID(mCaller_id);
                if (item != null) {

                    sMsisdn = item.msisdn;
                    sName = TextUtils.isEmpty(item.name) ? strStateOutgoingUnknown : item.name;
                    String sPic = TextUtils.isEmpty(item.caller_pic) ? "" : item.caller_pic;

                    remoteUser.setText(sName);
                    showBackgroundScreen(sPic);

                }else {
                    remoteUser.setText(strStateOutgoingUnknown);
                    showBackgroundScreen("");
                }
            }else {
                remoteUser.setText(strStateOutgoingUnknown);
                showBackgroundScreen("");
            }
        }

        try{
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_IN_APP2APP); // Tracker screen google analitycs...
            mCallStart = System.currentTimeMillis();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.STATE_INCALL_RTC_CALL");
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(actionImcomingBR, _if);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(actionImcomingBR);
        CallerViewUtil.getInstance(getApplicationContext()).dismissView();
        declineClicked();
        if(mDurationTask != null && mTimer != null) {
            mDurationTask.cancel();
            mTimer.cancel();
        }

        currentRingMode();
        try {
            dismissVideoCaller();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        CallerViewUtil.getInstance(getApplicationContext()).dismissView();
    }

    private BroadcastReceiver actionImcomingBR = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (!IncomingScreenActivity.this.isFinishing()) {
                    String strState = intent.getStringExtra("state");
                    if (!TextUtils.isEmpty(strState)){
                        if (strState.equals("answare")){
                            answerClicked();
                        }else {
                            declineClicked();
                        }
                    }
                }
            }
        }
    };

    private void isAnswareCall(boolean isConnected){
        if(isConnected){
            lnCallContainer.setVisibility(View.VISIBLE);
            lnButtonIncoming.setVisibility(View.GONE);
            lnButtonOutgoing.setVisibility(View.VISIBLE);
        }else {
            lnCallContainer.setVisibility(View.GONE);
            lnButtonOutgoing.setVisibility(View.GONE);
            lnButtonIncoming.setVisibility(View.VISIBLE);
        }
    }

    public void inCommingProcess(final String msisdn) {
        new Handler().postDelayed(() -> {
            ContactItem item = ContactDBManager.getInstance(IncomingScreenActivity.this).getCallerByMSISDN(msisdn);
            if (item == null) {
                CallerViewUtil.getInstance(getApplicationContext()).dismissView();
            } else {
                CallerViewUtil.getInstance(getApplicationContext()).showView(msisdn, true,true);

                ContactItem own = SessionManager.getProfile(IncomingScreenActivity.this);
                if (own != null) {
                    try {
                        GATRacker.getInstance(getApplication()) // Tracker event google analitycs...
                                .sendEventWithScreen(TrackerConstant.SCREEN_IN_APP2APP,
                                        TrackerConstant.EVENT_CAT_VIDEO_CALLER,
                                        "show video caller", own.msisdn);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        }, 1000);

    }

    private String formatTimespan(long timespan) throws Exception {
        long totalSeconds = timespan / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        if (mCallStart > 0) {
            try {
                mCallDuration.setText(formatTimespan(System.currentTimeMillis() - mCallStart));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void setPlayRingtone() {
        try {
            mAudioPlayer = new AudioPlayer(IncomingScreenActivity.this);
            mAudioPlayer.playRingtone();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void answerClicked() {
        if (mAudioPlayer != null) {
            mAudioPlayer.stopRingtone();
        }

        if (!TextUtils.isEmpty(mCallId) && !TextUtils.isEmpty(mCaller_id)) {
            if(getSinchServiceInterface() != null) {
                try {
                    Call call = getSinchServiceInterface().getCall(mCallId);
                    if (call != null) {
                        call.answer();
                        clearNotification();

                        currentRingMode();
                        CallerViewUtil.getInstance(getApplicationContext()).dismissView();

                        isAnswareCall(true);
                    }
                }catch (Exception e){
                    clearNotification();
                    currentRingMode();
                    CallerViewUtil.getInstance(getApplicationContext()).dismissView();
                    finish();
                }
            }
        }
    }

    private void declineClicked() {
        if (mAudioPlayer != null) {
            mAudioPlayer.stopRingtone();
        }
        if (!TextUtils.isEmpty(mCallId)) {
            if(getSinchServiceInterface() != null) {
                try {
                    Call call = getSinchServiceInterface().getCall(mCallId);
                    if (call != null) {
                        call.hangup();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        clearNotification();
        CallerViewUtil.getInstance(getApplicationContext()).dismissView();
        finish();
    }

    @SuppressLint("CheckResult")
    private void showBackgroundScreen(String url){
        try {

            RequestOptions defBanner = new RequestOptions();
            defBanner.placeholder(R.drawable.newversinbg_);
            Glide.with(getApplicationContext())
                    .load(url)
                    .apply(defBanner)
                    .into(mImageBackground);

            // Profile Picture
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.userman);
            Glide.with(getApplicationContext())
                    .load(url).apply(requestOptions)
                    .into(profilePic);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onServiceConnected() {
        if (!TextUtils.isEmpty(mCallId)) {
            if(getSinchServiceInterface() != null) {
                try {
                    Call call = getSinchServiceInterface().getCall(mCallId);
                    if (call != null) {
                        call.addCallListener(new SinchCallListener());
                        AppStatusBarNotifier updateStatusBarNotifier = AppStatusBarNotifier.getInstance(this);
                        if (updateStatusBarNotifier != null) {
                            String strNotifTitleIncoming = TextUtils.isEmpty(model.getLang().string.notifTitleIncoming) ? "Incoming Call" : model.getLang().string.notifTitleIncoming;
                            String strNotifContentIncoming = TextUtils.isEmpty(model.getLang().string.notifContentIncoming) ? "Calling from" : model.getLang().string.notifContentIncoming;
                            String strStateOutgoingUnknown = TextUtils.isEmpty(model.getLang().string.stateUnknowAppRtc) ? "Unknow Number" : model.getLang().string.stateUnknowAppRtc;
                            sName = TextUtils.isEmpty(sName)?strStateOutgoingUnknown:sName;
                            updateStatusBarNotifier.updateNotification(strNotifTitleIncoming, strNotifContentIncoming + " " + sName, "incoming", IncomingScreenActivity.class);
                        }

                        if (getSinchServiceInterface() != null) {
                            getSinchServiceInterface().settSpeaker(false);
                            getSinchServiceInterface().settMicrophone(true);
                        }

                        if (!TextUtils.isEmpty(sMsisdn)) {
                            inCommingProcess(sMsisdn);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    clearNotification();
                    CallerViewUtil.getInstance(getApplicationContext()).dismissView();
                    finish();
                }
            }
        }
    }

    private class SinchCallListener implements CallListener {

        @Override
        public void onCallEnded(Call call) {
            clearNotification();
            CallerViewUtil.getInstance(getApplicationContext()).dismissView();
            if (mAudioPlayer != null) {
                mAudioPlayer.stopRingtone();
            }
            finish();
        }

        @Override
        public void onCallEstablished(Call call) {

            String strStateOutgoingConnected = TextUtils.isEmpty(model.getLang().string.stateConnectedAppRtc) ? "Connected" : model.getLang().string.stateConnectedAppRtc;
            String strNotifTitleDuringCall = TextUtils.isEmpty(model.getLang().string.notifTitleDuringCall) ? "Call Active" : model.getLang().string.notifTitleDuringCall;
            String strNotifContentDuringCall = TextUtils.isEmpty(model.getLang().string.notifContentDuringCall) ? "Calling From" : model.getLang().string.notifContentDuringCall;

            AppStatusBarNotifier updateStatusBarNotifier = AppStatusBarNotifier.getInstance(IncomingScreenActivity.this);
            if (updateStatusBarNotifier != null) {
                updateStatusBarNotifier.updateNotification(
                        strNotifTitleDuringCall, strNotifContentDuringCall+" "+sName, "during call",IncomingScreenActivity.class);
            }

            mCallState.setText(strStateOutgoingConnected);
            setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

            try {
                mCallStart = System.currentTimeMillis();
                mTimer = new Timer();
                mDurationTask = new UpdateCallDurationTask();
                mTimer.schedule(mDurationTask, 0, 500);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }
    }

    private View.OnClickListener mClickListener = v -> {
        switch (v.getId()) {
            case R.id.answerButton:
                answerClicked();
                break;
            case R.id.declineButton:
                declineClicked();
                break;
            case R.id.hangupButton:
                declineClicked();
                break;
            case R.id.btn_speaker:

                if (speakIsActive){
                    if(getSinchServiceInterface() != null){
                        speakIsActive = false;
                        getSinchServiceInterface().settSpeaker(false);
                        btnSpeaker.setSelected(false);
                    }
                }else {
                    if(getSinchServiceInterface() != null) {
                        speakIsActive = true;
                        getSinchServiceInterface().settSpeaker(true);
                        btnSpeaker.setSelected(true);
                    }
                }
                break;
            case R.id.btn_mic:

                if (micIsActive){
                    if(getSinchServiceInterface() != null) {
                        micIsActive = false;
                        getSinchServiceInterface().settMicrophone(true);
                        btnMic.setSelected(false);
                    }
                }else {
                    if(getSinchServiceInterface() != null) {
                        micIsActive = true;
                        getSinchServiceInterface().settMicrophone(false);
                        btnMic.setSelected(true);
                    }
                }
                break;
        }
    };

    private void currentRingMode(){

        try {
            final Context mContext = getApplicationContext();
            AudioManager mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
            int mCurrentRingerMode = mAudioManager.getRingerMode();
            int mCurrentMusicVolume =  mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            int mCurrentRingerVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_RING);

            mAudioManager.setRingerMode(mCurrentRingerMode);
            mAudioManager.setStreamVolume(AudioManager.STREAM_RING, mCurrentRingerVolume,0);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mCurrentMusicVolume,0);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void dismissVideoCaller() throws Exception{
        final Context context = getApplicationContext();
        if (context!= null) {
            CallerViewUtil.getInstance(context).dismissView();
        }
    }
}
