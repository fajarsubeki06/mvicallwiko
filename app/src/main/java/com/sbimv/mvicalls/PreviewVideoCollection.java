package com.sbimv.mvicalls;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.sbimv.mvicalls.activity.SetVideoToneActivity;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.MyCollection;
import com.sbimv.mvicalls.pojo.SetVideoTone;
import com.sbimv.mvicalls.services.SyncVideoProfileServices;
import com.sbimv.mvicalls.sync.SyncContactProfileManager;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.OwnVideoManager;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.view.DialogUpload;
import com.sbimv.mvicalls.walktrough.PreviewFreeVideoCollection;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreviewVideoCollection extends AppCompatActivity {

    private VideoView vvPreviewCol;
    public static PreviewVideoCollection pvcl;
    private MediaController mediaController;
    private ProgressDialog pd;
    private LinearLayout layout;
    private ImageView imgClose;
    private Button btnSetVideo;
    private DialogUpload dialogUpload;
    private MyCollection collection;
    private String decodedValue2;
    private ProgressBar progressBar;
    private TextView tvTitle,tvArtist;
    private ContactItem own;
    private boolean isViewAttached = false;
    private String rewardID;
    LangViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isViewAttached = true;
        model = LangViewModel.getInstance();
        this.setFinishOnTouchOutside(false);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_preview_video_collection);

        try {
            collection = getIntent().getParcelableExtra("dataCollections");
            rewardID = getIntent().getStringExtra("rewardID");
        }catch (Exception e){
            return;
        }

        tvArtist = findViewById(R.id.tvArtistPreview);
        tvTitle = findViewById(R.id.tvTitlePreview);
        tvTitle.setText(collection.judul);
        tvArtist.setText(collection.alias);

        vvPreviewCol = findViewById(R.id.vv_preview_collection);
        vvPreviewCol.setZOrderOnTop(true);

        imgClose = findViewById(R.id.imgClosePreviews);
        btnSetVideo = findViewById(R.id.btnSetAsVideoTone);
        btnSetVideo.setText(model.getLang().string.setVideoTone);
        if (!TextUtils.isEmpty(rewardID)){
            btnSetVideo.setVisibility(View.INVISIBLE);
        }

        layout = findViewById(R.id.linPreview);
        pd = new ProgressDialog(this);
        pvcl = this;
        progressBar = findViewById(R.id.pgPreview);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnSetVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setAsVideoTone();
            }
        });
        initPreviewVideo();

    }

    private void setAsVideoTone() {
        showDialog();
        Call<APIResponse<List<SetVideoTone>>> call = ServicesFactory.getService().getVideoTone(collection.caller_id, collection.content_id);
        call.enqueue(new Callback<APIResponse<List<SetVideoTone>>>() {
            @Override
            public void onResponse(Call<APIResponse<List<SetVideoTone>>> call, final Response<APIResponse<List<SetVideoTone>>> response) {
                if (!isViewAttached){
                    return;
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful() && response.body().isSuccessful()) {
                            List<SetVideoTone> setVideoTone = response.body().data;
                            String urls = TextUtils.isEmpty(setVideoTone.get(0).source_content)?"":setVideoTone.get(0).source_content;
                            File url = new File(urls);

                            try {
                                decodedValue2 = URLDecoder.decode(urls, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            File dest = new File(OwnVideoManager.getOwnVideoUri(PreviewVideoCollection.this).getPath());
                            try {

                                if (!SyncContactProfileManager.getInstance(PreviewVideoCollection.this).isSynchronizing()) {
                                    Intent i = new Intent(PreviewVideoCollection.this, SyncVideoProfileServices.class);
                                    PreviewVideoCollection.this.startService(i);
                                }

                                // save to session and update local variable
                                ContactItem ci = new ContactItem();
                                ci.profiles = null;// Untuk syncronize firebase
                                ci.video_caller = decodedValue2;
                                own = SessionManager.saveProfile(PreviewVideoCollection.this, ci);

                                SharedPreferences sharedPreferences = PreviewVideoCollection.this.getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean(ConfigPref.SET_CONTENT, true);
                                editor.putString(ConfigPref.URL_BUY, decodedValue2);
                                editor.commit();

                                FileUtil.moveFile(url, dest);

                                // google analytics...
                                GATRacker.getInstance(getApplication()).sendEventWithScreen(
                                        TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
                                        TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
                                        "set as personal video",
                                        collection.caller_id+"/"+collection.content_id);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, 1000);
            }

            @Override
            public void onFailure(Call<APIResponse<List<SetVideoTone>>> call, Throwable t) {
                if (!isViewAttached){
                    return;
                }
            }
        });
    }

    private void initPreviewVideo() {
        mediaController = new MediaController(PreviewVideoCollection.this);
        mediaController.setAnchorView(mediaController);
        String url_video = collection.source_content;
        url_video = url_video.replaceAll(" ", "%20");
        Uri uri = Uri.parse(url_video);
        vvPreviewCol.setVideoURI(uri);

        vvPreviewCol.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
                vvPreviewCol.start();
            }
        });

        vvPreviewCol.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progressBar.setVisibility(View.GONE);
                showAlertErrorPlay();
                return true;
            }
        });

    }

    private void showAlertErrorPlay(){
        if (!PreviewVideoCollection.this.isFinishing()) {

            String sTitle = TextUtils.isEmpty(model.getLang().string.str_cannot_play)
                    ? "Can't play this video."
                    : model.getLang().string.str_cannot_play;

            String sApprove = TextUtils.isEmpty(model.getLang().string.approveCofirm)
                    ? "Ok"
                    : model.getLang().string.approveCofirm;

            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setMessage(sTitle)
                    .setCancelable(false)
                    .setPositiveButton(sApprove, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            finish();
                        }
                    }).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.UPLOAD_DONE");
        LocalBroadcastManager.getInstance(PreviewVideoCollection.this).registerReceiver(downloadVideo, _if);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(PreviewVideoCollection.this).unregisterReceiver(downloadVideo);
    }

    private BroadcastReceiver downloadVideo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            doneProgress();
        }
    };

    public void doneProgress(){
        if (dialogUpload != null){
            dissmisDialog();
            Intent intent = new Intent(PreviewVideoCollection.this, SetVideoToneActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();

        }
    }

    private void showDialog(){
        if (!PreviewVideoCollection.this.isFinishing()){
            try{
                dialogUpload = new DialogUpload();
                dialogUpload.show((PreviewVideoCollection.this).getSupportFragmentManager(), null);
                dialogUpload.setMessage(model.getLang().string.updatingVideoTone);
            }catch (Exception e){
                e.printStackTrace();
                return;
            }
        }
    }

    private void dissmisDialog(){
        if (!PreviewVideoCollection.this.isFinishing()){
            try{
                dialogUpload.dismissAllowingStateLoss();
                dialogUpload = null;
            }catch (Exception e){
                e.printStackTrace();
                return;
            }
        }
    }

}
