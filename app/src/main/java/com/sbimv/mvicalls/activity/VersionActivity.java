package com.sbimv.mvicalls.activity;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.BuildConfig;
import com.sbimv.mvicalls.R;

public class VersionActivity extends BaseActivity {

    private TextView txtVersionApps;
    private PackageManager packageManager;
    private String packageName;
    private TextView tvLast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version);

        txtVersionApps = findViewById(R.id.txtVersionApps);
        txtVersionApps.setText("v."+BuildConfig.VERSION_NAME);

        setDefaultToolbar(true);
    }


}
