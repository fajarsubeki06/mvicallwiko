package com.sbimv.mvicalls.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.sbimv.mvicalls.LocaleHelper;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.databinding.ActivityDetailRewardBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataRedeemReward;
import com.sbimv.mvicalls.pojo.DataRewards;
import com.sbimv.mvicalls.util.SessionManager;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PopupDetailRewardActivity extends AppCompatActivity {

    private DataRewards.ListRewards datas;
    private ImageView imgBanner;
    private ContactItem own;
    private int userPoint = 0;
    private ImageButton btnClose;
    LangViewModel model;
    private ActivityDetailRewardBinding binding;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(LocaleHelper.onAttach(context));
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        model = LangViewModel.getInstance();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_reward);
        binding.setLangModel(model);
        own = SessionManager.getProfile(PopupDetailRewardActivity.this);
        btnClose = findViewById(R.id.btnClosePopup);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            datas = bundle.getParcelable("dataReward");
            userPoint = Integer.parseInt(bundle.getString("userPoint"));
        }

        TextView tvDetHeadline = findViewById(R.id.tvDetHeadLine);
        TextView tvDetPoint = findViewById(R.id.tvDetItemPoint);
        CircleImageView imgDetHeadline = findViewById(R.id.imgDetHeadLine);
        WebView wvTncReward = findViewById(R.id.wvTncReward);
        imgBanner = findViewById(R.id.imgBGDetReward);
        Button btnRedeem = findViewById(R.id.btnDetRedeemPoint);
        wvTncReward.loadUrl(datas.getTnc());

        tvDetHeadline.setText(datas.getHeadline());
        tvDetPoint.setText(datas.getPoin() + " " + model.getLang().string.point);

        try {
            RequestOptions defBanner = new RequestOptions();
            defBanner.placeholder(R.drawable.bg_banner_reward);
            Glide.with(this).asBitmap().apply(defBanner).load(datas.getBanner()).into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(final Bitmap bitmap, Transition<? super Bitmap> transition) {
                    final int w = bitmap.getWidth();
                    final int h = bitmap.getHeight();
                    imgBanner.post(new Runnable() {
                        @Override
                        public void run() {
                            int imgWidth = imgBanner.getMeasuredWidth();
                            int imgHeight = imgWidth * h / w;
                            imgBanner.getLayoutParams().height = imgHeight;
                            imgBanner.requestLayout();
                            imgBanner.setImageBitmap(bitmap);
                        }
                    });
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.icx_mv_color);
            Glide.with(this).load(datas.getIcon()).apply(requestOptions).into(imgDetHeadline);
        }catch (Exception e){
            e.printStackTrace();
        }

        btnRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (datas.getType().equals("video")) {
                    // Handle redeem video
                    if (userPoint >= Integer.parseInt(datas.getPoin())) {
                        Intent intent = new Intent(PopupDetailRewardActivity.this, RedeemVideoActivity.class);
                        intent.putExtra("dataReward", datas);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(PopupDetailRewardActivity.this, model.getLang().string.notEnoughPoint, Toast.LENGTH_SHORT).show();
                    }
                } else if (datas.getType().equals("subscription")) {
                    // Handle redeem subscription
                    if (userPoint >= Integer.parseInt(datas.getPoin())) {
                        // progress bar
                        RelativeLayout layout = findViewById(R.id.rvProgress);
                        final ProgressBar progressBar = new ProgressBar(PopupDetailRewardActivity.this);
                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.addRule(RelativeLayout.CENTER_IN_PARENT);
                        layout.addView(progressBar,params);
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        progressBar.setVisibility(View.VISIBLE);

                        Call<APIResponse<DataRedeemReward>> call = ServicesFactory.getService().getRedeemReward(own.msisdn, null, null, datas.getReward_id(), "redeem_subscription", null);
                        call.enqueue(new Callback<APIResponse<DataRedeemReward>>() {
                            @Override
                            public void onResponse(Call<APIResponse<DataRedeemReward>> call, Response<APIResponse<DataRedeemReward>> response) {
                                progressBar.setVisibility(View.GONE);

                                String wording = model.getLang().string.wordingSuccessRedeem.replace("[ITEM]", datas.getHeadline());
                                AlertDialog.Builder builder = new AlertDialog.Builder(PopupDetailRewardActivity.this);
                                builder.setTitle(model.getLang().string.success);
                                builder.setCancelable(false);
                                builder.setMessage(wording);
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent intent = new Intent(PopupDetailRewardActivity.this,RewardActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }

                            @Override
                            public void onFailure(Call<APIResponse<DataRedeemReward>> call, Throwable t) {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        Toast.makeText(PopupDetailRewardActivity.this, model.getLang().string.notEnoughPoint, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


}
