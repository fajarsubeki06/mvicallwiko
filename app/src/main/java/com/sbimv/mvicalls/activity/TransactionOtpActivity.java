package com.sbimv.mvicalls.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.databinding.ActivityTransactionOtpBinding;
import com.sbimv.mvicalls.databinding.ActivityTransactionOtpBindingImpl;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.register.PhoneAuthentication;
import com.sbimv.mvicalls.register.PhoneRegister;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TransactionOtpActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnVerify;
    private PinView pvAuthCode;
    private String transaction_id = "";
    private LangViewModel modelLang;
    private String error_msg = "";
    private String sAlias = "";
    private String sName = "";
    private String sMisdn = "";
    private String sContenId = "";
    private String sPrice = "";
    private String sPriceLbl = "";
    private String sType = "";
    private TextView tvCount, tvGetOtp, tvContent, tvPhone, tvTitleContent, tvToolbar, tvSecond;
    private ProgressBar progressBar;
    private ImageView mBack, mImgContent;

    //Timer
    Timer mTimer;
    private int mCounter = 60;
    Handler mHandler = new Handler(Looper.getMainLooper());

    private TextView tvActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityTransactionOtpBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_transaction_otp);
        modelLang = LangViewModel.getInstance();
        binding.setLangModel(modelLang);

        setTimmer();

        initView();
        Bundle bundle = getIntent().getExtras();
        initDataBundle(bundle);

    }

    private void initView(){
        btnVerify = findViewById(R.id.btnVerify);
        pvAuthCode = findViewById(R.id.pvAuthCode);
        tvCount = findViewById(R.id.tvCount);
        progressBar = findViewById(R.id.progress_bar);
        mBack = findViewById(R.id.btn_actionbar_back);
        tvGetOtp = findViewById(R.id.tvGetOtp);
        tvContent = findViewById(R.id.tvContent);
        tvPhone = findViewById(R.id.tvPhone);
        mImgContent = findViewById(R.id.imageContent);
        tvTitleContent = findViewById(R.id.tvTitleContent);
        tvToolbar = findViewById(R.id.tv_judul_content);
        tvSecond = findViewById(R.id.tvSecond);
        mBack.setOnClickListener(this);
        btnVerify.setOnClickListener(this);
        tvCount.setOnClickListener(this);
        tvGetOtp.setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    private void initDataBundle(Bundle bundle){
        if (bundle != null){
            sAlias = TextUtils.isEmpty(bundle.getString("sAlias"))?"":bundle.getString("sAlias");
            sName = TextUtils.isEmpty(bundle.getString("sName"))?"":bundle.getString("sName");
            sMisdn = bundle.getString("sMisdn");
            sContenId = TextUtils.isEmpty(bundle.getString("sContenId"))?"":bundle.getString("sContenId");
            sPrice = TextUtils.isEmpty(bundle.getString("sPrice"))?"":bundle.getString("sPrice");
            sPriceLbl = TextUtils.isEmpty(bundle.getString("sPriceLabel"))?"":bundle.getString("sPriceLabel");
            sType = TextUtils.isEmpty(bundle.getString("sType"))?"":bundle.getString("sType");

            String sTitle = TextUtils.isEmpty(modelLang.getLang().string.str_title_subs)
                    ?"You are going to enjoy MviCall Premium Feature":modelLang.getLang().string.str_title_subs;

            String sFor = TextUtils.isEmpty(modelLang.getLang().string.str_for)
                    ?"for":modelLang.getLang().string.str_for;

            String sMonthly = TextUtils.isEmpty(modelLang.getLang().string.str_monthly)
                    ?"monthly":modelLang.getLang().string.str_monthly;

            String sTitleBuy = TextUtils.isEmpty(modelLang.getLang().string.str_content_otp)
                    ?"You are going to buy videotone":modelLang.getLang().string.str_content_otp;

            String sSubscribe = TextUtils.isEmpty(modelLang.getLang().string.str_subscribe)
                    ?"Subscribe":modelLang.getLang().string.str_subscribe;

            String sBuy = TextUtils.isEmpty(modelLang.getLang().string.str_btn_buy)
                    ?"Buy":modelLang.getLang().string.str_btn_buy;

            String sTitleTb = TextUtils.isEmpty(modelLang.getLang().string.str_jdl_otp_act)
                    ?"Buy Content":modelLang.getLang().string.str_jdl_otp_act;

            tvContent.setText("\"" + sAlias + " - " + sName +"\"" +  " " + sFor + " " + sPriceLbl + " " + sPrice);
            tvPhone.setText(sMisdn);

            if (!TextUtils.isEmpty(sType)){
                if (sType.equals("sub")){
                    tvTitleContent.setText(sTitle + " " + sFor + " " + sPriceLbl + " " + sPrice + " " + sMonthly);
                    mImgContent.setImageDrawable(getResources().getDrawable(R.drawable.icx_subs));
                    tvContent.setVisibility(View.GONE);
                    btnVerify.setText(sSubscribe);
                    tvToolbar.setText(sSubscribe);
                }else{
                    tvTitleContent.setText(sTitleBuy);
                    tvToolbar.setText(sTitleTb);
                    btnVerify.setText(sBuy);
                }
            }


            requestOtp(sMisdn, sContenId, sPrice, sType);
            return;

        }
        Toast.makeText(TransactionOtpActivity.this, modelLang.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
    }

    // *
    // Request OTP transaction...
    // *
    private void requestOtp (String msisdn, String content_id, String price, String type) {
        if (!TextUtils.isEmpty(msisdn)) {
            Call<APIResponse<String>> call = ServicesFactory.getService().reqDcb(msisdn, content_id, price, type);
            call.enqueue(new Callback<APIResponse<String>>() {
                @Override
                public void onResponse(@NotNull Call<APIResponse<String>> call, @NotNull Response<APIResponse<String>> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().data != null) {
                            transaction_id = response.body().data;
                            return;
                        }
                        Toast.makeText(TransactionOtpActivity.this, modelLang.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
                        stopTimmer();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<APIResponse<String>> call, @NotNull Throwable t) {
                    Toast.makeText(TransactionOtpActivity.this, modelLang.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
                    stopTimmer();
                }
            });
        }
    }

    // *
    // Request transaction...
    // *
    private void requestTransaction (String otp, String trx_id, String msisdn, String content_id, String price, String type) {
        isProgress(true);
        if (!TextUtils.isEmpty(msisdn)) {
            Call<APIResponse<String>> call = ServicesFactory.getService().dcbTransaction(otp, trx_id, msisdn, price, content_id, type);
            call.enqueue(new Callback<APIResponse<String>>() {
                @Override
                public void onResponse(@NotNull Call<APIResponse<String>> call, @NotNull Response<APIResponse<String>> response) {
                    if (response.isSuccessful() && response.body() != null) {
                        if (response.body().data != null) {
                            String code = TextUtils.isEmpty(response.body().code)?"":response.body().code;
                            error_msg = TextUtils.isEmpty(response.body().error_message) ? "" : response.body().error_message;
                            if (code.equalsIgnoreCase("200")){
                                Toast.makeText(TransactionOtpActivity.this, error_msg, Toast.LENGTH_SHORT).show();
                                finish();
                            }

                        }
                        stopTimmer();
                        Toast.makeText(TransactionOtpActivity.this, error_msg, Toast.LENGTH_SHORT).show();
                    }
                    isProgress(false);
                }

                @Override
                public void onFailure(@NotNull Call<APIResponse<String>> call, @NotNull Throwable t) {
                    Toast.makeText(TransactionOtpActivity.this, modelLang.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
                    isProgress(false);
                    stopTimmer();
                }
            });
        }
    }

    //Set Timer
    private void setTimmer(){
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                mCounter--;
                mHandler.post(() -> {
                    try {
                        String sResend = TextUtils.isEmpty(modelLang.getLang().string.str_resend_otp)
                                ?"Resend":modelLang.getLang().string.str_resend_otp;
                        String sSecond = TextUtils.isEmpty(modelLang.getLang().string.str_second)
                                ?"Second":modelLang.getLang().string.str_second;
                        //tvCount.setText(sResend + " " + "(" + mCounter + " " + sSecond + ")");
                        tvCount.setText(sResend);
                        tvSecond.setText("(" + mCounter + " " + sSecond + ")");
                    }catch (Exception e){
                        return;
                    }

                    if(mCounter == 0){
                        mCounter = 60;
                        mTimer.cancel();
                        stopTimmer();
                    }

                });

            }
        }, 0, 1000);
    }

    private void stopTimmer(){
        if (mTimer != null){
            mTimer.cancel();
            validateResendCode(true);
        }
    }

    private void isProgress(boolean isLoading) {
        if (!TransactionOtpActivity.this.isFinishing()) {
            if (isLoading) {
                progressBar.setVisibility(View.VISIBLE);
                btnVerify.setVisibility(View.GONE);
            } else {
                progressBar.setVisibility(View.GONE);
                btnVerify.setVisibility(View.VISIBLE);
            }
        }
    }

    private void validateResendCode(boolean isResend){
        if (!TransactionOtpActivity.this.isFinishing()) {
            if (isResend == true) {
                tvCount.setEnabled(true);
                tvCount.setTextColor(getResources().getColor(R.color.orange_reward));
                tvSecond.setVisibility(View.GONE);
                mCounter = 60;
            } else {
                tvCount.setEnabled(false);
                tvCount.setVisibility(View.VISIBLE);
                tvSecond.setVisibility(View.VISIBLE);
                tvCount.setTextColor(getResources().getColor(R.color.black));
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (btnVerify == view){
            String codeOtp = Objects.requireNonNull(pvAuthCode.getText()).toString();
            if (!TextUtils.isEmpty(transaction_id) && !TextUtils.isEmpty(codeOtp)) {
                stopTimmer();
                requestTransaction(codeOtp, transaction_id, sMisdn, sContenId, sPrice, sType);
            }
        }else if (mBack == view){
            finish();
        }else if (tvCount == view){
            setTimmer();
            requestOtp(sMisdn, sContenId, sPrice, sType);
            validateResendCode(false);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        stopTimmer();
        isProgress(false);
    }
}