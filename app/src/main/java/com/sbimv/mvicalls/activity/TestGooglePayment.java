package com.sbimv.mvicalls.activity;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

//import com.anjlab.android.iab.v3.BillingProcessor;
//import com.anjlab.android.iab.v3.TransactionDetails;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.icallservices.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TestGooglePayment extends AppCompatActivity
//        implements BillingProcessor.IBillingHandler
{

    ScrollView mScrollView;
    LinearLayout mLinearLayout;
    TextView mLoading;
//    BillingProcessor bp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mScrollView = new ScrollView(this);
        mScrollView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        mLinearLayout = new LinearLayout(this);
        mLinearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mLinearLayout.setOrientation(LinearLayout.VERTICAL);
        mLinearLayout.setGravity(Gravity.CENTER);
        mLinearLayout.setPadding(40, 40, 40, 40);
        mScrollView.addView(mLinearLayout);

        mLoading = new TextView(this);
        mLoading.setText("LOADING PRODUCTS...");
        mLoading.setTextColor(Color.BLUE);
        mLinearLayout.addView(mLoading);

        setContentView(mScrollView);
//        getParseLocalJSON();
        loadProducts();

//        bp = new BillingProcessor(this, BuildConfig.KEY_PAYMENT, this);
//        bp.initialize();

    }

    private void getParseLocalJSON(){
        String json;
        try {
            InputStream is = getAssets().open("gpayriset.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
            JSONArray jsonArray = new JSONArray(json);

            mLinearLayout.removeAllViews();
            Button btn;

            for (int i =0; i<jsonArray.length(); i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                String _product = obj.getString("keyword");
                btn = new Button(TestGooglePayment.this);
                btn.setText(_product);
                btn.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        if(_product.contains("product")){
//                            bp.consumePurchase(_product);
//                            bp.purchase(TestGooglePayment.this, _product);
//                        }else {
//                            bp.subscribe(TestGooglePayment.this, _product);
//                        }

                    }
                });
                mLinearLayout.addView(btn);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    void loadProducts(){
        Call<List<String>> call = ServicesFactory.getTestService().getProducts();
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(@NonNull Call<List<String>> call, @NonNull Response<List<String>> response) {
                if (response.body() != null && response.isSuccessful()) {
                    mLinearLayout.removeAllViews();
                    Button btn;

                    for (final String product : response.body()) {

                        btn = new Button(TestGooglePayment.this);
                        btn.setText(product);
                        btn.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        btn.setOnClickListener(view -> {
                            if (!TextUtils.isEmpty(product)){
//                                bp.subscribe(TestGooglePayment.this, product);
                            }
                        });
                        mLinearLayout.addView(btn);
                    }

                }else {
                    Log.i("test_failed","test_failed");
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t) {
                mLoading.setText(t.getLocalizedMessage()==null? "" : t.getLocalizedMessage());
            }
        });
    }

//    @Override
//    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
//        Toast.makeText(this, "Success buy : "+productId, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onPurchaseHistoryRestored() {
//
//    }
//
//    @Override
//    public void onBillingError(int errorCode, @Nullable Throwable error) {
//        Toast.makeText(this, "Error buy : "+errorCode, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onBillingInitialized() {
//
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (!bp.handleActivityResult(requestCode, resultCode, data)) {
//            super.onActivityResult(requestCode, resultCode, data);
//        }
//    }

}