package com.sbimv.mvicalls.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.LocaleHelper;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.DataHowtoGetPoin;
import com.sbimv.mvicalls.util.PreferenceUtil;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PopUpHowToGetPoint extends Activity {

    private ImageView imageView;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(LocaleHelper.onAttach(context));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_how_to_get_point);
        imageView = findViewById(R.id.imgHowToGetPoint);
        ImageButton imageButton = findViewById(R.id.btnClosePopupPoint);

        imageButton.setOnClickListener(view -> finish());
        String lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
        loadImage(lang);

    }

    private void loadImage(String language) {
        Call<APIResponse<DataHowtoGetPoin>> call = ServicesFactory.getService().getHowtoGetPoin(language);
        call.enqueue(new Callback<APIResponse<DataHowtoGetPoin>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<DataHowtoGetPoin>> call, @NotNull Response<APIResponse<DataHowtoGetPoin>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    DataHowtoGetPoin link = response.body().data;
                    String url = link.getUrl();
                    if (!TextUtils.isEmpty(url)) {

                        if(!PopUpHowToGetPoint.this.isFinishing()) { // Check destroyed activity....
                            try {
                                Glide.with(PopUpHowToGetPoint.this).asBitmap().load(url).into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(@NotNull final Bitmap bitmap, Transition<? super Bitmap> transition) {
                                        final int w = bitmap.getWidth();
                                        final int h = bitmap.getHeight();
                                        imageView.post(() -> {
                                            int ivW = imageView.getMeasuredWidth();
                                            imageView.getLayoutParams().height = ivW * h / w;
                                            imageView.requestLayout();
                                            imageView.setImageBitmap(bitmap);
                                        });

                                    }
                                });
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<DataHowtoGetPoin>> call, @NotNull Throwable t) {
                if (!isFinishing()) {
                    LangViewModel model = LangViewModel.getInstance();
                    Toast.makeText(PopUpHowToGetPoint.this, model.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
