package com.sbimv.mvicalls.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.adapter.ProfileContactAdapter;
import com.sbimv.mvicalls.databinding.ActivityAddProfileBinding;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.ProfileItem;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;
import com.sbimv.mvicalls.util.ValidationInputUtil;
import com.sbimv.mvicalls.view.DialogUpload;

import java.util.HashSet;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddProfileActivity extends BaseActivity {

    final static int REQ_PICK_CONTACT = 909;
    // bind view
    EditText mEtProfile;
    ImageView mBtnPhoneBook;
    ProfileContactAdapter adapter;
    // var
    String[] msisdn;
    DialogUpload dialog;
    ProfileItem.Item editedItem;
    private Button btn_confirm;
    private ContactItem own;
    private String callerId;
    private AutoCompleteTextView mAutoMsisdn;
    private TextView mNameUsr, textName;
    private TextView tvSetStatus;
    private ActivityAddProfileBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(AddProfileActivity.this, R.layout.activity_add_profile);
        binding.setLangModel(model);
        setDefaultToolbar(true);


        tvSetStatus = findViewById(R.id.tvSetStatus);
        textName = findViewById(R.id.txtName);
        tvSetStatus.setText(model.getLang().string.setStatus);

        String strWordingTitleName = TextUtils.isEmpty(model.getLang().string.setName)
                ?"Name" :
                model.getLang().string.setName;
        textName.setText(strWordingTitleName);

        mEtProfile = findViewById(R.id.et_profile);
        mBtnPhoneBook = findViewById(R.id.btn_phone_book);
        btn_confirm = findViewById(R.id.btn_confirm);
        mAutoMsisdn = findViewById(R.id.et_auto_msisdn); // autocompleteTextView...
        mNameUsr = findViewById(R.id.txtNameUsr);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            callerId = extras.getString("caller_id");
            String nm = extras.getString("nameUsr");
            String pn = extras.getString("phoneUsr");

            if (!TextUtils.isEmpty(nm) && !TextUtils.isEmpty(pn)){
                mNameUsr.setText(nm);
                mNameUsr.setVisibility(View.VISIBLE);
                mAutoMsisdn.setText(pn);
            }
        }

        editedItem = getIntent().getParcelableExtra("item");
        if (editedItem != null) {
            if (TextUtils.isEmpty(editedItem.msisdn)) {
                mAutoMsisdn.setHint("All Contact");
            }else {
                mAutoMsisdn.setHint("Phone Number");
                mNameUsr.setText(editedItem.name);
                mAutoMsisdn.setText(editedItem.msisdn);
                mEtProfile.setText(editedItem.profile);
                mBtnPhoneBook.setVisibility(View.GONE);
                mNameUsr.setVisibility(View.VISIBLE);
                mAutoMsisdn.setKeyListener(null);
            }
        }

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            btn_confirm.setBackgroundResource(R.drawable.button_selector);
        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            btn_confirm.setBackgroundResource(R.drawable.selector_rounded_grey_e4e4e4);
        }

        initAutoCompleteNameUsersDB();

    }

    public void doClick(View v) {
        if (v.getId() == R.id.btn_confirm) {
            if (checkValidation()) {

                String strMsisdn = mAutoMsisdn.getText().toString();
                if (!TextUtils.isEmpty(strMsisdn)){
                    if (getCallerID(strMsisdn) ==  true){
                        showDialogs();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (editedItem != null){
                                    String update = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat_nmr_tdk_kenal)
                                            ? "Nomor yang Anda Masukkan Tidak Dikenal"
                                            : model.getLang().string.str_set_personal_stat_nmr_tdk_kenal;
                                    showCustomDialogs(update);
                                    addProfile();
                                }

                                else{
                                    String added = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat__profile_added)
                                            ? "Adding Profile"
                                            : model.getLang().string.str_set_personal_stat__profile_added;
                                    showCustomDialogs(added);
                                    addProfile();
                                }


                            }
                        }, 200);
                    }else {

                        String nomor = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat_nmr_tdk_kenal)
                                ? "Nomor yang Anda Masukkan Tidak Dikenal"
                                : model.getLang().string.str_set_personal_stat_nmr_tdk_kenal;
                        toast(nomor);
                    }

                }
                return;
            }
        }else if (v.getId() == R.id.btn_phone_book) {
            Intent itn = new Intent(this, DataProfileUser.class);
            startActivity(itn);
            finish();

        }
    }

    private boolean getCallerID(String arg){
        boolean found = false;
        ContactDBManager contactDBManager = new ContactDBManager(AddProfileActivity.this);
        if (contactDBManager != null) {
            Cursor data = contactDBManager.getUserMV(isSparator(arg));
            if (data == null) {
                found = false;
            } else if (data != null) {
                found = true;
            }
        }
        return found;
    }

    private String isSparator(String... arg){
        if (!arg[0].contains("'")){
            return arg[0];
        }
        return "";
    }

    private void initAutoCompleteNameUsersDB(){
        ContactDBManager contactDBManager = new ContactDBManager(AddProfileActivity.this);
        if (contactDBManager != null) {
            final String[] namaProvDB = contactDBManager.selectAllDataNameUsers();
            if (namaProvDB != null) {
                ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, namaProvDB);
                mAutoMsisdn.setAdapter(adapter);
                mAutoMsisdn.setThreshold(1);
                mAutoMsisdn.dismissDropDown();

                mAutoMsisdn.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String selectedPosition = (String) parent.getItemAtPosition(position);
                        if (!TextUtils.isEmpty(selectedPosition)) {
                            selectionCriteria(selectedPosition);
                        }
                    }
                });
            }
        }
    }

    private void selectionCriteria (String arg){
        String[] split = arg.split(",");
        String selectName = split[0].trim();
        String selectNumber = split[1].trim();
        if (!TextUtils.isEmpty(selectName) && !TextUtils.isEmpty(selectNumber)){
            mNameUsr.setText(selectName);
            mNameUsr.setVisibility(View.VISIBLE);
            mAutoMsisdn.setText(selectNumber);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_PICK_CONTACT && resultCode == RESULT_OK) {
            Uri contactData = data.getData();
            Cursor c = getContentResolver().query(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                Set<String> setMsisdn = getMsisdnByID(id);

                if (setMsisdn.size() == 0) {
                    String msdin = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat__no_msisdn)
                            ? "No msisdn found"
                            : model.getLang().string.str_set_personal_stat__no_msisdn;
                    toastShort(msdin);
                    setSelectedMSISDN("");
                } else if (setMsisdn.size() == 1) {
                    msisdn = setMsisdn.toArray(new String[setMsisdn.size()]);
                    setSelectedMSISDN(msisdn[0]);
                } else {
                    msisdn = setMsisdn.toArray(new String[setMsisdn.size()]);
                    showMsisdnListDialog();
                }
            }
        }
    }

    private Set<String> getMsisdnByID(String key) {
        String[] COLS = {ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.TYPE};

        Cursor phones = getContentResolver()
                .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, COLS,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + key, null, null);

        Set<String> setMsisdn = new HashSet<>();

        while (phones.moveToNext()) {
            String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
            String prefix = AppPreference.getPreference(getApplicationContext(), AppPreference.PREFIX);
            String formatted = Util.formatMSISDNREG(number, prefix);
            String item;

            switch (type) {
                case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                    item = formatted + " (Home)";
                    setMsisdn.add(item);
                    break;
                case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                    item = formatted + " (Mobile)";
                    setMsisdn.add(item);
                    break;
                case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                    item = formatted + " (Work)";
                    setMsisdn.add(item);
                    break;
            }
        }
        phones.close();

        return setMsisdn;
    }

    private String getMSISDN(String setLabel) {
        return setLabel.substring(0, setLabel.indexOf(" "));
    }

    private void setSelectedMSISDN(String setLabel) {
        mAutoMsisdn.setText(setLabel);
    }

    private void showMsisdnListDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select number");
        builder.setItems(msisdn, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                setSelectedMSISDN(msisdn[item]);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void addProfile() {

        try {
            own = SessionManager.getProfile(this);
        } catch (Exception e) {
            return;
        }

        String msisdn = mAutoMsisdn.getText().toString();
        if (msisdn.contains("("))
            msisdn = getMSISDN(msisdn);
        String profile = mEtProfile.getText().toString();

        if (editedItem != null && profile.equals(editedItem.profile)) {
            dismissDialogs();
            String up = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat__profile_update)
                    ? "Profile updated"
                    : model.getLang().string.str_set_personal_stat__profile_update;
            toast(up);
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        Call<APIResponse<ContactItem>> call = ServicesFactory.getService().addProfile(own.caller_id, msisdn, profile);
        call.enqueue(new Callback<APIResponse<ContactItem>>() {
            @Override
            public void onResponse(Call<APIResponse<ContactItem>> call, Response<APIResponse<ContactItem>> response) {
                dismissDialogs();
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    if (editedItem != null) {
                        String update = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat_update_profil)
                                ? "Updating Profile..."
                                : model.getLang().string.str_set_personal_stat_update_profil;
                        toast(update);//profile update
                        if (own != null) {
                            if (!TextUtils.isEmpty(callerId)) {
                                GATRacker.getInstance(getApplication())
                                        .sendEventWithScreen(
                                                TrackerConstant.SCREEN_PROFILE,
                                                TrackerConstant.EVENT_CAT_PROFILE,
                                                "edit contact status sucess",
                                                own.caller_id + "/" + callerId);
                            }
                        }

                    } else {
                        String added = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat__profile_added)
                                ? "Adding Profile"
                                : model.getLang().string.str_set_personal_stat__profile_added;
                        toast(added);
                        if (own != null) {
                            if (!TextUtils.isEmpty(callerId)) {
                                GATRacker.getInstance(getApplication())
                                        .sendEventWithScreen(
                                                TrackerConstant.SCREEN_PROFILE,
                                                TrackerConstant.EVENT_CAT_PROFILE,
                                                "add contact status sucess",
                                                own.caller_id + "/" + callerId);
                            }
                        }


                    }

//                    PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.STATUS_SET, true).commit();
                    ContactItem item = response.body().data;

                    try {
                        own.profiles = item.profiles;
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        if (e instanceof NullPointerException) {
                            return;
                        }
                    }

                    SessionManager.saveProfile(getApplicationContext(), own);
                    finish();
                } else {
                    if (editedItem != null) {
                        String fail = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat_update_failed)
                                ? "Update profile failed"
                                : model.getLang().string.str_set_personal_stat_update_failed;
                        toast(fail);
                        if (own != null) {
                            if (!TextUtils.isEmpty(callerId)) {
                                GATRacker.getInstance(getApplication())
                                        .sendEventWithScreen(
                                                TrackerConstant.SCREEN_PROFILE,
                                                TrackerConstant.EVENT_CAT_PROFILE,
                                                "edit contact status failed",
                                                own.caller_id + "/" + callerId);
                            }
                        }

                    } else {
                        String failed = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat_add_failed)
                                ? "Add Profile failed"
                                : model.getLang().string.str_set_personal_stat_add_failed;
                        toast(failed);
                        if (own != null) {
                            if (!TextUtils.isEmpty(callerId)) {
                                GATRacker.getInstance(getApplication())
                                        .sendEventWithScreen(
                                                TrackerConstant.SCREEN_PROFILE,
                                                TrackerConstant.EVENT_CAT_PROFILE,
                                                "add contact status failed",
                                                own.caller_id + "/" + callerId);
                            }
                        }

                    }

                }
            }

            @Override
            public void onFailure(Call<APIResponse<ContactItem>> call, Throwable t) {

                try {
                    dismissDialogs();
                } catch (Exception e) {
                    return;
                }

                if (editedItem != null) {
                    String failed = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat_update_failed)
                            ? "Add Profile failed"
                            : model.getLang().string.str_set_personal_stat_update_failed;//update
                    toast(failed);
                    if (own != null) {
                        if (!TextUtils.isEmpty(callerId)) {
                            GATRacker.getInstance(getApplication())
                                    .sendEventWithScreen(
                                            TrackerConstant.SCREEN_PROFILE,
                                            TrackerConstant.EVENT_CAT_PROFILE,
                                            "edit contact status failed",
                                            own.caller_id + "/" + callerId);
                        }
                    }
                } else {
                    String failed = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat_add_failed)
                            ? "Add Profile failed"
                            : model.getLang().string.str_set_personal_stat_add_failed;
                    toast(failed);
                    if (own != null) {
                        if (!TextUtils.isEmpty(callerId)) {
                            GATRacker.getInstance(getApplication())
                                    .sendEventWithScreen(
                                            TrackerConstant.SCREEN_PROFILE,
                                            TrackerConstant.EVENT_CAT_PROFILE,
                                            "add contact status failed",
                                            own.caller_id + "/" + callerId);
                        }
                    }

                }

            }
        });
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!ValidationInputUtil.hasText(mAutoMsisdn)) ret = false;
        if (!ValidationInputUtil.hasText(mEtProfile)) ret = false;
        return ret;
    }

    /**
     * UI Threads
     * Added by Yudha Pratama Putra, 08 Okt 2018
     */
    private void showCustomDialogs(String msg) {
        if (!isViewAttached)
            return;
        try {
            dialog.setMessage(msg);
        } catch (Exception e) {
            return;
        }
    }

    private void showDialogs() {
        if (!isViewAttached)
            return;
        try {
            dialog = new DialogUpload();
            dialog.show(getSupportFragmentManager(), null);
        } catch (Exception e) {
            return;
        }
    }

    private void dismissDialogs() {
        if (!isViewAttached)
            return;
        try {
            dialog.dismiss();
        } catch (Exception e) {
            return;
        }
    }

}
