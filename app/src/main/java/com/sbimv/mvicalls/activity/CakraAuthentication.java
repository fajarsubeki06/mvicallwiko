package com.sbimv.mvicalls.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.provider.Settings;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chaos.view.PinView;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.apprtc.SinchService;
import com.sbimv.mvicalls.databinding.ActivityCakraAuthenticationBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.intro.IntroActivitySecond;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.ModelOTP;
import com.sbimv.mvicalls.receiver.InstallReferrerReceiver;
import com.sbimv.mvicalls.register.PhoneRegister;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;
import com.sinch.android.rtc.SinchError;


import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CakraAuthentication extends BaseActivity implements View.OnClickListener, SinchService.StartFailedListener {

    private TextView tvPhoneNumber;
    private Button btnVerification;
    private TextView tvWrongNumber;
    private PinView pinViewCode;
    private TextView tvTimeCount;
    private TextView tvResendCode;
    private ProgressBar progress_bar;
    private ImageView imgResendGrey;
    private ImageView imgResendGren;
    // Global Variable...
    private String mCountryCode;
    private String mGetSerialSIM;
    private String strMsisdn;
    private String codeAuth;
    private Timer mTimer;
    private int mCounter = 60;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private ActivityCakraAuthenticationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cakra_authentication);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cakra_authentication);
        binding.setLangModel(model);

        tvPhoneNumber = findViewById(R.id.tvPhoneNumber);
        tvWrongNumber = findViewById(R.id.tvWrongNumber);
        btnVerification = findViewById(R.id.btnVerification);
        pinViewCode = findViewById(R.id.pinViewCode);
        pinViewCode.setInputType(InputType.TYPE_CLASS_TEXT);
        tvTimeCount = findViewById(R.id.tvTimeCount);
        tvResendCode = findViewById(R.id.tvResendCode);
        progress_bar = findViewById(R.id.progress_bar);
        imgResendGrey = findViewById(R.id.imgResendGrey);
        imgResendGren = findViewById(R.id.imgResendGren);

        getBroadcastSMS();
        setTimmer();

        /**
         * Intent Get String ......
         */
        Bundle bundle = getIntent().getExtras();
        if (bundle != null){

            String getMsisdn = bundle.getString("valNum");
            String countyCode = bundle.getString("countyCode");
            String getSerialSIM = bundle.getString("getSerialSIM");

            if (!TextUtils.isEmpty(getMsisdn) && !TextUtils.isEmpty(countyCode) && !TextUtils.isEmpty(getSerialSIM)) {
//            if (!TextUtils.isEmpty(getMsisdn) && !TextUtils.isEmpty(countyCode)) {
                // Global Variable...
                strMsisdn = getMsisdn;
                tvPhoneNumber.setText(strMsisdn);
                mCountryCode = countyCode;
                mGetSerialSIM = getSerialSIM;
                // Call Function...
                requestOTP(strMsisdn);
            }
        }

        btnVerification.setOnClickListener(this);
        tvResendCode.setOnClickListener(this);
        tvWrongNumber.setOnClickListener(this);

        /*
         * Facebook Screen Event
         * */
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_otp");

        /**
         * Google Analitycs Screen......
         */
        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_AUTH);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {

        if (btnVerification == v){ // If Verification On Click...

            String pinValue = pinViewCode.getText().toString();
            if (!TextUtils.isEmpty(pinValue)){
                validateShowProgress(true);
                verifyVerificationCode(pinValue);
            }else {
                showSnackBar(model.getLang().string.wordingEmptyVerificationCode);
            }
            // Hide keyboard...
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(pinViewCode.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
            }
            // Send Google Analitycs...
            if (!TextUtils.isEmpty(strMsisdn)) {
                sendGoogleTracker(TrackerConstant.SCREEN_REGISTER, TrackerConstant.EVENT_CAT_SMS_VERIFICATION, "click verification code", strMsisdn);
                sendGoogleTracker(TrackerConstant.SCREEN_AUTH, TrackerConstant.EVENT_CAT_AUTH, "phone auth succcessful", strMsisdn);
            }

        }else if (tvResendCode == v){ // If Resend Code On Click...
            // Resend verification...
            if (!TextUtils.isEmpty(strMsisdn)) {
                setTimmer();
                requestOTP(strMsisdn);
                validateResendCode(false);
            }else {
                showSnackBar(model.getLang().string.errorFetchingData);
            }

        }else if (tvWrongNumber == v){ // If Wrong Number On Click...
            Intent intent = new Intent(CakraAuthentication.this, PhoneRegister.class);
            startActivity(intent);
            finish();
        }
    }

// ===============================================================================================================================================================
// ============================================================ Life Cycle Android ===============================================================================
// ===============================================================================================================================================================

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.SMS_CODE");
        LocalBroadcastManager.getInstance(CakraAuthentication.this).registerReceiver(gettingSMSCode, _if);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork(tvPhoneNumber);
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopTimmer();
        LocalBroadcastManager.getInstance(CakraAuthentication.this).unregisterReceiver(gettingSMSCode);
    }


// ===============================================================================================================================================================
// ============================================================ Cakra OTP ========================================================================================
// ===============================================================================================================================================================

    private void requestOTP(String msisdn) {
        Call<APIResponse<List<ModelOTP>>> call = ServicesFactory.getService().getOTP(msisdn);
        call.enqueue(new Callback<APIResponse<List<ModelOTP>>>() {
            @Override
            public void onResponse(Call<APIResponse<List<ModelOTP>>> call, Response<APIResponse<List<ModelOTP>>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    List<ModelOTP> data = response.body().data;
                    if (data != null) {
                        try {
                            codeAuth = data.get(0).otp;
                        }catch (NullPointerException e){
                            e.printStackTrace();
                        }
                        validateShowProgress(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResponse<List<ModelOTP>>> call, Throwable t) {
                Log.i("OTP","Gagal");
                if (!TextUtils.isEmpty(msisdn)){
                    sendGoogleTracker(TrackerConstant.SCREEN_AUTH, TrackerConstant.EVENT_CAT_AUTH, "phone auth failed", msisdn);
                }
            }
        });
    }

    /**
     * auth user validation......
     */
    private void oauthToken(String msisdn,String hascode) {
        // Added by hendi
        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        String device_id = TextUtils.isEmpty(android_id)?"":android_id;

        String referrer = InstallReferrerReceiver.getReferrer(getApplicationContext());
        Call<APIResponse<ContactItem>> call = ServicesFactory.getService().cakraOtp(msisdn, referrer, "android",hascode, mGetSerialSIM, device_id);
        call.enqueue(new Callback<APIResponse<ContactItem>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<ContactItem>> call, @NotNull Response<APIResponse<ContactItem>> response) {
                if(response.isSuccessful() && response.body() != null && response.body().isSuccessful()){
                    if (!isViewAttached)
                        return;
                    ContactItem item = response.body().data;
                    if (item != null){

                        if (setPreferenceSimCard() ==  true){

                            String sCallerId = item.caller_id;
                            if(getSinchServiceInterface() != null) {
                                if (!getSinchServiceInterface().isStarted() && !TextUtils.isEmpty(sCallerId)) {
                                    getSinchServiceInterface().startClient(sCallerId); // Send caller_id to server sinch
                                }
                            }

                            SessionManager.saveProfile(CakraAuthentication.this, item);
                            nextProcess();
                        }else {
                            String sError = TextUtils.isEmpty(model.getLang().string.str_session_error)
                                    ?"Save session error....":model.getLang().string.str_session_error;
                            showSnackBar(sError);
                        }

                    }else {
                        String sError = TextUtils.isEmpty(model.getLang().string.str_failed_register)
                                ?"Failed while getting registration data...":model.getLang().string.str_failed_register;
                        showSnackBar(sError);
                    }

                } else{
                    showSnackBar( model.getLang().string.authenticationFailed);

                }
            }
            @Override
            public void onFailure(@NotNull Call<APIResponse<ContactItem>> call, @NotNull Throwable t) {
                if (!CakraAuthentication.this.isFinishing()) {
                    showSnackBar( model.getLang().string.authenticationFailed);
                    if (!TextUtils.isEmpty(msisdn)){
                        sendGoogleTracker(TrackerConstant.SCREEN_AUTH, TrackerConstant.EVENT_CAT_AUTH, "phone auth failed", msisdn);
                    }
                }
            }
        });
    }

    /**
     * Save Preference SIM Card......
     */
    private boolean setPreferenceSimCard(){
        boolean has = false;
        if (!TextUtils.isEmpty(strMsisdn) && !TextUtils.isEmpty(mGetSerialSIM) && !TextUtils.isEmpty(mCountryCode)){
            String setPrefMsisdn = Util.formatMSISDNREG(strMsisdn,mCountryCode);
            AppPreference.setPreference(CakraAuthentication.this, AppPreference.SERIAL_SIM, mGetSerialSIM);
            AppPreference.setPreference(CakraAuthentication.this, AppPreference.CURRENT_MSISDN, setPrefMsisdn);
            has = true;
        }
        return has;
    }

// ===============================================================================================================================================================
// ============================================================ Methode Proses ===================================================================================
// ===============================================================================================================================================================

    /**
     * Sms Retriever Client......
     */
    private void getBroadcastSMS() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.i("onSuccess","SMS Retriever starts");

            }
        });
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                String sError = TextUtils.isEmpty(model.getLang().string.str_error_retrieval)
                        ?"Error Retriever Code":model.getLang().string.str_error_retrieval;
                toast(sError);
                stopTimmer();
            }
        });
    }

    /**
     * Broadcast Receiver......
     */
    private BroadcastReceiver gettingSMSCode = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String retriveData = intent.getStringExtra("msg");
            if(!TextUtils.isEmpty(retriveData)) {
                selectionCaracter(retriveData);
            }
        }
    };

    /**
     * Selection Caracter......
     */
    private void selectionCaracter(String arg){
        String[] split = arg.split(":");
        String hasCode = split[1].substring(1,7);
        pinViewCode.setText(hasCode);
        stopTimmer();
        if (!TextUtils.isEmpty(codeAuth) && !TextUtils.isEmpty(strMsisdn)){
            if (hasCode.equals(codeAuth))
                oauthToken(strMsisdn,hasCode);
        }else {
            showSnackBar( model.getLang().string.authenticationFailed);
            stopTimmer();
        }
    }

    /**
     * Verification code......
     */
    private void verifyVerificationCode(String code) {
        if (!TextUtils.isEmpty(codeAuth) && !TextUtils.isEmpty(strMsisdn) && code.equals(codeAuth)) {
            oauthToken(strMsisdn,code);
        }else {
            String sError = TextUtils.isEmpty(model.getLang().string.str_verification_invalid)
                    ?"Verification code invalid...":model.getLang().string.str_verification_invalid;
            showSnackBar(sError);
            stopTimmer();
        }
    }

    /**
     * Set Timmer......
     */
    private void setTimmer(){
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mCounter--;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            tvTimeCount.setText(String.valueOf(mCounter));
                        }catch (Exception e){
                            return;
                        }

                        if(mCounter == 0){
                            mCounter = 60;
                            mTimer.cancel();
                            stopTimmer();
                        }

                    }
                });

            }
        }, 0, 1000);
    }

    /**
     * Stop Timmer......
     */
    private void stopTimmer(){
        if (mTimer != null){
            mTimer.cancel();
            validateResendCode(true);
            validateShowProgress(false);
        }
    }

    /**
     * Next Process......
     */
    private void nextProcess(){
        // remove referrer
        InstallReferrerReceiver.setReferrer(getApplicationContext(), null);
        AppPreference.setPreference(CakraAuthentication.this, AppPreference.PREFIX, mCountryCode);
        PreferenceUtil.getEditor(CakraAuthentication.this).putBoolean(PreferenceUtil.ISLOGIN, true).commit();
        if (!isViewAttached)
            return;

        boolean isSwitch = PreferenceUtil.getPref(CakraAuthentication.this).getBoolean(PreferenceUtil.ISWITCH, false); //Is Login
        if (isSwitch == true){
            startActivity(new Intent(CakraAuthentication.this, IntroActivitySecond.class));
        }else {
            startActivity(new Intent(CakraAuthentication.this, IntroActivity.class));
        }

        generateTokenFirebase();

        finish();
    }

    /**
     * Validate UI Resend Code......
     */
    private void validateResendCode(boolean isResend){
        if (!CakraAuthentication.this.isFinishing()) {
            if (isResend == true) {
                tvResendCode.setTextColor(getResources().getColor(R.color.green_dark));
                tvResendCode.setEnabled(true);
                imgResendGrey.setVisibility(View.GONE);
                imgResendGren.setVisibility(View.VISIBLE);
                tvTimeCount.setVisibility(View.GONE);
                mCounter = 60;
            } else {
                tvResendCode.setTextColor(getResources().getColor(R.color.grey_ACACACF));
                tvResendCode.setEnabled(false);
                imgResendGren.setVisibility(View.GONE);
                imgResendGrey.setVisibility(View.VISIBLE);
                tvTimeCount.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Validate Show Progress......
     */
    private void validateShowProgress(boolean isShowProgress){
        if (!CakraAuthentication.this.isFinishing()) {
            if (isShowProgress == true) {
                progress_bar.setVisibility(View.VISIBLE);
                btnVerification.setVisibility(View.INVISIBLE);
            }else {
                progress_bar.setVisibility(View.INVISIBLE);
                btnVerification.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * Google Analitycs Tracker......
     */
    private void sendGoogleTracker(String screen, String event, String action, String phoneNumber){
        try {
            GATRacker.getInstance(getApplication()).sendEventWithScreen(screen, event, action, phoneNumber);
        }catch (Exception e){
            e.printStackTrace();
            return;
        }
    }

    /**
     * UI Snackbar........
     */
    private void showSnackBar(String argument){
        if (!isViewAttached)
            return;
        try{
            Snackbar.make(findViewById(android.R.id.content), argument, Snackbar.LENGTH_LONG).show();
        }catch (Exception e){
            return;
        }
    }

    /**
     * Double tap to exit......
     */
    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        showSnackBar(model.getLang().string.pressToExit);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onServiceConnected() {
        if(getSinchServiceInterface() != null) {
            getSinchServiceInterface().setStartListener(this);
        }
    }

    @Override
    public void onStartFailed(SinchError error) {}

    @Override
    public void onStarted() {}

// ===============================================================================================================================================================
// ===============================================================================================================================================================
// ===============================================================================================================================================================

}
