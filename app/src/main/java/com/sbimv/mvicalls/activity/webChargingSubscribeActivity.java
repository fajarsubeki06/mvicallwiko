package com.sbimv.mvicalls.activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataSubscriberUser;
import com.sbimv.mvicalls.util.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class webChargingSubscribeActivity extends AppCompatActivity {
    private SwipeRefreshLayout swipeRefreshLayout;
    private WebView webView;
    private Button btnDone;
    ContactItem own;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_charging_subscribe);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        btnDone = findViewById(R.id.btnDone);
        own = SessionManager.getProfile(this);
        pd= new ProgressDialog(this);
        pd.setMessage("Processing ...");
        pd.setCancelable(false);

        String url = getIntent().getStringExtra("urlCharging");
        webView = findViewById(R.id.webViewCharging);
        webView.setWebViewClient(new CustomWebViewClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setInitialScale(320);
        webView.loadUrl(url);

        swipeRefreshLayout = findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.reload();
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();
                checkSubscription(own.msisdn);
            }
        });
    }

    private void checkSubscription(String msisdn) {
        Call<DataSubscriberUser> call = ServicesFactory.getService().getStatusSubscribe(msisdn);
        call.enqueue(new Callback<DataSubscriberUser>() {
            @Override
            public void onResponse(Call<DataSubscriberUser> call, Response<DataSubscriberUser> response) {
                pd.dismiss();
                finish();
            }

            @Override
            public void onFailure(Call<DataSubscriberUser> call, Throwable t) {

            }
        });
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String request) {
            view.loadUrl(request);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
