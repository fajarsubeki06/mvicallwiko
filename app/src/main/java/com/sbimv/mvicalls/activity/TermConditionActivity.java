package com.sbimv.mvicalls.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataAboutmvicall;
import com.sbimv.mvicalls.pojo.DataAppSettings;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermConditionActivity extends BaseActivity {

    SwipeRefreshLayout swipeRefreshLayout;
    private WebView wv_tnc;
    private String urlTnC;
    private ProgressBar pgTnc;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_condition);
        setDefaultToolbar(true, model.getLang().string.termOfService);

        swipeRefreshLayout = findViewById(R.id.swapTNC);
        pgTnc = findViewById(R.id.pgTnc);
        wv_tnc = findViewById(R.id.wv_tnc);

        wv_tnc.getSettings().setJavaScriptEnabled(true);
        wv_tnc.getSettings().setBuiltInZoomControls(true);
        wv_tnc.getSettings().setDisplayZoomControls(true);

        getUrlTnC();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                wv_tnc.reload();
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(TermConditionActivity.this).unregisterReceiver(popupNotifAppears);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork(wv_tnc);
        LocalBroadcastManager.getInstance(TermConditionActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

    private void getUrlTnC () {
        pgTnc.setVisibility(View.VISIBLE);
//        ContactItem own = SessionManager.getProfile(TermConditionActivity.this);
//        if (own != null) {

            String lang = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
//            String msisdn = own.msisdn;
            if (!TextUtils.isEmpty(lang)) {

                Call<APIResponse<DataAppSettings>> call = ServicesFactory.getService().getAppSettings(lang);
                call.enqueue(new Callback<APIResponse<DataAppSettings>>() {
                    @Override
                    public void onResponse(@NotNull Call<APIResponse<DataAppSettings>> call, @NotNull Response<APIResponse<DataAppSettings>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            if (!TermConditionActivity.this.isFinishing()) {
                                DataAboutmvicall data = response.body().data.getAboutmvicall();
                                if (data != null) {
                                    urlTnC = data.getTnc();
                                    if (!TextUtils.isEmpty(urlTnC)) {
                                        wv_tnc.setWebViewClient(new CustomWebViewClient());
                                        wv_tnc.loadUrl(urlTnC);
                                    }
                                }
                            }
                        }
                        hideProgressBar();
                    }

                    @Override
                    public void onFailure(@NotNull Call<APIResponse<DataAppSettings>> call, @NotNull Throwable t) {
                        hideProgressBar();
                    }
                });

            }
//        }
    }

    private void hideProgressBar(){
        if (!TermConditionActivity.this.isFinishing()) {
            pgTnc.setVisibility(View.GONE);
        }
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String urlWeb) {
            swipeRefreshLayout.setRefreshing(false);
            urlTnC = urlWeb;
            super.onPageFinished(view, urlTnC);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
        }
    }

}
