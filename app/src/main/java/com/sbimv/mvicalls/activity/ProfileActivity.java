package com.sbimv.mvicalls.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.appevents.AppEventsLogger;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.PreviewPremiumVideo;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.adapter.ProfileContactAdapter;
import com.sbimv.mvicalls.databinding.ActivityProfileNewBinding;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.http.ProgressRequestBody;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataSubs;
import com.sbimv.mvicalls.pojo.ProfileItem;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.ImageSelector;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseWrapper;
import com.sbimv.mvicalls.util.Util;
import com.sbimv.mvicalls.util.ValidationInputUtil;
import com.sbimv.mvicalls.view.DialogUpload;
import com.sbimv.mvicalls.view.DividerItemDecoration;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity {

    // request profile
    public final int REQUEST_ADD_PROFILE = 908;
    public final int REQUEST_EDIT_PROFILE = 907;
    public ImageView btnUserStatus;
    // flags
    boolean isEditingStatus;
    ShowCaseWrapper showCaseWrapper;
    // temporary path
    private File proPicPath;
    // view bind
    private CircleImageView ivUserPic;
    private Button btnSave;
    private LinearLayout btnAddProfile;
    private EditText etUserName;
    private EditText etUserPhone;
    private EditText etUserEmail;
    private EditText etUserStatus;
    private TextView tvUserStatus;
    private RecyclerView rvProfile;
    // variables
    private DialogUpload dialogUpload;
    private ContactItem own;
    private ProfileContactAdapter adapter;
    private boolean isExperience;
    private String cont = "";
    private String endDate;
    private LinearLayout checkStatus;
    // added by Hendi
    private ImageSelector mImageSelector;
    private EditText mEtSetProfile;
    private LinearLayout mLayoutActive;
    private Button mBtnSubscribeNow;
    private TextView mTvActiveUntil;
    private TextView tvWriteProfile;
    private Cursor cursor;
    private ActivityProfileNewBinding binding;

    private TextWatcher textWatcherUserName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            checkProfileChanged();
        }
    };
    private TextWatcher textWatcherUserEmail = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            checkProfileChanged();
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile_new);
        binding.setLangModel(model);
        setDefaultToolbar(true);

        // added by Hendi
        tvWriteProfile = findViewById(R.id.et_set_profile);
        tvWriteProfile.setHint(model.getLang().string.writeProfile);
        mTvActiveUntil = findViewById(R.id.tv_active_until);
        mLayoutActive = findViewById(R.id.layout_active);
        mBtnSubscribeNow = findViewById(R.id.btn_subscribe_now);
        btnAddProfile = findViewById(R.id.btn_add_profile);
        ivUserPic = findViewById(R.id.iv_user_pic);
        btnSave = findViewById(R.id.btnSave);
        etUserName = findViewById(R.id.et_user_name);
        etUserPhone = findViewById(R.id.et_user_phone);
        etUserEmail = findViewById(R.id.et_user_email);
        etUserStatus = findViewById(R.id.et_user_status);
        tvUserStatus = findViewById(R.id.tv_user_status);
        mEtSetProfile = findViewById(R.id.et_set_profile);
        checkStatus = findViewById(R.id.linProgressCheck);
        rvProfile = findViewById(R.id.rv_profile);
        btnUserStatus = findViewById(R.id.btn_edit_status);

        btnUserStatus.setEnabled(true);
        mLayoutActive.setVisibility(View.GONE);
        mBtnSubscribeNow.setVisibility(View.GONE);
        etUserName.addTextChangedListener(textWatcherUserName);
        etUserEmail.addTextChangedListener(textWatcherUserEmail);
        mEtSetProfile.addTextChangedListener(textWatcherUserEmail);
        etUserPhone.setKeyListener(null);

        own = SessionManager.getProfile(ProfileActivity.this);

        if (isViewAttached) {
            showProgressBar();
            setUserInfo();
            setContactStatus();
            configDynamic(ProfileActivity.this);
            if (own != null)
                getDataSubs(own.msisdn); // butuh cek own
        }

        showCaseWrapper = ShowCaseWrapper.create(ProfileActivity.this);
//        AppPreference.setPreferenceBoolean(ProfileActivity.this, AppPreference.SHOW_CASE_PROFILE, false);
        mImageSelector = ImageSelector.create(ProfileActivity.this, new ImageSelector.ImageSelectorDelegate() {
            @Override
            public void onImageSelected(Uri uri) {
                CropImage.activity(uri).setAspectRatio(1, 1).start(ProfileActivity.this);
            }
        });

        mBtnSubscribeNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ContactItem own = SessionManager.getProfile(ProfileActivity.this);
//                if (own != null)
//                    getCriteriaUmb(own.msisdn);

                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                    startActivity(new Intent(ProfileActivity.this, PopupVietSubsActivity.class));
                    return;
                }

                Intent intent = new Intent(ProfileActivity.this, PopUpSubsActivity.class);
                startActivity(intent);

            }
        });

        btnAddProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkSubs(own.msisdn, new CheckSubscribe() {
                    @Override
                    public void success(boolean isSuccess) {
                        if (!BaseActivity.contentBuys) {
                            if (!isSuccess) {
                                if (!isViewAttached)
                                    return;

//                                if (own != null)
//                                    getCriteriaUmb(own.msisdn);

                                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                                    startActivity(new Intent(ProfileActivity.this, PopupVietSubsActivity.class));
                                    return;
                                }

                                Intent intent = new Intent(ProfileActivity.this, PopUpSubsActivity.class);
                                startActivity(intent);

                            } else {
                                isExperience = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.SYNC_EXPERIENCE, false);
                                if (isExperience) {
                                    if (!isViewAttached)
                                        return;
                                    Intent i = new Intent(ProfileActivity.this, AddProfileActivity.class);
                                    startActivityForResult(i, REQUEST_ADD_PROFILE);
                                } else {
                                    Toast.makeText(ProfileActivity.this, model.getLang().string.checkSync, Toast.LENGTH_SHORT).show();
                                }
                            }
                            GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_PROFILE, TrackerConstant.EVENT_CAT_PROFILE, "click button add contact status", own.caller_id);
                        }
                    }
                });
            }
        });

//        checkShowCase();

        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_PROFILE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logFcb_screen_editprofileEvent();
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logFcb_screen_editprofileEvent () {
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_editprofile");
    }


    public SharedPreferences getPreference() {
        return ProfileActivity.this.getSharedPreferences("showcase_preference", Context.MODE_PRIVATE);
    }

//    private void checkShowCase() {
//        boolean isNameShow = AppPreference.getPreferenceBoolean(ProfileActivity.this, AppPreference.BUBBLE_VIEW_NAME);
//        boolean isStatusShow = AppPreference.getPreferenceBoolean(ProfileActivity.this, AppPreference.BUBBLE_VIEW_STATUS);
//        if (isNameShow) {
//            scEditName();
//        } else if (isStatusShow) {
//            scEditStatus();
//        }
//    }

//    private void scEditName() {
//        showCaseWrapper.showFancydoAction(ShowCaseWrapper.EDIT_NAME, model.getLang().string.wordingUpdateName, etUserName, new ShowCaseListener.ActionListener() {
//            @Override
//            public void onScreenTap() {
//                startEditName();
//                AppPreference.setPreferenceBoolean(ProfileActivity.this, AppPreference.BUBBLE_VIEW_NAME, false);
//            }
//
//            @Override
//            public void onOk() {
//                startEditName();
//                AppPreference.setPreferenceBoolean(ProfileActivity.this, AppPreference.BUBBLE_VIEW_NAME, false);
//            }
//
//            @Override
//            public void onSkip() {
//                scEditStatus();
//            }
//        });
//    }

//    private void scEditStatus() {
//        showCaseWrapper.showFancydoAction(ShowCaseWrapper.EDIT_STATUS, model.getLang().string.wordingUpdateStatus, btnUserStatus, new ShowCaseListener.ActionListener() {
//            @Override
//            public void onScreenTap() {
//                startEditStatus();
//                AppPreference.setPreferenceBoolean(ProfileActivity.this, AppPreference.BUBBLE_VIEW_STATUS, false);
//            }
//
//            @Override
//            public void onOk() {
//                startEditStatus();
//                AppPreference.setPreferenceBoolean(ProfileActivity.this, AppPreference.BUBBLE_VIEW_STATUS, false);
//            }
//
//            @Override
//            public void onSkip() {
//                scNext();
//            }
//        });
//    }


//    private void scNext() {
//        //flag for sc edit status
//        PreferenceUtil.getEditor(ProfileActivity.this).putBoolean(PreferenceUtil.SC_STATUS,true).commit();
//
//        if (showCaseWrapper == null){
//            showCaseWrapper = ShowCaseWrapper.create(ProfileActivity.this);
//        }
//        showCaseWrapper.showFancyNext(ShowCaseWrapper.ID_PROFILE, new ShowCaseListener.NextListener() {
//            @Override
//            public void onNext() {
//                finish();
//            }
//        });
//    }
//
//    private void startEditName() {
//        etUserName.requestFocus();
//        etUserName.setSelection(etUserName.getText().length());
//        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(etUserName, InputMethodManager.SHOW_IMPLICIT);
//    }

    // ================================== Check Subs Buy, Call API ================================== added by Yudha Pratama Putra, 15 okt 2018
//    private void getCriteriaUmb(String msisdn) {
//        Call<APIResponse<ArrayList<DataChargingContent>>> call = ServicesFactory.getService().getChargingContent(msisdn, "", "sub");
//        call.enqueue(new Callback<APIResponse<ArrayList<DataChargingContent>>>() {
//            @Override
//            public void onResponse(Call<APIResponse<ArrayList<DataChargingContent>>> call, Response<APIResponse<ArrayList<DataChargingContent>>> response) {
//                if (response.isSuccessful() && response.body().isSuccessful()) {
//                    ArrayList<DataChargingContent> data = response.body().data;
//                    if (data != null && data.size() > 0) {
//                        for (int i = 0; i < data.size(); i++) {
//                            DataChargingContent chargingData = data.get(i);
//                            String keyword = chargingData.getKeyword();
//                            if (!TextUtils.isEmpty(keyword) && keyword.startsWith("*") && keyword.endsWith("#")) {
//                                callChargingContent(keyword);
//                                return;
//                            }
//                        }
//                    }else {
//                        startActivity(new Intent(ProfileActivity.this, PopUpSubsActivity.class));
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIResponse<ArrayList<DataChargingContent>>> call, Throwable t) {
//
//            }
//        });
//    }

    // ================================== Method Call UMB... ================================== added by Yudha Pratama Putra, 15 okt 2018
//    private void callChargingContent(String umb) {
//        if (ProfileActivity.this != null && !ProfileActivity.this.isFinishing() && !TextUtils.isEmpty(umb)) {
//            Intent intent = new Intent(Intent.ACTION_DIAL);
//            intent.setData(Uri.fromParts("tel", umb, null));
//            if (intent.resolveActivity(getPackageManager()) != null) {
//                startActivity(intent);
//            } else {
//                toast("Applikasi phone call tidak di temukan. Anda harus tentukan settingan default phone call");
//            }
//        }
//    }
    // =========================================================================================

    @Override
    protected void onStart() {
        super.onStart();
        refreshListProfile();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showBroadcastConfig(ProfileActivity.this);
        checkNetwork(etUserName);
        LocalBroadcastManager.getInstance(ProfileActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopBroadcastConfig(ProfileActivity.this);
        LocalBroadcastManager.getInstance(ProfileActivity.this).unregisterReceiver(popupNotifAppears);
    }

    public void refreshListProfile() {
        isExperience = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.SYNC_EXPERIENCE, false);
        if (isExperience) {
            if (isViewAttached) {
                getDefaultStatusProfile();
            }
        }
    }

    protected void getDefaultStatusProfile() {
        if (own != null) {
            Call<APIResponse<ContactItem>> call = ServicesFactory.getService().listProfileStatus(own.msisdn);
            call.enqueue(new Callback<APIResponse<ContactItem>>() {
                @Override
                public void onResponse(Call<APIResponse<ContactItem>> call, Response<APIResponse<ContactItem>> response) {
                    if (response.isSuccessful() && response.body().isSuccessful()) {

                        ContactItem item = response.body().data;
                        own.profiles = item.profiles;
                        SessionManager.saveProfile(getApplicationContext(), own);
                        saveContactFirebase(own);
                        if (isViewAttached) {
                            setContactStatus();
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResponse<ContactItem>> call, Throwable t) {
                    if (isViewAttached) {
                        showSnackBar(btnAddProfile, model.getLang().string.wordingFailureResponse);
                    }
                }
            });
        }
    }

    private void getDataSubs(String phoneNumber) {
        mBtnSubscribeNow.setVisibility(View.GONE);
        mLayoutActive.setVisibility(View.GONE);
        showProgressBar();

        BaseActivity.getSubscriptionStatus(ProfileActivity.this, phoneNumber, new SubscriptionStatusListener() {
            @Override
            public void onComplete(DataSubs subsData) {
                if (!isViewAttached) // skip updating view
                    return;

                dismissProgressBar();
                if (!BaseActivity.isSubscribeActive(subsData) && !BaseActivity.isFreemiumActive(subsData)) {
                    // show subscribe button
                    mBtnSubscribeNow.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFail() {
                if (!isViewAttached) // skip updating view
                    return;

                dismissProgressBar();
            }
        });
    }

    @SuppressLint("WrongConstant")
    public void setContactStatus() {
        ContactDBManager contactDBM = new ContactDBManager(ProfileActivity.this);
        if (contactDBM == null) return;
        cursor = contactDBM.getMVContactAll();
        if (cursor == null) return;

        LinearLayoutManager layout = new LinearLayoutManager(ProfileActivity.this);
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        rvProfile.setLayoutManager(layout);
        rvProfile.addItemDecoration(new DividerItemDecoration(ProfileActivity.this, DividerItemDecoration.VERTICAL_LIST, ContextCompat.getColor(ProfileActivity.this, R.color.grey_e4e4e4), (int) Util.convertDpToPixel(0.5f, ProfileActivity.this)));

        adapter = new ProfileContactAdapter(own, cursor, ProfileActivity.this, new ProfileContactAdapter.Listener() {
            @Override
            public void onEditProfile(final int index) {
                checkSubs(own.msisdn, new CheckSubscribe() {
                    @Override
                    public void success(boolean isSuccess) {
                        if (BaseActivity.contentBuys == false) {
                            if (isSuccess == false) {
                                if (!isViewAttached) return;

//                                if (own != null)
//                                    getCriteriaUmb(own.msisdn);

                                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                                    startActivity(new Intent(ProfileActivity.this, PopupVietSubsActivity.class));
                                    return;
                                }

                                Intent intent = new Intent(ProfileActivity.this, PopUpSubsActivity.class);
                                startActivity(intent);

                            } else {
                                ProfileItem.Item item = adapter.getData().get(index);
                                Intent i = new Intent(ProfileActivity.this, AddProfileActivity.class);
                                i.putExtra("item", item);
                                startActivityForResult(i, REQUEST_EDIT_PROFILE);
                            }
                        }

                    }
                });
            }

            @Override
            public void onDeleteProfile(final int index) {

                if (own == null) {
                    return;
                }

                // delete profile for custom contact
                checkSubs(own.msisdn, new CheckSubscribe() {
                    @Override
                    public void success(boolean isSuccess) {
                        if (BaseActivity.contentBuys == false) {

                            if (isSuccess == false) {
                                if (!isViewAttached) return;

                                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                                    startActivity(new Intent(ProfileActivity.this, PopupVietSubsActivity.class));
                                    return;
                                }

                                Intent intent = new Intent(ProfileActivity.this, PopUpSubsActivity.class);
                                startActivity(intent);

                            } else {
                                final ProfileItem.Item item = adapter.getData().get(index);
                                String message;
                                if (item.name != null) {
                                    message = model.getLang().string.areYouSureDeleteStatus + " " + item.name + " ?";
                                } else {
                                    message = model.getLang().string.areYouSureDeleteStatus + " " + item.msisdn + " ?";
                                }

                                if (!ProfileActivity.this.isFinishing()) {
                                    new AlertDialog.Builder(ProfileActivity.this).setTitle(model.getLang().string.deleteStatus).setMessage(message).setPositiveButton(model.getLang().string.titleYes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            showDialogs();
                                            new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    showCustomDialog(model.getLang().string.removingProfile);
                                                    deleteProfile(index);
                                                }
                                            }, 200);
                                        }
                                    }).setNegativeButton(model.getLang().string.titleNo, null).show();
                                }
                            }

                        }
                    }
                });
            }
        });
        rvProfile.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        ViewCompat.setNestedScrollingEnabled(rvProfile, false);

    }

    private void deleteProfile(final int index) {
        if (own != null) {
            final ProfileItem.Item item = adapter.getData().get(index);
            Call<APIResponse> call = ServicesFactory.getService().deleteProfile(own.caller_id, item.msisdn);
            call.enqueue(new Callback<APIResponse>() {
                @Override
                public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                    dissmisDialogs();
                    if (response.isSuccessful() && response.body().isSuccessful()) {
                        toastShort(model.getLang().string.profileRemoved);

                        for (int i = 0; i < own.profiles.list.size(); i++) {
                            if (own.profiles.list.get(i).msisdn.equals(item.msisdn)) {
                                own.profiles.list.remove(i);
                                SessionManager.saveProfile(ProfileActivity.this, own);
                                break;
                            }
                        }

                        // invalidate adapter
                        adapter.getData().remove(index);
                        adapter.setData();
                        // Firebase Update Data
                        saveContactFirebase(own);

                    } else {
                        String remove = TextUtils.isEmpty(model.getLang().string.str_edit_profile_remove_failed)
                                ?"Remove Failed"
                                : model.getLang().string.str_edit_profile_remove_failed;
                        toastShort(remove);
                    }
                }

                @Override
                public void onFailure(Call<APIResponse> call, Throwable t) {

                }
            });
        }
    }

    @SuppressLint("CheckResult")
    private void setUserInfo() {
        if (own != null) {
            etUserPhone.setText(own.msisdn);
            etUserName.setText(own.getName());
            etUserEmail.setText(own.getEmail());
            mEtSetProfile.setText(own.profile_status);

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.userpng);

            Glide.with(ProfileActivity.this).load(own.caller_pic).apply(requestOptions).into(ivUserPic);
            saveContactFirebase(own);

            tvUserStatus.setText(own.getStatusCaller());
            etUserStatus.setHint(model.getLang().string.whatDoYouThink);

            if (TextUtils.isEmpty(own.status_caller)) {
                etUserStatus.setText("");
            } else {
                etUserStatus.setText(own.status_caller);
            }
        }
    }

    private void selectImage() {
        new AlertDialog.Builder(this)
                .setTitle(model.getLang().string.selectImageFrom)
                .setItems(new String[]{model.getLang().string.selectImageFromCamera, model.getLang().string.gallery},
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (i == 0)// camera
                                    mImageSelector.captureImage();
                                else // gallery
                                    mImageSelector.openGallery();
                            }
                        })
                .show();
    }

    public void doAction(View view) {
        int vID = view.getId();
        if (vID == R.id.btn_edit_user_pic) {
            selectImage();
            sendGoogleAnalitycs("click change picture");
        } else if (vID == R.id.iv_user_pic) {
            selectImage();
            sendGoogleAnalitycs("click change picture");
        } else if (vID == R.id.btnSave) {
            Boolean update = (Boolean) btnSave.getTag();
            if (update != null && update) {
                if (checkValidation()) {
                    etUserName.clearFocus();
                    etUserEmail.clearFocus();
                    mEtSetProfile.clearFocus();
                    Util.hideKeyboard(this);

                    showDialogs();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showCustomDialog(model.getLang().string.updatingProfile);
                            updateProfile();
                            sendGoogleAnalitycs("click save profile");
                        }
                    }, 200);
                }
            }
        } else if (vID == R.id.btn_edit_status) {
            startEditStatus();
        } else
            ucInfo();
    }

    private void sendGoogleAnalitycs(String action) {
        try {
            ContactItem own = SessionManager.getProfile(ProfileActivity.this);
            if (own != null) {
                GATRacker.getInstance(getApplication())
                        .sendEventWithScreen(TrackerConstant.SCREEN_PROFILE, TrackerConstant.EVENT_CAT_SAVE_PERSONAL_PROFILE, action,
                                own.caller_id + "/" + action);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startEditStatus() {
        try {
            GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_PROFILE, TrackerConstant.EVENT_CAT_PROFILE, "click button status", own.caller_id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!isEditingStatus) {
            isEditingStatus = true;
            btnUserStatus.setImageResource(R.drawable.ic_checkbox_on);
            tvUserStatus.setVisibility(View.GONE);
            etUserStatus.setVisibility(View.VISIBLE);
            etUserStatus.setSelection(etUserStatus.getText().length());
            etUserStatus.requestFocus();
            Util.showKeyboard(this, etUserStatus);

        } else {

            isEditingStatus = false;
            btnUserStatus.setImageResource(R.drawable.ic_edit);
            tvUserStatus.setVisibility(View.VISIBLE);
            etUserStatus.setVisibility(View.GONE);
            etUserStatus.clearFocus();
            Util.hideKeyboard(ProfileActivity.this);

            final String etStr = etUserStatus.getText().toString();
            if (own != null) {
                String currentStatus = own.status_caller == null ? "" : own.status_caller;
                if (!etStr.equals(currentStatus)) {

                    checkSubs(own.msisdn, new CheckSubscribe() {
                        @Override
                        public void success(boolean isSuccess) {
                            if (!BaseActivity.contentBuys) {
                                if (!isSuccess) {
                                    if (!isViewAttached) return;

                                    String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                    if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                                        startActivity(new Intent(ProfileActivity.this, PopupVietSubsActivity.class).putExtra("scase","scase"));
                                        return;
                                    }

                                    Intent intent = new Intent(ProfileActivity.this, PopUpSubsActivity.class);
                                    intent.putExtra("scase","scase");
                                    startActivity(intent);

                                } else {
                                    showDialogs();
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            showCustomDialog(model.getLang().string.updatingProfile);
                                            updateStatusOnly(etStr);
                                        }
                                    }, 200);
                                }
                            }

                        }
                    });
                }
            }

        }
    }

    private void updateStatusOnly(final String status) {
        if (own != null) {
            Call<APIResponse<String>> call = ServicesFactory.getService().updateCallerStatus(own.caller_id, status);
            call.enqueue(new Callback<APIResponse<String>>() {
                @Override
                public void onResponse(Call<APIResponse<String>> call, Response<APIResponse<String>> response) {
                    if (response.isSuccessful() && response.body().isSuccessful()) {
                        dissmisDialogs();
                        toastShort(model.getLang().string.updateStatusSuccess);
                        own.status_caller = status;
                        SessionManager.saveProfile(ProfileActivity.this, own);
                        saveContactFirebase(own);
                        if (!isViewAttached)
                            return;
                        // update ui
                        setUserInfo();
                        checkProfileChanged();

//                        boolean isOpened = PreferenceUtil.getPref(ProfileActivity.this).getBoolean(PreferenceUtil.SC_STATUS,false);
//                        if (!isOpened){
//                            scNext();
//                        }

                        try {
                            GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_PROFILE, TrackerConstant.EVENT_CAT_PROFILE, "update status success", own.caller_id);
                        } catch (Exception e) {
                            return;
                        }

                    } else {
                        toastShort(model.getLang().string.updateStatusFailed);
                        try {
                            GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_PROFILE, TrackerConstant.EVENT_CAT_PROFILE, "update status failed", own.caller_id);
                        } catch (Exception e) {
                            return;
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIResponse<String>> call, Throwable t) {

                    if (!isViewAttached) return;
                    toastShort(model.getLang().string.updateStatusFailed);
                    dissmisDialogs();
                    try {
                        GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_PROFILE, TrackerConstant.EVENT_CAT_PROFILE, "update status failed", own.caller_id);
                    } catch (Exception e) {
                        return;
                    }
                }
            });

        }
    }

    private void updateProfile() {
        if (own != null) {
            // multipart
            MultipartBody.Part _file = null;
            if (proPicPath != null) {
                // ini progress listener
                ProgressRequestBody.ProgressListener progressListener = new ProgressRequestBody.ProgressListener() {
                    @Override
                    public void transferred(final int num, long transferred, long totalSize) {
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    showCustomDialog("Updating " + num + "%");
                                } catch (Exception ne) {
                                    ne.printStackTrace();
                                }
                            }
                        });
                    }
                };

                File file = new File(String.valueOf(proPicPath));
                // init request body
                ProgressRequestBody requestFileBody = new ProgressRequestBody(file, "multipart/form-data", progressListener);
                _file = MultipartBody.Part.createFormData("caller_pic", file.getName(), requestFileBody);
                System.out.println("ini file multipart : "+_file);
            }

            // set request body
            RequestBody _caller_id = RequestBody.create(MediaType.parse("text/plain"), own.caller_id);
            RequestBody _name = null;
            if (!etUserName.getText().toString().isEmpty())
                _name = RequestBody.create(MediaType.parse("text/plain"), etUserName.getText().toString());
            RequestBody _email = null;
            if (!etUserEmail.getText().toString().isEmpty())
                _email = RequestBody.create(MediaType.parse("text/plain"), etUserEmail.getText().toString());
            RequestBody _profile = null;
            if (!mEtSetProfile.getText().toString().isEmpty())
                _profile = RequestBody.create(MediaType.parse("text/plain"), mEtSetProfile.getText().toString());

            Call<APIResponse<ContactItem>> call = ServicesFactory.getService().updateProfile(_caller_id, _name, _email, _profile, _file);
            call.enqueue(new Callback<APIResponse<ContactItem>>() {
                @Override
                public void onResponse(Call<APIResponse<ContactItem>> call, Response<APIResponse<ContactItem>> response) {
                    dissmisDialogs();
                    if (response.isSuccessful() && response.body().isSuccessful()) {
                        proPicPath = null;
                        ContactItem updated = response.body().data;
                        updated.profiles = null;// Untuk syncronize firebase
                        // save to session and update local variable
                        own = SessionManager.saveProfile(ProfileActivity.this, updated);
                        if (own != null) {
                            saveContactFirebase(own);
                        }
                        // update ui
                        if (isViewAttached) {
                            setUserInfo();
                            checkProfileChanged();
                            if (adapter != null)
                                adapter.notifyDataSetChanged();
                            toast(model.getLang().string.updateProfileSuccess);
//                            checkShowCase();
                            try {
                                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_PROFILE, TrackerConstant.EVENT_CAT_PROFILE, "update profile success", own.caller_id);
                            } catch (Exception e) {
                                e.printStackTrace();
                                return;
                            }
                        }

                    } else {

                        toast(model.getLang().string.updateProfileFailed);
                        try {
                            GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_PROFILE, TrackerConstant.EVENT_CAT_PROFILE, "update profile failed", own.caller_id);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        checkShowCase();
                    }
                }

                @Override
                public void onFailure(Call<APIResponse<ContactItem>> call, Throwable t) {
                    if (!isViewAttached) return;
                    dissmisDialogs();
                    toast(model.getLang().string.updateProfileFailed);
                    try {
                        GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_PROFILE, TrackerConstant.EVENT_CAT_PROFILE, "update profile failed", own.caller_id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mImageSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result for crop image
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultsUri = result.getUri();
                proPicPath = new File(resultsUri.getPath());

                Glide.with(ProfileActivity.this)
                        .load(proPicPath)
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivUserPic);

                checkProfileChanged();
            }
        } else if (resultCode == RESULT_OK && (requestCode == REQUEST_ADD_PROFILE || requestCode == REQUEST_EDIT_PROFILE)) {
            if (own != null) {
                ContactItem item = data.getParcelableExtra("contact");
                own.profiles = item.profiles;
                SessionManager.saveProfile(this, own);
                saveContactFirebase(own);
                adapter.setData();
            }
        } else {
            mImageSelector.onActivityResult(requestCode, resultCode, data);
        }
    }


    void checkProfileChanged() {
        boolean isChanged = false;
        if (own != null) {
            if (proPicPath != null) isChanged = true;

            if (!etUserName.getText().toString().equals(own.getName())) isChanged = true;

            if (!etUserEmail.getText().toString().equals(own.getEmail())) isChanged = true;

            if (!mEtSetProfile.getText().toString().equals(own.profile_status)) isChanged = true;

            if (isChanged) {
                btnSave.setBackground(getResources().getDrawable(R.drawable.selector_rounded_green));
                btnSave.setTag(true);
            } else {
                btnSave.setBackground(getResources().getDrawable(R.drawable.selector_rounded_grey_e4e4e4));
                btnSave.setTag(false);
            }
        }
    }

    /**
     * UI Threads
     * Added by Yudha Pratama Putra, 08 Okt 2018
     */
    private void showCustomDialog(String msg) {
        if (ProfileActivity.this.isFinishing()) // skip updating view
            return;
        try {
            dialogUpload.setMessage(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialogs() {
        if (ProfileActivity.this.isFinishing()) // skip updating view
            return;
        try {
            dialogUpload = new DialogUpload();
            dialogUpload.show(getSupportFragmentManager(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dissmisDialogs() {
        if (ProfileActivity.this.isFinishing()) // skip updating view
            return;
        try {
            dialogUpload.dismiss();
            dialogUpload = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showProgressBar() {
        if (ProfileActivity.this.isFinishing()) // skip updating view
            return;
        try {
            checkStatus.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dismissProgressBar() {
        if (ProfileActivity.this.isFinishing()) // skip updating view
            return;
        try {
            checkStatus.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!ValidationInputUtil.hasText(etUserName)) ret = false;
        return ret;
    }

}
