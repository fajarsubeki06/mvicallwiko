package com.sbimv.mvicalls.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

public class TselActivity extends AppCompatActivity {

    private WebView webView;
    private String sUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_tsel);

        webView = findViewById(R.id.web_view);

        Bundle bundle = getIntent().getExtras();
        initDataBundle(bundle);

        setupWebView();
//        loadUrl("http://117.54.3.28:11680/layout/");

    }

    @SuppressLint({"SetJavaScriptEnabled", "JavascriptInterface"})
    private void setupWebView() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(false);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);

        webView.setWebViewClient(new WebViewClientExtension());
        webView.setWebChromeClient(new WebChromeClientExtension());
        webView.addJavascriptInterface(new WebAppInterface(), "Android");
    }

    private class WebViewClientExtension extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            final Uri uri = Uri.parse(url);
            return handleUri(uri);
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            final Uri uri = request.getUrl();
            return handleUri(uri);
        }

        private boolean handleUri(final Uri uri) {
            return false;
        }
    }

    private class WebChromeClientExtension extends WebChromeClient {
        // TODO
    }

    private void loadUrl(String url) {
        if (!TextUtils.isEmpty(url)) {
            webView.loadUrl(url);
        }
    }

    private void initDataBundle(Bundle bundle){
        if (bundle != null){
            sUrl = TextUtils.isEmpty(bundle.getString("sUrl"))?"":bundle.getString("sUrl");
            loadUrl(sUrl);
        }
    }

    private class WebAppInterface {

        @JavascriptInterface
        public void confirmSMS(){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_APP_MESSAGING);
            startActivity(intent);
        }

    }
}
