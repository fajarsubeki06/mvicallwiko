package com.sbimv.mvicalls.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.adapter.PopUpVietSubsAdapter;
import com.sbimv.mvicalls.billings.SubscriptionGoogleManager;
import com.sbimv.mvicalls.databinding.ActivityPopupVietSubsBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataChargingContent;
import com.sbimv.mvicalls.pojo.GpayData;
import com.sbimv.mvicalls.util.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PopupVietSubsActivity extends AppCompatActivity implements PopUpVietSubsAdapter.DoWeboptin{

    private ContactItem own;
    private RecyclerView rvRadio;
    public static int RESULT_CODE = 101;
    public static String STR_EXTRA_VALUE = "popup_dismiss";
    private boolean isData;
    private ProgressDialog progressDialog;
    private String urlCharging;
    private int exit = 0;
    private boolean isOnline = true;
    private String price;
    private LangViewModel model;

    SubscriptionGoogleManager subscriptionGoogleManager;
    private String mStrSKU;

    public BroadcastReceiver receiverNetwork = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {

            String ac = intent.getAction();

            if ("turn_off_wifi".equals(ac)) {
                price = intent.getStringExtra("price");
                isData = intent.getBooleanExtra("isData", false);
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                if (wifiManager != null) {
                    wifiManager.setWifiEnabled(false);
                }
            }

            else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(ac)) {
                if (isData) {
//                    progressDialog.show();
                    final Thread thread = new Thread(){
                        @Override
                        public void run() {
                            try {
                                while (!isConnected(context)){
                                    int wait = 1000;
                                    Thread.sleep(wait);
                                    exit += wait;
                                    if (exit == 10000){
                                        ((Activity)context).runOnUiThread(new Runnable() {
                                            public void run() {
                                                if (!PopupVietSubsActivity.this.isFinishing()) {
//                                                    progressDialog.dismiss();
                                                }
                                                Toast.makeText(context, model.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        isOnline = false;
                                        break;
                                    }
                                }

                                if (isOnline){
                                    ((Activity)context).runOnUiThread(new Runnable() {
                                        public void run() {
                                            getwebCharging(price);
                                            isData = false;
                                        }
                                    });
                                }

                            }catch (Exception e){
                                e.printStackTrace();
                                if (!PopupVietSubsActivity.this.isFinishing()) {
//                                    progressDialog.dismiss();
                                }
                            }
                        }
                    };
                    thread.start();
                }
                isData = false;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityPopupVietSubsBinding binding = DataBindingUtil.setContentView(PopupVietSubsActivity.this, R.layout.activity_popup_viet_subs);
        model = LangViewModel.getInstance();
        binding.setLangModel(model);

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            String scase = bundle.getString("scase");
        }

        rvRadio = findViewById(R.id.rvRadioSubs);
        own = SessionManager.getProfile(this);

        initRecycler();

        Intent intent = new Intent();
        intent.putExtra(STR_EXTRA_VALUE,100);
        setResult(RESULT_CODE,intent);


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("turn_off_wifi");
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(receiverNetwork,intentFilter);

//        progressDialog = new ProgressDialog(this);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setMessage("Processing...");
//        progressDialog.setIndeterminate(true);

    }

    @SuppressLint("WrongConstant")
    private void initRecycler() {
        LinearLayoutManager layout = new LinearLayoutManager(this);
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        rvRadio.setLayoutManager(layout);
        // SubsData saved in MainActivity
        ArrayList<DataChargingContent> getSubData = SessionManager.getChargeArrayList(getApplicationContext());
        if (getSubData != null && getSubData.size()>0){
            PopUpVietSubsAdapter ar = new PopUpVietSubsAdapter(getSubData,PopupVietSubsActivity.this);
            rvRadio.setAdapter(ar);

            String channel = getSubData.get(0).getChannel(); // check methode payment g-pay...
            if (!TextUtils.isEmpty(channel) && channel.equalsIgnoreCase("GOOGLE")){
                String keyword = getSubData.get(0).getKeyword();
                if(!TextUtils.isEmpty(keyword)) {
                    mStrSKU = keyword;
                    subscriptionGoogleManager = new SubscriptionGoogleManager(PopupVietSubsActivity.this, subsListener);
                }
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (own != null) {
            try {
                GATRacker.getInstance(getApplication())
                        .sendEventWithScreen(TrackerConstant.SCREEN_SUBSCRIPTION_DIALOG,
                                TrackerConstant.EVENT_CAT_SUBSCRIPTION_DIALOG, "dismiss dialog subscription", own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*LocalBroadcastManager.getInstance(PopupVietSubsActivity.this).*/
        unregisterReceiver(receiverNetwork);

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(false);
    }

    protected static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.getState() == NetworkInfo.State.CONNECTED;
    }

    @Override
    public void getwebCharging(String price) {
        if (!PopupVietSubsActivity.this.isFinishing()) {
//            progressDialog.show();
            Call<APIResponse<String>> call = ServicesFactory.getService().getUrlChargingTsel(own.msisdn, null, price);
            call.enqueue(new Callback<APIResponse<String>>() {
                @Override
                public void onResponse(@NotNull Call<APIResponse<String>> call, @NotNull Response<APIResponse<String>> response) {
                    if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                        if (!PopupVietSubsActivity.this.isFinishing()) {
//                            progressDialog.dismiss();
                            urlCharging = response.body().data;
                            if (!TextUtils.isEmpty(urlCharging)) {
                                ArrayList<DataChargingContent> getSubData = SessionManager.getChargeArrayList(getApplicationContext());
                                if (getSubData != null && getSubData.size()>0) {
                                    String sChannel = getSubData.get(0).getChannel(); // check methode payment g-pay...
                                    if (!TextUtils.isEmpty(sChannel) && sChannel.equalsIgnoreCase("weboptin_otp")){
                                        startActivity(new Intent(PopupVietSubsActivity.this, TselActivity.class).putExtra("sUrl", urlCharging));
                                    }else {
                                        if (!PopupVietSubsActivity.this.isFinishing()) {
                                            WebView webView = new WebView(PopupVietSubsActivity.this);
                                            webView.setWebViewClient(new PopupVietSubsActivity.TselWebViewClient());
                                            webView.loadUrl(urlCharging);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull Call<APIResponse<String>> call, @NotNull Throwable t) {
                    if (!PopupVietSubsActivity.this.isFinishing()) {
//                        progressDialog.dismiss();
                        String wdErr = TextUtils.isEmpty(model.getLang().string.errorFetchingData)?"":model.getLang().string.errorFetchingData;
                        Toast.makeText(PopupVietSubsActivity.this, wdErr, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    @Override
    public void getgoogleCharging() {
        subscriptionGoogleManager.performSubscribe();
    }

    @Override
    public void getRequestDcb(String price, String priceLbl) {
        if (!PopupVietSubsActivity.this.isFinishing()) {
            ContactItem owns = SessionManager.getProfile(PopupVietSubsActivity.this);
            if (owns != null) {

                String sMsisdn = TextUtils.isEmpty(owns.msisdn)?"":owns.msisdn;
                String sType = "sub";

                if (!TextUtils.isEmpty(price)) {
                    startActivity(new Intent(PopupVietSubsActivity.this, TransactionOtpActivity.class)
                            .putExtra("sMisdn", sMsisdn)
                            .putExtra("sContenId", "")
                            .putExtra("sPrice", price)
                            .putExtra("sPriceLabel", priceLbl)
                            .putExtra("sType", sType)
                    );
                }

            }
        }
    }

// ============================================== Function Google Payment ===============================================
// ======================================================================================================================

    private SubscriptionGoogleManager.SubscriptionGoogleListener subsListener = new SubscriptionGoogleManager.SubscriptionGoogleListener() {

        @Override
        protected void onBillingConnected(boolean success) {
            if (success){
                if (!TextUtils.isEmpty(mStrSKU)) {
                    subscriptionGoogleManager.loadSKU(mStrSKU);
                }
            }else {
                Toast.makeText(PopupVietSubsActivity.this, "Data on failed", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onProductLoaded() {
            super.onProductLoaded();
        }

        @Override
        protected void onSubscribe(String json, String price, String period) {
            ContactItem own = SessionManager.getProfile(PopupVietSubsActivity.this);
            if (own != null && !TextUtils.isEmpty(json) && !TextUtils.isEmpty(price) && !TextUtils.isEmpty(period)) {
                subscriptionGoogleManager.updateSubsStatus(own.msisdn, "sub", json, false, period, price);
                GpayData gpayData = new GpayData(own.msisdn, "sub", json, false, period, price);
                SessionManager.saveDataGpay(PopupVietSubsActivity.this, gpayData); // Save data g-pay....
            }
        }

        @Override
        protected void onUpdateSubsStatus(boolean success, String json, String price, String period) {
            if (!success){
                Toast.makeText(PopupVietSubsActivity.this, "Error connection server", Toast.LENGTH_SHORT).show();
            }
            finish();
        }

    };

// ======================================================================================================================
// ======================================================================================================================

    private class TselWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String urlWeb) {
            if (!PopupVietSubsActivity.this.isFinishing()) {
//                progressDialog.dismiss();
                urlCharging = urlWeb;
                super.onPageFinished(view, urlCharging);
                finish();
                //after dissmiss, popup USSD TSEL should shown
            }
        }
    }
}
