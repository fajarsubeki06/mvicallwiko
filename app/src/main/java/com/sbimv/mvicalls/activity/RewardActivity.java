package com.sbimv.mvicalls.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.snackbar.Snackbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.adapter.RewardAdapter;
import com.sbimv.mvicalls.databinding.ActivityRewardBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataRewards;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;



import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RewardActivity extends BaseActivity {

    private TextView tvName;
    private DataRewards dataRewards;
    private RecyclerView rvReward;
    private ArrayList<DataRewards.ListRewards> listRewards = new ArrayList<>();
    private String userPoint;
    private LinearLayout linEmpty;
    private boolean rewardHasShown;
    private ProgressBar pgProgressBar;

    @SuppressLint("CheckResult")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityRewardBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_reward);
        binding.setLangModel(model);
        setDefaultToolbar(true, model.getLang().string.pointReward);

        TextView tvEmptyState = findViewById(R.id.tvEmptyReward);
        tvEmptyState.setText(model.getLang().string.wordingEmptyReward);
        pgProgressBar = findViewById(R.id.pgPreviewReward);
        ImageView imgUser = findViewById(R.id.imgProfileReward);
        tvName = findViewById(R.id.tvPNameReward);
        rvReward = findViewById(R.id.rvRewardList);
        linEmpty = findViewById(R.id.linEmpty_reward);
        Button btnHowGetPoints = findViewById(R.id.btnHowGetPoints);

        ContactItem own = SessionManager.getProfile(this);
        if (own != null) {
            tvName.setText(own.name);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.userpng);
            Glide.with(this).load(own.caller_pic).apply(requestOptions).into(imgUser);

            initRecycler();
            btnHowGetPoints.setOnClickListener(v -> {
                Intent intent = new Intent(RewardActivity.this, PopUpHowToGetPoint.class);
                startActivity(intent);
            });
        }
        logFcb_screen_pointrewardEvent();
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logFcb_screen_pointrewardEvent () {
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_pointreward");
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUsrPoint();
    }

    @SuppressLint("WrongConstant")
    private void initRecycler() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        rvReward.setLayoutManager(manager);
        RewardAdapter adapter = new RewardAdapter(userPoint, listRewards, this);
        rvReward.setAdapter(adapter);
    }

    private void getUsrPoint() {
        ContactItem own = SessionManager.getProfile(this);
        if (own != null) {
            String lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
            Call<APIResponse<DataRewards>> call = ServicesFactory.getService().getRewards(own.msisdn, lang);
            call.enqueue(new Callback<APIResponse<DataRewards>>() {
                @Override
                public void onResponse(@NotNull Call<APIResponse<DataRewards>> call, @NotNull Response<APIResponse<DataRewards>> response) {
                    if (response.body() != null && response.isSuccessful()) {
                        dataRewards = response.body().data;
                        if (dataRewards.getPoin() != null) {
                            userPoint = dataRewards.getPoin();
                            TextView tvPoin = findViewById(R.id.tvPTotalPoint);
                            tvPoin.setText(model.getLang().string.yourPoint.replace("[POINT]", userPoint));
                        }
                        if (dataRewards.getRewardList().size() > 0) {
                            listRewards.clear();
                            listRewards.addAll(dataRewards.getRewardList());
                            pgProgressBar.setVisibility(View.GONE);
                            rvReward.setVisibility(View.VISIBLE);
                        } else {
                            pgProgressBar.setVisibility(View.GONE);
                            linEmpty.setVisibility(View.VISIBLE);
                        }
                        rvReward.getAdapter().notifyDataSetChanged();
                        initRecycler();

                        // handle deep link reward
                        handleDeepLinkReward();
                    }
                }

                @Override
                public void onFailure(@NotNull Call<APIResponse<DataRewards>> call, @NotNull Throwable t) {
                    if (!isFinishing()) {
                        pgProgressBar.setVisibility(View.GONE);
                        Snackbar.make(tvName, model.getLang().string.wordingFailureResponse, Snackbar.LENGTH_SHORT);
                    }
                }
            });
        }
    }

    private void handleDeepLinkReward(){
        Uri deepLink = getIntent().getData();
        if(deepLink != null && listRewards.size() > 0){

            String reward_id = deepLink.getQueryParameter("id");
            if(!TextUtils.isEmpty(reward_id)) {
                for (DataRewards.ListRewards reward : listRewards) {
                    if (reward.getReward_id().equals(reward_id) && !rewardHasShown) {
                        Intent intent = new Intent(RewardActivity.this, PopupDetailRewardActivity.class);
                        intent.putExtra("dataReward", reward);
                        intent.putExtra("userPoint", userPoint);
                        startActivity(intent);
                        rewardHasShown = true;
                    }
                }
            }
        }
    }

}
