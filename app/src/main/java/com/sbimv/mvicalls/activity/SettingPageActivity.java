package com.sbimv.mvicalls.activity;

import android.content.DialogInterface;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRadioButton;

import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.LocaleHelper;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.databinding.ActivitySettingPageBinding;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.util.PreferenceUtil;

public class SettingPageActivity extends BaseActivity {
    private LinearLayout rlLanguage;
    private ActivitySettingPageBinding binding;
    private String cNameCode = "";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting_page);
        binding.setLangModel(model);
        setDefaultToolbar(true);
        rlLanguage = findViewById(R.id.rlLanguage);
        rlLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeLanguage();
            }
        });

    }

    private void changeLanguage() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        final View dialogView = this.getLayoutInflater().inflate(R.layout.choose_language_radio,null);
        final RadioGroup group = dialogView.findViewById(R.id.rgLanguage);
        AppCompatRadioButton rb_Country = dialogView.findViewById(R.id.rbCountry);
        AppCompatRadioButton rb_En = dialogView.findViewById(R.id.rbEnglish);

        String selected = TextUtils.isEmpty(LocaleHelper.getLanguage(SettingPageActivity.this))?"":
                LocaleHelper.getLanguage(SettingPageActivity.this);
        String ccode = PreferenceUtil.getPref(this).getString(PreferenceUtil.COUNTRY_CODE,"1");
        rb_Country.setText(PreferenceUtil.getPref(this).getString(PreferenceUtil.COUNTRY_NAME,""));
        rb_En.setText("English");


        if (selected.equalsIgnoreCase("EN")) {
            rb_En.setChecked(true);
        }else {
            rb_Country.setChecked(true);
        }

        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        dialogBuilder.setTitle(model.getLang().string.chooseLanguage);
        dialogBuilder.setPositiveButton(model.getLang().string.save, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String codeNoPlus;
                int selected = group.getCheckedRadioButtonId();
                if (selected == R.id.rbCountry){
                    cNameCode = PreferenceUtil.getPref(SettingPageActivity.this).getString(PreferenceUtil.COUNTRY_NAME_CODE,"EN");
                    codeNoPlus = ccode.replace("+","");

                    model.setLangFromRemote(cNameCode,codeNoPlus , new LangViewModel.LangRemoteListener() {
                        @Override
                        public void onUpdate() {
                            binding.invalidateAll();
                            getDataLang(cNameCode);
                        }
                    });
                }else if (selected == R.id.rbEnglish){
                    cNameCode = "EN";
                    model.setLangFromRemote(cNameCode, "1", new LangViewModel.LangRemoteListener() {
                        @Override
                        public void onUpdate() {
                            binding.invalidateAll();
                            getDataLang(cNameCode);
                        }
                    });
                }

//                cNameCode = TextUtils.isEmpty(cNameCode)?"":cNameCode;
//                LocaleHelper.setLocale(SettingPageActivity.this,cNameCode);
//                PreferenceUtil.getEditor(SettingPageActivity.this).putString(PreferenceUtil.SAVED_LANG_KEY, cNameCode).commit();
//                Utility.restartApp(SettingPageActivity.this);

            }
        });
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();

    }


    private void getDataLang(String cNameCode){
        if (!TextUtils.isEmpty(cNameCode)) {
            cNameCode = TextUtils.isEmpty(cNameCode)?"":cNameCode;
            LocaleHelper.setLocale(SettingPageActivity.this,cNameCode);
            PreferenceUtil.getEditor(SettingPageActivity.this).putString(PreferenceUtil.SAVED_LANG_KEY, cNameCode).commit();
            Utility.getInstance(getApplicationContext()).restartApp(SettingPageActivity.this);
        }
    }

}
