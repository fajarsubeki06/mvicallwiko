package com.sbimv.mvicalls.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.databinding.ActivityContactUsBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataAppSettings;
import com.sbimv.mvicalls.pojo.DataContactUs;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends BaseActivity {

    private TextView noCs;
    private TextView emailCs;
    private TextView tvfbmsg;
    private TextView tvInstagram;
    private TextView tvTagFbm;
    private TextView tvTagIg;
    private ProgressBar pgContactUs;
    private RelativeLayout rlBody;

    private String strPhone;
    private String strEmail;
    private String strFb;
    private String strIg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityContactUsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_us);
        binding.setLangModel(model);
        setDefaultToolbar(true);

        noCs = findViewById(R.id.no_cs);
        emailCs = findViewById(R.id.email_cs);
        tvfbmsg = findViewById(R.id.tvfbmsg);
        tvInstagram = findViewById(R.id.tvInstagram);
        tvTagFbm = findViewById(R.id.tvTagFbm);
        tvTagIg = findViewById(R.id.tvTagIg);
        pgContactUs = findViewById(R.id.pgContactUs);
        rlBody = findViewById(R.id.rlBody);

        String strTagFbm = TextUtils.isEmpty(model.getLang().string.tagFbm)
                ?"Facebook Messenger":model.getLang().string.tagFbm;

        String strTagIg = TextUtils.isEmpty(model.getLang().string.tagIg)
                ?"Instagram":model.getLang().string.tagIg;

        tvTagFbm.setText(strTagFbm);
        tvTagIg.setText(strTagIg);

        getDataContactUs();

    }

    private void getDataContactUs () {
//        ContactItem own = SessionManager.getProfile(ContactUsActivity.this);
//        if (own != null){

            String lang = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
//            String msisdn = own.msisdn;
            if (!TextUtils.isEmpty(lang)){

                Call<APIResponse<DataAppSettings>> call = ServicesFactory.getService().getAppSettings(lang);
                call.enqueue(new Callback<APIResponse<DataAppSettings>>() {
                    @Override
                    public void onResponse(@NotNull Call<APIResponse<DataAppSettings>> call, @NotNull Response<APIResponse<DataAppSettings>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            DataContactUs data = response.body().data.getContactUs();
                            if (data != null){

                                strPhone = data.getCall();
                                strEmail = data.getEmail();
                                strFb = data.getFb();
                                strIg = data.getIg();

                                if (!TextUtils.isEmpty(strPhone) && !TextUtils.isEmpty(strEmail) &&
                                        !TextUtils.isEmpty(strFb) && !TextUtils.isEmpty(strIg)){

                                    if (!ContactUsActivity.this.isFinishing()) {
                                        noCs.setText(strPhone);
                                        emailCs.setText(strEmail);
                                        tvfbmsg.setText(strFb);
                                        tvInstagram.setText(strIg);
                                        init();
                                    }

                                }

                            }
                        }
                        hideProgressBar();
                    }

                    @Override
                    public void onFailure(@NotNull Call<APIResponse<DataAppSettings>> call, @NotNull Throwable t) {
                        hideProgressBar();
                    }
                });

            }
//        }
    }

    private void hideProgressBar(){
        if (!ContactUsActivity.this.isFinishing()) {
            pgContactUs.setVisibility(View.GONE);
            rlBody.setVisibility(View.VISIBLE);
        }
    }

    private void init(){

        findViewById(R.id.rpTelp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(strPhone)) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + strPhone));
                    callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    if (ActivityCompat.checkSelfPermission(ContactUsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    startActivity(callIntent);
                }
            }
        });

        findViewById(R.id.rpEmail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(strEmail)) {
                    Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", strEmail, null));
                    startActivity(Intent.createChooser(intent, "Mvicall Support"));
                }
            }
        });

        findViewById(R.id.rpfbmsg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(strFb)) {
                    Uri uri = Uri.parse(strFb);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            }
        });

        findViewById(R.id.rpInstagram).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(strIg)) {
                    Uri uri = Uri.parse(strIg);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(ContactUsActivity.this).unregisterReceiver(popupNotifAppears);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(ContactUsActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

}
