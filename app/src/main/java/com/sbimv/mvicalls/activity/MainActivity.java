package com.sbimv.mvicalls.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustEvent;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.appevents.AppEventsLogger;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.inappmessaging.FirebaseInAppMessaging;
import com.sbimv.mvicalls.AppDelegate;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.BuildConfig;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.PreviewFrontVideo;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TimeLineNewActivity;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.apprtc.SinchService;
import com.sbimv.mvicalls.billings.SubscriptionGoogleManager;
import com.sbimv.mvicalls.databinding.ActivityMainBinding;
import com.sbimv.mvicalls.databinding.CaseViewConfirmationBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataChargingContent;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.pojo.DataDinamicVideos;
import com.sbimv.mvicalls.pojo.DataSubs;
import com.sbimv.mvicalls.pojo.DataViewContent;
import com.sbimv.mvicalls.pojo.GpayData;
import com.sbimv.mvicalls.pojo.ModelPopupBanner;
import com.sbimv.mvicalls.register.PhoneRegister;
import com.sbimv.mvicalls.services.AutostartService;
import com.sbimv.mvicalls.services.SyncConfigService;
import com.sbimv.mvicalls.services.YpService;
import com.sbimv.mvicalls.sync.SyncConfig;
import com.sbimv.mvicalls.sync.SyncUtils;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.MIUIUtils;
import com.sbimv.mvicalls.util.PermissionUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseListener;
import com.sbimv.mvicalls.util.ShowCaseWrapper;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;
import com.sinch.android.rtc.SinchError;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import me.toptas.fancyshowcase.FancyShowCaseView;
import me.toptas.fancyshowcase.FocusShape;
import me.toptas.fancyshowcase.OnViewInflateListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements SinchService.StartFailedListener {

    // request code permission
    private static final int SIMCARD_PERMISSION_CODE = 1;
    private static final String Job_Tag = "my_job_tag";
    // variables
    private static ContactItem own;
    private static boolean isfinishorDenied = false;
    private final String TAG = MainActivity.class.getSimpleName();
    boolean doubleBackToExitPressedOnce = false;
    private FancyShowCaseView fancyShowCaseViewFirst;
    private FancyShowCaseView fancyShowCaseViewSecond;
    private FancyShowCaseView fancyShowCaseViewThird;
    private FancyShowCaseView fancyShowCaseViewFour;
    private FancyShowCaseView fancyShowCaseViewTimeline;
    private TextView tvUserPhone;
    private TextView tvUserName;
    private TextView tvUserStatus;
    private LinearLayout btn_edit_profile;
    private LinearLayout btn_video_tone;
    private LinearLayout btn_friend;
    private int REQUEST_OVERLAY_PERMISSION = 1;
    private FirebaseJobDispatcher jobDispatcher;
    private FirebaseJobDispatcher mDispatcher;
    private CircleImageView imgProfile;
    private boolean protecttedApp = false;
    private boolean isParsing;
    private String wordingSCFinish;
    private ActivityMainBinding binding;
    private CaseViewConfirmationBinding caseBinding;
    /*
     * Adjust............
     * */
    private String ADJUST_EVENT_TOKEN_APP_LOGIN = "8pb9tw";
    private boolean hasFinalScase;
    private ImageView mBtntimeline;
    private SyncConfig config;
    private SubscriptionGoogleManager subscriptionGoogleManager;
    private String keyword;
    private boolean isShowing = false;
    private ImageButton mBtnReward;
    private ImageButton mBtnPoint;

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    public void onGetResponse(boolean isUpdateAvailable) {
//        Log.e("ResultAPPMAIN", String.valueOf(isUpdateAvailable));
//        if (isUpdateAvailable) {
//            checkUpdateApp(MainActivity.this);
//        }
//    }

//    public void showUpdateDialog() {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
//
//        alertDialogBuilder.setTitle(MainActivity.this.getString(R.string.app_name));
//        alertDialogBuilder.setMessage(MainActivity.this.getString(R.string.update_message));
//        alertDialogBuilder.setCancelable(false);
//        alertDialogBuilder.setPositiveButton(R.string.update_now, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                MainActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
//                dialog.cancel();
//            }
//        });
//        alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                if (isForceUpdate) {
//                    finish();
//                }
//                dialog.dismiss();
//            }
//        });
//        alertDialogBuilder.show();
//    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setLangModel(model);
//        caseBinding.setLangModel(model);
        config = SyncConfig.getInstance(this);
        setDefaultToolbar(false);
        setDefaultDrawable();

        tvUserPhone = findViewById(R.id.et_user_phone);
        tvUserName = findViewById(R.id.et_user_name);
        tvUserStatus = findViewById(R.id.tv_user_status);
        imgProfile = findViewById(R.id.iv_user);
        btn_edit_profile = findViewById(R.id.btn_edit_profile);
        btn_video_tone = findViewById(R.id.btn_video_tone);
        btn_friend = findViewById(R.id.tab_friend);
        LinearLayout btn_Howto = findViewById(R.id.btn_HowTo);
        mBtntimeline = findViewById(R.id.btnTimeLine);

        mBtnReward = findViewById(R.id.btn_rewards);
        mBtnPoint = findViewById(R.id.btn_best_deal);

        ProgressDialog pd = new ProgressDialog(this);
        pd.setCancelable(false);
        pd.setMessage("Processing ...");

        Glide.with(this)
                .load(getResources().getIdentifier("btn_timeline", "drawable", getPackageName()))
                .into(mBtntimeline);

        mBtntimeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, TimeLineNewActivity.class));
            }
        });

        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_HOME);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Utility.defaultPreference(MainActivity.this);
        BaseActivity.firstGetData = false;
        callAlarmTriggerService();
        callAutoStartService();
        SyncUtils.setAlarmManager(getApplicationContext());


        /**
         * Config Dynamic
         * Added by Yudha Pratama Putra, 12 Nov 2018
         */
        PreferenceUtil.getEditor(MainActivity.this).putBoolean(PreferenceUtil.LANG_MUST_UPDATE, true).commit();
        configDynamic(MainActivity.this);
        updateLang(MainActivity.this, new SyncConfig.UpdateLangListener() {
            @Override
            public void onResponse() {
                binding.invalidateAll();
            }
        });

        /**
         * Is Protect App For MUI
         * Added by Yudha Pratama Putra, 12 Sept 2018
         */
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (MIUIUtils.isMIUI() && Build.VERSION.SDK_INT >= 23 && !Settings.canDrawOverlays(getApplicationContext())) {
                    Log.i(TAG, "SDK_INT > 23: Screen Overlay Not allowed");
                    startActivityForResult(new Intent("android.settings.action.MANAGE_OVERLAY_PERMISSION",
                            Uri.parse("package:" + getPackageName())), REQUEST_OVERLAY_PERMISSION);
                } else {
                    Log.i(TAG, "SKK_INT < 19 or Have overlay permission");
                }
            }
        }, 3000);

        own = SessionManager.getProfile(MainActivity.this);

        /**
         * Firebase Push Notif
         * Added by Yudha Pratama Putra, 12 Sept 2018
         */
        if (own != null) {
            getDataTopic(own.msisdn);
        }

        /**
         * Firebase Send Token
         * Added by Yudha Pratama Putra, 12 Sept 2018
         */
        generateTokenFirebase(own);
        addUserDataInApplicationDir();

        /**
         * Firebase Jobs Dispatcher
         * Added by Yudha Pratama Putra, 12 Sept 2018
         */
        jobDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(MainActivity.this));
        FirebaseJobDispatcher mDispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(MainActivity.this));
        startJob();

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContactItem own = SessionManager.getProfile(MainActivity.this);
                if (own != null) {
                    Intent i = new Intent(MainActivity.this, ProfileActivity.class);
                    startActivity(i);
                    try {
                        GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_HOME, TrackerConstant.EVENT_CAT_HOME, "click profile picture", own.caller_id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        // read dynamic link
        readDynamicLink();
        readSharedDeepLink();

        // read deep link
        readDeepLink();

//        new GooglePlayStoreAppVersionNameLoader(getApplicationContext(), this).execute();

        /*
         * Adjust................
         * */
        AdjustEvent event = new AdjustEvent(ADJUST_EVENT_TOKEN_APP_LOGIN);
        event.setCallbackId("aj_app_login");
        event.addCallbackParameter("key", "value");
        event.addPartnerParameter("foo", "bar");
        Adjust.trackEvent(event);

        //FLAG to change wording push notif is user doesn't finish SCase
        PreferenceUtil.getEditor(MainActivity.this).putBoolean(PreferenceUtil.IS_REGISTERED, true).commit();

        /*
         * Firebase analitycs and in-app-messaging - Yudha Pratama Putra 28-02-2019
         * */
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this); // Firebase Analitycs
        FirebaseInAppMessaging mInAppMessaging = FirebaseInAppMessaging.getInstance();// Firebase in-app-messaging

        mInAppMessaging.setAutomaticDataCollectionEnabled(true);// Firebase in-app-messaging
        mInAppMessaging.setMessagesSuppressed(false);// Firebase in-app-messaging
        mFirebaseAnalytics.logEvent("mvi_home_screen", new Bundle()); // Firebase Analitycs

        /*
         * Facebook Screen Event
         * */
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_home");

    }

    @Override
    protected void onServiceConnected() {
        System.out.println("onServiceConnected");
        if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().setStartListener(this);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        super.onServiceDisconnected(componentName);
        System.out.println("onServiceDisconnected");
    }

    @Override
    public void onStartFailed(SinchError error) {
        System.out.println("onStartFailed");
    }

    @Override
    public void onStarted() {
        System.out.println("onStarted");
    }

//    private void cancelnotifAlarm() {
//        int currentReqCode = PreferenceUtil.getPref(MainActivity.this).getInt(PreferenceUtil.REQ_CODE, 0);
//        Intent notifItent = new Intent(MainActivity.this, DailyNotifReceiver.class);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, currentReqCode, notifItent, PendingIntent.FLAG_UPDATE_CURRENT);
//        pendingIntent.cancel();
//        AlarmManager alarmManager = (AlarmManager) MainActivity.this.getSystemService(Context.ALARM_SERVICE);
//        if (alarmManager != null) {
//            alarmManager.cancel(pendingIntent);
//        }
//        PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.FINISH_SCASE, true).commit();
//    }

    private void readSharedDeepLink() {

        String source = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.SOURCE_DEEPLINK, "");

        if (!TextUtils.isEmpty(source)) {
            String url = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.URL_DEEPLINK, "");
            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_HOME, "source", source, own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!TextUtils.isEmpty(url)) {

                PreferenceManager
                        .getDefaultSharedPreferences(MainActivity.this)
                        .edit()
                        .putString("deep_link", url)
                        .apply();

                sendCampaignData(url);
            }

        } else {

            String msisdn = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.MSISDN_DEEPLINK, "");
            String country = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
            String content = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.CONTENT_DEEPLINK, "");

            if (!TextUtils.isEmpty(msisdn) && !TextUtils.isEmpty(country) && !TextUtils.isEmpty(content)) {
                getDetailContent(msisdn, country, content);
            }

        }


    }

    private void readDynamicLink() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        if (pendingDynamicLinkData == null)
                            return;
                        Uri deepLink = pendingDynamicLinkData.getLink();

                        // handle install source
                        dynamicLinkInstall(deepLink);
                    }
                });
    }

// =======================================================================================================================================================================
// ============================================================================ Show Case ================================================================================
// =======================================================================================================================================================================

    private void dynamicLinkInstall(Uri deepLink) {
        if (deepLink != null) {

            if (deepLink.getBooleanQueryParameter("source", false)) {
                String source = deepLink.getQueryParameter("source");
                try {
                    GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_HOME, "source", source, own.caller_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (deepLink.getBooleanQueryParameter("_msisdn", false)
                    && deepLink.getBooleanQueryParameter("content_id", false)) {

                String msisdn = deepLink.getQueryParameter("_msisdn");
                String content = deepLink.getQueryParameter("content_id");

                if (!TextUtils.isEmpty(msisdn) && !TextUtils.isEmpty(content)) {

                    String country = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");

                    if (!TextUtils.isEmpty(msisdn) && !TextUtils.isEmpty(country) && !TextUtils.isEmpty(content)) {
                        getDetailContent(msisdn, country, content);
                        return;
                    }

                }

            }

            String url = deepLink.toString();
            PreferenceManager
                    .getDefaultSharedPreferences(MainActivity.this)
                    .edit()
                    .putString("deep_link", url)
                    .apply();

            sendCampaignData(url);
        } else { // make sure referral sent
            String deep_link = PreferenceManager.getDefaultSharedPreferences(MainActivity.this)
                    .getString("deep_link", null);
            if (!TextUtils.isEmpty(deep_link)) {
                sendCampaignData(deep_link);
            }
        }
    }

    public void showCaseIntroVietTwenty() {
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_TWENTY_VIETNAME, true).commit();
            LangViewModel model = LangViewModel.getInstance();

            String strWordingTitleVietTwentyTeen = TextUtils.isEmpty(model.getLang().string.caseTitleEditProfileHere)
                    ? "You can edit your profile here In this page you can Change your profile pic Change your name Change your status" :
                    model.getLang().string.caseTitleEditProfileHere;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(MainActivity.this);
            showCaseWrapper.showFancyVietnameTwenty(
                    ShowCaseWrapper.SC_VIET_TWENTYEEN,
                    strWordingTitleVietTwentyTeen,
                    btn_edit_profile,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            showCaseIntroVietTwentyOne();
                        }

                        @Override
                        public void onOk() {
                            showCaseIntroVietTwentyOne();
                        }

                        @Override
                        public void onSkip() {
                            showCaseIntroVietTwentyOne();
                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showCaseIntroVietTwentyOne() {
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_TWENTYONE_VIETNAME, true).commit();
            LangViewModel model = LangViewModel.getInstance();

            ContactItem own = SessionManager.getProfile(getApplicationContext());
            String strWordingTitleVietTwentyOne;
            if (own != null) {
                strWordingTitleVietTwentyOne = TextUtils.isEmpty(model.getLang().string.caseCongratulations)
                        ? "Congratulations " + "(" + own.getName() + ")" + " !" :
                        model.getLang().string.caseCongratulations;

            } else {
                strWordingTitleVietTwentyOne = TextUtils.isEmpty(model.getLang().string.caseCongratulations)
                        ? "Congratulations (name)!" :
                        model.getLang().string.caseCongratulations;

            }

            String strWordingSubtitleVietTwentyOne = TextUtils.isEmpty(model.getLang().string.caseSubtitleCongrat)
                    ? "I now declare you officially a MViCaller! You can start having FUN with your friends Go surprise them!" :
                    model.getLang().string.caseSubtitleCongrat;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(MainActivity.this);
            showCaseWrapper.showFancyVietnameTwentyOne(
                    ShowCaseWrapper.SC_VIET_TWENTTYONETEEN,
                    strWordingTitleVietTwentyOne,
                    strWordingSubtitleVietTwentyOne,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                           setFinalScase();
                        }

                        @Override
                        public void onOk() {
                            setFinalScase();
                        }

                        @Override
                        public void onSkip() {
                            setFinalScase();
                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPopupBanner(){
        ContactItem owns= SessionManager.getProfile(MainActivity.this);

        if (owns != null) {

            String msisdn = TextUtils.isEmpty(owns.msisdn)?"":owns.msisdn;
            String app_version = TextUtils.isEmpty(BuildConfig.VERSION_NAME) ? "" : BuildConfig.VERSION_NAME;
            app_version = app_version.replace(".", "");
            String lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");

            if ( !TextUtils.isEmpty(msisdn) && !TextUtils.isEmpty(app_version) && !TextUtils.isEmpty(lang) ){

                Call<APIResponse<ModelPopupBanner>> call = ServicesFactory.getService().popupBanner(msisdn, lang, "android", app_version);
                call.enqueue(new Callback<APIResponse<ModelPopupBanner>>() {
                    @Override
                    public void onResponse(@NotNull Call<APIResponse<ModelPopupBanner>> call, @NotNull Response<APIResponse<ModelPopupBanner>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            ModelPopupBanner data = response.body().data;
                            if (data != null) {
                                String url_banner = data.getBanner();
                                if (!TextUtils.isEmpty(url_banner)) {

                                    ConfigData cdt = SessionManager.getConfigData(getApplicationContext());
                                    if(cdt != null) {
                                        boolean mgmAvailable = cdt.isMgm_enable();
                                        if (mgmAvailable) {
                                            startActivity(new Intent(MainActivity.this, CustomDialogActivity.class).putExtra("url_banner", url_banner));
                                        }
                                    }

                                }
                            }
                        }else {
                            System.err.println("error");
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<APIResponse<ModelPopupBanner>> call, @NotNull Throwable t) {
                        System.err.println("failure");
                    }
                });

            }
        }

    }

    private void initDialogHome() {
        if(!MainActivity.this.isFinishing()) {
            getPopupBanner();
        }
    }

    private void readDeepLink() {
        Uri deepLink = getIntent().getData();

        if (deepLink != null) {
            if (deepLink.getPathSegments().size() > 0 && deepLink.getPathSegments().get(0).equalsIgnoreCase("rewards")) {
                Intent intent = new Intent(MainActivity.this, RewardActivity.class);
                intent.setData(deepLink);
                startActivity(intent);
            }else if (deepLink.getPathSegments().size() > 0 && deepLink.getPathSegments().get(0).equalsIgnoreCase("reward_mgm")){
                startActivity(new Intent(MainActivity.this, ContactTabActivity.class).putExtra(ContactTabActivity.INVITE_FRIENDS, 2).setData(deepLink));
            }else if (deepLink.getPathSegments().size() > 0 && deepLink.getPathSegments().get(0).equalsIgnoreCase("user_profile")){
                startActivity(new Intent(MainActivity.this, ProfileActivity.class).setData(deepLink));
            }else if (deepLink.getPathSegments().size() > 0 && deepLink.getPathSegments().get(0).equalsIgnoreCase("contact")){
                startActivity(new Intent(MainActivity.this, ContactTabActivity.class).setData(deepLink));
            }else if (deepLink.getPathSegments().size() > 0 && deepLink.getPathSegments().get(0).equalsIgnoreCase("set_video_tone")){
                startActivity(new Intent(MainActivity.this, SetVideoToneActivity.class).setData(deepLink));
            }else if (deepLink.getPathSegments().size() > 0 && deepLink.getPathSegments().get(0).equalsIgnoreCase("bestdeal")){
                startActivity(new Intent(MainActivity.this, BestDealActivity.class).setData(deepLink));
            }else if (deepLink.getPathSegments().size() > 0 && deepLink.getPathSegments().get(0).equalsIgnoreCase("explore")){
                startActivity(new Intent(MainActivity.this, TimeLineNewActivity.class).setData(deepLink));
            }else if (deepLink.getPathSegments().size() > 0 && deepLink.getPathSegments().get(0).equalsIgnoreCase("premium_content")){
                startActivity(new Intent(MainActivity.this, DinamicCatalogActivity.class).setData(deepLink));
            }
        }
    }

    private void getDetailContent(String msisdn, String lang, String content_id) {

        try {
            showCustomProgressDialog("Get data from server...");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Call<APIResponse<DataViewContent>> call = ServicesFactory.getService().getDetailContent(msisdn,lang,content_id);
        call.enqueue(new Callback<APIResponse<DataViewContent>>() {
            @Override
            public void onResponse(@org.jetbrains.annotations.NotNull Call<APIResponse<DataViewContent>> call,
                                   @org.jetbrains.annotations.NotNull Response<APIResponse<DataViewContent>> response) {

                if (response.body() != null && response.isSuccessful() && response.body().isSuccessful()) {
                    DataViewContent data = response.body().data;
                    if (data != null) {

                        DataDinamicVideos dataVideos = new DataDinamicVideos();
                        dataVideos.setContent_id(data.getContent_id());
                        dataVideos.setAlias(data.getAlias());
                        dataVideos.setSource_content(data.getSource_content());
                        dataVideos.setThumb_pic(data.getThumb_pic());
                        dataVideos.setPrice(String.valueOf(data.getPrice()));
                        dataVideos.setPrice_label(data.getPrice_label());
                        dataVideos.setJudul(data.getTitle());

                        ArrayList<DataDinamicVideos> videos = data.getContent();
                        if (videos.size()>0) {

                            DataDinamicTitle dataTitle = new DataDinamicTitle();
                            dataTitle.setCategory(data.getTitle());
                            dataTitle.setOthers(data.getOthers());
                            dataTitle.setContent(videos);

                            startActivity(new Intent(MainActivity.this, PreviewFrontVideo.class)
                                    .putExtra("video", dataVideos)
                                    .putExtra("title", dataTitle));

                            PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.MSISDN_DEEPLINK, "").commit();
                            PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.CONTENT_DEEPLINK, "").commit();

                        }

                    }
                }

                try {
                    dismissProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@org.jetbrains.annotations.NotNull Call<APIResponse<DataViewContent>> call,
                                  @org.jetbrains.annotations.NotNull Throwable t) {

                try {
                    dismissProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void regDatatoSinch(){
        ContactItem own = SessionManager.getProfile(this);
        if(getSinchServiceInterface() != null) {
            if (!getSinchServiceInterface().isStarted() && own != null) {
                String mCallerId = own.caller_id;
                if (!TextUtils.isEmpty(mCallerId)) {
                    getSinchServiceInterface().startClient(mCallerId);
                }
            }
        }
    }

    private void sendCampaignData(String url) {
        if (own == null) return;

        Call<APIResponse> call = ServicesFactory.getService().campaign(url, own.msisdn);
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse> call, @NotNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    // clear saved link

                    PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.SOURCE_DEEPLINK, "").commit();
                    PreferenceUtil.getEditor(getApplicationContext()).putString(PreferenceUtil.URL_DEEPLINK, "").commit();

                    PreferenceManager
                            .getDefaultSharedPreferences(MainActivity.this)
                            .edit()
                            .putString("deep_link", null)
                            .apply();
                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse> call, @NotNull Throwable t) {
            }
        });
    }

    private void checkShowCase() {

        boolean isFirstSC = AppPreference.getPreferenceBoolean(MainActivity.this, AppPreference.FIRST_SHOW_CASE);
        boolean isScTimeline = AppPreference.getPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_TIMELINE);
        boolean isScProfile = AppPreference.getPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_PROFILE);
        boolean isScVideo = AppPreference.getPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_VIDEOS);
        boolean isSCFriends = AppPreference.getPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_FRIENDS);

        if (isScTimeline) {
            showCaseTimeLine();
        }else if (isFirstSC){
            showCaseIntro();
        }else if (isScProfile){
            showCaseEditProfile();
        }else if (isScVideo){
            showCaseVideoTone();
        }else if (isSCFriends){
            showCaseFriends();
        }
    }

    private void showCaseTimeLine() {
        if (!MainActivity.this.isFinishing()) {
            if (fancyShowCaseViewTimeline == null) {
                fancyShowCaseViewTimeline = new FancyShowCaseView.Builder(MainActivity.this)
                        .focusOn(mBtntimeline)
                        .focusCircleRadiusFactor(1.5)
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .roundRectRadius(90)
                        .enableTouchOnFocusedView(true)
                        .closeOnTouch(false)
                        .focusBorderSize(10)
                        .focusBorderColor(getResources().getColor(R.color.orange))
                        .customView(R.layout.case_view_timeline, new OnViewInflateListener() {
                            @Override
                            public void onViewInflated(@NonNull View view) {

                                TextView message = view.findViewById(R.id.textView);
                                Button btnNo = view.findViewById(R.id.btnNo);
                                Button btnExplore = view.findViewById(R.id.btnExplore);

                                message.setText(model.getLang().string.wordingTimeline);
                                btnExplore.setText(model.getLang().string.explore);
                                btnNo.setText(model.getLang().string.no);

                                view.setOnClickListener(viewLayout -> sCaseAction());
                                view.findViewById(R.id.btnExplore).setOnClickListener(viewButton -> sCaseAction());
                            }
                        }).build();
            }

            if (fancyShowCaseViewTimeline.isShown()) {
                return;
            }
            fancyShowCaseViewTimeline.show();
        }
    }

    private void sCaseAction() {
        fancyShowCaseViewTimeline.removeView();
        startActivity(new Intent(MainActivity.this, TimeLineNewActivity.class));
        AppPreference.setPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_TIMELINE, false);
    }

    private void showCaseEditProfile() {
        if (!MainActivity.this.isFinishing()) {
            if (fancyShowCaseViewSecond == null) {
                fancyShowCaseViewSecond = new FancyShowCaseView.Builder(MainActivity.this)
                        .focusOn(btn_edit_profile).focusCircleRadiusFactor(1.5)
                        .focusShape(FocusShape.ROUNDED_RECTANGLE).roundRectRadius(90)
                        .enableTouchOnFocusedView(true).closeOnTouch(false)
                        .focusBorderSize(10)
                        .focusBorderColor(getResources().getColor(R.color.orange))

                        .customView(R.layout.case_view_editprofile, new OnViewInflateListener() {
                            @Override
                            public void onViewInflated(@NonNull View view) {
                                TextView title = view.findViewById(R.id.judul);
                                TextView message = view.findViewById(R.id.textView);
                                title.setText(model.getLang().string.editProfile);
                                message.setText(model.getLang().string.wordingShowcaseProfile);

                                view.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        fancyShowCaseViewSecond.removeView();
                                        showCaseVideoTone();
                                        AppPreference.setPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_PROFILE, false);
                                    }
                                });
                            }
                        }).build();
            }

            if (fancyShowCaseViewSecond.isShown()) {
                return;
            }
            fancyShowCaseViewSecond.show();
        }
    }

    private void showCaseVideoTone() {
        if (!MainActivity.this.isFinishing()) {
            if (fancyShowCaseViewThird == null) {
                fancyShowCaseViewThird = new FancyShowCaseView.Builder(MainActivity.this)
                        .focusOn(btn_video_tone)
                        .focusCircleRadiusFactor(1.5)
                        .enableTouchOnFocusedView(true)
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .roundRectRadius(90)
                        .closeOnTouch(false).focusBorderSize(10)
                        .focusBorderColor(getResources().getColor(R.color.orange))
                        .customView(R.layout.case_view_videotone, new OnViewInflateListener() {
                            @Override
                            public void onViewInflated(@NonNull View view) {
                                TextView title = view.findViewById(R.id.judul);
                                TextView desc = view.findViewById(R.id.textView);
                                title.setText(model.getLang().string.videoTone);
                                desc.setText(model.getLang().string.wordingShowcaseEditvideotone);

                                view.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        fancyShowCaseViewThird.removeView();
                                        showCaseFriends();
                                        AppPreference.setPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_VIDEOS, false);
                                    }
                                });
                            }
                        }).build();
            }
            if (fancyShowCaseViewThird.isShown()) {
                return;
            }
            fancyShowCaseViewThird.show();
        }
    }

    private void showCaseFriends() {
        if (!MainActivity.this.isFinishing()) {
            if (fancyShowCaseViewFour == null) {
                fancyShowCaseViewFour = new FancyShowCaseView.Builder(MainActivity.this)
                        .focusOn(btn_friend)
                        .focusCircleRadiusFactor(1.5)
                        .enableTouchOnFocusedView(true)
                        .focusShape(FocusShape.ROUNDED_RECTANGLE)
                        .roundRectRadius(90).closeOnTouch(false)
                        .focusBorderSize(10)
                        .focusBorderColor(getResources().getColor(R.color.orange))
                        .customView(R.layout.case_view_friend, (View view) ->
                        {
                            TextView title = view.findViewById(R.id.judul);
                            TextView message = view.findViewById(R.id.textView);
                            title.setText(model.getLang().string.friend);
                            message.setText(model.getLang().string.wordingShowcaseFriends);
                            view.setOnClickListener(view12 ->
                                    view12.setOnClickListener(view1 -> {
                                        fancyShowCaseViewFour.removeView();
                                        AppPreference.setPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_FRIENDS, false);
                                        hasFinalScase = PreferenceUtil.getEditor(this).putBoolean(PreferenceUtil.FINAL_SCASE, true).commit();
                                        setFinalScase();
                                        isfinishorDenied = true;
//                                        cancelnotifAlarm();
                                        finalScase();
                                    }));
                        }).build();
            }
            if (fancyShowCaseViewFour.isShown()) {
                return;
            }
            fancyShowCaseViewFour.show();
        }
    }

    private void finalScase() {
        ConfigData cdt = SessionManager.getConfigData(MainActivity.this);
        if (cdt != null) {
            if (!MainActivity.this.isFinishing()) {
                String curLang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
                String wording = curLang.equalsIgnoreCase("ID") ? cdt.wording_free_id : cdt.wording_free_en;
                String wBtnClose = TextUtils.isEmpty(model.getLang().string.close)?"":model.getLang().string.close;
                String wMsgBody = TextUtils.isEmpty(model.getLang().string.wFinalShowCase)?"":model.getLang().string.wFinalShowCase;

                if (!TextUtils.isEmpty(wMsgBody) && !TextUtils.isEmpty(wBtnClose)) {

                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setCancelable(false);
                    builder.setMessage(wMsgBody);
                    builder.setNegativeButton(wBtnClose, null);
                    AlertDialog dialog = builder.create();
                    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(final DialogInterface dialog) {
                            //Button Negative
                            Button btnNegative = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE);
                            btnNegative.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                    initDialogHome();
                                    isShowing = true;
                                }
                            });
                        }
                    });
                    dialog.show();
                }else {
                    initDialogHome();
                    isShowing = true;
                }
            }
        } else {
            Intent i = new Intent(MainActivity.this, SyncConfigService.class);
            startService(i);
        }
    }

    private void showCaseIntro() {

        if (!MainActivity.this.isFinishing()) {
            if (fancyShowCaseViewFirst == null) {
                fancyShowCaseViewFirst = new FancyShowCaseView.Builder(MainActivity.this)
                        .closeOnTouch(false)
                        .customView(R.layout.case_view_confirmation, new OnViewInflateListener() {
                            @Override
                            public void onViewInflated(@NonNull View view) {

                                TextView tv = view.findViewById(R.id.textView);
                                TextView title = view.findViewById(R.id.judul);
                                Button btn = view.findViewById(R.id.btnOK);
                                tv.setText(model.getLang().string.wordingScConfirmation);
                                title.setText(model.getLang().string.holla);
                                btn.setText(model.getLang().string.startNow);

                                view.findViewById(R.id.btnNo).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        fancyShowCaseViewFirst.removeView();
                                        isfinishorDenied = true;
                                    }
                                });
                                view.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        fancyShowCaseViewFirst.removeView();
                                        AppPreference.setPreferenceBoolean(MainActivity.this, AppPreference.FIRST_SHOW_CASE, false);
                                        showCaseEditProfile();
                                    }
                                });
                            }
                        }).build();

            }

            if (fancyShowCaseViewFirst.isShown()) {
                return;
            }
            fancyShowCaseViewFirst.show();

        }
    }

    private void showCaseFinish() {
        final ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(MainActivity.this);
        showCaseWrapper.showFancyFinish(ShowCaseWrapper.ID_ALL_DONE, model.getLang().string.wordingFinishSc, new ShowCaseListener.InviteListener() {
            @Override
            public void onFinish() {
//                AppPreference.setPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_FINISH, false);
//                finalScase();
            }

            @Override
            public void onInvite() {
                AppPreference.setPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_FINISH, false);
                Intent intent = new Intent(MainActivity.this, ContactTabActivity.class);
                intent.putExtra(ContactTabActivity.INVITE_FRIENDS, 1);
                startActivity(intent);
            }
        });
    }

    private void checkWording() {
        ConfigData cdt = SessionManager.getConfigData(MainActivity.this);
        if (cdt != null) {
            String scIN = cdt.wording_showcase_id;
            String scEN = cdt.wording_showcase_en;

            boolean isPointReward = cdt.point_reward;
            boolean isBeastDeal = cdt.best_deal;

            if (isPointReward) {
                mBtnPoint.setVisibility(View.GONE);
            }

            if (isBeastDeal) {
                mBtnReward.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(scIN) && !TextUtils.isEmpty(scEN)) {
                if (curLang().equalsIgnoreCase("ID")) {
                    wordingSCFinish = scIN;
                } else if (curLang().equalsIgnoreCase("en")) {
                    wordingSCFinish = scEN;
                }
            }


        } else {
            Intent i = new Intent(MainActivity.this, SyncConfigService.class);
            startService(i);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        /**
         * Check get Subs price..
         * Added by Yudha Pratama Putra, 31 Okt 2018
         */
        if (!isParsing) {
            getSubsPrice();
        }
        /**
         * Check permission..
         * Added by Yudha Pratama Putra, 31 Okt 2018
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissionAllow();
        }

        /**
         * Sync Data Config Service...
         * Added by Yudha Pratama Putra, 12 Nov 2018
         */
        Intent i = new Intent(MainActivity.this, SyncConfigService.class);
        startService(i);
        checkWording();
        checkPhoneDefault();
        trigerEventTracker();

        Intent data = getIntent();
        if (data != null) {
            String key = data.getStringExtra("Share");
            if (key != null) {
                boolean isDone = PreferenceUtil.getPref(this).getBoolean(PreferenceUtil.CASE_DIALOG,false);
                if (isDone){
                    AppPreference.setPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_FRIENDS, false);
                    showPopup();
                }
            } else {
                boolean isFriend = AppPreference.getPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_FRIENDS);
                if (!isFriend) {
                    if (!isShowing) {
                        initDialogHome();
                        isShowing = true;
                    }
                }
            }
        }

//        Intent data = getIntent();
//        String getData = data.getStringExtra("TIMELINE");
//        if (!TextUtils.isEmpty(getData)){
//            String ccodeViet = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
//            if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
//                checkShowCaseViet();
//            }
//        }

//        String ccodeViet = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
//        if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
//            showCaseIntroVietTwenty();
//        }

    }

    private void showPopup(){

        setFinalScase();

        String strWordingTitleCongrats = TextUtils.isEmpty(model.getLang().string.titleCongrats)
                ?"Selamat!" :
                model.getLang().string.titleCongrats;

        String strWordingSubTitleCongrats = TextUtils.isEmpty(model.getLang().string.subtileCongrat)
                ?"Kamu kini bisa munculin Video Pilihannmu, Langsung di Hape Temanmu saat kamu telfon dia!" :
                model.getLang().string.subtileCongrat;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View v = getLayoutInflater().inflate(R.layout.dialog_congratulations, null);
        builder.setView(v);

        TextView titleCongrats = v.findViewById(R.id.textTitleCongrats);
        TextView subtitleCongrats = v.findViewById(R.id.textSubtitleCongrats);

        titleCongrats.setText(strWordingTitleCongrats);
        subtitleCongrats.setText(strWordingSubTitleCongrats);

        final AlertDialog alert = builder.create();
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alert.setCanceledOnTouchOutside(true);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFinalScase();
                PreferenceUtil.getEditor(Objects.requireNonNull(getApplicationContext())).putBoolean(PreferenceUtil.CASE_DIALOG, false).commit();
                boolean isFriend = AppPreference.getPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_FRIENDS);
                if (!isFriend) {
                    if (!isShowing) {
                        initDialogHome();
                        isShowing = true;
                    }
                }
                alert.dismiss();
            }
        });

        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                setFinalScase();
                PreferenceUtil.getEditor(Objects.requireNonNull(getApplicationContext())).putBoolean(PreferenceUtil.CASE_DIALOG, false).commit();
                boolean isFriend = AppPreference.getPreferenceBoolean(MainActivity.this, AppPreference.SHOW_CASE_FRIENDS);
                if (!isFriend) {
                    if (!isShowing) {
                        initDialogHome();
                        isShowing = true;
                    }
                }
            }
        });

        if (!alert.isShowing()) {
            alert.show();
        } else {
            alert.dismiss();
        }
        PreferenceUtil.getEditor(Objects.requireNonNull(getApplicationContext())).putBoolean(PreferenceUtil.CASE_DIALOG, false).commit();
    }

    private void trigerEventTracker() {
        ContactItem own = SessionManager.getProfile(MainActivity.this);
        if (own != null) {
            getSubscriptionStatus(MainActivity.this, own.msisdn, new SubscriptionStatusListener() {
                @Override
                public void onComplete(DataSubs subsData) {
                    Log.i("trigerEventTracker", "Success");
                }

                @Override
                public void onFail() {
                    Log.i("trigerEventTracker", "Failed");
                }
            });
        }
    }

    private void checkPhoneDefault() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            Intent intent = new Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER);
            intent.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, getPackageName());
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }

    public void showCaseIntroVietEightTeen() {
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_EIGHTTEEEN_VIETNAME, true).commit();
            LangViewModel model = LangViewModel.getInstance();

            String strWordingTitleVietSevenTeen = TextUtils.isEmpty(model.getLang().string.caseTitleAlmostForgot)
                    ?"Oh, I almost forgot! I have 1 last cool feature for you" :
                    model.getLang().string.caseTitleAlmostForgot;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(MainActivity.this);
            showCaseWrapper.showFancyVietnameEightTeen(
                    ShowCaseWrapper.SC_VIET_EIGHTTEEN,
                    strWordingTitleVietSevenTeen,
                    mBtntimeline,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            stractivity();
                        }

                        @Override
                        public void onOk() {
                            stractivity();
                        }

                        @Override
                        public void onSkip() {
                            stractivity();
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void stractivity(){
        //PreferenceUtil.getEditor(Objects.requireNonNull(MainActivity.this)).putBoolean(PreferenceUtil.SETUP_SHARE, true).commit();
        startActivity(new Intent(MainActivity.this, TimeLineNewActivity.class).putExtra("KEY_EXPLORE", "true"));
    }

//    //New Broadcast
//    public BroadcastReceiver popupNotifAppears = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if (context != null && intent != null){
//
//                String strFrom = intent.getStringExtra("from");
//                if (!TextUtils.isEmpty(strFrom)){
//
//                    if (strFrom.equals("notif_logout")){
//                        processAutoLogout();
//                    }
//
//                }else {
//
//                    String ccodeViet = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
//                    if (!TextUtils.isEmpty(ccodeViet)) {
//                        if (Objects.requireNonNull(ccodeViet).equalsIgnoreCase("+84")) {
//
//                            String title = TextUtils.isEmpty(intent.getStringExtra("title"))?"":intent.getStringExtra("title");
//                            String message = TextUtils.isEmpty(intent.getStringExtra("message"))?"":intent.getStringExtra("message");
//                            String direction = TextUtils.isEmpty(intent.getStringExtra("direction"))?"":intent.getStringExtra("direction");
//
//                            try {
//                                dialogUpSuccessBuy(title, message, direction);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//
//                        }
//                    }
//
//                }
//
//
//            }
//        }
//    };

    @Override
    protected void onResume() {
        super.onResume();
        if (!isfinishorDenied) {
            String ccodeViet = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
            assert ccodeViet != null;
            if (!TextUtils.isEmpty(ccodeViet) && !ccodeViet.equalsIgnoreCase("+84")) {
//                checkShowCase();
            }
        }

        Intent data = getIntent();
        String getData = data.getStringExtra("TIMELINE");
        String getData2 = data.getStringExtra("TOMAIN");
        String ccodeViet2 = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
        boolean caseVietnamNineTeen = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_NINETEEEN_VIETNAME, false);
        assert ccodeViet2 != null;
        if (!TextUtils.isEmpty(ccodeViet2) && ccodeViet2.equalsIgnoreCase("+84")){
            if (!TextUtils.isEmpty(getData)){
                if (caseVietnamNineTeen){
                    showCaseIntroVietTwenty();
                }
            }else if (!TextUtils.isEmpty(getData2)){
                showCaseIntroVietEightTeen();
            }
        }

        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));


        /**
         * Remote Config Update Version App...
         * Added by Yudha Pratama Putra, 12 Nov 2018
         */
        checkNetwork(tvUserPhone);
        setUserInfo();
        readSerialSim();
        showBroadcastConfig(MainActivity.this);
        counterDataInbox();

        String directPage = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceUtil.DIRECT_TO_PAGE, ""); //validate direction
        String directionApp = PreferenceUtil.getPref(MainActivity.this).getString(PreferenceUtil.DIRECTION, ""); //validate direction
        if (!TextUtils.isEmpty(directionApp)) {

            if (directionApp.equalsIgnoreCase("contact_tab_activity")) {
                Intent i = new Intent(MainActivity.this, ContactTabActivity.class);
                startActivity(i);
                PreferenceUtil.getEditor(MainActivity.this).putString(PreferenceUtil.DIRECTION, "").commit();
            } else if (directionApp.equalsIgnoreCase("purchase")) {
                Intent intent = new Intent(this, UploadVideoToneActivity.class);
                startActivity(intent);
                PreferenceUtil.getEditor(MainActivity.this).putString(PreferenceUtil.DIRECTION, "").commit();
            }
        } else if (!TextUtils.isEmpty(directPage)) {
            if (directPage.contains("bestdeal")) {
                Intent i = new Intent(MainActivity.this, BestDealActivity.class);
                startActivity(i);
                PreferenceUtil.getEditor(MainActivity.this).putString(PreferenceUtil.DIRECT_TO_PAGE, "").commit();
            } else if (directPage.equalsIgnoreCase("reward")) {
                startActivity(new Intent(MainActivity.this, RewardActivity.class));
                PreferenceUtil.getEditor(MainActivity.this).putString(PreferenceUtil.DIRECT_TO_PAGE, "").commit();
            } else if (directPage.equalsIgnoreCase("explore")) { // Direct to explore page......
                startActivity(new Intent(MainActivity.this, TimeLineNewActivity.class));
                PreferenceUtil.getEditor(MainActivity.this).putString(PreferenceUtil.DIRECT_TO_PAGE, "").commit();
            }else if (directPage.equalsIgnoreCase("mgm")) { // Direct to mgm page......
                startActivity(new Intent(MainActivity.this, ContactTabActivity.class).putExtra(ContactTabActivity.INVITE_FRIENDS, 2));
                PreferenceUtil.getEditor(MainActivity.this).putString(PreferenceUtil.DIRECT_TO_PAGE, "").commit();
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        stopBroadcastConfig(MainActivity.this);
        LocalBroadcastManager.getInstance(MainActivity.this).unregisterReceiver(popupNotifAppears);
    }


    /**
     * Service On Background
     * Added by Yudha Pratama Putra, 10 Mei 2018
     */
    private void callAutoStartService() {
        if (PermissionUtil.hashPermission(MainActivity.this, permissions)) {
            AutostartService mSensorService = new AutostartService(MainActivity.this);
            Intent mServiceIntent = new Intent(MainActivity.this, mSensorService.getClass());
            if (Build.VERSION.SDK_INT >= 26) {
                startForegroundService(new Intent(mServiceIntent));
            } else {
                startService(new Intent(mServiceIntent));
            }
        }
    }

    private void getSubsPrice() {
        ContactItem own = SessionManager.getProfile(MainActivity.this);
        if (own != null) {
            String strMsisdn = own.msisdn;
            if (!TextUtils.isEmpty(strMsisdn)) {
                String lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
                String multiLang = TextUtils.isEmpty(lang)?"":lang;
                Call<APIResponse<ArrayList<DataChargingContent>>> call = ServicesFactory.getService().getNewChargingContent(strMsisdn, "", "sub",multiLang);
                call.enqueue(new Callback<APIResponse<ArrayList<DataChargingContent>>>() {
                    @Override
                    public void onResponse(@NonNull Call<APIResponse<ArrayList<DataChargingContent>>> call, @NonNull Response<APIResponse<ArrayList<DataChargingContent>>> response) {
                        if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {

                            ArrayList<DataChargingContent> data = response.body().data;
                            if (data != null && data.size() > 0) {

                                SessionManager.saveChargeArrayList(MainActivity.this, data); // Save data payment method to local...
                                BaseActivity.firstGetData = true;
                                isParsing = true;

                                String channel = data.get(0).getChannel(); // check subscriber g-pay...
                                if (!TextUtils.isEmpty(channel) && channel.equalsIgnoreCase("GOOGLE")) {
                                    keyword = data.get(0).getKeyword();
                                    if (!TextUtils.isEmpty(keyword)) {
                                        subscriptionGoogleManager = new SubscriptionGoogleManager(MainActivity.this, subsListener); // check subscriber g-pay...
                                    }
                                }

                            }
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<APIResponse<ArrayList<DataChargingContent>>> call, @NotNull Throwable t) {
                        if (!isViewAttached) return;
                        isParsing = false;
                        toast(model.getLang().string.errorFetchingData);
                    }
                });
            }
        }
    }


// ============================================== Check google subscribe ================================================
// ======================================================================================================================
    private SubscriptionGoogleManager.SubscriptionGoogleListener subsListener = new SubscriptionGoogleManager.SubscriptionGoogleListener() {
        @Override
        protected void onBillingConnected(boolean success) {
            if (success){
                if (!TextUtils.isEmpty(keyword)) {
                    subscriptionGoogleManager.loadSKU(keyword);
                }
            }else {
                Toast.makeText(MainActivity.this, "Data on failed", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onSubsChecked(boolean exists) {

            ContactItem contactItem = SessionManager.getProfile(getApplicationContext());
            if (!exists) { // If Unsubscribe not exixts....

                if (contactItem != null) {

                    subscriptionGoogleManager.updateSubsStatus(contactItem.msisdn, null, null, true, null, null); // Update status Unsubscribe to server...
                    checkSubs(contactItem.msisdn, isSuccess -> {
                        if (!BaseActivity.contentBuys) {
                            if (!isSuccess) {
                                if (!MainActivity.this.isFinishing()) { // If unsubscribe, call popup subs.....
                                    String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                    if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                                        startActivity(new Intent(MainActivity.this, PopupVietSubsActivity.class));
                                        return;
                                    }
                                    startActivity(new Intent(MainActivity.this, PopUpSubsActivity.class));
                                }
                            }
                        }
                    });

                }


            }else { // If subscribe exists.....
                if (contactItem != null) {

                    try {
                        GpayData gpayData = SessionManager.getGpayData(MainActivity.this);
                        if (gpayData != null) {

                            String msisdn = gpayData.msisdn;
                            String type = gpayData.type;
                            String json = gpayData.json;
                            boolean cancel = gpayData.cancel;
                            String periode = gpayData.periode;
                            String price = gpayData.price;

                            if (!TextUtils.isEmpty(msisdn) && !TextUtils.isEmpty(type) && !TextUtils.isEmpty(json) && !TextUtils.isEmpty(periode) && !TextUtils.isEmpty(price)) {
                                subscriptionGoogleManager.updateSubsStatus(msisdn, type, json, cancel, periode, price); // Update status Unsubscribe to server...
                            }

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }
        }
    };

// ======================================================================================================================
// ======================================================================================================================

    @Override
    protected void onDestroy() {
        callAutoStartService();
        startJob();
        deleteContactDBFirebase();
        deleteCache(MainActivity.this);
        super.onDestroy();
    }

    /**
     * Firebase Jobs Dispatcher
     * Added by Yudha Pratama Putra, 12 Sept 2018
     */
    public void startJob() {
        Job job = jobDispatcher.newJobBuilder()
                .setService(YpService.class)
                .setLifetime(Lifetime.FOREVER)
                .setRecurring(true)
                .setTag(Job_Tag)
                .setTrigger(Trigger.executionWindow(0, 15))
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setReplaceCurrent(false)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();
        jobDispatcher.mustSchedule(job);
    }

    private void addUserDataInApplicationDir() {
        // Add shared preferences
        SharedPreferences settings = getSharedPreferences("sample", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("key1", true);
        editor.putString("key2", "Some strings in prefs");
        editor.commit();
        // Add file with content
        try {
            final String FILECONTENT = "This is string in file samplefile.txt";
            FileOutputStream fOut = openFileOutput("samplefile.txt", Context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            osw.write(FILECONTENT);
            osw.flush();
            osw.close();
        } catch (Exception ioe) {
            ioe.printStackTrace();
            return;
        }
    }

    /**
     * UI Threads
     * Update by Yudha Pratama Putra & Asykur, 12 Okt 2018
     */
    @SuppressLint("HardwareIds")
    public void readSerialSim() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                SubscriptionManager manager = (SubscriptionManager) getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE); //min Lollipop MR1
                if (manager != null){
                    int simID = manager.getDefaultSubscriptionId();// min Nougat
                    SubscriptionInfo info = manager.getActiveSubscriptionInfo(simID);
                    if (info != null) {
                        String iccid = info.getIccId();
                        if (!TextUtils.isEmpty(iccid)) {
                            compareSerialSIM(iccid);
                        }
                    }
                }

            } else {

                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (telephonyManager != null) {
                    String serialSIM = telephonyManager.getSimSerialNumber();
                    if (!TextUtils.isEmpty(serialSIM)) {
                        compareSerialSIM(serialSIM);
                    }
                }

            }

        } else {
            requestStoragePermission();
        }
    }

    /**
     * UI Threads
     * Update by Yudha Pratama Putra & Asykur, 12 Okt 2018
     */
    private void compareSerialSIM(String serialSIM) {
        String preferenceSerialSIM = AppPreference.getPreference(MainActivity.this, AppPreference.SERIAL_SIM);
        if (TextUtils.isEmpty(serialSIM) || !serialSIM.equals(preferenceSerialSIM)) {
            showDialogAutoLogout();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    FirebaseAuth.getInstance().signOut();
                    deleteAppData();
                }
            }, 4000);

        }
    }

    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_PHONE_STATE)) {

            if (!MainActivity.this.isFinishing()) {
                new AlertDialog.Builder(this).setTitle("Permission needed").setMessage("This permission is needed").setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, SIMCARD_PERMISSION_CODE);
                    }
                }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
            }

        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, SIMCARD_PERMISSION_CODE);
        }
    }

//    private void deleteAppData() {
//        try {
//            // clearing app data
//            String packageName = getApplicationContext().getPackageName();
//            Runtime runtime = Runtime.getRuntime();
//            runtime.exec("pm clear " + packageName);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    @SuppressLint("CheckResult")
    public void setUserInfo() {
        if (!MainActivity.this.isFinishing()) {
            ContactItem owns = SessionManager.getProfile(MainActivity.this);
            if (owns != null) {

                try {
                    //set Default profile Name & status

                    String strPhone = TextUtils.isEmpty(owns.msisdn)?"":owns.msisdn;
                    String strName = TextUtils.isEmpty(owns.name)?"":owns.name;
                    String strStatus = TextUtils.isEmpty(owns.status_caller)?"":owns.status_caller;

                    tvUserPhone.setText(strPhone);
                    tvUserName.setText(strName);
                    tvUserStatus.setText(strStatus);
                    // set Default status
                    if (TextUtils.isEmpty(strStatus)) {
                        String defaultStatus = TextUtils.isEmpty(model.getLang().string.defaultStatus)
                                ?"":model.getLang().string.defaultStatus;
                        tvUserStatus.setText(defaultStatus);
                        updateStatus(defaultStatus);
                    } else {
                        tvUserStatus.setText(strStatus);
                    }

                    RequestOptions request = new RequestOptions();
                    request.placeholder(R.drawable.userpng);
                    Glide.with(this).load(owns.caller_pic).apply(request).into(imgProfile);

                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

                saveContactFirebase(owns);
            }

        }
    }

    private void updateStatus(final String status) {
        ContactItem owns = SessionManager.getProfile(MainActivity.this);
        if (owns != null) {
            String callerId = owns.caller_id;
            if (!TextUtils.isEmpty(callerId)) {
                Call<APIResponse<String>> call = ServicesFactory.getService().updateCallerStatus(callerId, status);
                call.enqueue(new Callback<APIResponse<String>>() {
                    @Override
                    public void onResponse(@NotNull Call<APIResponse<String>> call, @NotNull Response<APIResponse<String>> response) {
                        if (response.body() != null && response.isSuccessful() && response.body().isSuccessful()) {
                            owns.status_caller = status;
                            SessionManager.saveProfile(MainActivity.this, owns);
                            // update ui
                            setUserInfo();
                        } else {
                            toastShort("Update status failed");
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<APIResponse<String>> call, @NotNull Throwable t) {
                        if (!isViewAttached) return;
                        toastShort(model.getLang().string.wordingFailureResponse);
                    }
                });
            }
        }
    }

    public void doClick(View view) {
        if (view.getId() == R.id.btn_video_tone) {
            Intent i = new Intent(MainActivity.this, SetVideoToneActivity.class);
            startActivity(i);

            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_HOME, TrackerConstant.EVENT_CAT_HOME, "click button personal video", own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (view.getId() == R.id.btn_edit_profile) {
            ContactItem own = SessionManager.getProfile(MainActivity.this);

            if (own != null) {

                Intent i = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(i);

                try {
                    GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_HOME, TrackerConstant.EVENT_CAT_HOME, "click button edit profile", own.caller_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (view.getId() == R.id.tab_friend) {
            if (PermissionUtil.hashPermission(MainActivity.this, permissions)) {
                stopBroadcastConfig(MainActivity.this);
                Intent i = new Intent(MainActivity.this, ContactTabActivity.class);
                startActivity(i);
                regDatatoSinch();
                try {
                    GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_HOME, TrackerConstant.EVENT_CAT_HOME, "click button friends", own.caller_id);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                openSettings();
            }
        } else if (view.getId() == R.id.btn_best_deal) {
            Intent intent = new Intent(MainActivity.this, BestDealActivity.class);
            startActivity(intent);

            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_HOME, TrackerConstant.EVENT_CAT_HOME, "click button best deal", own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (view.getId() == R.id.btn_HowTo) {
            Intent intent = new Intent(MainActivity.this, HowToUseActivity.class);
            startActivity(intent);

            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_HOME, TrackerConstant.EVENT_CAT_HOME, "click button how to", own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (view.getId() == R.id.btn_rewards) {
            Intent intent = new Intent(MainActivity.this, RewardActivity.class);
            startActivity(intent);

            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_HOME, TrackerConstant.EVENT_CAT_HOME, "click button rewards", own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.getItem(0).setTitle(model.getLang().string.settings);
        menu.getItem(1).setTitle(model.getLang().string.contactUs);
        menu.getItem(2).setTitle(model.getLang().string.about);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_settings) {
            Intent intent = new Intent(MainActivity.this, SettingPageActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.item_contact) {
            Intent intent = new Intent(MainActivity.this, ContactUsActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.id.item_help) {
            Intent i = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(MainActivity.this, model.getLang().string.pressToExit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
//
//    private void checkShowCaseViet(){
//
//        boolean caseVietnamTwenty = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_TWENTY_VIETNAME, false);
//        boolean caseVietnamTwentyOne = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_TWENTYONE_VIETNAME, false);
//
//        if (!caseVietnamTwenty){
//            showCaseIntroVietTwenty();
//        }else if (!caseVietnamTwentyOne){
//            showCaseIntroVietTwentyOne();
//        }
//
//    }
}