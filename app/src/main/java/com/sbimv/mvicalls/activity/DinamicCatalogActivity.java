package com.sbimv.mvicalls.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.PreviewPremiumVideo;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.adapter.DinamicTitleVideoAdapter;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DinamicCatalogActivity extends BaseActivity {

    private RecyclerView rvCatTitle;
    private ContactItem own;
    private ArrayList<DataDinamicTitle> dataCatalogVideos = new ArrayList<>();
    private String lang;
    private ProgressBar pgAllVideo;
    private ShimmerFrameLayout shimmerFrameLayout1, shimmerFrameLayout2, shimmerFrameLayout3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dinamic_catalog);
        pgAllVideo = findViewById(R.id.pgAllVid);
        setDefaultToolbar(true);
        rvCatTitle = findViewById(R.id.rvPerCategory);
        own = SessionManager.getProfile(this);
        lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");

        shimmerFrameLayout1 = findViewById(R.id.shimmerDinamicCatalog);
        shimmerFrameLayout2 = findViewById(R.id.shimmerDinamicCatalog2);
        shimmerFrameLayout3 = findViewById(R.id.shimmerDinamicCatalog3);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getCatalogData(own.msisdn);
                initRecycler();
                shimmerFrameLayout1.setVisibility(View.GONE);
                shimmerFrameLayout2.setVisibility(View.GONE);
                shimmerFrameLayout3.setVisibility(View.GONE);
            }
        }, BaseActivity.TIME_LOADING);
        shimmerFrameLayout1.stopShimmer();
        shimmerFrameLayout2.stopShimmer();
        shimmerFrameLayout3.stopShimmer();

    }

    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout1.startShimmer();
        shimmerFrameLayout2.startShimmer();
        shimmerFrameLayout3.startShimmer();
        LocalBroadcastManager.getInstance(DinamicCatalogActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout3.stopShimmer();
        shimmerFrameLayout2.stopShimmer();
        shimmerFrameLayout1.stopShimmer();
        LocalBroadcastManager.getInstance(DinamicCatalogActivity.this).unregisterReceiver(popupNotifAppears);
    }

    private void getCatalogData(String msisdn) {
        Call<APIResponse<List<DataDinamicTitle>>> call = ServicesFactory.getService().getDinamicTitleNew(msisdn,lang);
        call.enqueue(new Callback<APIResponse<List<DataDinamicTitle>>>() {
            @Override
            public void onResponse(Call<APIResponse<List<DataDinamicTitle>>> call, Response<APIResponse<List<DataDinamicTitle>>> response) {
                if (response.body() != null && response.isSuccessful() && response.body().isSuccessful()) {
                    List<DataDinamicTitle> datas = response.body().data;
                    if (datas != null) {
                        dataCatalogVideos.clear();
                        dataCatalogVideos.addAll(datas);
                        rvCatTitle.getAdapter().notifyDataSetChanged();
                        dismissProgressBar();
                        rvCatTitle.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResponse<List<DataDinamicTitle>>> call, Throwable t) {
                if (!isViewAttached)
                    return;
                dismissProgressBar();
            }
        });
    }

    private void initRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(DinamicCatalogActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        int column = 1;
        rvCatTitle.setLayoutManager(new GridLayoutManager(DinamicCatalogActivity.this,column));
        DinamicTitleVideoAdapter adapter = new DinamicTitleVideoAdapter(dataCatalogVideos,DinamicCatalogActivity.this);
        rvCatTitle.setAdapter(adapter);
    }

    private void dismissProgressBar(){
        if(!isViewAttached) // skip updating view
            return;
        try{
            pgAllVideo.setVisibility(View.GONE);
        }catch (Exception e){
           return;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //to set flag showcase success grab free content at first time
        PreferenceUtil.getEditor(DinamicCatalogActivity.this).putBoolean(PreferenceUtil.GRAB_FREE_CONTENT, true).commit();

    }
}
