package com.sbimv.mvicalls.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;

import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.LocaleHelper;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.databinding.RegisterPhoneWithCodeCountryBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.intro.IntroActivitySecond;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.receiver.InstallReferrerReceiver;
import com.sbimv.mvicalls.util.PermissionUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;
import com.sbimv.mvicalls.util.ValidationInputUtil;

import java.util.concurrent.TimeUnit;

import io.michaelrocks.libphonenumber.android.NumberParseException;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPhoneActivity extends BaseActivity implements View.OnClickListener,
        CountryCodePicker.OnCountryChangeListener, CompoundButton.OnCheckedChangeListener {

    private EditText etPhoneNo;
    private Button btnRegister;
    private Button tvResendOTP;
    private TextView tvTNC;
    private CheckBox cbTNC;
    public static RegisterPhoneActivity rpa;
    private CountryCodePicker ccp;
    private String getSimNumber = "";
    private String getSerialSIM = "";
    private String countyCode;
    private LinearLayout lnTnc;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    String mVerificationID;
    private static final String TAG = "PhoneAuthActivity";
    private String msisdn;
    private ProgressDialog pdLogin;
    Handler mHandler = new Handler(Looper.getMainLooper());
    private String countryName ;
    private String msisdnAuth;
    private String nameCode;

    private String nameCodeNew;
    private String countryCodeName;
    final String key = "lang_has_set";
    final String key_country = "country_has_set";
    private RegisterPhoneWithCodeCountryBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.register_phone_with_code_country);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        etPhoneNo = findViewById(R.id.etPhoneNo);
        ccp = findViewById(R.id.codeCountry);
        btnRegister = findViewById(R.id.btnReqToken);
        lnTnc = findViewById(R.id.lnTnc);
        tvTNC = findViewById(R.id.tvTnC);
        cbTNC = findViewById(R.id.cbTNC);
        tvResendOTP = findViewById(R.id.tvResendOTP);

        // Firebase authentication
        mAuth = FirebaseAuth.getInstance();

        // google analytics...
        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_REGISTER);
        }catch (Exception e){
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            btnRegister.setBackgroundResource(R.drawable.button_selector);
        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            btnRegister.setBackgroundResource(R.drawable.selector_rounded_grey_e4e4e4);
            btnRegister.setBackgroundResource(R.drawable.selector_rounded_grey_e4e4e4);
        }

        // Inisial on action...........
        tvTNC.setOnClickListener(RegisterPhoneActivity.this);
        btnRegister.setOnClickListener(RegisterPhoneActivity.this);

        String selectedCCP = PreferenceManager.getDefaultSharedPreferences(this).getString(key_country, null);
        if(!TextUtils.isEmpty(selectedCCP)){
            ccp.setDefaultCountryUsingNameCode(selectedCCP);
            ccp.resetToDefaultCountry();
        }
        countyCode = ccp.getDefaultCountryCodeWithPlus();
        nameCodeNew = ccp.getDefaultCountryNameCode();
        ccp.setOnCountryChangeListener(this);

        tvResendOTP.setBackground(getResources().getDrawable(R.drawable.selector_rounded_blue_logo));
        cbTNC.setOnCheckedChangeListener(this);

    }

// ============================================================================================================
// ============================================ Android methode ===============================================
// ============================================================================================================

    // onClick methode........
    @Override
    public void onClick(View view) {

        if (tvTNC == view){ // TNC on click............
            Intent intent = new Intent(RegisterPhoneActivity.this, TermConditionActivity.class);
            startActivity(intent);
            // google analytics...
            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(
                        TrackerConstant.SCREEN_REGISTER,
                        TrackerConstant.EVENT_CAT_REGISTER,
                        "click tnc",
                        "");
            }catch (Exception e){
                e.printStackTrace();
                return;
            }

        }
        else if (btnRegister == view){ // Button register on click...............

            String phoneNo = etPhoneNo.getText().toString();
            if(checkValidation()){
                String filter = "";
                showDialogs();
                if (phoneNo.startsWith("0")) {
                    filter = phoneNo.replace("0", "");
                } else if (phoneNo.startsWith("+")) {
                    filter = takeCountryCode(phoneNo,nameCode);
                } else {
                    filter = phoneNo;
                }
                msisdn = countyCode + filter;
                verifyPhoneNumber(msisdn);

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        showCustomDialogs(model.getLang().string.authenticating);
                    }
                });
                // google analytics...
                if (!TextUtils.isEmpty(msisdn)){
                    try {
                        GATRacker.getInstance(getApplication()).sendEventWithScreen(
                                TrackerConstant.SCREEN_REGISTER,
                                TrackerConstant.EVENT_CAT_REGISTER,
                                "click register",
                                msisdn);
                    }catch (Exception e){
                        e.printStackTrace();
                        return;
                    }
                }
            }

        }
    }

    // onCountrySelected methode..........
    @Override
    public void onCountrySelected(Country country) {
        if (country != null) {
            countyCode = ccp.getSelectedCountryCodeWithPlus();
            nameCode = ccp.getSelectedCountryNameCode();

            // google analytics...
            if (!TextUtils.isEmpty(countyCode)) {
                try {
                    GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_REGISTER, TrackerConstant.EVENT_CAT_REGISTER, "change countryCodeName code", countyCode);
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    // onCheckedChanged methode.......
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            btnRegister.setEnabled(true);
            btnRegister.setBackground(getResources().getDrawable(R.drawable.selector_rounded_blue_logo));
            btnRegister.setTextColor(getResources().getColor(R.color.white));
        } else {
            btnRegister.setEnabled(false);
            btnRegister.setBackground(getResources().getDrawable(R.drawable.button_selector));
            btnRegister.setTextColor(getResources().getColor(R.color.white));
        }
    }

// ============================================================================================================
// ============================================================================================================
// ============================================================================================================


// ============================================================================================================
// =========================================== Android life cycle =============================================
// ============================================================================================================

    @Override
    protected void onResume() {
        super.onResume();
        readPhoneAuto();
    }

// ============================================================================================================
// ============================================================================================================
// ============================================================================================================

// ============================================================================================================
// =========================================== Firebase auth function =========================================
// ============================================================================================================

    void verifyCode(String code){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationID, code);
        signInWithPhoneAuthCredential(credential);
    }

    void verifyPhoneNumber(String number){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                this,
                new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                    @Override
                    public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                        signInWithPhoneAuthCredential(phoneAuthCredential);
                    }

                    @Override
                    public void onVerificationFailed(FirebaseException e) {
                        dismissDialogs();
                        if (e instanceof FirebaseAuthInvalidCredentialsException) {
                            showSnackBar("Authentication Failed..");
                        }
                        else if (e instanceof FirebaseTooManyRequestsException) {
                            showSnackBar("Quota exceeded...");
                        }
                        else if (e instanceof FirebaseAuthInvalidCredentialsException){
                            showSnackBar("Code Failed..");
                        }else if (e instanceof FirebaseAuthException){
                            showSnackBar("FirebaseAuthException..");
                        }
                    }

                    @Override
                    public void onCodeSent(String verificationID, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                        mVerificationID = verificationID;
                        mResendToken = forceResendingToken;
                    }

                    @Override
                    public void onCodeAutoRetrievalTimeOut(String s) {
                        super.onCodeAutoRetrievalTimeOut(s);
                        dismissDialogs();
                        String rto = TextUtils.isEmpty(model.getLang().string.str_all_page_rto)
                                ? "Request Time Out"
                                : model.getLang().string.str_all_page_rto;
                        showSnackBar(rto);
                        // google analytics...
                        if (!TextUtils.isEmpty(msisdn)){
                            try {
                                // google analytics...
                                GATRacker.getInstance(getApplication()).sendEventWithScreen(
                                        TrackerConstant.SCREEN_REGISTER,
                                        TrackerConstant.EVENT_CAT_REGISTER,
                                        "phone auth time out",
                                        msisdn);
                            }catch (Exception e){
                                e.printStackTrace();
                                return;
                            }
                        }
                    }
                }
        );
    }

    void signInWithPhoneAuthCredential(final PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                // Update by Yudha Pratama Putra - 01/11/2018
                // Validate User +62 And 0 in database....
                if (task.isSuccessful()) {
                    if (task == null) {
                        toast("Respone authentication null...");
                        return;
                    }else {
                        if (!RegisterPhoneActivity.this.isFinishing()){
                            try {
                                msisdnAuth = task.getResult().getUser().getPhoneNumber();
                            }catch (Exception e){
                                toast("Phone Number not invalid, please try again...");
                                return;
                            }
                            if (!TextUtils.isEmpty(getSerialSIM)){
                                oauthToken(msisdnAuth, getSerialSIM);
                            }
                            // google analytics...
                            if (!TextUtils.isEmpty(msisdnAuth)){
                                try {
                                    GATRacker.getInstance(getApplication()).sendEventWithScreen(
                                            TrackerConstant.SCREEN_REGISTER,
                                            TrackerConstant.EVENT_CAT_REGISTER,
                                            "phone auth succcessful",
                                            msisdnAuth);
                                }catch (Exception e){
                                    e.printStackTrace();
                                    return;
                                }
                            }
                        }
                    }
                } else {
                    dismissDialogs();
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        String otp = TextUtils.isEmpty(model.getLang().string.str_otp_page_invalid_code)
                                ? "Invalid Code..."
                                : model.getLang().string.str_otp_page_invalid_code;
                        showSnackBar(otp);
                        // google analytics...
                        if (!TextUtils.isEmpty(msisdnAuth)){
                            try {
                                GATRacker.getInstance(getApplication()).sendEventWithScreen(
                                        TrackerConstant.SCREEN_REGISTER,
                                        TrackerConstant.EVENT_CAT_REGISTER,
                                        "phone auth failed",
                                        msisdnAuth);
                            }catch (Exception e){
                                e.printStackTrace();
                                return;
                            }
                        }
                    }
                }
            }
        });
    }

// ============================================================================================================
// ============================================================================================================
// ============================================================================================================

// ============================================================================================================
// ================================================ Fuctions ==================================================
// ============================================================================================================

    // Read phone auto function.............
    @SuppressLint("HardwareIds")
    public void readPhoneAuto() {
        if (PermissionUtil.hashPermission(RegisterPhoneActivity.this, permissions)) {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {

                if (telephonyManager!= null){
                    setDefaultLanguage(telephonyManager.getSimCountryIso());
                }
                String filter;
                try {
                    if (telephonyManager != null) {
                        getSimNumber = telephonyManager.getLine1Number();
                    }
                }catch (Exception e){
                    getSimNumber = "";
//                    toast("SIM Number not detected...");
                }

                try {
                    if (telephonyManager != null) {
                        getSerialSIM = telephonyManager.getSimSerialNumber();
                    }

                }catch (Exception e){
//                    toast("Serial SIM not detected...");
                }

                if (TextUtils.isEmpty(getSimNumber))
                    return;
                if (getSimNumber.startsWith("0")) {
                    filter = getSimNumber.replace("0", "");
                } else if (getSimNumber.startsWith("+")) {
                    filter = takeCountryCode(getSimNumber,nameCodeNew);
                } else {
                    filter = getSimNumber;
                }

                String setMsisdn = Util.formatMSISDNREG(filter,countyCode);
                etPhoneNo.setText(setMsisdn);
            }
        }
    }

    private void setDefaultLanguage(String simCountryCodeISO){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean hasSet = preferences.getBoolean(key, false);
        if(!hasSet){
            preferences.edit().putBoolean(key, true).apply();

            // set default language
            if (simCountryCodeISO.equalsIgnoreCase("ID")) {
                // set to Bahasa Indonesia
                LocaleHelper.setLocale(this, simCountryCodeISO);
            }

            // set default country code spinner
            // ID,VN,TH,MY,SG,MM,PH,LA,KH,BN
            String code = simCountryCodeISO.toUpperCase();
            switch (code){
                case "ID":
                case "VN":
                case "TH":
                case "MY":
                case "SG":
                case "MM":
                case "PH":
                case "LA":
                case "KH":
                case "BN":
                    preferences.edit().putString(key_country, code).apply();
            }
            recreate();
        }
    }

    /**
     * UI Threads
     * Update by Yudha Pratama Putra & Asykur, 12 Okt 2018
     */
    private boolean checkValidation() {
        boolean ret = true;
        if (!ValidationInputUtil.hasText(etPhoneNo)) ret = false;
        return ret;
    }



    private String takeCountryCode(String filterNo,String codeName) {
        long nationalNumber = 0;

            try {
                // phone must begin with '+'
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(this);
                Phonenumber.PhoneNumber numberProto = phoneUtil.parse(filterNo, codeName);
                nationalNumber = numberProto.getNationalNumber();
            } catch (NumberParseException e) {
                System.err.println("NumberParseException was thrown: " + e.toString());
            }
        return String.valueOf(nationalNumber);
    }

    // Oauth user validation....
    private void oauthToken(String msisdn, String serial_sim) {
        // Added by hendi

        @SuppressLint("HardwareIds")
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        String device_id = TextUtils.isEmpty(android_id)?"":android_id;

        String referrer = InstallReferrerReceiver.getReferrer(getApplicationContext());

        Call<APIResponse<ContactItem>> call = ServicesFactory.getService().register(msisdn, referrer, "android",serial_sim, device_id);
        call.enqueue(new Callback<APIResponse<ContactItem>>() {
            @Override
            public void onResponse(Call<APIResponse<ContactItem>> call, Response<APIResponse<ContactItem>> response) {
                dismissDialogs();
                if(response.isSuccessful() && response.body().isSuccessful()){
                    if (!isViewAttached)
                        return;
                    ContactItem item = response.body().data;
                    if (item != null){
                        if (!TextUtils.isEmpty(item.name)){
                            //If Old User...
                            if (setPreferenceSimCard() ==  true){
                                SessionManager.saveProfile(RegisterPhoneActivity.this, item);
                                nextProcess();
                            }else {
                                toast("Save session error...");
                            }
                        }else {
                            //If New User...
                            if (setPreferenceSimCard() ==  true){
                                SessionManager.saveProfile(RegisterPhoneActivity.this, item);
                                PreferenceUtil.getEditor(RegisterPhoneActivity.this).putBoolean(PreferenceUtil.NEWUSER, true).commit();
                                nextProcess();
                            }else {
                                toast("Save session error...");
                            }
                        }

                    }else {
                        toast("Failed while getting registration data...");
                    }
                }
                else{
                    toast("Authentication failed");
                }
            }
            @Override
            public void onFailure(Call<APIResponse<ContactItem>> call, Throwable t) {
                if (!isViewAttached)
                    return;
                toast("Authentication failed");
                dismissDialogs();
            }
        });
    }

    // Intent activity........
    private void nextProcess(){
        // remove referrer
        InstallReferrerReceiver.setReferrer(getApplicationContext(), null);

        AppPreference.setPreference(RegisterPhoneActivity.this, AppPreference.PREFIX, countyCode);
        if (!isViewAttached)
            return;
//        Intent intent = new Intent(RegisterPhoneActivity.this, IntroActivity.class);
        Intent intent = new Intent(RegisterPhoneActivity.this, IntroActivitySecond.class);
        startActivity(intent);
        finish();
    }

    // Save to preference........
    private boolean setPreferenceSimCard(){
        boolean has = false;
        String preferMsisdn = etPhoneNo.getText().toString();
        if (!TextUtils.isEmpty(preferMsisdn) && !TextUtils.isEmpty(getSerialSIM) && !TextUtils.isEmpty(countyCode)){
            String setPrefMsisdn = Util.formatMSISDNREG(preferMsisdn,countyCode);
            // Save data for serial sim
            AppPreference.setPreference(RegisterPhoneActivity.this, AppPreference.SERIAL_SIM, getSerialSIM);
            AppPreference.setPreference(RegisterPhoneActivity.this,AppPreference.COUNTRY_CODE_NAME,countryCodeName);
            // Save data for sim number
            AppPreference.setPreference(RegisterPhoneActivity.this, AppPreference.CURRENT_MSISDN, setPrefMsisdn);
            has = true;
        }else {
            has = false;
        }
        return has;
    }

// ============================================================================================================
// ============================================================================================================
// ============================================================================================================

    /**
     * UI Threads
     * Added by Yudha Pratama Putra, 08 Okt 2018
     */
// ============================================================================================================
// ================================================ UI Threads ================================================
// ============================================================================================================
    private void showCustomDialogs(String msg){
        if (!isViewAttached)
            return;
        try{
            pdLogin.setMessage(msg);
        }catch (Exception e){
            return;
        }
    }

    private void showDialogs(){
        if (!isViewAttached)
            return;
        try{
            pdLogin = new ProgressDialog(this);
            pdLogin.setMessage("Authenticating ...");
            pdLogin.setCancelable(false);
            pdLogin.show();
        }catch (Exception e){
            return;
        }
    }

    private void dismissDialogs(){
        if (!isViewAttached)
            return;
        try{
            pdLogin.dismiss();
        }catch (Exception e){
            return;
        }
    }

    private void showSnackBar(String argument){
        if (!isViewAttached)
            return;
        try{
            Snackbar.make(findViewById(android.R.id.content), argument, Snackbar.LENGTH_LONG).show();
        }catch (Exception e){
            return;
        }
    }

// ============================================================================================================
// ============================================================================================================
// ============================================================================================================

}