package com.sbimv.mvicalls.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.auth.FirebaseAuth;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;

/**
 * Created by admin on 3/21/2018.
 */

public class Utility {

    private final Context mContext;
    private static Utility instance;


    private Utility(Context applicationContext) {
        this.mContext = applicationContext;
    }

    public static Utility getInstance(Context appContext) {
        if (instance == null)
            instance = new Utility(appContext);
        return instance;
    }

    public void restartApp(Activity activity) {
        if (!activity.isFinishing()) {
            Intent i = activity.getBaseContext()
                    .getPackageManager()
                    .getLaunchIntentForPackage(activity.getBaseContext().getPackageName());
            assert i != null;
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(i);
            activity.finish();
        }
    }

    public void logoutApp(boolean isDialog){
        if (isDialog) {
            if (mContext != null) {
                Intent intentVerApp = new Intent(ConfigPref.PUSH_NOTIFICATION);
                intentVerApp.putExtra("from", "notif_logout");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intentVerApp);
            }
        }else {
            if (mContext != null) {
                PreferenceUtil.getEditor(mContext).putBoolean(PreferenceUtil.DIALOG_AUTO_LOGOUT, true).commit();
            }
        }
    }

}
