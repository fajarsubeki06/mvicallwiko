package com.sbimv.mvicalls.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkTextView;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.contract.MainContract;
import com.sbimv.mvicalls.interactor.IntractorImpl;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.InboxModel;
import com.sbimv.mvicalls.presenter.InboxPresenterImpl;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.List;


public class DetailInboxActivity extends BaseActivity implements MainContract.InboxView, View.OnClickListener {

    private LinearLayout btn_close;
    private TextView tvd_title;
    private TextView tvd_time;
    private AutoLinkTextView tvd_message;

    private LangViewModel modelLang;
    MainContract.InboxPresenter inboxPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_detail);

        modelLang = LangViewModel.getInstance();
        setToolbarInbox(model.getLang().string.detailInbox);
        setInitView();

        inboxPresenter = new InboxPresenterImpl(this, new IntractorImpl());

        // Load Data Inbox
        InboxModel collection = getIntent().getParcelableExtra("dataInbox");
        ContactItem contactItem = SessionManager.getProfile(this);
        inboxPresenter.readDataInbox(contactItem, "read",collection);

    }

    private void setInitView(){

        TextView tvCLose = findViewById(R.id.tvCLose);
        tvCLose.setText(modelLang.getLang().string.btnclose);
        tvd_title = findViewById(R.id.tvd_title);
        tvd_message = findViewById(R.id.tvd_message);
        tvd_message.addAutoLinkMode(AutoLinkMode.MODE_URL);
        tvd_message.setUrlModeColor(getResources().getColor(R.color.blue_facebook));
        tvd_message.enableUnderLine();
        tvd_time = findViewById(R.id.tvd_time);
        btn_close = findViewById(R.id.btn_close);

        tvd_message.setAutoLinkOnClickListener((autoLinkMode, matchedText) -> {
            if (matchedText.contains("rewards")){
                String appUrl = matchedText.replace("http","app")
                        .replace("https","app")
                        .replace(" ","");
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appUrl)));
            }
        });

        btn_close.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(DetailInboxActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(DetailInboxActivity.this).unregisterReceiver(popupNotifAppears);
    }

    @Override
    public void onClick(View v) {
        if (btn_close == v){
            backIntent();
        }
    }

    @Override
    public void isProgress() {

    }

    @Override
    public void isFinish() {

    }

    @Override
    public void isEmpty() {

    }

    @Override
    public void isSuccess(List dataArrayList) {

    }

    @Override
    public void isDetailSuccess(String title, String message, String createDate) {
        if (DetailInboxActivity.this.isFinishing()) // skip updating view
            return;

        tvd_title.setText(title);
        tvd_message.setText(message);
        tvd_time.setText(createDate);

    }

    @Override
    public void isFailure() {
        if (modelLang == null)
            return;

        String txIsFailure = TextUtils.isEmpty(modelLang.getLang().string.wordingFailureResponse)?"":modelLang.getLang().string.wordingFailureResponse;
        Toast.makeText(this, txIsFailure, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        backIntent();
    }

    private void backIntent(){
        Intent intent = new Intent(DetailInboxActivity.this, InboxActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @SuppressLint("CheckResult")
    protected void setToolbarInbox(String title) {
        toolbar = findViewById(R.id.tbAbout);
        setSupportActionBar(toolbar);

        View backButton = findViewById(R.id.btn_actionbar_back);
        ImageView ivLogo = findViewById(R.id.iv_logo);
        TextView tvTitle = findViewById(R.id.tv_title_menu);
        RelativeLayout toInbox = findViewById(R.id.rl_inbox);

        toInbox.setVisibility(View.GONE);

        if (TextUtils.isEmpty(title)) {
            tvTitle.setText("");
            tvTitle.setVisibility(View.GONE);
            ivLogo.setVisibility(View.VISIBLE);
        } else {
            tvTitle.setText(title);
            tvTitle.setVisibility(View.VISIBLE);
            ivLogo.setVisibility(View.GONE);
        }

        backButton.setVisibility(View.VISIBLE);
        backButton.setOnClickListener(view -> backIntent());

    }
}
