package com.sbimv.mvicalls.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import androidx.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.adapter.VideoFrontAdapter;
import com.sbimv.mvicalls.databinding.ActivitySetVideoToneBinding;
import com.sbimv.mvicalls.http.ProgressRequestBody;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.icallservices.Log;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.services.SyncVideoProfileServices;
import com.sbimv.mvicalls.sync.SyncContactProfileManager;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.OwnVideoManager;
import com.sbimv.mvicalls.util.PermissionUtil;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseListener;
import com.sbimv.mvicalls.util.ShowCaseWrapper;
import com.sbimv.mvicalls.view.DialogUpload;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;


import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yudha on 17/07/18.
 */

public class SetVideoToneActivity extends BaseActivity {

    public static String lang;
    private static DialogUpload dialogUpload;
    final int REQUEST_UPLOAD = 989;
    public Button btn_change_video, btnOthers;
    ContactItem own;
    ShowCaseWrapper showCaseWrapper;
    private ShimmerFrameLayout shimmerVideoTone;
    private VideoView videoView;
    private RecyclerView rvFrontVideo;
    private FrameLayout mediaControllerAnchor;
    private CheckBox cbEnableVideoTone;
    private MediaController mediaController;
    private TextView tvMoreTopArtis;
    private TextView tvCategory;
    private SharedPreferences sharedPreferences;
    private boolean setCont = false;
    private String urlBy = "";
    private DataDinamicTitle datas;
    private boolean isScUpload = false;
    private BroadcastReceiver downloadVideo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            doneProgress();
        }
    };
    private BroadcastReceiver startDownload = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            startProgress();
        }
    };
    private CompoundButton.OnCheckedChangeListener cbListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton cb, boolean isChecked) {
            if (isChecked) {
                PreferenceUtil
                        .getEditor(SetVideoToneActivity.this)
                        .putBoolean(PreferenceUtil.VIDEO_TONE_ENABLE, isChecked)
                        .commit();
                Toast.makeText(SetVideoToneActivity.this, "Video tone enabled", Toast.LENGTH_SHORT).show();
            } else {
                PreferenceUtil
                        .getEditor(SetVideoToneActivity.this)
                        .putBoolean(PreferenceUtil.VIDEO_TONE_DISABLE, isChecked)
                        .commit();
                Toast.makeText(SetVideoToneActivity.this, "Video tone disabled", Toast.LENGTH_SHORT).show();
            }
        }
    };
    private ActivitySetVideoToneBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_set_video_tone);
        binding.setLangModel(model);

        setDefaultToolbar(true);
        showCaseWrapper = ShowCaseWrapper.create(SetVideoToneActivity.this);
        lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
        sharedPreferences = SetVideoToneActivity.this.getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);

        btnOthers = findViewById(R.id.btn_check_others);
        videoView = findViewById(R.id.video_view);
        rvFrontVideo = findViewById(R.id.rv_front_video);
        cbEnableVideoTone = findViewById(R.id.cb_enable_video);
        tvMoreTopArtis = findViewById(R.id.tvTop);
        tvCategory = findViewById(R.id.tvCategory);
        btn_change_video = findViewById(R.id.btn_change_video);
        shimmerVideoTone = findViewById(R.id.shimmerVideoTone);
        btn_change_video.setEnabled(true);

        try {
            own = SessionManager.getProfile(SetVideoToneActivity.this);
        } catch (Exception e) {
            return;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initRecyclerView();
                loadFrontContent(own.msisdn, lang);
                shimmerVideoTone.setVisibility(View.GONE);
            }
        }, BaseActivity.TIME_LOADING    );
        shimmerVideoTone.stopShimmer();

        configDynamic(SetVideoToneActivity.this);
        // set checkbox (enable video tone)
        boolean isVideoToneEnable = PreferenceUtil.getPref(SetVideoToneActivity.this).getBoolean(PreferenceUtil.VIDEO_TONE_ENABLE, true);
        cbEnableVideoTone.setChecked(isVideoToneEnable);
        cbEnableVideoTone.setOnCheckedChangeListener(cbListener);

//        tvMoreTopArtis.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Intent intent = new Intent(SetVideoToneActivity.this, MoreTopArtisActivitys.class);
////                startActivity(intent);
//            }
//        });

//        if (!((Activity) SetVideoToneActivity.this).isFinishing()) {
//            showCaseWrapper.showFancydoAction(ShowCaseWrapper.SET_VIDEO_TONE, model.getLang().string.wordingChangeVideo, btn_change_video,
//                    new ShowCaseListener.ActionListener() {
//                        @Override
//                        public void onScreenTap() {
//                            checkSuscription();
//                        }
//
//                        @Override
//                        public void onOk() {
//                            checkSuscription();
//                        }
//
//                        @Override
//                        public void onSkip() {
//                            AppPreference.setPreferenceBoolean(SetVideoToneActivity.this, AppPreference.SKIPPED, false);
//                            finish();
//                        }
//                    });
//
//        }

        try {
            // google analytics...
            GATRacker.getInstance(getApplication())
                    .sendScreen(TrackerConstant.SCREEN_PERSONAL_VIDEO);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    private void checkSuscription() {
        checkSubs(own.msisdn, new CheckSubscribe() {
            @Override
            public void success(boolean isSuccess) {
                if (!isSuccess) {
                    startActivity(new Intent(SetVideoToneActivity.this, UploadVideoToneActivity.class));
                } else {
                    if (!isViewAttached)
                        return;

                    String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                    if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                        startActivity(new Intent(SetVideoToneActivity.this, PopupVietSubsActivity.class));
                        return;
                    }

                    Intent intent = new Intent(SetVideoToneActivity.this, PopUpSubsActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void loadFrontContent(String msisdn, String lang) {
        Call<APIResponse<DataDinamicTitle>> call = ServicesFactory.getService().getFrontContent(msisdn, lang);
        call.enqueue(new Callback<APIResponse<DataDinamicTitle>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<DataDinamicTitle>> call, @NotNull Response<APIResponse<DataDinamicTitle>> response) {
                if (response.body() != null && response.isSuccessful() && response.body().isSuccessful()) {
                    tvCategory.setText(response.body().data.getCategory());
                    datas = response.body().data;
                    if (datas != null) {
                        rvFrontVideo.getAdapter().notifyDataSetChanged();
                    }
                    initRecyclerView();
                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<DataDinamicTitle>> call, @NotNull Throwable t) {
                if (!isViewAttached) // skip updating view••••••••s
                    return;
                toast(model.getLang().string.wordingFailureResponse);
            }
        });
    }

    private void initRecyclerView() {
        LinearLayoutManager layout = new LinearLayoutManager(SetVideoToneActivity.this);
        layout.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvFrontVideo.setLayoutManager(layout);
        VideoFrontAdapter adapter = new VideoFrontAdapter(datas);
        rvFrontVideo.setAdapter(adapter);
    }

    public void refreshActivity() {
        own = SessionManager.getProfile(SetVideoToneActivity.this);
        initMediaPlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerVideoTone.stopShimmer();
        stopBroadcastConfig(SetVideoToneActivity.this);
        LocalBroadcastManager.getInstance(SetVideoToneActivity.this).unregisterReceiver(popupNotifAppears);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (PermissionUtil.hashPermission(SetVideoToneActivity.this, permissions)) {
            boolean firstSyncVideoProfile = PreferenceUtil.getPref(this)
                    .getBoolean(PreferenceUtil.FIRST_SYNC_VIDEO_CONTENT, false);

            if (firstSyncVideoProfile == false) {
                if (!SyncContactProfileManager.getInstance(getApplicationContext()).isSynchronizing()) {
                    Intent i = new Intent(SetVideoToneActivity.this, SyncVideoProfileServices.class);
                    startService(i);
                }
            }
            //download video...
            IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.DOWNLOAD_VIDEO");
            LocalBroadcastManager.getInstance(SetVideoToneActivity.this).registerReceiver(downloadVideo, _if);
            //Start download video...
            IntentFilter _iff = new IntentFilter("com.sbimv.mvicalls.START_PROGRESS");
            LocalBroadcastManager.getInstance(SetVideoToneActivity.this).registerReceiver(startDownload, _iff);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(SetVideoToneActivity.this).unregisterReceiver(downloadVideo);
        LocalBroadcastManager.getInstance(SetVideoToneActivity.this).unregisterReceiver(startDownload);
    }

    private void doneProgress() {
        if (!SetVideoToneActivity.this.isFinishing()) {
            refreshActivity();
            dismissDialogs();
            setResult(RESULT_OK);
        }
    }

    public void startProgress() {
        showDialogs();
        // ini progress listener
        ProgressRequestBody.ProgressListener progressListener = new ProgressRequestBody.ProgressListener() {
            @Override
            public void transferred(final int num, long transferred, long totalSize) {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            showCustomDialogs("Get video profile... " + num + "%");
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();

        shimmerVideoTone.startShimmer();
        checkNetwork(rvFrontVideo);
        showBroadcastConfig(SetVideoToneActivity.this);
        sharedPreferences = SetVideoToneActivity.this.getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        setCont = sharedPreferences.getBoolean(ConfigPref.SET_CONTENT, false);
        urlBy = sharedPreferences.getString(ConfigPref.URL_BUY, "");
        if (!TextUtils.isEmpty(urlBy)) {
            urlBy = urlBy.replaceAll(" ", "%20");
        }
        initMediaPlayer();

        LocalBroadcastManager.getInstance(SetVideoToneActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));


        isScUpload = PreferenceUtil.getPref(SetVideoToneActivity.this).getBoolean(PreferenceUtil.SHOW_CASE_UPLOAD_VIDEO, false);
        if (isScUpload) {
            // show showcase next
            showCaseWrapper.showFancyNext(ShowCaseWrapper.ID_UPLOAD_VIDEO, new ShowCaseListener.NextListener() {
                @Override
                public void onNext() {
//                    finish();
                    String camera = PreferenceUtil.getPref(SetVideoToneActivity.this).getString(PreferenceUtil.FROM_CAMERA,"");
                    String gallery = PreferenceUtil.getPref(SetVideoToneActivity.this).getString(PreferenceUtil.FROM_GALLERY,"");
                    if (camera.equals("camera")){
                        Intent intent = new Intent(SetVideoToneActivity.this,UploadVideoToneActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        PreferenceUtil.getEditor(SetVideoToneActivity.this).putString(PreferenceUtil.FROM_CAMERA,"camera_opened").commit();
                    }else if(gallery.equals("gallery")){
                        Intent intent = new Intent(SetVideoToneActivity.this,DinamicCatalogActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        PreferenceUtil.getEditor(SetVideoToneActivity.this).putString(PreferenceUtil.FROM_GALLERY,"gallery_opened").commit();
                    }else{
                        Intent intent = new Intent(SetVideoToneActivity.this,DinamicCatalogActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }

                    PreferenceUtil
                            .getEditor(SetVideoToneActivity.this)
                            .putBoolean(PreferenceUtil.SHOW_CASE_UPLOAD_VIDEO, false)
                            .commit();
                }
            });
        }

    }

    private void initMediaPlayer() {
        mediaController = new MediaController(SetVideoToneActivity.this);
        mediaController.setAnchorView(mediaControllerAnchor);
        videoView.setMediaController(mediaController);
        videoView.setZOrderOnTop(true);

        ContactItem contactItem = SessionManager.getProfile(SetVideoToneActivity.this);
        saveContactFirebase(contactItem);

        if (setCont) {
            Uri uris = Uri.parse(urlBy);
            videoView.setVideoURI(uris);
            videoView.seekTo(100);
            videoView.start();
            saveContactFirebase(contactItem);
        } else {
            if (OwnVideoManager.isOwnVideoExist(SetVideoToneActivity.this)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Uri ownVideoUri = OwnVideoManager.getOwnVideoUri(getApplicationContext());
                        videoView.setVideoURI(ownVideoUri);
                        videoView.seekTo(100);
                        videoView.start();
                    }
                });
            } else if (OwnVideoManager.collectionExist(SetVideoToneActivity.this)) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Uri uri = OwnVideoManager.getCollectionUri(getApplicationContext());
                        videoView.setVideoURI(uri);
                        videoView.seekTo(100);
                        videoView.start();
                    }
                });
            }
        }
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(false);
            }
        });
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return true;
            }
        });
    }

    public void changeVideo(View view) {

//        checkSubs(own.msisdn, new CheckSubscribe() {
//            @Override
//            public void success(boolean isSuccess) {
//                // google analytics...
//                if (own != null) {
//                    try {
//                        GATRacker.getInstance(getApplication()).sendEventWithScreen(
//                                TrackerConstant.SCREEN_PERSONAL_VIDEO,
//                                TrackerConstant.EVENT_CAT_PERSONAL_VIDEO,
//                                "click change video",
//                                own.caller_id);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                if (!isViewAttached)
//                    return;
//                if (!isSuccess) {
//                    if (!isViewAttached)
//                        return;
//
//                    String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
//                    if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
//                        startActivity(new Intent(SetVideoToneActivity.this, PopupVietSubsActivity.class));
//                        return;
//                    }
//
//                    Intent intent = new Intent(SetVideoToneActivity.this, PopUpSubsActivity.class);
//                    startActivity(intent);
//
//                } else {
//                    if (!isViewAttached)
//                        return;
//                    Intent i = new Intent(SetVideoToneActivity.this, UploadVideoToneActivity.class);
//                    startActivityForResult(i, REQUEST_UPLOAD);
//                }
//            }
//        });

        if (!isViewAttached)
            return;
        Intent i = new Intent(SetVideoToneActivity.this, UploadVideoToneActivity.class);
        startActivityForResult(i, REQUEST_UPLOAD);

        // google analytics...
        if (own != null) {
            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(
                        TrackerConstant.SCREEN_PERSONAL_VIDEO,
                        TrackerConstant.EVENT_CAT_PERSONAL_VIDEO,
                        "click change video",
                        own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_UPLOAD) {
                if (OwnVideoManager.isOwnVideoExist(SetVideoToneActivity.this)) {
                    Uri ownVideoUri = OwnVideoManager.getOwnVideoUri(SetVideoToneActivity.this);
                    videoView.setVideoURI(ownVideoUri);
                    videoView.seekTo(100);
                    ContactItem contactItem = SessionManager.getProfile(SetVideoToneActivity.this);
                    saveContactFirebase(contactItem);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void checkOthers(View view) {
        Intent i = new Intent(SetVideoToneActivity.this, DinamicCatalogActivity.class);
        startActivity(i);
        // google analytics...
        if (own != null) {
            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(
                        TrackerConstant.SCREEN_PERSONAL_VIDEO,
                        TrackerConstant.EVENT_CAT_PERSONAL_VIDEO,
                        "click other category",
                        own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * UI Threads
     * Added by Yudha Pratama Putra, 08 Okt 2018
     */
    private void showCustomDialogs(String msg) {
        if (!SetVideoToneActivity.this.isFinishing()) // skip updating view
            return;
        try {
            dialogUpload.setMessage(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialogs() {
        if (!SetVideoToneActivity.this.isFinishing()) // skip updating view
            return;
        try {
            dialogUpload = new DialogUpload();
            dialogUpload.show(getSupportFragmentManager(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dismissDialogs() {
        if (!SetVideoToneActivity.this.isFinishing()) // skip updating view
            return;
        try {
            dialogUpload.dismissAllowingStateLoss();
            dialogUpload.dismiss();
            dialogUpload = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
