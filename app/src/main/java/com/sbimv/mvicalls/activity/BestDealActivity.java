package com.sbimv.mvicalls.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.adapter.BestDealAdapter;
import com.sbimv.mvicalls.contract.MainContract;
import com.sbimv.mvicalls.interactor.IntractorImpl;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataBestDeal;
import com.sbimv.mvicalls.presenter.BestDealPresenterImpl;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class BestDealActivity extends BaseActivity implements MainContract.BestDealView {

    private RecyclerView rvBestDeal;
    private ArrayList<DataBestDeal> dataBestDeals = new ArrayList<>();
    private ProgressBar progressBar;
    private LinearLayout emptyBestdeal;
    public static final int REQUEST_CODE = 100;
    MainContract.BestDealPresenter bestDealPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_best_deal);

        // Initial View
        setInitView();
        // Initial Recycle
        initRecycler();
        // Initial Presenter
        bestDealPresenter = new BestDealPresenterImpl(this, new IntractorImpl());
        ContactItem contactItem = SessionManager.getProfile(BestDealActivity.this);
        bestDealPresenter.setDataBestDeal(contactItem,"");

    }

    private void setInitView(){
        TextView tvEmptyState = findViewById(R.id.tvEmptyState);
        tvEmptyState.setText(model.getLang().string.bestDealEmptyWording);
        setDefaultToolbar(true, model.getLang().string.bestDeals);
        progressBar = findViewById(R.id.pgBestDeal);
        rvBestDeal = findViewById(R.id.rvBestDeal);
        emptyBestdeal = findViewById(R.id.linEmpty_bestdeal);
        initViewAction();
    }

    private void initViewAction() {
        logFcb_screen_bestdealEvent();
        try{
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_BEST_DEAL);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logFcb_screen_bestdealEvent () {
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_bestdeal");
    }


    private void initRecycler() {
        LinearLayoutManager layout = new LinearLayoutManager(this);
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        rvBestDeal.setLayoutManager(layout);
        BestDealAdapter adapter = new BestDealAdapter(dataBestDeals, BestDealActivity.this);
        rvBestDeal.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE){
            if (resultCode == RESULT_OK){
                String action = data.getStringExtra("action");
                if(!TextUtils.isEmpty(action) && action.equals("rewards")){
                    finish();
                    return;
                }

                String datas = data.getStringExtra("MY_M3");
                if (datas.equals("clicked")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(BestDealActivity.this);
                    builder.setTitle(model.getLang().string.freeContent);
                    builder.setMessage(model.getLang().string.wordingGotFreeContent);
                    builder.setCancelable(true);
                    builder.setPositiveButton(model.getLang().string.goToMyCollection, (dialogInterface, i) -> {
                        startActivity(new Intent(BestDealActivity.this,UploadVideoToneActivity.class));
                        finish();
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        }
    }

    @Override
    public void isProgress() {

    }

    @Override
    public void isFinish() {
        if (!BestDealActivity.this.isFinishing()) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void isEmpty() {
        if (!BestDealActivity.this.isFinishing()) {
            rvBestDeal.setVisibility(View.GONE);
            emptyBestdeal.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void isSuccess(List<DataBestDeal> data) {
        if (!BestDealActivity.this.isFinishing()) {
            rvBestDeal.setVisibility(View.VISIBLE);
            dataBestDeals.clear();
            dataBestDeals.addAll(data);
            Objects.requireNonNull(rvBestDeal.getAdapter()).notifyDataSetChanged();
        }
    }

    @Override
    public void isFailure() {
        if (!BestDealActivity.this.isFinishing()) {
            rvBestDeal.setVisibility(View.GONE);
            emptyBestdeal.setVisibility(View.VISIBLE);
        }
    }
}
