package com.sbimv.mvicalls.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.appevents.AppEventsLogger;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.adapter.BacksoundAdapter;
import com.sbimv.mvicalls.adapter.MyCollectionAdapter;
import com.sbimv.mvicalls.http.ProgressRequestBody;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.BackSound;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.MyCollection;
import com.sbimv.mvicalls.services.SyncVideoProfileServices;
import com.sbimv.mvicalls.sync.SyncContactProfileManager;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.CustomVideoView;
import com.sbimv.mvicalls.util.FileUtil;
import com.sbimv.mvicalls.util.FileUtils;
import com.sbimv.mvicalls.util.OwnVideoManager;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.VideoSelector;
import com.sbimv.mvicalls.util.video_audio_merge.StringVideo;
import com.sbimv.mvicalls.util.video_audio_merge.UtilVideoAudio;
import com.sbimv.mvicalls.util.video_audio_merge.VideoAudioCallBack;
import com.sbimv.mvicalls.util.video_audio_merge.VideoAudioMerge;
import com.sbimv.mvicalls.util.video_compress.CompressionListener;
import com.sbimv.mvicalls.util.video_compress.VideoCompressor;
import com.sbimv.mvicalls.view.DialogUpload;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Update by Yudha Pratama Putra on 10/29/17.
 */
public class UploadVideoToneActivity extends BaseActivity implements VideoAudioCallBack, CompressionListener, View.OnTouchListener, BacksoundAdapter.OnItemClickListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener{

    // request activity
    private static final int REQUEST_VIDEO_TRIMMER = 0x02;

    // permission to read external storage
    // view bind
    private CustomVideoView videoView;

    private LinearLayout btnRecord;
    private LinearLayout btnGallery;
    private LinearLayout btnPremium;
    private LinearLayout btnUploadVideo;
    private LinearLayout btnCancel;

    private RecyclerView rvCollection;

    private DialogUpload dialogUpload;

    // variables
    private Uri trimmedUri;
    private Uri videoUriForUpload;
    private boolean isUploading;
    private ContactItem own;
    private List<MyCollection> collection = new ArrayList<>();
    private String path;
    private File file, masterVideoFile, masterAudioFile;;
    private long length;
    private boolean isScDoing = false;

    //Ffmpeg
    private StringVideo stringVideo = new StringVideo();
    private ProgressDialog progressDialog;
    UtilVideoAudio utilVideoAudio = new UtilVideoAudio();
    private String filePathVideo, stringVideo_;
    Uri sendUri;

    //Backsound
    public MediaPlayer mediaPlayer;
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    private String outputhFileName = "audio" + ".mp3";
    private UtilVideoAudio utils = new UtilVideoAudio();
    BacksoundAdapter adapter;
    private int duration = 0;
    private ImageView image;
    private List<BackSound> backSoundList = new ArrayList<>();
    AlertDialog alert;
    private int currecnPos;
    boolean isComplete = true;
    boolean onError;

    private VideoCompressor mVideoCompressor = new VideoCompressor(this);

    VideoSelector mVideoSelector = VideoSelector.create(this, new VideoSelector.VideoSelectorDelegate() {
        @Override
        public void onCaptured(final Uri selectedUri) {
            new Handler().postDelayed(() -> {
                if (selectedUri != null) {
                    startTrimActivity(selectedUri);
                    PreferenceUtil.getEditor(UploadVideoToneActivity.this).putString(PreferenceUtil.FROM_CAMERA,"camera").commit();
                }
            }, 1000);
        }

        @Override
        public void onSelected(final Uri selectedUri) {
            showDialogs();
            new Handler().postDelayed(() -> {
                dismissDialogs();

                try {
                    path = FileUtils.getRealPath(UploadVideoToneActivity.this, selectedUri);
                    file = new File(path);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    selectedButtonUpload(false);
                    String not = TextUtils.isEmpty(model.getLang().string.str_preview_video_not_support)
                            ? "File not Support"
                            : model.getLang().string.str_preview_video_not_support;
                    toast(not); // File not supported
                }

                try {
                    length = file.length();
                    length = length / 1024;

                    if (length > 40000) {
                        new AlertDialog.Builder(UploadVideoToneActivity.this).setMessage(model.getLang().string.wordingVideoTooBig).setCancelable(false).setPositiveButton("Ok", (dialog, which) -> {
                        }).show();

                    } else {
                        if (selectedUri != null) {
                            startTrimActivity(selectedUri);
                            PreferenceUtil.getEditor(UploadVideoToneActivity.this).putString(PreferenceUtil.FROM_GALLERY, "gallery").commit();
                        }
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                    selectedButtonUpload(false);
                    String not = TextUtils.isEmpty(model.getLang().string.str_preview_video_not_support)
                            ? "File not Support"
                            : model.getLang().string.str_preview_video_not_support;
                    toast(not); // File not supported
                }

            }, 1000);
        }

        @Override
        public void onCancelCapture() {
//            showCaseGallery();
        }

        @Override
        public void onCancelGallery() {
//            showCaseNext();
        }
    });
    private BroadcastReceiver downloadVideo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            doneProgress();
        }
    };
    private String h = "";
    private String w = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        com.sbimv.mvicalls.databinding.ActivityUploadVideoToneBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_upload_video_tone);
        binding.setLangModel(model);
        setDefaultToolbar(true);

//        showCaseWrapper = ShowCaseWrapper.create(UploadVideoToneActivity.this);
//        checkShowCase();

        TextView tvUpload = findViewById(R.id.tvUpload);
        tvUpload.setText(model.getLang().string.upload);
        videoView = findViewById(R.id.video_view);

        btnRecord = findViewById(R.id.btnRecord);
        btnGallery = findViewById(R.id.btnGallery);
        btnPremium = findViewById(R.id.btnPremium);
        btnUploadVideo = findViewById(R.id.btn_upload_video);
        btnCancel = findViewById(R.id.btn_cancel);

        rvCollection = findViewById(R.id.rv_collection);
        FrameLayout mediaControllerAnchor = findViewById(R.id.controller_anchor);

        try {
            own = SessionManager.getProfile(UploadVideoToneActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try{
            Intent data = getIntent();
            if (data != null){


            }else{
                selectedButtonUpload(false);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(mediaControllerAnchor);
        videoView.setMediaController(mediaController);
        videoView.setZOrderOnTop(true);

        initRecyclerView();
        loadCollection();

        try {
            // google analytics...
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        logFcb_screen_uploadvideotoneEvent();
    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logFcb_screen_uploadvideotoneEvent () {
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_uploadvideotone");
    }


//    private void checkShowCase() {
//        boolean isSCRec = AppPreference.getPreferenceBoolean(UploadVideoToneActivity.this, AppPreference.REC);
//        boolean isSCGallery = AppPreference.getPreferenceBoolean(UploadVideoToneActivity.this, AppPreference.GALLERY);
//
//        if (isSCRec) {
////            showCaseRec();
//        } else if (isSCGallery) {
////            showCaseGallery();
//        }
//    }

//    private void showCaseRec() {
//
//        AppPreference.setPreferenceBoolean(UploadVideoToneActivity.this,AppPreference.REC,false);
//        if (!UploadVideoToneActivity.this.isFinishing()) {
//            showCaseWrapper.showFancydoAction(ShowCaseWrapper.RECORD, model.getLang().string.wordingShowcaseRec, findViewById(R.id.btnRecord),
//                    new ShowCaseListener.ActionListener() {
//                        @Override
//                        public void onScreenTap() {
//                            doRecord();
//                        }
//
//                        @Override
//                        public void onOk() {
//                            doRecord();
//                        }
//
//                        @Override
//                        public void onSkip() {
////                            showCaseGallery();
//                        }
//                    });
//        }
//
//    }
//
//    private void showCaseNext() {
//        showCaseWrapper.showFancyNext(ShowCaseWrapper.ABORT_UPLOAD_GALLERY, new ShowCaseListener.NextListener() {
//            @Override
//            public void onNext() {
//                Intent intent = new Intent(UploadVideoToneActivity.this, DinamicCatalogActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//            }
//        });
//    }
//
//    private void showCaseGallery() {
////        final boolean skipped = AppPreference.getPreferenceBoolean(UploadVideoToneActivity.this, AppPreference.SKIPPED);
//        AppPreference.setPreferenceBoolean(UploadVideoToneActivity.this,AppPreference.GALLERY,false);
//        if (!UploadVideoToneActivity.this.isFinishing()) {
//            showCaseWrapper.showFancydoAction(ShowCaseWrapper.GLLERY, model.getLang().string.wordingShowCaseGallery, findViewById(R.id.btnGallery),
//                    new ShowCaseListener.ActionListener() {
//                        @Override
//                        public void onScreenTap() {
//                            intentGallery();
//                        }
//
//                        @Override
//                        public void onOk() {
//                            intentGallery();
//                        }
//
//                        @Override
//                        public void onSkip() {
////                            showCaseNext();
////                            if (!skipped) {
////
////                            } else {
////                                showCaseNext();
////                            }
//                        }
//                    });
//        }
//
//    }


    @Override
    protected void onResume() {
        super.onResume();
        showBroadcastConfig(UploadVideoToneActivity.this); // Register broadcast receiver
        LocalBroadcastManager.getInstance(UploadVideoToneActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        setMediaPlayer();
        if (image != null){
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_button));
        }
        stopBroadcastConfig(UploadVideoToneActivity.this); // Unregister broadcast receiver
        LocalBroadcastManager.getInstance(UploadVideoToneActivity.this).unregisterReceiver(popupNotifAppears);
    }

    @SuppressLint("WrongConstant")
    private void initRecyclerView() {
        LinearLayoutManager layout = new LinearLayoutManager(this);
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        int numOfColumn = 2;
        rvCollection.setLayoutManager(new GridLayoutManager(this, numOfColumn));
        MyCollectionAdapter ar = new MyCollectionAdapter(this, collection);
        rvCollection.setAdapter(ar);
    }

    private void loadCollection() {
        ContactItem owns = SessionManager.getProfile(this);
        if (owns != null) {
            String callerID = owns.caller_id;
            if (!TextUtils.isEmpty(callerID)) {
                Call<APIResponse<List<MyCollection>>> call = ServicesFactory.getService().getMyCollection(callerID);
                call.enqueue(new Callback<APIResponse<List<MyCollection>>>() {
                    @Override
                    public void onResponse(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<List<MyCollection>>> call, @org.jetbrains.annotations.NotNull @NotNull Response<APIResponse<List<MyCollection>>> response) {
                        if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                            List<MyCollection> data = response.body().data;
                            if (data != null) {
                                collection.clear();
                                collection.addAll(data);
                                Objects.requireNonNull(rvCollection.getAdapter()).notifyDataSetChanged();
                                rvCollection.setVisibility(View.VISIBLE);
                            }
                        }
                    }

                    @Override
                    public void onFailure(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<List<MyCollection>>> call, @org.jetbrains.annotations.NotNull @NotNull Throwable t) {
                        if (!isViewAttached) return;
                        toast(model.getLang().string.wordingFailureResponse);
                    }
                });
            }
        }
    }

    public void doClick(View view) {
        int id = view.getId();
        if (id == R.id.btnRecord) {

            ContactItem ctItem = SessionManager.getProfile(UploadVideoToneActivity.this);
            if(ctItem != null) {
                String strMsisdn = ctItem.msisdn;
                if (!TextUtils.isEmpty(strMsisdn)) {
                    checkSubs(strMsisdn, isSuccess -> {
                        if (!isSuccess) {
                            if (!UploadVideoToneActivity.this.isFinishing()) {

                                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                assert ccode != null;
                                if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")){
                                    startActivity(new Intent(UploadVideoToneActivity.this, PopupVietSubsActivity.class));
                                    return;
                                }

                                Intent intent = new Intent(UploadVideoToneActivity.this, PopUpSubsActivity.class);
                                startActivity(intent);
                            }
                        } else {
                            if (!UploadVideoToneActivity.this.isFinishing()) {

                                if (isUploading)
                                    return;

                                // capture video
                                mVideoSelector.capture();

                                // google analytics...
                                ContactItem owns = SessionManager.getProfile(UploadVideoToneActivity.this);
                                if (owns != null) {
                                    String strCallerId = owns.caller_id;
                                    if (!TextUtils.isEmpty(strCallerId)) {
                                        try {
                                            GATRacker.getInstance(getApplication()).sendEventWithScreen(
                                                    TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
                                                    TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
                                                    "click record", strCallerId);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                    });
                }
            }

//            if (!UploadVideoToneActivity.this.isFinishing()) {
//
//                if (isUploading)
//                    return;
//
//                // capture video
//                mVideoSelector.capture();
//
//                // google analytics...
//                ContactItem owns = SessionManager.getProfile(UploadVideoToneActivity.this);
//                if (owns != null) {
//                    String strCallerId = owns.caller_id;
//                    if (!TextUtils.isEmpty(strCallerId)) {
//                        try {
//                            GATRacker.getInstance(getApplication()).sendEventWithScreen(
//                                    TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
//                                    TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
//                                    "click record", strCallerId);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }


        }
        // Fungsi button gallery untuk akses video lokal pada gallery
        else if (id == R.id.btnGallery) {

//            if (!UploadVideoToneActivity.this.isFinishing()) {
//
//                if (isUploading)
//                    return;
//
//                // open gallery
//                mVideoSelector.gallery();
//
//                // google analytics...
//                ContactItem owns = SessionManager.getProfile(UploadVideoToneActivity.this);
//                if (owns != null) {
//                    String strCallerId = owns.caller_id;
//                    if (!TextUtils.isEmpty(strCallerId)) {
//                        try {
//                            GATRacker.getInstance(getApplication())
//                                    .sendEventWithScreen(TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
//                                            TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
//                                            "click gallery", strCallerId);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//
//            }

            ContactItem ctItem = SessionManager.getProfile(UploadVideoToneActivity.this);
            if(ctItem != null) {
                String strMsisdn = ctItem.msisdn;
                if (!TextUtils.isEmpty(strMsisdn)) {

                    checkSubs(strMsisdn, isSuccess -> {
                        if (!isSuccess) {
                            if (!UploadVideoToneActivity.this.isFinishing()) {

                                String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                                assert ccode != null;
                                if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                                    startActivity(new Intent(UploadVideoToneActivity.this, PopupVietSubsActivity.class));
                                    return;
                                }

                                Intent intent = new Intent(UploadVideoToneActivity.this, PopUpSubsActivity.class);
                                startActivity(intent);
                            }
                        } else {
                            if (!UploadVideoToneActivity.this.isFinishing()) {

                                if (isUploading)
                                    return;

                                // open gallery
                                mVideoSelector.gallery();

                                // google analytics...
                                ContactItem owns = SessionManager.getProfile(UploadVideoToneActivity.this);
                                if (owns != null) {
                                    String strCallerId = owns.caller_id;
                                    if (!TextUtils.isEmpty(strCallerId)) {
                                        try {
                                            GATRacker.getInstance(getApplication())
                                                    .sendEventWithScreen(TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
                                                            TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
                                                            "click gallery", strCallerId);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                            }
                        }
                    });

                }
            }

        }

        else if (id == R.id.btnPremium) {
            Intent i = new Intent(UploadVideoToneActivity.this, DinamicCatalogActivity.class);
            startActivity(i);
            // google analytics...
            if (own != null) {
                String strCallerId = own.caller_id;
                if (!TextUtils.isEmpty(strCallerId)) {
                    try {
                        GATRacker.getInstance(getApplication())
                                .sendEventWithScreen(TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
                                        TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
                                        "click premium content", strCallerId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        else if (id == R.id.btn_cancel){
            selectedButtonUpload(false);
        }

        // Fungsi untuk upload video setelah di trim
        else if (id == R.id.btn_upload_video) {

//            if (!UploadVideoToneActivity.this.isFinishing()) {
//                doUpload();
//            }
//
//            // google analytics...
//            ContactItem owns = SessionManager.getProfile(UploadVideoToneActivity.this);
//            if (owns != null) {
//                String strCallerId = owns.caller_id;
//                if (!TextUtils.isEmpty(strCallerId)) {
//                    try {
//                        GATRacker.getInstance(getApplication())
//                                .sendEventWithScreen(TrackerConstant.SCREEN_PERSONAL_VIDEO,
//                                        TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
//                                        "click upload video", strCallerId);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }

            String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
            assert ccode != null;
            if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                processingUpload();
                return;
            }

            boolean isShowPopUpTNC = PreferenceUtil.getPref(UploadVideoToneActivity.this).getBoolean(PreferenceUtil.POPUP_TNC, false);
            if (!isShowPopUpTNC) {

                String title = TextUtils.isEmpty(model.getLang().string.termOfService)
                        ? "Syarat & Ketentuan"
                        : model.getLang().string.termOfService;

                String msg = TextUtils.isEmpty(model.getLang().string.msgBeforeUpload)
                        ? "Pengguna / pengunduh menjamin bahwa video yang di unduh merupakan miliknya, memperoleh izin untuk menggunakan " +
                        "/ mengunduh video ini. Penguna / pengunduh menjamin bahwa video yang diunduh tidak mengandung " +
                        "materi yang bertentangan dengan hukum serta membebaskan pihak Mvicall " +
                        "dari semua tuntutan dalam bentuk apapun di kemudian hari terkait video yang di unduh oleh penguna / pengunduh"
                        : model.getLang().string.msgBeforeUpload;

                String btnPositive = TextUtils.isEmpty(model.getLang().string.wordingAgree) ? "Setuju" : model.getLang().string.wordingAgree;
                String btnNegative = TextUtils.isEmpty(model.getLang().string.cancelWording) ? "Batal" : model.getLang().string.cancelWording;

                try {
                    showAlertUpload(title, msg, btnPositive, btnNegative);
                } catch (Exception e) {
                    e.printStackTrace();
                    processingUpload();
                }

            }else {
                processingUpload();
            }

        }
    }

    @SuppressLint("WrongConstant")
    private void showAlertUpload(String title, String msg, String btnPositive, String btnNegative) throws Exception{ //Show alert upload...

        if (!UploadVideoToneActivity.this.isFinishing()) {

            final TextView myView = new TextView(getApplicationContext());
            myView.setText(msg);
            myView.setTextAlignment(2);
            myView.setPadding(45,5,45,5);
            myView.setTextSize(12);

            new androidx.appcompat.app.AlertDialog.Builder(UploadVideoToneActivity.this)
                    .setTitle(title)
                    .setView(myView)
                    .setPositiveButton(btnPositive, (dialog, which) -> {
                        if (!UploadVideoToneActivity.this.isFinishing()) {
                            try {
                                PreferenceUtil.getEditor(UploadVideoToneActivity.this).putBoolean(PreferenceUtil.POPUP_TNC, true).commit();
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            processingUpload();
                        }
                    }).setNegativeButton(btnNegative, (dialog, which) -> {
                        if (!UploadVideoToneActivity.this.isFinishing()) {
                            try {
                                dialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).show();
        }

    }

    private void processingUpload(){ // Processing Upload...
        ContactItem ctItem = SessionManager.getProfile(UploadVideoToneActivity.this);
        if(ctItem != null) {
            String strMsisdn = ctItem.msisdn;
            if (!TextUtils.isEmpty(strMsisdn)) {
                checkSubs(strMsisdn, isSuccess -> {

                    if (!isSuccess) {
                        if (!UploadVideoToneActivity.this.isFinishing()) {

                            String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                            assert ccode != null;
                            if (!TextUtils.isEmpty(ccode) && ccode.equals("+84")){
                                startActivity(new Intent(UploadVideoToneActivity.this, PopupVietSubsActivity.class));
                                return;
                            }

                            Intent intent = new Intent(UploadVideoToneActivity.this, PopUpSubsActivity.class);
                            startActivity(intent);
                        }
                    } else {
                        if (!UploadVideoToneActivity.this.isFinishing()) {
                            doUpload();
                        }
                    }

                    // google analytics...
                    ContactItem owns = SessionManager.getProfile(UploadVideoToneActivity.this);
                    if (owns != null) {
                        String strCallerId = owns.caller_id;
                        if (!TextUtils.isEmpty(strCallerId)) {
                            try {
                                GATRacker.getInstance(getApplication())
                                        .sendEventWithScreen(TrackerConstant.SCREEN_PERSONAL_VIDEO,
                                                TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
                                                "click upload video", strCallerId);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                });
            }
        }
    }


    private void doUpload() {
        // stop video player
        if (videoView.isPlaying()) {
            videoView.stopPlayback();
            videoView.seekTo(100);
        }

        showDialogs();

        try {
            compressVideo();
        } catch (Exception e) {
            toast(model.getLang().string.errorFetchingData);
            dismissDialogs();
        }
        // google analytics...
        if (own != null) {
            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(
                        TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
                        TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
                        "click upload",
                        own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

//    private void intentGallery() {
//        mVideoSelector.gallery();
//        isScDoing = true;
//        // google analytics...
//        if (own != null) {
//            GATRacker.getInstance(getApplication()).sendEventWithScreen(
//                    TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
//                    TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
//                    "click gallery",
//                    own.caller_id);
//        }
//
//    }
//
//    private void doRecord() {
//
//        // eksekusi method camera intent
//        mVideoSelector.capture();
//        isScDoing = true;
//        // google analytics...
//        if (own != null) {
//            GATRacker.getInstance(getApplication()).sendEventWithScreen(
//                    TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
//                    TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
//                    "click record",
//                    own.caller_id);
//        }
//    }

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {

        if (requestCode == REQUEST_VIDEO_TRIMMER && resultCode == RESULT_OK) {
            if (data.getData() != null){
                showDialogs();
                new Handler().postDelayed(() -> {
                    if (isViewAttached) {
                        dismissDialogs();
                        selectedButtonUpload(true);
                        trimmedUri = data.getData();

                        sendUri = data.getData();
                        stringVideo_ = String.valueOf(sendUri);

                        videoView.setVideoURI(trimmedUri);
                        videoView.setZOrderOnTop(true);
                        videoView.start();

                        masterVideoFile = new File(trimmedUri.getPath());
                        filePathVideo = String.valueOf(masterVideoFile);

                        videoView.setOnErrorListener((mediaPlayer, i, i1) -> {
                            onError = true;
                            showAlertErrorPlay();
                            showAlert(false);
                            return true;
                        });

                        if (onError){
                            showAlert(false);
                        }else{
                            showAlert(true);
                        }

                    }
                }, 1000);
            }
        } else {
            mVideoSelector.onActivityResult(requestCode, resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showAlertErrorPlay(){
        if (!UploadVideoToneActivity.this.isFinishing()) {

            String sTitle = TextUtils.isEmpty(model.getLang().string.str_cannot_play)
                    ? "Can't play this video."
                    : model.getLang().string.str_cannot_play;

            String sApprove = TextUtils.isEmpty(model.getLang().string.approveCofirm)
                    ? "Ok"
                    : model.getLang().string.approveCofirm;

            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setMessage(sTitle)
                    .setCancelable(false)
                    .setPositiveButton(sApprove, (dialog, which) -> {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).show();
        }
    }

    private void selectedButtonUpload(boolean isSelected){
        if (!UploadVideoToneActivity.this.isFinishing()) {
            if (isSelected) {
                try {
                    btnRecord.setVisibility(View.GONE);
                    btnGallery.setVisibility(View.GONE);
                    btnPremium.setVisibility(View.GONE);
                    btnUploadVideo.setVisibility(View.VISIBLE);
                    btnCancel.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    btnRecord.setVisibility(View.VISIBLE);
                    btnGallery.setVisibility(View.VISIBLE);
                    btnPremium.setVisibility(View.VISIBLE);
                    btnUploadVideo.setVisibility(View.GONE);
                    btnCancel.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void startTrimActivity(@NonNull final Uri uri) {
        Intent intent = new Intent(UploadVideoToneActivity.this, TrimmingActivity.class);
        intent.putExtra("path", uri);
        startActivityForResult(intent, REQUEST_VIDEO_TRIMMER);
        videoView.stopPlayback();
    }

    private void compressVideo(){

        try{
            if (trimmedUri != null){
                selectCompressVideo(trimmedUri);
            }else {
                selectCompressVideo(videoUriForUpload);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void selectCompressVideo(Uri uri){

        String inPath = uri.getPath();
        String outPath = getCacheDir().getPath() + File.separator + "mvicall_compressed_video.mp4";

        if(!TextUtils.isEmpty(inPath) && !TextUtils.isEmpty(outPath)){

            try {
                MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                retriever.setDataSource(inPath);
                h = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
                w = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
            }catch (Exception e){
                e.printStackTrace();
            }

            VideoCompressor.Companion.with(getApplicationContext())
                    .setInputPath(inPath)
                    .setOutputPath(outPath)
                    .setCallBack(UploadVideoToneActivity.this)
                    .compressVideo(w, h);

        }
    }

    private void uploadVideoFile(final String videoPath) {
        isUploading = true;
        // Progress listener
        ProgressRequestBody.ProgressListener progressListener = (num, transferred, totalSize) -> {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> {
                if (!isViewAttached)
                    return;
                showCustomDialog(model.getLang().string.uploading + " " + num + "%");
            });
        };

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part _file = null;
        File file = new File(videoPath);
        // init request body
        ProgressRequestBody requestFileBody = new ProgressRequestBody(file, "multipart/form-data", progressListener);
        _file = MultipartBody.Part.createFormData("video", file.getName(), requestFileBody);
        // set request body
        RequestBody _caller_id = RequestBody.create(MediaType.parse("text/plain"), own.caller_id);

        Call<APIResponse> call = ServicesFactory.getService().uploadVideo(_file, _caller_id);
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse> call, @org.jetbrains.annotations.NotNull @NotNull Response<APIResponse> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    isUploading = false;
                    // move file on upload success
                    try {
                        SharedPreferences sharedPreferences = getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(ConfigPref.SET_CONTENT, false);
                        editor.putString(ConfigPref.URL_BUY, "");
                        editor.apply();

                        File source = new File(videoPath);
                        File dest = new File(OwnVideoManager.getOwnVideoUri(UploadVideoToneActivity.this).getPath());
                        FileUtil.moveFile(source, dest);
                        oauthToken(own.msisdn);

                        // Google analytics
                        if (own != null) {
                            try {
                                GATRacker.getInstance(getApplication()).sendEventWithScreen(
                                        TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
                                        TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
                                        "upload video success",
                                        own.caller_id);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    isUploading = false;
                    toast(model.getLang().string.uploadVideoFailed); // Upload failed
                    dismissDialogs();
                    selectedButtonUpload(false);
                    // Google analytics
                    if (own != null) {
                        try {
                            GATRacker.getInstance(getApplication()).sendEventWithScreen(
                                    TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
                                    TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
                                    "upload video failed",
                                    own.caller_id);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse> call, @org.jetbrains.annotations.NotNull @NotNull Throwable t) {
                if (!isViewAttached)
                    return;
                try {
                    isUploading = false;
                    dismissDialogs();
                    selectedButtonUpload(false);
                } catch (Exception e) {
                    return;
                }
                toast(model.getLang().string.uploadVideoFailed);
                // Google analytics
                if (own != null) {
                    try {
                        GATRacker.getInstance(getApplication()).sendEventWithScreen(
                                TrackerConstant.SCREEN_UPLOAD_PERSONAL_VIDEO,
                                TrackerConstant.EVENT_CAT_UPLOAD_PERSONAL_VIDEO,
                                "upload video failed",
                                own.caller_id);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void oauthToken(String msisdn) {
        Call<APIResponse<ContactItem>> call = ServicesFactory.getService().getProfile(msisdn);
        call.enqueue(new Callback<APIResponse<ContactItem>>() {
            @Override
            public void onResponse(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<ContactItem>> call, @org.jetbrains.annotations.NotNull @NotNull Response<APIResponse<ContactItem>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {

                    String ccode = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.COUNTRY_CODE, "1");
                    assert ccode != null;
                    if (!TextUtils.isEmpty(ccode) && Objects.equals(ccode, "+84")){
                        showAlertCuration();
                    }else {
                        if (!SyncContactProfileManager.getInstance(getApplicationContext()).isSynchronizing()) {

                            ContactItem item = response.body().data;
                            if (item != null) {
                                item.profiles = null;// Untuk syncronize firebase
                                SessionManager.saveProfile(UploadVideoToneActivity.this, item);
                                Intent i = new Intent(UploadVideoToneActivity.this, SyncVideoProfileServices.class);
                                startService(i);
                            }

                        }
                    }

//                    if (!SyncContactProfileManager.getInstance(getApplicationContext()).isSynchronizing()) {
//                        Intent i = new Intent(UploadVideoToneActivity.this, SyncVideoProfileServices.class);
//                        startService(i);
//                    }

                } else {
                    toast(model.getLang().string.authenticationFailed);
                }
            }

            @Override
            public void onFailure(@org.jetbrains.annotations.NotNull @NotNull Call<APIResponse<ContactItem>> call, @org.jetbrains.annotations.NotNull @NotNull Throwable t) {
                if (!isViewAttached)
                    return;
                toast(model.getLang().string.authenticationFailed);
            }
        });
    }

    private void showAlertCuration(){
        if (!UploadVideoToneActivity.this.isFinishing()) {

            String sCuration = TextUtils.isEmpty(model.getLang().string.wordingCuration)
                    ? "Video dalam proses review tim kami"
                    : model.getLang().string.wordingCuration;

            String sApprove = TextUtils.isEmpty(model.getLang().string.approveCofirm)
                    ? "Ok"
                    : model.getLang().string.approveCofirm;

            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setMessage(sCuration)
                    .setPositiveButton(sApprove, (dialog, which) -> {
                        try {
                            dialog.dismiss();
                            dismissDialogs();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        finish();
                    }).show();

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.UPLOAD_VIDEO");
        LocalBroadcastManager.getInstance(UploadVideoToneActivity.this).registerReceiver(downloadVideo, _if);
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(UploadVideoToneActivity.this).unregisterReceiver(downloadVideo);
    }

    public void doneProgress() {
        isUploading = false;
        if (dialogUpload != null) {
            try {
                dismissDialogs();
            } catch (Exception e) {
                return;
            }
            setResult(RESULT_OK);
            if (isScDoing) {
                PreferenceUtil
                        .getEditor(UploadVideoToneActivity.this)
                        .putBoolean(PreferenceUtil.SHOW_CASE_UPLOAD_VIDEO, true)
                        .commit();
                isScDoing = false;
            }
            finish();
        } else {
            Intent il = new Intent("com.sbimv.mvicalls.UPLOAD_DONE");
            LocalBroadcastManager.getInstance(UploadVideoToneActivity.this).sendBroadcast(il);
            if (isScDoing) {
                PreferenceUtil
                        .getEditor(UploadVideoToneActivity.this)
                        .putBoolean(PreferenceUtil.SHOW_CASE_UPLOAD_VIDEO, true)
                        .commit();
                isScDoing = false;
            }
        }
    }

    /**
     * UI Threads
     * Added by Yudha Pratama Putra, 08 Okt 2018
     */
    private void showCustomDialog(String msg) {
        if (!isViewAttached) // skip updating view
            return;
        try {
            dialogUpload.setMessage(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialogs() {
        if (!isViewAttached) // skip updating view
            return;
        try {
            dialogUpload = new DialogUpload();
            dialogUpload.show(getSupportFragmentManager(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dismissDialogs() {
        if (!isViewAttached) // skip updating view
            return;
        try {
            dialogUpload.dismiss();
            dialogUpload = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void compressionFinished(int status, boolean isVideo, String fileOutputPath) {
        if (mVideoCompressor.isDone()){
            String ready = TextUtils.isEmpty(model.getLang().string.str_preview_video_ready_upload)
                    ? "Ready to Upload"
                    : model.getLang().string.str_preview_video_ready_upload;
            showCustomDialog(ready);
            uploadVideoFile(fileOutputPath);
        }
    }

    @Override
    public void onFailure(String message) {
        if (!UploadVideoToneActivity.this.isFinishing()) {
            try {
                dialogUpload.dismiss();
                selectedButtonUpload(false);
                String not = TextUtils.isEmpty(model.getLang().string.str_preview_video_not_support)
                        ? "File not Support"
                        : model.getLang().string.str_preview_video_not_support;
                toast(not); // File not supported
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onProgress(int progress) {
        String preap = TextUtils.isEmpty(model.getLang().string.str_preview_video_preaparing)
                ? "Preparing..."
                : model.getLang().string.str_preview_video_preaparing;
        showCustomDialog(preap);
    }

    //FFmpeg
    private void stopRunningProcess(){
        FFmpeg.getInstance(getApplicationContext()).killRunningProcesses();
    }

    private Boolean isRunning(){
        return FFmpeg.getInstance(this).isFFmpegCommandRunning();
    }

    @Override
    public void onProgress(String progress) {
        Log.e("TAG", "Running " + progress);

    }

    @Override
    public void onSuccess(File file, String success) {

        String strWordingMerge = TextUtils.isEmpty(model.getLang().string.titleSuccessMerge)
                ?"Success Merge" :
                model.getLang().string.titleSuccessMerge;

        Toast.makeText(this, strWordingMerge, Toast.LENGTH_SHORT).show();
        if (success.equals(stringVideo.TYPE_VIDEO)){
            videoUriForUpload = Uri.fromFile(file);
            videoView.setVideoURI(Uri.fromFile(file));
            videoView.setZOrderOnTop(true);
            videoView.start();
        }
    }

    @Override
    public void onFailure(Exception exception) {
        exception.printStackTrace();
        Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
        Log.e("FAILURE", Objects.requireNonNull(exception.getMessage()));
        dismissLoading();
    }

    @Override
    public void onNotAvailable(Exception notAvailable) {
        Toast.makeText(this, notAvailable.getMessage(), Toast.LENGTH_SHORT).show();
        Log.e("NOT AVAILABLE", Objects.requireNonNull(notAvailable.getMessage()));
        dismissLoading();
    }

    @Override
    public void onFinish() {
        dismissLoading();
    }

    public void showLoading(String message)
    {
        progressDialog = new ProgressDialog(UploadVideoToneActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void dismissLoading(){
        try{
            progressDialog.dismiss();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //*
    // Show popup bacskound
    //*
    private void showBacksoundList(){
        if (!UploadVideoToneActivity.this.isFinishing()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            @SuppressLint("InflateParams") View v = getLayoutInflater().inflate(R.layout.popup_backsound, null);
            builder.setView(v);
            builder.setCancelable(true);

            RecyclerView recyclerBacksound = v.findViewById(R.id.recyclerBacksound);
            ImageView ivClose = v.findViewById(R.id.ivClose);

            alert = builder.create();
            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alert.setCanceledOnTouchOutside(false);

            ivClose.setOnClickListener(view -> {
                alert.dismiss();
                setMediaPlayer();
            });

            alert.setOnCancelListener(dialogInterface -> {
                alert.dismiss();
                setMediaPlayer();
            });

            initPlayaer();
            initRecycler(recyclerBacksound);
            loadBacksound(recyclerBacksound);

            if (!alert.isShowing()) {
                alert.show();
            } else {
                alert.dismiss();
            }
        }
    }

    //*
    // Init Recycler
    //*
    private void initRecycler(RecyclerView recyclerView) {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        adapter = new BacksoundAdapter(UploadVideoToneActivity.this, backSoundList, this);
        recyclerView.setAdapter(adapter);
    }

    //*
    // Calling bakcsound list from API
    //*
    private void loadBacksound(RecyclerView recyclerView){
        if (own != null){
            String sMsisdn = TextUtils.isEmpty(own.msisdn) ? "" : own.msisdn;
            Call<APIResponse<List<BackSound>>> call = ServicesFactory.getService().getBackSound(sMsisdn);
            call.enqueue(new Callback<APIResponse<List<BackSound>>>() {
                @Override
                public void onResponse(@org.jetbrains.annotations.NotNull Call<APIResponse<List<BackSound>>> call, @org.jetbrains.annotations.NotNull Response<APIResponse<List<BackSound>>> response) {
                    assert response.body() != null;
                    if (response.isSuccessful() && response.body().isSuccessful()){
                        backSoundList = response.body().data;
                        if (backSoundList != null && backSoundList.size() != 0){
                            Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                            initRecycler(recyclerView);
                        }
                    }
                }

                @Override
                public void onFailure(@org.jetbrains.annotations.NotNull Call<APIResponse<List<BackSound>>> call, @org.jetbrains.annotations.NotNull Throwable t) {
                    String s_failure = TextUtils.isEmpty(model.getLang().string.wordingFailureResponse)
                            ? "Failure Fetching Data"
                            : model.getLang().string.wordingFailureResponse;
                    Toast.makeText(UploadVideoToneActivity.this, s_failure, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    //*
    // Merging Video With Audio
    //*
    private void forMerge(){
        stopRunningProcess();

        String outputhFileName = "merged_mvicall" + ".mp4";

        if (masterAudioFile != null && masterVideoFile != null){
            if (!isRunning()) {
                VideoAudioMerge.Companion.with(getApplicationContext())
                        .setAudioFile(masterAudioFile)
                        .setVideoFile(masterVideoFile)
                        .setOutputPath(utilVideoAudio.getOutputPath() + "video")
                        .setOutputFileName(outputhFileName)
                        .setVideoAudioCallBack(UploadVideoToneActivity.this)
                        .merge();

                String wait = TextUtils.isEmpty(model.getLang().string.str_preview_video_please_wait)
                        ? "Please Wait..."
                        : model.getLang().string.str_preview_video_please_wait;
                showLoading(wait);
            } else {
                Toast.makeText(UploadVideoToneActivity.this, "Operation already in progress! Try again in a while.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //*
    // Alert Confirm Adding backsound
    //*
    private void showAlert(boolean isShow){

        if (isShow){
            String strWordingAddingMusic = TextUtils.isEmpty(model.getLang().string.titleAddMusic)
                    ?"Tambahkan music ke video anda?" :
                    model.getLang().string.titleAddMusic;

            String strWordingYes = TextUtils.isEmpty(model.getLang().string.titleYes)
                    ?"Ya" :
                    model.getLang().string.titleYes;

            String strWordingNo = TextUtils.isEmpty(model.getLang().string.titleNo)
                    ?"Tidak" :
                    model.getLang().string.titleNo;

            AlertDialog.Builder builder1 = new AlertDialog.Builder(UploadVideoToneActivity.this);
            builder1.setTitle("MVICALL");
            builder1.setMessage(strWordingAddingMusic);
            builder1.setCancelable(false);
            builder1.setIcon(getResources().getDrawable(R.drawable.logo_rebranding));

            builder1.setPositiveButton(
                    strWordingYes,
                    (dialog, id) -> actionToMusic());

            builder1.setNegativeButton(
                    strWordingNo,
                    (dialog, id) -> dialog.cancel());

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }

    }

    //*
    // Show popup bacskound
    //*
    private void actionToMusic(){
        try{
            if (filePathVideo != null && stringVideo_ != null){
                if (videoView != null){
                    if (videoView.isPlaying()) {
                        videoView.pause();
                    }
                }
                showBacksoundList();
            }else{
                Toast.makeText(this, "Choose Video", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {

    }

    //*
    // Flag if media player finish playing
    //*
    @Override
    public void onCompletion(MediaPlayer mediaPlayer2) {
        if (mediaPlayer != null && image != null){

            image.setImageResource(R.drawable.ic_play_button);

            duration = 0;
            mediaPlayer.release();
            mediaPlayer = null;

            adapter.refreshAdapter();
            adapter.notifyDataSetChanged();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }

    //*
    // Backsound click event
    //*
    @Override
    public void onItemPlayMusic(BackSound backSound, ImageView imageView, int previousPosition, int currentPosition) {
        currecnPos = currentPosition;
        image = imageView;
        if (mediaPlayer != null){
            if (mediaPlayer.isPlaying()){
                duration = mediaPlayer.getCurrentPosition();
                if (previousPosition != currentPosition){
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    duration = 0;
                }else {
                    mediaPlayer.pause();
                    mediaPlayer.reset();
                    return;
                }
            }else{
                if (previousPosition != currentPosition){
                    duration = 0;
                }
            }

            try {
                mediaPlayer.setDataSource(backSound.getSound());
                mediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else{
            initPlayaer();
            if (mediaPlayer.isPlaying()){
                duration = mediaPlayer.getCurrentPosition();
                if (previousPosition != currentPosition){
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    duration = 0;
                }else {
                    mediaPlayer.pause();
                    mediaPlayer.reset();
                    return;
                }
            }else{
                if (previousPosition != currentPosition){
                    duration = 0;
                }
            }

            try {
                mediaPlayer.setDataSource(backSound.getSound());
                mediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    //*
    // Download backsound for merging with video
    //*
    @Override
    public void onItemChoiceMusic(BackSound backSound) {
        new DownloadFileFromURL().execute(backSound.getSound());
    }


    //Download from URL
    @SuppressLint("StaticFieldLeak")
    private class DownloadFileFromURL extends AsyncTask<String, String, String> {
        protected  void onPreExecute(){
            super.onPreExecute();
            try{
                showDialog(progress_bar_type);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                int lenghOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                @SuppressLint("SdCardPath") OutputStream output = new FileOutputStream( utils.getOutputPath() + outputhFileName);
                byte[] data = new byte[1024];
                long total = 0;

                while ((count = input.read(data)) != -1){
                    total += count;
                    publishProgress(""+(int)((total*100)/lenghOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e){
                Log.e("Error : ", e.getMessage());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {
            try{
                if (!UploadVideoToneActivity.this.isFinishing()) {
                    pDialog.setProgress(Integer.parseInt(progress[0]));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        protected void onPostExecute(String file_url){
            try{
                dismissDialog(progress_bar_type);

                String audioPath = utils.getOutputPath() + outputhFileName;

                if (alert != null){
                    if (filePathVideo != null && stringVideo_ != null){
                        trimmedUri = null;
                        masterAudioFile = new File(audioPath);
                        masterVideoFile = new File(filePathVideo);
                        Log.e("AUDIO PATH", audioPath);
                        Log.e("VIDEO PATH", filePathVideo);
                        forMerge();

                        if (alert.isShowing()){
                            alert.dismiss();
                        }

                        selectedButtonUpload(true);

                        videoView.setVideoURI(Uri.parse(stringVideo_));
                        videoView.setZOrderOnTop(true);
                        videoView.start();

                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    //*
    // Initial Media Player
    //*
    private void initPlayaer(){
        try{
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mediaPlayer.setOnBufferingUpdateListener(this);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnPreparedListener(mediaPlayer -> tooglePlayPause());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //*
    // Pause Start Media Player
    //*
    private void tooglePlayPause(){
        try{
            if(mediaPlayer != null){
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                }else{
                    mediaPlayer.seekTo(duration);
                    mediaPlayer.start();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //*
    // Release Media Player to be null
    //*
    private void setMediaPlayer(){
        try{
            if (mediaPlayer != null){
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                }
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //*
    // Dialog Download Progress
    //*
    protected Dialog onCreateDialog(int id){

        String titleDownload = TextUtils.isEmpty(model.getLang().string.titleDownload)
                ? "Downloading file, Please Wait"
                : model.getLang().string.titleDownload;

        if (id == progress_bar_type) {
            try {
                pDialog = new ProgressDialog(this);
                pDialog.setMessage(titleDownload);
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setMediaPlayer();
    }
}