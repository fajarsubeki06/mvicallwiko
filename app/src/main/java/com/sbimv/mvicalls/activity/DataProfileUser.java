package com.sbimv.mvicalls.activity;


import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.adapter.AdataProfile;
import com.sbimv.mvicalls.db.ContactDBManager;

public class DataProfileUser extends BaseActivity implements TextWatcher {

    ListView mListView;
    EditText mEtSearch;
    ImageView mBtnClearText;
    private String mSearchTerm;

    private ContactDBManager contactDBManager;
    private Cursor cursor;
    private AdataProfile adataProfile;
    private TextView tvHint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_profile_user);
        setDefaultToolbar(true);

//        tvHint.setHint(model.getLang().string.search);
        mEtSearch = findViewById(R.id.et_search);
        mBtnClearText = findViewById(R.id.btn_clear_text);
        mListView = findViewById(R.id.lv_contacts);
        mListView.setClickable(true);

        try{
            contactDBManager = new ContactDBManager(getApplicationContext());
            cursor = contactDBManager.getMVContactAll();
        }catch (Exception e){
            toast("Database error...");
            return;
        }

        adataProfile = new AdataProfile(DataProfileUser.this,  cursor, 0);
        mListView.setAdapter(adataProfile);

        mBtnClearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEtSearch.setText("");
            }
        });
        mEtSearch.addTextChangedListener(this);

    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        String newFilter = !TextUtils.isEmpty(charSequence) ? charSequence.toString() : "";
        if (mSearchTerm == null && newFilter == null) {
            return;
        }
        if (mSearchTerm != null && mSearchTerm.equals(newFilter)) {
            return;
        }
        mSearchTerm = newFilter;
        cursor=contactDBManager.getCriteriaContact(mSearchTerm);
        adataProfile.swapCursor(cursor);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}

