package com.sbimv.mvicalls.activity;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.contract.MainContract;
import com.sbimv.mvicalls.fragment.FAQFragment;
import com.sbimv.mvicalls.fragment.TutorialFragment;
import com.sbimv.mvicalls.interactor.IntractorImpl;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.presenter.HowToUsePresenterImpl;
import com.sbimv.mvicalls.util.PreferenceUtil;

import java.util.ArrayList;
import java.util.List;


public class HowToUseActivity extends BaseActivity implements MainContract.HowToUseView, View.OnClickListener{

    private ViewPager viewPager;
    private CoordinatorLayout layoutHowto;
    private LinearLayout layoutNoConnection;
    private ProgressBar pgHowTo;
    private YouTubePlayerView youtubePlayerView;
    private TabLayout tabLayout;
    private Button btnReconnect;

    private LangViewModel modelLang;
    MainContract.HowToUsePresenter howToUsePresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_use);
        setInitView();
    }

    private void setInitView(){

        // Initial multilang
        modelLang = LangViewModel.getInstance();

        // Initial view
        setDefaultToolbar(true, modelLang.getLang().string.howTo);
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        tabLayout = findViewById(R.id.tabHowto);
        btnReconnect = findViewById(R.id.btnReConnect);
        youtubePlayerView = findViewById(R.id.videoHowto);
        viewPager = findViewById(R.id.vpHowto);
        layoutHowto = findViewById(R.id.layoutHowto);
        layoutNoConnection = findViewById(R.id.layoutNoConnection);
        pgHowTo = findViewById(R.id.pgHowTo);

        // Initial presenter
        howToUsePresenter = new HowToUsePresenterImpl(this, new IntractorImpl());
        String lang = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
        howToUsePresenter.setDataHowToUse(lang);

        // Initial view action
        initViewAction(appBarLayout);


    }

    private void initViewAction(AppBarLayout appBarLayout){
        btnReconnect.setOnClickListener(this);
        getLifecycle().addObserver(youtubePlayerView);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            private State state;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == 0) {
                    if (state != State.EXPANDED) {
                        Log.d("TAG", "Expanded");
                    }
                    state = State.EXPANDED;
                } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                    if (state != State.COLLAPSED) {
                        Log.d("TAG", "Collapsed");
                        youtubePlayerView.initialize(youTubePlayer -> youTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                            @Override
                            public void onReady() {
                                try {
                                    youTubePlayer.pause();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }), true);
                    }
                    state = State.COLLAPSED;
                } else {
                    if (state != State.IDLE) {
                        Log.d("TAG", "Idle");
                    }
                    state = State.IDLE;
                }
            }
        });

        GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_HOW_TO);
    }

    @Override
    public void onClick(View view) {
        if (btnReconnect == view){
            pgHowTo.setVisibility(View.VISIBLE);
            layoutNoConnection.setVisibility(View.GONE);
            new Handler().postDelayed(() -> {
                String lang = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
                howToUsePresenter.setDataHowToUse(lang);
            },2000);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (youtubePlayerView != null) {
            youtubePlayerView.release();
        }
    }

    @Override
    public void isProgress() {

    }

    @Override
    public void isFinish() {
        if (!HowToUseActivity.this.isFinishing()) {
            layoutHowto.setVisibility(View.VISIBLE);
            pgHowTo.setVisibility(View.GONE);
            layoutNoConnection.setVisibility(View.GONE);
        }
    }

    @Override
    public void isEmpty() {
        if (!HowToUseActivity.this.isFinishing()) {
            pgHowTo.setVisibility(View.GONE);
            layoutNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void isSuccess(String videoId, String urlHowToUse, String urlFaq) {
        if (!HowToUseActivity.this.isFinishing()) {
            setupViewPager(viewPager, urlHowToUse, urlFaq);
            tabLayout.setupWithViewPager(viewPager);
            initYoutubePlayer(videoId);
        }
    }

    @Override
    public void isFailure() {
        if (!HowToUseActivity.this.isFinishing()) {
            pgHowTo.setVisibility(View.GONE);
            layoutNoConnection.setVisibility(View.VISIBLE);
        }
    }

    private void initYoutubePlayer(String videoId) {
        youtubePlayerView.initialize(initializedYouTubePlayer -> initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady() {
                if (!HowToUseActivity.this.isFinishing()) {
                    try {
                        initializedYouTubePlayer.cueVideo(videoId, 0);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }), true);
    }

    private enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    private void setupViewPager(ViewPager viewPagers, String urlHowToUse, String urlFaq) {
        try {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
            Bundle bundle = new Bundle();
            bundle.putString("urlHowToUse", urlHowToUse);
            bundle.putString("urlFaq", urlFaq);

            TutorialFragment tutorialFragment = new TutorialFragment();
            FAQFragment faqFragment = new FAQFragment();

            tutorialFragment.setArguments(bundle);
            faqFragment.setArguments(bundle);

            String titleTutorial = TextUtils.isEmpty(modelLang.getLang().string.titleTutorial)
                    ? ""
                    : modelLang.getLang().string.titleTutorial;

            String titleFAQ = TextUtils.isEmpty(modelLang.getLang().string.titleFAQ)
                    ? ""
                    : modelLang.getLang().string.titleFAQ;

            adapter.addFragment(tutorialFragment, titleTutorial);
            adapter.addFragment(faqFragment, titleFAQ);
            viewPagers.setAdapter(adapter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> titleList = new ArrayList<>();

        private ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

        private void addFragment(Fragment fragment, String title){
            fragmentList.add(fragment);
            titleList.add(title);
        }
    }


}