package com.sbimv.mvicalls.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import androidx.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.apprtc.OutgoingScreenActivity;
import com.sbimv.mvicalls.apprtc.SinchService;
import com.sbimv.mvicalls.databinding.ActivityContactTabBinding;
import com.sbimv.mvicalls.db.ContactDBManager;
import com.sbimv.mvicalls.fragment.ContactMVICallListFragment;
import com.sbimv.mvicalls.fragment.InviteFriendsFragment;
import com.sbimv.mvicalls.fragment.MemberGetMemberFragment;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.services.SyncFirebaseService;
import com.sbimv.mvicalls.sync.SyncUtils;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseListener;
import com.sbimv.mvicalls.util.ShowCaseWrapper;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

//import com.sbimv.mvicalls.services.SyncFirebaseService;

/**
 * Created by Yudha Pratama Putra on 11/6/17.
 */

public class ContactTabActivity extends BaseActivity implements ContactMVICallListFragment.OnCallDataClickedCallListner, SinchService.StartFailedListener {

    public static int RESULT_CODE = 101;
    public static String STR_INTENT_DATA = "FINISH_SC";
    public static String INVITE_FRIENDS = "INVITE";
    public final int REQUEST_PERMISSION = 789;
    // bind view
    private ViewPager viewPager;
    private TextView mBtnFriend;
    private TextView mBtnInvite;
    private MenuItem sync;
    private ProgressDialog progressDialog;
    // variables
//    private ContactPagerAdapter mPagerAdapter;
    private LinearLayout lnContactTab;
    private LinearLayout lnLdTab;
    private boolean isSyncronizing;
    private ContactItem own;
    private ShowCaseWrapper showCaseWrapper;
    private int tabInvite;
    private Cursor cursor;
    private ContactDBManager db;
    private ActivityContactTabBinding binding;
    private TextView tvSync;
    private TabLayout tabLayout;
    protected ProgressBar progressPercentBar;
    private TextView tvPercent;


//    private DatabaseReference mDatabaseContact; // Firebase...
//    private ChildEventListener contactSeenListener;
    private BroadcastReceiver showLoadingBR = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (context != null && intent != null) {

                String strFrom = intent.getStringExtra("from");
                if (!TextUtils.isEmpty(strFrom)){

                    if (strFrom.equals("percent_progress")) {
                        int percentProgress = intent.getIntExtra("percent", 0);
                        progressPercentBar.setProgress(percentProgress);
                        String strPercent = String.valueOf(percentProgress);
                        if (!TextUtils.isEmpty(strPercent)){
                            tvPercent.setText(strPercent);
                        }
                    }

                }else {

                    if (isViewAttached) {
                        lnLdTab.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    };
    private BroadcastReceiver hideLoadingBR = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!ContactTabActivity.this.isFinishing()) {
                try {
                    lnLdTab.setVisibility(View.GONE);
                    progressPercentBar.setProgress(0);
                    tvPercent.setText("0");
                    String complete = TextUtils.isEmpty(model.getLang().string.str_friends_sync_complete)
                            ? "Syncronization is complete"
                            : model.getLang().string.str_friends_sync_complete;
                    showSnackBar(getWindow().getDecorView(), complete);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(ContactTabActivity.this, R.layout.activity_contact_tab);
        binding.setLangModel(model);
        setDefaultToolbar(true);
        showCaseWrapper = ShowCaseWrapper.create(ContactTabActivity.this);


        try {
            GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_CONTACT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        showProgressDialogs(model.getLang().string.synchronizationProcess);

        tabLayout = findViewById(R.id.tabContact);
        tvSync = findViewById(R.id.st_loading);
        tvSync.setText(model.getLang().string.syncronizing);
        own = SessionManager.getProfile(ContactTabActivity.this);
        viewPager = findViewById(R.id.vp_contact);
        mBtnFriend = findViewById(R.id.tab_friend);
        mBtnInvite = findViewById(R.id.tab_invite);
        sync = findViewById(R.id.item_sync);
        lnContactTab = findViewById(R.id.lnContactTab);
        lnLdTab = findViewById(R.id.lnLdTab);
        progressPercentBar = findViewById(R.id.progress_percent);
        tvPercent = findViewById(R.id.tv_percents);

        tabInvite = getIntent().getIntExtra(INVITE_FRIENDS, 0);

        Bundle bundle = new Bundle();
        bundle.putString("key", String.valueOf(tabInvite));
        InviteFriendsFragment fragmentOne = new InviteFriendsFragment();
        fragmentOne.setArguments(bundle);


        ViewTreeObserver vto = viewPager.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (tabInvite == 1) {
                    viewPager.setCurrentItem(tabInvite);
                }else if (tabInvite == 2){
                    viewPager.setCurrentItem(tabInvite);
                }
                viewPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            }
        });

        setupViewPager();

        try {
            RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.mgm_custom_tab, tabLayout, false);
            TextView mWordMgmCustom = relativeLayout.findViewById(R.id.txtWordMgmCustom);
            mWordMgmCustom.setText(model.getLang().string.mgm);
            Objects.requireNonNull(tabLayout.getTabAt(2)).setCustomView(relativeLayout);
        }catch (Exception e){
            e.printStackTrace();
        }

        /**
         * Config Dynamic
         * Added by Yudha Pratama Putra, 12 Nov 2018
         */
        configDynamic(ContactTabActivity.this);
//        sCaseInvite();

//        firebaseDatabaseRealtime(); // Firebase Realtime Database..........
        logFcb_screen_friendpageEvent();
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logFcb_screen_friendpageEvent () {
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_friendpage");
    }

    private void setupViewPager() {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ContactMVICallListFragment(), model.getLang().string.friend);
        adapter.addFragment(new InviteFriendsFragment(), model.getLang().string.inviteFriends);

        ConfigData cdt = SessionManager.getConfigData(getApplicationContext());
        if(cdt != null){
            boolean mgmAvailable = cdt.isMgm_enable();
            if (mgmAvailable){
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                adapter.addFragment(new MemberGetMemberFragment(), model.getLang().string.mgm);
            }else {
                tabLayout.setTabMode(TabLayout.MODE_FIXED);
            }
        }

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork(lnContactTab);
        LocalBroadcastManager.getInstance(ContactTabActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(ContactTabActivity.this).unregisterReceiver(popupNotifAppears);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Show syncronizing...
        IntentFilter _if = new IntentFilter("com.sbimv.mvicalls.SHOW");
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(showLoadingBR, _if);
        //Hide syncronizing...
        IntentFilter _it = new IntentFilter("com.sbimv.mvicalls.HIDE");
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(hideLoadingBR, _it);

        isSyncronizing = PreferenceUtil.getPref(this).getBoolean(PreferenceUtil.SYNC_LOADING, false);
        if (isSyncronizing) {
            if (!ContactTabActivity.this.isFinishing()) {
                try {
                    lnLdTab.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (!ContactTabActivity.this.isFinishing()) {
                try {
                    lnLdTab.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        //CAll data config
        Intent intent = new Intent(ContactTabActivity.this, SyncFirebaseService.class);
        startService(intent);

    }

    /*
     * ======================================================================================================================================================================================================================
     * Firebase Realtime Database
     * ======================================================================================================================================================================================================================
     * */

//    private void firebaseDatabaseRealtime(){
//        mDatabaseContact = FirebaseDatabase.getInstance().getReference("MviContact");
//        contactSeenListener = mDatabaseContact.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                if (dataSnapshot != null && dataSnapshot.exists()) {
//                    Object sCallerID = dataSnapshot.child("caller_id").getValue();
//                    Object sVideoCaller = dataSnapshot.child("video_caller").getValue();
//                    actionToInsert(sCallerID,sVideoCaller,dataSnapshot);
//                }
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                if (dataSnapshot != null && dataSnapshot.exists()) {
//                    Object sCallerID = dataSnapshot.child("caller_id").getValue();
//                    Object sVideoCaller = dataSnapshot.child("video_caller").getValue();
//                    actionToInsert(sCallerID,sVideoCaller,dataSnapshot);
//                }
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }
//
//    private void actionToInsert(Object sCallerID, Object sVideoCaller,
//                                DataSnapshot dataSnapshot){
//
//        if (sCallerID != null && sVideoCaller != null){
//            String caller_id = sCallerID.toString();
//            String video_caller = sVideoCaller.toString();
//
//            if (getCallerID(caller_id) == true){
//                List<ContactItem> contact =
//                        Collections.singletonList(dataSnapshot.getValue(ContactItem.class));
//                saveIntoDB(contact,video_caller,caller_id);
//            }
//        }
//
//    }
//
//    private void saveIntoDB(List<ContactItem> contacts,String... arg){
//        try{
//            db = ContactDBManager.getInstance(ContactTabActivity.this);
//        }catch (Exception e){
//            return;
//        }
//        if (db != null) {
//            if (contacts != null && contacts.size()>0) {
//                for (ContactItem item : contacts) {
//
//                    String name = item.name;
//                    if (TextUtils.isEmpty(name)) {
//                        String phonenum = item.msisdn;
//                        item.name = "~ " + selectionContactDetail(phonenum);
//                    }
//
//                    db.insertContact(item);
//                    Intent i = new Intent("com.sbimv.mvicalls.UPDATE_FRIEND");
//                    LocalBroadcastManager.getInstance(ContactTabActivity.this).sendBroadcast(i);
//                }
//
//                if (!TextUtils.isEmpty(arg[0]) && !TextUtils.isEmpty(arg[1])) {
//                    Intent syncVideoIntent = new Intent(ContactTabActivity.this, SyncFirebaseVideoServices.class);
//                    syncVideoIntent.putExtra("video_caller", arg[0]);
//                    syncVideoIntent.putExtra("caller_id", arg[1]);
//                    startService(syncVideoIntent);
//                }
//
//            }
//        }
//    }
//
//    private boolean getCallerID(String... arg){
//        boolean found = false;
//        ContactDBManager contactDBManager = new ContactDBManager(ContactTabActivity.this);
//        if (contactDBManager != null) {
//            Cursor data = contactDBManager.getMVUID(arg[0]);
//            if (data == null) {
//                found = false;
//            } else if (data != null) {
//                found = true;
//            }
//        }
//        return found;
//    }
//
//    private String selectionContactDetail(String msisdn){
//        ContentResolver cr = getContentResolver();
//        String selection = "(" + ContactsContract.CommonDataKinds.Phone.NUMBER + " LIKE '%" + msisdn + "%') OR (" + ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER + " LIKE '%" + msisdn + "%')";
//        String[] projection = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER};
//
//        try{
//            cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, selection, null, null);
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//
//        if (cursor!=null && cursor.getCount() > 0) {
//            while (cursor.moveToNext()) {
//                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
//                if (!TextUtils.isEmpty(name)) {
//                    return name;
//                }
//            }
//        }
//        cursor.close();
//
//        return "Unknown";
//    }
    /*
     * ======================================================================================================================================================================================================================
     * */

    @Override
    public void onStop() {
        super.onStop();

//        mDatabaseContact.removeEventListener(contactSeenListener);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(showLoadingBR);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(hideLoadingBR);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    String permission = "";
                    for (String per : permissions) {
                        permission += "\n" + per;
                    }
                }
                return;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_contact, menu);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                View menuItemView = findViewById(R.id.item_sync);
            }
        }, 500);

        return super.onCreateOptionsMenu(menu);
    }

    private void sCaseInvite() {
        if (!isViewAttached)
            return;
        if (!ContactTabActivity.this.isFinishing()) {
            View tabView = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(1);// Pointing to tab Invite
            showCaseWrapper.showFancydoAction(ShowCaseWrapper.INVITE, model.getLang().string.wordingScInviteFriends, tabView,
                    new ShowCaseListener.ActionListener() {
                        @Override
                        public void onScreenTap() {
                            viewPager.setCurrentItem(1);
                            showSCNext();
                        }

                        @Override
                        public void onOk() {
                            viewPager.setCurrentItem(1);
                            showSCNext();
                        }

                        @Override
                        public void onSkip() {
                            PreferenceUtil.getEditor(ContactTabActivity.this).putBoolean(PreferenceUtil.SKIP_INVITE, true).commit();
//                            finish();
                            showSCNext();
                        }
                    });

        }
    }

    private void showSCNext() {
        View mgmView = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(2);// Pointing to tab MGM
        viewPager.setCurrentItem(2);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showCaseWrapper.showFancydoAction("MGM", model.getLang().string.wordingScMGM, mgmView, new ShowCaseListener.ActionListener() {
                    @Override
                    public void onScreenTap() {
//                        viewPager.setCurrentItem(2);
                    }

                    @Override
                    public void onOk() {
//                        viewPager.setCurrentItem(2);
                    }

                    @Override
                    public void onSkip() {
                        finish();
                    }
                });
            }
        }, 1000);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_sync) {
            try {
                GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_CONTACT, TrackerConstant.EVENT_CAT_CONTACT, "click button sync", own.caller_id);
            } catch (Exception e) {
                e.printStackTrace();
            }

            ViewPropertyAnimator view = findViewById(R.id.item_sync).animate().rotation(360);
            view.start();
            GATRacker.getInstance(getApplication()).sendEventWithScreen(TrackerConstant.SCREEN_CONTACT, TrackerConstant.EVENT_CAT_CONTACT, "click button sync", own.caller_id);

            SyncUtils.permissionSyncContact(ContactTabActivity.this);

            String syncro = TextUtils.isEmpty(model.getLang().string.str_friends_sync_process)
                    ? "Syncronization in process"
                    : model.getLang().string.str_friends_sync_process;
            showSnackBar(getWindow().getDecorView(), syncro);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void showProgressDialogs(String... arg) {
        if (!isViewAttached)
            return;

        try {
            progressDialog = new ProgressDialog(ContactTabActivity.this);
            String syncro = TextUtils.isEmpty(model.getLang().string.str_friends_sync_process)
                    ? "Syncronization in process..."
                    : model.getLang().string.str_friends_sync_process;
            progressDialog.setMessage(syncro);
            progressDialog.setCanceledOnTouchOutside(false);
        } catch (Exception e) {
            Log.e("ContactTabActivity", e.getMessage());
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> titleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            titleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

    }

//    private class ContactPagerAdapter extends FragmentStatePagerAdapter {
//        public ContactPagerAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            if (position == 0) {
//                return new ContactMVICallListFragment();
//            } else {
//                try {
//                    GATRacker.getInstance(getApplication())
//                            .sendEventWithScreen(TrackerConstant.SCREEN_CONTACT,
//                                    TrackerConstant.EVENT_CAT_CONTACT,
//                                    "click tab invite", own.caller_id);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                return new InviteFriendsFragment();
//            }
//        }
//
//        @Override
//        public int getCount() {
//            return 2;
//        }
//    }

    @Override
    protected void onServiceConnected() {

    }

    @Override
    public void onStartFailed(SinchError error) {

    }

    @Override
    public void onStarted() {

    }

    @Override
    public void onDestroy() {
        if (getSinchServiceInterface() != null) {
            getSinchServiceInterface().stopClient();
        }
        super.onDestroy();
    }

    @Override
    public void onDataSelected(Context ctx, String msisdn, String caller_id) {
        try {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                    callWithDataClicked(ctx, msisdn, caller_id);
                } else {
//                    showInfoDialog();
                    callConventional(ctx, msisdn);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            callWithDataClicked(ctx, msisdn, caller_id);
        }
    }

    public void callWithDataClicked(Context ctx, String msisdn, String caller_id) {
        Map<String, String> headers = new HashMap<String, String>();
        if(getSinchServiceInterface() != null) {
            try {
                Call call = getSinchServiceInterface().callUser(caller_id, headers);
                if (call != null) {
                    String callId = call.getCallId();
                    if (!TextUtils.isEmpty(callId)) {
                        Intent callScreen = new Intent(this, OutgoingScreenActivity.class);
                        callScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        callScreen.putExtra(SinchService.CALL_ID, callId);
                        callScreen.putExtra("id", caller_id);
                        startActivity(callScreen);
                    }
                }
            }catch (Exception e){
                callConventional(ctx, msisdn);
            }
        }else {
            callConventional(ctx, msisdn);
        }
    }

    private void callConventional(Context ctx, String msisdn){
        Intent phoneIntent = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", msisdn, null));
        if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        ctx.startActivity(phoneIntent);
    }

}
