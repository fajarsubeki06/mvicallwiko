package com.sbimv.mvicalls.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.sbimv.mvicalls.R;


public class PopUpProcedureMGM extends AppCompatActivity {

    private String url;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up_procedure_mgm);

        ImageView btnClose = findViewById(R.id.btnCloseProcedure);
        WebView wvProcedure = findViewById(R.id.wvProcedure);

        btnClose.bringToFront();

        wvProcedure.getSettings().setLoadWithOverviewMode(true);
        wvProcedure.getSettings().setUseWideViewPort(true);
        wvProcedure.getSettings().setDomStorageEnabled(true);
        wvProcedure.getSettings().setJavaScriptEnabled(true);
        wvProcedure.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            url = bundle.getString("urlWebview");
            wvProcedure.setWebViewClient(new HowToWebViewClient());
            wvProcedure.loadUrl(url);
        }

        btnClose.setOnClickListener(view -> finish());

    }

    public class HowToWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String urlWeb) {
            url = urlWeb;
            super.onPageFinished(view, url);
        }
    }

}
