package com.sbimv.mvicalls.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;

public class WebChargingContentActivity extends BaseActivity {

    private WebView webViewContent;
    private SwipeRefreshLayout swipeRefreshLayout;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_charging_content);
        setDefaultToolbar(true);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshContent);
        webViewContent = findViewById(R.id.webViewChargingContent);
        url = getIntent().getStringExtra("url");

        webViewContent.loadUrl(url);
        webViewContent.setWebViewClient(new customWebClient());
        webViewContent.getSettings().setJavaScriptEnabled(true);
        webViewContent.setInitialScale(320);
    }


    private class customWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String request) {
            view.loadUrl(request);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
