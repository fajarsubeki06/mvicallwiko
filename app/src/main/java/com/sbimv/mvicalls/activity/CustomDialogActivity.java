package com.sbimv.mvicalls.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.SessionManager;

public class CustomDialogActivity extends AppCompatActivity implements android.view.View.OnClickListener {

    public Activity activity;
    private ImageView ivDialogHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_dialog);

        ivDialogHome = findViewById(R.id.iv_dialog_home);
        ImageView ivCloseDialog = findViewById(R.id.iv_close_dialog);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String url_banner = bundle.getString("url_banner");
            if (!TextUtils.isEmpty(url_banner)){
                try {
                    Glide.with(this)
                            .asBitmap()
                            .load(url_banner)
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NotNull final Bitmap bitmap, Transition<? super Bitmap> transition) {
                                    try {
                                        final int w = bitmap.getWidth();
                                        final int h = bitmap.getHeight();
                                        ivDialogHome.post(() -> {
                                            int ivW = ivDialogHome.getMeasuredWidth();
                                            ivDialogHome.getLayoutParams().height = (ivW * h) / w;
                                            ivDialogHome.requestLayout();
                                            ivDialogHome.setImageBitmap(bitmap);
                                        });
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ivCloseDialog.setOnClickListener(this);
                ivDialogHome.setOnClickListener(this);

            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close_dialog:
                finish();
                break;
            case R.id.iv_dialog_home:
                this.startActivity(new Intent(this, ContactTabActivity.class).putExtra(ContactTabActivity.INVITE_FRIENDS, 2));
                try {
                    ContactItem own = SessionManager.getProfile(CustomDialogActivity.this);
                    if (own != null) {
                        GATRacker.getInstance(getApplication()).sendEvent(TrackerConstant.EVENT_CAT_POPUP_MGM, "click popup mgm", own.caller_id);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                finish();
                break;
        }
    }

}
