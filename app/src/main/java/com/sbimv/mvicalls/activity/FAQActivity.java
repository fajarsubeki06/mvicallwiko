package com.sbimv.mvicalls.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;

import java.util.Objects;

/**
 * Created by Hendi on 10/27/17.
 */

public class FAQActivity extends BaseActivity {

//    String url = "http://www.mvicall.com/frequently.html";
    String url ;
    SwipeRefreshLayout swipeRefreshLayout;

    private WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        setDefaultToolbar(true,"FAQ");

        if (curLang().equalsIgnoreCase("ID")) {
            url = "http://mvicall.com/frequently_id.html";
        }else {
            url = "http://mvicall.com/frequently_en.html";
        }

        swipeRefreshLayout = findViewById(R.id.swapFAQ);
        wv = findViewById(R.id.wv);

        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setDisplayZoomControls(true);

        wv.setWebViewClient(new MyWebViewClient());

        try{
            wv.loadUrl(url);
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                wv.reload();
            }
        });


    }

    public class MyWebViewClient extends WebViewClient{
        @Override
        public void onPageFinished(WebView view, String urlWeb) {
            swipeRefreshLayout.setRefreshing(false);
            url = urlWeb;
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(FAQActivity.this).unregisterReceiver(popupNotifAppears);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork(wv);
        LocalBroadcastManager.getInstance(FAQActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

}
