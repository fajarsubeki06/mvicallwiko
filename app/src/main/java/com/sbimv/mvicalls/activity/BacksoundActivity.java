 package com.sbimv.mvicalls.activity;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.adapter.BacksoundAdapter;
import com.sbimv.mvicalls.databinding.ActivityBacksoundBinding;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.BackSound;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.video_audio_merge.UtilVideoAudio;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BacksoundActivity extends BaseActivity implements View.OnTouchListener, BacksoundAdapter.OnItemClickListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnBufferingUpdateListener{

    private ContactItem own;
    private RecyclerView recyclerBacksound;
    private List<BackSound> backSoundList = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;

    //Media
    public MediaPlayer mediaPlayer;
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    private String outputhFileName = "audio" + ".mp3";
    private String imagePath, videoPath, sendUri, imagePath2, videoPath2, sendUri2;
    private Uri uri;
    private UtilVideoAudio utils = new UtilVideoAudio();
    BacksoundAdapter adapter;
    private int duration = 0;
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityBacksoundBinding binding = DataBindingUtil.setContentView(BacksoundActivity.this, R.layout.activity_backsound);
        binding.setLangModel(model);

        checkPermission();

        setDefaultToolbar(true, model.getLang().string.backSound);
        recyclerBacksound = findViewById(R.id.recyclerBacksound);
        swipeRefreshLayout = findViewById(R.id.swipeBackSound);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            loadBacksound();
            swipeRefreshLayout.setRefreshing(false);
        });

        try{
            own = SessionManager.getProfile(this);

            initPlayaer();

            Intent data = getIntent();
            if (data != null){
                videoPath = data.getStringExtra("SKEY_VIDEO");
                sendUri = data.getStringExtra("SEND_URI");
                videoPath2 = data.getStringExtra("SKEY_VIDEO2");
                sendUri2 = data.getStringExtra("SEND_URI2");
            }

            initRecycler();
            loadBacksound();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void initPlayaer(){
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnBufferingUpdateListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnPreparedListener(mediaPlayer -> tooglePlayPause());
    }

    private void initRecycler() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerBacksound.setLayoutManager(manager);
        adapter = new BacksoundAdapter(BacksoundActivity.this, backSoundList, this);
        recyclerBacksound.setAdapter(adapter);
    }

    private void tooglePlayPause(){
        try{
            if(mediaPlayer != null){
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.pause();
                }else{
                    mediaPlayer.seekTo(duration);
                    mediaPlayer.start();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void loadBacksound(){
        if (own != null){
            String sMsisdn = TextUtils.isEmpty(own.msisdn) ? "" : own.msisdn;
            Call<APIResponse<List<BackSound>>> call = ServicesFactory.getService().getBackSound(sMsisdn);
            call.enqueue(new Callback<APIResponse<List<BackSound>>>() {
                @Override
                public void onResponse(@NotNull Call<APIResponse<List<BackSound>>> call, @NotNull Response<APIResponse<List<BackSound>>> response) {
                    assert response.body() != null;
                    if (response.isSuccessful() && response.body().isSuccessful()){
                        backSoundList = response.body().data;
                        if (backSoundList != null && backSoundList.size() != 0){
                            Objects.requireNonNull(recyclerBacksound.getAdapter()).notifyDataSetChanged();
                            initRecycler();
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull Call<APIResponse<List<BackSound>>> call, @NotNull Throwable t) {
                    String s_failure = TextUtils.isEmpty(model.getLang().string.wordingFailureResponse)
                            ? "Failure Fetching Data"
                            : model.getLang().string.wordingFailureResponse;
                    Toast.makeText(BacksoundActivity.this, s_failure, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void setMediaPlayer(){
        try{
            if (mediaPlayer != null){
                if (mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                }
                mediaPlayer.release();
                mediaPlayer = null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadBacksound();
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {

    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer2) {
       if (mediaPlayer != null && image != null){
           duration = 0;
           mediaPlayer.release();
           mediaPlayer = null;
           image.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_button));
       }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }

    protected Dialog onCreateDialog(int id){

        String titleDownload = TextUtils.isEmpty(model.getLang().string.titleDownload)
                ? "Downloading file, Please Wait"
                : model.getLang().string.titleDownload;

        if (id == progress_bar_type) {
            try {
                pDialog = new ProgressDialog(this);
                pDialog.setMessage(titleDownload);
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void onItemPlayMusic(BackSound backSound, ImageView imageView, int previousPosition, int currentPosition) {
        image = imageView;
        if (mediaPlayer != null){
            if (mediaPlayer.isPlaying()){
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_button));
                if (duration != 0){
                    duration = 0;
                }else{
                    duration = mediaPlayer.getCurrentPosition();
                }
                mediaPlayer.pause();
                mediaPlayer.reset();
            }else{
                try {
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_button));
                    mediaPlayer.setDataSource(backSound.getSound());
                    mediaPlayer.prepareAsync();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else{
            initPlayaer();
            if (mediaPlayer.isPlaying()){
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_play_button));
                mediaPlayer.pause();
                mediaPlayer.reset();
            }
            try {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause_button));
                mediaPlayer.setDataSource(backSound.getSound());
                mediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemChoiceMusic(BackSound backSound) {
        new DownloadFileFromURL().execute(backSound.getSound());
    }

    @SuppressLint("StaticFieldLeak")
    protected class DownloadFileFromURL extends AsyncTask<String, String, String>  {
        protected  void onPreExecute(){
            super.onPreExecute();
            try{
                showDialog(progress_bar_type);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();

                int lenghOfFile = connection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                @SuppressLint("SdCardPath") OutputStream output = new FileOutputStream( utils.getOutputPath() + outputhFileName);
                byte[] data = new byte[1024];
                long total = 0;

                while ((count = input.read(data)) != -1){
                    total += count;
                    publishProgress(""+(int)((total*100)/lenghOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
            } catch (Exception e){
                Log.e("Error : ", e.getMessage());
            }
            return null;
        }

        protected void onProgressUpdate(String... progress) {
           try{
               if (!BacksoundActivity.this.isFinishing()) {
                   pDialog.setProgress(Integer.parseInt(progress[0]));
               }
           }catch (Exception e){
               e.printStackTrace();
           }
        }

        protected void onPostExecute(String file_url){
            try{
                dismissDialog(progress_bar_type);

                imagePath = utils.getOutputPath() + outputhFileName;
                imagePath2 = utils.getOutputPath() + outputhFileName;
                uri = Uri.parse(imagePath);
                Log.e("IMAGE PATH", imagePath);

                moveIntent();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    protected void checkPermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener(){

                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void moveIntent(){
        try{
            Intent data = getIntent();
            String fromVideo = data.getStringExtra("KEY_VIDEO");
            if (fromVideo != null){
                Intent intent = new Intent(getApplicationContext(), UploadVideoToneActivity.class);
                intent.putExtra("KEY_MUSIC", imagePath);
                intent.putExtra("KEY_VIDEO", videoPath);
                intent.putExtra("KEY_URI", sendUri);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }else{
                Intent intent = new Intent(getApplicationContext(), SetupProfileActivity.class);
                intent.putExtra("KEY_MUSIC", imagePath2);
                intent.putExtra("KEY_VIDEO", videoPath2);
                intent.putExtra("KEY_URI", sendUri2);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        setMediaPlayer();
    }


}
