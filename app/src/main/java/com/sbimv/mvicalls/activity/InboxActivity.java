package com.sbimv.mvicalls.activity;

import android.app.AlertDialog;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.adapter.InboxAdapter;
import com.sbimv.mvicalls.contract.MainContract;

import com.sbimv.mvicalls.interactor.IntractorImpl;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.InboxModel;
import com.sbimv.mvicalls.presenter.InboxPresenterImpl;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.view.DialogUpload;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class InboxActivity extends BaseActivity
        implements MainContract.InboxView, InboxAdapter.onClickedDeleteListner {

    private RecyclerView rvInbox;
    private ArrayList<InboxModel> inboxModelArrayList = new ArrayList<>();
    private DialogUpload dialogUpload;
    private LinearLayout emptyState;
    private LangViewModel modelLang;
    MainContract.InboxPresenter inboxPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);

        modelLang = LangViewModel.getInstance();
        setDefaultToolbar(true, modelLang.getLang().string.inbox);

        // Initial View
        setInitView();

        // Initial Presenter
        inboxPresenter = new InboxPresenterImpl(this, new IntractorImpl());
        initInboxRecyclerView();

        // Load Data Inbox
        ContactItem contactItem = SessionManager.getProfile(this);
        inboxPresenter.setDataInbox(contactItem, "","");

    }

    private void setInitView(){
        TextView tvEmpty = findViewById(R.id.tvEMptyInbox);
        tvEmpty.setText(modelLang.getLang().string.emptyInbox);
        emptyState = findViewById(R.id.linEmpty_inbox);
        rvInbox = findViewById(R.id.rvInbox);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(InboxActivity.this).unregisterReceiver(popupNotifAppears);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(InboxActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

    private void initInboxRecyclerView() {
        LinearLayoutManager layout = new LinearLayoutManager(this);
        layout.setOrientation(LinearLayoutManager.VERTICAL);
        int numOfColumn = 1;
        rvInbox.setLayoutManager(new GridLayoutManager(this, numOfColumn));
        InboxAdapter inboxAdapter = new InboxAdapter(this, inboxModelArrayList, this);
        rvInbox.setAdapter(inboxAdapter);
    }

    private void showPopupConfirmation(final String... arg) {

        if (!InboxActivity.this.isFinishing()) {
            String titleDelMsg = TextUtils.isEmpty(modelLang.getLang().string.deleteMessage)?"":modelLang.getLang().string.deleteMessage;
            AlertDialog.Builder builder1 = new AlertDialog.Builder(InboxActivity.this);
            builder1.setMessage(titleDelMsg);
            builder1.setPositiveButton(arg[1], null);
            builder1.setNegativeButton(arg[2], null);
            AlertDialog dialog1 = builder1.create();

            dialog1.setOnShowListener(dialog -> {
                Button buttonPositive = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                buttonPositive.setOnClickListener(v -> {
                    dialog1.dismiss();
                    ContactItem contactItem = SessionManager.getProfile(this);
                    inboxPresenter.setDataInbox(contactItem, "delete",arg[3]);

                });

                Button buttonNegative = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEGATIVE);
                buttonNegative.setOnClickListener(v -> dialog1.dismiss());
            });
            dialog1.show();
        }

    }

    @Override
    public void onDelete(String delete_confirm, String approve_confirm, String cancel_confirm, String uid) {
        showPopupConfirmation(delete_confirm, approve_confirm, cancel_confirm, uid);
    }


    @Override
    public void isProgress() {
        if (InboxActivity.this.isFinishing()) // skip updating view
            return;

        try {
            dialogUpload = new DialogUpload();
            dialogUpload.show((InboxActivity.this).getSupportFragmentManager(), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void isFinish() {
        if (InboxActivity.this.isFinishing()) // skip updating view
            return;
        try {
            dialogUpload.dismissAllowingStateLoss();
            dialogUpload.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void isEmpty() {
        if (InboxActivity.this.isFinishing()) // skip updating view
            return;
        try{
            emptyState.setVisibility(View.VISIBLE);
            rvInbox.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void isSuccess(List dataArrayList) {
        inboxModelArrayList.clear();
        inboxModelArrayList.addAll(dataArrayList);
        Objects.requireNonNull(rvInbox.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void isDetailSuccess(String title, String message, String createDate) {

    }

    @Override
    public void isFailure() {
        if (modelLang == null)
            return;

        String txIsFailure = TextUtils.isEmpty(modelLang.getLang().string.wordingFailureResponse)?"":modelLang.getLang().string.wordingFailureResponse;
        Toast.makeText(this, txIsFailure, Toast.LENGTH_SHORT).show();

    }

}