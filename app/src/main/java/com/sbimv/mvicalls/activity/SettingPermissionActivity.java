package com.sbimv.mvicalls.activity;

import android.content.SharedPreferences;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
//import androidx.preference.PreferenceManager;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.Toast;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.fragment.SettingsFragment;

public class SettingPermissionActivity extends BaseActivity {

    public static final String OVERLAY_SWITCH = "overlay_switch";
    public static final String CALL_DEFAULT_SWITCH = "call_default_switch";
    private Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_permission);
        setDefaultToolbar(true, getResources().getString(R.string.setting_page)); // belum di set multi language....
        mBtnNext = findViewById(R.id.btnNextPage);

//        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//        ft.replace(R.id.fragment_placeholder, new SettingsFragment());
//        ft.commit();

        mBtnNext.setOnClickListener(v -> {
//            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(SettingPermissionActivity.this);
//            boolean overlayPref = sharedPref.getBoolean(SettingPermissionActivity.OVERLAY_SWITCH, false);
//            boolean defaultPref = sharedPref.getBoolean(SettingPermissionActivity.CALL_DEFAULT_SWITCH, false);
//
//            if (overlayPref == true && defaultPref == true){
//                finish();
//            }else {
//                Toast.makeText(SettingPermissionActivity.this, "Pengaturan aplikasi mu belum selesai..", Toast.LENGTH_SHORT).show();
//            }

        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
