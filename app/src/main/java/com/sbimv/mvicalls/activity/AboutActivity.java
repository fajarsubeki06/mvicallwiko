package com.sbimv.mvicalls.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.BuildConfig;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.adapter.ProfileContactAdapter;
import com.sbimv.mvicalls.databinding.ActivityAboutBinding;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.walktrough.FreePremiumContent;

import java.util.Objects;

public class AboutActivity extends BaseActivity {

    private ContactItem own;
    private ProfileContactAdapter adapter;
    private TextView tvFAQ, tvVersion, tvTNC, tvPrivPol,tvSbi,tvLast;
    private ActivityAboutBinding binding;
    private ImageView ivVietLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_about);
        binding.setLangModel(model);
        setDefaultToolbar(true, model.getLang().string.about);

        tvFAQ = findViewById(R.id.tvFAQ);
        tvVersion = findViewById(R.id.tvVersion);
        tvTNC = findViewById(R.id.tvTermCon);
        tvPrivPol = findViewById(R.id.tvPrivacy);
        ivVietLogo = findViewById(R.id.iv_viet_logo);
        String versionName = BuildConfig.VERSION_NAME;
        tvVersion.setText("v" + versionName);

        tvSbi = findViewById(R.id.tv_sbi);

        String ccodeViet = PreferenceUtil.getPref(AboutActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");

        assert ccodeViet != null;
        if (!TextUtils.isEmpty(ccodeViet) && ccodeViet.equalsIgnoreCase("+84")){
            ivVietLogo.setVisibility(View.VISIBLE);
            ivVietLogo.setOnClickListener(view -> startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://online.gov.vn/Home/AppDetails/975"))));
        }


        String sbi = TextUtils.isEmpty(model.getLang().string.str_about_mvicall_mark)
                ? "© 2017-2020 PT. Sinergi Bestama Indonesia \n All right reserverd."
                : model.getLang().string.str_about_mvicall_mark;
        tvSbi.setText(sbi);

        tvFAQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AboutActivity.this, FAQActivity.class);
                startActivity(intent);
            }
        });

        tvTNC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AboutActivity.this, TermConditionActivity.class);
                startActivity(intent);
            }
        });
        tvPrivPol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AboutActivity.this, PrivacyPolicy.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(AboutActivity.this).unregisterReceiver(popupNotifAppears);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(AboutActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

}
