package com.sbimv.mvicalls.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.sbimv.mvicalls.R;

public class IntroVietnamActivity extends AppCompatActivity {

    ImageView imageChoose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro_vietnam);

        imageChoose = findViewById(R.id.imageChoose);
        imageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(IntroVietnamActivity.this, PermissionVietnamActivity.class));
            }
        });
    }
}
