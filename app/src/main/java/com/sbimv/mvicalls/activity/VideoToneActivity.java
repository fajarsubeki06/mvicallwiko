package com.sbimv.mvicalls.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.databinding.ActivityVideoToneBinding;
import com.sbimv.mvicalls.databinding.SetupProfileFrgBinding;
import com.sbimv.mvicalls.databinding.VideotoneIncludeBinding;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.services.SyncConfigService;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.OwnVideoManager;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.walktrough.FriendsDemoActivity;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class VideoToneActivity extends BaseActivity {

    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityVideoToneBinding binding = DataBindingUtil.setContentView(VideoToneActivity.this, R.layout.activity_video_tone);
        binding.setLangModel(model);

        initMediaPlayer();
        initVideo();
    }

    @SuppressLint("CheckResult")
    private void initVideo(){

        String strWordingPreviewVideo = TextUtils.isEmpty(model.getLang().string.titlePreviewVideo)
                ?"Preview Videotone" :
                model.getLang().string.titlePreviewVideo;

        String strWordingSkipButton = TextUtils.isEmpty(model.getLang().string.titleSkip)
                ?"Lewati" :
                model.getLang().string.titleSkip;

        String strWordingYourFriend = TextUtils.isEmpty(model.getLang().string.titleYourFriend)
                ?"Hape Temanmu Saat Kamu telfon dia" :
                model.getLang().string.titleYourFriend;

        String strWordingButtonNext = TextUtils.isEmpty(model.getLang().string.buttonNext)
                ?"Lanjut" :
                model.getLang().string.buttonNext;

        Button btn_next = findViewById(R.id.btn_next);
        TextView titleTool = findViewById(R.id.judul_activity);
        Button buttonSkip = findViewById(R.id.btn_lewat);
        TextView yourFriends = findViewById(R.id.textTitleVideoTone);
        TextView textVideoName = findViewById(R.id.tv_video_name);
        TextView textVideoPhone = findViewById(R.id.tv_video_phone);
        TextView tvLike = findViewById(R.id.tvQtyLike);
        TextView tvReport = findViewById(R.id.tvQtyReport);
        CircleImageView circleImageView = findViewById(R.id.imageUser);

        // caller contact
        ContactItem item = SessionManager.getProfile(VideoToneActivity.this);

        if (item != null){
            textVideoName.setText(item.getName());
            textVideoPhone.setText(TextUtils.isEmpty(item.msisdn)?"":item.msisdn);
            RequestOptions request = new RequestOptions();
            request.placeholder(R.drawable.userpng);
            Glide.with(this).load(item.caller_pic).apply(request).into(circleImageView);
        }

        String like = TextUtils.isEmpty(model.getLang().string.like)
                ?"Like" :
                model.getLang().string.like;
        String report = TextUtils.isEmpty(model.getLang().string.wordingReport)
                ?"Report" :
                model.getLang().string.wordingReport;
        tvLike.setText(like);
        tvReport.setText(report);
        btn_next.setText(strWordingButtonNext);

        titleTool.setText(strWordingPreviewVideo);
        buttonSkip.setText(strWordingSkipButton);
        yourFriends.setText(strWordingYourFriend);

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceUtil.getEditor(VideoToneActivity.this).putBoolean(PreferenceUtil.SETUP_VIDEOTONE, true).commit();
                Intent intent = new Intent(VideoToneActivity.this, FriendsDemoActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        buttonSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCaseToFalse();
                //setFinalScase();
                Intent intent = new Intent(VideoToneActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void initMediaPlayer() {

        SharedPreferences sharedPreferences = getSharedPreferences(ConfigPref.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String urlBy = sharedPreferences.getString(ConfigPref.URL_BUY, "");
        urlBy = urlBy.replaceAll(" ", "%20");

        VideoView mVideoView = findViewById(R.id.video_view);
        FrameLayout mediaControllerAnchor = findViewById(R.id.controller_anchor);
        boolean setCont = false;

        if (!VideoToneActivity.this.isFinishing()) {
            mediaController = new MediaController(VideoToneActivity.this);
            mediaController.setAnchorView(mediaControllerAnchor);
            mVideoView.setMediaController(mediaController);
            mVideoView.setZOrderOnTop(true);

            ContactItem contactItem = SessionManager.getProfile(VideoToneActivity.this);
            if(contactItem == null)
                return;

            if (setCont) {
                if (VideoToneActivity.this.isFinishing()) {
                    try {
                        Uri uris = Uri.parse(urlBy);
                        mVideoView.setVideoURI(uris);
                        mVideoView.seekTo(100);
                        mVideoView.start();
                        setCont = false;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {

                if (!VideoToneActivity.this.isFinishing()) {
                    if (OwnVideoManager.isOwnVideoExist(VideoToneActivity.this)) {
                        VideoToneActivity.this.runOnUiThread(() -> {
                            if (!VideoToneActivity.this.isFinishing()) {
                                try {
                                    Uri ownVideoUri = OwnVideoManager.getOwnVideoUri(VideoToneActivity.this);
                                    mVideoView.setVideoURI(ownVideoUri);
                                    mVideoView.seekTo(100);
                                    mVideoView.start();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } else if (OwnVideoManager.collectionExist(VideoToneActivity.this)) {

                        VideoToneActivity.this.runOnUiThread(() -> {
                            if (!VideoToneActivity.this.isFinishing()) {
                                try {
                                    Uri uri = OwnVideoManager.getCollectionUri(VideoToneActivity.this);
                                    mVideoView.setVideoURI(uri);
                                    mVideoView.seekTo(100);
                                    mVideoView.start();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }
            mVideoView.setOnPreparedListener(mediaPlayer -> mediaPlayer.setLooping(false));
            mVideoView.setOnErrorListener((mediaPlayer, i, i1) -> true);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        PreferenceUtil.getEditor(VideoToneActivity.this).putBoolean(PreferenceUtil.SETUP_VIDEOTONE, true).commit();
//        Intent intent = new Intent(VideoToneActivity.this, FriendsDemoActivity.class);
//        startActivity(intent);
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(VideoToneActivity.this, model.getLang().string.pressToExit, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
