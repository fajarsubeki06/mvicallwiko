package com.sbimv.mvicalls.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataBestDeal;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//extends AppCompat for dialog theme
public class PopUpBestDeals extends AppCompatActivity {

    private ImageView imageView;
    private ContactItem own;
    private ImageView closeButton;
    private LangViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pop_up_best_deals);
        model = LangViewModel.getInstance();
        own = SessionManager.getProfile(PopUpBestDeals.this);
        imageView = findViewById(R.id.imgBannerPopUp);
        closeButton = findViewById(R.id.btnClosePopupBestDeal);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Bundle bestDeals  = getIntent().getExtras();
        if (bestDeals != null){
            final DataBestDeal data = bestDeals.getParcelable("data");

            if (data != null) {
                try {
                    Glide.with(PopUpBestDeals.this).asBitmap().load(data.getFull_lp()).into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(final Bitmap bitmap, Transition<? super Bitmap> transition) {
                            final int w = bitmap.getWidth();
                            final int h = bitmap.getHeight();
                            imageView.post(new Runnable() {
                                @Override
                                public void run() {
                                    int ivW = imageView.getMeasuredWidth();
                                    int ivH = ivW * h / w;
                                    imageView.getLayoutParams().height = ivH;
                                    imageView.requestLayout();
                                    imageView.setImageBitmap(bitmap);
                                }
                            });

                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String link = data.getSource_content();
                    if(link.contains("rewards")){ // handle dynamic link
                        String appLink = link
                                .replace("http","app")
                                .replace("https", "app");
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appLink)));

                        // for popup get promo
                        Intent intent = new Intent();
                        intent.putExtra("action", "rewards");
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                    else{
                        sendBest_id(data.getDeal_id(), link);
                    }
                }
            });
        }
    }

    private void sendBest_id(String deal_id, final String url) {
        Call<APIResponse<List<DataBestDeal>>> call = ServicesFactory.getService().getBestDeal(own.msisdn,deal_id);
        call.enqueue(new Callback<APIResponse<List<DataBestDeal>>>() {
            @Override
            public void onResponse(Call<APIResponse<List<DataBestDeal>>> call, Response<APIResponse<List<DataBestDeal>>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()){
                    startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse(url)));

                    // for popup get promo
                    Intent intent = new Intent();
                    intent.putExtra("MY_M3", "clicked");
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<APIResponse<List<DataBestDeal>>> call, Throwable t) {
                if (!isFinishing()) {
                    Toast.makeText(PopUpBestDeals.this, model.getLang().string.errorFetchingData, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
