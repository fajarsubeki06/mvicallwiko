package com.sbimv.mvicalls.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.PreviewPremiumVideo;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataAboutmvicall;
import com.sbimv.mvicalls.pojo.DataAppSettings;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrivacyPolicy extends BaseActivity {

    private SwipeRefreshLayout swipeRefreshLayout;
    private WebView wv;
    private String urlPrivacy;
    private ProgressBar pgPrivacy;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        setDefaultToolbar(true, model.getLang().string.privacyPolicy);

        swipeRefreshLayout = findViewById(R.id.swapFAQ);
        pgPrivacy = findViewById(R.id.pgPrivacy);

        wv = findViewById(R.id.wv);

        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setDisplayZoomControls(true);

        getUrlPrivacyPolicy();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                wv.reload();
            }
        });


    }

    private void getUrlPrivacyPolicy () {
        pgPrivacy.setVisibility(View.VISIBLE);
//        ContactItem own = SessionManager.getProfile(PrivacyPolicy.this);
//        if (own != null) {

            String lang = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
//            String msisdn = own.msisdn;
            if (!TextUtils.isEmpty(lang)) {

                Call<APIResponse<DataAppSettings>> call = ServicesFactory.getService().getAppSettings(lang);
                call.enqueue(new Callback<APIResponse<DataAppSettings>>() {
                    @Override
                    public void onResponse(@NotNull Call<APIResponse<DataAppSettings>> call, @NotNull Response<APIResponse<DataAppSettings>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            if (!PrivacyPolicy.this.isFinishing()) {
                                DataAboutmvicall data = response.body().data.getAboutmvicall();
                                if (data != null) {
                                    urlPrivacy = data.getPrivacy();
                                    if (!TextUtils.isEmpty(urlPrivacy)) {
                                        wv.setWebViewClient(new CustomWebViewClient());
                                        wv.loadUrl(urlPrivacy);
                                    }
                                }
                            }
                        }
                        hideProgressBar();
                    }

                    @Override
                    public void onFailure(@NotNull Call<APIResponse<DataAppSettings>> call, @NotNull Throwable t) {
                        hideProgressBar();
                    }
                });

            }
//        }

    }

    private void hideProgressBar(){
        if (!PrivacyPolicy.this.isFinishing()) {
            pgPrivacy.setVisibility(View.GONE);
        }
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String urlWeb) {
            swipeRefreshLayout.setRefreshing(false);
            urlPrivacy = urlWeb;
            super.onPageFinished(view, urlPrivacy);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        public void onPageCommitVisible(WebView view, String url) {
            super.onPageCommitVisible(view, url);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(PrivacyPolicy.this).unregisterReceiver(popupNotifAppears);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork(wv);
        LocalBroadcastManager.getInstance(PrivacyPolicy.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

}
