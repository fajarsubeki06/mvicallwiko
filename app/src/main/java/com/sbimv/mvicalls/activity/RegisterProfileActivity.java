package com.sbimv.mvicalls.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.http.ProgressRequestBody;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.util.ImageSelector;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Util;
import com.sbimv.mvicalls.util.ValidationInputUtil;
import com.sbimv.mvicalls.view.DialogUpload;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Activity Register
 * Added by Yudha Pratama Putra, 17 Okt 2018
 */

public class RegisterProfileActivity extends BaseActivity implements View.OnClickListener {

    private EditText etUsersPhone;
    private EditText etUsersName;
    private Button btnSaves;
    private CircleImageView ivUserPict;
    private ImageView btn_edit_user_pict;
    private TextView tvUsersStatus;

    private ImageSelector mImageSelectors;
    private File proPicPaths;
    private ContactItem own;
    private DialogUpload dialogUpload;
    private Button btnReg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_profile);

        btnReg = findViewById(R.id.btnSaves);
        btnReg.setText(model.getLang().string.loginRegister);
        ivUserPict = findViewById(R.id.iv_user_pict);
        etUsersName = findViewById(R.id.et_users_name);
        etUsersPhone = findViewById(R.id.et_users_phone);
        tvUsersStatus = findViewById(R.id.tv_users_status);

        btnSaves = findViewById(R.id.btnSaves);
        btnSaves.setEnabled(false);
        btn_edit_user_pict = findViewById(R.id.btn_edit_user_pict);

        try{
            own = SessionManager.getProfile(this);
        }catch (NullPointerException e){
            e.printStackTrace();
            if (e instanceof NullPointerException){
                toast("Preference null...");
                return;
            }
        }

        etUsersName.addTextChangedListener(textWatcherUserName);
        tvUsersStatus.addTextChangedListener(textWatcherUserStatus);
        btn_edit_user_pict.setOnClickListener(this);
        btnSaves.setOnClickListener(this);

        if (isViewAttached){
            setUserInfo();
        }

        mImageSelectors = ImageSelector.create(RegisterProfileActivity.this, new ImageSelector.ImageSelectorDelegate() {
            @Override
            public void onImageSelected(Uri uri) {
                CropImage.activity(uri).setAspectRatio(1, 1).start(RegisterProfileActivity.this);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork(etUsersName);
    }

    private void setUserInfo() {

        try{
            own = SessionManager.getProfile(RegisterProfileActivity.this);
        }catch (NullPointerException e){
            e.printStackTrace();
            if (e instanceof NullPointerException){
                toast("Preference null...");
                return;
            }
        }

        etUsersPhone.setText(own.msisdn);
        etUsersName.setText(own.getName());

        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.userpng);

        Glide.with(RegisterProfileActivity.this)
                .load(own.caller_pic)
                .apply(requestOptions)
                .into(ivUserPict);

        tvUsersStatus.setText(own.getStatusCaller());
        ContactItem contactItem = SessionManager.getProfile(RegisterProfileActivity.this);
        saveContactFirebase(contactItem);
    }

    private TextWatcher textWatcherUserName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            checkRegisterChanged();
        }
    };

    private TextWatcher textWatcherUserStatus = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            checkRegisterChanged();
        }
    };

    void checkRegisterChanged() {
        boolean isChanged = false;

        if (proPicPaths != null)
            isChanged = true;

        if (!etUsersName.getText().toString().equals(own.getName()))
            isChanged = true;

        if (!tvUsersStatus.getText().toString().equals(own.getStatusCaller()))
            isChanged = true;

        if (isChanged) {
            btnSaves.setBackground(getResources().getDrawable(R.drawable.selector_rounded_green));
            btnSaves.setEnabled(true);
            btnSaves.setTag(true);
        } else {
            btnSaves.setBackground(getResources().getDrawable(R.drawable.selector_rounded_grey_e4e4e4));
            btnSaves.setEnabled(false);
            btnSaves.setTag(false);
        }
    }

    @Override
    public void onClick(View v) {
        if (btn_edit_user_pict == v){
            selectImage();
        } else if (btnSaves == v){
            Boolean update = (Boolean) btnSaves.getTag();
            if (update != null && update) {
                if(checkValidation()){
                    etUsersName.clearFocus();
                    Util.hideKeyboard(this);
                    showDialogs();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showCustomDialog("Create profile...");
                            updateProfile();
                        }
                    }, 200);
                }
            }
        }
        else {
            ucInfo();
        }
    }

    private void selectImage() {
        new AlertDialog.Builder(this)
                .setTitle(model.getLang().string.selectImageFrom)
                .setItems(new String[]{model.getLang().string.selectImageFromCamera, model.getLang().string.gallery},
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (i == 0)// camera
                                    mImageSelectors.captureImage();
                                else // gallery
                                    mImageSelectors.openGallery();
                            }
                        })
                .show();
    }

    private void updateProfile() {
        // multipart
        MultipartBody.Part _file = null;
        if (proPicPaths != null) {
            // ini progress listener
            ProgressRequestBody.ProgressListener progressListener = new ProgressRequestBody.ProgressListener() {
                @Override
                public void transferred(final int num, long transferred, long totalSize) {
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            showCustomDialog("Updating " + num + "%");
                        }
                    });
                }
            };

            File file = new File(String.valueOf(proPicPaths));
            // init request body
            ProgressRequestBody requestFileBody = new ProgressRequestBody(file, "multipart/form-data", progressListener);
            _file = MultipartBody.Part.createFormData("caller_pic", file.getName(), requestFileBody);
        }

        // set request body
        RequestBody _caller_id = RequestBody.create(MediaType.parse("text/plain"), own.caller_id);
        RequestBody _name = null;
        if (!etUsersName.getText().toString().isEmpty())
            _name = RequestBody.create(MediaType.parse("text/plain"), etUsersName.getText().toString());
        RequestBody _email = null;
        RequestBody _profile = null;

        Call<APIResponse<ContactItem>> call = ServicesFactory.getService().updateProfile(_caller_id, _name, _email, _profile, _file);
        call.enqueue(new Callback<APIResponse<ContactItem>>() {
            @Override
            public void onResponse(Call<APIResponse<ContactItem>> call, Response<APIResponse<ContactItem>> response) {
                dissmisDialogs();
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    proPicPaths = null;
                    ContactItem updated = response.body().data;
                    updated.profiles = null;// Untuk syncronize firebase
                    // save to session and update local variable
                    own = SessionManager.saveProfile(RegisterProfileActivity.this, updated);
                    // update ui
                    if (isViewAttached){
                        toast("Update profile success");
                        startActivity(new Intent(RegisterProfileActivity.this, MainActivity.class));
                        PreferenceUtil.getEditor(RegisterProfileActivity.this).putBoolean(PreferenceUtil.NEWUSER, false).commit();
                        finish();
                    }

                } else {
                    String fail = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat_update_failed)
                            ? "Update Profile Failed"
                            : model.getLang().string.str_set_personal_stat_update_failed;
                    toast(fail);
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ContactItem>> call, Throwable t) {
                dissmisDialogs();
                String fail = TextUtils.isEmpty(model.getLang().string.str_set_personal_stat_update_failed)
                        ? "Update Profile Failed"
                        : model.getLang().string.str_set_personal_stat_update_failed;
                toast(fail);
            }
        });
    }

    private void createStatusOnly(final String status) {
        Call<APIResponse<String>> call = ServicesFactory.getService().updateCallerStatus(own.caller_id, status);
        call.enqueue(new Callback<APIResponse<String>>() {
            @Override
            public void onResponse(Call<APIResponse<String>> call, Response<APIResponse<String>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    toastShort("Update status success");
                    own.status_caller = status;
                    SessionManager.saveProfile(RegisterProfileActivity.this, own);
                } else
                    toastShort("Update status failed");
            }

            @Override
            public void onFailure(Call<APIResponse<String>> call, Throwable t) {
                toastShort("Update status failed");
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mImageSelectors.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        // Result for crop image
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                proPicPaths = new File(resultUri.getPath());

                Glide.with(RegisterProfileActivity.this)
                        .load(proPicPaths)
                        .apply(RequestOptions.circleCropTransform())
                        .into(ivUserPict);

                checkRegisterChanged();
            }
        }
            mImageSelectors.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * UI Threads
     * Added by Yudha Pratama Putra, 17 Okt 2018
     */
    private void showCustomDialog(String msg){
        if(!isViewAttached) // skip updating view
            return;
        try{
            dialogUpload.setMessage(msg);
        }catch (IllegalStateException e){
            e.printStackTrace();
            if (e instanceof IllegalStateException){
                return;
            }
        }
    }

    private void showDialogs(){
        if(!isViewAttached) // skip updating view
            return;
        try{
            dialogUpload = new DialogUpload();
            dialogUpload.show(getSupportFragmentManager(), null);
        }catch (IllegalStateException e){
            e.printStackTrace();
            if (e instanceof IllegalStateException){
                return;
            }
        }
    }

    private void dissmisDialogs(){
        if(!isViewAttached) // skip updating view
            return;
        try{
            dialogUpload.dismiss();
            dialogUpload = null;
        }catch (IllegalStateException e){
            e.printStackTrace();
            if (e instanceof IllegalStateException){
                return;
            }
        }
    }

    private boolean checkValidation() {
        boolean ret = true;
        if (!ValidationInputUtil.hasText(etUsersName)) ret = false;
        if (!ValidationInputUtil.hasText(etUsersPhone)) ret = false;
        return ret;
    }

}
