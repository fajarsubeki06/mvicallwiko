package com.sbimv.mvicalls.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.register.PhoneRegister;

import java.util.List;

public class PermissionVietnamActivity extends AppCompatActivity {

    Button buttonCalls, buttonContacts, buttonAudio;
    public static String permissionsButtonCalls[] = new String[]{
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE,
    };

    public static String permissionsContacts = Manifest.permission.READ_CONTACTS;
    public static String permissionsRecordAudio = Manifest.permission.RECORD_AUDIO;
    protected static LangViewModel model;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permission_vietnam);
        model = LangViewModel.getInstance();

        init();
    }

    private void init(){
        buttonCalls = findViewById(R.id.buttonPhoneCells);
        buttonContacts = findViewById(R.id.buttonContacts);
        buttonAudio = findViewById(R.id.buttonAudio);

        buttonCalls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissionAllowCalls();
            }
        });

        buttonContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissionsAllowContacts();
            }
        });

        buttonAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissionsAllowAudio();
            }
        });
    }



    protected void checkPermissionAllowCalls() {
        Dexter.withActivity(this).withPermissions(permissionsButtonCalls).withListener(new MultiplePermissionsListener() {

            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    Log.i("allow all permission", "All permissions are granted");
                } else if (!report.getDeniedPermissionResponses().isEmpty()) {
                    showSettingsDialog();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                Log.i("error persion", String.valueOf(error));
            }
        }).onSameThread().check();
    }

    protected void checkPermissionsAllowContacts(){
        Dexter.withActivity(this).withPermission(permissionsContacts).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                Log.i("allow all permission", "All permissions are granted");
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                if (response.isPermanentlyDenied()) {
                    showSettingsDialog();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                Log.i("error persion", String.valueOf(error));
            }
        }).onSameThread().check();
    }

    protected void checkPermissionsAllowAudio(){
        Dexter.withActivity(this).withPermission(permissionsRecordAudio).withListener(new PermissionListener() {
            @Override
            public void onPermissionGranted(PermissionGrantedResponse response) {
                Log.i("allow all permission", "All permissions are granted");
                startActivity(new Intent(PermissionVietnamActivity.this, PhoneRegister.class));
            }

            @Override
            public void onPermissionDenied(PermissionDeniedResponse response) {
                if (response.isPermanentlyDenied()) {
                    showSettingsDialog();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).withErrorListener(new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                Log.i("error persion", String.valueOf(error));
            }
        }).onSameThread().check();
    }

    protected void showSettingsDialog() {

        if (!PermissionVietnamActivity.this.isFinishing()) {
            final AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle(model.getLang().string.titlePermissionAlert)
                    .setMessage(model.getLang().string.bodyPermissionAlert)
                    .setPositiveButton(model.getLang().string.buttonPermissionAlert, null) //Set to null. We override the onclick
                    .setCancelable(false).create();

            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    Button buttonPositive = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                    buttonPositive.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            openSettings();
                        }
                    });

                }
            });
            dialog.show();
        }
    }

    protected void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
