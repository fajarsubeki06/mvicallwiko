package com.sbimv.mvicalls.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.adapter.AllDinamicVideoAdapter;
import com.sbimv.mvicalls.pojo.DataDinamicTitle;
import com.sbimv.mvicalls.util.ConfigPref;
import com.sbimv.mvicalls.util.PreferenceUtil;

import java.util.Objects;

public class AllPremiumDinamicVideoActivity extends BaseActivity {

    RecyclerView rvTrailer;
    DataDinamicTitle datas;
    private ShimmerFrameLayout shimmerFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_premium_video);
        rvTrailer = findViewById(R.id.rvVideoTrailer);
        shimmerFrameLayout = findViewById(R.id.shimmerVideoPremium);
        datas = getIntent().getParcelableExtra("data");

        setDefaultToolbar(true, datas.getCategory());
        initTrailerVideo();
        logFcb_screen_marketplaceEvent();

    }
    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logFcb_screen_marketplaceEvent () {
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_marketplace");
    }

    @Override
    protected void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
        LocalBroadcastManager.getInstance(AllPremiumDinamicVideoActivity.this).registerReceiver(popupNotifAppears,
                new IntentFilter(ConfigPref.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
        LocalBroadcastManager.getInstance(AllPremiumDinamicVideoActivity.this).unregisterReceiver(popupNotifAppears);
    }

    private void initTrailerVideo() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        int numOfColumn = 2;
        rvTrailer.setLayoutManager(new GridLayoutManager(this, numOfColumn));
        AllDinamicVideoAdapter ar = new AllDinamicVideoAdapter(datas);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                rvTrailer.setAdapter(ar);
                shimmerFrameLayout.setVisibility(View.GONE);
            }
        }, BaseActivity.TIME_LOADING);
        shimmerFrameLayout.stopShimmer();
    }

}
