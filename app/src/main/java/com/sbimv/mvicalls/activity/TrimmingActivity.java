package com.sbimv.mvicalls.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.material.snackbar.Snackbar;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.ConfigData;
import com.sbimv.mvicalls.util.FileUtils;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.Utils;
import com.sbimv.mvicalls.util.video_audio_merge.StringVideo;

import org.telegram.messenger.CustomVideoTimelinePlayView;
import org.telegram.messenger.TrimUtils;

import java.io.File;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TrimmingActivity extends BaseActivity implements View.OnClickListener {

    private static final int VIDEO_MIN_DURATION_MS = 3000;
    private static int VIDEO_MAX_DURATION_MS = 7000;
    private static final long VIDEO_MAX_SIZE = 10 * 1024 * 1024;

    FrameLayout videoViewWrapper;
    VideoView videoView;
    ImageView playBtn;
    TextView trimDurAndSizeTxt;
    TextView trimDurRangeTxt;
    CustomVideoTimelinePlayView timelineView;
    TextView trimBtn, cancelBtn;

    private Uri videoUri;
    private File videoFile;
    private float videoDuration;
    private long trimStartTime;
    private long trimEndTime;
    private long originalSize;
    private Disposable trimTask;
    private ProgressDialog progressDialog;
    private Runnable updateProgressRunnable;
    private LangViewModel model;
    File result;
    private StringVideo stringVideo = new StringVideo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trimming);
        setDefaultToolbar(true);

        initialize();
    }

    private void initialize() {
        model = LangViewModel.getInstance();
        videoViewWrapper = findViewById(R.id.videoViewWrapper);
        videoView = findViewById(R.id.videoView);
        playBtn = findViewById(R.id.playBtn);
        trimDurAndSizeTxt = findViewById(R.id.trimDurAndSizeTxt);
        trimDurRangeTxt = findViewById(R.id.trimDurRangeTxt);
        timelineView = findViewById(R.id.timelineView);
        trimBtn = findViewById(R.id.trimBtn);
        cancelBtn = findViewById(R.id.cancelBtn);

        videoViewWrapper.setOnClickListener(this);
        trimBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);

        String str_cancel = TextUtils.isEmpty(model.getLang().string.cancelWording)
                ? "Cancel"
                : model.getLang().string.cancelWording;

        String str_save = TextUtils.isEmpty(model.getLang().string.save)
                ? "Save"
                : model.getLang().string.save;

        cancelBtn.setText(str_cancel);
        trimBtn.setText(str_save);

        try{
            videoUri = getIntent().getParcelableExtra("path");
            String path = FileUtils.getRealPath(this, videoUri);
            videoFile = new File(path);

            initVideo();

        }catch (Exception e){
            String not = TextUtils.isEmpty(model.getLang().string.str_preview_video_not_support)
                    ? "File not Support"
                    : model.getLang().string.str_preview_video_not_support;
            toast(not); // File not supported
        }
    }

    private void initVideoTimelineView() {
        if(videoDuration>=(VIDEO_MIN_DURATION_MS)) {
            float minProgressDiff = VIDEO_MIN_DURATION_MS/videoDuration;
            timelineView.setMinProgressDiff(minProgressDiff);
        }

        ConfigData cdt = SessionManager.getConfigData(TrimmingActivity.this);
        if (cdt != null){
            int duration = cdt.max_duration;
            if (duration <= 0){
                VIDEO_MAX_DURATION_MS = 7000;
            }else{
                VIDEO_MAX_DURATION_MS = cdt.max_duration * 1000;
            }
        }

        if(videoDuration>=(VIDEO_MAX_DURATION_MS)) {
            float maxProgressDiff = VIDEO_MAX_DURATION_MS/videoDuration;
            timelineView.setMaxProgressDiff(maxProgressDiff);
        }
        if(videoDuration < VIDEO_MIN_DURATION_MS){
            String str_must_more = TextUtils.isEmpty(model.getLang().string.str_wording_more_than_video)
                    ? "Duration video must be more than"
                    : model.getLang().string.str_wording_more_than_video;
            String str_second = TextUtils.isEmpty(model.getLang().string.str_second)
                    ? "second"
                    : model.getLang().string.str_second;
            Toast.makeText(this, str_must_more + " " + VIDEO_MIN_DURATION_MS + " " + str_second, Toast.LENGTH_SHORT).show();
            trimBtn.setVisibility(View.GONE);
        }
        timelineView.setMaxVideoSize(VIDEO_MAX_SIZE,originalSize);
        timelineView.setDelegate(new CustomVideoTimelinePlayView.VideoTimelineViewDelegate() {

            @Override
            public void onLeftProgressChanged(float progress) {
                Log.d("onLeftProgressChanged ", String.valueOf(progress));
                if (videoView.isPlaying()) {
                    videoView.pause();
                }
                //videoView.seekTo((int) (videoDuration * progress));
                timelineView.setProgress(0);
                updateVideoInfo();
            }

            @Override
            public void onRightProgressChanged(float progress) {
                Log.d("onRightProgressChanged ", String.valueOf(progress));
                if (videoView.isPlaying()) {
                    videoView.pause();
                }
                //videoView.seekTo((int) (videoDuration * progress));
                timelineView.setProgress(0);
                updateVideoInfo();
            }

            @Override
            public void onPlayProgressChanged(float progress) {
                Log.d("onPlayProgressChanged ", String.valueOf(progress));
                videoView.seekTo((int) (videoDuration * progress));
            }

            @Override
            public void didStartDragging() {
                Log.d("didStartDragging", "");
            }

            @Override
            public void didStopDragging() {
                Log.d("didStopDragging", "");
                videoView.seekTo((int) (videoDuration * timelineView.getLeftProgress()));
            }
        });
        timelineView.setVideoPath(videoUri);
    }

    private void updateVideoInfo() {
        trimStartTime = (long) Math.ceil(timelineView.getLeftProgress()*videoDuration);
        trimEndTime = (long) Math.ceil(timelineView.getRightProgress()*videoDuration);
        long estimatedDuration = trimEndTime - trimStartTime;
        long estimatedSize = (int) (originalSize * ((float) estimatedDuration / videoDuration));
        String videoTimeSize = String.format(Locale.US,"%s, ~%s", Utils.getMinuteSeconds(estimatedDuration), Utils.formatFileSize(estimatedSize));
        trimDurAndSizeTxt.setText(videoTimeSize);
        String trimRangeDurStr = Utils.getMinuteSeconds(trimStartTime) + "-" + Utils.getMinuteSeconds(trimEndTime);
        trimDurRangeTxt.setText(trimRangeDurStr);
    }

    private void initVideo() {

        updateProgressRunnable = ()->{
            if(videoView==null||!videoView.isPlaying()) {
                timelineView.removeCallbacks(updateProgressRunnable);
            }
            updatePlayProgress();
            timelineView.postDelayed(updateProgressRunnable,17);
        };

        originalSize = videoFile.length();
        videoView.setOnPreparedListener(mediaPlayer -> {
            videoDuration = mediaPlayer.getDuration();
            initVideoTimelineView();
            playBtn.setVisibility(View.VISIBLE);
            updateVideoInfo();
        });

        videoView.setOnErrorListener((mediaPlayer, i, i1) -> {
            showAlertErrorPlay();
            trimBtn.setVisibility(View.GONE);
            return true;
        });
        //videoView.setMediaController(new MediaController(this));
        videoView.setVideoURI(videoUri);
    }

    private void showAlertErrorPlay(){
        if (!TrimmingActivity.this.isFinishing()) {

            String sTitle = TextUtils.isEmpty(model.getLang().string.str_cannot_play)
                    ? "Can't play this video."
                    : model.getLang().string.str_cannot_play;

            String sApprove = TextUtils.isEmpty(model.getLang().string.approveCofirm)
                    ? "Ok"
                    : model.getLang().string.approveCofirm;

            new androidx.appcompat.app.AlertDialog.Builder(this)
                    .setMessage(sTitle)
                    .setCancelable(false)
                    .setPositiveButton(sApprove, (dialog, which) -> {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).show();
        }
    }

    private void updatePlayProgress() {
        float progress = 0;
        if (videoView != null) {
            progress = videoView.getCurrentPosition() / (float) videoView.getDuration();
            if (timelineView.getVisibility() == View.VISIBLE) {
                progress -= timelineView.getLeftProgress();
                if (progress < 0) {
                    progress = 0;
                }
                progress /= (timelineView.getRightProgress() - timelineView.getLeftProgress());
                if (progress > 1) {
                    progress = 1;
                }
            }
        }
        timelineView.setProgress(progress);
    }

    private Timer trimDurCounterTimer;

    @Override
    public void onClick(View view) {
        if (videoViewWrapper == view){
            if(videoView.isPlaying()) {
                trimDurCounterTimer.cancel();
                videoView.pause();
                playBtn.setVisibility(View.VISIBLE);
            } else {
                trimDurCounterTimer = new Timer();
                trimDurCounterTimer.scheduleAtFixedRate(new TimerTask() {
                    long currentTime;
                    @Override
                    public void run() {
                        try{
                            currentTime = videoView.getCurrentPosition();
                            String trimRangeDurStr = Utils.getMinuteSeconds(currentTime) + "-" + Utils.getMinuteSeconds(trimEndTime);
                            runOnUiThread(()-> trimDurRangeTxt.setText(trimRangeDurStr));
                            if(currentTime>=trimEndTime) {
                                trimDurCounterTimer.cancel();
                                runOnUiThread(()->{
                                    videoView.pause();
                                    videoView.seekTo((int)trimStartTime);
                                    String trimRangeDurStr2 = Utils.getMinuteSeconds(trimStartTime) + "-" + Utils.getMinuteSeconds(trimEndTime);
                                    trimDurRangeTxt.setText(trimRangeDurStr2);
                                });
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },0,100);
                videoView.start();
                timelineView.post(updateProgressRunnable);
                playBtn.setVisibility(View.GONE);
            }

        }else if (trimBtn == view){

            try{
                String path = Environment.getExternalStorageDirectory().toString() + File.separator + stringVideo.APP_FOLDER + File.separator;
//                File outDir = Environment.getExternalStorageDirectory().toString() + File.separator + stringVideo.APP_FOLDER + File.separator;
                File outDir = new File(path);
                //File outDir = Environment.getExternalStorageDirectory();
                Completable trimCompletable = Completable.fromAction(()->{
                    result = TrimUtils.trimVideo(videoFile,outDir,(int)trimStartTime,(int)trimEndTime);
                    Log.d("Trim result ", result.getAbsolutePath());
                });
                trimTask = trimCompletable
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(disposable -> {

                            String str_loading = TextUtils.isEmpty(model.getLang().string.str_wording_trim_progress)
                                    ? "Trim in progress, please wait..."
                                    : model.getLang().string.str_wording_trim_progress;

                            showLoading(true, str_loading);
                        })
                        .subscribe(()->{

                            String str_success = TextUtils.isEmpty(model.getLang().string.success)
                                    ? "Success"
                                    : model.getLang().string.success;

                            showLoading(false,null);
                            showMessage(str_success);
                            Intent intent = new Intent();
                            intent.setDataAndType(Uri.fromFile(result), "video/mp4");
                            setResult(RESULT_OK, intent);
                            finish();
                        },throwable -> {
                            throwable.printStackTrace();
                            showLoading(false,null);
                            showMessage(throwable.getMessage());
                        });
            }catch (Exception e){
                showLoading(false, null);
                e.printStackTrace();
            }

        }else if (cancelBtn == view){
            try{
                if (videoView != null){
                    videoView.stopPlayback();
                    if (trimTask != null){
                        trimTask.dispose();
                    }
                    finish();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void showMessage(String message) {
        Snackbar.make(findViewById(android.R.id.content),message,Snackbar.LENGTH_LONG).show();
    }

    private void showLoading(boolean show,String message) {
        try{
            if (!TrimmingActivity.this.isFinishing()){
                if(show) {
                    progressDialog = new ProgressDialog(TrimmingActivity.this);
                    progressDialog.setMessage(message);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                } else if(progressDialog!=null&&progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        if(trimTask!=null) {
            trimTask.dispose();
        }
        super.onDestroy();
    }
}
