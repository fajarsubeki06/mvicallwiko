package com.sbimv.mvicalls.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDex;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.sbimv.mvicalls.AppPreference;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.icallservices.Log;
import com.sbimv.mvicalls.intro.IntroActivityFirst;
import com.sbimv.mvicalls.intro.IntroActivitySecond;
import com.sbimv.mvicalls.intro.IntroActivityViet;
import com.sbimv.mvicalls.receiver.DailyNotifReceiver;
import com.sbimv.mvicalls.register.PhoneRegister;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.walktrough.FriendsDemoActivity;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;
import com.sbimv.mvicalls.walktrough.ShareDemoActivity;


/**
 * Created by Hendi on 10/20/17.
 */

public class SplashActivity extends AppCompatActivity {

    public static final int ALARM_CODE_ONEDAY = 11;
    private static final String SWITCH_INTRO = "switch_intro"; // * Required for trigger value (Firebase Remote Config)....
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        FirebaseApp.initializeApp(SplashActivity.this);

        ImageView imgLogoMv = findViewById(R.id.imgLogoMv);

        Glide.with(SplashActivity.this)
                .load(getResources().getIdentifier("logo_rebranding", "drawable", SplashActivity.this.getPackageName()))
                .into(imgLogoMv);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String channel = bundle.getString("direction");
            String strTopic = bundle.getString("topic");

            if (!TextUtils.isEmpty(channel)) {
                PreferenceUtil.getEditor(SplashActivity.this).putString(PreferenceUtil.DIRECTION, channel).commit();
            } else if (!TextUtils.isEmpty(strTopic)) {
                PreferenceUtil.getEditor(SplashActivity.this).putString(PreferenceUtil.DIRECT_TO_PAGE, strTopic).commit();
            }
        }


        Intent intent = getIntent();
        if (intent.getData() != null) {
            Uri uri = intent.getData();
            String scheme = uri.getScheme();
            if (!TextUtils.isEmpty(scheme) && scheme.equals("tel")) {
                String decode = Uri.decode(uri != null ? uri.getSchemeSpecificPart() : null);
                CharSequence charSequence = decode;
                if (charSequence != null) {
                    String subscribeInfo = charSequence.toString();
                    if (!TextUtils.isEmpty(subscribeInfo)) {
                        callService(subscribeInfo);
                        finish();
                        return;
                    }
                }
            }
        }

        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance(); // Firebase Remote Config - Yudha Pratama Puta, 15-01-2019
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(false).build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults); // * Required default value (Firebase Remote Config)....
        fetchSwitchIntro();

//        /*Push notif if user not open app after install
//         * FLAG to check user allready register or not, set true in MainActivity*/
        boolean isFirstOpen = PreferenceUtil.getPref(SplashActivity.this).getBoolean(PreferenceUtil.FIRST_OPEN_APP, false);
        if (!isFirstOpen) {
            setInterValPushNotif();
        }

    }

    private void callService(String subscribeInfo) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(subscribeInfo)));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }

    private void setInterValPushNotif() {
        final int eightHour = 8 * 60 * 60 * 1000;

        Intent intent = new Intent(SplashActivity.this, DailyNotifReceiver.class);
        intent.putExtra("req_code", ALARM_CODE_ONEDAY);
        PreferenceUtil.getEditor(SplashActivity.this).putInt(PreferenceUtil.REQ_CODE, 8).commit();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(SplashActivity.this, 8, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager manager = (AlarmManager) SplashActivity.this.getSystemService(ALARM_SERVICE);
        if (manager != null) {
            int SDK_INT = Build.VERSION.SDK_INT;
            if (SDK_INT < Build.VERSION_CODES.KITKAT)
                manager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + eightHour, pendingIntent);
            else if (Build.VERSION_CODES.KITKAT < SDK_INT && SDK_INT < Build.VERSION_CODES.M)
                manager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + eightHour, pendingIntent);
            else if (SDK_INT > Build.VERSION_CODES.M) {
                manager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + eightHour, pendingIntent);
            }
        }
        PreferenceUtil.getEditor(SplashActivity.this).putBoolean(PreferenceUtil.FIRST_OPEN_APP, true).commit();
    }

    private void fetchSwitchIntro() { // Firebase Remote Config - Yudha Pratama Puta, 15-01-2019
        long cacheExpiration = 10; // 1 hour in seconds.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mFirebaseRemoteConfig.activateFetched();
                }
                showIntro();
            }
        });
    }

    private void showIntro() { // Firebase Remote Config - Yudha Pratama Puta, 15-01-2019
        if (mFirebaseRemoteConfig.getBoolean(SWITCH_INTRO)) {
            activeSplash(true);
        } else {
            activeSplash(false);
        }
    }


    private void activeSplash(final boolean isNewIntro) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean isLogin = PreferenceUtil.getPref(SplashActivity.this).getBoolean(PreferenceUtil.ISLOGIN, false); //Is Login
                if (isNewIntro) {
                    schemaNewIntro(isLogin);
                } else {
                    schemaOldIntro(isLogin);
                }
            }
        }, 500);

    }

    private void schemaNewIntro(boolean isLogin) { // Schema New Intro - Yudha Pratama Puta, 15-01-2019
        boolean isIntroFirst = PreferenceUtil.getPref(SplashActivity.this).getBoolean(PreferenceUtil.INTRO_ACTIVITY_FIRST, false);
        PreferenceUtil.getEditor(SplashActivity.this).putBoolean(PreferenceUtil.ISWITCH, true).commit();

        if (isLogin) {

            boolean isProfileDone = PreferenceUtil.getPref(this).getBoolean(PreferenceUtil.SETUP_PROFILE,false);
            boolean isFriendDone = PreferenceUtil.getPref(this).getBoolean(PreferenceUtil.SETUP_CONTACT,false);
            boolean isShareDone = PreferenceUtil.getPref(this).getBoolean(PreferenceUtil.SETUP_SHARE,false);
            boolean isVideoToneDone = PreferenceUtil.getPref(this).getBoolean(PreferenceUtil.SETUP_VIDEOTONE,false);

            boolean isIntroSecond = PreferenceUtil.getPref(SplashActivity.this).getBoolean(PreferenceUtil.INTRO_ACTIVITY_SECOND, false);
            if (!isIntroSecond) {
                startActivity(new Intent(SplashActivity.this, IntroActivitySecond.class));
                finish();
            } else {

                if (!isProfileDone && !isVideoToneDone && !isFriendDone && !isShareDone ){
                    startActivity(new Intent(this, SetupProfileActivity.class));
                }else if(isProfileDone && !isVideoToneDone && !isFriendDone && !isShareDone){
                    String ccodeViet = PreferenceUtil.getPref(SplashActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
                    assert ccodeViet != null;
                    if (!TextUtils.isEmpty(ccodeViet) && !ccodeViet.equalsIgnoreCase("+84")){
                        startActivity(new Intent(this, VideoToneActivity.class));
                    }
                }else if (isProfileDone && isVideoToneDone && !isFriendDone && !isShareDone){
                    startActivity(new Intent(this, FriendsDemoActivity.class));
                }else if (isProfileDone && isVideoToneDone && isFriendDone && !isShareDone){
                    startActivity(new Intent(this, ShareDemoActivity.class));
                }else {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    intent.setData(getIntent().getData());
                    startActivity(intent);
                }
                finish();

            }
        } else {
            if (!isIntroFirst) {
                String getLanguage = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.GET_LANG, "");
                if (!TextUtils.isEmpty(getLanguage)){
                    if (getLanguage.equalsIgnoreCase("vi") || getLanguage.equalsIgnoreCase("vn")){
                        startActivity(new Intent(SplashActivity.this, IntroActivityViet.class));
                        finish();
                    }else{
                        startActivity(new Intent(SplashActivity.this, IntroActivityFirst.class));
                        finish();
                    }
                }

            } else {
                String getLanguage = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.GET_LANG, "");

                if (!TextUtils.isEmpty(getLanguage)){
                    if (getLanguage.equalsIgnoreCase("vi") || getLanguage.equalsIgnoreCase("vn")){
                        //startActivity(new Intent(SplashActivity.this, IntroVietnamActivity.class));
                        startActivity(new Intent(SplashActivity.this, IntroActivityViet.class));
                        finish();
                    }else{
                        startActivity(new Intent(SplashActivity.this, PhoneRegister.class));
                        finish();
                    }
                }
            }
        }
    }

    private void schemaOldIntro(boolean isLogin) { // Schema Old Intro - Yudha Pratama Puta, 15-01-2019

        boolean isProfileDone = PreferenceUtil.getPref(this).getBoolean(PreferenceUtil.SETUP_PROFILE,false);
        boolean isFriendDone = PreferenceUtil.getPref(this).getBoolean(PreferenceUtil.SETUP_CONTACT,false);
        boolean isShareDone = PreferenceUtil.getPref(this).getBoolean(PreferenceUtil.SETUP_SHARE,false);
        boolean isVideoToneDone = PreferenceUtil.getPref(this).getBoolean(PreferenceUtil.SETUP_VIDEOTONE,false);
        Log.e("PROFILE", String.valueOf(isProfileDone));
        Log.e("FRIEND", String.valueOf(isFriendDone));
        Log.e("SHARE", String.valueOf(isShareDone));

        boolean isFirstRun = PreferenceUtil.getPref(SplashActivity.this).getBoolean(PreferenceUtil.INTRO, false); //Is Login
        PreferenceUtil.getEditor(SplashActivity.this).putBoolean(PreferenceUtil.ISWITCH, false).commit();

        if (isLogin) {
            if (isFirstRun) {
                Intent itn = new Intent(SplashActivity.this, IntroActivity.class);
                startActivity(itn);
                finish();
            } else {

                if (!isProfileDone && !isVideoToneDone && !isFriendDone && !isShareDone ){
                    startActivity(new Intent(this, SetupProfileActivity.class));
                }else if(isProfileDone && !isVideoToneDone && !isFriendDone && !isShareDone){
                    String ccodeViet = PreferenceUtil.getPref(SplashActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
                    assert ccodeViet != null;
                    if (!TextUtils.isEmpty(ccodeViet) && !ccodeViet.equalsIgnoreCase("+84")){
                        startActivity(new Intent(this, VideoToneActivity.class));
                    }
                }else if (isProfileDone && isVideoToneDone && !isFriendDone && !isShareDone){
                    startActivity(new Intent(this, FriendsDemoActivity.class));
                }else if (isProfileDone && isVideoToneDone && isFriendDone && !isShareDone){
                    startActivity(new Intent(this, ShareDemoActivity.class));
                }else {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    intent.setData(getIntent().getData());
                    startActivity(intent);
                }
                finish();

            }
        } else {
            String getLanguage = PreferenceUtil.getPref(getApplicationContext()).getString(PreferenceUtil.GET_LANG, "");

            if (!TextUtils.isEmpty(getLanguage)){
                if (getLanguage.equalsIgnoreCase("vi") || getLanguage.equalsIgnoreCase("vn")){
                    //startActivity(new Intent(SplashActivity.this, IntroVietnamActivity.class));
                    startActivity(new Intent(SplashActivity.this, IntroActivityViet.class));
                }else{
                    startActivity(new Intent(SplashActivity.this, PhoneRegister.class));
                    finish();
                }
            }
        }
    }

}
