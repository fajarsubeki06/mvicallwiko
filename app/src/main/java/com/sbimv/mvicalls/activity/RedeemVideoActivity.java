package com.sbimv.mvicalls.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.R;
import com.sbimv.mvicalls.adapter.RedeemVideoAdapter;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataRedeemReward;
import com.sbimv.mvicalls.pojo.DataRewards;
import com.sbimv.mvicalls.pojo.MyCollection;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RedeemVideoActivity extends BaseActivity {
    String listContent_ID;
    private RecyclerView rvRedeemVideo;
    private ArrayList<MyCollection> listVideo = new ArrayList();
    private DataRewards.ListRewards dataReward;
    private int item;
    private Button btnSubmitReward;
    private int total_item;
    private ContactItem own;
    private ArrayList<String> contentID = new ArrayList<>();
    public BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("select_video_reward")){

                item = intent.getIntExtra("itemVideo", 0);
                contentID = intent.getStringArrayListExtra("contentID");

                listContent_ID = new Gson().toJson(contentID, new TypeToken<ArrayList<String>>() {
                }.getType());

                final String strItem = String.valueOf(item);
                if (item == total_item) {
                    ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(btnSubmitReward, View.ALPHA, 0.0f, 1.0f);
                    alphaAnimator.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(final Animator animation) {
                            btnSubmitReward.setText(model.getLang().string.choose5Video.replace("[ITEM]", String.valueOf(total_item)));
                            btnSubmitReward.setBackground(getResources().getDrawable(R.drawable.rounded_white_green_stroke));
                            btnSubmitReward.setTextColor(getResources().getColor(R.color.colorPrimary));
                            btnSubmitReward.setEnabled(false);
                        }
                    });
                    alphaAnimator.setDuration(500);
                    alphaAnimator.start();

                } else if (item > 0) {
                    ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(btnSubmitReward, View.ALPHA, 0.0f, 1.0f);
                    alphaAnimator.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(final Animator animation) {
                            btnSubmitReward.setText(model.getLang().string.choose5MoreVideo.replace("[ITEM]", strItem));
                            btnSubmitReward.setBackground(getResources().getDrawable(R.drawable.rounded_white_green_stroke));
                            btnSubmitReward.setTextColor(getResources().getColor(R.color.colorPrimary));
                            btnSubmitReward.setEnabled(false);
                        }
                    });
                    alphaAnimator.setDuration(500);
                    alphaAnimator.start();

                } else {
                    ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(btnSubmitReward, View.ALPHA, 0.0f, 1.0f);
                    alphaAnimator.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(final Animator animation) {
                            btnSubmitReward.setBackground(getResources().getDrawable(R.drawable.rounded_green_whatsapp));
                            btnSubmitReward.setTextColor(getResources().getColor(R.color.white));
                            btnSubmitReward.setText(model.getLang().string.redeemNow);
                            btnSubmitReward.setEnabled(true);
                        }
                    });
                    alphaAnimator.setDuration(500);
                    alphaAnimator.start();

                }

            }
        }
    };
    private Button btnRedeem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem_video);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            dataReward = bundle.getParcelable("dataReward");
        }


        btnRedeem = findViewById(R.id.btnSubmitRewards);
        btnRedeem.setText(model.getLang().string.choose5Video);
        TextView tbTitle = findViewById(R.id.tv_title_menu);
        tbTitle.setSingleLine(true);
        tbTitle.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tbTitle.setMarqueeRepeatLimit(3);
        tbTitle.setSelected(true);

        setDefaultToolbar(true, dataReward.getHeadline());
        own = SessionManager.getProfile(RedeemVideoActivity.this);
        rvRedeemVideo = findViewById(R.id.rvRedeemVideo);
        btnSubmitReward = findViewById(R.id.btnSubmitRewards);
        initRecycler();
        loadRedeemVideo();

        LocalBroadcastManager.getInstance(RedeemVideoActivity.this)
                .registerReceiver(receiver, new IntentFilter("select_video_reward"));

        btnSubmitReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doRedeem();
            }
        });
    }

    private void doRedeem() {

        Call<APIResponse<DataRedeemReward>> call = ServicesFactory.getService().getRedeemReward(own.msisdn, null, null, dataReward.getReward_id(), "redeem_video", listContent_ID);
        call.enqueue(new Callback<APIResponse<DataRedeemReward>>() {
            @Override
            public void onResponse(Call<APIResponse<DataRedeemReward>> call, Response<APIResponse<DataRedeemReward>> response) {
                if (response.isSuccessful()) {
                    String mssge = model.getLang().string.wordingSuccessRedeem.replace("[ITEM]", dataReward.getHeadline());
                    AlertDialog.Builder builder = new AlertDialog.Builder(RedeemVideoActivity.this);
                    builder.setTitle(model.getLang().string.success);
                    builder.setMessage(mssge);
                    builder.setCancelable(false);
                    builder.setPositiveButton(model.getLang().string.next, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(RedeemVideoActivity.this, UploadVideoToneActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    });
                    builder.setNegativeButton(model.getLang().string.back, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }

            @Override
            public void onFailure(Call<APIResponse<DataRedeemReward>> call, Throwable t) {

            }
        });
    }

    private void initRecycler() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        rvRedeemVideo.setLayoutManager(new GridLayoutManager(this, 2));
        RedeemVideoAdapter adapter = new RedeemVideoAdapter(listVideo, this, dataReward.getReward_id(), total_item);
        rvRedeemVideo.setAdapter(adapter);
    }

    private void loadRedeemVideo() {
        Call<APIResponse<DataRedeemReward>> call = ServicesFactory.getService().getRedeemReward(own.msisdn, dataReward.getType(), dataReward.getPrice(), dataReward.getReward_id(), null, null);
        call.enqueue(new Callback<APIResponse<DataRedeemReward>>() {
            @Override
            public void onResponse(Call<APIResponse<DataRedeemReward>> call, Response<APIResponse<DataRedeemReward>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {
                    btnSubmitReward.setVisibility(View.VISIBLE);
                    total_item = Integer.parseInt(response.body().data.getTotalItem());
                    btnSubmitReward.setText(model.getLang().string.choose5Video.replace("[ITEM]", String.valueOf(total_item)));
                    ArrayList<MyCollection> datas = response.body().data.getVideoList();
                    if (datas.size() != 0) {
                        listVideo.clear();
                        listVideo.addAll(datas);
                        rvRedeemVideo.getAdapter().notifyDataSetChanged();
                    }
                    initRecycler();
                }
            }

            @Override
            public void onFailure(Call<APIResponse<DataRedeemReward>> call, Throwable t) {

            }
        });
    }
}
