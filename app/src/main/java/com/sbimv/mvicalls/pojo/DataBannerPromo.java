package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class DataBannerPromo implements Parcelable {

    private String banner;
    private String url;
    private String source_content;


    protected DataBannerPromo(Parcel in) {
        banner = in.readString();
        url = in.readString();
        source_content = in.readString();
    }

    public static final Creator<DataBannerPromo> CREATOR = new Creator<DataBannerPromo>() {
        @Override
        public DataBannerPromo createFromParcel(Parcel in) {
            return new DataBannerPromo(in);
        }

        @Override
        public DataBannerPromo[] newArray(int size) {
            return new DataBannerPromo[size];
        }
    };

    public String getBanner() {
        return banner;
    }

    public String getUrl() {
        return url;
    }

    public String getSource_content() {
        return source_content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(banner);
        parcel.writeString(url);
        parcel.writeString(source_content);
    }
}
