package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class DataRedeemReward implements Parcelable {
    private String totalItem;
    private ArrayList<MyCollection> videoList;

    protected DataRedeemReward(Parcel in) {
        totalItem = in.readString();
        videoList = in.createTypedArrayList(MyCollection.CREATOR);
    }

    public static final Creator<DataRedeemReward> CREATOR = new Creator<DataRedeemReward>() {
        @Override
        public DataRedeemReward createFromParcel(Parcel in) {
            return new DataRedeemReward(in);
        }

        @Override
        public DataRedeemReward[] newArray(int size) {
            return new DataRedeemReward[size];
        }
    };

    public String getTotalItem() {
        return totalItem;
    }

    public ArrayList<MyCollection> getVideoList() {
        return videoList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(totalItem);
        parcel.writeTypedList(videoList);
    }
}
