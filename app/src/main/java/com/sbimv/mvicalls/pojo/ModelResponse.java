package com.sbimv.mvicalls.pojo;

public class ModelResponse {

    public String caller_id;
    public String caller_id_like;
    public String video_caller;

    public ModelResponse(String caller_id, String caller_id_like, String video_caller) {
        this.caller_id = caller_id;
        this.caller_id_like = caller_id_like;
        this.video_caller = video_caller;
    }

    public String getCaller_id() {
        return caller_id;
    }

    public void setCaller_id(String caller_id) {
        this.caller_id = caller_id;
    }

    public String getCaller_id_like() {
        return caller_id_like;
    }

    public void setCaller_id_like(String caller_id_like) {
        this.caller_id_like = caller_id_like;
    }

    public String getVideo_caller() {
        return video_caller;
    }

    public void setVideo_caller(String video_caller) {
        this.video_caller = video_caller;
    }
}
