package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class DataWellcomePush implements Parcelable {

    private String title;
    private String content;
    private String titleScase;
    private String contentScase;

    protected DataWellcomePush(Parcel in) {
        title = in.readString();
        content = in.readString();
        titleScase = in.readString();
        contentScase = in.readString();
    }

    public static final Creator<DataWellcomePush> CREATOR = new Creator<DataWellcomePush>() {
        @Override
        public DataWellcomePush createFromParcel(Parcel in) {
            return new DataWellcomePush(in);
        }

        @Override
        public DataWellcomePush[] newArray(int size) {
            return new DataWellcomePush[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getTitleScase() {
        return titleScase;
    }

    public String getContentScase() {
        return contentScase;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(content);
        parcel.writeString(titleScase);
        parcel.writeString(contentScase);
    }
}
