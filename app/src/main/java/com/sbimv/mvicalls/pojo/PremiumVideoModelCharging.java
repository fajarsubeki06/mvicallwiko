package com.sbimv.mvicalls.pojo;

/**
 * Created by admin on 12/25/2017.
 */

public class PremiumVideoModelCharging {
    private String artis_id;
    private String name;
    private String price;
    private String artis_image;
    private String type;
    private String judul;
    private String source_content;
    private String category;
    private String label;
    private String thumb_pic;
    private String content_id;


    public PremiumVideoModelCharging() {
        this.artis_id = artis_id;
        this.name = name;
        this.price = price;
        this.artis_image = artis_image;
        this.type = type;
        this.judul = judul;
        this.source_content = source_content;
        this.category = category;
        this.label = label;
        this.thumb_pic = thumb_pic;
        this.content_id = content_id;
    }

    public String getArtis_id() {
        return artis_id;
    }

    public void setArtis_id(String artis_id) {
        this.artis_id = artis_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getArtis_image() {
        return artis_image;
    }

    public void setArtis_image(String artis_image) {
        this.artis_image = artis_image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getSource_content() {
        return source_content;
    }

    public void setSource_content(String source_content) {
        this.source_content = source_content;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getThumb_pic() {
        return thumb_pic;
    }

    public void setThumb_pic(String thumb_pic) {
        this.thumb_pic = thumb_pic;
    }

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }
}

