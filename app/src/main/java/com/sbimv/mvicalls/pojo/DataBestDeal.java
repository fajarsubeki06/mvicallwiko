package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class DataBestDeal implements Parcelable {

    private String deal_id;
    private String deal_banner;
    private String full_lp;
    private String source_content;
    private String operator_id;
    private String status;
    private String promo;
    private String start_date;
    private String end_date;
    private String err_message;

    public DataBestDeal() {
    }

    protected DataBestDeal(Parcel in) {
        deal_id = in.readString();
        deal_banner = in.readString();
        full_lp = in.readString();
        source_content = in.readString();
        operator_id = in.readString();
        status = in.readString();
        promo = in.readString();
        start_date = in.readString();
        end_date = in.readString();
        err_message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(deal_id);
        dest.writeString(deal_banner);
        dest.writeString(full_lp);
        dest.writeString(source_content);
        dest.writeString(operator_id);
        dest.writeString(status);
        dest.writeString(promo);
        dest.writeString(start_date);
        dest.writeString(end_date);
        dest.writeString(err_message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DataBestDeal> CREATOR = new Creator<DataBestDeal>() {
        @Override
        public DataBestDeal createFromParcel(Parcel in) {
            return new DataBestDeal(in);
        }

        @Override
        public DataBestDeal[] newArray(int size) {
            return new DataBestDeal[size];
        }
    };

    public String getDeal_id() {
        return deal_id;
    }

    public void setDeal_id(String deal_id) {
        this.deal_id = deal_id;
    }

    public String getDeal_banner() {
        return deal_banner;
    }

    public void setDeal_banner(String deal_banner) {
        this.deal_banner = deal_banner;
    }

    public String getFull_lp() {
        return full_lp;
    }

    public void setFull_lp(String full_lp) {
        this.full_lp = full_lp;
    }

    public String getSource_content() {
        return source_content;
    }

    public void setSource_content(String source_content) {
        this.source_content = source_content;
    }

    public String getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(String operator_id) {
        this.operator_id = operator_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPromo() {
        return promo;
    }

    public void setPromo(String promo) {
        this.promo = promo;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getErr_message() {
        return err_message;
    }

    public void setErr_message(String err_message) {
        this.err_message = err_message;
    }
}
