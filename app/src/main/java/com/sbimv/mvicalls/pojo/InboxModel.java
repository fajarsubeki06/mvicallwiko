package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class InboxModel implements Parcelable {

    public String id;
    public String title;
    public String msisdn;
    public String message;
    public String status;
    public String created_date;

    public InboxModel(){}

    protected InboxModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        msisdn = in.readString();
        message = in.readString();
        status = in.readString();
        created_date = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InboxModel> CREATOR = new Creator<InboxModel>() {
        @Override
        public InboxModel createFromParcel(Parcel in) {
            return new InboxModel(in);
        }

        @Override
        public InboxModel[] newArray(int size) {
            return new InboxModel[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(msisdn);
        dest.writeString(message);
        dest.writeString(status);
        dest.writeString(created_date);
    }
}
