package com.sbimv.mvicalls.pojo;

public class ModelOTP{
    public String msisdn;
    public String otp;

    public ModelOTP(String msisdn, String otp) {
        this.msisdn = msisdn;
        this.otp = otp;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getOtp() {
        return otp;
    }
}
