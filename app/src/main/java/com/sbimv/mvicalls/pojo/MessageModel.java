package com.sbimv.mvicalls.pojo;

public class MessageModel {

    public String title;
    public String message;
    public String timestamp;

    public MessageModel(String title, String message, String timestamp) {
        this.title = title;
        this.message = message;
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public String getMessage() {
        return message;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
