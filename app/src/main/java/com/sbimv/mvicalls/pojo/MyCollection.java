package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 12/27/2017.
 */

public class MyCollection implements Parcelable {

    public String caller_id;
    public String content_id;
    public String alias ;
    public String judul;
    public String source_content;
    public String thumb_pic;
    public String sub_start ;
    public String sub_end ;
    public String status;
    public boolean isChecked;

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    protected MyCollection(Parcel in) {
        caller_id = in.readString();
        content_id = in.readString();
        alias = in.readString();
        judul = in.readString();
        source_content = in.readString();
        thumb_pic = in.readString();
        sub_start = in.readString();
        sub_end = in.readString();
        status = in.readString();
        isChecked = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(caller_id);
        dest.writeString(content_id);
        dest.writeString(alias);
        dest.writeString(judul);
        dest.writeString(source_content);
        dest.writeString(thumb_pic);
        dest.writeString(sub_start);
        dest.writeString(sub_end);
        dest.writeString(status);
        dest.writeByte((byte) (isChecked ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MyCollection> CREATOR = new Creator<MyCollection>() {
        @Override
        public MyCollection createFromParcel(Parcel in) {
            return new MyCollection(in);
        }

        @Override
        public MyCollection[] newArray(int size) {
            return new MyCollection[size];
        }
    };
}
