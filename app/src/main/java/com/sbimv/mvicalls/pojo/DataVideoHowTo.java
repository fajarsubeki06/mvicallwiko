package com.sbimv.mvicalls.pojo;

import android.text.TextUtils;

public class DataVideoHowTo {

    private String video_code;
    private String howtouse;
    private String faq;

    public DataVideoHowTo() {
    }

    public DataVideoHowTo(String video_code, String howtouse, String faq) {
        this.video_code = video_code;
        this.howtouse = howtouse;
        this.faq = faq;
    }

    public String getVideo_code() {
        return TextUtils.isEmpty(video_code)?"":video_code;
    }

    public void setVideo_code(String video_code) {
        this.video_code = video_code;
    }

    public String getHowtouse() {
        return TextUtils.isEmpty(howtouse)?"":howtouse;
    }

    public void setHowtouse(String howtouse) {
        this.howtouse = howtouse;
    }

    public String getFaq() {
        return TextUtils.isEmpty(faq)?"":faq;
    }

    public void setFaq(String faq) {
        this.faq = faq;
    }
}
