package com.sbimv.mvicalls.pojo;

public class ModelTopic {
    public String topic;
    public boolean active;

    public ModelTopic(String topic, boolean active) {
        this.topic = topic;
        this.active = active;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
