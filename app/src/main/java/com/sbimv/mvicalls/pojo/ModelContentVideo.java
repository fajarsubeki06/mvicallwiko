package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelContentVideo {
    public String caller_id;
    public String name;
    public String name_pic;
    public String thumb_video;
    public String video_caller;
    public String last_updated;
    public String counter;
    public int like_status;
    public int report_status;


    public String getCaller_id() {
        return caller_id;
    }

    public void setCaller_id(String caller_id) {
        this.caller_id = caller_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName_pic() {
        return name_pic;
    }

    public void setName_pic(String name_pic) {
        this.name_pic = name_pic;
    }

    public String getThumb_video() {
        return thumb_video;
    }

    public void setThumb_video(String thumb_video) {
        this.thumb_video = thumb_video;
    }

    public String getVideo_caller() {
        return video_caller;
    }

    public void setVideo_caller(String video_caller) {
        this.video_caller = video_caller;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public int getLike_status() {
        return like_status;
    }

    public void setLike_status(int like_status) {
        this.like_status = like_status;
    }

    public int getReport_status() {
        return report_status;
    }

    public void setReport_status(int report_status) {
        this.report_status = report_status;
    }
}
