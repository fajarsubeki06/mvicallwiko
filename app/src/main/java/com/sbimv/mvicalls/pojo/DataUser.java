package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class DataUser implements Parcelable{

    public Integer code ;
    public String error_message ;
    public Object data ;

    public String caller_id;
    public String msisdn;
    public String name;
    public String status_caller;
    public String video_caller;
    //    public String status;
//    public String created_date;
    public String email;
    public String caller_pic;
    public String serialsim;


    protected DataUser(Parcel in) {
        if (in.readByte() == 0) {
            code = null;
        } else {
            code = in.readInt();
        }
        error_message = in.readString();
        caller_id = in.readString();
        msisdn = in.readString();
        name = in.readString();
        status_caller = in.readString();
        video_caller = in.readString();
        email = in.readString();
        caller_pic = in.readString();
        serialsim = in.readString();
    }

    public static final Creator<DataUser> CREATOR = new Creator<DataUser>() {
        @Override
        public DataUser createFromParcel(Parcel in) {
            return new DataUser(in);
        }

        @Override
        public DataUser[] newArray(int size) {
            return new DataUser[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (code == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(code);
        }
        dest.writeString(error_message);
        dest.writeString(caller_id);
        dest.writeString(msisdn);
        dest.writeString(name);
        dest.writeString(status_caller);
        dest.writeString(video_caller);
        dest.writeString(email);
        dest.writeString(caller_pic);
        dest.writeString(serialsim);
    }
}
