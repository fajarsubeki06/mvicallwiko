package com.sbimv.mvicalls.pojo;

public class DataDynamicLink {
    private String shortLink;

    public DataDynamicLink() {
    }

    public String getShortLink() {
        return shortLink;
    }

    @Override
    public String toString() {
        return "DataDynamicLink{" +
                "shortLink='" + shortLink + '\'' +
                '}';
    }
}
