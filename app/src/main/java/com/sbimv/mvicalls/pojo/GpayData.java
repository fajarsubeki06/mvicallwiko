package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class GpayData {

    public String msisdn;
    public String type;
    public String json;
    public boolean cancel;
    public String periode;
    public String price;

    public GpayData(String msisdn, String type, String json, boolean cancel, String periode, String price) {
        this.msisdn = msisdn;
        this.type = type;
        this.json = json;
        this.cancel = cancel;
        this.periode = periode;
        this.price = price;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public boolean isCancel() {
        return cancel;
    }

    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
