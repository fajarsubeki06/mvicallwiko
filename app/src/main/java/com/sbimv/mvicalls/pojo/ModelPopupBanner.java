package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class ModelPopupBanner implements Parcelable {

    private String banner;

    protected ModelPopupBanner(Parcel in) {
        banner = in.readString();
    }

    public static final Creator<DataBannerPromo> CREATOR = new Creator<DataBannerPromo>() {
        @Override
        public DataBannerPromo createFromParcel(Parcel in) {
            return new DataBannerPromo(in);
        }

        @Override
        public DataBannerPromo[] newArray(int size) {
            return new DataBannerPromo[size];
        }
    };

    public String getBanner() {
        return TextUtils.isEmpty(banner)?"":banner;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(banner);
    }

}
