package com.sbimv.mvicalls.pojo;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BackSound implements Serializable {

    @SerializedName("id")
    private String id;
    @SerializedName("judul")
    private String judul;
    @SerializedName("sound")
    private String sound;
    @SerializedName("singer")
    private String singer;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public String getSinger() {
        return TextUtils.isEmpty(singer)?"No Singer":singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    @Override
    public String toString() {
        return "BackSound{" +
                "id='" + id + '\'' +
                ", judul='" + judul + '\'' +
                ", sound='" + sound + '\'' +
                ", singer='" + singer + '\'' +
                '}';
    }
}
