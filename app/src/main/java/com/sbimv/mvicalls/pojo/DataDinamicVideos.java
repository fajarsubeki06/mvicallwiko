package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class DataDinamicVideos implements Parcelable{
    public String category_id;
    public String category_name;
    public String content_id;
    public String alias;
    public String judul;
    public String price_label;
    public String source_content;
    public String thumb_pic;
    public String price;

    public DataDinamicVideos() {
    }

    public String getCategory_id() {
        return TextUtils.isEmpty(category_id)?"":category_id;
    }

    public String getCategory_name() {
        return TextUtils.isEmpty(category_name)?"":category_name;
    }

    public String getContent_id() {
        return TextUtils.isEmpty(content_id)?"":content_id;
    }

    public String getAlias() {
        return TextUtils.isEmpty(alias)?"":alias;
    }

    public String getJudul() {
        return TextUtils.isEmpty(judul)?"":judul;
    }

    public String getPrice_label() {
        return TextUtils.isEmpty(price_label)?"":price_label;
    }

    public String getSource_content() {
        return TextUtils.isEmpty(source_content)?"":source_content;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public void setPrice_label(String price_label) {
        this.price_label = price_label;
    }

    public void setSource_content(String source_content) {
        this.source_content = source_content;
    }

    public void setThumb_pic(String thumb_pic) {
        this.thumb_pic = thumb_pic;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getThumb_pic() {
        return TextUtils.isEmpty(thumb_pic)?"":thumb_pic;
    }

    public String getPrice() {
        return TextUtils.isEmpty(price)?"":price;
    }

    protected DataDinamicVideos(Parcel in) {
        category_id = in.readString();
        category_name = in.readString();
        content_id = in.readString();
        alias = in.readString();
        judul = in.readString();
        price_label = in.readString();
        source_content = in.readString();
        thumb_pic = in.readString();
        price = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category_id);
        dest.writeString(category_name);
        dest.writeString(content_id);
        dest.writeString(alias);
        dest.writeString(judul);
        dest.writeString(price_label);
        dest.writeString(source_content);
        dest.writeString(thumb_pic);
        dest.writeString(price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DataDinamicVideos> CREATOR = new Creator<DataDinamicVideos>() {
        @Override
        public DataDinamicVideos createFromParcel(Parcel in) {
            return new DataDinamicVideos(in);
        }

        @Override
        public DataDinamicVideos[] newArray(int size) {
            return new DataDinamicVideos[size];
        }
    };
}
