package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ModelDataContent {

    private String limit;
    private String last_update;
    private ArrayList<ModelContentVideo> content;

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public ArrayList<ModelContentVideo> getContent() {
        return content;
    }

    public void setContent(ArrayList<ModelContentVideo> content) {
        this.content = content;
    }
}
