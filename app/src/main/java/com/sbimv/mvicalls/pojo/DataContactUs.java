package com.sbimv.mvicalls.pojo;


public class DataContactUs {

    public String call;
    public String email;
    public String fb;
    public String ig;

    public DataContactUs() {
    }

    public DataContactUs(String call, String email, String fb, String ig) {
        this.call = call;
        this.email = email;
        this.fb = fb;
        this.ig = ig;
    }

    public String getCall() {
        return call;
    }

    public void setCall(String call) {
        this.call = call;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFb() {
        return fb;
    }

    public void setFb(String fb) {
        this.fb = fb;
    }

    public String getIg() {
        return ig;
    }

    public void setIg(String ig) {
        this.ig = ig;
    }
}
