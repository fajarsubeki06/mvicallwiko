package com.sbimv.mvicalls.pojo;

/**
 * Created by admin on 12/20/2017.
 */

public class ChargingRequest {

    private String caller_id;
    private String price;
    private String content_id;

    public String getCaller_id() {
        return caller_id;
    }

    public void setCaller_id(String caller_id) {
        this.caller_id = caller_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }
}
