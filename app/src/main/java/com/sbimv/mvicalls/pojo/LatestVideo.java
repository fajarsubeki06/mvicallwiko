package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Hendi on 10/28/17.
 */

public class LatestVideo implements Parcelable {
    public String artis_id;
    public String name;
    public String price;
    public String artis_image;
    public String type;
    public String judul;
    public String source_content;
    public String category;
    public String label;
    public String thumb_pic;
    public String content_id;

    public LatestVideo() {
    }


    protected LatestVideo(Parcel in) {
        artis_id = in.readString();
        name = in.readString();
        price = in.readString();
        artis_image = in.readString();
        type = in.readString();
        judul = in.readString();
        source_content = in.readString();
        category = in.readString();
        label = in.readString();
        thumb_pic = in.readString();
        content_id = in.readString();
    }

    public static final Creator<LatestVideo> CREATOR = new Creator<LatestVideo>() {
        @Override
        public LatestVideo createFromParcel(Parcel in) {
            return new LatestVideo(in);
        }

        @Override
        public LatestVideo[] newArray(int size) {
            return new LatestVideo[size];
        }
    };

    @Override
    public String toString() {
        return "LatestVideo{" +
                "artis_id='" + artis_id + '\'' +
                ", name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", artis_image='" + artis_image + '\'' +
                ", type='" + type + '\'' +
                ", judul='" + judul + '\'' +
                ", source_content='" + source_content + '\'' +
                ", category='" + category + '\'' +
                ", label='" + label + '\'' +
                ", thumb_pic='" + thumb_pic + '\'' +
                ", content_id='" + content_id + '\'' +
                '}';
    }

    public String getArtis_id() {
        return artis_id;
    }

    public void setArtis_id(String artis_id) {
        this.artis_id = artis_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getArtis_image() {
        return artis_image;
    }

    public void setArtis_image(String artis_image) {
        this.artis_image = artis_image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getSource_content() {
        return source_content;
    }

    public void setSource_content(String source_content) {
        this.source_content = source_content;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getThumb_pic() {
        return thumb_pic;
    }

    public void setThumb_pic(String thumb_pic) {
        this.thumb_pic = thumb_pic;
    }

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(artis_id);
        dest.writeString(name);
        dest.writeString(price);
        dest.writeString(artis_image);
        dest.writeString(type);
        dest.writeString(judul);
        dest.writeString(source_content);
        dest.writeString(category);
        dest.writeString(label);
        dest.writeString(thumb_pic);
        dest.writeString(content_id);
    }
}
