package com.sbimv.mvicalls.pojo;

import android.text.TextUtils;

import java.io.Serializable;

public class DataSubs implements Serializable {

    private String ID;
    private String msisdn;
    private String operator_id;
    private String service;
    private String status;
    private String apps_status;
    private String sub_start;
    private String sub_end;
    private String renewal;
    public String free_start;
    public String free_end;
    public String server_date;
    public long free_duration;
    public boolean free_sub;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(String operator_id) {
        this.operator_id = operator_id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getStatus() {
        return TextUtils.isEmpty(status) ? "" : status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApps_status() {
        return apps_status;
    }

    public void setApps_status(String apps_status) {
        this.apps_status = apps_status;
    }

    public String getSub_start() {
        return TextUtils.isEmpty(sub_start) ? "" : sub_start;
    }

    public void setSub_start(String sub_start) {
        this.sub_start = sub_start;
    }

    public String getSub_end() {
        return sub_end;
    }

    public void setSub_end(String sub_end) {
        this.sub_end = sub_end;
    }

    public String getRenewal() {
        return renewal;
    }

    public void setRenewal(String renewal) {
        this.renewal = renewal;
    }

    public String getFree_start() { return free_start; }

    public String getFree_end() { return free_end; }

    public long getFree_duration() { return free_duration; }

    public String getServer_date() {
        return TextUtils.isEmpty(server_date) ? "" : server_date;
    }

    public boolean isFree_sub() {
        return free_sub;
    }

    public void setFree_sub(boolean free_sub) {
        this.free_sub = free_sub;
    }

    @Override
    public String toString() {
        return "DataSubs{" +
                "ID='" + ID + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", operator_id='" + operator_id + '\'' +
                ", service='" + service + '\'' +
                ", status='" + status + '\'' +
                ", apps_status='" + apps_status + '\'' +
                ", sub_start='" + sub_start + '\'' +
                ", sub_end='" + sub_end + '\'' +
                ", renewal='" + renewal + '\'' +
                ", free_start='" + free_start + '\'' +
                ", free_end='" + free_end + '\'' +
                ", server_date='" + server_date + '\'' +
                '}';
    }
}
