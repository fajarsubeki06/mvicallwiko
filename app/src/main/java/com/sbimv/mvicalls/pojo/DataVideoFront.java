package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class DataVideoFront implements Parcelable{

    private String content_id;
    private String judul;
    private String source_content;
    private String thumb_pic;
    private String price;
    private String alias;

    public DataVideoFront() {
    }

    protected DataVideoFront(Parcel in) {
        content_id = in.readString();
        judul = in.readString();
        source_content = in.readString();
        thumb_pic = in.readString();
        price = in.readString();
        alias = in.readString();
    }

    public static final Creator<DataVideoFront> CREATOR = new Creator<DataVideoFront>() {
        @Override
        public DataVideoFront createFromParcel(Parcel in) {
            return new DataVideoFront(in);
        }

        @Override
        public DataVideoFront[] newArray(int size) {
            return new DataVideoFront[size];
        }
    };

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getSource_content() {
        return source_content;
    }

    public void setSource_content(String source_content) {
        this.source_content = source_content;
    }

    public String getThumb_pic() {
        return thumb_pic;
    }

    public void setThumb_pic(String thumb_pic) {
        this.thumb_pic = thumb_pic;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(content_id);
        parcel.writeString(judul);
        parcel.writeString(source_content);
        parcel.writeString(thumb_pic);
        parcel.writeString(price);
        parcel.writeString(alias);
    }
}
