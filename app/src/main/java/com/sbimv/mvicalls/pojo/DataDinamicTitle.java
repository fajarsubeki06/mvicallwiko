package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class DataDinamicTitle implements Parcelable{

    private String category;
    private String others;
    private ArrayList<DataDinamicVideos> content;
    private ArrayList<DataDinamicTitle> child;

    public DataDinamicTitle() {

    }

    protected DataDinamicTitle(Parcel in) {
        category = in.readString();
        others = in.readString();
        content = in.createTypedArrayList(DataDinamicVideos.CREATOR);
        child = in.createTypedArrayList(DataDinamicTitle.CREATOR);
    }

    public static final Creator<DataDinamicTitle> CREATOR = new Creator<DataDinamicTitle>() {
        @Override
        public DataDinamicTitle createFromParcel(Parcel in) {
            return new DataDinamicTitle(in);
        }

        @Override
        public DataDinamicTitle[] newArray(int size) {
            return new DataDinamicTitle[size];
        }
    };

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public ArrayList<DataDinamicVideos> getContent() {
        return content;
    }

    public void setContent(ArrayList<DataDinamicVideos> content) {
        this.content = content;
    }
    public ArrayList<DataDinamicTitle> getChild() {
        return child;
    }

    public void setChild(ArrayList<DataDinamicTitle> child) {
        this.child = child;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(category);
        parcel.writeString(others);
        parcel.writeTypedList(content);
        parcel.writeTypedList(child);
    }
}
