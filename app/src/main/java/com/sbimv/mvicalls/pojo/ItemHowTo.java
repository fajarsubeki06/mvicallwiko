package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class ItemHowTo implements Parcelable {
    private String item_desc;
    private String file;

    protected ItemHowTo(Parcel in) {
        item_desc = in.readString();
        file = in.readString();
    }

    public static final Creator<ItemHowTo> CREATOR = new Creator<ItemHowTo>() {
        @Override
        public ItemHowTo createFromParcel(Parcel in) {
            return new ItemHowTo(in);
        }

        @Override
        public ItemHowTo[] newArray(int size) {
            return new ItemHowTo[size];
        }
    };

    public String getItem_desc() {
        return item_desc;
    }

    public String getFile() {
        return file;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(item_desc);
        dest.writeString(file);
    }
}