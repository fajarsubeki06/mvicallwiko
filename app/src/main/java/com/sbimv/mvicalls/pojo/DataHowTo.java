package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class DataHowTo implements Parcelable {

    private String id;
    private String menu;
    private String url;


    protected DataHowTo(Parcel in) {
        id = in.readString();
        menu = in.readString();
        url = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(menu);
        dest.writeString(url);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DataHowTo> CREATOR = new Creator<DataHowTo>() {
        @Override
        public DataHowTo createFromParcel(Parcel in) {
            return new DataHowTo(in);
        }

        @Override
        public DataHowTo[] newArray(int size) {
            return new DataHowTo[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getMenu() {
        return menu;
    }

    public String getUrl() {
        return url;
    }
}

