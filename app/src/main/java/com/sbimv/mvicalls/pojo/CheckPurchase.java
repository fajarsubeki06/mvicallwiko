package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class CheckPurchase implements Parcelable {

    private String status;

    protected CheckPurchase(Parcel in) {
        status = in.readString();
    }

    public String getStatus() {
        return status;
    }

    public static final Creator<CheckPurchase> CREATOR = new Creator<CheckPurchase>() {
        @Override
        public CheckPurchase createFromParcel(Parcel in) {
            return new CheckPurchase(in);
        }

        @Override
        public CheckPurchase[] newArray(int size) {
            return new CheckPurchase[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(status);
    }
}
