package com.sbimv.mvicalls.pojo;

import com.google.gson.annotations.SerializedName;

public class DataStatusSubscribe {

    public DataStatusSubscribe() {
    }

    @SerializedName("ID")
    private String iD;
    @SerializedName("msisdn")
    private String msisdn;
    @SerializedName("operator_id")
    private String operatorId;
    @SerializedName("service")
    private String service;
    @SerializedName("status")
    private String status;
    @SerializedName("apps_status")
    private String appsStatus;
    @SerializedName("sub_start")
    private String subStart;
    @SerializedName("sub_end")
    private String subEnd;
    @SerializedName("renewal")
    private String renewal;


    public String getiD() {
        return iD;
    }

    public void setiD(String iD) {
        this.iD = iD;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAppsStatus() {
        return appsStatus;
    }

    public void setAppsStatus(String appsStatus) {
        this.appsStatus = appsStatus;
    }

    public String getSubStart() {
        return subStart;
    }

    public void setSubStart(String subStart) {
        this.subStart = subStart;
    }

    public String getSubEnd() {
        return subEnd;
    }

    public void setSubEnd(String subEnd) {
        this.subEnd = subEnd;
    }

    public String getRenewal() {
        return renewal;
    }

    public void setRenewal(String renewal) {
        this.renewal = renewal;
    }
}

