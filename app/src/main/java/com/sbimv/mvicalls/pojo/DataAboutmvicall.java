package com.sbimv.mvicalls.pojo;


public class DataAboutmvicall {

    public String privacy;
    public String tnc;

    public DataAboutmvicall(String privacy, String tnc) {
        this.privacy = privacy;
        this.tnc = tnc;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getTnc() {
        return tnc;
    }

    public void setTnc(String tnc) {
        this.tnc = tnc;
    }
}
