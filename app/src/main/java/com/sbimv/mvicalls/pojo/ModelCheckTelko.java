package com.sbimv.mvicalls.pojo;

public class ModelCheckTelko {
    public String id;
    public String name;
    public String long_name;
    public String auth_type;

    public ModelCheckTelko(String id, String name, String long_name, String auth_type) {
        this.id = id;
        this.name = name;
        this.long_name = long_name;
        this.auth_type = auth_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getAuth_type() {
        return auth_type;
    }

    public void setAuth_type(String auth_type) {
        this.auth_type = auth_type;
    }
}
