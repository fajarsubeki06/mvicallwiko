package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class DataMGM implements Parcelable {
    private String banner;
    private String tnc;
    private String rule;
    private String winner;
    private MGMRank current_user;
    private List<MGMRank> list;

    protected DataMGM(Parcel in) {
        banner = in.readString();
        rule = in.readString();
        tnc = in.readString();
        winner = in.readString();
        current_user = in.readParcelable(MGMRank.class.getClassLoader());
        list = in.createTypedArrayList(MGMRank.CREATOR);
    }

    public static final Creator<DataMGM> CREATOR = new Creator<DataMGM>() {
        @Override
        public DataMGM createFromParcel(Parcel in) {
            return new DataMGM(in);
        }

        @Override
        public DataMGM[] newArray(int size) {
            return new DataMGM[size];
        }
    };

    public String getBanner() {
        return banner;
    }

    public String getRule() {
        return rule;
    }

    public String getTnc() {
        return tnc;
    }

    public String getWinner() {
        return winner;
    }

    public MGMRank getCurrent_user() {
        return current_user;
    }

    public List<MGMRank> getList() {
        return list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(banner);
        parcel.writeString(rule);
        parcel.writeString(tnc);
        parcel.writeString(winner);
        parcel.writeParcelable(current_user, i);
        parcel.writeTypedList(list);
    }

    public static class MGMRank implements Parcelable{
        private int no;
        private String name;
        private String msisdn;
        private String poin;
        private String note;

        protected MGMRank(Parcel in) {
            no = in.readInt();
            name = in.readString();
            msisdn = in.readString();
            poin = in.readString();
            note = in.readString();
        }

        public static final Creator<MGMRank> CREATOR = new Creator<MGMRank>() {
            @Override
            public MGMRank createFromParcel(Parcel in) {
                return new MGMRank(in);
            }

            @Override
            public MGMRank[] newArray(int size) {
                return new MGMRank[size];
            }
        };

        public int getNo() {
            return no;
        }

        public String getName() {
            return name;
        }

        public String getMsisdn() {
            return msisdn;
        }

        public String getPoin() {
            return poin;
        }

        public String getNote() {
            return note;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(no);
            parcel.writeString(name);
            parcel.writeString(msisdn);
            parcel.writeString(poin);
            parcel.writeString(note);
        }
    }
}
