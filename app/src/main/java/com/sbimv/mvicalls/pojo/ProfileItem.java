package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ProfileItem implements Parcelable {
    public ArrayList<Item> list = new ArrayList<>();

    public ProfileItem(){}

    protected ProfileItem(Parcel in) {
        if (in.readByte() == 0x01) {
            list = new ArrayList<Item>();
            in.readList(list, Item.class.getClassLoader());
        }
        else {
            list = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (list == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(list);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ProfileItem> CREATOR = new Parcelable.Creator<ProfileItem>() {
        @Override
        public ProfileItem createFromParcel(Parcel in) {
            return new ProfileItem(in);
        }

        @Override
        public ProfileItem[] newArray(int size) {
            return new ProfileItem[size];
        }
    };

    public static class Item implements Parcelable {
        public String msisdn;
        public String profile;
        public String name;
        public String caller_pic;

        public Item(){}

        public String getCaller_pic() {
            return caller_pic;
        }

        public void setCaller_pic(String caller_pic) {
            this.caller_pic = caller_pic;
        }

        protected Item(Parcel in) {
            msisdn = in.readString();
            profile = in.readString();
            name = in.readString();
            caller_pic = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(msisdn);
            dest.writeString(profile);
            dest.writeString(name);
            dest.writeString(caller_pic);
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
            @Override
            public Item createFromParcel(Parcel in) {
                return new Item(in);
            }

            @Override
            public Item[] newArray(int size) {
                return new Item[size];
            }
        };
    }
}
