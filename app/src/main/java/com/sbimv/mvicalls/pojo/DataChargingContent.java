package com.sbimv.mvicalls.pojo;

import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;
import java.io.Serializable;


public class DataChargingContent implements Serializable {
    @SerializedName("keyword")
    private String keyword;
    @SerializedName("sdc")
    private String sdc;
    @SerializedName("price")
    private int price;
    @SerializedName("desc")
    private String desc;
    @SerializedName("channel")
    private String channel;
    @SerializedName("price_label")
    private String price_label;
    @SerializedName("confirm_enable")
    private boolean confirm_enable;
    @SerializedName("title")
    private String title;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getSdc() {
        return sdc;
    }

    public void setSdc(String sdc) {
        this.sdc = sdc;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getPrice_label() {
        return TextUtils.isEmpty(price_label)?"":price_label;
    }

    public void setPrice_label(String price_label) {
        this.price_label = price_label;
    }

    public boolean isConfirm_enable() {
        return confirm_enable;
    }

    public void setConfirm_enable(boolean confirm_enable) {
        this.confirm_enable = confirm_enable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NotNull
    @Override
    public String toString() {
        return "DataChargingContent{" +
                "keyword='" + keyword + '\'' +
                ", sdc='" + sdc + '\'' +
                ", desc='" + desc + '\'' +
                ", price='" + price + '\'' +
                ", channel=" + channel +
                ", price_label=" + price_label +
                ", title=" + title +
                '}';
    }

}
