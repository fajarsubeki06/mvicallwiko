package com.sbimv.mvicalls.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataLike implements Serializable {
    @SerializedName("caller_id")
    public String caller_id;
    @SerializedName("video")
    public String video;
    @SerializedName("total")
    public String total;

    public DataLike() {

    }

    public String getCaller_id() {
        return caller_id;
    }

    public void setCaller_id(String caller_id) {
        this.caller_id = caller_id;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }


    @Override
    public String toString() {
        return "DataLike{" +
                "caller_id='" + caller_id + '\'' +
                ", video='" + video + '\'' +
                ", total='" + total + '\'' +
                '}';
    }

}
