package com.sbimv.mvicalls.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 12/19/2017.
 */

public class DataJsonCharging {

    @SerializedName("caller_id")
    @Expose
    private String caller_id;
    @SerializedName("content_id")
    @Expose
    private String content_id ;
    @SerializedName("artis_id")
    @Expose
    private String artis_id;
    @SerializedName("name")
    @Expose
    private String name ;
    @SerializedName("artis_image")
    @Expose
    private String artis_image;
    @SerializedName("judul")
    @Expose
    private String judul;
    @SerializedName("source_content")
    @Expose
    private String source_content;
    @SerializedName("text")
    @Expose
    private String text;


    public String getCaller_id() {
        return caller_id;
    }

    public void setCaller_id(String caller_id) {
        this.caller_id = caller_id;
    }

    public String getContent_id() {
        return content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getArtis_id() {
        return artis_id;
    }

    public void setArtis_id(String artis_id) {
        this.artis_id = artis_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtis_image() {
        return artis_image;
    }

    public void setArtis_image(String artis_image) {
        this.artis_image = artis_image;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getSource_content() {
        return source_content;
    }

    public void setSource_content(String source_content) {
        this.source_content = source_content;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

