package com.sbimv.mvicalls.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hendi on 9/26/17.
 */

public abstract class BaseResponse {
    @SerializedName("code")
    public String code;
    public String error_message;

    public boolean isSuccessful(){
        return code.equalsIgnoreCase("200");
    }
}
