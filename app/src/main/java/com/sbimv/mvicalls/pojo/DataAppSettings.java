package com.sbimv.mvicalls.pojo;


public class DataAppSettings {

    public DataContactUs contactUs;
    public DataAboutmvicall aboutmvicall;

    public DataAppSettings() {
    }

    public DataContactUs getContactUs() {
        return contactUs;
    }

    public DataAboutmvicall getAboutmvicall() {
        return aboutmvicall;
    }
}
