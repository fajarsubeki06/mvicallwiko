package com.sbimv.mvicalls.pojo;

/**
 * Created by admin on 1/3/2018.
 */

public class SetVideoTone {
    public String caller_id;
    public String msisdn ;
    public String name;
    public String status_caller;
    public String source_content;
    public String caller_pic ;

    public SetVideoTone(String video_caller) {
        this.source_content = video_caller;
    }

    public String getCaller_id() {
        return caller_id;
    }

    public void setCaller_id(String caller_id) {
        this.caller_id = caller_id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus_caller() {
        return status_caller;
    }

    public void setStatus_caller(String status_caller) {
        this.status_caller = status_caller;
    }

    public String getSource_content() {
        return source_content;
    }

    public void setSource_content(String source_content) {
        this.source_content = source_content;
    }

    public String getCaller_pic() {
        return caller_pic;
    }

    public void setCaller_pic(String caller_pic) {
        this.caller_pic = caller_pic;
    }
}
