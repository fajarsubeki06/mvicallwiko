package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class ConfigData implements Parcelable {

    public String wording_share_id;
    public String wording_share_en;
    public String wording_showcase_id;
    public String wording_showcase_en;
    public String wording_free_id;
    public String wording_free_en;
    public String version_code;
    public boolean googlepay_enable;
    public String version_lang;
    public boolean a2ac_android;
    public boolean a2ac_ios;
    public boolean mgm_enable;
    public int sync_duration;
    public boolean viettel_pay_enable;
    public boolean showcase_finish;
    public boolean point_reward;
    public boolean best_deal;
    public int max_duration;

    public static final Creator<ConfigData> CREATOR = new Creator<ConfigData>() {
        @Override
        public ConfigData createFromParcel(Parcel in) {
            return new ConfigData(in);
        }

        @Override
        public ConfigData[] newArray(int size) {
            return new ConfigData[size];
        }
    };

    public ConfigData(){}

    protected ConfigData(Parcel in) {
        wording_share_id = in.readString();
        wording_share_en = in.readString();
        wording_showcase_id = in.readString();
        wording_showcase_en = in.readString();
        wording_free_id = in.readString();
        wording_free_en = in.readString();
        version_code = in.readString();
        version_lang = in.readString();
        googlepay_enable = in.readByte() != 0;
        a2ac_android = in.readByte() != 0;
        a2ac_ios = in.readByte() != 0;
        mgm_enable = in.readByte() != 0;
        sync_duration = in.readInt();
        viettel_pay_enable = in.readByte() != 0;
        showcase_finish = in.readByte() != 0;
        point_reward = in.readByte() != 0;
        best_deal = in.readByte() != 0;
        max_duration = in.readInt();
    }

    public String getWording_share_id() {
        return wording_share_id;
    }

    public String getWording_share_en() {
        return wording_share_en;
    }

    public String getWording_showcase_id() {
        return wording_showcase_id;
    }

    public String getWording_showcase_en() {
        return wording_showcase_en;
    }

    public String getWording_free_id() {
        return wording_free_id;
    }

    public String getWording_free_en() {
        return wording_free_en;
    }

    public String getVersion_code() {
        return version_code;
    }

    public boolean isGooglepay_enable() {
        return googlepay_enable;
    }

    public boolean isA2ac_andorid() {
        return a2ac_android;
    }

    public boolean isA2ac_ios() {
        return a2ac_ios;
    }

    public boolean isMgm_enable() {
        return mgm_enable;
    }

    public String getVersion_lang() {
        return version_lang== null ? "":version_lang;
    }

    public int getSync_duration() {
        return sync_duration;
    }

    public boolean isViettel_pay_enable() {
        return viettel_pay_enable;
    }

    public boolean isShowcase_finish() {
        return showcase_finish;
    }

    public boolean isPoint_reward() {
        return point_reward;
    }

    public boolean isBest_deal() {
        return best_deal;
    }

    public int getMax_duration() {
        return max_duration;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(wording_share_id);
        parcel.writeString(wording_share_en);
        parcel.writeString(wording_showcase_id);
        parcel.writeString(wording_showcase_en);
        parcel.writeString(wording_free_id);
        parcel.writeString(wording_free_en);
        parcel.writeString(version_code);
        parcel.writeByte((byte) (googlepay_enable ? 1 : 0));
        parcel.writeByte((byte) (a2ac_android ? 1 : 0));
        parcel.writeByte((byte) (a2ac_ios ? 1 : 0));
        parcel.writeByte((byte) (mgm_enable ? 1 : 0));
        parcel.writeByte((byte) (viettel_pay_enable ? 1 : 0));
        parcel.writeByte((byte) (showcase_finish ? 1 : 0));
        parcel.writeByte((byte) (point_reward ? 1 : 0));
        parcel.writeByte((byte) (best_deal ? 1 : 0));
        parcel.writeString(version_lang);
        parcel.writeInt(sync_duration);
        parcel.writeInt(max_duration);
    }
}
