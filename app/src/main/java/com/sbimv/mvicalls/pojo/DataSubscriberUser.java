package com.sbimv.mvicalls.pojo;

import java.io.Serializable;
import java.util.List;

public class DataSubscriberUser implements Serializable {

    private int code;
    private String error_message;
    private List <DataSubs> data;

    public List<DataSubs> getData() {
        return data;
    }

    public void setData(List<DataSubs> data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }
}

