package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 12/18/2017.
 */

public class PremiumCharging implements Parcelable {
    public String caller_id;
    public String content_id;
    public String artis_id;
    public String name;
    public String artis_image ;
    public String judul;
    public String source_content;
    public String text;


    protected PremiumCharging(Parcel in) {
        caller_id = in.readString();
        content_id = in.readString();
        artis_id = in.readString();
        name = in.readString();
        artis_image = in.readString();
        judul = in.readString();
        source_content = in.readString();
        text = in.readString();
    }

    public static final Creator<PremiumCharging> CREATOR = new Creator<PremiumCharging>() {
        @Override
        public PremiumCharging createFromParcel(Parcel in) {
            return new PremiumCharging(in);
        }

        @Override
        public PremiumCharging[] newArray(int size) {
            return new PremiumCharging[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(caller_id);
        parcel.writeString(content_id);
        parcel.writeString(artis_id);
        parcel.writeString(name);
        parcel.writeString(artis_image);
        parcel.writeString(judul);
        parcel.writeString(source_content);
        parcel.writeString(text);
    }
}