package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class ContactItem implements Parcelable {
    public String caller_id;
    public String msisdn;
    public String name;
    public String name_phone_book;// added name phone book
    public String status_caller;
    public String device;
    public String video_caller;
    public String status;
    public String created_date;
    public String email;
    public String caller_pic;
    public String profile_status; // added by Hendi - 30/08/2018
    public String ratio;
    public ProfileItem profiles = new ProfileItem();

    public ContactItem(){}

    public String getName(){
        return TextUtils.isEmpty(name)?"":name;
    }

    public String getName(String def){
        return TextUtils.isEmpty(name)?def:name;
    }

    public String getName_phone_book(String pb){ return TextUtils.isEmpty(name_phone_book)?pb:name_phone_book; } // added name phone book

    public String getEmail(){
        return email==null?"":email;
    }

    public String getStatusCaller(){
        return TextUtils.isEmpty(status_caller)?"'No status'":"'"+status_caller+"'";
    }

    public String getDevice(){
        return TextUtils.isEmpty(device)?"":device;
    }

    public String getRatio(){
        return TextUtils.isEmpty(ratio)?"":ratio;
    }

    protected ContactItem(Parcel in) {
        caller_id = in.readString();
        msisdn = in.readString();
        name = in.readString();
        name_phone_book = in.readString();// added name phone book
        status_caller = in.readString();
        video_caller = in.readString();
        status = in.readString();
        device = in.readString();
        created_date = in.readString();
        email = in.readString();
        caller_pic = in.readString();
        profile_status = in.readString();
        ratio = in.readString();
        profiles = (ProfileItem) in.readValue(ProfileItem.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(caller_id);
        dest.writeString(msisdn);
        dest.writeString(name);
        dest.writeString(name_phone_book);// added name phone book
        dest.writeString(status_caller);
        dest.writeString(video_caller);
        dest.writeString(status);
        dest.writeString(device);
        dest.writeString(created_date);
        dest.writeString(email);
        dest.writeString(caller_pic);
        dest.writeString(profile_status);
        dest.writeString(ratio);
        dest.writeValue(profiles);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ContactItem> CREATOR = new Parcelable.Creator<ContactItem>() {
        @Override
        public ContactItem createFromParcel(Parcel in) {
            return new ContactItem(in);
        }

        @Override
        public ContactItem[] newArray(int size) {
            return new ContactItem[size];
        }
    };
}
