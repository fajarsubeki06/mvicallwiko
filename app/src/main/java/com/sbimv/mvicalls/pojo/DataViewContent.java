package com.sbimv.mvicalls.pojo;


import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class DataViewContent implements Serializable {

    @SerializedName("content_id")
    public String content_id;
    @SerializedName("alias")
    public String alias;
    @SerializedName("source_content")
    public String source_content;
    @SerializedName("thumb_pic")
    public String thumb_pic;
    @SerializedName("price")
    public int price;
    @SerializedName("price_label")
    public String price_label;
    @SerializedName("title")
    private String title;
    @SerializedName("others")
    private String others;

    @SerializedName("content")
    private ArrayList<DataDinamicVideos> content;

    public DataViewContent(String content_id, String alias, String source_content, String thumb_pic, int price, String price_label, String title, String others, ArrayList<DataDinamicVideos> content) {
        this.content_id = content_id;
        this.alias = alias;
        this.source_content = source_content;
        this.thumb_pic = thumb_pic;
        this.price = price;
        this.price_label = price_label;
        this.title = title;
        this.others = others;
        this.content = content;
    }

    public String getContent_id() {
        return TextUtils.isEmpty(content_id)?"":content_id;
    }

    public void setContent_id(String content_id) {
        this.content_id = content_id;
    }

    public String getAlias() {
        return TextUtils.isEmpty(alias)?"":alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getSource_content() {
        return TextUtils.isEmpty(source_content)?"":source_content;
    }

    public void setSource_content(String source_content) {
        this.source_content = source_content;
    }

    public String getThumb_pic() {
        return TextUtils.isEmpty(thumb_pic)?"":thumb_pic;
    }

    public void setThumb_pic(String thumb_pic) {
        this.thumb_pic = thumb_pic;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getPrice_label() {
        return TextUtils.isEmpty(price_label)?"":price_label;
    }

    public void setPrice_label(String price_label) {
        this.price_label = price_label;
    }

    public String getTitle() {
        return TextUtils.isEmpty(title)?"":title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOthers() {
        return TextUtils.isEmpty(others)?"":others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public ArrayList<DataDinamicVideos> getContent() {
        return content;
    }

    public void setContent(ArrayList<DataDinamicVideos> content) {
        this.content = content;
    }

}
