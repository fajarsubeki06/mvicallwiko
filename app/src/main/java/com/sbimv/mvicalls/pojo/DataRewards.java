package com.sbimv.mvicalls.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class DataRewards implements Parcelable {
    private String msisdn;
    private String name;
    private String poin;
    private ArrayList<ListRewards> rewardList;

    protected DataRewards(Parcel in) {
        msisdn = in.readString();
        name = in.readString();
        poin = in.readString();
        rewardList = in.createTypedArrayList(ListRewards.CREATOR);
    }

    public static final Creator<DataRewards> CREATOR = new Creator<DataRewards>() {
        @Override
        public DataRewards createFromParcel(Parcel in) {
            return new DataRewards(in);
        }

        @Override
        public DataRewards[] newArray(int size) {
            return new DataRewards[size];
        }
    };

    public String getMsisdn() {
        return msisdn;
    }

    public String getName() {
        return name;
    }

    public String getPoin() {
        return poin;
    }

    public ArrayList<ListRewards> getRewardList() {
        return rewardList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(msisdn);
        parcel.writeString(name);
        parcel.writeString(poin);
        parcel.writeTypedList(rewardList);
    }

    public static class ListRewards implements Parcelable{
        private String reward_id;
        private String banner;
        private String headline;
        private String icon;
        private String poin;
        private String type;
        private String price;
        private String total_item;
        private String tnc;

        protected ListRewards(Parcel in) {
            reward_id = in.readString();
            banner = in.readString();
            headline = in.readString();
            icon = in.readString();
            poin = in.readString();
            type = in.readString();
            price = in.readString();
            total_item = in.readString();
            tnc = in.readString();
        }

        public static final Creator<ListRewards> CREATOR = new Creator<ListRewards>() {
            @Override
            public ListRewards createFromParcel(Parcel in) {
                return new ListRewards(in);
            }

            @Override
            public ListRewards[] newArray(int size) {
                return new ListRewards[size];
            }
        };

        public String getReward_id() {
            return reward_id;
        }

        public String getBanner() {
            return banner;
        }

        public String getHeadline() {
            return headline;
        }

        public String getIcon() {
            return icon;
        }

        public String getPoin() {
            return poin;
        }

        public String getType() {
            return type;
        }

        public String getPrice() {
            return price;
        }

        public String getTotal_item() {
            return total_item;
        }

        public String getTnc() {
            return tnc;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(reward_id);
            parcel.writeString(banner);
            parcel.writeString(headline);
            parcel.writeString(icon);
            parcel.writeString(poin);
            parcel.writeString(type);
            parcel.writeString(price);
            parcel.writeString(total_item);
            parcel.writeString(tnc);
        }
    }

}

