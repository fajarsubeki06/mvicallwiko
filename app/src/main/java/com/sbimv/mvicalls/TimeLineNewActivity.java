package com.sbimv.mvicalls;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.appevents.AppEventsLogger;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.firebase.database.annotations.NotNull;
import com.sbimv.mvicalls.activity.MainActivity;
import com.sbimv.mvicalls.http.ServicesFactory;
import com.sbimv.mvicalls.language.LangViewModel;
import com.sbimv.mvicalls.pojo.APIResponse;
import com.sbimv.mvicalls.pojo.ContactItem;
import com.sbimv.mvicalls.pojo.ModelContentVideo;
import com.sbimv.mvicalls.pojo.ModelDataContent;
import com.sbimv.mvicalls.timeline.DividerItemDecoration;
import com.sbimv.mvicalls.timeline.PaginationScrollListener;
import com.sbimv.mvicalls.timeline.VideoRecycler;
import com.sbimv.mvicalls.util.PreferenceUtil;
import com.sbimv.mvicalls.util.SessionManager;
import com.sbimv.mvicalls.util.ShowCaseListener;
import com.sbimv.mvicalls.util.ShowCaseWrapper;
import com.sbimv.mvicalls.walktrough.SetupProfileActivity;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimeLineNewActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private VideoRecycler mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ExoPlayerRecycler recyclerViewFeed;
    private boolean firstTime = true;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 10;
    private int currentPage = 0;
    private ProgressBar mProgressBar;
    private ModelDataContent dataContent;
    private String limit;
    private String lastUpdate;
    private int PAGE_CONTENT;
    private LinearLayoutManager linearLayoutManager;
    private ShimmerFrameLayout shimmerFrameLayout;
    LangViewModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_line_new);
        model = LangViewModel.getInstance();
        TextView tv = findViewById(R.id.tv_toolbar_title);
        tv.setText(model.getLang().string.liveExplore);

        shimmerFrameLayout = findViewById(R.id.shimmerExplore);
        new Handler().postDelayed(() -> {
            if (!TimeLineNewActivity.this.isFinishing()) {
                shimmerFrameLayout.setVisibility(View.GONE);
            }
        }, BaseActivity.TIME_LOADING);
        shimmerFrameLayout.stopShimmer();

        initialize();
        loadContentVideo();
        triggerScreenExplore();

        String ccodeViet = PreferenceUtil.getPref(TimeLineNewActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");

        Intent data = getIntent();
        if (data != null){
            String key = data.getStringExtra("KEY_EXPLORE");
            if (key != null){
                if (!TextUtils.isEmpty(ccodeViet) && Objects.requireNonNull(ccodeViet).equalsIgnoreCase("+84")){
                    checkShowCase();
                }
            }
        }


    }

    private void triggerScreenExplore(){

//        /*
//         * Adjust................
//         * */
//        AdjustEvent event = new AdjustEvent(ADJUST_EVENT_TOKEN_SCREEN_EXPLORE);
//        event.setCallbackId("aj_screen_lv_explore");
//        event.addCallbackParameter("key", "value");
//        event.addPartnerParameter("foo", "bar");
//        Adjust.trackEvent(event);

        /*
         * Facebook Screen Event
         * */
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent("fcb_screen_lv_explore");

        /*
         * GA Screen Event
         * */
        GATRacker.getInstance(getApplication()).sendScreen(TrackerConstant.SCREEN_LV_EXPLORE);
    }

    private void initialize(){

        Toolbar mToolbar = findViewById(R.id.my_toolbarss);
        setSupportActionBar(mToolbar);

        mSwipeRefreshLayout = findViewById(R.id.swipe_list);
        recyclerViewFeed = findViewById(R.id.recyclerViewFeed);
        mProgressBar = findViewById(R.id.main_progress);
        ImageView mBackArrow = findViewById(R.id.btn_toolbar_actionbar_back);

        mAdapter = new VideoRecycler(this);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        Drawable dividerDrawable = ContextCompat.getDrawable(this, R.drawable.divider_drawable);

        recyclerViewFeed.setLayoutManager(linearLayoutManager);
        recyclerViewFeed.addItemDecoration(new DividerItemDecoration(dividerDrawable));
        recyclerViewFeed.setItemAnimator(new DefaultItemAnimator());
        recyclerViewFeed.setAdapter(mAdapter);

        //mProgressBar.setVisibility(View.VISIBLE);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        mBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(TimeLineNewActivity.this, MainActivity.class).putExtra("TIMELINE", "true"));
//                finish();
                onBackPressed();
            }
        });

        recyclerViewFeed.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage = 1;

                try{
                    if (!TextUtils.isEmpty(lastUpdate) && PAGE_CONTENT == TOTAL_PAGES) {
                        loadContentVideo(lastUpdate);
                    }
                }catch (NumberFormatException e){
                    e.printStackTrace();
                }
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        String ccodeViet = PreferenceUtil.getPref(TimeLineNewActivity.this).getString(PreferenceUtil.COUNTRY_CODE,"1");
        if (!TextUtils.isEmpty(ccodeViet) && Objects.requireNonNull(ccodeViet).equalsIgnoreCase("+84")) {

            Intent data = getIntent();
            if (data != null) {
                String key = data.getStringExtra("Setup");
                if (key != null) {
                    startActivity(new Intent(TimeLineNewActivity.this, SetupProfileActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(TimeLineNewActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("TIMELINE", "true"));
                    prefFriendsCase();
                    finish();
                }
            } else {
                startActivity(new Intent(TimeLineNewActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).putExtra("TIMELINE", "true"));
                prefFriendsCase();
                finish();
            }

        }else {
            prefFriendsCase();
            finish();
        }

    }

    private void prefFriendsCase(){
        PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.SHOW_CASE_MAIN, true).commit();
    }

    @Override
    protected void onPause() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                recyclerViewFeed.onPausePlayer();
            }
        });
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (!TimeLineNewActivity.this.isFinishing()) {
            if (recyclerViewFeed != null) {
                finish();
            }
        }
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        if(recyclerViewFeed!=null) {
            recyclerViewFeed.onRelease();
        }
        super.onDestroy();

    }

    private void loadContentVideo() {
        List<ModelContentVideo> loadArraylist = new ArrayList<>();
        ContactItem contactItem = SessionManager.getProfile(TimeLineNewActivity.this);
        if (contactItem == null)
            return;

        String callerId = contactItem.caller_id;

        if (TextUtils.isEmpty(callerId))
            return;

        Call<APIResponse<ModelDataContent>> call = ServicesFactory.getService().getTimeline("ID", "", callerId);
        call.enqueue(new Callback<APIResponse<ModelDataContent>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<ModelDataContent>> call, @NotNull Response<APIResponse<ModelDataContent>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    mAdapter.clear();
                    recyclerViewFeed.clear();
                    dataContent = response.body().data;

                    limit = dataContent.getLimit();
                    PAGE_CONTENT = Integer.parseInt(limit);
                    lastUpdate = dataContent.getLast_update();

                    List<ModelContentVideo> videoInfoList = dataContent.getContent();
                    if (videoInfoList ==  null)
                        return;

                    for (ModelContentVideo  model : videoInfoList){
                        try{
                            int reportStatus = model.report_status;
                            if (reportStatus != 1){
                                ModelContentVideo modelContentVideo = new ModelContentVideo();
                                modelContentVideo.setCaller_id(model.caller_id);
                                modelContentVideo.setName(model.name);
                                modelContentVideo.setName_pic(model.name_pic);
                                modelContentVideo.setThumb_video(model.thumb_video);
                                modelContentVideo.setVideo_caller(model.video_caller);
                                modelContentVideo.setLast_updated(model.last_updated);
                                modelContentVideo.setCounter(model.counter);
                                modelContentVideo.setLike_status(model.like_status);
                                modelContentVideo.setReport_status(model.report_status);
                                loadArraylist.add(modelContentVideo);
                            }
                        }catch (NumberFormatException e){
                            e.printStackTrace();
                        }
                    }
                    if (loadArraylist.size() > 0) {
                        PAGE_CONTENT = loadArraylist.size();
                        TOTAL_PAGES = loadArraylist.size();
                    }

                    mProgressBar.setVisibility(View.GONE);
                    mAdapter.addAll(loadArraylist);
                    recyclerViewFeed.setVideoInfoList(loadArraylist);
                    isLoading = false;

                    initRecycle(mAdapter);

                    mSwipeRefreshLayout.setRefreshing(false);
                    if (firstTime) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recyclerViewFeed.playVideo();
                            }
                        }, 1000);
                        firstTime = false;
                        recyclerViewFeed.scrollToPosition(0);
                    }

                } else{
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<ModelDataContent>> call, @NotNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void loadContentVideo(String id) {
        List<ModelContentVideo> loadMoreArraylist = new ArrayList<>();
        ContactItem contactItem = SessionManager.getProfile(TimeLineNewActivity.this);
        if (contactItem == null)
            return;

        String callerId = contactItem.caller_id;

        if (TextUtils.isEmpty(callerId))
            return;

        Call<APIResponse<ModelDataContent>> call = ServicesFactory.getService().getTimeline("ID", id, "");
        call.enqueue(new Callback<APIResponse<ModelDataContent>>() {
            @Override
            public void onResponse(@NotNull Call<APIResponse<ModelDataContent>> call, @NotNull Response<APIResponse<ModelDataContent>> response) {
                if (response.isSuccessful() && response.body() != null && response.body().isSuccessful()) {
                    mAdapter.removeLoadingFooter();
                    isLoading = false;

                    dataContent = response.body().data;
                    limit = dataContent.getLimit();
                    PAGE_CONTENT = Integer.parseInt(limit);
                    if (PAGE_CONTENT == 0)
                        return;

                    lastUpdate = dataContent.getLast_update();
                    List<ModelContentVideo> contentInfoLists = dataContent.getContent();
                    if (contentInfoLists ==  null)
                        return;

                    for (ModelContentVideo  model : contentInfoLists){
                        try{
                            int reportStatus = model.report_status;
                            if (reportStatus != 1){
                                ModelContentVideo modelContentVideo = new ModelContentVideo();
                                modelContentVideo.setCaller_id(model.caller_id);
                                modelContentVideo.setName(model.name);
                                modelContentVideo.setName_pic(model.name_pic);
                                modelContentVideo.setThumb_video(model.thumb_video);
                                modelContentVideo.setVideo_caller(model.video_caller);
                                modelContentVideo.setLast_updated(model.last_updated);
                                modelContentVideo.setCounter(model.counter);
                                modelContentVideo.setLike_status(model.like_status);
                                modelContentVideo.setReport_status(model.report_status);
                                loadMoreArraylist.add(modelContentVideo);
                            }
                        }catch (NumberFormatException e){
                            e.printStackTrace();
                        }
                    }
                    if (loadMoreArraylist.size() > 0) {
                        PAGE_CONTENT = loadMoreArraylist.size();
                        TOTAL_PAGES = loadMoreArraylist.size();
                    }

                    mAdapter.addAll(loadMoreArraylist);
                    recyclerViewFeed.setVideoInfoList(loadMoreArraylist);
                    mSwipeRefreshLayout.setRefreshing(false);

                    initRecycle(mAdapter);

                }else{
                    isLoading = false;
                }
            }

            @Override
            public void onFailure(@NotNull Call<APIResponse<ModelDataContent>> call, @NotNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void showCaseIntroVietNineTeen() {
        try {
            PreferenceUtil.getEditor(getApplicationContext()).putBoolean(PreferenceUtil.CASE_NINETEEEN_VIETNAME, true).commit();
            LangViewModel model = LangViewModel.getInstance();

            String strWordingTitleVietSevenTeen = TextUtils.isEmpty(model.getLang().string.caseTitleBrowseVideo)
                    ?"Here you can browse videos ringtones created by other users. Have Fun exploring MViCallers creativity!" :
                    model.getLang().string.caseTitleBrowseVideo;

            ShowCaseWrapper showCaseWrapper = ShowCaseWrapper.create(TimeLineNewActivity.this);
            showCaseWrapper.showFancyVietnameNineTeen(
                    ShowCaseWrapper.SC_VIET_NINETEEN,
                    strWordingTitleVietSevenTeen,
                    new ShowCaseListener.ActionListener() {

                        @Override
                        public void onScreenTap() {
                            ///startActivity(new Intent(TimeLineNewActivity.this, MainActivity.class).putExtra("TIMELINE", "true"));
                        }

                        @Override
                        public void onOk() {
                            //startActivity(new Intent(TimeLineNewActivity.this, MainActivity.class).putExtra("TIMELINE", "true"));
                        }

                        @Override
                        public void onSkip() {
                            //startActivity(new Intent(TimeLineNewActivity.this, MainActivity.class).putExtra("TIMELINE", "true"));
                        }

                    });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initRecycle(VideoRecycler mAdapter) {
        if (currentPage != TOTAL_PAGES)
            mAdapter.addLoadingFooter();
        else
            isLastPage = true;
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                loadContentVideo();
            }
        });
    }

    private void checkShowCase(){

        boolean caseVietnamNineTeen = PreferenceUtil.getPref(getApplicationContext()).getBoolean(PreferenceUtil.CASE_NINETEEEN_VIETNAME, false);

        if (!caseVietnamNineTeen){
            showCaseIntroVietNineTeen();
        }

    }


}
